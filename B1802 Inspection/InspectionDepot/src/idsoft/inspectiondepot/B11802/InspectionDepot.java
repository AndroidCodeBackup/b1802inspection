package idsoft.inspectiondepot.B11802;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;

public class InspectionDepot extends Activity {

	Button login, close,cleartext;
	EditText password;
	AutoCompleteTextView username;
	ProgressDialog progressDialog;
	int logincheck = 0, usercheck = 0,cleartxt=0;
	String autousername[];
	String uname, rempwd="0",pwd, dbuname, dbpwd, dbinsid, status = "false";
	private static final String TAG = null;
	TableLayout tblshow;
	View v1;
	TextWatcher watcher;
	CheckBox rememberpwd;
	public CommonFunctions cf;
	android.text.format.DateFormat df;
	CharSequence cd;
	SoapObject resultforemailphotoextn;
	ProgressDialog pd;
	int k=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		cf = new CommonFunctions(this);
		System.out.println("insisde inspection depot");
			
		rememberpwd = (CheckBox) this.findViewById(R.id.chkrememberpwd);
		this.password = (EditText) this.findViewById(R.id.epwd);
		watcher = new TextWatcher() {

			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
				textchangefunction();				
			}
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
				textchangefunction();
			}

			public void afterTextChanged(Editable e) {
				// TODO Auto-generated method stub
					textchangefunction();
			};
		};
		
		this.login = (Button) this.findViewById(R.id.login);
		this.username = (AutoCompleteTextView) this.findViewById(R.id.eusername);			
		username.addTextChangedListener(watcher);
		
		this.close = (Button) this.findViewById(R.id.cancellogin);
		tblshow = (TableLayout) findViewById(R.id.tableLayout3);
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());

		/** create inspector table **/
		cf.Createtablefunction(1);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String value = extras.get("back").toString();
			if (value.equals("exit")) {
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(startMain);
			}
		}
		
		try {

			Cursor cur1 = cf.SelectTablefunction(cf.inspectorlogin,
					"where Ins_Flag1='1'");
			int rws1 = cur1.getCount();
			if (rws1 == 1) {
				Intent iInspectionList = new Intent(InspectionDepot.this,
						HomeScreen.class);
				startActivity(iInspectionList);

			} else if (rws1 > 1) {
				/** More than 0ne user logged in, so we loggoed out the users **/
				cf.db.execSQL("update " + cf.inspectorlogin
						+ " set Ins_Flag1=0 where Ins_Flag1=1");
				if(cf.application_sta)
				{
					Intent loginpage = new Intent(Intent.ACTION_MAIN);
					  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
					  startActivity(loginpage);
				}
			}
			else if(rws1==0 && cf.application_sta)
	        {
	        	  Intent loginpage = new Intent(Intent.ACTION_MAIN);
				  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
				  startActivity(loginpage);
	        }
		} catch (Exception e) {
			Log.i(TAG, "error=" + e.getMessage());

		}
		try {
			Cursor cur = cf.db.rawQuery("select * from " + cf.inspectorlogin, null);
			cur.moveToFirst();
			int result = cur.getColumnIndex("Ins_ImageName");
			if (result == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_ImageName VARCHAR");
				
			}
		} catch (Exception e) {
			System.out.println("e" + e.getMessage());
		}
		
		try {
			Cursor cur = cf.db.rawQuery("select * from " + cf.inspectorlogin, null);
			cur.moveToFirst();
			int result = cur.getColumnIndex("Ins_rememberpwd");
			if (result == -1) {
				cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_rememberpwd VARCHAR");
			}
		} catch (Exception e) {
			System.out.println("e" + e.getMessage());
		}
		
		
		
		this.close.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		rememberpwd.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					
					rempwd="1";
					Cursor cur = cf.db.rawQuery("select * from " + cf.inspectorlogin
							+ " where Ins_UserName='" + username.getText().toString().toLowerCase() + "'", null);
					if(cur.getCount()>0)
					{
						cur.moveToFirst();
							cf.db.execSQL("UPDATE " + cf.inspectorlogin
									+ " SET Ins_rememberpwd='1'"
									+ " WHERE Ins_UserName ='" + username.getText().toString().toLowerCase() + "'");
							
					}
					
				} else {
					rempwd="0";
					
					Cursor cur = cf.db.rawQuery("select * from " + cf.inspectorlogin
							+ " where Ins_UserName='" + username.getText().toString().toLowerCase() + "'", null);
					if(cur.getCount()>0)
					{
						cur.moveToFirst();
							cf.db.execSQL("UPDATE " + cf.inspectorlogin
									+ " SET Ins_rememberpwd='0'"
									+ " WHERE Ins_UserName ='" + username.getText().toString().toLowerCase() + "'");
							
						
					}
				}
			}
		});
		this.login.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				
				
				progressDialog = ProgressDialog.show(InspectionDepot.this, "",
						"Processing...");
				new Thread() {
					public void run() {
						try {
							
							
							if (!"".equals(username.getText().toString())
									|| !"".equals(password.getText().toString())) {
								if (!"".equals(username.getText().toString())) {
									if (!"".equals(password.getText()
											.toString())) {
										String achk;
										String expression = "[^\'\"<>]*";
										CharSequence inputStr = username
												.getText().toString();
										Pattern pattern = Pattern
												.compile(expression);
										Matcher matcher = pattern
												.matcher(inputStr);
										if (matcher.matches()) {

											logincheck = 0;

											Cursor cur = cf
													.SelectTablefunction(
															cf.inspectorlogin,
															" where Ins_UserName='"
																	+ username
																			.getText()
																			.toString()
																			.toLowerCase()
																	+ "'");

											int rws = cur.getCount();
											int Column1 = cur
													.getColumnIndex("Ins_UserName");
											int Column2 = cur
													.getColumnIndex("Ins_Password");
											int Column3 = cur
													.getColumnIndex("Ins_Id");

											cur.moveToFirst();

											if (cur.moveToFirst() == true) {
												dbuname = cf.getsinglequotes(cur
														.getString(Column1));
												dbpwd = cf.getsinglequotes(cur
														.getString(Column2));
												dbinsid = cf.getsinglequotes(cur
														.getString(Column3));
												if (username.getText()
														.toString()
														.toLowerCase()
														.equals(dbuname)
														&& password.getText()
																.toString()
																.equals(dbpwd)) {
													cf.db.execSQL("UPDATE "
															+ cf.inspectorlogin
															+ " SET Ins_Flag1=1"
															+ " WHERE Ins_Id ='"
															+ dbinsid + "'");
													
													logincheck = 1;
													
												} else {
													logincheck = 2;
												}
											} else {
												logincheck = 0;
											}
											if (logincheck == 0) {
												if (isInternetOn() == true) {
													SoapObject request = new SoapObject(
															cf.NAMESPACE,
															cf.METHOD_NAME);
													SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
															SoapEnvelope.VER11);
													envelope.dotNet = true;
													request.addProperty(
															"UserName",
															username.getText()
																	.toString());
													request.addProperty(
															"Password",
															password.getText()
																	.toString());
													System.out.println("request "+request);
													
													envelope.setOutputSoapObject(request);
													HttpTransportSE androidHttpTransport = new HttpTransportSE(
															cf.URL);
													try {
															androidHttpTransport
																.call(cf.SOAP_ACTION,
																		envelope);
														
														SoapObject result = (SoapObject) envelope
																.getResponse();
														System.out.println("result "+result);
														check(result);
													} catch (Exception e) {
														
														throw e;
													}

												} else {
													usercheck = 3;

												}
											} else if (logincheck == 1) {
												System.out.println("logincheck"+logincheck);
												logincheck = 0;
												getInsemail();
													Intent iInspectionList = new Intent(
															InspectionDepot.this,
															HomeScreen.class);
													startActivity(iInspectionList);
												
												
											} else {
												usercheck = 2;
											}
										} else {
											usercheck = 6;
										}

									} else {
										usercheck = 5;
									}
								} else {
									usercheck = 4;
								}

							} else {
								usercheck = 1;
							}

						} catch (SocketTimeoutException s) {
							usercheck = 8;// handler.sendEmptyMessage(0);
						} catch (NetworkErrorException n) {
							usercheck = 8;// handler.sendEmptyMessage(0);
						} catch (IOException io) {
							usercheck = 8;// handler.sendEmptyMessage(0);
						} catch (XmlPullParserException x) {
							usercheck = 8;// handler.sendEmptyMessage(0);
						} catch (Exception e) {
							usercheck = 8;
							// handler.sendEmptyMessage(0);
						}

						handler.sendEmptyMessage(0);
						progressDialog.dismiss();

					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							if (usercheck == 1) {
								usercheck = 0;
								cf.ShowToast("Please enter the UserName and Password.",1);
								username.requestFocus();
							}
							if (usercheck == 2) {
								usercheck = 0;
								cf.ShowToast("Invalid UserName or Password.",1);

								username.setText("");
								password.setText("");
							}
							if (usercheck == 3) {
								usercheck = 0;
								cf.ShowToast("Internet connection is not available.",1);

							}
							if (usercheck == 4) {
								usercheck = 0;
								cf.ShowToast("Please enter UserName.",1);
								username.requestFocus();
							}
							if (usercheck == 5) {
								usercheck = 0;
								cf.ShowToast("Please enter Password.",1);
								password.requestFocus();
							}
							if (usercheck == 7) {
								usercheck = 0;
								usercheck = 7;
								cf.ShowToast("You are not eligible to login.",1);
								username.setText("");
								password.setText("");

								tblshow.setVisibility(v1.VISIBLE);
							}
							if (usercheck == 8) {
								cf.ShowToast("There is a problem on your Network. Please try again later with better Network..",1);

							}
						}
					};
				}.start();

			}
		});
		if(!cf.application_sta)
		{
			AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(InspectionDepot.this);
			alertDialog1
		.setMessage("In order to use the latest version of B1 1802 application, You must update IDMA application. Please click below to update IDMA application.");
		
      	alertDialog1.setPositiveButton("Download",
		new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
				
				String source1 = "<font color=#FFFFFF>Downloading IDMA application ... Please wait...</font>";
				 pd = ProgressDialog.show(InspectionDepot.this, "", Html.fromHtml(source1),true);
			        new Thread(new Runnable() {
			                public void run() {
			                	Looper.prepare();
			                	System.out.println("rrrun");
			                	if(cf.isInternetOn())
			                	{
			                	downloadingapp();
			                	}else
			                	{
			                		k=25;
			                	}
			                	
			                	handler2.sendEmptyMessage(0);
			                }
			            }).start();


			}
		});
      
      	alertDialog1.setCancelable(false);
      	alertDialog1.show();
		}
		
		
		
	}

	protected void textchangefunction() {
		// TODO Auto-generated method stub
		try {
			Cursor cur1 = cf.db.rawQuery("select * from "+ cf.inspectorlogin + " where Ins_UserName like '" + username.getText().toString() + "%'",null);
			autousername = new String[cur1.getCount()];
			cur1.moveToFirst();
			if(cur1.getCount()!=0)
			{
				if (cur1 != null) {
					int i = 0;
					do {
						autousername[i] = cf.getsinglequotes(cur1.getString(cur1.getColumnIndex("Ins_UserName")));
						if (autousername[i].contains("null")) {
							autousername[i] = autousername[i].replace("null", "");
						}
						 
						i++;
					} while (cur1.moveToNext());
				}
				cur1.close();
			}
			 ArrayAdapter<String> adapter = new ArrayAdapter<String>(InspectionDepot.this,R.layout.loginnamelist,autousername);
			 username.setThreshold(1);
			 username.setAdapter(adapter);
			
			Cursor cur = cf.db.rawQuery("select * from " + cf.inspectorlogin
					+ " where Ins_UserName='" + username.getText().toString().toLowerCase() + "'", null);
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				if(cur.getString(cur.getColumnIndex("Ins_rememberpwd")).equals("1"))
				{
					password.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("Ins_Password"))));
					rememberpwd.setChecked(true);
				
				}
				else
				{
					password.setText("");
					rememberpwd.setChecked(false);
				}
					
			}
				
		}
		catch(Exception e1)
		{
			System.out.println(" Exception "+e1.getMessage());
		}
	}

	private void LoginInsert(SoapObject obj) {

		ContentValues tblInspectionData = new ContentValues();
		String ins_id, headshot,ins_photo,ins_firstname, ins_middlename, ins_lastname, ins_address, ins_photoextn,ins_companyname, ins_companyId, ins_username, ins_password, ins_email,ins_flag1, ins_flag2;
		ins_id = String.valueOf(obj.getProperty("Inspectorid"));
		ins_firstname = String.valueOf(obj.getProperty("Inspectorfirstname"));
		ins_middlename = String.valueOf(obj.getProperty("Inspectormiddlename"));
		ins_lastname = String.valueOf(obj.getProperty("Inspectorlastname"));
		ins_address = String.valueOf(obj.getProperty("Inspectoraddress"));
		ins_companyname = String.valueOf(obj.getProperty("Inspectorcompanyname"));
		ins_companyId = String.valueOf(obj.getProperty("InspectorcompanyId"));
		ins_username = String.valueOf(obj.getProperty("Inspectorusername"));
		ins_password = String.valueOf(obj.getProperty("Inspectorpassword"));
		ins_photo = String.valueOf(obj.getProperty("InspectorPhoto"));
		ins_photoextn = String.valueOf(obj.getProperty("InspectorPhotoExt"));
		ins_email = String.valueOf(obj.getProperty("InspectorEmail"));
		headshot = String.valueOf(obj.getProperty("Headshot"));System.out.println("photo="+headshot);
		System.out.println("ins_photo "+ins_photo);
		
		 	
		try
		{
			URL ulrn = new URL(headshot);
		    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
		    InputStream is = con.getInputStream();
		    Bitmap bmp = BitmapFactory.decodeStream(is);
		   
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
		    byte[] b = baos.toByteArray();
		    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
		    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

		    String FILENAME = ins_id +ins_photoextn;System.out.println("FILENAME "+FILENAME);
			FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
			fos.write(decode);
			fos.close();	
		    
		}
		catch (Exception e1){
		
			System.out.println("Imagge headshot disp = "+e1.getMessage());
		}
		
		ins_flag1 = "0";
		ins_flag2 = "0";
		
		try {

			cf.db.execSQL("INSERT INTO "
					+ cf.inspectorlogin
					+ " (Ins_Id,Ins_FirstName,Ins_MiddleName,Ins_LastName,Ins_Address,Ins_CompanyName,Ins_CompanyId,Ins_UserName,Ins_Password,Ins_Flag1,Ins_Flag2,Android_status,Ins_ImageName,Ins_rememberpwd,Ins_Email,Ins_PhotoExtn)"
					+ " VALUES ('" + ins_id + "','"
					+ cf.convertsinglequotes(ins_firstname) + "','"
					+ cf.convertsinglequotes(ins_middlename) + "','"
					+ cf.convertsinglequotes(ins_lastname) + "','"
					+ cf.convertsinglequotes(ins_address) + "','"
					+ cf.convertsinglequotes(ins_companyname) + "','"
					+ cf.convertsinglequotes(ins_companyId) + "','"
					+ cf.convertsinglequotes(ins_username.toLowerCase())
					+ "','" + cf.convertsinglequotes(ins_password) + "','"
					+ ins_flag1 + "','" + ins_flag2 + "','" + status + "','','"+rempwd+"','"+cf.convertsinglequotes(ins_email)+"','"+cf.convertsinglequotes(ins_photoextn)+"')");
			
		
			
		} catch (Exception e) {
			Log.i(TAG, "categorytableerror=" + e.getMessage());
		}

	}
	public void check(SoapObject login) throws NetworkErrorException,
			SocketTimeoutException, IOException, XmlPullParserException,
			Exception {
		SoapObject obj = (SoapObject) login.getProperty(0);
		String user = String.valueOf(obj.getProperty("userAuthentication"));
		status = String.valueOf(obj.getProperty("AndroidStatus"));
		System.out.println("stste "+status + " user "+user);
		if (user.equals("false")) {
			usercheck = 2;
		}
		if (user.equals("true")) {
			if (status.equals("true")) {
				final String id = String.valueOf(obj.getProperty("userId"));

				String deviceId = Settings.System.getString(
						getContentResolver(), Settings.System.ANDROID_ID);
				String model = android.os.Build.MODEL;
				String manuf = android.os.Build.MANUFACTURER;
				String devversion = android.os.Build.VERSION.RELEASE;
				String apiLevel = android.os.Build.VERSION.SDK;
				WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
				WifiInfo wifiInfo = wifiManager.getConnectionInfo();
				int ipAddress = wifiInfo.getIpAddress();
				SoapSerializationEnvelope envelope2 = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope2.dotNet = true;
				SoapObject ad_property2 = new SoapObject(cf.NAMESPACE,
						cf.METHOD_NAME2);
				ad_property2.addProperty("Deviceid", deviceId);
				ad_property2.addProperty("ModelNumber", model);
				ad_property2.addProperty("Manufacturer", manuf);
				ad_property2.addProperty("OSVersion", devversion);
				ad_property2.addProperty("APILevel", apiLevel);
				ad_property2.addProperty("IPAddress", ipAddress);
				ad_property2.addProperty("InspectorLd", id.toString());
				ad_property2.addProperty("Date", cd);

				envelope2.setOutputSoapObject(ad_property2);
				HttpTransportSE androidHttpTransport11 = new HttpTransportSE(
						cf.URL);
				androidHttpTransport11.call(cf.SOAP_ACTION2, envelope2);
				String result11 = String.valueOf(envelope2.getResponse());

				SoapObject request = new SoapObject(cf.NAMESPACE,
						cf.METHOD_NAME1);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("inspectorid", id.toString());
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						cf.URL);
				androidHttpTransport.call(cf.SOAP_ACTION1, envelope);
				SoapObject result = (SoapObject) envelope.getResponse();
				System.out.println("result "+result);
				Cursor cur = cf.SelectTablefunction(cf.inspectorlogin,
						" where Ins_Id = '" + id.toString()
								+ "' and Ins_Flag1='1'");
				int rws = cur.getCount();
				System.out.println("rws "+rws+ "ID "+id);
				if (rws == 0) {
					
					LoginInsert(result);
					String flag1 = "1";
					cf.db.execSQL("UPDATE " + cf.inspectorlogin
							+ " SET Ins_Flag1='" + flag1 + "'"
							+ " WHERE Ins_Id ='" + id.toString() + "'");

				} else {
				}
				getInsemail();
				System.out.println("getInsemail");
					Intent iInspectionList = new Intent(
							InspectionDepot.this,
							HomeScreen.class);
					startActivity(iInspectionList);
				
			} else {
				usercheck = 7;
			}

		}

	}
public void getInsemail()
{
	try {
		Cursor cur1 = cf.db.rawQuery("select * from " + cf.inspectorlogin, null);
		cur1.moveToFirst();
		int result1 = cur1.getColumnIndex("Ins_Email");
		int result2 = cur1.getColumnIndex("Ins_PhotoExtn");
		if (result1 == -1) {
			cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_Email VARCHAR DEFAULT ''");
			getvalueforemailphotoextn();
			String ins_email = String.valueOf(resultforemailphotoextn.getProperty("InspectorEmail"));
				cf.db.execSQL("UPDATE " + cf.inspectorlogin
						+ " SET Ins_Email='" + ins_email + "'"
						+ " WHERE Ins_Id ='" + cf.InspectorId + "'");
		}	
		if (result2 == -1) {
			cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_PhotoExtn VARCHAR DEFAULT ''");
			getvalueforemailphotoextn();
			String ins_photoname = String.valueOf(resultforemailphotoextn.getProperty("InspectorPhoto"));
			String ins_photo = String.valueOf(resultforemailphotoextn.getProperty("InspectorPhotoExt"));
			cf.db.execSQL("UPDATE " + cf.inspectorlogin
					+ " SET Ins_PhotoExtn='" + ins_photo + "'"
					+ " WHERE Ins_Id ='" + cf.InspectorId + "'");
			
			byte[] decode = Base64.decode(ins_photoname.toString(), 0);
			  
			try
			{
				String FILENAME = cf.InspectorId +ins_photo;System.out.println("FILENAME "+FILENAME);
				FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
				fos.write(decode);
				fos.close();	
			}
			catch (IOException e){
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SinkholeInspectionActivity.this+" "+" in the stage of retrieving image bytes of Inspector photo from its webservice at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
			
			
		}
		
		
	} catch (Exception e) {
		System.out.println("esdfdsdsdsfds" + e.getMessage());
	}
}
private void getvalueforemailphotoextn()
{
	try {
		cf.getInspectorId();

		System.out.println("table altered result"+cf.InspectorId);
	SoapObject request1 = new SoapObject(cf.NAMESPACE,cf.METHOD_NAME1);
	SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope1.dotNet = true;
	request1.addProperty("inspectorid", cf.InspectorId);
	envelope1.setOutputSoapObject(request1);
	HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL);
	androidHttpTransport1.call(cf.SOAP_ACTION1, envelope1);
	resultforemailphotoextn = (SoapObject) envelope1.getResponse();
	System.out.println("resultforemail "+resultforemailphotoextn);
	} catch (Exception e) {
		System.out.println("Exception "+e.getMessage());
	}
	
}



	public final boolean isInternetOn() {
		boolean chk = false;
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			chk = true;
		} else {
			chk = false;
		}

		return chk;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			tblshow.setVisibility(v1.GONE);
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
	private void downloadingapp()
	{
		
		SoapObject webresult;			
		SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;	
		System.out.println("Inp "+cf.InspectorId);
		//request.addProperty("InspectorID",cf.InspectorId);
		envelope.setOutputSoapObject(request);	
		System.out.println("request downlod "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

		try {
			
			androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);
			System.out.println("RESPONSE "+envelope.getResponse());
			Object response = envelope.getResponse();
			
			byte[] b;
			if (cf.checkresponce(response)) // check the response is valid or
											// not
			{
				
				b = Base64.decode(response.toString(),Base64.DEFAULT);

				try {
					String PATH = Environment.getExternalStorageDirectory()
							+ "/Download/IDMA";
					File file = new File(PATH);
					file.mkdirs();

					File outputFile = new File(PATH + "/IDMA.apk");
					FileOutputStream fileOuputStream = new FileOutputStream(outputFile);
					fileOuputStream.write(b);
					fileOuputStream.close();
					cf.fn_logout(cf.InspectorId);  //FOR LOGOUT
					k=11;
				} catch (IOException e) {
					cf.ShowToast("Update error!",1);
				}
			}
			else {
				k=22;
				
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			k=24;
		} catch (XmlPullParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			k=24;
		}

	}



	private Handler handler2 = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			if(k==11)
			{
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(new File(Environment
						.getExternalStorageDirectory()
						+ "/Download/IDMA/"
						+ "IDMA.apk")),
						"application/vnd.android.package-archive");
				startActivityForResult(intent,12);
			}
			else if(k==22)
			{
				cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
				startActivity(new Intent(InspectionDepot.this,InspectionDepot.class));
			}
			else if(k==25)
			{
				cf.ShowToast("Please enable internet connection and try again.",1);
				startActivity(new Intent(InspectionDepot.this,InspectionDepot.class));
			}else if(k==24)
			{
				cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
				startActivity(new Intent(InspectionDepot.this,InspectionDepot.class));
			}
			
		}
	};
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		System.out.println("stop called");
		//finish();
		super.onStop();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==12 && resultCode==RESULT_CANCELED)
		{
			startActivity(new Intent(this,InspectionDepot.class));
			
		}
	//	System.out.println("comes in the on activity result"+resultCode+requestCode);
	} 
}