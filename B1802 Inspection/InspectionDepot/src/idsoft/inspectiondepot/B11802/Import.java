package idsoft.inspectiondepot.B11802;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Enumeration;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Import extends Activity {

	String apk, newcode, newversion, error = "", dbhomeid,anytype = "anyType{}", homeid;
	static int usercheck = 0;
	private Button importnow, back;
	private volatile boolean stopRequested;
	private static final String TAG = null;
	final static int RUNNING = 1;
	int vcode,mState,count = 0;
	CommonFunctions cf;
	private AlertDialog alertDialog;
	ProgressDialog progDialog;
	int typeBar;
	int total; // Determines type progress bar: 0 = spinner, 1 = horizontal
	int delay = 40; // Milliseconds of delay in the update loop
	int maxBarValue = 0; // Maximum value of horizontal progress bar
	public ProgressThread progThread;
	Thread t;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.importdata);
		cf = new CommonFunctions(this);
		cf.getInspectorId();

		progDialog = new ProgressDialog(this);
		vcode = getcurrentversioncode();
		typeBar = 1;
		
		cf.releasecode = (TextView) this.findViewById(R.id.releasecode);
		cf.setRcvalue(cf.releasecode);
		
		
		importnow = (Button) this.findViewById(R.id.importnow);
		back = (Button) this.findViewById(R.id.back);
		 
		back.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				startActivity(new Intent(Import.this, HomeScreen.class));
			}
		});
		
		importnow.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				 if(cf.isInternetOn()==true) {
					total = 0;
					showDialog(1);
					
				t =	new Thread() {
						public void run() {
							try {
								if (getversioncodefromweb() == true) {
									total = 15;
									//SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME4);
									SoapObject request = new SoapObject(cf.NAMESPACE, "UpdateMobileDB_ScheAssing");
									SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
									envelope.dotNet = true;
									request.addProperty("inspectorid",cf.InspectorId.toString());
									envelope.setOutputSoapObject(request);
									HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
									SoapObject result = null;
									
									try

									{
									androidHttpTransport.call("http://tempuri.org/UpdateMobileDB_ScheAssing", envelope);
										result = (SoapObject) envelope.getResponse();
										
									} catch (Exception e) {
										throw e;

									}
                                  
									if (result.equals(anytype)) {
										cf.ShowToast("Server is busy. Please try again later",1);
										cf.gohome();
									} else {
										System.out.println("result "+result);
										InsertDate(result);
										total = 30;
										AgentInfo();
										total = 40;
										AddComments();
										total = 60;
										LoadQuestions();
										total = 80;		
										AdditionalService();
										total = 85;
										InsertCloudDate();
										total = 95;
										
										usercheck = 0;
									}

								} else {
									total = 100;
								}
							} catch (SocketTimeoutException s) {
								usercheck = 4;
								total = 100;
							} catch (NetworkErrorException n) {

								usercheck = 3;
								total = 100;
							} catch (IOException io) {
								usercheck = 3;
								total = 100;
							} catch (XmlPullParserException x) {
								usercheck = 3;
								total = 100;
							}

							catch (Exception e) {
								total = 100;
							}
							try {
								progThread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							total = total + 5;
						}
					};
					t.start();
				} else {
					usercheck = 2;
					handler.sendEmptyMessage(0);
				}
			}
		});
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 1:
			progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progDialog.setMax(maxBarValue);
			progDialog.setMessage("Importing please wait:");
			progDialog.setCancelable(false);
			progThread = new ProgressThread(handler1);
			progThread.start();
			return progDialog;
		default:
			return null;
		}
	}

	final Handler handler1 = new Handler() {

		public void handleMessage(Message msg) {
			// Get the current value of the variable total from the message data
			// and update the progress bar.
			try
			{
				int total = msg.getData().getInt("total");	
				progDialog.setProgress(total);	
				if (total == 100) 
				{
					progDialog.setCancelable(true);	
					dismissDialog(typeBar);					
					handler.sendEmptyMessage(0);					
					progThread.setState(ProgressThread.DONE);
				}
			}catch (Exception e) 
			{
				// TODO: handle exception
				System.out.println("fixed the isseus first");
				handler.sendEmptyMessage(0);
				progThread.setState(ProgressThread.DONE);
			}
		}
	};
	final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
try{
			if (usercheck == 1) {
				usercheck = 7;
				cf.ShowToast("Invalid UserName and Password.",1);
				
			} else if (usercheck == 2) {
				usercheck = 7;
				cf.ShowToast("Internet connection not Available.",1);
				startActivity(new Intent(Import.this,Import.class));
			} else if (usercheck == 3) {
				usercheck = 7;
				cf.ShowToast("There is a problem on your network so pleases try again later with better network ",1);
				startActivity(new Intent(Import.this,Import.class));
			} else if (usercheck == 4) {
				usercheck = 7;
				cf.ShowToast("There is a problem with Connecting server please try again later ",1);
				startActivity(new Intent(Import.this,Import.class));
			} else if (usercheck == 0) {
				usercheck = 7;
				 cf.alerttitle="Import";
				 cf.alertcontent="Import successfully completed";
				 cf.showalert(cf.alerttitle,cf.alertcontent);
				
			

			}
}catch (Exception e) {
	// TODO: handle exception
	System.out.println("fixed the isseus ");
}
		}

	};

	private class ProgressThread extends Thread {

		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;

		// Constructor with an argument that specifies Handler on main thread
		// to which messages will be sent by this thread.

		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			mState = RUNNING;
			while (mState == RUNNING) {
				try {
					// Control speed of update (but precision of delay not
					// guaranteed)
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					Log.e("ERROR", "Thread was Interrupted");
				}

				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("total", total);
				msg.setData(b);
				mHandler.sendMessage(msg);

			}
		}

		public void setState(int state) {
			mState = state;
		}

	}
	
	
	public boolean getversioncodefromweb() throws NetworkErrorException,
			SocketTimeoutException, XmlPullParserException, IOException {
		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);

		androidHttpTransport.call(cf.SOAP_ACTION3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		SoapObject obj = (SoapObject) result.getProperty(0);
		newcode = String.valueOf(obj.getProperty("VersionCode"));
		newversion = String.valueOf(obj.getProperty("VersionName"));
		System.out.println("comes correct after the first ");
		if (!"null".equals(newcode) && null != newcode && !newcode.equals("")) {
			if (Integer.toString(vcode).equals(newcode)) {
				//return true;
			} else {
				try {
					cf.Createtablefunction(16);
					Cursor c12 = cf.SelectTablefunction(cf.VersionshowTable,
							" ");

					if (c12.getCount() < 1) {
						try {
							cf.db.execSQL("INSERT INTO "
									+ cf.VersionshowTable
									+ "(VId,VersionCode,VersionName,ModifiedDate)"
									+ " VALUES ('1','"+newcode+"','" + newversion
									+ "',date('now'))");

						} catch (Exception e) {
							Log.i(TAG, "categorytableerror=" + e.getMessage());
						}

					} else {
						try {
							cf.db

							.execSQL("UPDATE " + cf.VersionshowTable
									+ " set VersionCode='"+newcode+"',VersionName='"
									+ newversion
									+ "',ModifiedDate =date('now')");
						} catch (Exception e) {
							Log.i(TAG, "categorytableerror=" + e.getMessage());
						}

					}
					
				} catch (Exception e) {
					

				}

			}
		} else {
			
		}
		System.out.println("no more issues thats ok ");
		
		 SoapObject request1 = new SoapObject(cf.NAMESPACE, "GetVersionInformation_IDMS");
			SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope1.dotNet = true;
			envelope1.setOutputSoapObject(request1);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_IDMA);
	    	androidHttpTransport1.call(cf.NAMESPACE+"GetVersionInformation_IDMS", envelope1);
	    	
	    	SoapObject result1 = (SoapObject) envelope1.getResponse();
			
			SoapObject obj1 = (SoapObject) result1.getProperty(0);
			cf.newcode = String.valueOf(obj1.getProperty("VersionCode"));
			cf.newversion = String.valueOf(obj1.getProperty("VersionName"));
			System.out.println(" version name and version code  "+cf.newcode+"  "+cf.newversion);
			if (!"null".equals(cf.newcode) && null != cf.newcode && !cf.newcode.equals("")) {
					try {     
						cf.Createtablefunction(20);
						Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," Where ID_VersionType='IDMA_New' ");

						if (c12.getCount() < 1) {
							
								cf.db.execSQL("INSERT INTO "
										+ cf.IDMAVersion
										+ "(VId,ID_VersionCode,ID_VersionName,ID_VersionType)"
										+ " VALUES ('2','"+cf.newcode+"','" + cf.newversion
										+ "','IDMA_New')");

							

						} else {
								cf.db.execSQL("UPDATE " + cf.IDMAVersion
										+ " set ID_VersionCode='"+cf.newcode+"',ID_VersionName='"
										+ cf.newversion
										+ "' Where  ID_VersionType='IDMA_New'");
							
						}
				
					} catch (Exception e) {
				
						System.out.println("new data  error  "+e.getMessage());
					}

				}  
			return true;
	}

	private int getcurrentversioncode() {
		// TODO Auto-generated method stub

		try {
			vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vcode;
	}

	private void LoadQuestions() throws NetworkErrorException, IOException,
			XmlPullParserException, SocketTimeoutException {
		String bc_original, rc_original, rd_original, rw_original, rg_original, swr_original, op_original, wc_original;

		cf.Createtablefunction(6);
		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME9);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID", cf.InspectorId);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);

		androidHttpTransport.call(cf.SOAP_ACTION9, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		if (cf.checkresponce(result) == false) {
			total = 80;
			return;
		}

		else {
			double Totaltmp = 0.0;
			int countchk = result.getPropertyCount();
			Totaltmp = (25.00 / (double) countchk);
			double temptot = Totaltmp;
			int temptotal = total;
			for (int i = 0; i < result.getPropertyCount(); i++) {
				SoapObject result1 = (SoapObject) result.getProperty(i);

				try {
					homeid = String.valueOf(result1.getProperty("SRID"));
					bc_original = String.valueOf(result1
							.getProperty("BuildCodeOriginal"));
					rc_original = String.valueOf(result1
							.getProperty("RoofCoverOriginal"));
					rd_original = String.valueOf(result1
							.getProperty("RoofDeckOriginal"));
					rw_original = String.valueOf(result1
							.getProperty("RoofWallOriginalValue"));
					rg_original = String.valueOf(result1
							.getProperty("RoofGeoOriginalValue"));
					swr_original = String.valueOf(result1
							.getProperty("SWROriginalValue"));
					op_original = String.valueOf(result1
							.getProperty("OpenProtectOriginalValue"));
					wc_original = String.valueOf(result1
							.getProperty("WallConstrOriginal"));

					Cursor c2 = cf.SelectTablefunction(
							cf.quetsionsoriginaldata, " where homeid='"
									+ homeid + "'");
					int rws1 = c2.getCount();
					c2.moveToFirst();

					if (rws1 == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.quetsionsoriginaldata
								+ " (homeid,bcoriginal,rcoriginal,rdoriginal,rworiginal,rgoriginal,swroriginal,oporiginal,wcoriginal)"
								+ " VALUES ('" + homeid + "','"
								+ cf.convertsinglequotes(bc_original) + "','"
								+ cf.convertsinglequotes(rc_original) + "','"
								+ cf.convertsinglequotes(rd_original) + "','"
								+ cf.convertsinglequotes(rw_original) + "','"
								+ cf.convertsinglequotes(rg_original) + "','"
								+ cf.convertsinglequotes(swr_original) + "','"
								+ cf.convertsinglequotes(op_original) + "','"
								+ cf.convertsinglequotes(wc_original) + "')");

					} else {

					}
				} catch (Exception e)

				{
				}
				if (total <= 86) {

					total = temptotal + (int) (Totaltmp);
				} else {
					total = 85;
				}
				Totaltmp += temptot;

			}
		}
		
	}

	private void AddComments() throws NetworkErrorException, IOException,
			XmlPullParserException, SocketTimeoutException {
		// TODO Auto-generated method stub
		cf.Createtablefunction(5);

		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME7);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.SOAP_ACTION7, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		if (cf.checkresponce(result) == false) {
			total = 65;
			return;
		} else {
			check(result);
		}

	}

	public void check(SoapObject SoapObject) {
		String instypeid, quesID, optionID, Comments;
		int Cnt = SoapObject.getPropertyCount();
		double Totaltmp = 0.0;
		int countchk = Cnt;
		Totaltmp = (20.00 / (double) countchk);
		double temptot = Totaltmp;
		int temptotal = total;
		for (int i = 0; i < Cnt; i++) {
			count++;

			try {
				SoapObject obj = (SoapObject) SoapObject.getProperty(i);
				instypeid = String.valueOf(obj.getProperty("InspectionType"));
				quesID = String.valueOf(obj.getProperty("questId"));
				optionID = String.valueOf(obj.getProperty("OptionId"));
				Comments = String.valueOf(obj.getProperty("Command"));
				Cursor c2 = cf.SelectTablefunction(cf.tbl_admcomments,
						" where questionid='" + quesID + "' and optionid='"
								+ optionID + "' and InspectorId='" + instypeid
								+ "' and description='" + cf.convertsinglequotes(Comments) + "'");
				int rws = c2.getCount();
				c2.moveToFirst();
				
				if (rws == 0) {
					if (quesID.equals("8")) {

					} else {
						cf.db.execSQL("INSERT INTO "
								+ cf.tbl_admcomments
								+ " (InspectorId,questionid,optionid,optionvalue,description,status)"
								+ " VALUES ('" + instypeid + "','" + quesID
								+ "','" + optionID + "','','"
								+ cf.convertsinglequotes(Comments)
								+ "','Active')");
					}
				} else {

				}

				c2.close();
			} catch (Exception e) {
				System.out.println("cException " + e.getMessage());

			}
			if (total <= 61) {

				total = temptotal + (int) (Totaltmp);
			} else {
				total = 60;
			}
			Totaltmp += temptot;

		}
		
	}

	private void InsertDate(SoapObject objInsert) throws NetworkErrorException,
			IOException, XmlPullParserException, SocketTimeoutException {
		String substatus = "0",buildingsize="", email, cperson, IsInspected = "0", insurancecompanyname, IsUploaded = "0", homeid = "", firstname, 
				lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, status, 
				companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname, scheduleddate, yearbuilt, nstories, inspectionstarttime, 
				inspectionendtime, inspectioncomment, inspectiontypeid,TPQA="",Additionalservice="",QAInspector="",licenceexpirydate="";
		cf.Createtablefunction(2);
		int Cnt = objInsert.getPropertyCount();
		double Totaltmp = 0.0;
		int countchk = Cnt;
		Totaltmp = (15.00 / (double) countchk);
		double temptot = Totaltmp;
		int temptotal = total;

		for (int i = 0; i < Cnt; i++) {

			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
					homeid = String.valueOf(obj.getProperty("SRID"));
					firstname = String.valueOf(obj.getProperty("FirstName"));
					lastname = String.valueOf(obj.getProperty("LastName"));
					middlename = String.valueOf(obj.getProperty("AssignedDate"));
					addr1 = String.valueOf(obj.getProperty("Address1"));
					addr2 = String.valueOf(obj.getProperty("Address2"));
					city = String.valueOf(obj.getProperty("City"));
					state = String.valueOf(obj.getProperty("State"));
					country = String.valueOf(obj.getProperty("Country"));
					zip = String.valueOf(obj.getProperty("Zip"));
					homephone = String.valueOf(obj.getProperty("HomePhone"));
					cellphone = String.valueOf(obj.getProperty("CellPhone"));
					workphone = String.valueOf(obj.getProperty("WorkPhone"));
					email = String.valueOf(obj.getProperty("Email"));
					cperson = String.valueOf(obj.getProperty("ContactPerson"));

					ownerpolicyno = String.valueOf(obj.getProperty("OwnerPolicyNo"));
					status = String.valueOf(obj.getProperty("Status"));
					companyid = String.valueOf(obj.getProperty("CompanyId"));
					inspectorid = String.valueOf(obj.getProperty("InspectorId"));
					wId = String.valueOf(obj.getProperty("wId"));
					cId = String.valueOf(obj.getProperty("CId"));
					inspectorfirstname = String.valueOf(obj.getProperty("InspectorFirstName"));
					inspectorlastname = String.valueOf(obj.getProperty("InspectorLastName"));
					scheduleddate = String.valueOf(obj.getProperty("ScheduledDate"));

					yearbuilt = String.valueOf(obj.getProperty("YearBuilt"));
					nstories = String.valueOf(obj.getProperty("Nstories"));
					inspectionstarttime = String.valueOf(obj.getProperty("InspectionStartTime"));
					inspectionendtime = String.valueOf(obj.getProperty("InspectionEndTime"));
					inspectioncomment = String.valueOf(obj.getProperty("InspectionComment"));
					inspectiontypeid = String.valueOf(obj.getProperty("InspectionTypeId"));
					insurancecompanyname = String.valueOf(obj.getProperty("InsuranceCompany"));
					buildingsize = String.valueOf(obj.getProperty("BuildingSize"));					
					substatus = String.valueOf(obj.getProperty("SubStatusID"));
					
					TPQA = String.valueOf(obj.getProperty("TPQA"));
					Additionalservice = String.valueOf(obj.getProperty("Additionalservice"));					
					QAInspector = String.valueOf(obj.getProperty("QAInspector"));
					licenceexpirydate = String.valueOf(obj.getProperty("ExpiryDate"));
					System.out.println("ownerpolicyno="+ownerpolicyno+"Status="+status+"status="+status+"Substtsua="+substatus);
					Cursor cur = cf.SelectTablefunction(cf.mbtblInspectionList, " where InspectorId='" + cf.InspectorId + "' and s_SRID='"+homeid+"'");
					int rws = cur.getCount();
					if (cur.getCount() >= 1) {
							cur.moveToFirst();
							//System.out.println("ISINS="+cur.getString(cur.getColumnIndex("IsInspected")));
								if(cur.getString(cur.getColumnIndex("IsInspected")).equals("0"))
								{
									cf.db.execSQL("UPDATE "
												+ cf.mbtblInspectionList
												+ " SET  s_OwnersNameFirst='"
												+ cf.convertsinglequotes(firstname)
												+ "',s_OwnersNameLast='"
												+ cf.convertsinglequotes(lastname)
												+ "',MiddleName='"
												+ middlename
												+ "',s_propertyAddress='"
												+ cf.convertsinglequotes(addr1)
												+ "',Address2='"
												+ cf.convertsinglequotes(addr2)
												+ "'"
												+ ",s_City='"
												+ cf.convertsinglequotes(city)
												+ "',s_State='"
												+ cf.convertsinglequotes(state)
												+ "',s_County='"
												+ cf.convertsinglequotes(country)
												+ "',s_ZipCode='"
												+ zip
												+ "',s_ContactHomePhone='"
												+ homephone
												+ "'"
												+ ",s_ContactCellPhone='"
												+ cellphone
												+ "',s_ContactWorkPhone='"
												+ workphone
												+ "',s_ContactPerson='"
												+ cf.convertsinglequotes(cperson)
												+ "',s_OwnerPolicyNumber='"
												+ cf.convertsinglequotes(ownerpolicyno)
												+ "',IsInspected='"
												+ IsInspected
												+ "',IsUploaded='"
												+ IsUploaded
												+ "'"
												+ ",Status='"
												+ status
												+ "',InsurcarrierId='"
												+ companyid
												+ "',InspectorId='"
												+ inspectorid.toString()
												+ "',wId='"
												+ wId
												+ "',CompanyId='"
												+ cId
												+ "',InspectorFirstName='"
												+ cf.convertsinglequotes(inspectorfirstname)
												+ "',InspectorLastName='"
												+ cf.convertsinglequotes(inspectorlastname)
												+ "',ScheduleDate='"
												+ scheduleddate
												+ "',s_YearBuilt='"
												+ yearbuilt
												+ "'"
												+ ",s_Nstories='"
												+ nstories
												+ "',InspectionStartTime='"
												+ inspectionstarttime
												+ "',InspectionEndTime='"
												+ inspectionendtime
												+ "',InspectionComment='"
												+ cf.convertsinglequotes(inspectioncomment)
												+ "',s_ContactEmail='"
												+ email
												+ "',InspectionTypeId='"
												+ inspectiontypeid
												+ "',InspectionDate='"
												+ scheduleddate
												+ "',InsuranceCompanyname='"
												+ cf.convertsinglequotes(insurancecompanyname)+ "',BuildingSize='"+buildingsize+"',SubStatus='"+substatus+"' where s_SRID='" + homeid
												+ "'");
								}
								else
								{
									
									if(cur.getString(cur.getColumnIndex("IsInspected")).equals("2"))
									{
										cf.db.execSQL("UPDATE " + cf.mbtblInspectionList + " SET Status='"+status+"',SubStatus='"+substatus+"' where s_SRID='" + homeid+ "'");
									}
								}
								
								cf.db.execSQL("UPDATE " + cf.mbtblInspectionList + " SET TPQA='"+cf.convertsinglequotes(TPQA)+"'," +
										"Additionalservice='"+Additionalservice+"',QAInspector='"+QAInspector+"',Licenceexpirydate='"+cf.convertsinglequotes(licenceexpirydate)+"' where s_SRID='" + homeid+ "'");
								
					}
					else
					{
							cf.db.execSQL("INSERT INTO "
									+ cf.mbtblInspectionList
									+ " (s_SRID,s_OwnersNameFirst,s_OwnersNameLast,MiddleName,s_propertyAddress,Address2,s_City,s_State,s_County,s_ZipCode,s_ContactHomePhone,s_ContactCellPhone,s_ContactWorkPhone,s_ContactPerson,s_OwnerPolicyNumber,IsInspected,IsUploaded,Status,InsurcarrierId,InspectorId,wId,CompanyId,InspectorFirstName,InspectorLastName,ScheduleDate,s_YearBuilt,s_Nstories,InspectionStartTime,InspectionEndTime,InspectionComment,InspectionTypeId,Insurancerenewaldate,InspectionDate,InsuranceCompanyname,s_ContactEmail,chkbx,SchFlag,SubStatus,BuildingSize,TPQA,Additionalservice,QAInspector,Licenceexpirydate)"
									+ " VALUES ('" + homeid + "','"
									+ cf.convertsinglequotes(firstname) + "','"
									+ cf.convertsinglequotes(lastname) + "','"
									+ middlename + "','"
									+ cf.convertsinglequotes(addr1) + "','"
									+ cf.convertsinglequotes(addr2) + "','"
									+ cf.convertsinglequotes(city) + "','"
									+ cf.convertsinglequotes(state) + "','"
									+ cf.convertsinglequotes(country) + "','" + zip
									+ "','" + homephone + "','" + cellphone + "','"
									+ workphone + "','"
									+ cf.convertsinglequotes(cperson) + "','"
									+ cf.convertsinglequotes(ownerpolicyno) + "','"
									+ IsInspected + "','" + IsUploaded + "','" + status
									+ "','" + companyid + "','"
									+ inspectorid.toString() + "','" + wId + "','"
									+ cId + "','"
									+ cf.convertsinglequotes(inspectorfirstname)
									+ "','" + cf.convertsinglequotes(inspectorlastname)
									+ "','" + scheduleddate + "','" + yearbuilt + "','"
									+ nstories + "','" + inspectionstarttime + "','"
									+ inspectionendtime + "','"
									+ cf.convertsinglequotes(inspectioncomment) + "','"
									+ inspectiontypeid + "','','" + scheduleddate
									+ "','"
									+ cf.convertsinglequotes(insurancecompanyname)
									+ "','" + email + "','0','0','" + substatus + "','"+buildingsize+"','"+cf.convertsinglequotes(TPQA)+"','"+Additionalservice+"','"+QAInspector+"','"+cf.convertsinglequotes(licenceexpirydate)+"')");
						
					}

					count++;
					if (total <= 31) {

						total = temptotal + (int) (Totaltmp);
					} else {
						total = 30;
					}
					Totaltmp += temptot;
				
			} catch (Exception e) {
				System.out.println("e=" + e.getMessage());
			}
		}
		/**For pagination **/
		try
		{
			
			 cf.onlresult=cf.Calling_WS1(cf.InspectorId, "UpdateMobileDBCount");
			 System.out.println("onlresult"+cf.onlresult);
			 retrieveonlinedata(cf.onlresult);
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
	}
private void retrieveonlinedata(SoapObject result) {
		
		int Cnt = result.getPropertyCount();
		int onpre=0,onuts=0,oncan=0,onrr=0,ontyppre=0,ontyputs=0,ontypcan=0,ontyprr=0;
		if (Cnt >= 1) {
			SoapObject obj = (SoapObject) result;

			for (int i = 0; i < obj.getPropertyCount(); i++) {
				SoapObject obj1 = (SoapObject) obj.getProperty(i);
				if (!obj1.getProperty("i_maininspectiontype").toString()
						.equals("")) {

					if (29 == Integer.parseInt(obj1.getProperty(
							"i_maininspectiontype").toString())) {
						
						onpre = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						onuts = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						oncan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						onrr =Integer.parseInt(obj1.getProperty("Completed")
														.toString());
						
                         
					}

					if (28 == Integer.parseInt(obj1.getProperty(
							"i_maininspectiontype").toString())) {
						ontyppre = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						ontyputs = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						ontypcan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						ontyprr =  Integer.parseInt(obj1.getProperty("Completed")
														.toString());
						
					}

				}
			}
			try
			{
				//insp_id varchar(50),C_28_pre varchar(5) Default('0'),C_28_uts varchar(5) Default('0'),C_28_can varchar(5) Default('0'),C_28_RR varchar(5) Default('0')
				Cursor c=cf.SelectTablefunction(cf.count_tbl, " WHERE insp_id='"+cf.InspectorId+"'");
				if(c.getCount()>0)
				{
					cf.db.execSQL(" UPDATE "+cf.count_tbl+" SET C_28_pre='"+ontyppre+"',C_28_uts='"+ontyputs+"',C_28_can='"+ontypcan+"',C_28_RR='"+ontyprr+"',C_29_pre='"+onpre+"',C_29_uts='"+onuts+"',C_29_can='"+oncan+"',C_29_RR='"+onrr+"' Where  insp_id='"+cf.InspectorId+"'");
				}
				else
				{
					cf.db.execSQL(" INSERT INTO  "+cf.count_tbl+" (insp_id,C_28_pre,C_28_uts,C_28_can,C_28_RR,C_29_pre,C_29_uts,C_29_can,C_29_RR) VALUES " +
							"('"+cf.InspectorId+"','"+ontyppre+"',"+ontyputs+","+ontypcan+","+ontyprr+","+onpre+","+onuts+","+oncan+","+onrr+")");
				}
				c.close();
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
}
/**For pagination **/
	private void AdditionalService() throws NetworkErrorException, IOException,
	XmlPullParserException, SocketTimeoutException {
		
		double Totaltmp = 0.0;
		cf.Createtablefunction(19);
		String isrecord,usertypename,srid,usertype,contactemail, phnumber, mobnumber,besttimetocallu,firstchoice,secondchoice,thirdchoice,feedbackcomments;
		try
		{
		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME33);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("inspectorid", cf.InspectorId);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.SOAP_ACTION33, envelope);
		
			SoapObject result = (SoapObject) envelope.getResponse();
			if (cf.checkresponce(result) == false) {
				total = 80;
				return;
			}
			else
			{
					int countchk = result.getPropertyCount();
					Totaltmp = (5.00 / (double) countchk);
					double temptot = Totaltmp;
					int temptotal = total;
					
					for (int i = 0; i < result.getPropertyCount(); i++) 
					{
						SoapObject obj = (SoapObject) result.getProperty(i);
						srid = String.valueOf(obj.getProperty("SRID"));
						usertypename = String.valueOf(obj.getProperty("UserTypeName"));
						contactemail = String.valueOf(obj.getProperty("ContactEmail"));
						phnumber = String.valueOf(obj.getProperty("PhoneNumber"));
						mobnumber = String.valueOf(obj.getProperty("MobileNumber"));
						besttimetocallu = String.valueOf(obj.getProperty("BestTimetoCallYou"));
						firstchoice = String.valueOf(obj.getProperty("FirstChoice"));
						secondchoice = String.valueOf(obj.getProperty("SecondChoice"));
						thirdchoice = String.valueOf(obj.getProperty("ThirdChoice"));
						feedbackcomments = String.valueOf(obj.getProperty("FeedbackComments"));
							Cursor cur = cf.SelectTablefunction(cf.additionalservice,
									" where InspectorId='" + cf.InspectorId + "' and SRID='"+homeid+"'");
								if(cur.getCount()>0)
								{
									try {
											cf.db.execSQL("UPDATE "+cf.additionalservice+" set UserTypeName='"+usertypename+"',ContactEmail='"+contactemail+"'," +
													"PhoneNumber='"+phnumber+"',MobileNumber='"+mobnumber+"',BestTimetoCallYou='"+besttimetocallu+"'," +
															"FirstChoice='"+firstchoice+"',SecondChoice='"+ secondchoice+"',ThirdChoice='"+thirdchoice+"'," +
																	"FeedbackComments='"+feedbackcomments+"' WHERE SRID='"+srid+"' and " +
																			"InspectorId='"+cf.InspectorId+"'");
										
									} catch (Exception e) {
									}
								}
								else
								{
									try
									{
										cf.db.execSQL("INSERT INTO "
											+ cf.additionalservice
											+ "(SRID,InspectorId,IsRecord,UserTypeName,ContactEmail,PhoneNumber,MobileNumber,BestTimetoCallYou,FirstChoice,SecondChoice,ThirdChoice,FeedbackComments)"
											+ " VALUES ('"+srid+"','"+cf.InspectorId+"','','"+usertypename+"','"+contactemail+"','"+phnumber+"','"+mobnumber+"','"+besttimetocallu+"','"+firstchoice+"','"+secondchoice+"','"+thirdchoice+"','"+feedbackcomments+"')");
								}
									catch(Exception e)
									{
										System.out.println("DFDD "+e.getMessage());
									}
								}
								if (total <= 86) {
									total = temptotal + (int) (Totaltmp);
								} else {
									total = 85;
								}
								Totaltmp += temptot;
		
					}
			}
					
				}
				catch (Exception e) {
					System.out.println("EE "+e.getMessage());
				}
		
	}
	private void InsertCloudDate() throws NetworkErrorException, IOException,
			XmlPullParserException, SocketTimeoutException {
		int av = 0;
		double Totaltmp = 0.0, temptot = 0;
		int countchk, temptotal = 0;
		cf.Createtablefunction(3);
		SoapObject crequest = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME5);
		SoapSerializationEnvelope cenvelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		cenvelope.dotNet = true;
		crequest.addProperty("InspectorID", cf.InspectorId.toString());
		cenvelope.setOutputSoapObject(crequest);
		HttpTransportSE androidHttpTransportC = new HttpTransportSE(cf.URL);
		SoapObject cresult = null;
		try

		{
			androidHttpTransportC.call(cf.SOAP_ACTION5, cenvelope);
			cresult = (SoapObject) cenvelope.getResponse();
			if (cf.checkresponce(cresult) == false) {
				total = 100;
				usercheck = 0;
				return;
			} else {
				countchk = cresult.getPropertyCount();

				int count = 0;
				Totaltmp = (10.00 / (double) countchk);
				temptot = Totaltmp;
				temptotal = total;
				for (int i = 0; i < countchk; i++) {
					Double assignmentId, assignmentMasterId, inspectionPolicy_Id, priorAssignmentId;
					String c_Homeid, inspectionType, assignmentType, policyId, policyVersion, policyEndorsement, policyEffectiveDate, policyForm, policySystem, lob, structureCount, structureNumber, structureDescription, propertyAddress, propertyAddress2, propertyCity, propertyState, propertyCounty, propertyZip, insuredFirstName, insuredLastName, insuredHomePhone, insuredWorkPhone, insuredAlternatePhone, insuredEmail, insuredMailingAddress, insuredMailingAddress2, insuredMailingCity, insuredMailingState, insuredMailingZip, agencyID, agencyName, agencyPhone, agencyFax, agencyEmail, agencyPrincipalFirstName, agencyPrincipalLastName, agencyPrincipalEmail, agencyFEIN, agencyMailingAddress, agencyMailingAddress2, agencyMailingCity, agencyMailingState, agencyMailingZip, agentID, agentFirstName, agentLastName, agentEmail, previousInspectionDate, previousInspectionCompany, previousInspectorFirstName, previousInspectorLastName, previousInspectorLicense, yearBuilt, squareFootage, numberOfStories, construction, roofCovering, roofDeckAttachment, roofWallAttachment, roofShape, secondaryWaterResistance, openingCoverage, buildingType;
					SoapObject result = (SoapObject) cresult.getProperty(i);
					c_Homeid = result.getProperty("SRID").toString();
					Cursor c1 = cf.SelectTablefunction(cf.cloudtable,
							" where Homeid='" + c_Homeid + "'");

					if (c1.getCount() < 1) {
						if (result.getProperty("assignmentId").toString() == "") {
							assignmentId = (double) 0;
						} else {
							assignmentId = Double.valueOf(result.getProperty(
									"assignmentId").toString());
						}
						if (result.getProperty("assignmentMasterId").toString() == "") {
							assignmentMasterId = (double) 0;
						} else {
							assignmentMasterId = Double.valueOf(result
									.getProperty("assignmentMasterId")
									.toString());
						}
						if (result.getProperty("inspectionPolicy_Id")
								.toString() == "") {
							inspectionPolicy_Id = (double) 0;
						} else {
							inspectionPolicy_Id = Double.valueOf(result
									.getProperty("inspectionPolicy_Id")
									.toString());
						}
						if (result.getProperty("priorAssignmentId").toString() == "") {
							priorAssignmentId = (double) 0;
						} else {
							priorAssignmentId = Double.valueOf(result
									.getProperty("priorAssignmentId")
									.toString());
						}
						inspectionType = result.getProperty("inspectionType")
								.toString();
						assignmentType = result.getProperty("assignmentType")
								.toString();
						policyId = result.getProperty("policyId").toString();
						policyVersion = result.getProperty("policyVersion")
								.toString();
						policyEndorsement = result.getProperty(
								"policyEndorsement").toString();
						policyEffectiveDate = result.getProperty(
								"policyEffectiveDate").toString();
						policyForm = result.getProperty("policyForm")
								.toString();
						policySystem = result.getProperty("policySystem")
								.toString();
						lob = result.getProperty("lob").toString();
						structureCount = result.getProperty("structureCount")
								.toString();
						structureNumber = result.getProperty("structureNumber")
								.toString();

						structureDescription = "";
						propertyAddress = result.getProperty("propertyAddress")
								.toString();
						propertyAddress2 = result.getProperty(
								"propertyAddress2").toString();
						propertyCity = result.getProperty("propertyCity")
								.toString();
						propertyState = result.getProperty("propertyState")
								.toString();
						propertyCounty = result.getProperty("propertyCounty")
								.toString();
						propertyZip = result.getProperty("propertyZip")
								.toString();
						insuredFirstName = result.getProperty(
								"insuredFirstName").toString();
						insuredLastName = result.getProperty("insuredLastName")
								.toString();

						insuredHomePhone = result.getProperty(
								"insuredHomePhone").toString();
						insuredWorkPhone = result.getProperty(
								"insuredWorkPhone").toString();
						insuredAlternatePhone = result.getProperty(
								"insuredAlternatePhone").toString();
						insuredEmail = result.getProperty("insuredEmail")
								.toString();
						insuredMailingAddress = result.getProperty(
								"insuredMailingAddress").toString();
						insuredMailingAddress2 = result.getProperty(
								"insuredMailingAddress2").toString();
						insuredMailingCity = result.getProperty(
								"insuredMailingCity").toString();
						insuredMailingState = result.getProperty(
								"insuredMailingState").toString();

						insuredMailingZip = result.getProperty(
								"insuredMailingZip").toString();
						agencyID = result.getProperty("agencyID").toString();
						agencyName = result.getProperty(34).toString();
						agencyPhone = result.getProperty("agencyPhone")
								.toString();
						agencyFax = result.getProperty(36).toString();
						agencyEmail = result.getProperty("agencyEmail")
								.toString();
						agencyPrincipalFirstName = result.getProperty(38)
								.toString();
						agencyPrincipalLastName = result.getProperty(
								"agencyPrincipalLastName").toString();
						agencyPrincipalEmail = result.getProperty(40)
								.toString();
						agencyFEIN = result.getProperty("agencyFEIN")
								.toString();

						agencyMailingAddress = result.getProperty(
								"agencyMailingAddress").toString();
						agencyMailingAddress2 = result.getProperty(43)
								.toString();
						agencyMailingCity = result.getProperty(
								"agencyMailingCity").toString();
						agencyMailingState = result.getProperty(45).toString();
						agencyMailingZip = result.getProperty(
								"agencyMailingZip").toString();
						agentID = result.getProperty(47).toString();
						agentFirstName = result.getProperty("agentFirstName")
								.toString();
						agentLastName = result.getProperty(49).toString();
						agentEmail = result.getProperty("agentEmail")
								.toString();

						previousInspectionDate = result.getProperty(
								"previousInspectionDate").toString();
						previousInspectionCompany = result.getProperty(52)
								.toString();
						previousInspectorFirstName = result.getProperty(
								"previousInspectorFirstName").toString();
						previousInspectorLastName = result.getProperty(54)
								.toString();
						previousInspectorLicense = result.getProperty(
								"previousInspectorLicense").toString();
						yearBuilt = result.getProperty(56).toString();
						squareFootage = result.getProperty("squareFootage")
								.toString();

						numberOfStories = result.getProperty("numberOfStories")
								.toString();
						construction = result.getProperty(59).toString();
						roofCovering = result.getProperty("roofCovering")
								.toString();
						roofDeckAttachment = result.getProperty(61).toString();
						roofWallAttachment = result.getProperty(
								"roofWallAttachment").toString();
						roofShape = result.getProperty(63).toString();
						secondaryWaterResistance = result.getProperty(
								"secondaryWaterResistance").toString();
						openingCoverage = result.getProperty("openingCoverage")
								.toString();
						buildingType = "";
						try {
							cf.db.execSQL("INSERT INTO "
									+ cf.cloudtable
									+ " (Homeid,assignmentId,assignmentMasterId,inspectionPolicy_Id,priorAssignmentId,inspectionType,assignmentType,policyId,policyVersion,policyEndorsement,policyEffectiveDate,policyForm,policySystem,lob,structureCount,structureNumber,structureDescription,propertyAddress,propertyAddress2,propertyCity,propertyState,propertyCounty,propertyZip,insuredFirstName,insuredLastName,insuredHomePhone,insuredWorkPhone,insuredAlternatePhone,insuredEmail,insuredMailingAddress,insuredMailingAddress2,insuredMailingCity,insuredMailingState,insuredMailingZip,agencyID,agencyName,agencyPhone,agencyFax,agencyEmail,agencyPrincipalFirstName,agencyPrincipalLastName,agencyPrincipalEmail,agencyFEIN,agencyMailingAddress,agencyMailingAddress2,agencyMailingCity,agencyMailingState,agencyMailingZip,agentID,agentFirstName,agentLastName,agentEmail,previousInspectionDate,previousInspectionCompany,previousInspectorFirstName,previousInspectorLastName,previousInspectorLicense,yearBuilt,squareFootage,numberOfStories,construction,roofCovering,roofDeckAttachment,roofWallAttachment,roofShape,secondaryWaterResistance,openingCoverage,buildingType)"
									+ " VALUES ('" + c_Homeid + "','"
									+ assignmentId + "','" + assignmentMasterId
									+ "','" + inspectionPolicy_Id + "','"
									+ priorAssignmentId + "','"
									+ inspectionType + "','" + assignmentType
									+ "','" + policyId + "','" + policyVersion
									+ "','" + policyEndorsement + "','"
									+ policyEffectiveDate + "','" + policyForm
									+ "','" + policySystem + "','" + lob
									+ "','" + structureCount + "','"
									+ structureNumber + "','"
									+ structureDescription + "','"
									+ propertyAddress + "','"
									+ propertyAddress2 + "','" + propertyCity
									+ "','" + propertyState + "','"
									+ propertyCounty + "','" + propertyZip
									+ "','" + insuredFirstName + "','"
									+ insuredLastName + "','"
									+ insuredHomePhone + "','"
									+ insuredWorkPhone + "','"
									+ insuredAlternatePhone + "','"
									+ insuredEmail + "','"
									+ insuredMailingAddress + "','"
									+ insuredMailingAddress2 + "','"
									+ insuredMailingCity + "','"
									+ insuredMailingState + "','"
									+ insuredMailingZip + "','" + agencyID
									+ "','" + agencyName + "','" + agencyPhone
									+ "','" + agencyFax + "','" + agencyEmail
									+ "','" + agencyPrincipalFirstName + "','"
									+ agencyPrincipalLastName + "','"
									+ agencyPrincipalEmail + "','" + agencyFEIN
									+ "','" + agencyMailingAddress + "','"
									+ agencyMailingAddress2 + "','"
									+ agencyMailingCity + "','"
									+ agencyMailingState + "','"
									+ agencyMailingZip + "','" + agentID
									+ "','" + agentFirstName + "','"
									+ agentLastName + "','" + agentEmail
									+ "','" + previousInspectionDate + "','"
									+ previousInspectionCompany + "','"
									+ previousInspectorFirstName + "','"
									+ previousInspectorLastName + "','"
									+ previousInspectorLicense + "','"
									+ yearBuilt + "','" + squareFootage + "','"
									+ numberOfStories + "','" + construction
									+ "','" + roofCovering + "','"
									+ roofDeckAttachment + "','"
									+ roofWallAttachment + "','" + roofShape
									+ "','" + secondaryWaterResistance + "','"
									+ openingCoverage + "','" + buildingType
									+ "')");

						} catch (Exception e) {

						}
					}
					if (total <= 101) {
						total = temptotal + (int) (Totaltmp);
					} else {
						total = 100;
					}
					Totaltmp += temptot;
				}
			}
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}

	private void getCloudInformation(String result2)
			throws NetworkErrorException, IOException, XmlPullParserException {

		cf.Createtablefunction(3);
		Cursor c1 = cf.SelectTablefunction(cf.cloudtable, " where Homeid='"
				+ result2 + "'");

		if (c1.getCount() < 1) {
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.addMapping(cf.NAMESPACE, "CommonData", Import.class);
			SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME5);
			SoapObject ad_property = new SoapObject(cf.NAMESPACE,
					"ExportDataEntity");
			request.addProperty("SRID", result2);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);

			androidHttpTransport.call(cf.SOAP_ACTION5, envelope);
			SoapObject result1 = (SoapObject) envelope.getResponse();
			int count = 0;

			Double assignmentId, assignmentMasterId, inspectionPolicy_Id, priorAssignmentId;
			String c_Homeid, inspectionType, assignmentType, policyId, policyVersion, policyEndorsement, policyEffectiveDate, policyForm, policySystem, lob, structureCount, structureNumber, structureDescription, propertyAddress, propertyAddress2, propertyCity, propertyState, propertyCounty, propertyZip, insuredFirstName, insuredLastName, insuredHomePhone, insuredWorkPhone, insuredAlternatePhone, insuredEmail, insuredMailingAddress, insuredMailingAddress2, insuredMailingCity, insuredMailingState, insuredMailingZip, agencyID, agencyName, agencyPhone, agencyFax, agencyEmail, agencyPrincipalFirstName, agencyPrincipalLastName, agencyPrincipalEmail, agencyFEIN, agencyMailingAddress, agencyMailingAddress2, agencyMailingCity, agencyMailingState, agencyMailingZip, agentID, agentFirstName, agentLastName, agentEmail, previousInspectionDate, previousInspectionCompany, previousInspectorFirstName, previousInspectorLastName, previousInspectorLicense, yearBuilt, squareFootage, numberOfStories, construction, roofCovering, roofDeckAttachment, roofWallAttachment, roofShape, secondaryWaterResistance, openingCoverage, buildingType;

			if (result1.getPropertyCount() == 1) {
				SoapObject result = (SoapObject) result1.getProperty(0);

				c_Homeid = result2;
				if (result.getProperty("assignmentId").toString() == "") {
					assignmentId = (double) 0;
				} else {
					assignmentId = Double.valueOf(result.getProperty(
							"assignmentId").toString());
				}
				if (result.getProperty("assignmentMasterId").toString() == "") {
					assignmentMasterId = (double) 0;
				} else {
					assignmentMasterId = Double.valueOf(result.getProperty(
							"assignmentMasterId").toString());
				}
				if (result.getProperty("inspectionPolicy_Id").toString() == "") {
					inspectionPolicy_Id = (double) 0;
				} else {
					inspectionPolicy_Id = Double.valueOf(result.getProperty(
							"inspectionPolicy_Id").toString());
				}
				if (result.getProperty("priorAssignmentId").toString() == "") {
					priorAssignmentId = (double) 0;
				} else {
					priorAssignmentId = Double.valueOf(result.getProperty(
							"priorAssignmentId").toString());
				}
				inspectionType = result.getProperty("inspectionType")
						.toString();
				assignmentType = result.getProperty("assignmentType")
						.toString();
				policyId = result.getProperty("policyId").toString();
				policyVersion = result.getProperty("policyVersion").toString();
				policyEndorsement = result.getProperty("policyEndorsement")
						.toString();
				policyEffectiveDate = result.getProperty("policyEffectiveDate")
						.toString();
				policyForm = result.getProperty("policyForm").toString();
				policySystem = result.getProperty("policySystem").toString();
				lob = result.getProperty("lob").toString();
				structureCount = result.getProperty("structureCount")
						.toString();
				structureNumber = result.getProperty("structureNumber")
						.toString();

				structureDescription = "";
				propertyAddress = result.getProperty("propertyAddress")
						.toString();
				propertyAddress2 = result.getProperty("propertyAddress2")
						.toString();
				propertyCity = result.getProperty("propertyCity").toString();
				propertyState = result.getProperty("propertyState").toString();
				propertyCounty = result.getProperty("propertyCounty")
						.toString();
				propertyZip = result.getProperty("propertyZip").toString();
				insuredFirstName = result.getProperty("insuredFirstName")
						.toString();
				insuredLastName = result.getProperty("insuredLastName")
						.toString();

				insuredHomePhone = result.getProperty("insuredHomePhone")
						.toString();
				insuredWorkPhone = result.getProperty("insuredWorkPhone")
						.toString();
				insuredAlternatePhone = result.getProperty(
						"insuredAlternatePhone").toString();
				insuredEmail = result.getProperty("insuredEmail").toString();
				insuredMailingAddress = result.getProperty(
						"insuredMailingAddress").toString();
				insuredMailingAddress2 = result.getProperty(
						"insuredMailingAddress2").toString();
				insuredMailingCity = result.getProperty("insuredMailingCity")
						.toString();
				insuredMailingState = result.getProperty("insuredMailingState")
						.toString();

				insuredMailingZip = result.getProperty("insuredMailingZip")
						.toString();
				agencyID = result.getProperty("agencyID").toString();
				agencyName = result.getProperty(34).toString();
				agencyPhone = result.getProperty("agencyPhone").toString();
				agencyFax = result.getProperty(36).toString();
				agencyEmail = result.getProperty("agencyEmail").toString();
				agencyPrincipalFirstName = result.getProperty(38).toString();
				agencyPrincipalLastName = result.getProperty(
						"agencyPrincipalLastName").toString();
				agencyPrincipalEmail = result.getProperty(40).toString();
				agencyFEIN = result.getProperty("agencyFEIN").toString();

				agencyMailingAddress = result.getProperty(
						"agencyMailingAddress").toString();
				agencyMailingAddress2 = result.getProperty(43).toString();
				agencyMailingCity = result.getProperty("agencyMailingCity")
						.toString();
				agencyMailingState = result.getProperty(45).toString();
				agencyMailingZip = result.getProperty("agencyMailingZip")
						.toString();
				agentID = result.getProperty(47).toString();
				agentFirstName = result.getProperty("agentFirstName")
						.toString();
				agentLastName = result.getProperty(49).toString();
				agentEmail = result.getProperty("agentEmail").toString();

				previousInspectionDate = result.getProperty(
						"previousInspectionDate").toString();
				previousInspectionCompany = result.getProperty(52).toString();
				previousInspectorFirstName = result.getProperty(
						"previousInspectorFirstName").toString();
				previousInspectorLastName = result.getProperty(54).toString();
				previousInspectorLicense = result.getProperty(
						"previousInspectorLicense").toString();
				yearBuilt = result.getProperty(56).toString();
				squareFootage = result.getProperty("squareFootage").toString();

				numberOfStories = result.getProperty("numberOfStories")
						.toString();
				construction = result.getProperty(59).toString();
				roofCovering = result.getProperty("roofCovering").toString();
				roofDeckAttachment = result.getProperty(61).toString();
				roofWallAttachment = result.getProperty("roofWallAttachment")
						.toString();
				roofShape = result.getProperty(63).toString();
				secondaryWaterResistance = result.getProperty(
						"secondaryWaterResistance").toString();
				openingCoverage = result.getProperty("openingCoverage")
						.toString();
				buildingType = "";

				try {
					cf.db.execSQL("INSERT INTO "
							+ cf.cloudtable
							+ " (Homeid,assignmentId,assignmentMasterId,inspectionPolicy_Id,priorAssignmentId,inspectionType,assignmentType,policyId,policyVersion,policyEndorsement,policyEffectiveDate,policyForm,policySystem,lob,structureCount,structureNumber,structureDescription,propertyAddress,propertyAddress2,propertyCity,propertyState,propertyCounty,propertyZip,insuredFirstName,insuredLastName,insuredHomePhone,insuredWorkPhone,insuredAlternatePhone,insuredEmail,insuredMailingAddress,insuredMailingAddress2,insuredMailingCity,insuredMailingState,insuredMailingZip,agencyID,agencyName,agencyPhone,agencyFax,agencyEmail,agencyPrincipalFirstName,agencyPrincipalLastName,agencyPrincipalEmail,agencyFEIN,agencyMailingAddress,agencyMailingAddress2,agencyMailingCity,agencyMailingState,agencyMailingZip,agentID,agentFirstName,agentLastName,agentEmail,previousInspectionDate,previousInspectionCompany,previousInspectorFirstName,previousInspectorLastName,previousInspectorLicense,yearBuilt,squareFootage,numberOfStories,construction,roofCovering,roofDeckAttachment,roofWallAttachment,roofShape,secondaryWaterResistance,openingCoverage,buildingType)"
							+ " VALUES ('" + c_Homeid + "','" + assignmentId
							+ "','" + assignmentMasterId + "','"
							+ inspectionPolicy_Id + "','" + priorAssignmentId
							+ "','" + inspectionType + "','" + assignmentType
							+ "','" + policyId + "','" + policyVersion + "','"
							+ policyEndorsement + "','" + policyEffectiveDate
							+ "','" + policyForm + "','" + policySystem + "','"
							+ lob + "','" + structureCount + "','"
							+ structureNumber + "','" + structureDescription
							+ "','" + propertyAddress + "','"
							+ propertyAddress2 + "','" + propertyCity + "','"
							+ propertyState + "','" + propertyCounty + "','"
							+ propertyZip + "','" + insuredFirstName + "','"
							+ insuredLastName + "','" + insuredHomePhone
							+ "','" + insuredWorkPhone + "','"
							+ insuredAlternatePhone + "','" + insuredEmail
							+ "','" + insuredMailingAddress + "','"
							+ insuredMailingAddress2 + "','"
							+ insuredMailingCity + "','" + insuredMailingState
							+ "','" + insuredMailingZip + "','" + agencyID
							+ "','" + agencyName + "','" + agencyPhone + "','"
							+ agencyFax + "','" + agencyEmail + "','"
							+ agencyPrincipalFirstName + "','"
							+ agencyPrincipalLastName + "','"
							+ agencyPrincipalEmail + "','" + agencyFEIN + "','"
							+ agencyMailingAddress + "','"
							+ agencyMailingAddress2 + "','" + agencyMailingCity
							+ "','" + agencyMailingState + "','"
							+ agencyMailingZip + "','" + agentID + "','"
							+ agentFirstName + "','" + agentLastName + "','"
							+ agentEmail + "','" + previousInspectionDate
							+ "','" + previousInspectionCompany + "','"
							+ previousInspectorFirstName + "','"
							+ previousInspectorLastName + "','"
							+ previousInspectorLicense + "','" + yearBuilt
							+ "','" + squareFootage + "','" + numberOfStories
							+ "','" + construction + "','" + roofCovering
							+ "','" + roofDeckAttachment + "','"
							+ roofWallAttachment + "','" + roofShape + "','"
							+ secondaryWaterResistance + "','"
							+ openingCoverage + "','" + buildingType + "')");
					Cursor c2 = cf.SelectTablefunction(cf.cloudtable,
							" where Homeid=" + result2);

				} catch (Exception e) {

				}

			}
			c1.close();

		}

	}

	public void AgentInfo() throws NetworkErrorException, IOException,
			XmlPullParserException, SocketTimeoutException {
		// TODO Auto-generated method stub
		cf.Createtablefunction(4);

		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME6);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectionID", cf.InspectorId);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.SOAP_ACTION6, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		if (cf.checkresponce(result) == false) {
			total = 35;
			return;
		} else {
			double Totaltmp = 0.0;
			int countchk = result.getPropertyCount();
			Totaltmp = (5.00 / (double) countchk);
			double temptot = Totaltmp;
			int temptotal = total;
			for (int i = 0; i < result.getPropertyCount(); i++) {
				SoapObject results = (SoapObject) result.getProperty(i);
				homeid = results.getProperty("SRID").toString();
				Cursor c1 = cf.SelectTablefunction(cf.agentinfo,
						" where Homeid='" + homeid + "'");

				if (c1.getCount() < 1) {
					InsertAgentData(results, homeid);
				}
				if (total <= 36) {
					total = temptotal + (int) (Totaltmp);
				} else {
					total = 35;
				}
				Totaltmp += temptot;
			}
		}

	}

	private void InsertAgentData(SoapObject objInsert, String homeid) {

		String agncyname, agntname, agntaddr1, agntaddr2, agntcity, agntcnty, agntrole, agntstate, agntzip, agntoffphn, agntconphn, agntfax, agntmail, agntweb;
		agncyname = String.valueOf(objInsert.getProperty("AgencyName"));
		agntname = String.valueOf(objInsert.getProperty("AgentName"));
		agntaddr1 = String.valueOf(objInsert.getProperty("AgentAddress"));
		agntaddr2 = String.valueOf(objInsert.getProperty("AgentAddress2"));
		agntcity = String.valueOf(objInsert.getProperty("AgentCity"));
		agntcnty = String.valueOf(objInsert.getProperty("AgentCounty"));
		agntrole = String.valueOf(objInsert.getProperty("AgentRole"));
		agntstate = String.valueOf(objInsert.getProperty("AgentState"));
		agntzip = String.valueOf(objInsert.getProperty("AgentZip"));
		agntoffphn = String.valueOf(objInsert.getProperty("AgentOffPhone"));
		agntconphn = String.valueOf(objInsert.getProperty("AgentContactPhone"));
		agntfax = String.valueOf(objInsert.getProperty("AgentFax"));
		agntmail = String.valueOf(objInsert.getProperty("AgentEmail"));
		agntweb = String.valueOf(objInsert.getProperty("AgentWebSite"));
		try {
			cf.db.execSQL("INSERT INTO "
					+ cf.agentinfo
					+ " (Homeid,agencyname,agentname,address1,address2,city,country,role,state,zip,offphone,contactphone,fax,email,website)"
					+ " VALUES ('" + homeid + "','" + agncyname + "','"
					+ agntname + "','" + agntaddr1 + "','" + agntaddr2 + "','"
					+ agntcity + "','" + agntcnty + "','" + agntrole + "','"
					+ agntstate + "','" + agntzip + "','" + agntoffphn + "','"
					+ agntconphn + "','" + agntfax + "','" + agntmail + "','"
					+ agntweb + "')");

		} catch (Exception e) {
			System.out.println("Error = " + e);
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg;
			intimg = new Intent(Import.this, HomeScreen.class);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
//		try
//		{
//			t.stop();
//			System.out.println("the error fixed");
//		}
//		catch (Exception e) {
//			// TODO: handle exception
//			System.out.println("error occures");
//		}
		finish();
		onDestroy();
		
		super.onStop();
	}

}