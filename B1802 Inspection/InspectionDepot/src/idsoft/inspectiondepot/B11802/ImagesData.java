package idsoft.inspectiondepot.B11802;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ScrollView;
import android.widget.TextView;

public class ImagesData extends Activity {
	private static final String TAG = null;
	Dialog dialog;
	private static final int SELECT_PICTURE = 0;
	String picname="", picname1="", homeid, fname, lname, addres, signpathdesc,
			photopathdesc, selectedImagePath = "empty", extStorageDirectory,
			InspectionType, status, filepath = "empty",
			selectedImagePathforpaper = "empty", capturedImageFilePath;
	int updrws, u, sgnrws, t, s = 0, quesid = 0, elev;
	Uri mCapturedImageURI;
	DrawView drawView;
	byte[] byteArray;
	private int nBitmapWidth = 400, nBitmapHeight = 170;
	private Bitmap mBitmap;
	private Canvas mCanvas;
	private Path mPath;
	private Paint mBitmapPaint;
	boolean bu1, bu2;
	private Paint mPaint = new Paint();
	TextView ownername;
	ProgressDialog pd;
	ListAdapter adapter;
	ImageView hmesignimg, papersignimg;
	String[] items = { "gallery", "sketch" };
	int value, Count;
	Canvas canvas;
	ImageView sgn, sgntick, frnt, frnttick, rgt, rgttick, lft, lfttick, bac,
			bactick, atc, atctick, addit, addittick;
	Button signimg, frntimg, bacimg, rgtimg, lftimg, atcimg, additionalimg;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	protected static final String CLEAR = null;
	private static final int visibility = 0;
	public int select_count_p = 0, select_count = 0;
	EditText ed1, ed2;
	ScrollView scr;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	TextView policyholderinfo;
	CommonFunctions cf;
	UploadRestriction upr = new UploadRestriction();

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);

		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		setContentView(R.layout.imagesdata);
		cf.getInspectorId();
		cf.getDeviceDimensions();
		cf.getinspectiondate();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 3, cf, 0));
		ScrollView scr = (ScrollView) findViewById(R.id.scr);
		scr.setMinimumHeight(cf.ht);

		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		ownername = (TextView) findViewById(R.id.header);
		sgntick = (ImageView) findViewById(R.id.signovr);
		frnttick = (ImageView) findViewById(R.id.frontovr);
		rgttick = (ImageView) findViewById(R.id.rightovr);
		bactick = (ImageView) findViewById(R.id.backovr);
		lfttick = (ImageView) findViewById(R.id.leftovr);
		atctick = (ImageView) findViewById(R.id.atticovr);
		addittick = (ImageView) findViewById(R.id.additionalovr);

		hmesignimg = (ImageView) findViewById(R.id.updimg1);
		papersignimg = (ImageView) findViewById(R.id.updimg2);
		ed1 = (EditText) findViewById(R.id.header4);
		ed2 = (EditText) findViewById(R.id.header5);
		scr = (ScrollView) findViewById(R.id.scr);
		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		cf.setRcvalue(rccode);
		try {
			Cursor cur = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
			int rws1 = cur.getCount();
			cur.moveToFirst();
			String ownerfirstname = cur.getString(cur
					.getColumnIndex("s_OwnersNameFirst"));
			String ownerlastname = cur.getString(cur
					.getColumnIndex("s_OwnersNameLast"));
			String policyno = cur.getString(cur
					.getColumnIndex("s_OwnerPolicyNumber"));
			addres = cur.getString(cur.getColumnIndex("s_propertyAddress"));
			policyholderinfo.setText(Html.fromHtml(cf
					.getsinglequotes(ownerfirstname)
					+ " "
					+ cf.getsinglequotes(ownerlastname)
					+ "<br/><font color=#bddb00>(Policy No : "
					+ cf.getsinglequotes(policyno) + ")</font>"));
			ownername.setText(cf.getsinglequotes(ownerfirstname) + " "
					+ cf.getsinglequotes(ownerlastname) + " " + ","
					+ cf.getsinglequotes(addres));
		} catch (Exception e) {
			System.out.println(" errrr " + e.getMessage());
		}

		/** Creating signature table **/
		cf.Createtablefunction(13);
		cf.Createtablefunction(9);

		signimg = (Button) findViewById(R.id.sign);
		frntimg = (Button) findViewById(R.id.front);
		rgtimg = (Button) findViewById(R.id.right);
		bacimg = (Button) findViewById(R.id.back);
		lftimg = (Button) findViewById(R.id.left);
		atcimg = (Button) findViewById(R.id.attic);
		additionalimg = (Button) findViewById(R.id.additional);
		changeimage();
		try {
			Cursor c1 = cf.db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE SrID='" + cf.Homeid + "'", null);
			sgnrws = c1.getCount();

			if (sgnrws == 0) {

			} else {
				u = 5;
				try {
					Cursor c11 = cf.db.rawQuery("SELECT * FROM "
							+ cf.ImageTable + " WHERE SrID='" + cf.Homeid
							+ "' and Elevation=6", null);
					int rws = c11.getCount();
					select_count = rws;
					int Column1 = c11.getColumnIndex("ImageName");
					int Column2 = c11.getColumnIndex("Description");
					c11.moveToFirst();
					if (c11 != null) {
						do {
							String sgnpath = cf.getsinglequotes(c11
									.getString(Column1));
							String signdesc = cf.getsinglequotes(c11
									.getString(Column2));
							ed1.setText(signdesc);
							selectedImagePath = sgnpath;
							signpathdesc = signdesc;
							homephotodisplay();
						} while (c11.moveToNext());
					}
					c11.close();
				} catch (Exception e) {
					Log.i(TAG, "error= " + e.getMessage());
				}
				try {
					Cursor c12 = cf.db.rawQuery("SELECT * FROM "
							+ cf.ImageTable + " WHERE SrID='" + cf.Homeid
							+ "' and Elevation=7", null);
					int rws = c12.getCount();
					select_count_p = rws;
					int Column1 = c12.getColumnIndex("ImageName");
					int Column2 = c12.getColumnIndex("Description");
					c12.moveToFirst();
					if (c12 != null) {
						do {
							String photopath = cf.getsinglequotes(c12
									.getString(Column1));
							String photodesc = c12.getString(Column2);
							ed2.setText(cf.getsinglequotes(photodesc));
							photopathdesc = photodesc;
							selectedImagePathforpaper = photopath;
							photographdisplay();

						} while (c12.moveToNext());
					}
					c12.close();
				} catch (Exception e) {
					Log.i(TAG, "error= " + e.getMessage());
				}

			}
		} catch (Exception e) {
			Log.i(TAG, "error= " + e.getMessage());
		}
		adapter = new ArrayAdapter<String>(getApplicationContext(),
				R.layout.list_row, items) {

			ViewHolder holder;
			Drawable icon;

			class ViewHolder {
				ImageView icon;
				TextView title;
			}

			public View getView(int position, View convertView, ViewGroup parent) {
				final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				if (convertView == null) {
					convertView = inflater.inflate(R.layout.list_row, null);

					holder = new ViewHolder();
					holder.icon = (ImageView) convertView
							.findViewById(R.id.icon);
					holder.title = (TextView) convertView
							.findViewById(R.id.title);
					convertView.setTag(holder);
				} else {
					// view already defined, retrieve view holder
					holder = (ViewHolder) convertView.getTag();
				}

				// this is an image from the drawables folder

				holder.title.setText(items[position]);
				if (items[position].equals("gallery")) {
					Drawable tile = getResources().getDrawable(
							R.drawable.gallery);
					holder.icon.setImageDrawable(tile);
				} else if (items[position].equals("sketch")) {
					Drawable tile1 = getResources().getDrawable(
							R.drawable.sketch);
					holder.icon.setImageDrawable(tile1);
				}

				return convertView;
			}
		};

	}

	private void changeimage() {
		// TODO Auto-generated method stub
		try {
			Cursor sgncur = cf
					.SelectTablefunction(
							cf.ImageTable,
							"where SrID='"
									+ cf.Homeid
									+ "' and (Elevation=6 or Elevation=7)");
			int sgnrws = sgncur.getCount();
			if (sgnrws != 0) {
				sgntick.setVisibility(visibility);
			}

			Cursor frncur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=1");
			int frnrws = frncur.getCount();
			if (frnrws != 0) {
				frnttick.setVisibility(visibility);
			}

			Cursor rgtcur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=2");
			int rgtrws = rgtcur.getCount();
			if (rgtrws != 0) {
				rgttick.setVisibility(visibility);
			}

			Cursor baccur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=3");
			int bacrws = baccur.getCount();
			if (bacrws != 0) {
				bactick.setVisibility(visibility);
			}

			Cursor lftcur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=4");
			int lftrws = lftcur.getCount();
			if (lftrws != 0) {
				lfttick.setVisibility(visibility);
			}

			Cursor atccur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=5");
			int atcrws = atccur.getCount();
			if (atcrws != 0) {
				atctick.setVisibility(visibility);
			}

			Cursor addcur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=8");
			int addrws = addcur.getCount();
			if (addrws != 0) {
				addittick.setVisibility(visibility);
			}

		} catch (Exception e) {

		}

	}

	private void photographdisplay() {
		// TODO Auto-generated method stub
		if(!selectedImagePathforpaper.equals("empty"))
		{
		try {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			String j = "0";
			try {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePathforpaper), null, o);
				j = "1";
			} catch (FileNotFoundException e) {
				j = "2";
			}
			if (j.equals("1")) {
			
			final int REQUIRED_SIZE = 200;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap2 = BitmapFactory.decodeStream(new FileInputStream(
					selectedImagePathforpaper), null, o2);
			BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
			papersignimg.setImageDrawable(bmd2);
			}
			else
			{
				Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
				papersignimg.setMinimumWidth(200);
				papersignimg.setMaxWidth(200);
				papersignimg.setMinimumHeight(200);
				papersignimg.setMaxHeight(200);
				papersignimg.setImageBitmap(bmp);
			}

		} catch (FileNotFoundException e) {
		}
		}
		else
		{
			System.out.println("else inside papersignimg"+selectedImagePathforpaper);
		}
	}

	private void homephotodisplay() {
		// TODO Auto-generated method stub
		if(!selectedImagePath.equals("empty"))
		{
			try {
				
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				String k = "0";
				try {
				BitmapFactory.decodeStream(new FileInputStream(selectedImagePath),
						null, o);
				k = "1";
				}
				catch (FileNotFoundException e) {
				k = "2";
				}
				if (k.equals("1")) {
					final int REQUIRED_SIZE = 200;
					int width_tmp = o.outWidth, height_tmp = o.outHeight;
					int scale = 1;
					while (true) {
						if (width_tmp / 2 < REQUIRED_SIZE
								|| height_tmp / 2 < REQUIRED_SIZE)
							break;
						width_tmp /= 2;
						height_tmp /= 2;
						scale *= 2;
					}
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					System.out.println("sss good"+selectedImagePath);
					if(!"".equals(selectedImagePath))
					{
						Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
								selectedImagePath), null, o2);
						if(bitmap==null)
						{
							
							selectedImagePath="";
							cf.ShowToast("This image is not a supported format. You cannot upload.",1);
							hmesignimg.setImageResource(R.drawable.noimage);
						}
						else
						{
						BitmapDrawable bmd = new BitmapDrawable(bitmap);
						hmesignimg.setImageDrawable(bmd);
						
						}
					}
				}
				else
				{
					Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
					hmesignimg.setMinimumWidth(200);
					hmesignimg.setMaxWidth(200);
					hmesignimg.setMinimumHeight(200);
					hmesignimg.setMaxHeight(200);
					hmesignimg.setImageBitmap(bmp);	
				}		
	
			} catch (FileNotFoundException e) {
				
			}
		}
		else
		{
			System.out.println("else inside home"+selectedImagePath);
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.sign:
			btnshowcommon();
			signimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case R.id.front:
			elev = 1;
			nextintent();
			break;
		case R.id.right:
			
			elev = 2;
			nextintent();
			break;
		case R.id.back:
			
			elev = 3;
			nextintent();
			break;
		case R.id.left:
			elev = 4;
			nextintent();
			break;
		case R.id.attic:
			elev = 5;
			nextintent();
			break;
		case R.id.additional:
			elev = 8;
			nextintent();
			break;
		case R.id.upd1:
			t = 0;
			u = 1;
			
			pickfromgallery();
			break;
		case R.id.upd4:
			t = 0;
			u = 4;
			pickfromgallery();
			break;
		case R.id.upd3:
			t = 1;
			u = 3;
			selectedImagePathforpaper = capturedImageFilePath;
			startCameraActivity();
			break;
		case R.id.upd2:
			t = 0;
			u = 1;
			final LinearLayout loyout = new LinearLayout(this);
			loyout.setOrientation(LinearLayout.VERTICAL);

			Button Capture = new Button(this);
			Capture.setText("Save and Close");
			loyout.addView(Capture);

			Button Close = new Button(this);
			Close.setText("Close");
			loyout.addView(Close);
			loyout.addView(new DrawView(this));
			dialog = new Dialog(ImagesData.this);
			dialog.setContentView(loyout);
			dialog.setTitle("Signature");
			dialog.setCancelable(true);
			dialog.show();

			Close.setOnClickListener(new View.OnClickListener() {

				public void onClick(View arg0) {
					dialog.dismiss();
				}
			});

			Capture.setOnClickListener(new View.OnClickListener() {
				public void onClick(View arg0) {

					if (s == 1) {
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
						byteArray = stream.toByteArray();
						
						OutputStream outStream;
						Date dat = new Date();
						dat.getDate();
						dat.getYear();
						dat.getMonth();
						dat.getHours();
						dat.getMinutes();
						dat.getSeconds();
						String _fileName = dat.getMonth() + "-" + dat.getDate()
								+ "-" + dat.getYear() + " " + dat.getHours()
								+ "-" + dat.getMinutes() + "-"
								+ dat.getSeconds();
						
						filepath = "/sdcard/DCIM/Camera/"
								+ _fileName.toString() + ".jpg";

						File file = new File(extStorageDirectory, String
								.valueOf(filepath));
						try {
							outStream = new FileOutputStream(file);
							outStream.write(byteArray);
							outStream.flush();
							outStream.close();
							selectedImagePath = filepath.toString();
							String[] bits = selectedImagePath.split("/");
							picname = bits[bits.length - 1];
							dialog.dismiss();
							displayphoto();
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();

						}
					} else {
						cf.ShowToast("Please upload Signature.",1);

					}

				}
			});

			break;

		case R.id.hme:
			Intent intimg = new Intent(ImagesData.this, HomeScreen.class);
			startActivity(intimg);
			break;
		case R.id.save:
			signpathdesc = ed1.getText().toString();
			photopathdesc = ed2.getText().toString();

			if (selectedImagePath.equals("empty") && selectedImagePathforpaper.equals("empty")) {

			/*	ShowToast toast = new ShowToast(getBaseContext(),
						"Please save Home Owner Signature.");*/
				elev = 1;
				nextintent();
			} else {

				if (select_count == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.ImageTable
							+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
							+ " VALUES ('" + cf.Homeid + "','" + quesid + "','"
							+ 6 + "','"
							+ cf.convertsinglequotes(selectedImagePath) + "','"
							+ cf.convertsinglequotes(signpathdesc) + "','"
							+ cf.convertsinglequotes(picname) + "','" + cd
							+ "','" + md + "','" + 1 + "','" + 0 + "')");

					if (!selectedImagePathforpaper.equals("empty")
							&& select_count_p == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.ImageTable
								+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
								+ " VALUES ('"
								+ cf.Homeid
								+ "','"
								+ quesid
								+ "','"
								+ 7
								+ "','"
								+ cf.convertsinglequotes(selectedImagePathforpaper)
								+ "','" + cf.convertsinglequotes(photopathdesc)
								+ "','" + cf.convertsinglequotes(picname1)
								+ "','" + cd + "','" + md + "','" + 1 + "','"
								+ 0 + "')");
					} else if (!selectedImagePathforpaper.equals("empty")
							&& select_count_p != 0) {
						cf.db.execSQL("UPDATE "
								+ cf.ImageTable
								+ " SET ImageName='"
								+ cf.convertsinglequotes(selectedImagePathforpaper)
								+ "',Description='"
								+ cf.convertsinglequotes(photopathdesc)
								+ "',Nameext='"
								+ cf.convertsinglequotes(picname1)
								+ "',ModifiedOn='" + md + "' WHERE SrID ='"
								+ cf.Homeid + "' and Elevation=7");
					}

					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c2.getCount();
						if (subchkrws == 0) {
							cf.db.execSQL("INSERT INTO "
									+ cf.SubmitCheckTable
									+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
									+ "VALUES ('" + cf.InspectorId + "','"
									+ cf.Homeid
									+ "',0,0,0,0,0,0,0,0,0,1,0,0,0,0,0)");
						} else {
							cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
									+ " SET fld_signature='1' WHERE Srid ='"
									+ cf.Homeid + "' and InspectorId='"
									+ cf.InspectorId + "'");
						}
					} catch (Exception e) {

					}
					sgntick.setVisibility(visibility);
					cf.ShowToast("Signature has been saved sucessfully.",1);
				} else {
					String[] bits = selectedImagePath.split("/");
					picname = bits[bits.length - 1];
					String[] bits1 = selectedImagePathforpaper.split("/");
					picname1 = bits1[bits1.length - 1];
					cf.db.execSQL("UPDATE " + cf.ImageTable
							+ " SET ImageName='"
							+ cf.convertsinglequotes(selectedImagePath)
							+ "',Description='"
							+ cf.convertsinglequotes(signpathdesc)
							+ "',Nameext='" + cf.convertsinglequotes(picname)
							+ "',ModifiedOn='" + md + "' WHERE SrID ='"
							+ cf.Homeid + "' and Elevation=6");
					if (!selectedImagePathforpaper.equals("empty")
							&& select_count_p != 0) {
						cf.db.execSQL("UPDATE "
								+ cf.ImageTable
								+ " SET ImageName='"
								+ cf.convertsinglequotes(selectedImagePathforpaper)
								+ "',Description='"
								+ cf.convertsinglequotes(photopathdesc)
								+ "',Nameext='"
								+ cf.convertsinglequotes(picname1)
								+ "',ModifiedOn='" + md + "' WHERE SrID ='"
								+ cf.Homeid + "' and Elevation=7");
					} else if (!selectedImagePathforpaper.equals("empty")
							&& select_count_p == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.ImageTable
								+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
								+ " VALUES ('"
								+ cf.Homeid
								+ "','"
								+ quesid
								+ "','"
								+ 7
								+ "','"
								+ cf.convertsinglequotes(selectedImagePathforpaper)
								+ "','" + cf.convertsinglequotes(photopathdesc)
								+ "','" + cf.convertsinglequotes(picname1)
								+ "','" + cd + "','" + md + "','" + 1 + "','"
								+ 0 + "')");
					}

					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c2.getCount();
						if (subchkrws == 0) {
							cf.db.execSQL("INSERT INTO "
									+ cf.SubmitCheckTable
									+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
									+ "VALUES ('" + cf.InspectorId + "','"
									+ cf.Homeid
									+ "',0,0,0,0,0,0,0,0,0,1,0,0,0,0,0)");
						} else {
							cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
									+ " SET fld_signature='1' WHERE Srid ='"
									+ cf.Homeid + "' and InspectorId='"
									+ cf.InspectorId + "'");
						}
					} catch (Exception e) {

					}
					sgntick.setVisibility(visibility);
					cf.ShowToast("Signature has been saved sucessfully.",1);

				}
				elev = 1;
				nextintent();

			}
			break;
		}
	}

	public void nextintent() {
		Intent intimg = new Intent(ImagesData.this, FrontElevation.class);
		
		intimg.putExtra("homeid", cf.Homeid);
		intimg.putExtra("elevation", elev);
		intimg.putExtra("InspectionType", cf.InspectionType);
		intimg.putExtra("status", cf.status);
		intimg.putExtra("keyName", cf.value);
		intimg.putExtra("Count", cf.Count);
		startActivity(intimg);
	}

	public class DrawView extends View {

		public DrawView(Context c) {
			super(c);

			mPaint.setAntiAlias(true);
			mPaint.setDither(true);
			mPaint.setColor(0xFF000000);
			mPaint.setStyle(Paint.Style.STROKE);
			mPaint.setStrokeJoin(Paint.Join.ROUND);
			mPaint.setStrokeCap(Paint.Cap.ROUND);
			mPaint.setStrokeWidth(4);

			mBitmap = Bitmap.createBitmap(nBitmapWidth, nBitmapHeight,
					Bitmap.Config.ARGB_8888);
			mBitmap.eraseColor(0xFFFFFFFF);
			mCanvas = new Canvas(mBitmap);

			mPath = new Path();
			mBitmapPaint = new Paint(Paint.DITHER_FLAG);

		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			super.onSizeChanged(w, h, oldw, oldh);
		}

		@Override
		protected void onDraw(Canvas canvas) {

			canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
			canvas.drawPath(mPath, mPaint);

		}

		private float mX, mY;
		private static final float TOUCH_TOLERANCE = 4;

		private void touch_start(float x, float y) {
			mPath.reset();
			mPath.moveTo(x, y);
			mX = x;
			mY = y;
		}

		private void touch_move(float x, float y) {
			float dx = Math.abs(x - mX);
			float dy = Math.abs(y - mY);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
				mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
				mX = x;
				mY = y;
			}
		}

		private void touch_up() {
			s = 1;
			mPath.lineTo(mX, mY);
			// commit the path to our offscreen
			mCanvas.drawPath(mPath, mPaint);
			// kill this so we don't double draw
			mPath.reset();
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			float x = event.getX();
			float y = event.getY();

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				touch_start(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				touch_move(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				touch_up();
				invalidate();
				break;
			}
			return true;
		}

		public void ClearPath() {
			mPath.reset();
			invalidate();
		}
	}

	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);

	}

	protected void sketch() {
		// TODO Auto-generated method stub

	}

	protected void pickfromgallery() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				SELECT_PICTURE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (t == 0) {
			if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					Uri selectedImageUri = data.getData();

					boolean bu = upr.common(getPath(selectedImageUri));

					if (bu) {

						if (u == 1) {
							selectedImagePath = getPath(selectedImageUri);
							String[] bits = selectedImagePath.split("/");
							picname = bits[bits.length - 1];
						} else {
							selectedImagePathforpaper = getPath(selectedImageUri);
							String[] bits = selectedImagePathforpaper
									.split("/");
							picname1 = bits[bits.length - 1];
						}

						displayphoto();
					} else {
						cf.ShowToast("Your file size exceeds 2MB.",1);

					}

				}
			}
		} else if (t == 1) {
			switch (resultCode) {
			case 0:
				break;
			case -1:
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(mCapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				capturedImageFilePath = cursor.getString(column_index_data);
				selectedImagePathforpaper = capturedImageFilePath;
				String[] bits = selectedImagePathforpaper.split("/");
				picname1 = bits[bits.length - 1];
				u = 4;
				displayphoto();

				break;

			}
		}

	}

	private void displayphoto() {
		// TODO Auto-generated method stub
		try {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			if (u == 1) {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePath), null, o);
			} else if (u == 4) {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePathforpaper), null, o);
			} else if (u == 5) {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePath), null, o);
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePathforpaper), null, o);
			}
			final int REQUIRED_SIZE = 200;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			if (u == 1) {
				System.out.println("selectedImagePath "+selectedImagePath);
				if(!"".equals(selectedImagePath))
				{
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePath), null, o2);
				
				if(bitmap==null)
				{
					selectedImagePath="";
					cf.ShowToast("This image is not a supported format. You cannot upolad.",1);
					hmesignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					hmesignimg.setImageDrawable(bmd);
				}
			}
			} else if (u == 4) {
				if(!"".equals(selectedImagePathforpaper))
				{
				Bitmap bitmap2 = BitmapFactory.decodeStream(new FileInputStream(selectedImagePathforpaper), null,o2);
				
				if(bitmap2==null)
				{
					selectedImagePathforpaper="";
					cf.ShowToast("This image is not a supported format. You cannot upload.",1);
					papersignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
					BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
					papersignimg.setImageDrawable(bmd2);
				}
				}
			} else if (u == 5) {
				if(!"".equals(selectedImagePath))
				{
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePath), null, o2);
				
				if(bitmap==null)
				{
					selectedImagePath="";
					cf.ShowToast("This image is not a supported format. You cannot u",1);
					hmesignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					hmesignimg.setImageDrawable(bmd);
				}
				}            
				if(!"".equals(selectedImagePathforpaper))
				{
				Bitmap bitmap2 = BitmapFactory.decodeStream(
						new FileInputStream(selectedImagePathforpaper), null,
						o2);
				
				if(bitmap2==null)
				{
					selectedImagePathforpaper="";
					cf.ShowToast("This image is not a supported format. You cannot u",1);
					papersignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
					BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
					papersignimg.setImageDrawable(bmd2);
				}
				}
			}

		} catch (FileNotFoundException e) {
		}

	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private void btnshowcommon() {
		// TODO Auto-generated method stub
		signimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		frntimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		bacimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		rgtimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		lftimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		atcimg.setBackgroundResource(R.drawable.submenusrepeatnor);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(ImagesData.this, InspectionList.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/*@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		System.out.println("coalled he stop");
		finish();
		super.onStop();
	}*/
	
}
