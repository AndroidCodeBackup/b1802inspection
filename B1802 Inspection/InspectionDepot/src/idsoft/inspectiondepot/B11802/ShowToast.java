package idsoft.inspectiondepot.B11802;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

public class ShowToast {
	public ShowToast(Context c, String s) {
		Toast toast = Toast.makeText(c, s, Toast.LENGTH_SHORT);
		TextView tv = (TextView) toast.getView().findViewById(
				android.R.id.message);
		tv.setTextColor(Color.YELLOW);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

}
