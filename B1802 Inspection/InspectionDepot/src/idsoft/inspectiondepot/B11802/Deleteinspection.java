package idsoft.inspectiondepot.B11802;

import java.util.ArrayList;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView; 
import android.widget.TextView;
import android.widget.Toast;

public class Deleteinspection extends Activity {
	View v;
	private TextView welcome, title;
	ArrayList<String> ExportList = new ArrayList<String>();
	private ListView lv1;
	TextView delrec;String s = " ";
	int val = 0,rws;
	ScrollView sv;
	String strtit,res = "",inspdata;
	String[] homelist = new String[200];
	String[] deletelist = new String[200];
	private Button homepage;
	public Button search;
	public EditText search_text;
	public Button search_clear_txt,deleteallinspections;
	LinearLayout onlinspectionlist;
	TextView tvstatus[];
	Button deletebtn[];
	public String[] data,countarr;
	CommonFunctions cf;
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deleteinspection);
		cf = new CommonFunctions(this);
		cf.getInspectorId();
		cf.welcomemessage();
		
		deleteallinspections = (Button)this.findViewById(R.id.deleteallinspections);
		onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		((TextView)findViewById(R.id.welcomename)).setText(Html.fromHtml("<font color=#f7a218>" + "Welcome : "+"</font>"+ cf.data));		
		title = (TextView) this.findViewById(R.id.deleteiinformation);
		
		try {
			cf.Createtablefunction(2);
			Cursor c = cf.SelectTablefunction(cf.mbtblInspectionList,
					"where InspectorId='" + cf.InspectorId + "'");
			int rws = c.getCount();
			if(rws==0)
			{
				deleteallinspections.setVisibility(v.GONE);
			}
			else
			{
				deleteallinspections.setVisibility(v.VISIBLE);
			c.moveToFirst();
			if (c.getString(c.getColumnIndex("InspectionTypeId")).equals("28")) {
				strtit = "B1 - 1802 (Rev. 01/12) ";
			} else {
				strtit = "B1 - 1802 (Rev. 01/12) Carr.Ord.";
			}
			}
		} catch (Exception e) {
			System.out.println("eror " + e);

		}
		this.search = (Button) this.findViewById(R.id.search);
		search_text = (EditText) findViewById(R.id.search_text);
		this.search_clear_txt = (Button) this.findViewById(R.id.search_clear_txt);

		this.search_clear_txt.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				search_text.setText("");
				res = "";
				dbquery();
			}
		});
		
		this.search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String temp = cf.convertsinglequotes(search_text.getText()
						.toString());
				if (temp.equals("")) {
					cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
					search_text.requestFocus();
				} else {
					res = temp;
					dbquery();
				}
			}
		});
		dbquery();
	}
	
	private void dbquery() {
		data = null;
		inspdata = "";
		countarr = null;
		rws = 0;
		
		String sql = "select * from " + cf.mbtblInspectionList
				+ " where InspectorId = '" + cf.InspectorId + "' and Status!='90'";

		if (!res.equals("")) {
			sql += " and (s_OwnersNameFirst like '%" + res
					+ "%' or s_OwnersNameLast like '%" + res
					+ "%' or s_OwnerPolicyNumber like '%" + res + "%')";
		}
		
		Cursor cur = cf.db.rawQuery(sql, null);
		rws = cur.getCount();
		
		title.setText("Total Record : " + rws);
		data = new String[rws];
		countarr = new String[rws];
		int j = 0;
		cur.moveToFirst();
		if (cur.getCount() >= 1) {

			do {
				
				/*if (cur.getString(cur.getColumnIndex("IsUploaded"))
						.equals("0")) {*/
					
					data[j]= " "+ cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnersNameFirst")))
									+ " ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("s_OwnersNameLast"))) + " | ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("s_OwnerPolicyNumber")))
									+ " \n ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("s_propertyAddress"))) + " | ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("s_City"))) + " | ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("s_State"))) + " | ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("s_County"))) + " | ";
					data[j] += cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_ZipCode")))
									+ " \n ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("ScheduleDate"))) + " | ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InspectionStartTime"))) + " - ";
					data[j] += cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InspectionEndTime"))) + " | ";
					data[j] += cf.getsinglequotes(cur.getString(cur.getColumnIndex("Status")));
							if (data[j].contains("| 90")) {
								data[j] =data[j].replace("| 90", "| Cancelled Inspection");
							}
							if (data[j].contains("| 110")) {
								data[j] = data[j].replace("| 110", "| Unable to Scheduled");
							}
							if (data[j].contains("| 40")) {
								String ss = cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("SubStatus")));
								if (!ss.equals("0")) {
									data[j] += ss;
									if (data[j].contains("| 4041")) {
										data[j] = data[j].replace("| 4041",
												"| Completed Inspections in Online");
									}
								} else {
									data[j] = data[j].replace("| 40", "| Schedule");
								}
							}
							if (data[j].contains("| 30")) {
								data[j] = data[j].replace("| 30", "| Assign");
							}
							countarr[j] = cur.getString(1);
							

							if (data[j].contains("null")) {
								data[j]=data[j].replace("null", "");
							}
							if (data[j].contains("N/A | ")) {
								data[j] = data[j].replace("N/A |", "");
							}
							if (data[j].contains("N/A - N/A")) {
								data[j] = data[j].replace("N/A - N/A", "");
							}
												
					j++;
				
					
					
			//	}
			} while (cur.moveToNext());
			search_text.setText("");
			display();
		} else {
			
			onlinspectionlist.removeAllViews();
			if(res.equals(""))
			{
				cf.gohome();
			}
			else
			{
				cf.ShowToast("Sorry, No results found.", 1);cf.hidekeyboard();
			}
			
		}

	}
	
	private void display() {
		
		onlinspectionlist.removeAllViews();
		sv = new ScrollView(this);
		onlinspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		if (data!=null && data.length>0) {
			
			for (int i = 0; i < data.length; i++) {
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				l2.setLayoutParams(mainparamschk);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
				tvstatus[i] = new TextView(this);
				System.out.println("displaydata"+data[i]); 
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

			    lchkbox.addView(tvstatus[i], paramschk);

				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
				ldelbtn.setLayoutParams(mainparamschk);
				ldelbtn.setGravity(Gravity.RIGHT);
			    l2.addView(ldelbtn);
				deletebtn[i] = new Button(this);
				deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
				deletebtn[i].setTag("deletebtn" + i);
				deletebtn[i].setPadding(30, 0, 0, 0);
				ldelbtn.addView(deletebtn[i], paramsdelbtn);
				deletebtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();    
						final String repidofselbtn = getidofselbtn.replace(
								"deletebtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						final String dt = countarr[cvrtstr];
						 cf.alerttitle="Delete";
						  cf.alertcontent="Are you sure want to delete?";
						    final Dialog dialog1 = new Dialog(Deleteinspection.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText( cf.alerttitle);
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml( cf.alertcontent));
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							btn_yes.setOnClickListener(new OnClickListener()
							{
			                	public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									cf.fn_delete(dt);
									startActivity(new Intent(Deleteinspection.this,Deleteinspection.class));
									
								}
								
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{

								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									
								}
								
							});
							dialog1.setCancelable(false);
							dialog1.show();
					
							
					} 
				});
			}
		}
		else
		{
		
		}

	}

	public void clicker(View v) {
		switch (v.getId()) {
	
		case R.id.deleteallinspections:			
			 cf.delete_all("","Are you sure want to delete all the inspections in B1-1802?");
			break;
			
		case R.id.deletehome:
			cf.gohome();
			break;
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Deleteinspection.this,HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
