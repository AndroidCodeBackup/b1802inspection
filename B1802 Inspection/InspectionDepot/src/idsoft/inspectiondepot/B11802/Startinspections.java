package idsoft.inspectiondepot.B11802;

import java.io.InputStream;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Startinspections extends Activity implements Runnable {
	int assign = 0, schedule = 0, exported = 0, total = 0, assigntype16 = 0,
			scheduletype16 = 0, exportedtype16 = 0, totaltype16 = 0,reportsready=0,reportsready16=0,
			typ16pre = 0, typ16uts = 0, typ16can = 0,typ16rr = 0, value = 0, pre = 0,
			onassign = 0, onschedule = 0, onpre = 0, ontotal = 0,onrr=0,
			onassigntype16 = 0, onscheduletype16 = 0, ontyppre = 0,
			ontotaltype16 = 0, uts = 0, can = 0,rr=0, onuts = 0, oncan = 0,
			ontyputs = 0,ontyprr = 0, ontypcan = 0;
	private Button wsiassign, wsischedule, wsiexported, wsipre, wsiuts, wsican,wsirr,
			wsitotal, type16assign, type16schedule, type16exported, type16pre,
			type16uts, type16can,type16rr, type16total, onwsiassign, onwsischedule,
			onwsipre, onwsiuts, onwsican, onwsirr,onwsitotal, ontype16assign,
			ontype16schedule, ontype16pre, ontype16uts, ontype16can,ontype16rr,
			ontype16total;
	private TextView head, welcome, compinsptab, compinsponline,
			compinsonlinepre, txtexplain;
	private Button home, online;
	String inspectorid, inspectiontypeid, status;
	protected static final int visibility = 0;
	private static final String TAG = null;
	ProgressDialog pd;
	int ichk, k,v;
	CommonFunctions cf;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getInt("keyName");
		}
		cf = new CommonFunctions(this);
		setContentView(R.layout.startinspection);
		
		cf.Createtablefunction(1);
		cf.getInspectorId();
		cf.welcomemessage();
		this.welcome = (TextView) this.findViewById(R.id.welcomename);System.out.println("wel");
		welcome.setText(cf.data);
		
		this.wsiassign = (Button) this.findViewById(R.id.wsiassign);
		this.wsischedule = (Button) this.findViewById(R.id.wsisschedule);
		this.wsiexported = (Button) this.findViewById(R.id.wsiexported);
		this.wsipre = (Button) this.findViewById(R.id.wsipre);
		this.wsiuts = (Button) this.findViewById(R.id.wsiunable);
		this.wsican = (Button) this.findViewById(R.id.wsican);
		this.wsirr = (Button) this.findViewById(R.id.wsirr);
		this.wsitotal = (Button) this.findViewById(R.id.wsitotal);System.out.println("tot");
		
		
		this.type16assign = (Button) this.findViewById(R.id.type16assign);
		this.type16schedule = (Button) this.findViewById(R.id.type16schedule);
		this.type16exported = (Button) this.findViewById(R.id.type16exported);
		this.type16pre = (Button) this.findViewById(R.id.type16pre);
		this.type16uts = (Button) this.findViewById(R.id.type16unable);
		this.type16can = (Button) this.findViewById(R.id.type16cancel);
		this.type16rr = (Button) this.findViewById(R.id.type16rr);
		this.type16total = (Button) this.findViewById(R.id.type16total);

		this.home = (Button) this.findViewById(R.id.home);System.out.println("home");

		this.compinsptab = (TextView) this.findViewById(R.id.txtcompinsptab);System.out.println("txtcomp");
		this.compinsponline = (TextView) this
				.findViewById(R.id.txtcompinsponline);System.out.println("txtcompinsponline");
		this.compinsonlinepre = (TextView) this.findViewById(R.id.ontxtpre);System.out.println("ontxtpre");
		this.txtexplain = (TextView) this
				.findViewById(R.id.txtbriefexplanation);System.out.println("txtbriefexplanation");
				System.out.println("txtexplain");
		txtexplain.setText(Html
				.fromHtml("1. CIT - Completed Inspections in Tablet " + "<br/>"
						+ "2. CIO - Completed Inspections in Online" + "<br/>"
						+ "3. UTS - Unable to Schedule " + "<br/>"
						+ "4. Can - Cancelled Inspection " + "<br/>"
						+ "5. RR - Reports Ready" + "<br/>"));

		compinsptab.setText(Html.fromHtml("CIT" + "<font color=red> * "
				+ "</font>"));
		compinsponline.setText(Html.fromHtml("CIO" + "<font color=red> * "
				+ "</font>"));
		compinsonlinepre.setText(Html.fromHtml("CIO" + "<font color=red> * "
				+ "</font>"));
		//System.out.println("println");
		this.online = (Button) this.findViewById(R.id.online);//System.out.println("online");
		schedulestatus();//System.out.println("schedulestatus");

		wsiassign.setText(String.valueOf(assign));
		wsischedule.setText(String.valueOf(schedule));
		wsiexported.setText(String.valueOf(exported));
		wsipre.setText(String.valueOf(pre));
		wsiuts.setText(String.valueOf(uts));
		wsican.setText(String.valueOf(can));
		wsirr.setText(String.valueOf(reportsready));
		wsitotal.setText(String.valueOf(total));System.out.println("schedul1111estatus");

		type16assign.setText(String.valueOf(assigntype16));
		type16schedule.setText(String.valueOf(scheduletype16));
		type16exported.setText(String.valueOf(exportedtype16));
		type16pre.setText(String.valueOf(typ16pre));
		type16uts.setText(String.valueOf(typ16uts));
		type16can.setText(String.valueOf(typ16can));
		type16rr.setText(String.valueOf(reportsready16));
		type16total.setText(String.valueOf(totaltype16));System.out.println("2222");
		
		onwsiassign = (Button) findViewById(R.id.onwsiassign);
		onwsischedule = (Button) findViewById(R.id.onwsisschedule);
		onwsipre = (Button) findViewById(R.id.onwsipre);
		onwsiuts = (Button) findViewById(R.id.onwsiuts);
		onwsican = (Button) findViewById(R.id.onwsican);
		onwsirr = (Button) findViewById(R.id.onwsirr);
		onwsitotal = (Button) findViewById(R.id.onwsitotal);System.out.println("3333");

		ontype16assign = (Button) findViewById(R.id.ontype16assign);
		ontype16schedule = (Button) findViewById(R.id.ontype16schedule);
		ontype16pre = (Button) findViewById(R.id.ontype16pre);
		ontype16uts = (Button) findViewById(R.id.ontype16uts);
		ontype16can = (Button) findViewById(R.id.ontype16can);
		ontype16rr = (Button) findViewById(R.id.ontype16rr);
		ontype16total = (Button) findViewById(R.id.ontype16total);System.out.println("444");
		this.wsiassign.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if (assign != 0) {
					Intent intent = new Intent(Startinspections.this,
							InspectionList.class);
					intent.putExtra("InspectionType", "29");
					intent.putExtra("Count", assign);
					intent.putExtra("status", "assign");
					intent.putExtra("keyName", value);
					startActivity(intent);
				} else {
					cf.ShowToast("Sorry, No records found for Assign status.",1);

				}

			}
		});

		this.wsischedule.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				if (schedule != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							InspectionList.class);
					iInspectionList.putExtra("InspectionType", "29");
					iInspectionList.putExtra("Count", schedule);
					iInspectionList.putExtra("status", "schedule");
					iInspectionList.putExtra("keyName", value);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Schedule status.",1);

				}

			}
		});

		this.wsiexported.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (exported != 0) {

					Intent iInspectionList = new Intent(Startinspections.this,
							InspectionList.class);
					iInspectionList.putExtra("InspectionType", "29");
					iInspectionList.putExtra("Count", schedule);
					iInspectionList.putExtra("status", "inspected");
					iInspectionList.putExtra("keyName", value);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for CIT status.",1);

				}
			}
		});
		this.wsipre.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (pre != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "preonl");
					iInspectionList.putExtra("InspectionType", "29");
					iInspectionList.putExtra("Total",pre);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for CIO status.",1);
				}
			}
		});
		this.wsiuts.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (uts != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "uts");
					iInspectionList.putExtra("InspectionType", "29");
					iInspectionList.putExtra("Total",uts);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for UTS status.",1);

				}
			}
		});
		this.wsican.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (can != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "can");
					iInspectionList.putExtra("InspectionType", "29");
					iInspectionList.putExtra("Total",can);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Cancelled status.",1);

				}
			}
		});
		this.wsirr.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (reportsready != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "rr");
					iInspectionList.putExtra("InspectionType", "29");
					iInspectionList.putExtra("Total",reportsready);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Reports Ready status.",1);

				}
			}
		});
		this.wsitotal.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				cf.ShowToast("Total No. of Records : " + total,1);

			}
		});

		this.type16assign.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (assigntype16 != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							InspectionList.class);
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Count", assigntype16);
					iInspectionList.putExtra("status", "assign");
					iInspectionList.putExtra("keyName", value);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Assign status.",1);
				}

			}
		});
		this.type16schedule.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				if (scheduletype16 != 0) {
					
					Intent iInspectionList = new Intent(Startinspections.this,
							InspectionList.class);
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Count", scheduletype16);
					iInspectionList.putExtra("status", "schedule");
					iInspectionList.putExtra("keyName", 2);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Schedule status.",1);
				}

			}
		});
		this.type16exported.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (exportedtype16 != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							InspectionList.class);
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Count", scheduletype16);
					iInspectionList.putExtra("status", "inspected");
					iInspectionList.putExtra("keyName", 3);
					startActivity(iInspectionList);
				} else {

					cf.ShowToast("Sorry, no records found for CIT status.",1);
				}
			}
		});
		this.type16pre.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (typ16pre != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "preonl");
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Total",typ16pre);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {

					cf.ShowToast("Sorry, No records found for CIO status.",1);
				}
			}
		});
		this.type16uts.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (typ16uts != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "uts");
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Total",typ16uts);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for UTS status.",1);

				}
			}
		});
		this.type16rr.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (reportsready16 != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "rr");
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Total",reportsready16);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Reports Ready status.",1);

				}
			}
		});
		this.type16can.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				if (typ16can != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "can");
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Total",typ16can);
					iInspectionList.putExtra("Online", false);
					startActivity(iInspectionList);
				} else {

					cf.ShowToast("Sorry, No records found for Cancelled status.",1);
				}
			}
		});

		this.type16total.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {

				cf.ShowToast("Total No. of Records : " + totaltype16,1);
			}
		});
		this.online.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				cf.alerttitle="Synchronization"; 
				  cf.alertcontent="Are you sure want to Syncronize with Online?";
				    final Dialog dialog1 = new Dialog(Startinspections.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alertsync);
					TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
					txttitle.setText( cf.alerttitle);
					TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
					txt.setText(Html.fromHtml( cf.alertcontent));
					Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
					Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
					btn_yes.setOnClickListener(new OnClickListener()
					{
	                	public void onClick(View arg0) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							if(cf.isInternetOn()==true)
							{
	                           String source = "<font color=#FFFFFF>Loading data. Please wait..."
										+ "</font>";
								cf.pd = ProgressDialog.show(Startinspections.this,"", Html.fromHtml(source), true);
								Thread thread = new Thread(Startinspections.this);
								thread.start();
							}
							else
							{
								cf.ShowToast("Internet connection is not available.",1);
							}
						}
						
					});
					btn_cancel.setOnClickListener(new OnClickListener()
					{

						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							
						}
						
					});
					dialog1.setCancelable(false);
					dialog1.show();
			}
		});
		this.home.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				cf.gohome();
			}
		});

	}

	private void retrieveonlinedata(SoapObject result) {
		
		int Cnt = result.getPropertyCount();
		System.out.println("Cnt "+Cnt);
		if (Cnt >= 1) {
			SoapObject obj = (SoapObject) result;

			for (int i = 0; i < obj.getPropertyCount(); i++) {
				SoapObject obj1 = (SoapObject) obj.getProperty(i);
				if (!obj1.getProperty("i_maininspectiontype").toString()
						.equals("")) {

					if (29 == Integer.parseInt(obj1.getProperty(
							"i_maininspectiontype").toString())) {
						onassign = Integer.parseInt(obj1.getProperty("Assign")
								.toString());
						onschedule = Integer.parseInt(obj1.getProperty("Sch")
								.toString());
						onpre = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						onuts = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						oncan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						ontotal = Integer.parseInt(obj1.getProperty("Total")
								.toString());
						
						onrr =Integer.parseInt(obj1.getProperty("Completed")
														.toString());
						
                         
					}

					if (28 == Integer.parseInt(obj1.getProperty(
							"i_maininspectiontype").toString())) {
						onassigntype16 = Integer.parseInt(obj1.getProperty(
								"Assign").toString());
						onscheduletype16 = Integer.parseInt(obj1.getProperty(
								"Sch").toString());
						ontyppre = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						ontyputs = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						ontypcan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						ontotaltype16 = Integer.parseInt(obj1.getProperty(
								"Total").toString());
						
						ontyprr =  Integer.parseInt(obj1.getProperty("Completed")
														.toString());
						
					}

				}
			}
		}
		this.onwsiassign.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				 cf.MoveTo_OnlineList("29",onassign,"30","0","Assigned Record","0");
				
			}
		});

		this.ontype16assign.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				 cf.MoveTo_OnlineList("28",onassigntype16,"30","0","Assigned Record","0");
				
			}
		});
		this.onwsischedule.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				 cf.MoveTo_OnlineList("29",onschedule,"40","0","Schedule","0");
				
			}
		});

		this.ontype16schedule.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				 cf.MoveTo_OnlineList("28",onscheduletype16,"40","0","Schedule","0");
				
			}
		});
		this.onwsipre.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				// cf.MoveTo_OnlineList("29",onpre,"40","41","Completed in Online","0");
				if (onpre != 0) {
				Intent iInspectionList = new Intent(Startinspections.this,
						List_view_limit.class);
				iInspectionList.putExtra("id", "preonl");
				iInspectionList.putExtra("InspectionType", "29");
				iInspectionList.putExtra("Online", true);
				iInspectionList.putExtra("Total",onpre);
				startActivity(iInspectionList);
			} else {

				cf.ShowToast("Sorry, No records found for CIO status.",1);
			}
			}
		});

		this.ontype16pre.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
			//	cf.MoveTo_OnlineList("28",ontyppre,"40","41","Completed in Online","0");
				if (ontyppre != 0) {
				Intent iInspectionList = new Intent(Startinspections.this,
						List_view_limit.class);
				iInspectionList.putExtra("id", "preonl");
				iInspectionList.putExtra("InspectionType", "28");
				iInspectionList.putExtra("Online", true);
				iInspectionList.putExtra("Total",ontyppre);
				startActivity(iInspectionList);
			} else {

				cf.ShowToast("Sorry, No records found for CIO status.",1);
			}
			}
		});
		this.onwsiuts.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				//cf.MoveTo_OnlineList("29",onuts,"110","111","Unable to Schedule Inspection","0");
				if(onuts!=0)
				{
				Intent iInspectionList = new Intent(Startinspections.this,
						List_view_limit.class);
				iInspectionList.putExtra("id", "uts");
				iInspectionList.putExtra("InspectionType", "29");
				iInspectionList.putExtra("Online", true);
				iInspectionList.putExtra("Total",onuts);
				startActivity(iInspectionList);
			} else {
				cf.ShowToast("Sorry, No records found for UTS status.",1);

			}
			}
		});

		this.ontype16uts.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
			//	cf.MoveTo_OnlineList("28",ontyputs,"110","111","Unable to Schedule Inspection","0");
				if(ontyputs!=0)
				{
				Intent iInspectionList = new Intent(Startinspections.this,
						List_view_limit.class);
				iInspectionList.putExtra("id", "uts");
				iInspectionList.putExtra("InspectionType", "28");
				iInspectionList.putExtra("Online", true);
				iInspectionList.putExtra("Total",ontyputs);
				startActivity(iInspectionList);
			} else {
				cf.ShowToast("Sorry, No records found for UTS status.",1);

			}
			}
		});
		this.onwsican.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				//cf.MoveTo_OnlineList("29",oncan,"90","0","Cancelled","0");
				if(oncan!=0)
				{
					Intent iInspectionList = new Intent(Startinspections.this,
						List_view_limit.class);
				iInspectionList.putExtra("id", "can");
				iInspectionList.putExtra("InspectionType", "29");
				iInspectionList.putExtra("Online", true);
				iInspectionList.putExtra("Total",oncan);
				startActivity(iInspectionList);
			} else {

				cf.ShowToast("Sorry, No records found for Cancelled status.",1);
			}
				
			}
		});
		this.ontype16can.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				//cf.MoveTo_OnlineList("28",ontypcan,"90","0","Cancellation","0");
				if(ontypcan!=0)
				{
				Intent iInspectionList = new Intent(Startinspections.this,
						List_view_limit.class);
				iInspectionList.putExtra("id", "can");
				iInspectionList.putExtra("InspectionType", "28");
				iInspectionList.putExtra("Online", true);
				iInspectionList.putExtra("Total",ontypcan);
				startActivity(iInspectionList);
			} else {

				cf.ShowToast("Sorry, No records found for Cancelled status.",1);
			}
			}
		});

		
		this.onwsirr.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				//cf.MoveTo_OnlineList("29",onrr,"2","0","Reports Ready","1");	
				if (onrr != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "rr");
					iInspectionList.putExtra("InspectionType", "29");
					
					iInspectionList.putExtra("Online", true);
					iInspectionList.putExtra("Total",onrr);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Reports Ready status.",1);

				}
			}
		});
		this.ontype16rr.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				//cf.MoveTo_OnlineList("28",ontyprr,"2","0","Reports Ready","1");
				if (ontyprr != 0) {
					Intent iInspectionList = new Intent(Startinspections.this,
							List_view_limit.class);
					iInspectionList.putExtra("id", "rr");
					iInspectionList.putExtra("InspectionType", "28");
					iInspectionList.putExtra("Total",ontyprr);
					iInspectionList.putExtra("Online", true);
					startActivity(iInspectionList);
				} else {
					cf.ShowToast("Sorry, No records found for Reports Ready status.",1);

				}
			}
		});

		this.onwsitotal.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
			
				 if(String.valueOf(ontotaltype16).equals("0"))
				  {
					  cf.ShowToast("Sorry, No records found for Inspection Depot.",0);
				  }
				  else
				  {
					  cf.ShowToast("Total No. of Records : " + ontotal, 1);
				  }
			}
		});
		this.ontype16total.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				 if(String.valueOf(ontotal).equals("0"))
				  {
					  cf.ShowToast("Sorry, No records found for Inspection Depot.",0);
				  }
				  else
				  {
					  cf.ShowToast("Total No. of Records : " + ontotaltype16, 1);
				  }
			}
		});
		
		
		
		
	}

	private void schedulestatus() {
		try {
			try {
				Cursor cur = cf.SelectTablefunction(cf.mbtblInspectionList,
						" where InspectorId='" + cf.InspectorId + "'");
				int Column1 = cur.getColumnIndex("InspectorId");
				int Column2 = cur.getColumnIndex("InspectionTypeId");
				int Column3 = cur.getColumnIndex("Status");
				int Column4 = cur.getColumnIndex("IsInspected");
				int Column5 = cur.getColumnIndex("IsUploaded");
				int Column6 = cur.getColumnIndex("SubStatus");

				cur.moveToFirst();
				if (cur != null) {
					do {
						String tbl_InspList_InspId = cur.getString(Column1)
								.toString();
						String tbl_InspList_TypeId = cur.getString(Column2)
								.toString();
						String tbl_InspList_Status = cur.getString(Column3)
								.toString();
						String tbl_InspList_Inspected = cur.getString(Column4)
								.toString();
						String tbl_InspList_Uploaded = cur.getString(Column5)
								.toString();
						String tbl_InspList_SubStatus = cur.getString(Column6)
								.toString();
						if (cf.InspectorId.equals(tbl_InspList_InspId)
								&& "29".equals(tbl_InspList_TypeId)) {
							if (tbl_InspList_Status.equals("40") && tbl_InspList_SubStatus.equals("0")) {
								schedule++;
							}
							if (tbl_InspList_Status.equals("30")) {
								assign++;
							}
							if (tbl_InspList_SubStatus.equals("41")) {
								pre++;
							}
							if (tbl_InspList_Status.equals("110")) {
								uts++;
							}
							if (tbl_InspList_Status.equals("90")) {
								can++;
							}
							if (tbl_InspList_Inspected.equals("1")) {
								exported++;
							}
							//System.out.println("test="+cur.getString(cur.getColumnIndex("s_OwnersNameFirst"))+"tbl_InspList_Status="+tbl_InspList_Status+"tbl_InspList_SubStatus="+tbl_InspList_SubStatus);
							if((tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("0"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("0"))
									|| (tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("121"))
									|| (tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("122"))
									|| (tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("123"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("151"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("153"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("152"))
									|| (tbl_InspList_Status.equals("70") && tbl_InspList_SubStatus.equals("71"))
									|| (tbl_InspList_Status.equals("70"))
									|| (tbl_InspList_Status.equals("140"))) 
							{
									reportsready++;
							}
						}
						if (cf.InspectorId.equals(tbl_InspList_InspId)
								&& "28".equals(tbl_InspList_TypeId)) {
							if (tbl_InspList_Status.equals("40")
									&& tbl_InspList_SubStatus.equals("0")) {
								scheduletype16++;
							}
							if (tbl_InspList_Status.equals("30")) {
								assigntype16++;
							}
							if (tbl_InspList_SubStatus.equals("41")) {
								typ16pre++;
							}
							if (tbl_InspList_Status.equals("110")) {
								typ16uts++;
							}
							if (tbl_InspList_Status.equals("90")) {
								typ16can++;
							}
							if (tbl_InspList_Inspected.equals("1")) {
								exportedtype16++;
							}
							if((tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("0"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("0"))
									|| (tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("121"))
									|| (tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("122"))
									|| (tbl_InspList_Status.equals("2") && tbl_InspList_SubStatus.equals("123"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("151"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("153"))
									|| (tbl_InspList_Status.equals("5") && tbl_InspList_SubStatus.equals("152"))
									|| (tbl_InspList_Status.equals("70") && tbl_InspList_SubStatus.equals("71"))
									|| (tbl_InspList_Status.equals("70"))
									|| (tbl_InspList_Status.equals("140"))) 
							{
									reportsready16++;
							}
						}

					} while (cur.moveToNext());
				}
				cur.close();
			} catch (Exception e) {

			}

			try
			{
				Cursor c=cf.SelectTablefunction(cf.count_tbl, " WHERE insp_id='"+cf.InspectorId+"'");
				c.moveToFirst();
				if(c.getCount()>0)
				{
					pre=c.getInt(c.getColumnIndex("C_29_pre"));
					uts=c.getInt(c.getColumnIndex("C_29_uts"));
					can=c.getInt(c.getColumnIndex("C_29_can"));
					reportsready=c.getInt(c.getColumnIndex("C_29_RR"));
					typ16pre=c.getInt(c.getColumnIndex("C_28_pre"));
					typ16uts=c.getInt(c.getColumnIndex("C_28_uts"));
					typ16can=c.getInt(c.getColumnIndex("C_28_can"));
					reportsready16=c.getInt(c.getColumnIndex("C_28_RR"));
					
					//cf.db.execSQL(" UPDATE "+cf.count_tbl+" SET (C_28_pre='"+ontyppre+"',C_28_uts='"+ontyputs+"',C_28_can='"+ontypcan+"',C_28_RR='"+ontyprr+"',C_29_pre='"+onpre+"',C_29_uts='"+onuts+"',C_29_can='"+oncan+"',C_29_RR='"+onrr+"')");
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			total = schedule + assign + exported + pre + uts + can + reportsready;

			totaltype16 = scheduletype16 + assigntype16 + exportedtype16
					+ typ16pre + typ16uts + reportsready16  + typ16can;

		} catch (Exception e) {

		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(Startinspections.this, HomeScreen.class);
			startActivity(intent);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void run() {
		// TODO Auto-generated method stub
		try {
			
			 cf.onlresult=cf.Calling_WS1(cf.InspectorId, "UpdateMobileDBCount");System.out.println("RESS "+cf.onlresult);
			 retrieveonlinedata(cf.onlresult);
             k=0;
           
		} catch (Exception e) {
          k=1;
		}
		 handler.sendEmptyMessage(0);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			cf.pd.dismiss();

			if (k == 1) {
				 cf.ShowToast("There is a problem in the server. Please contact paperless admin.", 1);

			} else if (k == 0) {

				if (onassign != 0) {
					onwsiassign.setText(String.valueOf(onassign));

				} else {
					onwsiassign.setText("0");
				}
				if (onschedule != 0) {
					onwsischedule.setText(String.valueOf(onschedule));
				} else {
					onwsischedule.setText("0");
				}
				if (ontotal != 0) {
					onwsitotal.setText(String.valueOf(ontotal));
				} else {
					onwsitotal.setText("0");
				}
				if (onuts != 0) {
					onwsiuts.setText(String.valueOf(onuts));
				} else {
					onwsiuts.setText("0");
				}
				if (oncan != 0) {
					onwsican.setText(String.valueOf(oncan));
				} else {
					onwsican.setText("0");
				}
				if (onrr != 0) {
					onwsirr.setText(String.valueOf(onrr));
				} else {
					onwsirr.setText("0");
				}
				
				if (onpre != 0) {
					onwsipre.setText(String.valueOf(onpre));
				} else {
					onwsipre.setText("0");
				}

				if (onassigntype16 != 0) {
					ontype16assign.setText(String.valueOf(onassigntype16));

				} else {
					ontype16assign.setText("0");
				}
				if (onscheduletype16 != 0) {
					ontype16schedule.setText(String.valueOf(onscheduletype16));
				} else {
					ontype16schedule.setText("0");
				}
				if (ontotaltype16 != 0) {
					ontype16total.setText(String.valueOf(ontotaltype16));
				} else {
					ontype16total.setText("0");
				}
				if (ontyputs != 0) {
					ontype16uts.setText(String.valueOf(ontyputs));
				} else {
					ontype16uts.setText("0");
				}
				if (ontypcan != 0) {
					ontype16can.setText(String.valueOf(ontypcan));
				} else {
					ontype16can.setText("0");
				}
				if (ontyprr != 0) {
					ontype16rr.setText(String.valueOf(ontyprr));
				} else {
					ontype16rr.setText("0");
				}
				if (ontyppre != 0) {
					ontype16pre.setText(String.valueOf(ontyppre));
				} else {
					ontype16pre.setText("0");
				}

			}

		}
	};
}