package idsoft.inspectiondepot.B11802;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AdditionalService extends Activity {
	String homeid, InspectionType, status;View v1;TableRow noinformation;
	TextView contactemail,phonenumber,mobilenumber,bsettimetocallu,firstchoice,secondchoice,thirdchoice,feedbackcomments; 
	int value, Count;
	public CommonFunctions cf;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(2);
		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		setContentView(R.layout.additionalservice);
		contactemail = (TextView) this.findViewById(R.id.valcontactemail);
		phonenumber = (TextView) this.findViewById(R.id.valphonenumber);
		mobilenumber = (TextView) this.findViewById(R.id.valmobilenumber);
		bsettimetocallu = (TextView) this.findViewById(R.id.valbesttimetocallu);
		firstchoice = (TextView) this.findViewById(R.id.valtxtfirstchoice);
		secondchoice = (TextView) this.findViewById(R.id.valsecondchoice);
		thirdchoice = (TextView) this.findViewById(R.id.valthirdchoice);
		feedbackcomments = (TextView) this.findViewById(R.id.valcomments);
		noinformation = (TableRow) this.findViewById(R.id.noinformation);
		
		
		cf.getDeviceDimensions();
		cf.getInspectorId();
		cf.getinspectiondate();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 1, cf, 0));
		/** Ph submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 16, cf, 1));
		
		
		try {
			Cursor cur = cf.SelectTablefunction(cf.additionalservice,"where SRID='" + cf.Homeid + "'");
			if(cur.getCount()>0)
			{
					cur.moveToFirst();
					contactemail.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("ContactEmail"))));
					phonenumber.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("PhoneNumber"))));
					mobilenumber.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("MobileNumber"))));
					bsettimetocallu.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("BestTimetoCallYou"))));
					firstchoice.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("FirstChoice"))));
					secondchoice.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("SecondChoice"))));
					thirdchoice.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("ThirdChoice"))));
					feedbackcomments.setText(cf.getsinglequotes(cur.getString(cur.getColumnIndex("FeedbackComments"))));
					noinformation.setVisibility(v1.GONE);
			}
			else
			{
				noinformation.setVisibility(v1.VISIBLE);
			}
			
		} catch (Exception e) {
			System.out.println(" errrr " + e.getMessage());
		}
		
	}
	
	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.hme:
			cf.gohome();
			break;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(AdditionalService.this, AdditionalInfo.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
					
				}
				
				break;

		}

	}
}
