package idsoft.inspectiondepot.B11802;

import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class ToaddComment extends Activity {
	int insertchk=0,instypeupdate=0,statuschkdeactivate = 0, statuschkactivate = 0, iden = 0,inspectedPos, inspectionType = 0, commvalue, quesid, i = 0, chkflag, updatecnt, spn2pos;
	Spinner spn1, spn2, spninspec;
	ArrayAdapter adapter, adapterinsp, adapter2;
	CheckBox chkall, temp_st;
	CheckBox cb[];
	Button[] editbtn, deletebtn;
	Button activate, deactivate, home, viewcomments, addcomments, back;
	TextView tvstatus[];
	TextView nocomments, welcome;
	String arruncheckdescp[], arrstatus[], arrcomment1[], arrquesordid[],
			arrdescription[], arrdesc[];
	String descvalue1, quesord, actstatus, optionvalue, strdesc, conchkbox,
			strquestion, selcatid, inspectorid, flag1, desccomm, key1,
			repidofselbtn;
	LinearLayout lnrlayouttype, lnrlayoutoption, lnrlayoutsuboption,lnrlayoutaddview,lnrlayout, txtheader;
	boolean strcount;
	View v1;
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();
	String optionselect[] = { "Select" };
	
	String inspectiontype[] = { "Select", "B1 - 1802 (Rev. 01/12)",
			"B1 - 1802 (Rev. 01/12) - Carrier Ordered" };
	/*String questiontype[] = { "Select Question Type", "Building Code",
			"Roof Covering", "Roof Deck Attachment", "Roof to Wall Attachment",
			"Roof Geometry", "Secondary Water Resistance(SWR)",
			"Opening Protection", "Wall Construction(Addendum)" };*/
	String buildcodeoption[] = { "Select", "Meets 2001 FBC", "Meets SFBC -94",
			"Unknown - Does Not Meet" };
	String roofcoveroption[] = { "Select",
			"Meets FBC(PD -03/2/02) or Original (after 2004)",
			"Meets SFBC (PD 09/01/1994) or Post 1997",
			"One or more does not meet A or B", "None Meet A or B" };
	String roofdeckoption[] = { "Select", "Mean Uplift less than B and C",
			"103 PSF", "182 PSF", "Concrete", "Other", "Unknown", "No Attic" };
	String roofwalloption[] = { "Select", "Toe Nail", "Clips", "Single Wraps",
			"Double Wraps", "Structural", "Other", "Unknown", "No Attic" };
	String roofgeometryoption[] = { "Select", "Hip Roof", "Flat Roof",
			"Other Roof Type" };
	String swroption[] = { "Select", "SWR", "No SWR", "Unknown" };
	String openprotectoption[] = { "Select", "FBC Plywood",
			"All Glazed Openings 4 � 8 lb Large Missile lb.",
			"All Glazed Openings 4 - 8 lb Large Missile",
			"Opening Protection Not Verified", "None / Some Not Protected" };
	String wallconstructionoption[] = { "Select", "Wood Frame",
			"Un - Reinforced Masonry", "Reinforced Masonry", "Poured Concrete",
			"Other" };
	CommonFunctions cf;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addcomments);
		cf = new CommonFunctions(this);
		cf.getDeviceDimensions();
		cf.getInspectorId();

		RelativeLayout layout = (RelativeLayout) findViewById(R.id.relativeLayout1);
		layout.setMinimumWidth(cf.wd);
		
		spninspec = (Spinner) this.findViewById(R.id.sprinspection);
		spn1 = (Spinner) this.findViewById(R.id.sprquestion);
		spn2 = (Spinner) this.findViewById(R.id.sproption);
		lnrlayout = (LinearLayout) this.findViewById(R.id.linscrollview);
		chkall = (CheckBox) this.findViewById(R.id.chkall);
		txtheader = (LinearLayout) this.findViewById(R.id.txtheader);
		
		cf.welcomemessage();
		((TextView)findViewById(R.id.welcomename)).setText(Html.fromHtml("<font color=#f7a218>" + "Welcome : "+"</font>"+ cf.data));
		
		lnrlayouttype = (LinearLayout) this.findViewById(R.id.lnrlayouttype);
		lnrlayoutoption = (LinearLayout) this
				.findViewById(R.id.lnrlayoutoption);
		lnrlayoutsuboption = (LinearLayout) this
				.findViewById(R.id.lnrlayoutsuboption);
		lnrlayoutaddview = (LinearLayout) this
				.findViewById(R.id.lnrlayoutaddviewbtn);
		activate = (Button) this.findViewById(R.id.activate);
		deactivate = (Button) this.findViewById(R.id.deactivate);
		nocomments = (TextView) this.findViewById(R.id.txtnocomments);
		this.home = (Button) this.findViewById(R.id.standardinshome);
		this.back = (Button) this.findViewById(R.id.standardinsback);

		adapterinsp = new ArrayAdapter(ToaddComment.this,android.R.layout.simple_spinner_item, inspectiontype);
		adapterinsp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spninspec.setAdapter(adapterinsp);
		spn2.setEnabled(false);
		

		spninspec.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

				spn1.setEnabled(false);
				spn1.setSelection(0);

				inspectedPos = arg2;
				if (arg2 == 1) {
					String questiontype[] = { "Select Question Type", "Building Code",
							"Roof Covering", "Roof Deck Attachment", "Roof to Wall Attachment",
							"Roof Geometry", "Secondary Water Resistance(SWR)",
							"Opening Protection" };
					spn1.setEnabled(true);
					inspectionType = 28;
					adapter = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item, questiontype);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn1.setAdapter(adapter);
				} else if (arg2 == 2) {
					String questiontype[] = { "Select Question Type", "Building Code",
							"Roof Covering", "Roof Deck Attachment", "Roof to Wall Attachment",
							"Roof Geometry", "Secondary Water Resistance(SWR)",
							"Opening Protection", "Wall Construction(Addendum)"};
					spn1.setEnabled(true);
					inspectionType = 29;
					adapter = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item, questiontype);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn1.setAdapter(adapter);
				}
				nocomments.setVisibility(v1.GONE);
				lnrlayout.setVisibility(v1.GONE);
				txtheader.setVisibility(v1.GONE);
				activate.setVisibility(v1.GONE);
				deactivate.setVisibility(v1.GONE);
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spn1.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				spn2.setEnabled(false);
				spn2.setSelection(0);

				if (position == 1) {
					spn2.setEnabled(true);
					quesid = position;

					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item,
							buildcodeoption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				if (position == 2) {
					spn2.setEnabled(true);
					quesid = position;
					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item,
							roofcoveroption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				if (position == 3) {
					spn2.setEnabled(true);
					quesid = position;
					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item,
							roofdeckoption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				if (position == 4) {
					spn2.setEnabled(true);
					quesid = position;
					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item,
							roofwalloption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				if (position == 5) {
					spn2.setEnabled(true);
					quesid = position;
					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item,
							roofgeometryoption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				if (position == 6) {
					spn2.setEnabled(true);
					quesid = position;
					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item, swroption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				if (position == 7) {
					spn2.setEnabled(true);
					quesid = position;
					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item,
							openprotectoption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				if (position == 8) {
					spn2.setEnabled(true);
					quesid = position;
					adapter2 = new ArrayAdapter(ToaddComment.this,
							android.R.layout.simple_spinner_item,
							wallconstructionoption);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spn2.setAdapter(adapter2);
				}
				nocomments.setVisibility(v1.GONE);
				lnrlayout.setVisibility(v1.GONE);
				txtheader.setVisibility(v1.GONE);
				activate.setVisibility(v1.GONE);
				deactivate.setVisibility(v1.GONE);
				spn2.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent, View v,
							int position, long id) {

						spn2pos = position;
						nocomments.setVisibility(v1.GONE);
						lnrlayout.setVisibility(v1.GONE);
						txtheader.setVisibility(v1.GONE);
						activate.setVisibility(v1.GONE);
						deactivate.setVisibility(v1.GONE);

					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});

			}

			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		try {
			cf.db.execSQL("CREATE TABLE IF NOT EXISTS "
					+ cf.tbl_admcomments
					+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,InspectorId Varchar,questionid INT NOT NULL DEFAULT ' ',optionid VARCHAR (100) NOT NULL DEFAULT ' ',optionvalue VARCHAR (50) NOT NULL DEFAULT ' ',description VARCHAR (50) NOT NULL DEFAULT ' ',status VARCHAR (50) NOT NULL DEFAULT ' ');");

		} catch (Exception e) {

		}
		this.home.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
					cf.gohome();
			}
		});
		this.back.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {

				lnrlayouttype.setVisibility(v1.VISIBLE);
				lnrlayoutoption.setVisibility(v1.VISIBLE);
				lnrlayoutsuboption.setVisibility(v1.VISIBLE);
				lnrlayoutaddview.setVisibility(v1.VISIBLE);
				back.setVisibility(v1.GONE);
				txtheader.setVisibility(v1.GONE);
				lnrlayout.setVisibility(v1.GONE);
				activate.setVisibility(v1.GONE);
				deactivate.setVisibility(v1.GONE);
				nocomments.setVisibility(v1.GONE);
			}
		});

		chkall.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					commentsection();
					updatecnt = 1;
					for (int j = 0; j < arrdescription.length; j++) {
						cb[j].setChecked(true);
					}
				} else {

					for (int j = 0; j < arrdescription.length; j++) {
						cb[j].setChecked(false);
					}
				}
			}
		});

	}

	private void commentsection() {
		// TODO Auto-generated method stub
		strdesc = "";
		actstatus = "";

		Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.tbl_admcomments
				+ " WHERE  questionid='" + quesid + "' and optionid='"
				+ spn2pos + "' and InspectorId='" + inspectionType + "'", null);
		int rws = c2.getCount();
		arrdescription = new String[rws];
		arrstatus = new String[rws];
		c2.moveToFirst();
		if (rws == 0) {
			iden = 1;
			nocomments.setVisibility(v1.VISIBLE);
			txtheader.setVisibility(v1.GONE);
			lnrlayout.setVisibility(v1.GONE);
			activate.setVisibility(v1.GONE);
			deactivate.setVisibility(v1.GONE);
		} else {
			iden = 0;
			if (c2 != null) {
				int i = 0;
				do {

					arrdescription[i] = cf.getsinglequotes(c2.getString(c2
							.getColumnIndex("description")));

					arrstatus[i] = c2.getString(c2.getColumnIndex("status"));
					if (arrdescription[i].contains("null")) {
						arrdescription[i] = arrdescription[i].replace("null",
								"");
					}
					if (actstatus.contains("null")) {
						arrstatus[i] = actstatus.replace("null", "");
					}
					System.out.println("ARRDESC "+arrdescription[i]);
					
					/*
					 * arrdescription = strdesc.split("~"); arrstatus =
					 * actstatus.split("~");
					 */
					i++;
				} while (c2.moveToNext());
			}
			c2.close();
		}

	}

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.viewcomments:

			if (inspectedPos == 0) {
				cf.ShowToast("Please select the Inspection Type.",1);

			} else if (quesid == 0) {
				cf.ShowToast("Please select the Question Type.",1);

			} else if (spn2pos == 0) {
				cf.ShowToast("Please select the option.",1);

			} else {
				lnrlayouttype.setVisibility(v1.GONE);
				lnrlayoutoption.setVisibility(v1.GONE);
				lnrlayoutsuboption.setVisibility(v1.GONE);
				lnrlayoutaddview.setVisibility(v1.GONE);
				back.setVisibility(v1.VISIBLE);
				nocomments.setVisibility(v1.GONE);
				txtheader.setVisibility(v1.VISIBLE);
				lnrlayout.setVisibility(v1.VISIBLE);
				activate.setVisibility(v1.VISIBLE);
				deactivate.setVisibility(v1.VISIBLE);
				commentsection();

				if (iden == 1) {

				} else {
					commentsplit();
				}
			}

			break;
		case R.id.addcomments:

			if (quesid == 1) {
				optionvalue = buildcodeoption[spn2pos];
			} else if (quesid == 2) {
				optionvalue = roofcoveroption[spn2pos];
			} else if (quesid == 3) {
				optionvalue = roofdeckoption[spn2pos];
			} else if (quesid == 4) {
				optionvalue = roofwalloption[spn2pos];
			} else if (quesid == 5) {
				optionvalue = roofgeometryoption[spn2pos];
			} else if (quesid == 6) {
				optionvalue = swroption[spn2pos];
			} else if (quesid == 7) {
				optionvalue = openprotectoption[spn2pos];
			} else if (quesid == 8) {
				optionvalue = wallconstructionoption[spn2pos];
			} else {
			}
			if (inspectedPos == 0) {
				cf.ShowToast("Please select the Inspection Type.",1);

			} else if (quesid == 0) {
				cf.ShowToast("Please select the Question Type.",1);

			} else if (spn2pos == 0) {
				cf.ShowToast("Please select the Options.",1);

			} else {

				Cursor c2 = cf.db.rawQuery("SELECT * FROM "
						+ cf.tbl_admcomments + " WHERE  questionid='" + quesid
						+ "' and optionid='" + spn2pos + "' and InspectorId='"
						+ inspectionType + "'", null);
				int rws = c2.getCount();

				if (rws == 5) {
					cf.ShowToast("You can add only Five Comments.",1);

				} else if (rws <= 5) {
					insertdata(optionvalue);
				} else if (rws > 5) {
					cf.ShowToast("Exceeds the limit.You can't add comments.",1);

				}

			}

			break;

		case R.id.activate:
			for (int j = 0; j < arrdescription.length; j++) {
				if (cb[j].isChecked() == true) {
					statuschkactivate = 1;
					String updateqryact = "UPDATE "
							+ cf.tbl_admcomments
							+ " SET status='Active'"
							+ " WHERE questionid ='"
							+ quesid
							+ "' and optionid='"
							+ spn2pos
							+ "' and description='"
							+ cf.convertsinglequotes(cb[j].getText().toString())
							+ "' and InspectorId='" + inspectionType + "'";
					try {
						cf.db.execSQL(updateqryact);
						commentsection();
					} catch (Exception e) {

					}
					if (updatecnt == 1) {
						chkall.setChecked(false);
					}
					tvstatus[j].setText("Active");
					cb[j].setChecked(false);

				} else if (cb[j].isChecked() == false) {
				}

			}
			if (statuschkactivate == 1) {
				cf.ShowToast("Activated successfully.",1);
				statuschkactivate = 0;
			} else {
				cf.ShowToast("Please select checkbox to Activate Comments.",1);

			}
			break;

		case R.id.deactivate:
			for (int j = 0; j < arrdescription.length; j++) {
				if (cb[j].isChecked() == true) {
					statuschkdeactivate = 1;
					String updateqrydeact = "UPDATE "
							+ cf.tbl_admcomments
							+ " SET status='InActive'"
							+ " WHERE questionid ='"
							+ quesid
							+ "' and optionid='"
							+ spn2pos
							+ "' and description='"
							+ cf.convertsinglequotes(cb[j].getText().toString())
							+ "' and InspectorId='" + inspectionType + "'";
					cf.db.execSQL(updateqrydeact);
					commentsection();

					if (updatecnt == 1) {
						chkall.setChecked(false);
					}
					tvstatus[j].setText("InActive");
					cb[j].setChecked(false);

				} else if (cb[j].isChecked() == false) {
				} 
			}
			if (statuschkdeactivate == 1) {
				cf.ShowToast("Deactivated successfully.",1);
				statuschkdeactivate = 0;
			} else {
				cf.ShowToast("Please select checkbox to Deactivate Comments.",1);

			}

			break;
		}
	};

	public void insertdata(final String quesoptionvalue) {
		System.out.println("optionvalue "+quesoptionvalue);
		 cf.alerttitle="Add Comments";
		    final Dialog dialog1 = new Dialog(ToaddComment.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alertsync);
			TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
			txttitle.setText( cf.alerttitle);
			TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
			txt.setVisibility(v1.GONE);
			final EditText edtvalue = (EditText) dialog1.findViewById(R.id.txtvalue);
			edtvalue.setVisibility(v1.VISIBLE);
			Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
			btn_yes.setText("Add");
			Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
			btn_cancel.setText("Cancel");
			CheckBox chkinsert = (CheckBox) dialog1.findViewById(R.id.updateinalternate);
			chkinsert.setVisibility(v1.VISIBLE);
			System.out.println("inse "+inspectionType);
			if(inspectionType==28)
			{
				instypeupdate=29;
				System.out.println("QUES ID "+quesid);
				if(quesid==8)
				{
					chkinsert.setVisibility(v1.GONE);
				}
				else
				{
				chkinsert.setText("Do you want to add this comments in carrier ordered also?");
				}
				
			}
			else
			{
				instypeupdate=28;
				System.out.println("QUES ID qq"+quesid);
				if(quesid==8)
				{
					chkinsert.setVisibility(v1.GONE);
				}
				else
				{
				chkinsert.setText("Do you want to add this comments in retail also?");
				
				}
			}
			
			chkinsert.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					if (((CheckBox) v).isChecked()) {
						insertchk=1;
						
					} else {
						insertchk=0;
					}
				}
			});
			
			
			btn_yes.setOnClickListener(new OnClickListener()
			{
           	public void onClick(View arg0) {
					// TODO Auto-generated method stub
           		desccomm = edtvalue.getText().toString();
           		if (!desccomm.trim().equals("")) {
					cf.db.execSQL("INSERT INTO "
							+ cf.tbl_admcomments
							+ " (InspectorId,questionid,optionid,optionvalue,description,status)"
							+ " VALUES ('" + inspectionType + "','" + quesid
							+ "','" + spn2pos + "','" + quesoptionvalue + "','"
							+ cf.convertsinglequotes(desccomm)
							+ "','InActive')");

					nocomments.setVisibility(v1.GONE);
					commentsection();

					back.setVisibility(v1.VISIBLE);
					lnrlayouttype.setVisibility(v1.GONE);
					lnrlayoutoption.setVisibility(v1.GONE);
					lnrlayoutsuboption.setVisibility(v1.GONE);
					lnrlayoutaddview.setVisibility(v1.GONE);
					txtheader.setVisibility(v1.VISIBLE);
					lnrlayout.setVisibility(v1.VISIBLE);
					activate.setVisibility(v1.VISIBLE);
					deactivate.setVisibility(v1.VISIBLE);
					commentsplit();
					if(insertchk==1)
					{
						cf.db.execSQL("INSERT INTO "
								+ cf.tbl_admcomments
								+ " (InspectorId,questionid,optionid,optionvalue,description,status)"
								+ " VALUES ('" + instypeupdate + "','" + quesid
								+ "','" + spn2pos + "','" + quesoptionvalue + "','"
								+ cf.convertsinglequotes(desccomm)
								+ "','InActive')");
					}
					cf.ShowToast("Comments added successfully.",1);
					
					
					

				} else {
					cf.ShowToast("Please enter Comments. It can't be empty.",1);

				}
					dialog1.dismiss();
					
					
				}
				
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					
				}
				
			});
			dialog1.setCancelable(false);
			dialog1.show();

	}

	private void commentsplit() {
		// TODO Auto-generated method stub

		lnrlayout.removeAllViews();
		ScrollView sv = new ScrollView(this);

		lnrlayout.addView(sv);

		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		l1.setMinimumWidth(925);

		cb = new CheckBox[arrdescription.length];
		editbtn = new Button[arrdescription.length];
		tvstatus = new TextView[arrdescription.length];
		deletebtn = new Button[arrdescription.length];

		for (i = 0; i < arrdescription.length; i++) {

			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l1.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					580, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 5;
			paramschk.bottomMargin = 15;
			l2.addView(lchkbox);

			cb[i] = new CheckBox(this);
			cb[i].setMinimumWidth(580);
			cb[i].setMaxWidth(580);
			cb[i].setText(arrdescription[i]);
			cb[i].setTextColor(Color.BLACK);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			//cb[i].setTextSize(16);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);
			lchkbox.addView(cb[i], paramschk);

			LinearLayout ltextbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
					100, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramstext.topMargin = 5;
			paramstext.leftMargin = 15;
			l2.addView(ltextbox);

			tvstatus[i] = new TextView(this);
			tvstatus[i].setText(arrstatus[i]);
			tvstatus[i].setTextColor(Color.BLACK);
			tvstatus[i].setMinimumWidth(100);
			tvstatus[i].setGravity(Gravity.RIGHT);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			//tvstatus[i].setTextSize(16);
			tvstatus[i].setTag("txtstatus" + i);
			ltextbox.addView(tvstatus[i], paramstext);

			LinearLayout leditbtn = new LinearLayout(this);
			LinearLayout.LayoutParams paramseditbtn = new LinearLayout.LayoutParams(
					93, 37);
			paramseditbtn.topMargin = 5;
			paramseditbtn.leftMargin = 24;
			l2.addView(leditbtn);
 
			editbtn[i] = new Button(this);
			editbtn[i].setBackgroundResource(R.drawable.edit);
			editbtn[i].setTag("editbtn" + i);
			editbtn[i].setPadding(30, 0, 0, 0);
			map1.put("editbtn" + i, cb[i]);
			leditbtn.addView(editbtn[i], paramseditbtn);

			LinearLayout ldelbtn = new LinearLayout(this);
			LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
					93, 37);
			paramsdelbtn.topMargin = 5;
			paramsdelbtn.leftMargin = 24;
			l2.addView(ldelbtn);

			deletebtn[i] = new Button(this);
			deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
			deletebtn[i].setTag("deletebtn" + i);
			deletebtn[i].setPadding(30, 0, 0, 0);
			map1.put("deletebtn" + i, cb[i]);
			ldelbtn.addView(deletebtn[i], paramsdelbtn);
			
			
			deletebtn[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					String repidofselbtn = getidofselbtn.replace("deletebtn",
							"");
					final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;

					if (cb[cvrtstr - 1].isChecked() == true) {
						temp_st = map1.get(getidofselbtn);

						 cf.alerttitle="Delete";
						  cf.alertcontent="Are you sure want to delete this comment?";
						    final Dialog dialog1 = new Dialog(ToaddComment.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText( cf.alerttitle);
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml( cf.alertcontent));
							
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							CheckBox chkdelete = (CheckBox) dialog1.findViewById(R.id.updateinalternate);
							chkdelete.setVisibility(v1.VISIBLE);
							if(inspectionType==28)
							{
								chkdelete.setText("Do you want to delete this comments in carrier ordered also?");
								instypeupdate=29;
							}
							else
							{
								instypeupdate=28;
								chkdelete.setText("Do you want to delete this comments in retail also?");
							}
							Cursor c2 = cf.db.rawQuery("SELECT * FROM "
									+ cf.tbl_admcomments + " WHERE  questionid='" + quesid
									+ "' and optionid='" + spn2pos + "' and InspectorId='"+ instypeupdate +"' and " +
											"description='"+cf.convertsinglequotes(temp_st.getText().toString())+"'", null);
							int rws = c2.getCount();
							System.out.println("rowsdelete "+rws);
							
							
							if(rws!=0)
							{
								chkdelete.setVisibility(v1.VISIBLE);
							}
							else
							{
								chkdelete.setVisibility(v1.GONE);
							}
							chkdelete.setOnClickListener(new OnClickListener() {
								public void onClick(View v) {
									if (((CheckBox) v).isChecked()) {
										cf.db.execSQL("DELETE FROM " + cf.tbl_admcomments
												+ " WHERE questionid='" + quesid + "' and optionid='" + spn2pos
												+ "' and description='"
												+ cf.convertsinglequotes(temp_st.getText().toString()) + "' and InspectorId='"+instypeupdate+"'");
									} else {
										
									}
								}
							});
							
							btn_yes.setOnClickListener(new OnClickListener()
							{
			                	public void onClick(View arg0) {
									// TODO Auto-generated method stub
									
									deletecomments(cvrtstr, temp_st);
									dialog1.dismiss();
								}
								
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{

								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									
								}
								
							});
							dialog1.setCancelable(false);
							dialog1.show();
						
						

					} else {
						cf.ShowToast("Select checkbox to Delete Comments.",1);

					}

				}
			});
			editbtn[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					String getidofselbtn = v.getTag().toString();

					String repidofselbtn = getidofselbtn.replace("editbtn", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;

					if (cb[cvrtstr - 1].isChecked() == true) {
						temp_st = map1.get(getidofselbtn);
						 cf.alerttitle="Update";
						    final Dialog dialog1 = new Dialog(ToaddComment.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText( cf.alerttitle);
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setVisibility(v1.GONE);
							final EditText edtvalue = (EditText) dialog1.findViewById(R.id.txtvalue);
							edtvalue.setVisibility(v1.VISIBLE);
							edtvalue.setText(temp_st.getText());
							
							
							
							
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							btn_yes.setText("Update");
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							btn_cancel.setText("Cancel");System.out.println("came here");
							 CheckBox chkalter = (CheckBox) dialog1.findViewById(R.id.updateinalternate);System.out.println("cupdaehere");
							 chkalter.setVisibility(v1.VISIBLE);
							 if(inspectionType==28)
								{
								 
									instypeupdate=29;
									if(quesid==8)
									{
										chkalter.setVisibility(v1.GONE);
									}
									else
									{
										chkalter.setText("Do you want to update this comments in carrier ordered also?");
									}
									
								}
								else
								{
									
									instypeupdate=28;
									if(quesid==8)
									{
										chkalter.setVisibility(v1.GONE);
									}
									else
									{
										chkalter.setText("Do you want to update this comments in retail also?");
									}
									
								}
								
								
							/*	
								if(rws!=0)
								{
									chkalter.setVisibility(v1.VISIBLE);
								}
								else
								{
									chkalter.setVisibility(v1.GONE);
								}*/
								
								chkalter.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {
										if (((CheckBox) v).isChecked()) {
											System.out.println("temp_st "+temp_st.getText().toString());
											
											Cursor c2 = cf.db.rawQuery("SELECT * FROM "
													+ cf.tbl_admcomments + " WHERE  questionid='" + quesid
													+ "' and optionid='" + spn2pos + "' and InspectorId='"+ instypeupdate +"' and " +
															"description='"+cf.convertsinglequotes(edtvalue.getText().toString())+"'", null);
											System.out.println("update "+"SELECT * FROM "
													+ cf.tbl_admcomments + " WHERE  questionid='" + quesid
													+ "' and optionid='" + spn2pos + "' and InspectorId='"+ instypeupdate +"' and " +
															"description='"+cf.convertsinglequotes(temp_st.getText().toString())+"'");
											if(c2.getCount()==0)
											{
												cf.db.execSQL("INSERT INTO "
														+ cf.tbl_admcomments
														+ " (InspectorId,questionid,optionid,description,status)"
														+ " VALUES ('" + instypeupdate + "','" + quesid
														+ "','" + spn2pos + "','"
														+ cf.convertsinglequotes(edtvalue.getText().toString())
														+ "','InActive')");
											}
											else
											{
											
											System.out.println("UPDATE " + cf.tbl_admcomments + " SET description ='"
													+ cf.convertsinglequotes(edtvalue.getText().toString()) + "'" + " WHERE  questionid='"
													+ quesid + "' and optionid='" + spn2pos + "' and description='"
													+ cf.convertsinglequotes(temp_st.getText().toString()) + "' and InspectorId='"+instypeupdate+"'");
											
											cf.db.execSQL("UPDATE " + cf.tbl_admcomments + " SET description ='"
													+ cf.convertsinglequotes(edtvalue.getText().toString()) + "'" + " WHERE  questionid='"
													+ quesid + "' and optionid='" + spn2pos + "' and description='"
													+ cf.convertsinglequotes(temp_st.getText().toString()) + "' and InspectorId='"+instypeupdate+"'");
											}
										} else {
											
										}
									}
								});
								
							btn_yes.setOnClickListener(new OnClickListener()
							{
			                	public void onClick(View arg0) {
									// TODO Auto-generated method stub
									
									String value = edtvalue.getText().toString();
									updatecomments(value, cvrtstr, temp_st);
									dialog1.dismiss();
								}
								
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{

								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									
								}
								
							});
							dialog1.setCancelable(false);
							dialog1.show();
						
					} else {
						cf.ShowToast("Select checkbox to Edit Comments.",1);

					}

				}
			});

		}

	}

	private void updatecomments(String value, int quesordid, CheckBox chkbx) {
		if(!"".equals(value.trim()))
		{
			cf.db.execSQL("UPDATE " + cf.tbl_admcomments + " SET description ='"
					+ cf.convertsinglequotes(value) + "'" + " WHERE  questionid='"
					+ quesid + "' and optionid='" + spn2pos + "' and description='"
					+ cf.convertsinglequotes(chkbx.getText().toString()) + "' and InspectorId='"+inspectionType+"'");
			cf.ShowToast("Updated successfully.",1);
			commentsection();
			chkbx.setText(value);
			chkbx.setChecked(false);
		}
		else
		{
			cf.ShowToast("Please enter comments. It cant't be empty.",1);
		}
		
	}

	private void deletecomments(int quesordid, CheckBox chkbx) {
		cf.db.execSQL("DELETE FROM " + cf.tbl_admcomments
				+ " WHERE questionid='" + quesid + "' and optionid='" + spn2pos
				+ "' and description='"
				+ cf.convertsinglequotes(chkbx.getText().toString()) + "' and InspectorId='"+inspectionType+"'");
		cf.ShowToast("Deleted successfully.",1);
		commentsection();

		chkbx.setChecked(false);
		if (iden == 1) {

		} else {
			commentsplit();
		}
	}
}