package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.OverallComments.OC_textwatcher;
import idsoft.inspectiondepot.B11802.OverallComments.Touch_Listener;

import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class RoofDeck extends Activity {
	TextView nocommentsdisp, txroofdeckheading,helptxt,prevmitidata,roofdeck_TV_type1;
	RelativeLayout tblcommentschange;public int focus=0;
	RadioButton rdioA, rdioB, rdioC, rdioD, rdioE, rdioF, rdioG;
	ListView list;
	ImageView Vimage;
	String commentsfill, InspectionType, status, rdiochk, comm, homeId,
			othertext = "", updatecnt="0",commdescrip = "",conchkbox,
			identity,inspectortypeid, helpcontent,roofdeckcomment,roofdeckvalueprev, roofdeckothrtxtprev,descrip, comm2, chkstatus = "true";
	EditText comments, txtother;
	Intent iInspectionList;
	int value, Count,viewimage = 1, commentsch,optionid;
	Button saveclose, prev;
	private static final int visibility = 0;
	LinearLayout lincomments,roofdeck_parrant,roofdeck_type1;
	private TableLayout tbllayout8, exceedslimit1;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	String[] arrcomment1;
	CheckBox temp_st;
	View v1;
	
	CheckBox[] cb;
	View vv;
	AlertDialog alertDialog;	
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();
	CommonFunctions cf;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		cf.getinspectiondate();
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.Homeid = homeId = bunhomeId.getString("homeid");
			cf.Identity = identity = bunhomeId.getString("iden");
			cf.InspectionType = InspectionType = bunhomeId
					.getString("InspectionType");
			cf.status = status = bunhomeId.getString("status");
			cf.value = value = bunhomeId.getInt("keyName");
			cf.Count = Count = bunhomeId.getInt("Count");
		}

		setContentView(R.layout.roofdeck);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.getDeviceDimensions();
		cf.changeimage();
		
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 23,
				cf, 1));
		focus=1;
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		txroofdeckheading = (TextView) findViewById(R.id.txtheadingroofdeck);
		txroofdeckheading.setText(Html.fromHtml("<font color=red> * "
				+ "</font>What is the " + "<u>" + "weakest" + "</u>"
				+ " form of roof deck attachment?"));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		

		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
		if (rdiochk==null){
			cf.alertcontent = "To see help comments, please select Roof Deck options.";
		}
		else if (rdiochk.equals("1") || rdioA.isChecked()) {
			cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 3 Roof Deck Attachment, where mean lift is less than that of B and C Selections.";
		} else if (rdiochk.equals("2") || rdioB.isChecked()) {
			cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 3 Roof Deck Attachment.";
		} else if (rdiochk.equals("3") || rdioC.isChecked()) {
			cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C, Question 3 Roof Deck Attachment.";
		} else if (rdiochk.equals("4") || rdioD.isChecked()) {
			cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D, Question 3 Roof Deck Attachment.";
		} else if (rdiochk.equals("5") || rdioE.isChecked()) {
			cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, Question 3 Roof Deck Attachment.";
		} else if (rdiochk.equals("6") || rdioF.isChecked()) {
			cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 3 Roof Deck Attachment.";
		} else if (rdiochk.equals("7") || rdioG.isChecked()) {
			cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
		} else {
			cf.alertcontent = "To see help comments, please select Roof Deck options.";
		}
		cf.showhelp("HELP",cf.alertcontent);
			}
		});
	
		
		
		this.tbllayout8 = (TableLayout) this.findViewById(R.id.tableLayoutComm);

		this.lincomments = (LinearLayout) this
				.findViewById(R.id.linearlayoutcomm);
		exceedslimit1 = (TableLayout) this.findViewById(R.id.exceedslimit);
		this.nocommentsdisp = (TextView) this.findViewById(R.id.notxtcomments);
		
		try {

			Cursor c2 = cf.db.rawQuery("SELECT * FROM "
					+ cf.quetsionsoriginaldata + " WHERE homeid='" + cf.Homeid
					+ "'", null);
			if(c2.getCount()>0){
				c2.moveToFirst();
				String bcoriddata = cf.getsinglequotes(c2.getString(c2.getColumnIndex("rdoriginal")));
				prevmitidata.setText(Html
					.fromHtml("<font color=blue>Original Value : " + "</font>"
							+ "<font color=red>" + bcoriddata + "</font>"));
			}
			else
			{
				prevmitidata.setText(Html
						.fromHtml("<font color=blue>Original Value : "
								+ "</font>" + "<font color=red>Not Available</font>"));
			}

		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" Problem in getting roofdeck original value on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
		
		

		Vimage = (ImageView) findViewById(R.id.Vimage);
		tblcommentschange = (RelativeLayout) findViewById(R.id.tbllayoutrdcomments);
		this.rdioA = (RadioButton) this.findViewById(R.id.rdio1);
		this.rdioA.setOnClickListener(OnClickListener);
		this.rdioB = (RadioButton) this.findViewById(R.id.rdio2);
		this.rdioB.setOnClickListener(OnClickListener);
		this.rdioC = (RadioButton) this.findViewById(R.id.rdio3);
		this.rdioC.setOnClickListener(OnClickListener);
		this.rdioD = (RadioButton) this.findViewById(R.id.rdio4);
		this.rdioD.setOnClickListener(OnClickListener);
		this.rdioE = (RadioButton) this.findViewById(R.id.rdio5);
		this.rdioE.setOnClickListener(OnClickListener);
		this.rdioF = (RadioButton) this.findViewById(R.id.rdio6);
		this.rdioF.setOnClickListener(OnClickListener);
		this.rdioG = (RadioButton) this.findViewById(R.id.rdio7);
		this.rdioG.setOnClickListener(OnClickListener);

		this.txtother = (EditText) this.findViewById(R.id.txtroofdeckother);
		
		roofdeck_parrant = (LinearLayout)this.findViewById(R.id.roofdeck_parrent);
		roofdeck_type1=(LinearLayout) findViewById(R.id.roofdeck_type);
		roofdeck_TV_type1 = (TextView) findViewById(R.id.roofdeck_txt);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		
		this.comments = (EditText) this.findViewById(R.id.txtroofdeckcomments);
		comments.setOnTouchListener(new Touch_Listener());
		comments.addTextChangedListener(new QUES_textwatcher());
		
		prev = (Button) findViewById(R.id.previous);

		try {
			Cursor c2 = cf.db.rawQuery(
					"SELECT RoofDeckValue,RoofDeckOtherText FROM "
							+ cf.tbl_questionsdata + " WHERE SRID='"
							+ cf.Homeid + "'", null);
			if (c2.getCount() == 0) {
				identity = "test";
			} else {
				identity = "prev";
			}
		} catch (Exception e) {
		
		}

		prev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				iInspectionList = new Intent(RoofDeck.this, RoofCover.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("iden", "prev");
				iInspectionList.putExtra("InspectionType", cf.InspectionType);
				iInspectionList.putExtra("status", cf.status);
				iInspectionList.putExtra("keyName", cf.value);
				iInspectionList.putExtra("Count", cf.Count);
				startActivity(iInspectionList);
			}
		});
		if (identity.equals("prev")) {
			try {
				Cursor cur = cf.db.rawQuery("select * from "
						+ cf.tbl_questionsdata + " where SRID='" + cf.Homeid
						+ "'", null);
				if(cur.getCount()>0){
				cur.moveToFirst();
				if (cur != null) {
					roofdeckvalueprev = cur.getString(cur
							.getColumnIndex("RoofDeckValue"));
					roofdeckothrtxtprev = cf.getsinglequotes(cur.getString(cur
							.getColumnIndex("RoofDeckOtherText")));

					if (roofdeckvalueprev.equals("1")) {
						rdioA.setChecked(true);
						rdiochk = "1";
					} else if (roofdeckvalueprev.equals("2")) {
						rdioB.setChecked(true);
						rdiochk = "2";
					} else if (roofdeckvalueprev.equals("3")) {
						rdioC.setChecked(true);
						rdiochk = "3";
					} else if (roofdeckvalueprev.equals("4")) {
						rdioD.setChecked(true);
						rdiochk = "4";
					} else if (roofdeckvalueprev.equals("5")) {
						rdioE.setChecked(true);
						rdiochk = "5";
						txtother.setText(roofdeckothrtxtprev);
					} else if (roofdeckvalueprev.equals("6")) {
						rdioF.setChecked(true);
						rdiochk = "6";
					} else if (roofdeckvalueprev.equals("7")) {
						rdioG.setChecked(true);
						rdiochk = "7";
					} else {

					}
				}
				}
			} catch (Exception e) {
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in retrieving QUES data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}

			try {
				Cursor cur = cf.db.rawQuery("select * from " + cf.tbl_comments	+ " where SRID='" + cf.Homeid + "'", null);
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
					if (cur != null) {
						roofdeckcomment = cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("RoofDeckComment")));
						cf.showing_limit(roofdeckcomment,roofdeck_parrant,roofdeck_type1,roofdeck_TV_type1,"474");
						
						comments.setText(roofdeckcomment);
					}
				}
			} catch (Exception e) {
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in retrieving roofdeck comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
		}
		this.Vimage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}
		});

		this.tblcommentschange.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}
		});
	}
	class Touch_Listener implements OnTouchListener
	{
		   Touch_Listener()
			{
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	
		    		cf.setFocus(comments);
				
		
				return false;
		    }
	}
	class QUES_textwatcher implements TextWatcher
	{
	     QUES_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
				
				cf.showing_limit(s.toString(),roofdeck_parrant,roofdeck_type1,roofdeck_TV_type1,"474"); 
			
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	protected void arrowcommentschange() {
		// TODO Auto-generated method stub
		Vimage.setBackgroundResource(R.drawable.arrowup);
		if (viewimage == 1) {
			Vimage.setBackgroundResource(R.drawable.arrowup);
			if (rdioA.isChecked()) {
				optionid = 1;
			} else if (rdioB.isChecked()) {
				optionid = 2;
			} else if (rdioC.isChecked()) {
				optionid = 3;
			} else if (rdioD.isChecked()) {
				optionid = 4;
			} else if (rdioE.isChecked()) {
				optionid = 5;
			} else if (rdioF.isChecked()) {
				optionid = 6;
			} else if (rdioG.isChecked()) {
				optionid = 7;
			}

			else {
				cf.ShowToast("Please select Roof Deck options to view Comments.",1);

			}

			try {
				descrip = "";
				arrcomment1 = null;
				Cursor c2 = cf.db.rawQuery("SELECT * FROM "
						+ cf.tbl_admcomments
						+ " WHERE questionid='3' and optionid='" + optionid
						+ "' and status='Active' and InspectorId='"
						+ cf.InspectionType + "'", null);
				int rws = c2.getCount();
				arrcomment1 = new String[rws];
				c2.moveToFirst();
				if (rws == 0) {
					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.VISIBLE);
					lincomments.setVisibility(v1.GONE);
					Vimage.setBackgroundResource(R.drawable.arrowdown);
				} else {

					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.GONE);

					lincomments.setVisibility(v1.VISIBLE);
					if (c2 != null) {
						int i=0;
						do {
							arrcomment1[i]=cf.getsinglequotes(c2.getString(c2.getColumnIndex("description")));
							if(arrcomment1[i].contains("null"))
							{
								arrcomment1[i] = arrcomment1[i].replace("null", "");
							}							
							i++;
						} while (c2.moveToNext());						
					}
					c2.close();

					addcomments();
					viewimage = 0;
				}

			} catch (Exception e) {
				/*
				 * ShowToast toast = new ShowToast(getBaseContext(),
				 * "Please add atleast one roof deck comment in dashboard");
				 */
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in retrieving roofdeck comments data in arrowclick on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}

		} else if (viewimage == 0) {
			Vimage.setBackgroundResource(R.drawable.arrowdown);
			tbllayout8.setVisibility(v1.GONE);
			viewimage = 1;
		}
	}

	private void addcomments() {
		// TODO Auto-generated method stub
		lincomments.removeAllViews();
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		lincomments.addView(l1);
		cb = new CheckBox[arrcomment1.length];

		for (int i = 0; i < arrcomment1.length; i++) {

			LinearLayout l2 = new LinearLayout(this);			
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.setMargins(0, 0, 20, 0);
			l1.addView(l2);			
			
			cb[i] = new CheckBox(this);
			cb[i].setText(arrcomment1[i].trim());
			cb[i].setTextColor(Color.GRAY);
			cb[i].setSingleLine(false);
			
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
			cb[i].setMaxWidth(padding);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,12);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);

			map1.put(conchkbox, cb[i]);
			l2.addView(cb[i],paramschk);

			cb[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					if(comments.getText().length()>=474)
					{
						cf.ShowToast("Comment text exceeds the limit.", 1);
						map1.get(getidofselbtn).setChecked(false);
					}
					else
					{
					temp_st = map1.get(getidofselbtn);
					setcomments(temp_st);
					}
				}
			});
		}

	}

	private void setcomments(final CheckBox tempSt) {
		// TODO Auto-generated method stub
		comm2 = "";
		String comm1 = tempSt.getText().toString();
		if (tempSt.isChecked() == true) {
			comm2 += comm1 + " ";
			int commenttextlength = comments.getText().length();
			comments.setText(comments.getText().toString() + " " + comm2);

			if (commenttextlength >= 474) {
				alertDialog = new AlertDialog.Builder(RoofDeck.this).create();
				alertDialog.setTitle("Exceeds Limit");
				alertDialog.setMessage("Comment text exceeds the limit");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								tempSt.setChecked(false);
								tempSt.setEnabled(true);
							}
						});
				alertDialog.show();
			} else {
				tempSt.setEnabled(false);
			}
		} else {
		}
	}

	private void getInspectortypeid() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.mbtblInspectionList,
				null);
		cur.moveToFirst();
		if (cur != null) {
			inspectortypeid = cur.getString(cur
					.getColumnIndex("InspectionTypeId"));
		}
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.rdio1:
				Vimage.setBackgroundResource(R.drawable.arrowdown);
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 3 Roof Deck Attachment, where mean lift is less than that of B and C Selections.";
				comments.setText(commentsfill);
				rdiochk = "1";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;
			case R.id.rdio2:
				Vimage.setBackgroundResource(R.drawable.arrowdown);
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "2";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(true);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;

				break;
			case R.id.rdio3:
				Vimage.setBackgroundResource(R.drawable.arrowdown);
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "3";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(true);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;

				break;
			case R.id.rdio4:
				Vimage.setBackgroundResource(R.drawable.arrowdown);
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "4";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(true);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;

				break;
			case R.id.rdio5:
				Vimage.setBackgroundResource(R.drawable.arrowdown);
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "5";
				rdioA.setChecked(false);
				txtother.setEnabled(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(true);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;
			case R.id.rdio6:
				Vimage.setBackgroundResource(R.drawable.arrowdown);
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "6";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(true);
				rdioG.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;
			case R.id.rdio7:
				Vimage.setBackgroundResource(R.drawable.arrowdown);
				commentsfill = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				comments.setText(commentsfill);
				rdiochk = "7";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(true);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;
			}
		}
	};

	public void clicker(View v) {

		switch (v.getId()) {
		
	
		case R.id.txthelpcontentoptionA:
			cf.alerttitle="A - Mean Uplift less than B and C";
		    cf.alertcontent="Plywood/oriented-strand board (OSB) roof sheathing attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by staples or 6d nails spaced at 6 inches along the edge and 12 inches in the field OR batten decking supporting wood shakes or wood shingles OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that has an equivalent mean uplift less than that required for options B or C below.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionB:
			cf.alerttitle="B - 103 PSF";
			cf.alertcontent="Plywood/OSB roof sheathing, with a minimum thickness of 7/16 inch, attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by 8d common nails spaced a maximum of 12 inches in the field OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that is shown to have an equivalent or greater resistance than 8d nails spaced a maximum of 12 inches in the field or has a mean uplift resistance of at least 103 psf.";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionC:
			cf.alerttitle="C - 182 PSF";
			cf.alertcontent="Plywood/OSB roof sheathing, with a minimum thickness of 7/16 inch, attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by 8d common nails spaced a maximum of 6 inches in the field OR dimensional lumber/tongue-and-groove decking with a minimum of two nails per board (or one nail per board if each board is equal to or less than 6 inches in width) OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that is shown to have an equivalent or greater resistance than 8d common nails spaced a maximum of 6 inches in the field or has a mean uplift resistance of at least 182 psf";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionD:
			cf.alerttitle="D - Concrete";
			cf.alertcontent="Reinforced concrete roof deck";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionE:
			cf.alerttitle="E - other";
		    cf.alertcontent="Other.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionF:
			cf.alerttitle="F - Unknown";
		    cf.alertcontent="Unknown or unidentified.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionG:
			cf.alerttitle="G - No Attic";
		    cf.alertcontent="No attic access.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.savenext:
			comm = comments.getText().toString();

			if ((rdioA.isChecked() == true) || (rdioB.isChecked() == true)
					|| (rdioC.isChecked() == true)
					|| (rdioD.isChecked() == true)
					|| (rdioE.isChecked() == true)
					|| (rdioF.isChecked() == true)
					|| (rdioG.isChecked() == true)) {
				if (rdioE.isChecked() == true) {
					othertext = txtother.getText().toString();
					if (othertext.trim().equals("")) {
						cf.ShowToast("Please enter the text for Other Roof Deck Attachment.",1);
						txtother.requestFocus();
						chkstatus = "false";
					} else {
						chkstatus = "true";
					}
				}

				if (comm.trim().equals("")) {
					cf.ShowToast("Please enter the Comments for Roof Deck.",1);
					comments.requestFocus();
					updatecnt = "0";
				} else {
					updatecnt = "1";

					if (chkstatus.equals("true") && updatecnt.equals("1")) {

						try {
							Cursor c2 = cf.db.rawQuery("SELECT * FROM "
									+ cf.tbl_questionsdata + " WHERE SRID='"
									+ cf.Homeid + "'", null);
							int rws = c2.getCount();
							if (rws == 0) {
								cf.db.execSQL("INSERT INTO "
										+ cf.tbl_questionsdata
										+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
										+ " VALUES ('" + cf.Homeid
										+ "','','','','" + 0 + "','" + 0
										+ "','','','','','" + 0 + "','"
										+ rdiochk + "','"
										+ cf.convertsinglequotes(othertext)
										+ "','" + 0 + "','" + 0 + "','" + 0
										+ "','" + 0 + "','" + 0 + "','','" + 0
										+ "','" + 0 + "','" + 0 + "','','" + 0
										+ "','','" + 0 + "','" + 0
										+ "','','','','','','','','" + 0
										+ "','" + 0 + "','" + 0 + "','" + 0
										+ "','" + 0 + "','" + 0 + "','" + 0
										+ "','" + 0 + "','','" + 0 + "','" + 0
										+ "','','" + 0 + "','','','" + cd
										+ "','" + 0 + "')");
							} else {
								cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
										+ " SET RoofDeckValue='" + rdiochk
										+ "',RoofDeckOtherText='"
										+ cf.convertsinglequotes(othertext)
										+ "'" + " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");
							}

						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
							//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in saving roofdeck values because of invalid input on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

						}
						try {

							cf.getInspectorId();
							Cursor c2 = cf.db.rawQuery("SELECT * FROM "
									+ cf.tbl_comments + " WHERE SRID='"
									+ cf.Homeid + "'", null);
							int rws = c2.getCount();
							if (rws == 0) {
								cf.db.execSQL("INSERT INTO "
										+ cf.tbl_comments
										+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment,RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
										+ " VALUES ('"
										+ cf.Homeid
										+ "','"
										+ cf.InspectionType
										+ "','','','"
										+ cf.convertsinglequotes(comments
												.getText().toString())
										+ "','','','','','','','','','','','','','','','','','"
										+ cd + "')");

							} else {
								cf.db.execSQL("UPDATE "
										+ cf.tbl_comments
										+ " SET RoofDeckComment='"
										+ cf.convertsinglequotes(comments
												.getText().toString())
										+ "',RoofDeckAdminComment='',CreatedOn ='"
										+ md + "'" + " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");
							}
						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
							//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in saving roofdeck comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

						}
						cf.getInspectorId();

						try {
							Cursor c3 = cf.db.rawQuery("SELECT * FROM "
									+ cf.SubmitCheckTable + " WHERE Srid='"
									+ cf.Homeid + "'", null);
							int subchkrws = c3.getCount();
							if (subchkrws == 0) {
								cf.db.execSQL("INSERT INTO "
										+ cf.SubmitCheckTable
										+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
										+ "VALUES ('" + cf.InspectorId + "','"
										+ cf.Homeid
										+ "',0,0,0,1,0,0,0,0,0,0,0,0,0,0,0)");
							} else {
								cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
										+ " SET fld_roofdeck='1' WHERE Srid ='"
										+ cf.Homeid + "' and InspectorId='"
										+ cf.InspectorId + "'");
							}
						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invlaid character.",1);
							//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in inserting or updating roofdeck values on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

						}

					}
				}
			} else {
				cf.ShowToast("Please select the Roof Deck Attachment.",1);

			}
			if (updatecnt.equals("1") && chkstatus.equals("true")) {
				cf.changeimage();
				// roofdecktick.setVisibility(visibility);
				cf.ShowToast("Roof deck details has been saved successfully.",1);
				iInspectionList = new Intent(RoofDeck.this, RoofWall.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("iden", "test");
				iInspectionList.putExtra("InspectionType", cf.InspectionType);
				iInspectionList.putExtra("status", cf.status);
				iInspectionList.putExtra("keyName", cf.value);
				iInspectionList.putExtra("Count", cf.Count);
				startActivity(iInspectionList);
			}

			break;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(RoofDeck.this, RoofCover.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("iden", "prev");
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	    if(focus==1)
	    {
	    	focus=0;
	    comments.setText(comments.getText().toString());    
	    } 
	 }    
}
