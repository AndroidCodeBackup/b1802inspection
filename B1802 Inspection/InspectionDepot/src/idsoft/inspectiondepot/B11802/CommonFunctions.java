package idsoft.inspectiondepot.B11802;


import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CommonFunctions {

	public String URL_IDMA ="http://72.15.221.153:91/AndroidWebService.asmx"; //live
	public String URL = "http://72.15.221.153:90/NewWebServieceForAndroid/AndroidWebService.asmx"; // live
	public String URL_PDF = "https://www.paperlessinspectors.com/retailinspection/singlelogin/SingleLoginWebService.asmx"; //live 
	
	
	//public String URL="http://72.15.221.151:83/NewWebServieceForAndroid/AndroidWebService.asmx"; // UAT
	//public String URL_IDMA="http://72.15.221.151:89/AndroidWebService.asmx"; // UAT
	//public String URL_PDF = "http://www.paperless-inspector.com/retailinspection/singlelogin/SingleLoginWebService.asmx"; //UAT
	
	public String NAMESPACE = "http://tempuri.org/";

	public String METHOD_NAME = "CheckUserAuthentication";
	public String SOAP_ACTION = "http://tempuri.org/CheckUserAuthentication";

	public String SOAP_ACTION1 = "http://tempuri.org/InspectorDetail";
	public String METHOD_NAME1 = "InspectorDetail";

	public String SOAP_ACTION2 = "http://tempuri.org/ExportDeviceInformation";
	public String METHOD_NAME2 = "ExportDeviceInformation";

	public static final String SOAP_ACTION3 = "http://tempuri.org/GetVersionInformation";
	public static final String METHOD_NAME3 = "GetVersionInformation";         

	public static final String SOAP_ACTION4 = "http://tempuri.org/UpdateMobileDB";
	public static final String METHOD_NAME4 = "UpdateMobileDB";
     
	public static final String METHOD_NAME5 = "LoadCloudInformation_New";
	public static final String SOAP_ACTION5 = "http://tempuri.org/LoadCloudInformation_New";

	public static final String SOAP_ACTION6 = "http://tempuri.org/LoadAgentInformation_New";
	public static final String METHOD_NAME6 = "LoadAgentInformation_New";

	public static final String SOAP_ACTION7 = "http://tempuri.org/LoadCommandlibraryFor1802";
	public static final String METHOD_NAME7 = "LoadCommandlibraryFor1802";

	public static final String METHOD_NAME8 = "LoginHistory";
	public static final String SOAP_ACTION8 = "http://tempuri.org/LoginHistory";

	public static final String SOAP_ACTION9 = "http://tempuri.org/LoadOriginalDataText_New";
	public static final String METHOD_NAME9 = "LoadOriginalDataText_New";

	public static final String SOAP_ACTION10 = "http://tempuri.org/GetCurrentAPKFile";
	public static final String METHOD_NAME10 = "GetCurrentAPKFile";

	public static final String SOAP_ACTION11 = "http://tempuri.org/LoadCallAttempt";
	public static final String METHOD_NAME11 = "LoadCallAttempt";

	public static final String SOAP_ACTION12 = "http://tempuri.org/ExportUTSCallAttempt";
	public static final String METHOD_NAME12 = "ExportUTSCallAttempt";

	public static final String SOAP_ACTION13 = "http://tempuri.org/ExportCallAttempt";
	public static final String METHOD_NAME13 = "ExportCallAttempt";

	public static final String METHOD_NAME14 = "ExportWSI1802ElevationPicture";
	public static final String SOAP_ACTION14 = "http://tempuri.org/ExportWSI1802ElevationPicture";

	public static final String METHOD_NAME15 = "UpdateInspectionStatus";
	public static final String SOAP_ACTION15 = "http://tempuri.org/UpdateInspectionStatus";

	public static final String METHOD_NAME16 = "ExportWSIInsComments";
	public static final String SOAP_ACTION16 = "http://tempuri.org/ExportWSIInsComments";
	
	public static final String METHOD_NAME17 = "ExportWSI1802RoofCoverType";
	public static final String SOAP_ACTION17 = "http://tempuri.org/ExportWSI1802RoofCoverType";

	public static final String METHOD_NAME18 = "ExportWSI1802GenHzdImages";
	public static final String SOAP_ACTION18 = "http://tempuri.org/ExportWSI1802GenHzdImages";

	public static final String METHOD_NAME19 = "ExportWSI1802GenHzdData";
	public static final String SOAP_ACTION19 = "http://tempuri.org/ExportWSI1802GenHzdData";

	public static final String METHOD_NAME20 = "ExportWSI1802FeedbackDocument";
	public static final String SOAP_ACTION20 = "http://tempuri.org/ExportWSI1802FeedbackDocument";

	public static final String METHOD_NAME21 = "ExportWSI1802FeedbackInformation";
	public static final String SOAP_ACTION21 = "http://tempuri.org/ExportWSI1802FeedbackInformation";

	public static final String SOAP_ACTION22 = "http://tempuri.org/ExportWSI1802Questions";
	public static final String METHOD_NAME22 = "ExportWSI1802Questions";

	public static final String SOAP_ACTION23 = "http://tempuri.org/ExportData";
	public static final String METHOD_NAME23 = "ExportData";

	public static final String SOAP_ACTION24 = "http://tempuri.org/UpdateMobileDBCount";
	public static final String METHOD_NAME24 = "UpdateMobileDBCount";

	public static final String SOAP_ACTION25 = "http://tempuri.org/SearchPolicyHolderInfo";
	public static final String METHOD_NAME25 = "SearchPolicyHolderInfo";

	public static final String SOAP_ACTION26 = "http://tempuri.org/AndroidErrorLogFile";
	public static final String METHOD_NAME26 = "AndroidErrorLogFile";

	public static final String SOAP_ACTION27 = "http://tempuri.org/ExportScheduleInfo";
	public static final String METHOD_NAME27 = "ExportScheduleInfo";

	public static final String SOAP_ACTION28 = "http://tempuri.org/GetInspectionStatus";
	public static final String METHOD_NAME28 = "GetInspectionStatus";
	
	public static final String SOAP_ACTION29 = "http://tempuri.org/OnlineSyncPolicyInfo";
	public static final String METHOD_NAME29 = "OnlineSyncPolicyInfo";
	
	public static final String SOAP_ACTION30 = "http://tempuri.org/TrackAndroidIsComplete";
	public static final String METHOD_NAME30 = "TrackAndroidIsComplete";

	public static final String SOAP_ACTION31 = "http://tempuri.org/IsCurrentInspector";
	public static final String METHOD_NAME31 = "IsCurrentInspector";	
	
	public static final String SOAP_ACTION32 = "http://tempuri.org/GetCurrentAPKFile_new";
	public static final String METHOD_NAME32 = "GetCurrentAPKFile_new";
	
	public static final String SOAP_ACTION33 = "http://tempuri.org/GetAdditionalInfo_new";
	public static final String METHOD_NAME33 = "GetAdditionalInfo_new";
	
	public static final String SOAP_ACTION34 = "http://tempuri.org/SendReportToIT";
	public static final String METHOD_NAME34 = "SendReportToIT";
	
	public static final String SOAP_ACTION35 = "http://tempuri.org/GetExportCount";
	public static final String METHOD_NAME35 = "GetExportCount";
	
	public String MY_DATABASE_NAME = "InspectionDepotDatabases";
	public String redcolor ="<font color=red> * </font>";
	public String welcotxtcolor ="<font color=#f7a218> * </font>",auditflag;
	SQLiteDatabase db;
	public int maxindb =8;
	public static final int CAPTURE_PICTURE_INTENT = 0;
	public CharSequence datewithtime;
	public Button btn_helpclose,btn_camcaptclos;
	public int currnet_rotated=0;
	protected Bitmap rotated_b;
	ImageView img1, img2, img3, img4, img6, img7, img8, img9;
	public static final String inspectorlogin = "tbl_inspectorlogin";
	public static final String mbtblInspectionList = "Retail_inpgeneralinfo";
	public static final String cloudtable = "cloud_information";
	public static final String agentinfo = "tbl_agent";
	public static final String tbl_admcomments = "tabless_commentsadd";
	public static final String quetsionsoriginaldata = "ques_original";
	public static final String tbl_questionsdata = "tbl_quescomm";
	public static final String tbl_comments = "tbl_comm";
	public static String SubmitCheckTable = "tbl_chkforsubmit";
	public static final String FeedBackInfoTable = "WSI1802FeedbackInformation";
	public static final String FeedBackDocumentTable = "WSI1802FeedbackDocuments";
	public static final String quesroofcover = "tbl_quesroofcover";
	public static final String tblphotocaption = "tbl_photocaption";
	public static final String additionalservice = "additional_service";
	public static final String IDMAVersion="ID_Version";
	public static String ImageTable = "WSI1802ElevationPicture";
	public static String HazardImageTable = "WSI1802GenHzdImages";
	public static String HazardDataTable = "WSI1802GenHzdData";
	public static String VersionshowTable = "VersionshowTable";
	public static String OnlineTable = "OnlineDataTable";
	public static String count_tbl = "Count_tbl";
	public String rcstr = "RC U 10-24-2013",Inspectiontypeid="",LicenceExpiryDate="";
	public Uri mCapturedImageURI;
	public Toast toast;
	Context con;
	public View show;
	public String newphone;
	public TextView cameratxt,tvcamhelp,releasecode,policyholderinfo;
	public String InspectorId, Homeid, InspectionType, inspectiondate,versionname,
			buildcodevalue = "0", roofdeckvalueprev = "0",alerttitle,alertcontent,
			rooftowallvalueprev = "0", roofgeovalue = "0", roofswrvalue = "0",
			openprovalue = "0", woodframeper = "0", reper = "0", unreper = "0",
			pcnper = "0", otrper = "0";
	public SoapObject onlresult;
	public String elevtype,elevid;
	public android.text.format.DateFormat df;
	public TextView SQ_TV_type1,SQ_TV_type2,txtversion;
	public String insfname, Identity,deviceId,model,manuf,devversion,apiLevel,picpath="";
	public String inslname,data,elev_name,Ins_Email,insaddress,inscompname,insemail;
	public int Current_year,mYear, mMonth, mDay;
	public String[] elevnames={"Select Elevation","Front Elevation","Right Elevation","Back Elevation","Left Elevation","Attic Elevation","Additional Elevation","Exterior and Grounds","Interior","Additional Images"};
	public ArrayAdapter adapter2;
	public ProgressDialog pd;
	public String colorname,status,strerrorlog,exprtcnt,selectedhomeid="",name="",checkInspectorAvail="false",Chk_Inspector="false";
	public int value,quesid=0,elev,onlretassign;
	public View v1;public CheckBox schchk;
	public DisplayMetrics dm;
	public int Density=160;
    public float dnt=(float)1.0;
	public LinearLayout SQ_ED_type1_parrant,SQ_ED_type1,SQ_ED_type2_parrant,SQ_ED_type2;
	public int Count, chkrwss, slenght,spinelevval,sp=0;
	public Dialog dialog1,dialog2;
	public boolean application_sta;
	public Spinner elevspin,captionspin;
	public int wd,ht;
	public String newcode,newversion;
	public RelativeLayout spinvw,capvw;public static final int SELECT_PICTURE = 0;
	public String methodname,commentsdescription,Insp_id="",newspin;
	public String Version; 
	public ArrayElevation arrelev = new ArrayElevation();
	public String bulkinsertstring="";
	public boolean db_avb=false; 
	
	CommonFunctions(Context con) {
		this.con = con;
		PackageManager m = con.getPackageManager();
		String s = con.getPackageName();
		PackageInfo p;
		try {
			
			p = m.getPackageInfo(s, 0);
			s = p.applicationInfo.dataDir;
			File f=new File(s+"/databases/"+MY_DATABASE_NAME);
			
	        if(f.exists())
	        {
	        	this.db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
	        	PackageManager packageManager = con.getPackageManager();
	        	ComponentName componentName = new ComponentName(con,Dummy_luncher.class);
	        	packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP);
	        }
	        else
	        {
	        	
	        	 this.db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
	        	 String currentDBPath = "/data/com.inspectiondepot/databases/"+MY_DATABASE_NAME;
	             //String backupDBPath = MY_DATABASE_NAME;
	        	 File data = Environment.getDataDirectory();
	             File currentDB = new File(data, currentDBPath);
	             File backupDB = new File(f.toString());
	            
	             if(currentDB.exists())
	             {
	            	 try
	            	 {
	            		
	            	 FileChannel src = new FileInputStream(currentDB).getChannel();
	                 FileChannel dst = new FileOutputStream(backupDB).getChannel();
	                
	                 dst.transferFrom(src, 0, src.size()); 
	                 
	                 src.close();
	                 dst.close();
	                 
	                 db_avb=true;
	            	 }
	            	 catch (Exception e) {
						// TODO: handle exception
	            		 System.out.println("Exception"+e.getMessage());
					}

	             }
	             else
	             {
	            	 this.db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
	             }
	                 
	        }
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error occure"+e.getMessage());
		}
		this.db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		final Calendar c = Calendar.getInstance();
		this.Current_year = c.get(Calendar.YEAR);
		df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		this.con = con;		
		
		/***checking for the All risk report application **/
		final PackageManager pm = con.getPackageManager();
        //get a list of installed apps.
		        List<ApplicationInfo> packages = pm
		                .getInstalledApplications(PackageManager.GET_META_DATA);
		
        for (ApplicationInfo packageInfo : packages) {

	          
			  if(packageInfo.packageName.toString().trim().equals("idsoft.inspectiondepot.IDMA")) // for checking the main app has installed  or not 
	            {
				  application_sta=true; // check if the main appliccation IDinspection has installed  or not 
	            }
			  if(packageInfo.packageName.toString().trim().equals("com.inspectiondepot")) // for checking the main app has installed  or not 
	            {
				  db_avb=true; // check if the main appliccation IDinspection has installed  or not 
	            }
        }
        /***checking for the All risk report application Ends here  **/

        //get_density_frm_db();   /*** chnage the density of the device based on the device ***/

	}
	public void get_density_frm_db()
{
	
	try
	{
		 /***checking for the All risk report application Ends here  **/
        Activity activity = (Activity) this.con;
        /** Set the medium density for the Screen Starts**/
		dm = new DisplayMetrics(); // Creat new Display metrics class 
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm); // set the Current Display metrics value to the new   
		/**Seting the customized value to the Display metrics**/
		
		
		int density=((dm.widthPixels*160)/1024); // Calculate the densitydpi for the currnet width 
		System.out.println("new Density"+density);
		           
		if(density>=120)
		{
			    
		float dnt= (float) ((float)density/160.00); // calcul ate the  density for the Width 
		//System.out.println("Dnst"+dnt); 
		dm.density=dnt;
		dm.densityDpi=density;
		dm.scaledDensity=dnt;    
		dm.xdpi=density;
		dm.ydpi=density;
		Density=density;
		this.dnt=dnt;
		System.out.println("else Density = "+Density+"dnt "+dnt);
		//dm.widthPixels=1024;
		/**Seting the customized value to the Display metrics Ends**/
		activity.getApplicationContext().getResources().getDisplayMetrics().setTo(dm); // Set the new class to the display metrics
				
		}
		else
		{
			dm.density=dnt;
			dm.densityDpi=Density;
			dm.scaledDensity=dnt;    
			dm.xdpi=Density;
			dm.ydpi=Density;
			Density=Density;
			this.dnt=dnt;
			System.out.println("else Density = "+Density+"dnt "+dnt);
			//dm.widthPixels=1024;
			/**Seting the customized value to the Display metrics Ends**/
			activity.getApplicationContext().getResources().getDisplayMetrics().setTo(dm); // Set the new class to the display metrics
		}
		//db.execSQL("Insert into "+Settings_table+" (SId,S_name,S_value) values (1,'Density','"+density+":"+dnt+"') ");
		//}
	}
	catch (Exception e) {
		// TODO: handle exception
		System.out.println("The error Was "+e.getMessage());
	}
}

	public void Createtablefunction(int select) {
		switch (select) {
		case 1:
			/** Inspector Login **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ inspectorlogin
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,Ins_Id varchar(100),Ins_FirstName varchar(100),Ins_MiddleName varchar(100),Ins_LastName varchar(100),Ins_Address varchar(100),Ins_CompanyName varchar(100),Ins_CompanyId varchar(100),Ins_UserName varchar(100),Ins_Password varchar(100),Ins_Flag1 varchar(100),Ins_Flag2 varchar(100),Android_status varchar(100),Ins_ImageName VARCHAR(100),Ins_rememberpwd VARCHAR(100),Ins_Email VARCHAR(100),Ins_PhotoExtn VARCHAR(100) DEFAULT '');");
			} catch (Exception e) {
				System.out.println("INSPECTOR LOGIN TABLE not");
			}
			break;
		case 2:
			/** Policyholder **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ mbtblInspectionList
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,s_SRID varchar(50),s_OwnersNameFirst varchar(100),s_OwnersNameLast varchar(100),MiddleName varchar(100),s_propertyAddress varchar(100),Address2 varchar(100),s_City varchar(100),s_State varchar(100),s_County varchar(100),s_ZipCode	varchar(100),s_YearBuilt varchar(100),s_ContactHomePhone varchar(100),s_ContactWorkPhone varchar(100),s_ContactCellPhone varchar(100),s_ContactPerson varchar(100),s_OwnerPolicyNumber varchar(100),s_InsuranceCompany varchar(100),s_ContactEmail varchar(100),s_Nstories varchar(100),IsInspected varchar(100),IsUploaded varchar(100),Status varchar(100),InsurcarrierId varchar(100),InspectorId varchar(100),wId varchar(100),CompanyId varchar(100),InspectorFirstName varchar(100),InspectorLastName varchar(100),ScheduleDate varchar(100),InspectionStartTime varchar(100),InspectionEndTime varchar(100),InspectionComment varchar(200),InspectionTypeId varchar(100),Insurancerenewaldate Varchar(100),InspectionDate varchar(100),InsuranceCompanyname varchar(100),chkbx int,SchFlag int,SubStatus VARCHAR(100),BuildingSize VARCHAR(10),TPQA VARCHAR(50) default '',Additionalservice VARCHAR(50) default '',QAInspector VARCHAR(50) default '',Licenceexpirydate varchar(50) default '',AuditFlag varchar(25) default '0');");

			} catch (Exception e) {
				
			}
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ count_tbl
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,insp_id varchar(50),C_28_pre varchar(5) Default('0'),C_28_uts varchar(5) Default('0'),C_28_can varchar(5) Default('0'),C_28_RR varchar(5) Default('0')," +
						"C_29_pre varchar(5) Default('0'),C_29_uts varchar(5) Default('0'),C_29_can varchar(5) Default('0'),C_29_RR varchar(5) Default('0'));");

			} catch (Exception e) {
				
			}
			
			
			break;
		case 3:
			/** cloud information **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ cloudtable
						+ "(cid INTEGER PRIMARY KEY AUTOINCREMENT,Homeid varchar(100),assignmentId 	REAL,assignmentMasterId REAL,inspectionPolicy_Id REAL,priorAssignmentId REAL"
						+ ",inspectionType varchar(100),assignmentType varchar(100),policyId varchar(100),policyVersion varchar(100),policyEndorsement varchar(100),policyEffectiveDate varchar(100),policyForm varchar(100),"
						+ "policySystem varchar(100),lob varchar(100),structureCount varchar(100),structureNumber varchar(100),structureDescription varchar(100),propertyAddress varchar(100),propertyAddress2 varchar(100),"
						+ "propertyCity varchar(100),propertyState varchar(100),propertyCounty varchar(100),propertyZip varchar(100),insuredFirstName varchar(100),insuredLastName varchar(100),insuredHomePhone varchar(100),insuredWorkPhone varchar(100),"
						+ "insuredAlternatePhone varchar(100),insuredEmail varchar(100),insuredMailingAddress varchar(100),insuredMailingAddress2 varchar(100),insuredMailingCity varchar(100),insuredMailingState varchar(100),insuredMailingZip varchar(100),"
						+ "agencyID varchar(100),agencyName varchar(100),agencyPhone varchar(100),agencyFax varchar(100),agencyEmail varchar(100),agencyPrincipalFirstName varchar(100),agencyPrincipalLastName varchar(100),agencyPrincipalEmail VARCHAR"
						+ ",agencyFEIN varchar(100),agencyMailingAddress varchar(100),agencyMailingAddress2 varchar(100),agencyMailingCity varchar(100),agencyMailingState varchar(100),agencyMailingZip varchar(100),agentID varchar(100),"
						+ "agentFirstName varchar(100),agentLastName varchar(100),agentEmail varchar(100),previousInspectionDate varchar(100),previousInspectionCompany varchar(100),previousInspectorFirstName varchar(100),"
						+ "previousInspectorLastName  varchar(100),previousInspectorLicense varchar(100),yearBuilt varchar(100),squareFootage varchar(100),numberOfStories varchar(100),construction varchar(100),roofCovering varchar(100),"
						+ "roofDeckAttachment varchar(100),roofWallAttachment varchar(100),roofShape varchar(100),secondaryWaterResistance varchar(100),openingCoverage varchar(100),buildingType VARCHAR(100));");

			} catch (Exception e) {
			
			}
			break;
		case 4:
			/** Agent Table **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ agentinfo
						+ " (AId INTEGER PRIMARY KEY AUTOINCREMENT,Homeid varchar(100),agencyname varchar(100),agentname varchar(100),address1 varchar(100),address2 varchar(100),city varchar(100),country varchar(100),role varchar(100),state varchar(100),zip varchar(100),offphone varchar(100),contactphone varchar(100),fax varchar(100),email varchar(100),website VARCHAR(100));");

			} catch (Exception e) {
			
			}
			break;
		case 5:
			/** Comments Table **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ tbl_admcomments
						+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,InspectorId varchar(100),questionid INT NOT NULL DEFAULT ' ',optionid VARCHAR (100) NOT NULL DEFAULT ' ',optionvalue VARCHAR (50) NOT NULL DEFAULT ' ',description VARCHAR (50) NOT NULL DEFAULT ' ',status VARCHAR (50) NOT NULL DEFAULT ' ');");

			} catch (Exception e) {
				
			}
			break;
		case 6:
			/** Original data for questions **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ quetsionsoriginaldata
						+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,homeid varchar(100),bcoriginal varchar(100),rcoriginal varchar(100),rdoriginal varchar(100),rworiginal varchar(100),rgoriginal varchar(100),swroriginal varchar(100),oporiginal varchar(100),wcoriginal Varchar(100));");

			} catch (Exception e) {
				
			}
			break;
		case 7:
			/** Question comments **/
			try {

				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ tbl_questionsdata
						+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,SRID VARCHAR (50) NOT NULL DEFAULT ' ',BuildingCodeYearBuilt VARCHAR (50) NOT NULL DEFAULT ' ',BuildingCodeValue int NOT NULL DEFAULT '0',BuildingCodePerApplnDate VARCHAR (50) NOT NULL DEFAULT ' ',RoofCoverType int NOT NULL DEFAULT ' ',RoofCoverValue int NOT NULL DEFAULT '0',RoofCoverOtherText VARCHAR (100) DEFAULT '',RoofCoverPerApplnDate VARCHAR (50) NOT NULL DEFAULT '',RoofCoverYearofInsDate VARCHAR (50) NOT NULL DEFAULT ' ',RoofCoverProdAppr VARCHAR (50) NOT NULL DEFAULT ' ',RoofCoverNoInfnProvide int  DEFAULT ' ',RoofDeckValue int NOT NULL DEFAULT '0',RoofDeckOtherText VARCHAR (100) NOT NULL DEFAULT '0',RooftoWallValue int DEFAULT ' ',RooftoWallSubValue int NOT NULL DEFAULT '0',RooftoWallClipsMinValue int NOT NULL DEFAULT '0',RooftoWallSingleMinValue int NOT NULL DEFAULT '0',RooftoWallDoubleMinValue int NOT NULL DEFAULT '0',RooftoWallOtherText VARCHAR (100) NOT NULL DEFAULT '0',RoofGeoValue int NOT NULL DEFAULT '',RoofGeoLength int NOT NULL DEFAULT '0',RoofGeoTotalArea int NOT NULL DEFAULT '0',RoofGeoOtherText VARCHAR (100) DEFAULT '',SWRValue int NOT NULL DEFAULT '',OpenProtectType VARCHAR (100) NOT NULL DEFAULT '',OpenProtectValue int NOT NULL DEFAULT '',OpenProtectSubValue int,OpenProtectLevelChart varchar(100),GOWindEntryDoorsValue VARCHAR (100),GOGarageDoorsValue VARCHAR (100),GOSkylightsValue VARCHAR (100),NGOGlassBlockValue VARCHAR (100),NGOEntryDoorsValue VARCHAR (100),NGOGarageDoorsValue VARCHAR (100),WoodFrameValue int,WoodFramePer int,ReinMasonryValue int,ReinMasonryPer int,UnReinMasonryValue int,UnReinMasonryPer int,PouredConcrete int,PouredConcretePer int,OtherWallTitle varchar(50),OtherWal int,OtherWallPer int,EffectiveDate varchar(50),OverallComments varchar(100),Completed int,ScheduleComments varchar(500),ModifyDate datetime,GeneralHazardInclude Integer);");
			} catch (Exception e) {
				
			}
			break;
		case 8:
			/** comments **/

			try {

				db.execSQL("CREATE TABLE IF NOT EXISTS " + tbl_comments
						+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
						+ "SRID VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "i_InspectionTypeID int NOT NULL DEFAULT '0',"
						+ "BuildingCodeComment varchar(510),"
						+ "BuildingCodeAdminComment VARCHAR (510),"
						+ "RoofCoverComment varchar(510),"
						+ "RoofCoverAdminComment VARCHAR (510),"
						+ "RoofDeckComment varchar(510),"
						+ "RoofDeckAdminComment VARCHAR (510),"
						+ "RoofWallComment varchar(510),"
						+ "RoofWallAdminComment VARCHAR (510),"
						+ "RoofGeometryComment varchar(510),"
						+ "RoofGeometryAdminComment VARCHAR (510),"
						+ "GableEndBracingComment varchar(510),"
						+ "GableEndBracingAdminComment VARCHAR (510),"
						+ "WallConstructionComment varchar(510),"
						+ "WallConstructionAdminComment VARCHAR (510),"
						+ "SecondaryWaterComment varchar(510),"
						+ "SecondaryWaterAdminComment VARCHAR (510),"
						+ "OpeningProtectionComment varchar(510),"
						+ "OpeningProtectionAdminComment VARCHAR (510),"
						+ "InsOverAllComments VARCHAR (510),"
						+ "CreatedOn datetime);");

			} catch (Exception e) {
				
				
			}
			break;
		case 9:
			/** submit check table **/

			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ SubmitCheckTable
						+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,InspectorId varchar(100),Srid varchar(100),fld_policy int,fld_builcode int,fld_roofcover int,fld_roofdeck int,fld_roofwall int,fld_roofgeo int,fld_swr int,fld_open int,fld_wall int,fld_signature int,fld_front int,fld_feedbackinfo int,fld_feedbackdoc int,fld_overall int,fld_hazarddata int);");

			} catch (Exception e) {
				
			}

			break;
		case 10:
			/** Feedback info table **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + FeedBackInfoTable + " ");
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ FeedBackInfoTable
						+ " (Wsid INTEGER PRIMARY KEY Not null,Srid Varchar(50) ,IsCusServiceCompltd Bit Not null Default(0),PresentatInspection Varchar(50) ,IsInspectionPaperAvbl Bit Not null Default(0),IsManufacturerInfo Bit Not null Default(0),FeedbackComments Varchar(2000),CreatedOn Datetime Null,othertxt Varchar(50));");

			} catch (Exception e) {
				
			}

			break;
		case 11:
			/** Creating feedbackdocuments table **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + FeedBackDocumentTable + " ");
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ FeedBackDocumentTable
						+ " (DocumentId INTEGER PRIMARY KEY Not null,SRID Varchar(50) ,InspectionTypeId int Not null,DocumentTitle Varchar(50),FileName Varchar(50) ,Nameext Varchar(150),ImageOrder INTEGER Not null default(0),IsOfficeUse Bit Not null Default(0),CreatedOn Datetime Null,ModifiedDate Datetime Null);");

			} catch (Exception e) {
				
			}
			break;
		case 12:
			/** creating roofcover table **/

			try {

				db.execSQL("CREATE TABLE IF NOT EXISTS " + quesroofcover
						+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
						+ "SRID VARCHAR (50) NOT NULL DEFAULT '',"
						+ "RoofCoverType VARCHAR (50) NOT NULL DEFAULT '0',"
						+ "RoofCoverValue int NOT NULL DEFAULT '0',"
						+ "PermitApplnDate1 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "PermitApplnDate2 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "PermitApplnDate3 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "PermitApplnDate4 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "PermitApplnDate5 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "PermitApplnDate6 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "RoofCoverTypeOther VARCHAR (100) DEFAULT '',"
						+ "ProdApproval1 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "ProdApproval2 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "ProdApproval3 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "ProdApproval4 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "ProdApproval5 VARCHAR (50) NOT NULL DEFAULT '',"
						+ "ProdApproval6 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "InstallYear1 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "InstallYear2 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "InstallYear3 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "InstallYear4 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "InstallYear5 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "InstallYear6 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "NoInfo1  VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "NoInfo2 VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "NoInfo3  VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "NoInfo4  VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "NoInfo5  VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "NoInfo6  VARCHAR (50) NOT NULL DEFAULT ' ',"
						+ "RoofPreDominant int NOT NULL DEFAULT '0');");

			}

			catch (Exception e) {
				
			}
			break;
		case 13:
			/** creating image table **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + ImageTable + " ");
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ ImageTable
						+ " (WSID INTEGER PRIMARY KEY Not null,SrID Varchar(50) ,Ques_Id INTEGER Not null Default(0),Elevation INTEGER Not null Default(0),ImageName Varchar(150) ,Nameext Varchar(150),Description Varchar(150),CreatedOn Datetime Null,ModifiedOn Datetime Null,ImageOrder INTEGER Not null default(0),Delflag INTEGER);");

			} catch (Exception e) {
				
			}
			break;
		case 14:
			/** creating general hazard table **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + HazardImageTable + " ");
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ HazardImageTable
						+ " (WSID INTEGER PRIMARY KEY Not null,SrID Varchar(50),Ques_Id INTEGER Not null Default(0),Elevation INTEGER Not null Default(0),ImageName Varchar(150) ,Description Varchar(150),CreatedOn Datetime Null,ModifiedOn Datetime Null,Nameext Varchar(150),ImageOrder INTEGER Not null default(0),Delflag INTEGER);");

			} catch (Exception e) {
				
			}
			break;
		case 15:
			/** creating hazard data **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + HazardDataTable + " ");
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ HazardDataTable
						+ " (GBId INTEGER PRIMARY KEY,Srid varchar(50),InsuredIs varchar(50),InsuredOther Varchar(50),OccupancyType varchar(50),OcccupOther Varchar(50),ObservationType varchar(50),ObservOther Varchar(50),OccupiedPercent varchar(50),OccupiedFlag Int,Vacant Int,VacantPercent varchar(50),NotDetermined Int,PermitConfirmed varchar(50),BuildingSize Int Not null Default(0),BalconyPresent varchar(50),AdditionalStructure varchar(50),OtherLocation varchar(500),Location1 varchar(50),Location2 varchar(50),Location3 varchar(50),Location4 varchar(50),Observation1 varchar(50),Observation2 varchar(50),Observation3 varchar(50),Observation4 varchar(50),Observation5 varchar(50),PerimeterPoolFence varchar(50),PoolFenceDisrepair varchar(50),SelfLatch varchar(50),ProfesInstall varchar(50),PoolPresent varchar(50),HotTubPresnt varchar(50),EmptyGroundPool varchar(50),PoolSlide varchar(50),DivingBoardPoolSlide varchar(50),PerimeterPoolEncl varchar(50),PoolEnclosure varchar(50),Vicious varchar(50),LiveStock varchar(50),OverHanging varchar(50),Trampoline varchar(50),SkateBoard varchar(50),bicycle varchar(50),TripHazardDesc varchar(50),TripHazardNoted varchar(50),UnsafeStairway varchar(50),PorchAndDeck varchar(50),NonStdConstruction varchar(50),OutDoorAppliances varchar(50),OpenFoundation varchar(50),WoodShingled varchar(50),ExcessDebris varchar(50),BusinessPremises varchar(50),GeneralDisrepair varchar(50),PropertyDamage varchar(50),StructurePartial varchar(50),InOperative varchar(50),RecentDrywall varchar(50),ChineseDrywall varchar(50),ConfirmDrywall varchar(50),NonSecurity varchar(50),NonSmoke varchar(50),Comments varchar(500));");

			} catch (Exception e) {
				
			}
			break;
		case 16:
			/** creating hazard data **/
			try {
				// db("DROP TABLE IF  EXISTS "
				// + HazardDataTable + " ");
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ VersionshowTable
						+ "(VId INTEGER PRIMARY KEY Not null,VersionCode varchar(50),VersionName varchar(50),ModifiedDate varchar(50));");

			} catch (Exception e) {
				
			}
			break;
		case 17:
			/** OnlineTable **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ OnlineTable
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,s_SRID varchar(50),InspectorId varchar(50),Status varchar(50),SubStatus varchar(50),InspectionTypeId varchar(50),s_OwnersNameFirst varchar(100),s_OwnersNameLast varchar(100),s_propertyAddress varchar(100),s_City varchar(100),s_State varchar(100),s_County varchar(100),s_ZipCode	varchar(100),s_OwnerPolicyNumber varchar(100),ScheduleDate varchar(100),InspectionStartTime varchar(100),InspectionEndTime varchar(100));");

			} catch (Exception e) {
				
			}
			break;
			
		case 18:
			/** PhotoCaption **/
			try {
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ tblphotocaption
						+ " (Id INTEGER PRIMARY KEY Not null,fld_photocaption varchar,fld_elevtype varchar(100),fld_elevid varchar(100));");

			} catch (Exception e) {
				   
				
			}
			break;

		case 19:
			/** Additional Service **/
			try {
					db.execSQL("CREATE TABLE IF NOT EXISTS "
					    + additionalservice + " (Id INTEGER PRIMARY KEY Not null,SRID  Varchar(50) ,InspectorId Varchar(50),IsRecord boolean,UserTypeName Varchar(50),ContactEmail varchar(50),PhoneNumber varchar(15),MobileNumber varchar(15),BestTimetoCallYou varchar(50),FirstChoice varchar(100),SecondChoice varchar(50),ThirdChoice varchar(50),FeedbackComments varchar(500));");
				

			} catch (Exception e) {
			
			}
			break;
		case 20:/** CREATING IDMA VERSION TABLE **/
			try {
				
				db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ IDMAVersion
						+ "(VId INTEGER PRIMARY KEY Not null,ID_VersionCode varchar(50),ID_VersionName varchar(50),ID_VersionType varchar(50));");

			} catch (Exception e) {
				
				
			}
			break;
		}

	}
	
    public void dropTable(int select) {
		switch (select) {
		case 17:
			/** drop OnlineTable **/
			try {
				db.execSQL("DROP TABLE IF EXISTS "+ OnlineTable );
				
			} catch (Exception e) {
				strerrorlog="Problem in deleting online table";
			
			}
			break;
		}
    }
	public Cursor SelectTablefunction(String tablename, String wherec) {
		Cursor cur = null;
		try {
			cur = db.rawQuery("select * from " + tablename + " " + wherec, null);

		}

		catch (Exception e) {
			cur = null;
			System.out.println("tablename" + e.getMessage());
			strerrorlog="Problem in selecting "+ tablename;
		
		}
		return cur;

	}
	public String Email_Validation(String string) {
		// TODO Auto-generated method stub
	
			Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
  		Matcher emailMatcher = emailPattern.matcher(string);
  		if (emailMatcher.matches()) {
              return "Yes";
    		} else {
    			return "No";
    		}
	}
	public void showhelpalert(String alertcontent) {
		// TODO Auto-generated method stub
		
		AlertDialog alertDialog = new AlertDialog.Builder(this.con).create();
		alertDialog.setMessage(alertcontent);
		alertDialog.setButton("OK",
		new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
			}
		});
		alertDialog.show();
	}
	public void Device_Information()
	{
		deviceId = Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		apiLevel = android.os.Build.VERSION.SDK;
		
	}
	public void showhelp(String alerttitle,String alertcontent) {
		// TODO Auto-generated method stub
		
		final Dialog dialog = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.alert);
		TextView txt = (TextView) dialog.findViewById(R.id.txtid);
		TextView helptxt = (TextView) dialog.findViewById(R.id.txthelp);
		
		txt.setText(Html.fromHtml(alertcontent));
		helptxt.setText(Html.fromHtml(alerttitle));
		/*Animation zoo= AnimationUtils.loadAnimation(this.con, R.anim.marquee_in);
		txt.startAnimation(zoo);*/
		Button btn_helpclose = (Button) dialog.findViewById(R.id.helpclose);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
			
		});
		dialog.setCancelable(true);
		dialog.show();
		
	}
	public void fn_delete(String dt) {
		// TODO Auto-generated method stub
		System.out.println("SRID "+dt);
		Createtablefunction(2);Createtablefunction(7);Createtablefunction(8);Createtablefunction(10);
		Createtablefunction(11);Createtablefunction(13);Createtablefunction(14);Createtablefunction(15);
		try {
			db.execSQL("DELETE FROM " + mbtblInspectionList
					+ " WHERE s_SRID ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			db.execSQL("DELETE FROM " + tbl_questionsdata
					+ " WHERE SRID ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			db.execSQL("DELETE FROM " + tbl_comments
					+ " WHERE SRID ='" + dt + "'");
		} catch (Exception e) {
		}
		try {
			db.execSQL("DELETE FROM " + ImageTable
					+ " WHERE SrID ='" + dt + "'");
		} catch (Exception e) {
		}
		try {
			db.execSQL("DELETE FROM " + FeedBackInfoTable
					+ " WHERE Srid ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			db.execSQL("DELETE FROM " + FeedBackDocumentTable
					+ " WHERE SRID ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			db.execSQL("DELETE FROM " + HazardDataTable
					+ " WHERE Srid ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			db.execSQL("DELETE FROM " + HazardImageTable
					+ " WHERE SrID ='" + dt + "'");
		} catch (Exception e) {

		}

		ShowToast("Deleted sucessfully.",1);
	}
	public void delete_all(final String where,String atitle) {
		// TODO Auto-generated method stub
		
		
		 	alerttitle="Delete";
		    alertcontent=atitle;
		    final Dialog dialog1 = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alertsync);
			TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
			txttitle.setText(alerttitle);
			TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
			txt.setText(Html.fromHtml( alertcontent));
			Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
			btn_yes.setOnClickListener(new OnClickListener()
			{
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					Createtablefunction(2);Createtablefunction(7);Createtablefunction(8);Createtablefunction(10);
					Createtablefunction(11);Createtablefunction(13);Createtablefunction(14);Createtablefunction(15);
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where s_SRID in "+where;
											
						db.execSQL("DELETE FROM " + mbtblInspectionList
								+ " " + temp + " ");
					} catch (Exception e) {

					}
					
					try {
						
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SRID in "+where;
											
						db.execSQL("DELETE FROM " + tbl_questionsdata
								+ " " + temp + "");
					} catch (Exception e) {
					}
					
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SrID in "+where;
						
						db.execSQL("DELETE FROM " + ImageTable
								+ " " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where Srid in "+where;
						
						db.execSQL("DELETE FROM " + FeedBackInfoTable
								+ " " + temp + "");
					} catch (Exception e) {

					}
					
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SRID in "+where;
						
						db.execSQL("DELETE FROM " + FeedBackDocumentTable
								+ " " + temp + "");
					} catch (Exception e) {

					}
					
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SRID in "+where;
						
						db.execSQL("DELETE FROM " + tbl_comments
								+ "  " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where Srid in "+where;
						
						db.execSQL("DELETE FROM " + HazardDataTable
								+ "  " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SrID in "+where;
						
						db.execSQL("DELETE FROM " + HazardImageTable
								+ "  " + temp + "");
					} catch (Exception e) {

					}
					ShowToast("Deleted sucessfully.",1);
					con.startActivity(new Intent(con,HomeScreen.class));
					
				}
				
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					
				}
				
			});
			dialog1.setCancelable(false);
			dialog1.show();
	 
		
	}
	
	public void fn_deleteall() {
		// TODO Auto-generated method stub
		
		Createtablefunction(2);Createtablefunction(7);Createtablefunction(8);Createtablefunction(10);
		Createtablefunction(11);Createtablefunction(13);Createtablefunction(14);Createtablefunction(15);
		try {
			db.execSQL("DELETE FROM " + mbtblInspectionList);
		} catch (Exception e) {

		}
		try {
			db.execSQL("DELETE FROM " + tbl_questionsdata);
		} catch (Exception e) {

		}
		try {
			db.execSQL("DELETE FROM " + tbl_comments);
		} catch (Exception e) {
		}
		try {
			db.execSQL("DELETE FROM " + ImageTable);
		} catch (Exception e) {
		}
		try {
			db.execSQL("DELETE FROM " + FeedBackInfoTable);
		} catch (Exception e) {
			
		}
		try {
			db.execSQL("DELETE FROM " + FeedBackDocumentTable);
		} catch (Exception e) {
			
		}
		try {
			db.execSQL("DELETE FROM " + HazardDataTable);
		} catch (Exception e) {
			
		}
		try {
			db.execSQL("DELETE FROM " + HazardImageTable);
		} catch (Exception e) {
			
		}
		
		ShowToast("Deleted sucessfully.",1);
	}
	public boolean errorlogmanage(String s, String Srid) {
		
		try {

			SoapSerializationEnvelope envelope2 = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope2.dotNet = true;

			SoapObject ad_property2 = new SoapObject(this.NAMESPACE,
					this.METHOD_NAME26);
			ad_property2.addProperty("Srid", Srid);
			ad_property2.addProperty("ErrorMsg", s);
			ad_property2.addProperty("Deviceid", "");
			ad_property2.addProperty("ModelNumber", "");
			ad_property2.addProperty("Manufacturer", "");
			ad_property2.addProperty("OSVersion", "");
			ad_property2.addProperty("APILevel", "");
			ad_property2.addProperty("IPAddress", "");
			ad_property2
					.addProperty("InspectorId", this.InspectorId.toString());

			envelope2.setOutputSoapObject(ad_property2);
			HttpTransportSE androidHttpTransport11 = new HttpTransportSE(
					this.URL);
			try {
				androidHttpTransport11.call(this.SOAP_ACTION26, envelope2);
			} catch (IOException e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
				return false;
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			try {
				String result11 = String.valueOf(envelope2.getResponse());
			} catch (SoapFault e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public void getInspectorId() {
		try {
			System.out.println(" getInspectorId.");
			Cursor cur = this.SelectTablefunction(this.inspectorlogin,
					" where Ins_Flag1=1");
			System.out.println("count "+cur.getCount());
			int Column1 = cur.getColumnIndex("Ins_Id");
			int Column2 = cur.getColumnIndex("Ins_FirstName");
			int Column3 = cur.getColumnIndex("Ins_LastName");
			int Column4 = cur.getColumnIndex("Ins_Address");
			int Column5 = cur.getColumnIndex("Ins_Email");
			int Column6 = cur.getColumnIndex("Ins_CompanyName");
			
			
			if(cur.getCount()!=0)
			{
				System.out.println("count greater ");
			cur.moveToFirst();
			if (cur != null) {
				do {
					System.out.println("count greaterdo  ");
					this.InspectorId = cur.getString(Column1);System.out.println("count InspectorId  "+InspectorId);
					this.insfname = cur.getString(Column2);
					this.inslname = cur.getString(Column3);
					this.insaddress = getsinglequotes(cur.getString(Column4));
					this.insemail = getsinglequotes(cur.getString(Column5));
					this.inscompname = getsinglequotes(cur.getString(Column6));
					System.out.println("this.InspectorId "+this.InspectorId+ " insemail "+insemail);
				} while (cur.moveToNext());
			}
			cur.close();
			
			}
			else
			{
				
				Intent iInspectionList = new Intent(this.con,InspectionDepot.class);
				this.con.startActivity(iInspectionList);
			}
		} catch (Exception e) {
			System.out.println("Cactch id "+e.getMessage());
		}

	}
	public void gohome() {
		// TODO Auto-generated method stub
		this.con.startActivity(new Intent(this.con,HomeScreen.class));
	}
	public void welcomemessage() {
		try {
			System.out.println("insi InspectorId "+this.InspectorId.toString());
			Cursor c = this.SelectTablefunction(this.inspectorlogin,
					" where Ins_Id='" + this.InspectorId.toString() + "'");
			int rws1 = c.getCount();
			c.moveToFirst();
			this.data = this.getsinglequotes(
					c.getString(c.getColumnIndex("Ins_FirstName")))
					.toLowerCase();
			this.data += " ";
			this.data += this.getsinglequotes(
					c.getString(c.getColumnIndex("Ins_LastName")))
					.toLowerCase();
			c.close();
		} catch (Exception e) {

		}
	}
	public String getdeviceversionname() {
		// TODO Auto-generated method stub
		try {
			versionname = this.con.getPackageManager().getPackageInfo(this.con.getPackageName(), 0).versionName;
			
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			//Error_LogFile_Creation(e.getMessage()+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of getting device version name at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		}
		return versionname;
	}
	public void startCameraActivity() {
		// TODO Auto-generated method stub
		String fileName = "temp.jpg";
 		ContentValues values = new ContentValues();
 		values.put(MediaStore.Images.Media.TITLE, fileName);
 		mCapturedImageURI =this.con.getContentResolver().insert(
 				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
 		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
 		((Activity) this.con).startActivityForResult(intent, CAPTURE_PICTURE_INTENT);
 		
	}
	public void getDeviceDimensions() {
		// TODO Auto-generated method stub
		    DisplayMetrics metrics = new DisplayMetrics();
			((Activity) this.con).getWindowManager().getDefaultDisplay().getMetrics(metrics);

			wd = metrics.widthPixels;
			ht = metrics.heightPixels;
	}
	public boolean check_Status(String result) {
		// TODO Auto-generated method stub
		try
		{
			if(result!=null)
			{
			if(result.trim().equals("true"))
			{
				return true;
			}
			else
			{
				return false;
			}
			}
			else
			{
				return false;
			}
			
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	
	
	public void getinspectiondate() {
		System.out.println("sd=" + Homeid + "\n" + this.InspectorId);
		try {
			Cursor cur = this.SelectTablefunction(this.mbtblInspectionList,
					" where s_SRID='" + this.Homeid + "' and InspectorId='"
							+ this.InspectorId + "'");
			cur.moveToFirst();
			this.inspectiondate = cur.getString(cur
					.getColumnIndex("ScheduleDate"));
			auditflag = cur.getString(cur
					.getColumnIndex("AuditFlag"));
			
		} catch (Exception e) {
		}
	}

	public void changeimage() {
		// TODO Auto-generated method stub
		try {
			Cursor cur6 = this.SelectTablefunction(this.tbl_questionsdata,
					"where SRID='" + this.Homeid + "'");
			if (cur6.getCount() > 0) {
				cur6.moveToFirst();
				if (cur6 != null) {
					buildcodevalue = cur6.getString(cur6
							.getColumnIndex("BuildingCodeValue"));

				}
			}
			Cursor cur = this.SelectTablefunction(this.tbl_questionsdata,
					"where SRID='" + this.Homeid + "'");
			if (cur.getCount() > 0) {
				cur.moveToFirst();
				if (cur != null) {
					roofdeckvalueprev = cur.getString(cur
							.getColumnIndex("RoofDeckValue"));

				}
			}

			Cursor cur1 = this.SelectTablefunction(this.tbl_questionsdata,
					"where SRID='" + this.Homeid + "'");
			if (cur1.getCount() > 0) {
				cur1.moveToFirst();
				if (cur1 != null) {
					rooftowallvalueprev = cur1.getString(cur1
							.getColumnIndex("RooftoWallValue"));

				}
			}

			Cursor cur2 = this.SelectTablefunction(this.tbl_questionsdata,
					"where SRID='" + this.Homeid + "'");
			if (cur2.getCount() > 0) {
				cur2.moveToFirst();
				if (cur2 != null) {
					roofgeovalue = cur2.getString(cur2
							.getColumnIndex("RoofGeoValue"));

				}
			}

			Cursor cur3 = this.SelectTablefunction(this.tbl_questionsdata,
					"where SRID='" + this.Homeid + "'");
			if (cur3.getCount() > 0) {
				cur3.moveToFirst();
				if (cur3 != null) {
					roofswrvalue = cur3.getString(cur3
							.getColumnIndex("SWRValue"));

				}
			}
			Cursor cur4 = this.SelectTablefunction(this.tbl_questionsdata,
					"where SRID='" + this.Homeid + "'");
			if (cur4.getCount() > 0) {
				cur4.moveToFirst();
				if (cur4 != null) {
					openprovalue = cur4.getString(cur4
							.getColumnIndex("OpenProtectValue"));

				}
			}
			Cursor cur5 = db.rawQuery("select * from " + this.tbl_questionsdata
					+ " where SRID='" + this.Homeid + "'", null);
			if (cur5.getCount() > 0) {
				cur5.moveToFirst();
				if (cur5 != null) {
					woodframeper = cur5.getString(cur5
							.getColumnIndex("WoodFramePer"));
					reper = cur5.getString(cur5
							.getColumnIndex("ReinMasonryPer"));
					unreper = cur5.getString(cur5
							.getColumnIndex("UnReinMasonryPer"));
					pcnper = cur5.getString(cur5
							.getColumnIndex("PouredConcretePer"));
					otrper = cur5
							.getString(cur5.getColumnIndex("OtherWallPer"));

				}
			}

		} catch (Exception e) {
			System.out.println("Exceptionintry= " + e);
		}
		try {
			Cursor c3 = this.SelectTablefunction(this.quesroofcover,
					"where SRID='" + this.Homeid + "'");
			chkrwss = c3.getCount();

		} catch (Exception e) {
			System.out.println("Exception= " + e);
		}
	}

	public boolean checkresponce(Object responce) {
		if (responce != null) {
			if (!"null".equals(responce.toString())
					&& !responce.toString().equals("")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public String IsCurrentInspector() throws IOException, XmlPullParserException {
		// TODO Auto-generated method stub
		String currrentinsp,resultres;
		SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope1.dotNet = true;
		SoapObject ad_property1 = new SoapObject(NAMESPACE,METHOD_NAME31);
		ad_property1.addProperty("InspectorID", InspectorId);
		ad_property1.addProperty("Srid", Homeid);
		envelope1.setOutputSoapObject(ad_property1);
		System.out.println(" ad_property1 "+ad_property1);
		HttpTransportSE androidHttpTransport2 = new HttpTransportSE(URL);
		androidHttpTransport2.call(SOAP_ACTION31,envelope1);
		resultres = String.valueOf(envelope1.getResponse());
		System.out.println(" resultres "+resultres);
		
		return resultres;
	}
	public String fn_CheckExportCount(String insp_id2, String selectedhomeid2) throws SocketException,IOException,NetworkErrorException,TimeoutException, XmlPullParserException,Exception {
		// TODO Auto-generated method stub
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject request = new SoapObject(NAMESPACE,METHOD_NAME35);	
		request.addProperty("InspectorID",insp_id2);
		request.addProperty("SRID",selectedhomeid2);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+"GetExportCount",envelope);
		String resultcnt = String.valueOf(envelope.getResponse());
		
		System.out.println("count= "+resultcnt);
		return resultcnt;
	}
	public String convertsinglequotes(String oldstring) {
		/*
		 * if(oldstring.contains("'")){return oldstring.replace("'", "&#39");}
		 * else { return oldstring; }
		 */
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return oldstring;

	}

	public String getsinglequotes(String newstring) {
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newstring;

	}
	
	public String[] gettheWifidata() {
		String s[] = new String[4];
		ConnectivityManager c = (ConnectivityManager) con
				.getSystemService(con.CONNECTIVITY_SERVICE);

		try {
			
			ConnectivityManager cm = (ConnectivityManager) con
					.getSystemService(con.CONNECTIVITY_SERVICE);
			
			NetworkInfo info = cm.getActiveNetworkInfo();
			
			int type = 0, subType = 0;
			
			if (info != null) {
				type = info.getType();
				subType = info.getSubtype();
				if (type == ConnectivityManager.TYPE_WIFI) {
				
					WifiManager wifiManager = (WifiManager) con
							.getSystemService(con.WIFI_SERVICE);
					s[0] = "WIFI";
					s[1] = String.valueOf(wifiManager.getConnectionInfo()
							.getLinkSpeed());
					s[2] = wifiManager.getConnectionInfo().LINK_SPEED_UNITS
							.toString();
					s[3] = "Strong";
					slenght = wifiManager.getConnectionInfo().getRssi();
					return s;
				} else if (type == ConnectivityManager.TYPE_MOBILE) {
					switch (subType) {
					case TelephonyManager.NETWORK_TYPE_1xRTT:
						s[0] = "1xRTT";
						s[1] = "50-100";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 50-100 kbps
					case TelephonyManager.NETWORK_TYPE_CDMA:

						s[0] = "CDMA";
						s[1] = "14-64";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 14-64 kbps
					case TelephonyManager.NETWORK_TYPE_EDGE:

						s[0] = "EDGE";
						s[1] = "50-100";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 50-100 kbps
					case TelephonyManager.NETWORK_TYPE_EVDO_0:

						s[0] = "3 GI Connection";
						s[1] = "400-1000";
						s[2] = "kbps";
						s[3] = "Not bad";
						return s; // ~ 400-1000 kbps
					case TelephonyManager.NETWORK_TYPE_EVDO_A:

						s[0] = "3 GI Connection";
						s[1] = "600-1400";
						s[2] = "kbps";
						s[3] = "Good";
						return s; // ~ 600-1400 kbps
					case TelephonyManager.NETWORK_TYPE_GPRS:

						s[0] = "GPRS";
						s[1] = "100";
						s[2] = "kbps";
						s[3] = "Poor";
						return s; // ~ 100 kbps
					case TelephonyManager.NETWORK_TYPE_HSDPA:

						s[0] = "3 GI Connection";
						s[1] = "2-14";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s;// ~ 2-14 Mbps
					case TelephonyManager.NETWORK_TYPE_HSPA:

						s[0] = "3 GI Connection";
						s[1] = "700-1700";
						s[2] = "kbps";
						s[3] = "Good";

						return s; // ~ 700-1700 kbps
					case TelephonyManager.NETWORK_TYPE_HSUPA:

						s[0] = "3 GI Connection";
						s[1] = "1-23";
						s[2] = "Mbps";
						s[3] = "Strong";
						return s; // ~ 1-23 Mbps
					case TelephonyManager.NETWORK_TYPE_UMTS:

						s[0] = "3 GI Connection";
						s[1] = "400-700";
						s[2] = "kbps";
						s[3] = "Good";
						return s; // ~ 400-7000 kbps
						// NOT AVAILABLE YET IN API LEVEL 7
					/*case TelephonyManager.NETWORK_TYPE_EHRPD:

						s[0] = "EHRPD";
						s[1] = "1-2";
						s[2] = "Mbps";
						s[3] = "Good";
						return s; // ~ 1-2 Mbps
					case TelephonyManager.NETWORK_TYPE_EVDO_B:

						s[0] = "EVDO";
						s[1] = "5";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s; // ~ 5 Mbps
					case TelephonyManager.NETWORK_TYPE_HSPAP:

						s[0] = "HSPAP";
						s[1] = "10-20";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s;// ~ 10-20 Mbps
					case TelephonyManager.NETWORK_TYPE_IDEN:

						s[0] = "IDEN";
						s[1] = "25";
						s[2] = "kbps";
						s[3] = "Poor";
						return s;// ~25 kbps
					case TelephonyManager.NETWORK_TYPE_LTE:

						s[0] = "LTE";
						s[1] = "10+";
						s[2] = "Mbps";
						s[3] = "Strong";

						return s;// ~ 10+ Mbps
						// Unknown
*/					case TelephonyManager.NETWORK_TYPE_UNKNOWN:

						s[0] = "UNKNOWN";
						s[1] = "N/D";
						s[2] = "N/D";
						s[3] = "N/D";

						return s;
					default:

						s[0] = "UNKNOWN";
						s[1] = "N/D";
						s[2] = "N/D";
						s[3] = "N/D";
						return s;
					}
				} else {

					s[0] = "UNKNOWN";
					s[1] = "N/D";
					s[2] = "N/D";
					s[3] = "N/D";
					return s;
				}
			} else {

				s[0] = "UNKNOWN";
				s[1] = "N/D";
				s[2] = "N/D";
				s[3] = "N/D";
				return s;
			}

		} catch (Exception e) {
			System.out.println("Problem" + e);
			s[0] = "UNKNOWN";
			s[1] = "N/D";
			s[2] = "N/D";
			s[3] = "N/D";
			return s;
		}
	}
	public void showselectedimage(final String capturedImageFilePath) {
		
		currnet_rotated=0;
		 System.gc();
		// TODO Auto-generated method stub
		picpath=capturedImageFilePath;
		BitmapFactory.Options o = new BitmapFactory.Options();
	 		o.inJustDecodeBounds = true;
		
	 		dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
			Re.setVisibility(View.GONE);
			LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
			Reup.setVisibility(View.GONE);
			LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
			camerapic.setVisibility(View.VISIBLE);
			tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
			tvcamhelp.setText("Save Picture");
			tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			spinvw = (RelativeLayout)dialog1.findViewById(R.id.cam_photo_update);
			capvw = (RelativeLayout)dialog1.findViewById(R.id.camaddcaption);
			
			/** get the help and update relative layou and set the visbilitys for the respective relative layout */
			
			final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
			btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
			btn_camcaptclos  = (Button) dialog1.findViewById(R.id.camcapthelpclose);
			Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
			//Button btn_cancl= (Button) dialog1.findViewById(R.id.camdelete);
			Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
			Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
			elevspin = (Spinner) dialog1.findViewById(R.id.cameraelev);
			cameratxt = (TextView)dialog1.findViewById(R.id.captxt);
			captionspin = (Spinner) dialog1.findViewById(R.id.cameracaption);
			 adapter2 = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, elevnames);
			 
 			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 			elevspin.setAdapter(adapter2);
 			elevspin.setOnItemSelectedListener(new MyOnItemSelectedListener());
			try {
	 				BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath),
	 						null, o);
	 				final int REQUIRED_SIZE = 400;
		 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
		 			int scale = 1;
		 			while (true) {
		 				if (width_tmp / 2 < REQUIRED_SIZE
		 						|| height_tmp / 2 < REQUIRED_SIZE)
		 					break;
		 				width_tmp /= 2;
		 				height_tmp /= 2;
		 				scale *= 2;
		 				BitmapFactory.Options o2 = new BitmapFactory.Options();
			 			o2.inSampleSize = scale;
			 			Bitmap bitmap = null;
			 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
			 	    		capturedImageFilePath), null, o2);
			 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
		 			   upd_img.setImageDrawable(bmd);
		 			}
	 		
	 		} catch (FileNotFoundException e) {
	 			System.out.println("Issues "+e.getMessage());
	 		}
			
			btn_save.setOnClickListener(new OnClickListener() {


	            public void onClick(View v) {

	            	System.out.println("elevspin.getSelectedItem().toString "+elevspin.getSelectedItem().toString());System.out.println("SSSSSSSSS "+spinelevval);
	            	if(!elevspin.getSelectedItem().toString().equals("Select Elevation"))
	            	{
	            		if(elevspin.getSelectedItem().toString().equals("Front Elevation")) {		maxindb=8;elev_name="FE "; }
	            		else if(elevspin.getSelectedItem().toString().equals("Right Elevation")) {	maxindb=8;	elev_name="RE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Back Elevation")){	maxindb=8;	elev_name="BE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Left Elevation"))	{ 	maxindb=8;	elev_name="LE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Attic Elevation")) { 	maxindb=8;	elev_name="AE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Additional Elevation")){	elev_name="Additional Photo "; maxindb=20;}
	            		else if(elevspin.getSelectedItem().toString().equals("Exterior and Grounds"))	{ 	maxindb=8;	elev_name="Exterior and Grounds ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Interior")) { 	maxindb=8;	elev_name="Interior ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Additional Images")){	elev_name="Additional "; maxindb=8;}
	            		
	            		String[] bits = picpath.split("/");
						String	picname = bits[bits.length - 1];
						System.out.println("getSelectedItem "+captionspin.getSelectedItem().toString());
						System.out.println("elevspin "+elevspin.getSelectedItem().toString());
						if(captionspin.getSelectedItem().toString().equals("Select")|| captionspin.getSelectedItem().toString().equals("SELECT") || captionspin.getSelectedItem().toString().equals("ADD PHOTO CAPTION"))
	            		{
							ShowToast("Please add Photo Caption.", 0);
							
	            		}
	            		else
	            		{		
	            			
	            			if(elevspin.getSelectedItem().toString().equals("Front Elevation") || elevspin.getSelectedItem().toString().equals("Right Elevation") 
	            					|| elevspin.getSelectedItem().toString().equals("Back Elevation") ||  elevspin.getSelectedItem().toString().equals("Left Elevation")
	            					|| elevspin.getSelectedItem().toString().equals("Additional Elevation") ||  elevspin.getSelectedItem().toString().equals("Attic Elevation"))
	            			
	            			{	
			            				int j;
			            				Cursor c11 = db.rawQuery("SELECT * FROM " + ImageTable
				        	 				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval
				        	 				+ "' and Delflag=0 order by CreatedOn DESC", null);
						        	 		int imgrws = c11.getCount();
						        	 		
						        	 		if (imgrws == 0) {
						        	 			j = imgrws + 1;
						        	 		} else {
						        	 			j = imgrws + 1;
					
						        	 		}
					        	 			if (imgrws > maxindb) 
					        	 			{								
					        	 				ShowToast toast = new ShowToast(con,"You are allowed to select only "+maxindb+" Images per Elevation.");
					        	 				elevspin.setSelection(0);
					        	 			}
					        	 			else 
					        	 			{
													db.execSQL("INSERT INTO "
															+ ImageTable
															+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
															+ " VALUES ('"+ Homeid+ "','"+ quesid+ "','"+ spinelevval+ "','"+ convertsinglequotes(picpath)+ "','"
															+ convertsinglequotes(newspin) + "','"+ convertsinglequotes(picname)+ "','" + datewithtime + "','" + datewithtime + "','"+ j + "','" + 0 + "')");
					        	 			}  
	            			}
	            			else
	            			{
	            				if(elevspin.getSelectedItem().toString().equals("Exterior and Grounds")){spinelevval=1;}
	            				else if(elevspin.getSelectedItem().toString().equals("Interior")){spinelevval=2;}
	            				else if(elevspin.getSelectedItem().toString().equals("Additional Images")){spinelevval=3;}
	            				
	            				System.out.println("saving camera images");
	            				int j;
	            				
	            				Cursor c11 = db.rawQuery("SELECT * FROM " + HazardImageTable
		        	 				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval
		        	 				+ "' and Delflag=0 order by CreatedOn DESC", null);
				        	 		int imgrws = c11.getCount();
				        	 		
				        	 		if (imgrws == 0) {
				        	 			j = imgrws + 1;
				        	 		} else {
				        	 			j = imgrws + 1;
			
				        	 		}
			        	 			if (imgrws==8) 
			        	 			{								
			        	 				ShowToast toast = new ShowToast(con,"You are allowed to select only "+maxindb+" Images per Elevation.");
			        	 				elevspin.setSelection(0);
			        	 			}          
			        	 			else 
			        	 			{
										
											db.execSQL("INSERT INTO "
													+ HazardImageTable
													+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
													+ " VALUES ('"+ Homeid+ "','"+ quesid+ "','"+ spinelevval+ "','"+ convertsinglequotes(picpath)+ "','"
													+ convertsinglequotes(newspin) + "','"+ convertsinglequotes(picname)+ "','" + datewithtime + "','" + datewithtime + "','"+ j + "','" + 0 + "')");
			        	 			
											System.out.println("saving camera images"+"INSERT INTO "
													+ HazardImageTable
													+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
													+ " VALUES ('"+ Homeid+ "','"+ quesid+ "','"+ spinelevval+ "','"+ convertsinglequotes(picpath)+ "','"
													+ convertsinglequotes(newspin) + "','"+ convertsinglequotes(picname)+ "','" + datewithtime + "','" + datewithtime + "','"+ j + "','" + 0 + "')");
			        	 			}   
	            			}
			            			
	            			
	            			/**Save the rotated value in to the external stroage place **/
							if(currnet_rotated>0)
							{ 

								try
								{
									/**Create the new image with the rotation **/
									String current=MediaStore.Images.Media.insertImage(con.getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
									
									 ContentValues values = new ContentValues();
									  values.put(MediaStore.Images.Media.ORIENTATION, 0);
									  con.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
									
									if(current!=null)
									{
									String path=getPath(Uri.parse(current));
									File fout = new File(capturedImageFilePath);
									fout.delete();
									/** delete the selected image **/
									File fin = new File(path);
									/** move the newly created image in the slected image pathe ***/
									fin.renameTo(new File(capturedImageFilePath));
									
									
								}
								} catch(Exception e)
								{
									System.out.println("Error occure while rotate the image "+e.getMessage());
								}
								
							}
						
						/**Save the rotated value in to the external stroage place ends **/	
							
							ShowToast("\"Taken photo successfully saved into your "+elevspin.getSelectedItem().toString()+" . \"",1);
							dialog1.dismiss();  
	            			
	            			
	            		}
	            }
	            	else
	            	{
	            		ShowToast("Please select Elevation type.", 0);
	            	}
	            }
			});
			
			btn_helpclose.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	dialog1.dismiss();
	            }
			});
			rotatecamright.setOnClickListener(new OnClickListener() {  
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					System.gc();
					currnet_rotated+=90;
					if(currnet_rotated>=360)
					{
						currnet_rotated=0;
					}
					
					Bitmap myImg;
					try {
						myImg = BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath));
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
					
						matrix.postRotate(currnet_rotated);
						
						 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
						        matrix, true);
						 System.gc();
						 upd_img.setImageBitmap(rotated_b);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (Exception e) {
						
					}
					catch (OutOfMemoryError e) {
						System.gc();
						try {
							myImg=null;
							System.gc();
							Matrix matrix =new Matrix();
							matrix.reset();
							//matrix.setRotate(currnet_rotated);
							matrix.postRotate(currnet_rotated);
							myImg= ShrinkBitmap(capturedImageFilePath, 800, 800);
							rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
							System.gc();
							upd_img.setImageBitmap(rotated_b); 

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 catch (OutOfMemoryError e1) {
								// TODO Auto-generated catch block
							ShowToast("You can not rotate this image. Image size is too large.",1);
						}
					}

				}
			});
			rotatecamleft.setOnClickListener(new OnClickListener() {  
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					System.gc();
					 currnet_rotated-=90;
						if(currnet_rotated<0)
						{
							currnet_rotated=270;
						}	
					Bitmap myImg;
					try {
						myImg = BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath));
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
					
						matrix.postRotate(currnet_rotated);
						
						 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
						        matrix, true);
						 System.gc();
						 upd_img.setImageBitmap(rotated_b);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (Exception e) {
						
					}
					catch (OutOfMemoryError e) {
						System.gc();
						try {
							myImg=null;
							System.gc();
							Matrix matrix =new Matrix();
							matrix.reset();
							//matrix.setRotate(currnet_rotated);
							matrix.postRotate(currnet_rotated);
							myImg= ShrinkBitmap(capturedImageFilePath, 800, 800);
							rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
							System.gc();
							upd_img.setImageBitmap(rotated_b); 

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 catch (OutOfMemoryError e1) {
								// TODO Auto-generated catch block
							ShowToast("You can not rotate this image. Image size is too large.",1);
						}
					}

				}
			});
			dialog1.setCancelable(false);
			dialog1.show();
				
			
	}
private class MyOnItemSelectedListener implements OnItemSelectedListener {
		
		public void onItemSelected(AdapterView<?> parent, View view,final int pos,

				long id) {
			String selected_elev = elevspin.getSelectedItem().toString();
			
			System.out.println("sleee "+selected_elev);
		    if(selected_elev.equals("Front Elevation"))
			{maxindb=8;
		    	cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=1;
			}
			else if(selected_elev.equals("Right Elevation"))
			{maxindb=8;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=2;
			}
			else if(selected_elev.equals("Back Elevation"))
			{maxindb=8;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=3;
			}
			else if(selected_elev.equals("Left Elevation"))
			{maxindb=8;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=4;
			}
			else if(selected_elev.equals("Attic Elevation"))
			{maxindb=8;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=5;
			}
			else if(selected_elev.equals("Additional Elevation"))
			{
				maxindb=20;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=8;
			}
			else if(selected_elev.equals("Exterior and Grounds"))
			{
				System.out.println("Exterior "+selected_elev);
				maxindb=8;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=9;
			}
			else if(selected_elev.equals("Interior"))
			{
				System.out.println("Interior "+selected_elev);
				maxindb=8;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=10;
			}
			else if(selected_elev.equals("Additional Images"))
			{
				System.out.println("Additional "+selected_elev);
				maxindb=8;
				cameratxt.setVisibility(view.VISIBLE);
		    	captionspin.setVisibility(view.VISIBLE);
		    	sp=11;
			}
			else 
			{
				cameratxt.setVisibility(view.GONE);
		    	captionspin.setVisibility(view.GONE);
		    	sp=0;
			}
		    System.out.println("selected_elev "+selected_elev + " sp2 "+sp);
		    call_eleavtion(selected_elev,sp);
		    
			}

		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			cameratxt.setVisibility(arg0.GONE);
	    	captionspin.setVisibility(arg0.GONE);
		}
		}
public void call_eleavtion(String selected_elev,int sp2) {
	// TODO Auto-generated method stub
	System.out.println("selected_elev "+selected_elev + " sp2 "+sp2);
	
	spinelevval = adapter2.getPosition(selected_elev);
	elev=sp2;
	
	
	if(spinelevval==6)
	{
		spinelevval=8;
	}
	else if(spinelevval==7)
	{
		spinelevval=9;
	}
	else if(spinelevval==8)
	{
		spinelevval=10;
	}
	else if(spinelevval==9)
	{
		spinelevval=11;
	}
	if(selected_elev.equals("Front Elevation")) {		maxindb=8;elev_name="FE "; }
	else if(selected_elev.equals("Right Elevation")) {	maxindb=8;	elev_name="RE ";}
	else if(selected_elev.equals("Back Elevation")){	maxindb=8;	elev_name="BE ";}
	else if(selected_elev.equals("Left Elevation"))	{ 	maxindb=8;	elev_name="LE ";}
	else if(selected_elev.equals("Attic Elevation")) { 	maxindb=8;	elev_name="AE ";}
	else if(selected_elev.equals("Additional Elevation")){	elev_name="Additional Photo "; maxindb=20;}
	else if(selected_elev.equals("Exterior and Grounds"))	{ 	maxindb=8;	elev_name="Exterior and Grounds ";}
	else if(selected_elev.equals("Interior")) { 	maxindb=8;	elev_name="Interior ";}
	else if(selected_elev.equals("Additional Images")){	elev_name="Additional "; maxindb=8;}
	
	if(elevspin.getSelectedItem().toString().equals("Front Elevation") || elevspin.getSelectedItem().toString().equals("Right Elevation") 
			|| elevspin.getSelectedItem().toString().equals("Back Elevation") ||  elevspin.getSelectedItem().toString().equals("Left Elevation")
			|| elevspin.getSelectedItem().toString().equals("Additional Elevation") ||  elevspin.getSelectedItem().toString().equals("Attic Elevation"))
		{	
		Cursor c11 = db.rawQuery("SELECT * FROM " + ImageTable
				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval 
				+ "' and Delflag=0  order by CreatedOn DESC", null);
	
		int k;
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			k = imgrws + 1;
		} else {
			k = imgrws + 1;

		}
		if(spinelevval==8)
		{
			if(k>20)
			{
				ShowToast toast = new ShowToast(con,"You can add only 20 Images in "+selected_elev);
				elevspin.setSelection(0);
			}
			else
			{
				fetchdata(k);
			}
		}
		else
		{
			System.out.println("Spin else"+k);
			if(k>8)
			{
				ShowToast toast = new ShowToast(con,"You can add only 8 Images in "+selected_elev);
				elevspin.setSelection(0);
			}
			else
			{
				System.out.println("Spin");
				fetchdata(k);
			}
		}
	}
	else
	{
		Createtablefunction(14);
		if(elevspin.getSelectedItem().toString().equals("Exterior and Grounds")){spinelevval=1;elev_name="Exterior and Grounds ";}
		else if(elevspin.getSelectedItem().toString().equals("Interior")){spinelevval=2;elev_name="Interior ";}
		else if(elevspin.getSelectedItem().toString().equals("Additional Images")){spinelevval=3;	elev_name="Additional ";}
/*
		if(elevspin.getSelectedItem().toString().equals("Exterior and Grounds")){spinelevval=1;}
		else if(elevspin.getSelectedItem().toString().equals("Interior")){spinelevval=2;}
		else if(elevspin.getSelectedItem().toString().equals("Additional Images")){spinelevval=3;}
		*/
		
	
		Cursor c11 = db.rawQuery("SELECT * FROM " + HazardImageTable
				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval 
				+ "' and Delflag=0  order by CreatedOn DESC", null);
		
		int k;
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			k = imgrws + 1;
		} else {
			k = imgrws + 1;

		}
		
			if(k>8)
			{
				ShowToast toast = new ShowToast(con,"You can add only 8 Images in "+selected_elev);
				elevspin.setSelection(0);
			}
			else
			{
				String[] temp;
				Cursor c1 = db.rawQuery("SELECT * FROM " + tblphotocaption + " WHERE fld_elevtype='" + elev_name + "' and fld_elevid='"+k+"'", null);	
				if (c1.getCount() > 0) {
					temp = new String[c1.getCount() + 2];
					temp[0] = "Select";
					temp[1] = "ADD PHOTO CAPTION";
					int i = 2;
					c1.moveToFirst();
					do {
						temp[i]=getsinglequotes(c1.getString(c1.getColumnIndex("fld_photocaption")));
						i++;
					} while (c1.moveToNext());
				} else {
					temp = new String[2];
					temp[0] = "Select";
					temp[1] = "ADD PHOTO CAPTION";
				}
				 ArrayAdapter adapter = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, temp);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					captionspin.setAdapter(adapter);
					captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
			}		
	}
}
private void fetchdata(int k)
{
Cursor c1 = db.rawQuery("SELECT * FROM " + tblphotocaption + " WHERE fld_elevtype='" + elev_name + "' and fld_elevid='"+k+"'", null);
	String[] temp,temp1;
	if(c1.getCount()>0)
	{
		temp=new String[c1.getCount()];
		int i=0;
		c1.moveToFirst();
		do
		{
			
			temp[i]=getsinglequotes(c1.getString(c1.getColumnIndex("fld_photocaption")));
			
			i++;
		}while(c1.moveToNext());
	}
	else
	{
		temp=new String[2];
		temp[0]="Select";
		temp[1]="ADD PHOTO CAPTION";
	}
	    ArrayAdapter adapter = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, temp);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		captionspin.setAdapter(adapter);
		captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
}
public void insertelevation()
{
	// TODO Auto-generated method stub
	int m=2,f=0,l=0;String photocaption ="";
	try
	{
		Cursor c1 = db.rawQuery("SELECT * FROM " + tblphotocaption, null);
		if(c1.getCount()==0)
		{
			
			for(int i=0;i<499;i++) // To insert first 500 records using compound select method .
			{
				String substr=arrelev.frontelevation1[i].substring(5,arrelev.frontelevation1[i].length());
				
				if(substr.equals("SELECT") || substr.equals("ADD PHOTO CAPTION") || substr.equals("FRONT ELEVATION") || substr.equals("FE-1 FRONT ELEVATION")|| substr.equals("BACK ELEVATION") || substr.equals("BE-1 BACK ELEVATION")
							|| substr.equals("LEFT ELEVATION") || substr.equals("LE-1 LEFT ELEVATION") || substr.equals("RIGHT ELEVATION") || substr.equals("RE-1 RIGHT ELEVATION") || substr.equals("ATTIC ELEVATION"))
				{					
					photocaption = convertsinglequotes(substr);
				}
				else
				{
					photocaption = convertsinglequotes(arrelev.frontelevation1[i]);
				}
				
				elevtype = arrelev.frontelevation1[i].substring(0, 2);
				elevid = arrelev.frontelevation1[i].substring(3,4);	
				
				
				bulkinsertstring += " UNION SELECT '"+m+"', '"+photocaption+"','" + elevtype+" "+"','" + elevid + "'";
				m++;
			}	
		
			db.execSQL("INSERT INTO "+tblphotocaption
				+ " SELECT '1' as Id, '' as  fld_photocaption,'' as  fld_elevtype,'' as fld_elevid" +
				bulkinsertstring); 
		
			bulkinsertstring="";
			System.out.println("outside abov"+bulkinsertstring);
			for(int j=500;j<arrelev.frontelevation1.length;j++) // You can use only 500 SELECT statements in Compound select method. so we have splitted it.
			{
				f=j+2;
				String substr1=arrelev.frontelevation1[j].substring(5,arrelev.frontelevation1[j].length());
				if(substr1.equals("SELECT") || substr1.equals("ADD PHOTO CAPTION") || substr1.equals("ATTIC ELEVATION"))
				{
					photocaption = convertsinglequotes(substr1);
				}
				else
				{
					photocaption = convertsinglequotes(arrelev.frontelevation1[j]);
				}
				
				elevtype = arrelev.frontelevation1[j].substring(0, 2);
				elevid = arrelev.frontelevation1[j].substring(3,4);
				bulkinsertstring += " UNION SELECT '"+f+"', '"+photocaption+"','" + elevtype+" "+ "','" + elevid + "'";					
			}
			System.out.println("outside "+bulkinsertstring);
			db.execSQL("INSERT INTO "+tblphotocaption
					+ " SELECT '501' as Id, 'SELECT' as  fld_photocaption,'AE ' as  fld_elevtype,'2' as fld_elevid" +
					bulkinsertstring);	
			bulkinsertstring="";
			
			              
			for(int p=0;p<20;p++) // For all Additional elevation we have the same caption to be inserted. So we have kept in seperate array and inserted it.
			{
				int h =p+1;
				for(int k=0;k<arrelev.addielev.length;k++)
				{
					l=f+2;
					elevtype = "Additional Photo";
					bulkinsertstring += " UNION SELECT '"+l+"', '"+convertsinglequotes(arrelev.addielev[k])+"','" + elevtype+" "+ "','" + h + "'";
					f++;
				}
				f=l;
			}
		
			int len = arrelev.frontelevation1.length+2;
			db.execSQL("INSERT INTO "+tblphotocaption
				+ " SELECT '"+len+"' as Id, '' as  fld_photocaption,' ' as  fld_elevtype,'' as fld_elevid" +
				bulkinsertstring);		
	}
	else
	{
		System.out.println("eleleleleleel");
	}
	}
	catch(Exception e)
	{
		System.out.println("Exception "+e.getMessage());
	}

	}
private class MyOnItemSelectedListener2 implements OnItemSelectedListener {

	
	public void onItemSelected(AdapterView<?> parent, View view,final int pos,long id) {
		 newspin = captionspin.getSelectedItem().toString();
		// hidekeyboard();
		final Button edt_save = (Button) dialog1.findViewById(R.id.edtsave);
		final Button edt_cancl= (Button) dialog1.findViewById(R.id.edtdelete);
		final EditText etcaption = (EditText) dialog1.findViewById(R.id.addcaption);
	
		if(newspin.equals("ADD PHOTO CAPTION"))
		{
			
			capvw.setVisibility(view.VISIBLE);
			spinvw.setVisibility(view.GONE);
			edt_save.setVisibility(v1.VISIBLE);
        	edt_cancl.setVisibility(v1.VISIBLE);
        	etcaption.setVisibility(v1.VISIBLE);
        	btn_camcaptclos.setVisibility(v1.VISIBLE);
		}
		else
		{
			tvcamhelp.setText("Add Caption");
		
			btn_camcaptclos.setOnClickListener(new OnClickListener() {

	            public void onClick(View v) {
	            	edt_save.setVisibility(v1.GONE);
	            	edt_cancl.setVisibility(v1.GONE);
	            	etcaption.setVisibility(v1.GONE);
	            	capvw.setVisibility(v1.VISIBLE);
	    			spinvw.setVisibility(v1.VISIBLE);
	    			captionspin.setSelection(0);
	            }
			});
			
			
			
			
			edt_cancl.setOnClickListener(new OnClickListener() {

	            public void onClick(View v) {
	            	System.out.println("cancel");
	            	edt_save.setVisibility(v1.GONE);
	            	edt_cancl.setVisibility(v1.GONE);
	            	etcaption.setVisibility(v1.GONE);
	            	capvw.setVisibility(v1.VISIBLE);
	    			spinvw.setVisibility(v1.VISIBLE);
	    			captionspin.setSelection(0);
	    			btn_camcaptclos.setVisibility(v1.GONE);
	    			
	    			InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(etcaption.getWindowToken(), 0);
	            }
			});
		
			
			
			edt_save.setOnClickListener(new OnClickListener() {
				  public void onClick(View v) {

					  if(elevspin.getSelectedItem().toString().equals("Front Elevation")){name="FE ";maxindb=8;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Right Elevation")){name="RE ";maxindb=8;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Back Elevation")){name="BE ";maxindb=8;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Left Elevation")){name="LE ";maxindb=8;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Attic Elevation")){name="AE ";maxindb=8;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Additional Elevation")){name="Additional Photo ";maxindb=20;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Exterior and Grounds")){name="Exterior and Grounds ";maxindb=8;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Interior")){name="Interior ";maxindb=8;}
	        	 		else if(elevspin.getSelectedItem().toString().equals("Additional Images")){name="Additional ";maxindb=8;}
					  
					  if(etcaption.getText().toString().trim().equals(""))
		              {
		            		ShowToast toast = new ShowToast(con,"Please enter caption.");
		              }
		              else
		              {
		            	  
		            	  if(elevspin.getSelectedItem().toString().equals("Front Elevation") || elevspin.getSelectedItem().toString().equals("Right Elevation") 
									|| elevspin.getSelectedItem().toString().equals("Back Elevation") ||  elevspin.getSelectedItem().toString().equals("Left Elevation")
									|| elevspin.getSelectedItem().toString().equals("Additional Elevation") ||  elevspin.getSelectedItem().toString().equals("Attic Elevation"))
						 {

		            		  int j;
			            		System.out.println("SAVING "+spinelevval);
			            	Cursor c11 = db.rawQuery("SELECT * FROM " + ImageTable
			        	 				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval
			        	 				+ "'and Delflag=0 order by CreatedOn DESC", null);
			            	int imgrws = c11.getCount();System.out.println("imgrws = "+imgrws);
			            	
			        	 		if (imgrws == 0) {
			        	 			j = imgrws + 1;
			        	 		} else {
			        	 			j = imgrws + 1;

			        	 		}
			        	 		String[] bits =  picpath.split("/");
								String	picname = bits[bits.length - 1];
								System.out.println("maxin "+maxindb + " imgrws "+imgrws);
								if (imgrws >=maxindb) {
									
									ShowToast toast = new ShowToast(con,"You are allowed to add only "+maxindb+" Images per Elevation.");
									elevspin.setSelection(0);
				            		
								}
								else
								{
									System.out.println("etcaption.getText().toString() "+etcaption.getText().toString());
									
									db.execSQL("INSERT INTO "
											+ tblphotocaption
											+ " (fld_photocaption,fld_elevtype,fld_elevid)"
											+ " VALUES ('" + convertsinglequotes(etcaption.getText().toString()) + "','" + name+ "','" + j + "')");
									System.out.println("IIN "+"INSERT INTO "
											+ tblphotocaption
											+ " (fld_photocaption,fld_elevtype,fld_elevid)"
											+ " VALUES ('" + convertsinglequotes(etcaption.getText().toString()) + "','" + name+ "','" + j + "')");
								}
								ShowToast toast = new ShowToast(con,"Caption has been added sucessfully");
								InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(etcaption.getWindowToken(), 0);
							    
							    
			            		etcaption.setText("");
				            	capvw.setVisibility(v.GONE);
								spinvw.setVisibility(v.VISIBLE);
								etcaption.setVisibility(v.GONE);
								btn_camcaptclos.setVisibility(v.VISIBLE);
								
								Cursor c1 = db.rawQuery("SELECT * FROM " + tblphotocaption +" WHERE fld_elevtype='"+name+"' and fld_elevid='"+j+"'",null);
								String[] temp;
								if(c1.getCount()>0)
								{
									temp=new String[c1.getCount()];
									
									int i=0;
									c1.moveToFirst();
									do
									{
										temp[i]=getsinglequotes(c1.getString(c1.getColumnIndex("fld_photocaption")));	
										i++;
									}while(c1.moveToNext());
								}
								else
								{
									temp=new String[2];
									temp[0]="Select";
									temp[1]="ADD PHOTO CAPTION";
								}
								  ArrayAdapter adapter = new ArrayAdapter(con,android.R.layout.simple_spinner_item, temp);
									adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
									captionspin.setAdapter(adapter);
									captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
								
							}

		            	 else
		            	 {
		            		 int j;
			            		System.out.println("SAVING "+spinelevval);
			            	Cursor c11 = db.rawQuery("SELECT * FROM " + HazardImageTable
			        	 				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval
			        	 				+ "'and Delflag=0 order by CreatedOn DESC", null);
			            	int imgrws = c11.getCount();System.out.println("imgrws = "+imgrws);
			            	
			        	 		if (imgrws == 0) {
			        	 			j = imgrws + 1;
			        	 		} else {
			        	 			j = imgrws + 1;

			        	 		}
			        	 		String[] bits =  picpath.split("/");
								String	picname = bits[bits.length - 1];
								System.out.println("maxin "+maxindb + " imgrws "+imgrws);
								if (imgrws >=maxindb) {
									
									ShowToast toast = new ShowToast(con,"You are allowed to add only "+maxindb+" Images per Elevation.");
									elevspin.setSelection(0);
				            		
								}
								else
								{
									System.out.println("etcaption.getText().toString() "+etcaption.getText().toString());
									
									db.execSQL("INSERT INTO "
											+ tblphotocaption
											+ " (fld_photocaption,fld_elevtype,fld_elevid)"
											+ " VALUES ('" + convertsinglequotes(etcaption.getText().toString()) + "','" + name+ "','" + j + "')");
									System.out.println("IIN "+"INSERT INTO "
											+ tblphotocaption
											+ " (fld_photocaption,fld_elevtype,fld_elevid)"
											+ " VALUES ('" + convertsinglequotes(etcaption.getText().toString()) + "','" + name+ "','" + j + "')");
								}
								ShowToast toast = new ShowToast(con,"Caption has been added sucessfully");
								InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(etcaption.getWindowToken(), 0);
			            		etcaption.setText("");
				            	capvw.setVisibility(v.GONE);
								spinvw.setVisibility(v.VISIBLE);
								etcaption.setVisibility(v.GONE);
								btn_camcaptclos.setVisibility(v.VISIBLE);
								Cursor c1 = db.rawQuery("SELECT * FROM " + tblphotocaption +" WHERE fld_elevtype='"+name+"' and fld_elevid='"+j+"'",null);
								String[] temp;
								if (c1.getCount() > 0) {
									
									temp = new String[c1.getCount() + 2];
									temp[0] = "Select";
									temp[1] = "ADD PHOTO CAPTION";
									int i = 2;
									c1.moveToFirst();
									do {  
										
										
										temp[i] = getsinglequotes(c1.getString(c1.getColumnIndex("fld_photocaption")));
										i++;
									} while (c1.moveToNext());
								} else {
									temp = new String[2];
									temp[0] = "Select";
									temp[1] = "ADD PHOTO CAPTION";
								}
								  ArrayAdapter adapter = new ArrayAdapter(con,android.R.layout.simple_spinner_item, temp);
									adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
									captionspin.setAdapter(adapter);
									captionspin.setOnItemSelectedListener(new MyOnItemSelectedListener2());
								

		            	 }
		            	  
		              }
				  }
			});
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {

		// TODO Auto-generated method stub
		
	}
	
}
public final boolean isInternetOn() {
	boolean chk = false;
	ConnectivityManager conMgr = (ConnectivityManager) this.con.getSystemService(Context.CONNECTIVITY_SERVICE);
	NetworkInfo info = conMgr.getActiveNetworkInfo();
	if (info != null && info.isConnected()) {
		chk = true;
	} else {
		chk = false;
	}
	System.out.println("chk "+chk);
	return chk;
}
public void Error_LogFile_Creation(String strerrorlog2)
{
    try {
			
			 DataOutputStream out = new DataOutputStream(this.con.openFileOutput("errorlogfile.txt", this.con.MODE_APPEND));
			 out.writeUTF(strerrorlog2);
			 
	 }
	 catch(Exception e)
	 {
		 
	 }
}
public void hidekeyboard()	
{
	System.out.println("hide ");
	InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    System.out.println("hide fuly");
}
public boolean common(String path) {
	// TODO Auto-generated method stub
	File f = new File(path);
	if (f.length() < 2097152) {
		return true;
	} else {
		return false;
	}

}
public void hidekeyboard(EditText ed)	
{
	try
	{
		
		((InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(ed.getWindowToken(), 0);	
	//imm.hideSoftInputFromWindow(((Activity) con).getCurrentFocus().getWindowToken(), 0);
	}
	catch (Exception e) {
		// TODO: handle exception
		System.out.println("issues clear"+e.getMessage());
	}

    //imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);
}

public Dialog showalert() {
	// TODO Auto-generated method stub
	final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
	dialog1.getWindow().setContentView(R.layout.alertphotocaption);
	TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
	TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
	Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose_ex);
	Button btn_helpclose_t = (Button) dialog1.findViewById(R.id.helpclose_ex_t);
	
	btn_helpclose.setOnClickListener(new OnClickListener()
	{

		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			dialog1.setCancelable(true);
			dialog1.dismiss();
		}
		
	});
	btn_helpclose_t.setOnClickListener(new OnClickListener()
	{

		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			dialog1.setCancelable(true);
			dialog1.dismiss();
		}
		
	});
	return dialog1;
	
}
public boolean getinspectionstatus(String id, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
	// TODO Auto-generated method stub
	SoapObject request = new SoapObject(NAMESPACE,string);
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	request.addProperty("InspectorID",id);
	request.addProperty("SRID",this.selectedhomeid);
	envelope.setOutputSoapObject(request);System.out.println("request"+request);
	HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	androidHttpTransport.call(NAMESPACE+"GetInspectionStatus",envelope);
	String result =  envelope.getResponse().toString();
	System.out.println("GETINSPECTIONSTATUS result "+result);
	if(result.trim().equals("40")||result.trim().equals("41"))
	{
		return true;
	}
	else
	{		
		return false;
	}
	
}
public void ShowToast(String s, int i) {
	
	switch(i)
	{
	case 0:
		colorname="#000000";
		break;
	case 1:
		colorname="#890200";
		break;
	case 2:
		colorname="#F3C3C3";
		break;
	}
	 toast = new Toast(this.con);
	 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 View layout = inflater.inflate(R.layout.toast, null);
	
	TextView tv = (TextView) layout.findViewById(R.id.text);
	tv.setTextColor(Color.parseColor(colorname)); 
	toast.setGravity(Gravity.CENTER, 0, 0);
	tv.setText(s);
	//tv.setTextSize(Typeface.BOLD);
	
	//toast.setGravity(Gravity.BOTTOM, 10, 80);
	toast.setView(layout);
	fireLongToast();


}
private void fireLongToast() {

    Thread t = new Thread() {
        public void run() {
            int count = 0;
            try {
                while (true && count < 10) {
                    toast.show();
                    sleep(3);
                    count++;

                    // do some logic that breaks out of the while loop
                }
            } catch (Exception e) {
               
            }
        }
    };
    t.start();
}
public void setFocus(EditText etcomments_obs1_opt12) {
	// TODO Auto-generated method stub
	
	etcomments_obs1_opt12.setFocusableInTouchMode(true);
	etcomments_obs1_opt12.requestFocus();
}
public void showing_limit(String s,LinearLayout lp,LinearLayout l,TextView t, String i) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
{
	System.out.println("showing "+s);
	int length=s.toString().length();
	System.out.println("Length "+length);
	double par_width= lp.getWidth();
	double par_unit;
	if(par_width==0)
	{
		par_width=Double.parseDouble(i); // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
	}
	
	par_unit=par_width/Double.parseDouble(i);
	System.out.println("par h"+par_unit);
	
	
	LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
	l.setLayoutParams(mParam);
	double totalle=(length*par_unit);
    int percent=(int) ((length)*(100.00/Double.parseDouble(i)));
    System.out.println("percent "+percent);
    if(length==Integer.parseInt(i))
    {
    	LinearLayout.LayoutParams mParam1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,(int)(8));
		l.setLayoutParams(mParam1);
    	
    	t.setText("   "+percent+" % Exceed limit");
    	//t.setText(Html.fromHtml("   "+percent+" % Exceed limit"));
    	
    }
    else
    {
    	t.setText("   "+percent+" %");
    }
	
}
public void showing_limit(String s,LinearLayout lp,LinearLayout l,TextView t) // Need to send the text in edit text and the parrent li then li then the text view whic show the percnetag   
{
	int length=s.toString().length();
	double par_width= lp.getWidth();
	double par_unit;
	if(par_width==0)
	{
		par_width=940; // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
	}
	
	par_unit=par_width/999.00;
	LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
	l.setLayoutParams(mParam);
	double totalle=(length*par_unit);
    int percent=(int) ((length)*(100.00/999.00));
    if(length==999)
    {
    	t.setText("   "+percent+" % Exceed limit");
    }
    else
    {
    	t.setText("   "+percent+" %");
    }
	
}
public void fn_logout(final String insp_id2) {
	// TODO Auto-generated method stub
	final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
	dialog1.getWindow().setContentView(R.layout.alert);
	LinearLayout lin = (LinearLayout)dialog1.findViewById(R.id.remember_password);
	lin.setVisibility(show.VISIBLE);
	TextView txttitle = (TextView) dialog1.findViewById(R.id.RP_txthelp);
	txttitle.setText("Logout");//txttitle.setTextAppearance(con,Typeface.BOLD);
	Button btn_helpclose = (Button) dialog1.findViewById(R.id.RP_close);
	TextView txt = (TextView) dialog1.findViewById(R.id.RP_txtid);
	txt.setText("Are you sure,Do you want to Exit the application?");
	Button btn_yes= (Button) dialog1.findViewById(R.id.RP_yes);
	Button btn_no= (Button) dialog1.findViewById(R.id.RP_no);
	btn_no.setText("   No   ");
	
	btn_helpclose.setOnClickListener(new OnClickListener()
	{

		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			dialog1.setCancelable(true);
			dialog1.dismiss();
		}
		
	});
	btn_no.setOnClickListener(new OnClickListener()
	{

		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			dialog1.setCancelable(true);
			dialog1.dismiss();
		}
		
	});
	btn_yes.setOnClickListener(new OnClickListener()
	{
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			dialog1.setCancelable(true);
			
			db.execSQL("UPDATE " + inspectorlogin
					+ " SET Ins_Flag1=0" );
			
			try{db.execSQL("UPDATE " + inspectorlogin
					+ " SET Ins_Flag1=0" + " WHERE Ins_Id ='"+ insp_id2 + "'");
			Intent loginpage;
			
			if(application_sta)
			{
				loginpage = new Intent(Intent.ACTION_MAIN);
				loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.LogOut"));
			}
			else
			{
			loginpage = new Intent(con,InspectionDepot.class);
			
			}
			con.startActivity(loginpage);
			
			}catch(Exception e)
			{
				Error_LogFile_Creation(e.getMessage()+" "+" at "+ con.getClass().getName().toString()+" "+" in the stage of logout(catch) at "+" "+datewithtime+" "+"in apk"+" "+rcstr);
				
			}
			dialog1.dismiss();
			ShowToast("You are Log out sucessfully.", 0);
		}
		
	});
	dialog1.show();

}
public void showalert(String alerttitle2, String alertcontent2) {
	// TODO Auto-generated method stub
	final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
	dialog1.getWindow().setContentView(R.layout.alert);
	TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
	txttitle.setText(alerttitle2);
	TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
	txt.setText(Html.fromHtml(alertcontent2));
	Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose);
	btn_helpclose.setVisibility(show.GONE);
	Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
	btn_ok.setVisibility(show.VISIBLE);
	btn_ok.setOnClickListener(new OnClickListener()
	{
	public void onClick(View arg0) {
			// TODO Auto-generated method stub
			dialog1.dismiss();
			con.startActivity(new Intent(con,Import.class));
		}
		
	});
	Button btn_dash = (Button) dialog1.findViewById(R.id.dash);
	btn_dash.setVisibility(show.VISIBLE);
	btn_dash.setOnClickListener(new OnClickListener()
	{
	public void onClick(View arg0) {
			// TODO Auto-generated method stub
			dialog1.dismiss();
			con.startActivity(new Intent(con,Startinspections.class));
		}
		
	});
	dialog1.setCancelable(false);
	dialog1.show();
}
public SoapObject Calling_WS1(String id, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException, XmlPullParserException {
	// TODO Auto-generated method stub
	SoapObject request = new SoapObject(NAMESPACE,string);
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;
	request.addProperty("inspectorid",id);
	envelope.setOutputSoapObject(request);;System.out.println("request "+request);
	HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	androidHttpTransport.call(NAMESPACE+string,envelope);
	SoapObject result = (SoapObject) envelope.getResponse();
	return result;
}

public void MoveTo_OnlineList(String string, int onlcarrassign2,
		String string2, String string3, String string4,String string5) {
	// TODO Auto-generated method stub
	   String name;
	   System.out.println("string ADSSIGN"+string);
	   
		if(string.equals("29"))
		{
			name=" B1 - 1802 (Rev.01/12) ";
		}
		else
		{
			name=" B1 - 1802 (Rev.01/12) Carr.Ord.";
		}
		  System.out.println("string onlcarrassign2"+onlcarrassign2);
		
		if(onlcarrassign2==0)
		{
			ShowToast("Sorry, No records found for "+string4+" status",0);
		}
		else
		{
			Intent intent = new Intent(this.con,OnlineList.class);
			intent.putExtra("InspectionType", string);
			intent.putExtra("status", string2);
			intent.putExtra("substatus", string3);
			intent.putExtra("flag", string5);
			this.con.startActivity(intent);
		}
}

public String PhoneNo_validation(String decode) {
	// TODO Auto-generated method stub
	if (decode.length() == 10) {
		StringBuilder sVowelBuilder = new StringBuilder(decode);
		sVowelBuilder.insert(0, "(");
		sVowelBuilder.insert(4, ")");
		sVowelBuilder.insert(8, "-");
		newphone = sVowelBuilder.toString();
		return "Yes";
        
	} else {
		return "No";
	}
}
public Bitmap ShrinkBitmap(String file, int width, int height) {
	 Bitmap bitmap =null;
		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
		    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);
          
			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
		
			return bitmap;
		}

	}
public void getInspecionTypeid(String srid)
{
	try {
		getInspectorId();
		Cursor c2 = db.rawQuery("SELECT * FROM "
				+ mbtblInspectionList + " WHERE s_SRID='" + srid
				+ "' and InspectorId='" + InspectorId + "'", null);
		
	
		c2.moveToFirst();
		if(c2.getCount()==1)
		{
			Inspectiontypeid=c2.getString(c2.getColumnIndex("InspectionTypeId"));
			LicenceExpiryDate=getsinglequotes(c2.getString(c2.getColumnIndex("Licenceexpirydate")));
		}
	}
	catch(Exception e)
	{
	    strerrorlog="Retrieving policy number ";
		Error_LogFile_Creation(strerrorlog+" "+" at "+ con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+rcstr);
		
	}
	
}
public String getPath(Uri uri) {
	String[] projection = { MediaStore.Images.Media.DATA };
	Cursor cursor = ((Activity) con).managedQuery(uri, projection, null, null, null);
	int column_index = cursor
			.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	cursor.moveToFirst();
	return cursor.getString(column_index);
}
public void setRcvalue(TextView releasecode2) {
	// TODO Auto-generated method stub
	System.out.println("URL="+URL+"RC="+rcstr);
	if(URL.contains("72.15.221.153"))
	{
		releasecode2.setText(rcstr.replace("U", "L"));
	}
	else
	{
			releasecode2.setText(rcstr);
	}
}
public String checklicenceexpiry(String licenceexpirydate)
{
	String chk="";
	System.out.println("licen="+licenceexpirydate);
	if(licenceexpirydate.equals("") || licenceexpirydate.equals("N/A"))
	{
		chk="false";
	}
	else
	{
		System.out.println("LLL="+licenceexpirydate);
		int i1 = licenceexpirydate.indexOf("/");
		String result = licenceexpirydate.substring(0, i1);
		int i2 = licenceexpirydate.lastIndexOf("/");
		String result1 = licenceexpirydate.substring(i1 + 1, i2);
		String result2 = licenceexpirydate.substring(i2 + 1);
		result2 = result2.trim();
		int smonth= Integer.parseInt(result);
		int sdate = Integer.parseInt(result1);
		int syear = Integer.parseInt(result2);
	    getCalender();
	    mMonth = mMonth + 1;
	    System.out.println("myear="+mYear+"syear"+syear+"mMonth="+mMonth+"syear"+smonth+"mDay="+mDay+"syear"+sdate);
	    if ( syear  < mYear || (smonth < mMonth && syear  <= mYear) || (smonth <= mMonth && syear <=mYear && sdate <= mDay )) {
	    	chk="true";
		} else {
			chk="false";
		}
	}
	System.out.println("chk="+chk);
	return chk;
}
public void getCalender() {
	// TODO Auto-generated method stub
	final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
}
}