package idsoft.inspectiondepot.B11802;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class Exportedddata extends Activity implements Runnable {

	String auditflag="0",insrendate, elevnamegh="",endtime, inspectioncomment, schdate, starttime,dt,value,flag="",
			flgstatus, comtxt, incstat="0", data, resultquestions, chk4, strBase64,
			filepath, InspectorId, _inspected = "IsInspected",showstr="",
			_uploaded = "IsUploaded", selectedItem, homeId, homeid,completedtabs="",
			Exportbartext="";  public String[] erro_trce=null;
	boolean isComplete=true,chkifalldatafilled=false,chkifpartialfilledtrue=false,complete_enable = false,toastshowed = false,chkifquesphofdbfilled=false;
	boolean statuschange[] ={false,false,false,false,false};
	boolean tochkfileexists[]={false,false,false};boolean completed=false;
	boolean Export_status[]={false,false,false,false,false,false};
	String Export_chk[]={"0","0","0","0","0","0"};
	CheckBox alert_chk[]=new CheckBox[6];
	LinearLayout lldisplaypdf;
	final CheckBox sta_chk[]= new CheckBox[5];	
	double total,increment_unit,incre_unit;  
	String path,status="Uploading Inspection Completed",erromsg,Chk_Inspector="false",server_error = "false",probpath,overallcomm="";
	SoapObject resulttpqa;
	View v1;	
	int countchk=0;
	TextView t;	
	int i = 0, verify = 0, inirws,mState,typeBar = 1,delay = 40,maxBarValue = 0,RUNNING = 1, valstart, valend,chkstatus=0,
			policy, bcode, rfcover, rfdeck, rfwall, rfgeo, swr, open, wall, sign,show_handler,show_handler3,
			frnt, fdinfo, fddoc, over, hazdat,hazardinclude,hzdatarws;
	String[] namelist, homelist = new String[50],pdfpath;	
	TextView tvstatus[];
	Button delbtn[];
	String rowlength[];
	Bitmap bitmap;
	ScrollView sv;
	Cursor qc;
	Dialog dialog1,dialog2;
	CommonFunctions cf;
    ProgressDialog progDialog,pd,pd1,pd2;
    LinearLayout inspectionlist;
	ProgressThread progThread;
	Button temb,temcmp,temaudit;
	AlertDialog alertDialog;
	PowerManager.WakeLock wl=null;
	
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exporteddata);
		temb = (Button) findViewById(R.id.export_list);
		temcmp = (Button) findViewById(R.id.cmpe);
		temaudit = (Button) findViewById(R.id.audit);
		Bundle extras = getIntent().getExtras();
		cf = new CommonFunctions(this);
		cf.getInspectorId();
		
		if (extras != null) 
		{
			value = extras.get("completedexport").toString();			
			if (value.equals("true")) {
				temb.setBackgroundResource(R.drawable.button_back_blue);
				temb.setVisibility(View.VISIBLE);
			}
			else if(value.equals("false"))
			{
				((TextView) findViewById(R.id.information)).setText("Re-Export Inspection");
				complete_enable = true;
				temcmp.setBackgroundResource(R.drawable.button_back_blue);
			}
			else if(value.equals("QA"))
			{
				System.out.println("inside QA");
				((TextView) findViewById(R.id.information)).setText("QA Inspection");
				complete_enable = true;
				temaudit.setBackgroundResource(R.drawable.button_back_blue);
				callthread();
				
			}
		}
		
		getNetConnectDetail();// get the detail of the internet connection
		t = (TextView) findViewById(R.id.txtlistheader);
		inspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		lldisplaypdf = (LinearLayout) findViewById(R.id.linearlayoutdiaplaypdf);

    	
		cf.welcomemessage();
		((TextView)findViewById(R.id.welcomename)).setText(Html.fromHtml("<font color=#f7a218>" + "Welcome "+"</font>"+ cf.data));
		cf.setRcvalue((TextView) this.findViewById(R.id.releasecode));
				
		BindExportData();
		progDialog = new ProgressDialog(this);
		//t.append("\n");		
		String headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time";
		((TextView)findViewById(R.id.note)).setText(headernote);
	}

	private void getNetConnectDetail() {
		// TODO Auto-generated method stub
		try {
			String[] da = cf.gettheWifidata();// get the detail of the data
												// connection and show it in the
												// screen

			if ((!da[0].equals("UNKNOWN") && !da[0].equals(""))) {
				TextView dataconnect = (TextView) findViewById(R.id.connectiondata);
				dataconnect.setText("Connection speed : " + da[1] + da[2] + " "
						+ "Connection : " + da[3]);
				if (da[0].equals("WIFI")) {
					TextView signallength = (TextView) findViewById(R.id.signallength);
					signallength.setVisibility(View.VISIBLE);
					signallength.setText(" Signal strength :"+da[3]);
					/*if (cf.slenght <= -80) {
						signallength.setText(" Signal strength : Poor"+cf.slenght);
					} else if (cf.slenght <= -75) {
						signallength.setText(" Signal strength : Not bad"+cf.slenght);
					} else if (cf.slenght <= -60) {
						signallength.setText(" Signal strength : Good"+cf.slenght);
					} else {
						signallength.setText(" Signal strength : Strong"+cf.slenght);
					}*/
				}System.out.println("getNetConnectDetail comple");
			}
		} catch (Exception e) {

		}

	}
	protected void Error_logupload()throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException 
	{
			// TODO Auto-generated method stub  	
	  	File assist = new File(this.getFilesDir()+"/errorlogfile.txt");	  	
	  	if(cf.common(this.getFilesDir()+"/errorlogfile.txt"))
	  	{
	    	SoapObject request = new SoapObject(cf.NAMESPACE,"ErrorLogFile_Tab");
	    	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	    	envelope.dotNet = true;
	    	request.addProperty("InspectorId",cf.Insp_id);
	    	if (assist.exists()) 
			{
	    		try  
	    		{
					InputStream fis = new FileInputStream(assist);
					long length = assist.length();
					byte[] bytes = new byte[(int) length];
					int offset = 0;
					int numRead = 0;
					while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
					{
						offset += numRead;
					}
					strBase64 = Base64.encode(bytes);
					request.addProperty("imgByte", strBase64);
					envelope.setOutputSoapObject(request);
					HttpTransportSE httpTransport = new HttpTransportSE(cf.URL);
					HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
					androidHttpTransport.call(cf.NAMESPACE+"ErrorLogFile_Tab",envelope);
					String result =  envelope.getResponse().toString();
					if(cf.check_Status(result))
					{
						assist.delete();
					}						
				}
				catch (Exception e) 
				{
				}
			}
	  	}
	}
	public final void Deleteimporting(int position) {
		int val = position;
		String select = homelist[val];
		String selectit = select.toString();

		String dt = selectit;
		try {
			cf.db.execSQL("DELETE FROM " + cf.mbtblInspectionList
					+ " WHERE s_SRID ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			cf.db.execSQL("DELETE FROM " + cf.tbl_questionsdata
					+ " WHERE SRID ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			cf.db.execSQL("DELETE FROM " + cf.tbl_comments + " WHERE SRID ='"
					+ dt + "'");
		} catch (Exception e) {
		}
		try {
			cf.db.execSQL("DELETE FROM " + cf.ImageTable + " WHERE SrID ='"
					+ dt + "'");
		} catch (Exception e) {
		}
		try {
			cf.db.execSQL("DELETE FROM " + cf.FeedBackInfoTable
					+ " WHERE Srid ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			cf.db.execSQL("DELETE FROM " + cf.FeedBackDocumentTable
					+ " WHERE SRID ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			cf.db.execSQL("DELETE FROM " + cf.HazardDataTable
					+ " WHERE Srid ='" + dt + "'");
		} catch (Exception e) {

		}
		try {
			cf.db.execSQL("DELETE FROM " + cf.HazardImageTable
					+ " WHERE SrID ='" + dt + "'");
		} catch (Exception e) {

		}
		cf.ShowToast("Deleted sucessfully",1);
		Intent move = new Intent(Exportedddata.this, Exportedddata.class);
		move.putExtra("completedexport", "true");
		startActivity(move);

	}

	private String IsCurrentInspector(String inspid, String homeid) throws IOException, XmlPullParserException { // To check the inspection has been reallocated to another inspector.
		// TODO Auto-generated method stub
		String currrentinsp,resultres;
		SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope1.dotNet = true;
		SoapObject ad_property1 = new SoapObject(cf.NAMESPACE,cf.METHOD_NAME31);
		ad_property1.addProperty("InspectorID", inspid);
		ad_property1.addProperty("Srid", homeid);System.out.println("IsCurrentInspector"+ad_property1);
		envelope1.setOutputSoapObject(ad_property1);
		HttpTransportSE androidHttpTransport2 = new HttpTransportSE(cf.URL);
		androidHttpTransport2.call(cf.SOAP_ACTION31,envelope1);
		resultres = String.valueOf(envelope1.getResponse());
	
		System.out.println(" resultres "+resultres);
		return resultres;
	}
	
	private void Error_traker(String ErrorMsg) {
		// TODO Auto-generated method stub
		
		if(erro_trce==null)
		{
			
			erro_trce=new String[1];
			
			erro_trce[0]=ErrorMsg;
			
		}
		else
		{
			
			String tmp[]=new String[this.erro_trce.length+1];
			int i;
			for(i =0;i<erro_trce.length;i++)
			{
				
				tmp[i]=erro_trce[i];
			}
		
			tmp[tmp.length-1]=ErrorMsg;
			erro_trce=null;
			erro_trce=tmp.clone();
		}
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 1:
			progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progDialog.setMax(maxBarValue);
			progDialog
					.setMessage(Html.fromHtml("Exporting please wait : It will take a few minutes \n"+Exportbartext));
			progDialog.setCancelable(false);
			progThread = new ProgressThread(handler1);
			progThread.start();
			return progDialog;
		default:
			return null;
		}
	}

	final Handler handler1 = new Handler() 
	{
		public void handleMessage(Message msg) {
			// Get the current value of the variable total from the message data
			// and update the progress bar.
			int total = msg.getData().getInt("total");

			progDialog.setProgress(total);
			progDialog.setMessage(Html.fromHtml("Exporting please wait : It will take a few minutes <br> "+Exportbartext));
			if (total == 100) {
				progDialog.setCancelable(true);
				dismissDialog(typeBar);
				handler2.sendEmptyMessage(0);
				progThread.setState(ProgressThread.DONE);
			}
		}
	};
	final Handler handler2 = new Handler() {
		private AlertDialog alertDialog;
		@Override
		public void handleMessage(Message msg) {
			 //System.err.println("handler2="+auditflag);
			if(cf.Chk_Inspector.equals("true"))
			{
				ImageView sta_im[]=new ImageView[5];
				TableRow sta_row[]=new TableRow[5];
				// TODO Auto-generated method stub
				/** Set the  dilog to cancel if any thing shown **/
				if(dialog1!=null)
				{ 
					if(dialog1.isShowing())
					{
						dialog1.dismiss();
					}
				}
				/** Set the  dilog to cancel if any thing  shown ends **/
				dialog1 = new Dialog(Exportedddata.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertexportcompleted);
				
				sta_chk[0]=(CheckBox) dialog1.findViewById(R.id.sta_chk1);
				sta_chk[1]=(CheckBox) dialog1.findViewById(R.id.sta_chk2);
				sta_chk[2]=(CheckBox) dialog1.findViewById(R.id.sta_chk5);
				sta_chk[3]=(CheckBox) dialog1.findViewById(R.id.sta_chk6);
				sta_chk[4]=(CheckBox) dialog1.findViewById(R.id.sta_chk7);
				
				sta_row[0]=(TableRow) dialog1.findViewById(R.id.sta_row1);
				sta_row[1]=(TableRow) dialog1.findViewById(R.id.sta_row2);
				sta_row[2]=(TableRow) dialog1.findViewById(R.id.sta_row5);
				sta_row[3]=(TableRow) dialog1.findViewById(R.id.sta_row6);
				sta_row[4]=(TableRow) dialog1.findViewById(R.id.sta_row7);
				
				sta_im[0]=(ImageView) dialog1.findViewById(R.id.sta_im1);
				sta_im[1]=(ImageView) dialog1.findViewById(R.id.sta_im2);
				sta_im[2]=(ImageView) dialog1.findViewById(R.id.sta_im5);
				sta_im[3]=(ImageView) dialog1.findViewById(R.id.sta_im6);
				sta_im[4]=(ImageView) dialog1.findViewById(R.id.sta_im7);
				
				 
				final LinearLayout tlist=(LinearLayout) dialog1.findViewById(R.id.error_reports);
					if(erro_trce!=null)
					{
						System.out.println("erro_trce.length "+erro_trce.length);
						if(erro_trce.length>=1)
						{
							LinearLayout.LayoutParams lm= new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
							for(int i=0;i<erro_trce.length;i++)
							{
								System.out.println("erro_trce i "+erro_trce[i]);
								TextView tv1=new TextView(Exportedddata.this);
								tv1.setText(erro_trce[i]);						
								tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
								tv1.setTextColor(Color.parseColor("#000000"));
								LinearLayout litop= new LinearLayout(Exportedddata.this);
								litop.setLayoutParams(lm);
								litop.setBackgroundColor(Color.GREEN);
								tlist.addView(litop);
								tlist.addView(tv1);
							}
							((Button) dialog1.findViewById(R.id.error_detail)).setVisibility(v1.VISIBLE);
					  }
					}
					else
					{	
						System.out.println("inside elese track null ");
						((Button) dialog1.findViewById(R.id.error_detail)).setVisibility(v1.INVISIBLE);
					}
					
				/** Set the tick icon , cross icon and visibility for the alert**/
					
					if(Export_chk[1].equals("1")) // PH information
					{
						sta_row[0].setVisibility(View.VISIBLE);
						if(Export_status[0])
						{
							sta_im[0].setBackgroundResource(R.drawable.tick_icon);	
						}
						else
						{
							sta_im[0].setBackgroundResource(R.drawable.cross_icon);
						}
					}
					if(Export_chk[2].equals("1")) //Questions
					{
						sta_row[1].setVisibility(View.VISIBLE);
						if(Export_status[1])
						{
							sta_im[1].setBackgroundResource(R.drawable.tick_icon);	
						}
						else
						{
							sta_im[1].setBackgroundResource(R.drawable.cross_icon);
						}
					}
					if(Export_chk[3].equals("1")) // Photos
					{
						sta_row[2].setVisibility(View.VISIBLE);
						if(Export_status[2])
						{
							if(tochkfileexists[0]==true)
							{
								sta_im[2].setBackgroundResource(R.drawable.cross_icon);
							}
							else
							{
								sta_im[2].setBackgroundResource(R.drawable.tick_icon);	
							}
						}
						else
						{
							sta_im[2].setBackgroundResource(R.drawable.cross_icon);
						}
					}
					if(Export_chk[4].equals("1")) //Feedback Information
					{
						sta_row[3].setVisibility(View.VISIBLE);
						if(Export_status[3])
						{
							if(tochkfileexists[1]==true)
							{
								sta_im[3].setBackgroundResource(R.drawable.cross_icon);
							}
							else
							{
								sta_im[3].setBackgroundResource(R.drawable.tick_icon);		
							}
						}
						else
						{
							sta_im[3].setBackgroundResource(R.drawable.cross_icon);
						}
					}
					
					if(Export_chk[5].equals("1")) // General Information
					{
						sta_row[4].setVisibility(View.VISIBLE);
						if(Export_status[4])
						{
							if(tochkfileexists[2]==true)
							{
								sta_im[4].setBackgroundResource(R.drawable.cross_icon);
							}
							else
							{
								sta_im[4].setBackgroundResource(R.drawable.tick_icon);
							}
						}
						else
						{
							sta_im[4].setBackgroundResource(R.drawable.cross_icon);
						}
					}
				
					((Button) dialog1.findViewById(R.id.helpclose_ex_t)).setVisibility(cf.v1.GONE);
				
				((Button) dialog1.findViewById(R.id.helpclose_ex)).setOnClickListener(new OnClickListener()// ok button
				{

					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						for(int i=1;i<Export_chk.length;i++)
						{
							Export_chk[i]="0";
						}
						dialog1.setCancelable(true);
						dialog1.dismiss();
						getauditflag();
						 System.err.println("helpclose_ex="+auditflag);
						 
						 if(auditflag.equals("1"))
						 {
							 if(chkifpartialldatafilled())
							 {
								 Changestatus();
							 }
							 else
							 {
								 if(complete_enable)
								 {
										showalert("Exporting inspection completed. Inspection in Pre-Inspected Status");
								 }
								 else
								 {
									   showalert("Exporting inspection completed. Inspection in scheduled Status");	
								}
							 }
						 }
						 else if(auditflag.equals("2"))
						 {
								if((statuschange[0]==true && statuschange[1]==true && statuschange[2]==true) || statuschange[3]==true || statuschange[4]==true)
								{
									if(chkifquesphofdbfilled())
									 {
										 System.out.println("partial");
									 if(cf.isInternetOn())
										{
										String source = "<font color=#FFFFFF>Generating PDF... Please wait..."+ "</font>";
										pd1 = ProgressDialog.show(Exportedddata.this,"", Html.fromHtml(source), true);
									        new Thread(new Runnable() {
									                public void run() {
									                	
									                	try {
									            			Status_change();
									            			if(QAEnabled())
									            			{
									            				if(TPQA())
										            			{
									            					cf.db.execSQL("UPDATE "
									    									+ cf.mbtblInspectionList
									    									+ " SET Status=2,SubStatus=0 WHERE s_SRID ='"+ cf.selectedhomeid+ "'");	
										            				show_handler=0;
										            				handler.sendEmptyMessage(0);
										            			}
										            			else
										            			{
										            				show_handler=1;
										            				handler.sendEmptyMessage(0);
										            			}
									            			}
									            			else
									            			{
									            				show_handler=3;
									            				handler.sendEmptyMessage(0);
									            			}
									            			
									            		} catch (SocketTimeoutException e) {
									            			show_handler=2;
									            			handler.sendEmptyMessage(0);
									            			// TODO Auto-generated catch block
									            			e.printStackTrace();
									            		} catch (NetworkErrorException e) {
									            			show_handler=2;
									            			handler.sendEmptyMessage(0);
									            			// TODO Auto-generated catch block
									            			e.printStackTrace();
									            		} catch (IOException e) {
									            			show_handler=2;
									            			// TODO Auto-generated catch block
									            			handler.sendEmptyMessage(0);
									            			
									            		} catch (XmlPullParserException e) {
									            			// TODO Auto-generated catch block
									            			System.out.println("error occuers exmess"+e.getMessage());
									            			show_handler=2;
									            			handler.sendEmptyMessage(0);
									            			
									            		}                
									                }
									            }).start();	
										}
										else
										{
											cf.ShowToast("Internet Connection not available.", 0);
										}
									 }
									 else
									 {
										 String exportnote ="";
											 if(chkifpartialldatafilled())
											 {
												 	Changestatus();
											 }

											 //showalert("You have to export Questions, Photos and Feedback section for Generating PDF.");
									 }
								}
								else
								{
									 if(complete_enable)
									 {
											showalert("Exporting inspection completed. Inspection in Pre-Inspected Status");
									 }
									 else
									 {
										   showalert("Exporting inspection completed. Inspection in scheduled Status");	
									 }
								}
						 }
						 else
						 {
							 if(chkifpartialldatafilled())
							 {
								 Changestatus();
							 }
							 else
							 {
								 if(complete_enable)
								 {
										showalert("Exporting inspection completed. Inspection in Pre-Inspected Status");
								 }
								 else
								 {
									   showalert("Exporting inspection completed. Inspection in scheduled Status");	
								 }
							 }
						 }
					}
				});
				
				((Button) dialog1.findViewById(R.id.error_detail)).setOnClickListener(new OnClickListener()
				{
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(tlist.getVisibility()==View.VISIBLE)
						{
							((Button) dialog1.findViewById(R.id.error_detail)).setText("Hide failure reports");
							tlist.setVisibility(View.GONE);
						}
						else
						{
							((Button) dialog1.findViewById(R.id.error_detail)).setText("Show failure reports");
							tlist.setVisibility(View.VISIBLE);
						}
					}
					
				});
				((Button) dialog1.findViewById(R.id.re_export_now)).setOnClickListener(new OnClickListener()
				{
            	   public void onClick(View arg0) {
						// TODO Auto-generated method stub
            		   cf.Createtablefunction(8);
            		   Cursor qc = cf.SelectTablefunction(cf.tbl_comments, " where SRID='" + cf.Homeid + "'");
            			if(qc.getCount()>0)
            			{
            				qc.moveToFirst();
            				if((!cf.getsinglequotes(qc.getString(qc.getColumnIndex("WallConstructionComment"))).equals("")))
            				{
            					re_export();
            				}
            				else
            				{
            					cf.ShowToast("Please Fill B1-1802 Question or Images or Feedback Section.",1);
            				}
            			}
            			else
            			{
            				re_export();	
            			}
						
					}
				});
               
				dialog1.setCancelable(false);
				dialog1.show();		
			}
			else
			{
				cf.ShowToast("Sorry. This record has been reallocated to another inspector.",1);				
			}
		}			
	};
	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd1.dismiss();
			if(show_handler==0)
			{
				//cf.ShowToast("Your inspection has been successfully moved to IMC Accepted..",1);
				AlertDialog alertDialog1 = new AlertDialog.Builder(Exportedddata.this).create();
				alertDialog1.setMessage("Your inspection has been moved to IMC Accepted and PDF generated successfully.");
				alertDialog1.setButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) 
					{				
						Intent move = new Intent(Exportedddata.this, Exportedddata.class);
						move.putExtra("completedexport", "QA");
						startActivity(move);
						callthread();
					}					
				});
				alertDialog1.show();	

			}
			else if(show_handler==1)
			{
				cf.ShowToast("Problem in generating PDF.",1);
			}
			else if(show_handler==2)
			{
				cf.ShowToast("There is some problem in network. Please try again later.",1);
			}
			else if(show_handler==3)
			{
				cf.ShowToast("This inspector is not QA Disabled Inspector.",1);
			}
		}
	};

	private void callthread()
	{
		((TextView)findViewById(R.id.note)).setVisibility(cf.v1.GONE);
		((LinearLayout) findViewById(R.id.linlayoutdyn)).setVisibility(cf.v1.GONE);
		//((TextView) findViewById(R.id.txtlistheader)).setText("Total Rows : 0");
		String source = "<font color=#FFFFFF>Loading QA Inspection... Please wait..."+ "</font>";
		pd2 = ProgressDialog.show(Exportedddata.this,"", Html.fromHtml(source), true);
        new Thread(new Runnable() {
                public void run() {
                	
                	try {
                		System.out.println("inside run");
                		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            			envelope.dotNet = true;
            			SoapObject ad_property1 = new SoapObject(cf.NAMESPACE,"TPQAInspectionListing");
            			
            			ad_property1.addProperty("InspectorID", cf.InspectorId);
            			envelope.setOutputSoapObject(ad_property1);
            			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL);
            			androidHttpTransport1.call(cf.NAMESPACE+"TPQAInspectionListing", envelope);
            			resulttpqa = (SoapObject) envelope.getResponse();
            			System.out.println("resulttpqa"+resulttpqa);
                		show_handler3=0;System.out.println("teeeee");
            			handler3.sendEmptyMessage(0);
            			
            			System.out.println("completed");
            		} catch (Exception e) {
            			// TODO Auto-generated catch block
            			System.out.println("EE"+e.getMessage());
            			show_handler3=2;
            			handler3.sendEmptyMessage(0);
            		}                
                }
            }).start();	
	}
	
	private Handler handler3 = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			if (show_handler3 == 0) 
			{
				pd2.dismiss();
				Call_Dynamic_Pdf_Display(resulttpqa);
			}
			else if(show_handler3 == 1)
			{
				cf.ShowToast("You have problem in the server. Please try again later",0); 
			}
			else 
			{
				cf.ShowToast("There is some problem in network. Please try again later.",0);
			}

		}
	};
	private void showalert(final String status) {
		// TODO Auto-generated method stub
		alertDialog = new AlertDialog.Builder(Exportedddata.this).create();
		alertDialog.setMessage(status);
		alertDialog.setButton("OK",
		new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				
				Intent inte = new Intent(getApplicationContext(),Exportedddata.class);
				if(!complete_enable)
				{	
					  inte.putExtra("completedexport", "true");					
				}
				else
				{
					  inte.putExtra("completedexport", "false");
				}
				  startActivity(inte);
			}
		});
		alertDialog.show();
	}		
	private void Call_Dynamic_Pdf_Display(SoapObject resulttpqa) {

		// TODO Auto-generated method stub
		
		
		lldisplaypdf.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lldisplaypdf.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		int Cnt = resulttpqa.getPropertyCount();System.out.println("CountTPQA="+Cnt);

		//t.append("Total Record : ");
		((TextView)findViewById(R.id.note)).setVisibility(cf.v1.VISIBLE);
		//t.setVisibility(cf.v1.VISIBLE);
		t.setText("Total Record : "+String.valueOf(Cnt));
		//t.append("\n");
		
		String headernote="Note : Owner First Name Last Name | Address | City | State | County | Zipcode";
		((TextView)findViewById(R.id.note)).setText(headernote);
		
		TextView[] tvstatus = new TextView[Cnt];
		Button[] view = new Button[Cnt];
		final Button[] download = new Button[Cnt];
		LinearLayout.LayoutParams llparams;
		LinearLayout.LayoutParams tvparams;
		LinearLayout.LayoutParams downloadparams,pterparams,viewparams;
		LinearLayout.LayoutParams lltxtparams;
		LinearLayout[] rl = new LinearLayout[Cnt];
		pdfpath = new String[Cnt];
		String Firstname="", Middlename="", Lastname="", Address1="", Address2="", City="", Zipcode="", Homephone="", Cellphone="", Email="", Flag="", Pdfpath="", State="", County="";
		if (Cnt >= 1) {
			
			for (int i = 0; i < Cnt; i++) {

				SoapObject obj = (SoapObject) resulttpqa.getProperty(i);
				
				Firstname = String.valueOf(obj.getProperty("Firstname"));
				Middlename = String.valueOf(obj.getProperty("Middlename"));
				Lastname = String.valueOf(obj.getProperty("Lastname"));
				Address1 = String.valueOf(obj.getProperty("Address1"));
				Address2 = String.valueOf(obj.getProperty("Address2"));
				City = String.valueOf(obj.getProperty("City"));
				Zipcode = String.valueOf(obj.getProperty("Zipcode"));
				Homephone = String.valueOf(obj.getProperty("Homephone"));					
				Cellphone = String.valueOf(obj.getProperty("Cellphone"));
				Email = String.valueOf(obj.getProperty("Email"));
				Flag = String.valueOf(obj.getProperty("Flag"));
				Pdfpath = String.valueOf(obj.getProperty("Pdfpath"));
				State = String.valueOf(obj.getProperty("State"));
				County = String.valueOf(obj.getProperty("County"));
				pdfpath[i] = Pdfpath;
		
				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				llparams.setMargins(0, 0, 0, 0);
				
				tvparams = new LinearLayout.LayoutParams(
						600,
						ViewGroup.LayoutParams.MATCH_PARENT);
				//tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
				tvparams.setMargins(10, 0, 10, 0);

				downloadparams = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.MATCH_PARENT);
				//downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				downloadparams.setMargins(10, 0, 0, 0);
				
				viewparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				//viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				viewparams.setMargins(125, 0, 0, 0);
				
				rl[i] = new LinearLayout(this);
				rl[i].setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(rl[i], llparams);
				
				LinearLayout lltxt=new LinearLayout(this);
				lltxt.setLayoutParams(tvparams);
				rl[i].addView(lltxt);
				
				lltxtparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(0, 10, 0, 10);

				tvstatus[i] = new TextView(this);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setId(1);
				tvstatus[i].setText(Firstname+" "+Lastname+" | "+Address1 + " | "+City + " | "+State + " | "+ County + " | "+Zipcode);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(14);
				lltxt.addView(tvstatus[i]);
				
				LinearLayout llview=new LinearLayout(this);
				llview.setLayoutParams(viewparams);
				rl[i].addView(llview);
				
				LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				downloadparams1.gravity=Gravity.CENTER_VERTICAL;

				view[i] = new Button(this);
				view[i].setLayoutParams(downloadparams1);
				view[i].setId(2);
				view[i].setText("View PDF");
				//iew[i].setBackgroundResource(R.drawable.buttonrepeat);
				view[i].setTextColor(Color.BLACK);
				view[i].setTextSize(14);
				view[i].setTypeface(null, Typeface.BOLD);
				view[i].setTag(i);
//				view[i].setGravity(Gravity.CENTER_VERTICAL);
				llview.addView(view[i]);
				
				
				path = pdfpath[i];
				String[] filenamesplit = path.split("/");
				String filename = filenamesplit[filenamesplit.length - 1];
				System.out.println("the file name is" + filename);
				File sdDir = new File(Environment.getExternalStorageDirectory()
						.getPath());
				File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
						+ filename);

			
				
				view[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Button b = (Button) v;
					String buttonvalue = v.getTag().toString();
					System.out.println("buttonvalue is" + buttonvalue);
					int s = Integer.parseInt(buttonvalue);
					path = pdfpath[s];
					String[] filenamesplit = path
							.split("/");
					final String filename = filenamesplit[filenamesplit.length - 1];
					System.out
							.println("The File Name is "
									+ filename);
					File sdDir = new File(Environment.getExternalStorageDirectory()
							.getPath());
					File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
							+ filename);
					
					if(file.exists())
					{
						View_Pdf_File(filename);
					}
					else
					{
						if (cf.isInternetOn() == true) {
							String source = "<b><font color=#00FF33>Downloading</font></b>";
							pd = ProgressDialog.show(Exportedddata.this, "", Html.fromHtml(source), true);
							new Thread() {
								public void run() {
									Looper.prepare();
									try {
										String extStorageDirectory = Environment
												.getExternalStorageDirectory()
												.toString();
										File folder = new File(
												extStorageDirectory,
												"DownloadedPdfFile");
										folder.mkdir();
										File file = new File(folder,
												filename);
										try {
											file.createNewFile();
											Downloader.DownloadFile(path,
													file);
										} catch (IOException e1) {
											e1.printStackTrace();
										}

										show_handler = 2;
										handler.sendEmptyMessage(0);

									} catch (Exception e) {
										// TODO Auto-generated catch block
										System.out.println("The error is "
												+ e.getMessage());
										e.printStackTrace();
										show_handler = 1;
										handler.sendEmptyMessage(0);

									}
								}

								private Handler handler = new Handler() {
									@Override
									public void handleMessage(Message msg) {
										pd.dismiss();
										// dialog1.dismiss();
										if (show_handler == 1) {
											show_handler = 0;
											cf.ShowToast("There is a problem on your application. Please contact Paperless administrator.",0);

										} else if (show_handler == 2) {
											show_handler = 0;
											
											View_Pdf_File(filename);

										}
									}
								};
							}.start();
						} else {
							cf.ShowToast("Internet Connection is not Available",0);


						}
					}
				}
			});

				
			}
		}		
	}
	private void View_Pdf_File(String filename)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
			startActivity(Intent.createChooser(intent, "Select PDF option..."));
        } 
        catch (ActivityNotFoundException e) {
			Intent intentview = new Intent(Exportedddata.this,
					ViewPdfFile.class);
			intentview.putExtra("path", path);
			startActivity(intentview);
        }
	}
	private void Changestatus() {
		// TODO Auto-generated method stub
		try
		{
			Status_change();
			alertDialog = new AlertDialog.Builder(Exportedddata.this)
			.create();
			alertDialog.setMessage(status);
			alertDialog.setButton("OK",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					
					getauditflag();
					System.out.println("wat is flag="+auditflag);
					if(auditflag.equals("2"))
					{
						if(chkifquesphofdbfilled())
						{
							Intent inte = new Intent(getApplicationContext(),Exportedddata.class);
							inte.putExtra("completedexport", "false");
							startActivity(inte);
						}
						else
						{
							 showalert("You have to export Questions,Photos and Feedback section for Generating PDF.");
						}
					}
					else
					{
						Intent inte = new Intent(getApplicationContext(),Exportedddata.class);
						inte.putExtra("completedexport", "false");
						startActivity(inte);
					}
				}
			});
			alertDialog.show();
		
	} catch (SocketTimeoutException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NetworkErrorException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (XmlPullParserException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	private class ProgressThread extends Thread {

		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;
		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			mState = RUNNING;
			while (mState == RUNNING) {
				try {
					// Control speed of update (but precision of delay not
					// guaranteed)
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					Log.e("ERROR", "Thread was Interrupted");
				}
				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("total", (int)total);
				msg.setData(b);
				mHandler.sendMessage(msg);
			}
		}

		public void setState(int state) {
			mState = state;
		}

	}
	protected void re_export() {
		// TODO Auto-generated method stub
		Export_chk[1]=Export_chk[2]=Export_chk[3]=Export_chk[4]=Export_chk[5]=Export_chk[0]="0";
		
			if(sta_chk[0].isChecked())
				Export_chk[1]="1";
			else
				Export_chk[1]="0";
			
			if(sta_chk[1].isChecked())
				Export_chk[2]="1";
			else
				Export_chk[2]="0";
			
			if(sta_chk[2].isChecked())
				Export_chk[3]="1";
			else
				Export_chk[3]="0";
			
			if(sta_chk[3].isChecked())
				Export_chk[4]="1";
			else
				Export_chk[4]="0";
			
			if(sta_chk[4].isChecked())
				Export_chk[5]="1";
			else
				Export_chk[5]="0";		
			
			
			/** get the units for increament for every uploading **/
			int j=0;
			for(int i=1;i<Export_chk.length;i++)
			{
				if(Export_chk[i].equals("1"))
					j++;
			}
			
			try
			{
				incre_unit=80.00/Double.parseDouble(String.valueOf(j));
				System.out.println("increament unit is "+incre_unit+" j = "+j);
			}
			catch(Exception e)
			{
				incre_unit=10.00;
			}
			//int incre_unit=100/j;
			/** get the units for increament for every uploading Ends **/
			/** export will start here **/
			if(j>=1)
			{
				dialog1.setCancelable(true);
				dialog1.dismiss();
				StartExport(cf.selectedhomeid,incre_unit);
				
			}
			else
			{
				cf.ShowToast("Please select atleast one option ",1);
				//fn_export();
				
			}
		
	}
	

	private void BindExportData() {
		fetchRow("1");
	}

	public void fetchRow(String inspect_Value) {
try {
			
			rowlength = null;
			data = "";
		
			inirws = 0;
			namelist = new String[20];
			Cursor c = null;System.out.println("complete_enable"+complete_enable);
			if (complete_enable) {
				/*c = cf.SelectTablefunction(cf.mbtblInspectionList,
						"where IsUploaded='1' and IsInspected='2' and InspectorId='"
								+ cf.InspectorId.toString() + "'");*/
				
			
				c = cf.SelectTablefunction(cf.mbtblInspectionList,
						"where  IsInspected='2' and IsUploaded='1' and Status!='2' and InspectorId='"
								+ cf.InspectorId.toString() + "'");
			} else {
				c = cf.SelectTablefunction(cf.mbtblInspectionList,
						"where IsInspected='1' and IsUploaded='0' and InspectorId='"
								+ cf.InspectorId.toString() + "'");
			}
			inirws = c.getCount();
			rowlength = new String[inirws];
		/*	t.append("Total Record : ");
			
			t.append(String.valueOf(inirws));
			t.append("\n");*/
			t.setText("Total Record : "+String.valueOf(inirws));
			int i = 0;
			c.moveToFirst();
			if (inirws > 0) {
				
				if (c != null) {
					do {
						//data="";
						homelist[i] = c.getString(c.getColumnIndex("s_SRID"));
						System.out.println("homelist[i]= "+homelist[i]);
						
						data += " "
								+ cf.getsinglequotes(c.getString(c.getColumnIndex("s_OwnersNameFirst")))
								+ " ";
						data += cf.getsinglequotes(c.getString(c
								.getColumnIndex("s_OwnersNameLast"))) + " | ";
						data += cf.getsinglequotes(c.getString(c
								.getColumnIndex("s_OwnerPolicyNumber")))
								+ " \n ";
						data += cf.getsinglequotes(c.getString(5));
						data += " | ";
						data += cf.getsinglequotes(c.getString(7));
						data += " | ";
						data += cf.getsinglequotes(c.getString(8));
						data += " | ";
						data += cf.getsinglequotes(c.getString(9));
						data += " | ";
						data += cf.getsinglequotes(c.getString(10));
						data += " \n ";
						data += c.getString(29);
						data += " | ";
						data += c.getString(c
								.getColumnIndex("InspectionStartTime")) + " - ";
						data += c.getString(c
								.getColumnIndex("InspectionEndTime"))+ "~";
						
						if (data.contains("null")) {
							data = data.replace("null", "");
						}
						if (data.contains("N/A | ")) {
							data = data.replace("N/A |", "");
						}
						if (data.contains("N/A - N/A")) {
							data = data.replace("N/A - N/A", "");
						}
						System.out.println("Staus="+c.getString(c.getColumnIndex("Status"))+"IsInspected="+c.getString(c.getColumnIndex("IsInspected"))+"Isuplo="+c.getString(c.getColumnIndex("IsUploaded")));
						
						i++;
					} while (c.moveToNext());
					dynamicview(data);
				}
			} else {
				((TextView)findViewById(R.id.note)).setVisibility(cf.v1.GONE);
			}
			c.close();
		} catch (Exception e) {
			cf.ShowToast("No data found",1);

		}



	}
	private void dynamicview(String data2) {
		// TODO Auto-generated method stub
		//inspectionlist.removeAllViews();
		sv = new ScrollView(this);
		inspectionlist.addView(sv);
		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		if (!data2.equals(null) && !data2.equals("null") && !data2.equals("")) {
			this.rowlength = data2.split("~");
		for(int i=0;i<rowlength.length;i++)
		{
			tvstatus = new TextView[inirws];
			delbtn = new Button[inirws];
			LinearLayout l2 = new LinearLayout(this);
			LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			l2.setLayoutParams(mainparamschk);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l1.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 8;
			paramschk.leftMargin = 20;
			paramschk.bottomMargin = 10;
			l2.addView(lchkbox);
			tvstatus[i] = new TextView(this);
			tvstatus[i].setTag("textbtn" + i);
			tvstatus[i].setText(rowlength[i]);
			tvstatus[i].setTextColor(Color.WHITE);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			lchkbox.addView(tvstatus[i], paramschk);
	
			
			LinearLayout ldelbtn = new LinearLayout(this);
			LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
			ldelbtn.setLayoutParams(mainparamschk);
			ldelbtn.setGravity(Gravity.RIGHT);
		    l2.addView(ldelbtn);
		   
		    tvstatus[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(final View v) {
					
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace("textbtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							//cf.getInspecionTypeid(homelist[cvrtstr]);
							/*if(cf.checklicenceexpiry(cf.LicenceExpiryDate).equals("true"))
							{
								cf.ShowToast("Your licence date has been expired.",1);
							}
							else
							{*/
								if (!complete_enable) {
									//temb.setBackgroundResource(R.drawable.button_back_blue);
									
									dt=homelist[cvrtstr];
									cf.selectedhomeid = dt;
									fn_export();
									/*if(checkifallfilled())
									{*/
										//showauditalert();
									//}
								}
								else
								{
									dt=homelist[cvrtstr];
									cf.selectedhomeid=dt;
									
									String source = "<b><font color=#00FF33>Retrieving data. Please wait..."
											+ "</font></b>";
									cf.pd = ProgressDialog.show(Exportedddata.this, "", Html.fromHtml(source), true);
									Thread thread = new Thread(Exportedddata.this);
									thread.start();
								}
							//}
						
				}
			});
		
			
			
		    if(value.equals("false"))
			{
		    	 	delbtn[i] = new Button(this);
				    delbtn[i].setBackgroundResource(R.drawable.deletebtn1);
				    delbtn[i].setTag("deletebtn" + i);
				    delbtn[i].setPadding(30, 0, 0, 0);
					ldelbtn.addView(delbtn[i], paramsdelbtn);
					
					delbtn[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();    
							final String repidofselbtn = getidofselbtn.replace(
									"deletebtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							final String dt = homelist[cvrtstr];
							 cf.alerttitle="Delete";
							  cf.alertcontent="Are you sure want to delete?";
							    final Dialog dialog1 = new Dialog(Exportedddata.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alertsync);
								TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
								txttitle.setText( cf.alerttitle);
								TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml( cf.alertcontent));
								Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
								Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
								btn_yes.setOnClickListener(new OnClickListener()
								{
				                	public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										cf.fn_delete(dt);
										//startActivity(new Intent(Exportedddata.this,Exportedddata.class));
										Intent inte = new Intent(Exportedddata.this,Exportedddata.class);
										  inte.putExtra("completedexport", "false");
										  startActivity(inte);

									}
									
								});
								btn_cancel.setOnClickListener(new OnClickListener()
								{

									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										
									}
									
								});
								dialog1.setCancelable(false);
								dialog1.show();
						
								
						} 
					});
			}
			else
			{
				
			}
		    
			
			System.out.println("value is what "+value);
			
			
	
			
			}
		}
	}

	 protected void showauditalert() {
		// TODO Auto-generated method stub

			cf.Createtablefunction(2);
			Cursor c1 = cf.db.rawQuery("select * from "+ cf.mbtblInspectionList + " where s_SRID='" + cf.selectedhomeid+ "'", null);
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				if(c1.getString(c1.getColumnIndex("InspectionTypeId")).equals("28"))
				{
					System.out.println("TPQA="+c1.getString(c1.getColumnIndex("TPQA")));
					System.out.println("Additionalservice="+c1.getString(c1.getColumnIndex("Additionalservice")));
					System.out.println("QA="+c1.getString(c1.getColumnIndex("QAInspector")));
					if(c1.getString(c1.getColumnIndex("TPQA")).equals("False") && c1.getString(c1.getColumnIndex("Additionalservice")).equals("False") && c1.getString(c1.getColumnIndex("QAInspector")).equals("True"))
					{
						final Dialog dialog2 = new Dialog(Exportedddata.this,android.R.style.Theme_Translucent_NoTitleBar);
						dialog2.getWindow().setContentView(R.layout.alert);
						TextView txttitle = (TextView) dialog2.findViewById(R.id.txthelp);
						txttitle.setText("Audit Process");
						TextView txt = (TextView) dialog2.findViewById(R.id.txtid);
						txt.setText(Html.fromHtml("Do you want to follow the audit process?"));
						Button btn_helpclose = (Button) dialog2.findViewById(R.id.helpclose);
						btn_helpclose.setVisibility(v1.VISIBLE);
						Button btn_ok = (Button) dialog2.findViewById(R.id.ok);
						btn_ok.setVisibility(v1.VISIBLE);btn_ok.setText("Yes");
						btn_ok.setOnClickListener(new OnClickListener()
						{
						public void onClick(View arg0) {
								// TODO Auto-generated method stub												
							dialog2.dismiss();
							flag="Yes";
							fn_export();
							}
						});
						Button btn_dash = (Button) dialog2.findViewById(R.id.dash);
						btn_dash.setVisibility(v1.VISIBLE);btn_dash.setText("No");
						btn_dash.setOnClickListener(new OnClickListener()
						{
						public void onClick(View arg0) {
								// TODO Auto-generated method stub
							dialog2.dismiss();
							flag="No";
							fn_export();
						 }												
						});
						dialog2.setCancelable(false);
						dialog2.show();
					}
					else
					{
						fn_export();
					}
				}
				else
				{
					fn_export();
				}
			}	
	}

	protected void fn_export() {
			// TODO Auto-generated method stub
		
		 	final Dialog alert=cf.showalert();
			Button btn_helpclose = (Button) alert.findViewById(R.id.helpclose_ex);
			Button btn_helpclose_t = (Button) alert.findViewById(R.id.helpclose_ex_t);
			btn_helpclose.setOnClickListener(new OnClickListener()
			{

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					for(int i=1;i<Export_chk.length;i++)
					{
						Export_chk[i]="0";
					}
					
					alert.setCancelable(true);
					alert.dismiss();
				}
				
			});
			btn_helpclose_t.setOnClickListener(new OnClickListener()
			{

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					for(int i=1;i<Export_chk.length;i++)
					{
						Export_chk[i]="0";
					}
					
					alert.setCancelable(true);
					alert.dismiss();
				}
				
			});
			alert_chk[0]=(CheckBox) alert.findViewById(R.id.Export_chk_all);
			alert_chk[1]=(CheckBox) alert.findViewById(R.id.Export_chk_PH);
			alert_chk[2]=(CheckBox) alert.findViewById(R.id.Export_chk_QUES);
			alert_chk[3]=(CheckBox) alert.findViewById(R.id.Export_chk_Photo);
			alert_chk[4]=(CheckBox) alert.findViewById(R.id.Export_chk_FB);
			alert_chk[5]=(CheckBox) alert.findViewById(R.id.Export_chk_GI);
			
			alert_chk[0].setOnClickListener(new clicker());
			alert_chk[1].setOnClickListener(new clicker());
			alert_chk[2].setOnClickListener(new clicker());
			alert_chk[3].setOnClickListener(new clicker());
			alert_chk[4].setOnClickListener(new clicker());
			alert_chk[5].setOnClickListener(new clicker());

			Button export=(Button) alert.findViewById(R.id.export_now);
			LinearLayout lin = (LinearLayout)alert.findViewById(R.id.ExportOption);
			lin.setVisibility(View.VISIBLE);
			LinearLayout lin1= (LinearLayout)alert.findViewById(R.id.maintable);
			lin1.setVisibility(View.GONE);
			export.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					int j=0;
					double incre_unit;
					
					for(int i=1;i<Export_chk.length;i++)
					{
						if(Export_chk[i].equals("1"))
							j++;
					}
					try
					{
						incre_unit=80.00/Double.parseDouble(String.valueOf(j));
					}
					catch(Exception e)
					{
						incre_unit=10.00;
					}
					if(j>=1)
					{
					/** export will start here **/
						alert.setCancelable(true);
						alert.dismiss();					
						StartExport(dt,incre_unit);						
					//StartExport(dt,incre_unit);
					/** export will Ends here **/
					}
					else
					{
						cf.ShowToast("Please select atleast one option ",1);
						//fn_export();
					}
				}
			});
			alert.setCancelable(false);
			alert.show();

		}
	 private void StartExport(String dt, final double inc_u) {
			// TODO Auto-generated method stub
		 //System.out.println("fal="+auditflag);
		 increment_unit=inc_u;
		 cf.selectedhomeid=dt;
		 
		 if (cf.isInternetOn() == true) {System.out.println("pow");
			 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);System.out.println("pm "+pm);
			 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");System.out.println("wl");
			 wl.acquire();System.out.println("acquire");
			 			 
			 getNetConnectDetail();System.out.println("getNetConnectDetail");
			 total=0.0;
         /**We need to delete the preivious progress bar then we start new one code starts herer**/
			try
			{
			removeDialog(1);
			}
			catch (Exception e)
			{
				
			}
			/**We need to delete the preivious progress bar then we start new one code Ends herer**/
          progDialog = new ProgressDialog(Exportedddata.this); // we creat dilog box 
			showDialog(1);
			
			new Thread() {

				private String elevvetiondes;
				private int usercheck;

				public void run() {
					Looper.prepare();

					try {
						total = 1.0;
						Exportbartext="Check the status of inspection ";
						total=5.0;
						if (!"".equals(cf.selectedhomeid)) {
							//Check if the record in the correct status for uploading
							
							cf.Chk_Inspector=IsCurrentInspector(cf.InspectorId,cf.selectedhomeid);
							if(cf.Chk_Inspector.equals("true"))
							{
								 boolean insp_sta=false;
								try
								{
									 insp_sta=cf.getinspectionstatus(cf.InspectorId,"GetInspectionStatus");
								}
								catch (NetworkErrorException n) 
								{
									String	strerrorlog="Network exception happens while retrieving the Inspection status Error= "+n;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									
									throw n;
								}
								catch (SocketTimeoutException s) 
								{
									String	strerrorlog="Socket Timeout exception happens while retrieving the Inspection status Error ="+s;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									
									throw s;
								}
								catch (IOException io) 
								{

									String	strerrorlog="Inputoutput exception happens while retrieving the Inspection status Error ="+io;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem in retrieving the Inspection status in the server please contact paperless admin ");
									
								}
								catch (XmlPullParserException x) 
								{
									String	strerrorlog="Xmlparser  exception happens while retrieving the Inspection status Error ="+x;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem retrieving the Inspection status information ");
									
								}
								catch (Exception ex) 
								{
									String	strerrorlog="Exception happens while Uploading the policy holder information Error ="+ex;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem retrieving the Inspection status information ");
									
								}
							
						if (insp_sta) {
							
							// Checking The option which are selected 
							if(Export_chk[0].equals("1")||Export_chk[1].equals("1"))
							{
								
								Exportbartext="Uploading Policy holder information ";
								try
								{
									cf.Createtablefunction(3);
									cf.Createtablefunction(7);
									Export_status[0]=(updatepolicy()) ? true:false;
									getNetConnectDetail();
									//Error_traker("Data moved success fully ");
								}
								catch (NetworkErrorException n) 
								{
									String	strerrorlog="Network exception happens while Uploading the policy holder information Error= "+n;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									
									throw n;
								}
								catch (SocketTimeoutException s) 
								{
									String	strerrorlog="Socket Timeout exception happens while Uploading the policy holder information Error ="+s;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									throw s;
								}
								catch (IOException io) 
								{

									String	strerrorlog="Inputoutput exception happens while Uploading the policy holder information Error ="+io;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem  in the server please contact paperless admin");
								}
								catch (XmlPullParserException x) 
								{

									String	strerrorlog="Xmlparser  exception happens while Uploading the policy holder information Error ="+x;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem uploading the Policy holder information ");
								}
								catch (Exception ex) 
								{

									String	strerrorlog="Exception happens while Uploading the policy holder information Error ="+ex;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem uploading the Policy holder information ");
								}
								total=total+inc_u;
							}
							if(Export_chk[0].equals("1")||Export_chk[2].equals("1"))
							{
								Exportbartext="Uploading Question Information ";
								try
								{	cf.Createtablefunction(4);
								Export_status[1]=(Upload_Question()) ? true:false;
								getNetConnectDetail();
								//Error_traker("Data moved successfully");
								}
								catch (NetworkErrorException n) 
								{
									String	strerrorlog="Network exception happens while Uploading the Upload_Summary_Question information Error= "+n;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									throw n;
								}
								catch (SocketTimeoutException s) 
								{
									String	strerrorlog="Socket Timeout exception happens while Uploading the Upload_Summary_Question information Error ="+s;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									throw s;
								}
								catch (IOException io) 
								{

									String	strerrorlog="Inputoutput exception happens while Uploading the Upload_Summary_Question information Error ="+io;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem  in the server please contact paperless admin");
									throw io;
									
								}
								catch (XmlPullParserException x) 
								{

									String	strerrorlog="Xmlparser  exception happens while Uploading the Upload_Summary_Question information Error ="+x;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem uploadin the uploading question information ");
								}
								catch (Exception ex) 
								{

									String	strerrorlog="Exception happens while Uploading the Upload_Summary_Question information Error ="+ex;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem uploadin the uploading question information ");
								}
								total=total+inc_u;
							}
							
							
							if(Export_chk[0].equals("1")||Export_chk[3].equals("1"))
							{ 
								Exportbartext="Uploading Photos ";
								try
								{
									cf.Createtablefunction(13);
									Export_status[2]=(Upload_Photos()) ? true:false;
									getNetConnectDetail();
								}
								catch (NetworkErrorException n) 
								{
									String	strerrorlog="Network exception happens while Uploading the Uploading_Observation2 information Error= "+n;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									throw n;
								}
								catch (SocketTimeoutException s) 
								{
									String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_Observation2 information Error ="+s;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									throw s;
								}
								catch (IOException io) 
								{
									countchk=1;
									String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_Observation2 information Error ="+io;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem  in the server please contact paperless admin ");
									throw io;
								}
								catch (XmlPullParserException x) 
								{

									String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_Observation2 information Error ="+x;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem uploading the photos");
								}
								catch (Exception ex) 
								{

									String	strerrorlog="Exception happens while Uploading the Uploading_Observation2 information Error ="+ex;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export insection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("you have problem uploading the photos");
								}
								
							}
							
				
							
							if(Export_chk[0].equals("1")||Export_chk[4].equals("1"))
							{
								Exportbartext="Uploading Feedback document ";
								try
								{
									cf.Createtablefunction(10);
									cf.Createtablefunction(11);
									Export_status[3]=(Uploading_feedback()) ? true:false;getNetConnectDetail();
								}
								catch (NetworkErrorException n) 
								{
									
									String	strerrorlog="Network exception happens while Uploading the Uploading_feedback information Error= "+n;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									throw n;
								}
								catch (SocketTimeoutException s) 
								{
									String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_feedback information Error ="+s;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your network connection ");
									throw s;
								}
								catch (IOException io) 
								{

									String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_feedback information Error ="+io;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have problem  in the server please contact paperless admin");
									throw io;
								}
								catch (XmlPullParserException x) 
								{

									String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_feedback information Error ="+x;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have problem while uploading the feed back  information. ");
								}
								catch (Exception ex) 
								{

									String	strerrorlog="Exception happens while Uploading the Uploading_feedback information Error ="+ex;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have problem while uploading the feed back information. ");
								}
								
							}
							
							if(Export_chk[0].equals("1")||Export_chk[5].equals("1"))
							{	
								Exportbartext="Uploading General Information";
								
								try
								{
									cf.Createtablefunction(14);
									cf.Createtablefunction(15);
									Export_status[4]=(Uploading_generalinformation()) ? true:false;getNetConnectDetail();
									System.out.println("Export_status[4] "+Export_status[4]);
								}
								catch (NetworkErrorException n) 
								{
									String	strerrorlog="Network exception happens while Uploading the Uploading_feedback information Error= "+n;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your networ connection ");
									throw n;
								}
								catch (SocketTimeoutException s) 
								{
									String	strerrorlog="Socket Timeout exception happens while Uploading the Uploading_feedback information Error ="+s;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have internet connection problem please check your networ connection ");
									throw s;
								}
								catch (IOException io) 
								{

									String	strerrorlog="Inputoutput exception happens while Uploading the Uploading_feedback information Error ="+io;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have problem  in the server please contact paperless admin");
									throw io;
								}
								catch (XmlPullParserException x) 
								{

									String	strerrorlog="Xmlparser  exception happens while Uploading the Uploading_feedback information Error ="+x;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have problem while uploading the general information.");
								}
								catch (Exception ex) 
								{

									String	strerrorlog="Exception happens while Uploading the Uploading_feedback information Error ="+ex;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									Error_traker("You have problem while uploading the general information.");
								}
								
							}
							Exportbartext="Checking Status";
							try
							{
								show_staus();
							}
							catch (NetworkErrorException n) 
							{
								String	strerrorlog="Network exception happens while getting the status  information Error= "+n;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
								throw n;
							}
							catch (SocketTimeoutException s) 
							{
								String	strerrorlog="Socket Timeout exception happens while  getting the  Error ="+s;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
								throw s;
							}
							catch (IOException io) 
							{
								String	strerrorlog="Inputoutput exception happens  gettting the status Error ="+io;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
								throw io;
							}
							catch (XmlPullParserException x) 
							{
								String	strerrorlog="Xmlparser  exception happens while  getting the status  Error ="+x;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
							}
							catch (Exception ex) 
							{
								String	strerrorlog="Exception happens while getting the status information Error ="+ex;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
							}
							try
							{
								
					
								
							}
							catch (Exception ex) 
							{

								String	strerrorlog="Exception happens while Uploading the Uploading_feedback information Error ="+ex;
								cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
								Error_traker("you have problem in moving the schedule to photots ");
							}
							total=total+5;
								try
								{
									Error_logupload();
								}
								catch (NetworkErrorException n) 
								{
									String	strerrorlog="Network exception happens while uploading error log file information Error= "+n;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									
									throw n;
								}
								catch (SocketTimeoutException s) 
								{
									String	strerrorlog="Socket Timeout exception happens while uploading error log file Error ="+s;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									
									throw s;
								}
								catch (IOException io) 
								{

									String	strerrorlog="Inputoutput exception happens uploading error log file Error ="+io;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
							
									throw io;
								}
								catch (XmlPullParserException x) 
								{

									String	strerrorlog="Xmlparser  exception happens while  uploading error log file Error ="+x;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									
								}
								catch (Exception ex) 
								{

									String	strerrorlog="Exception happens whileuploading error log file information Error ="+ex;
									cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
									
								}
								if(wl!=null)
								{
								wl.release();
								wl=null;
								}
								total=total+5;
								total=100; // Please coomment this.
						}
							
					     }
						}
						else
						{
							total=100;
							if(wl!=null)
							{
							wl.release();
							wl=null;
							}
							Error_traker("Selected Inspection Status has been changed so you cannot able to upload this Record");
						}
					}
					catch(Exception e)
						{
						String	strerrorlog="Exception happens in the overall exception Error ="+e;
						cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection   in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
							if(wl!=null)
							{
							wl.release();
							wl=null;
							}
						}
					if(wl!=null)
					{
					wl.release();
					wl=null;
					}
					total=100.00;
					}

				

				
				}.start();
	 }
	 else
	 {
		 cf.ShowToast("Internet connection is not available.", 1);
	 }
	 }
	private boolean checkifallfilled()
	{
		System.out.println("selecte="+cf.selectedhomeid);
		Cursor qc = cf.SelectTablefunction(cf.tbl_comments, " where SRID='" +cf.selectedhomeid + "'");System.out.println("ccoc"+qc.getCount());
		if(qc.getCount()>0)
		{
			qc.moveToFirst();
			if((!cf.getsinglequotes(qc.getString(qc.getColumnIndex("BuildingCodeComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofCoverComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofDeckComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("SecondaryWaterComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofWallComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofGeometryComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("OpeningProtectionComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("WallConstructionComment"))).equals("")))
			{				
				completed=true;
			}
			else
			{
				completed=false;
			}
		}
		
		 Cursor imgcur = cf.SelectTablefunction(cf.ImageTable, " where SRID='" + cf.selectedhomeid + "'");
		 Cursor	fdinfocur = cf.SelectTablefunction(cf.FeedBackInfoTable, " where SRID='" + cf.selectedhomeid + "'");
		 Cursor	fddocucur = cf.SelectTablefunction(cf.FeedBackDocumentTable, " where SRID='" + cf.selectedhomeid + "'");
		 Cursor overcur =  cf.SelectTablefunction(cf.tbl_comments, " WHERE SRID='" + cf.selectedhomeid+ "'");
		 if(overcur.getCount()>0)
		 {
			 overcur.moveToFirst();
			 overallcomm = cf.getsinglequotes(overcur.getString(overcur.getColumnIndex("InsOverAllComments")));
		 }
		 System.out.println("qc.getCount()"+qc.getCount());
		 System.out.println("completed="+completed);
		 System.out.println("imgcur.getCount()="+imgcur.getCount());
		 
		 
			
		 if((qc.getCount()!=0 || completed==true) && imgcur.getCount()!=0 && (fdinfocur.getCount()!=0 || fddocucur.getCount()!=0) && !overallcomm.equals(""))
		 {
			 chkifalldatafilled=true;
		 }
		 else
		 {
			 chkifalldatafilled=false;
		 }
System.out.println("chkifall="+chkifalldatafilled);

		return chkifalldatafilled; 
	}
	
	private boolean chkifquesphofdbfilled()
	{
		System.out.println("statuschange[0]="+statuschange[0]);
		System.out.println("statuschange[0]="+statuschange[1]);
		System.out.println("statuschange[0]="+statuschange[2]);
		System.out.println("statuschange[0]="+statuschange[3]);
		System.out.println("statuschange[0]="+statuschange[4]);
		if((statuschange[0]==true && statuschange[1]==true && statuschange[2]==true) && statuschange[3]==true && statuschange[4]==true)
		{
			try
			{
				Cursor qc = cf.SelectTablefunction(cf.tbl_comments, " where SRID='" + cf.selectedhomeid + "'");
				if(qc.getCount()>0)
				{
					qc.moveToFirst();
					if((!cf.getsinglequotes(qc.getString(qc.getColumnIndex("BuildingCodeComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofCoverComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofDeckComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("SecondaryWaterComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofWallComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofGeometryComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("OpeningProtectionComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("WallConstructionComment"))).equals("")))
					{
						chkifquesphofdbfilled=true;
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("eee "+e.getMessage());
			}
	
		}
		else
		{
			chkifquesphofdbfilled=false;
			
		}	
		System.out.println("chkifquesphofdbfilled="+chkifquesphofdbfilled);
		return chkifquesphofdbfilled; 
	}

	
	private boolean chkifpartialldatafilled()
	{
		if((statuschange[0]==true && statuschange[1]==true && statuschange[2]==true))
		{
			try
			{
				Cursor qc = cf.SelectTablefunction(cf.tbl_comments, " where SRID='" + cf.selectedhomeid + "'");
				if(qc.getCount()>0)
				{
					qc.moveToFirst();
					if((!cf.getsinglequotes(qc.getString(qc.getColumnIndex("BuildingCodeComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofCoverComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofDeckComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("SecondaryWaterComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofWallComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofGeometryComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("OpeningProtectionComment"))).equals(""))
							&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("WallConstructionComment"))).equals("")))
					{
						chkifpartialfilledtrue=true;
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("eee "+e.getMessage());
			}
	
		}
		else
		{
			if(statuschange[3]==true || statuschange[4]==true)
			{
				chkifpartialfilledtrue=true;
			}
			
		}	
		System.out.println("chkifpartialldatafilled="+chkifpartialfilledtrue);
		return chkifpartialfilledtrue; 
	}
	private class clicker implements OnClickListener 
		{
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch (v.getId()) {
				case R.id.Export_chk_all:
					if(alert_chk[0].isChecked())
					{
						Export_chk[0]="1";
						Export_chk[1]="1";
						Export_chk[2]="1";
						Export_chk[3]="1";
						Export_chk[4]="1";
						Export_chk[5]="1";
						
						alert_chk[1].setChecked(true);
						alert_chk[2].setChecked(true);
						alert_chk[3].setChecked(true);
						alert_chk[4].setChecked(true);
						alert_chk[5].setChecked(true);
					}
					else
					{
						Export_chk[0]="0";
						Export_chk[1]="0";
						Export_chk[2]="0";
						Export_chk[3]="0";
						Export_chk[4]="0";
						Export_chk[5]="0";
						
						alert_chk[1].setChecked(false);
						alert_chk[2].setChecked(false);
						alert_chk[3].setChecked(false);
						alert_chk[4].setChecked(false);
						alert_chk[5].setChecked(false);
					}
					
				break;
				case R.id.Export_chk_PH:
					if(alert_chk[1].isChecked())
					{
						Export_chk[1]="1";
					}
					else
					{
						Export_chk[0]="0";
						Export_chk[1]="0";
						alert_chk[0].setChecked(false);
					}
				break;
				case R.id.Export_chk_QUES:
					if(alert_chk[2].isChecked())
					{
						Export_chk[2]="1";
					}
					else
					{
						Export_chk[0]="0";
						Export_chk[2]="0";
						alert_chk[0].setChecked(false);
					}
				break;				
				case R.id.Export_chk_Photo:
					if(alert_chk[3].isChecked())
					{
						Export_chk[3]="1";
					}
					else
					{
						Export_chk[0]="0";
						Export_chk[3]="0";
						alert_chk[0].setChecked(false);
					}
				break;
				
				
				case R.id.Export_chk_FB:
					if(alert_chk[4].isChecked())
					{
						Export_chk[4]="1";
					}
					else
					{
						Export_chk[0]="0";
						Export_chk[4]="0";
						alert_chk[0].setChecked(false);
					}
				break;
				case R.id.Export_chk_GI:
					if(alert_chk[5].isChecked())
					{
						Export_chk[5]="1";
					}
					else
					{
						Export_chk[0]="0";
						Export_chk[5]="0";
						alert_chk[0].setChecked(false);
					}
				break;
				
				}
			}
	}
	public void run() {
		// TODO Auto-generated method stub
		try {
			cf.exprtcnt=cf.fn_CheckExportCount(cf.InspectorId,cf.selectedhomeid);
			show_handler=1;
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ Exportedddata.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			 
			
		} catch (NetworkErrorException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ Exportedddata.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ Exportedddata.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		
		} catch (TimeoutException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ Exportedddata.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		
		} catch (XmlPullParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ Exportedddata.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			show_handler=2;
			cf.Error_LogFile_Creation(e1.getMessage()+" "+" at "+ Exportedddata.this+" "+" in the stage of retrieving re-export count at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		
		}
		myhandler.sendEmptyMessage(0);
	}
	private Handler myhandler = new Handler() {


		public void handleMessage(Message msg) {
			if(show_handler==1)
			{
				show_handler=0;cf.pd.dismiss();
			
				if (complete_enable) 
				{
					
				}
					else{
				
					    dialog1.dismiss();}
				
						cf.alerttitle="Re-export";
					    cf.alertcontent="This inspection has been exported already."+"<br /><br />"+"<b>Details :</b>"+cf.exprtcnt+
							    "<b>Are you sure want to Re-export again?</b>";
					    final Dialog dialog1 = new Dialog(Exportedddata.this,android.R.style.Theme_Translucent_NoTitleBar);
						dialog1.getWindow().setContentView(R.layout.alertsync);
						TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
						txttitle.setText( cf.alerttitle);
						TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
						txt.setText(Html.fromHtml( cf.alertcontent));
						Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
						Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
						btn_yes.setOnClickListener(new OnClickListener()
						{
		                	public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								if(value.equals("false"))
								{
									fn_export();
								}
								else{
								 StartExport(cf.selectedhomeid,incre_unit);}
								
									//showauditalert();
								
							}

							
						
						});
						btn_cancel.setOnClickListener(new OnClickListener()
						{

							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								
							}
							
						});
						dialog1.setCancelable(false);
						dialog1.show();
			}
			else if(show_handler==2)
			{
				show_handler=0;cf.pd.dismiss();
				cf.ShowToast("Please check your network connection and try again later.",1);
				
			}
		}
	};

	private boolean QAEnabled() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException
	{
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			SoapObject ad_property = new SoapObject(cf.NAMESPACE,"QAINSPECTOR");
			ad_property.addProperty("InspectorID", cf.InspectorId);
			envelope.setOutputSoapObject(ad_property);System.out.println("ad_pr"+ad_property);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL);
			androidHttpTransport1.call(cf.NAMESPACE+"QAINSPECTOR", envelope);
			String result = String.valueOf(envelope.getResponse());
			System.out.println("QAEnabled"+result);
			return cf.check_Status(result);
			
	}
	private boolean TPQA() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException
	{
		getauditflag();
			if(auditflag.equals("1"))
			{
				flag="Yes";
			}
			else  if(auditflag.equals("2"))
			{
				flag="No";
			}
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			SoapObject ad_property = new SoapObject(cf.NAMESPACE,"GeneratePdf");
		    ad_property.addProperty("InspectorID", cf.InspectorId);
			ad_property.addProperty("SRID", cf.selectedhomeid);
			ad_property.addProperty("Flag", "No");
			envelope.setOutputSoapObject(ad_property);System.out.println("ad_pr"+ad_property);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_PDF);
			androidHttpTransport1.call(cf.NAMESPACE+"GeneratePdf", envelope);
			String result = String.valueOf(envelope.getResponse());
			System.out.println("TPQA "+result);
			return cf.check_Status(result);
			
}
	public void show_staus() throws IOException, XmlPullParserException ,SocketTimeoutException,Exception{
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(cf.NAMESPACE,"GetInspectionStatus");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID",cf.InspectorId);
		request.addProperty("SRID",cf.selectedhomeid);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE+"GetInspectionStatus",envelope);
		String result =  envelope.getResponse().toString();
		System.out.println("statusresult "+result);
		
	}
		private boolean updatepolicy() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException{
			
		cf.Createtablefunction(2);
			cf.methodname=cf.METHOD_NAME23;
			
			String chk;
			Cursor c = cf.SelectTablefunction(cf.mbtblInspectionList,
					"where s_SRID='" + cf.selectedhomeid + "'");
			if(c.getCount()>=1)
			{
			
			c.moveToFirst();
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			SoapObject ad_property = new SoapObject(cf.NAMESPACE,
					cf.METHOD_NAME23);
			
			ad_property.addProperty("InspectorID", cf.InspectorId);
			ad_property.addProperty("SRID", c.getString(1));
			homeid = c.getString(1);
			
			ad_property.addProperty("OwnerFirstName",
					cf.getsinglequotes(c.getString(2)));
			ad_property.addProperty("OwnerLastName",
					cf.getsinglequotes(c.getString(3)));
			ad_property.addProperty("Address",
					cf.getsinglequotes(c.getString(5)));
			ad_property.addProperty("City",
					cf.getsinglequotes(c.getString(7)));
			ad_property.addProperty("Zip", c.getString(10));
			ad_property.addProperty("State", "10");
			ad_property.addProperty("County",
					cf.getsinglequotes(c.getString(9)));
			ad_property.addProperty("YearBuilt", c.getString(11));
			ad_property.addProperty("InspectionDate", c.getString(29));
			ad_property.addProperty("ContactPerson",
					cf.getsinglequotes(c.getString(15)));
			ad_property.addProperty("HomePhone", c.getString(12));
			ad_property.addProperty("WorkPhone", c.getString(13));
			ad_property.addProperty("CellPhone", c.getString(14));
			ad_property.addProperty("Nstories", c.getString(19));
			ad_property.addProperty("Email", c.getString(18));
			ad_property.addProperty("SchedulingComments",
					cf.getsinglequotes(c.getString(32)));
			
			insrendate = c.getString(c
					.getColumnIndex("Insurancerenewaldate"));
			envelope.setOutputSoapObject(ad_property);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
					cf.URL);
			
			androidHttpTransport1.call(cf.SOAP_ACTION23, envelope);
		String result1 = String.valueOf(envelope.getResponse());
			System.out.println("REsult "+result1);
			return cf.check_Status(result1);
			
			}
			return true;
			}
		private boolean Upload_Question() throws NetworkErrorException,IOException, XmlPullParserException,SocketTimeoutException, Exception 
		{			
			if(Questionsthrows())
			{
				if(updateroofcover())
				{
					if(quescomm())
					{
						
					}
				}
			}
			return true;
		}
		private boolean Questionsthrows() throws NetworkErrorException,IOException, XmlPullParserException,SocketTimeoutException, Exception 
		{
			cf.methodname=cf.METHOD_NAME22;
			String chk1;
			cf.Createtablefunction(7);
			cf.Createtablefunction(12);
			
			qc = cf.SelectTablefunction(cf.tbl_questionsdata,
					"where SRID='" + cf.selectedhomeid + "'");
			
			
			if(qc.getCount()>=1)
			{

			qc.moveToFirst();
			Cursor roof = cf.SelectTablefunction(cf.quesroofcover,
					"where SRID='" + cf.selectedhomeid + "'");
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			SoapObject ad_property = new SoapObject(cf.NAMESPACE,
					cf.METHOD_NAME22);
			ad_property.addProperty("InspectorID", cf.InspectorId);
			if ("".equalsIgnoreCase(qc.getString(1))) {
				ad_property.addProperty("SRID", "");
			} else {
				ad_property.addProperty("SRID", qc.getString(1));
			}
			if ("".equalsIgnoreCase(qc.getString(2))) {
				ad_property.addProperty("BuildingCodeYearBuilt", "");
			} else {
				ad_property.addProperty("BuildingCodeYearBuilt",
						qc.getString(2));
			}
			if ("".equalsIgnoreCase(qc.getString(4))) {
				ad_property.addProperty("BuildingCodePerApplnDate", "");
			} else {
				ad_property.addProperty("BuildingCodePerApplnDate",
						qc.getString(4));
			}
			if ("".equalsIgnoreCase(qc.getString(3))) {
				ad_property.addProperty("BuildingCodeValue", 0);
			} else {
				ad_property.addProperty("BuildingCodeValue",
						qc.getString(3));
			}

			if ("".equalsIgnoreCase(qc.getString(5))) {
				ad_property.addProperty("RoofCoverType", 0);
			} else {
				ad_property.addProperty("RoofCoverType", 0);
			}
			
			roof.moveToFirst();
			if(roof.getCount()>=1)
			{
				if ("".equalsIgnoreCase(roof.getString(3))) {
					ad_property.addProperty("RoofCoverValue", 0);
				} else {
					ad_property.addProperty("RoofCoverValue",
							roof.getInt(3));
				}
				if ("".equalsIgnoreCase(roof.getString(10))) {
					ad_property.addProperty("RoofCoverOtherText", "");
				} else {
					ad_property.addProperty("RoofCoverOtherText",
							cf.getsinglequotes(roof.getString(10)));
				}
			}
			else
			{
				chk1="true";
			}
			
			if ("".equalsIgnoreCase(qc.getString(8))) {
				ad_property.addProperty("RoofCoverPerApplnDate", "");
			} else {
				ad_property.addProperty("RoofCoverPerApplnDate", "");
			}
			if ("".equalsIgnoreCase(qc.getString(9))) {
				ad_property.addProperty("RoofCoverYearofInsDate", "");
			} else {
				ad_property.addProperty("RoofCoverYearofInsDate", "");
			}
			if ("".equalsIgnoreCase(qc.getString(10))) {
				ad_property.addProperty("RoofCoverProdAppr", "");
			} else {
				ad_property.addProperty("RoofCoverProdAppr", "");
			}
			if ("".equalsIgnoreCase(qc.getString(11))) {
				ad_property.addProperty("RoofCoverNoInfnProvide", 0);
			} else {
				ad_property.addProperty("RoofCoverNoInfnProvide", 0);
			}
			if ("".equalsIgnoreCase(qc.getString(12))) {
				ad_property.addProperty("RoofDeckValue", 0);
			} else {
				ad_property.addProperty("RoofDeckValue",
						qc.getString(12));
			}

			if ("".equalsIgnoreCase(qc.getString(13))) {
				ad_property.addProperty("RoofDeckOtherText", "");
			} else {
				ad_property.addProperty("RoofDeckOtherText",
						cf.getsinglequotes(qc.getString(13)));
			}
			if ("".equalsIgnoreCase(qc.getString(14))) {
				ad_property.addProperty("RooftoWallValue", 0);
			} else {
				ad_property.addProperty("RooftoWallValue",
						qc.getString(14));
			}

			ad_property.addProperty("RooftoWallToeNailMinValue", 0);
			if ("".equalsIgnoreCase(qc.getString(16))) {
				ad_property.addProperty("RooftoWallClipsMinValue", 0);
			} else {
				ad_property.addProperty("RooftoWallClipsMinValue",
						qc.getString(16));
			}

			if ("".equalsIgnoreCase(qc.getString(17))) {
				ad_property.addProperty("RooftoWallSingleMinValue", 0);
			} else {
				ad_property.addProperty("RooftoWallSingleMinValue",
						qc.getString(17));
			}
			if ("".equalsIgnoreCase(qc.getString(18))) {
				ad_property.addProperty("RooftoWallDoubleMinValue", 0);
			} else {
				ad_property.addProperty("RooftoWallDoubleMinValue",
						qc.getString(18));
			}
			if ("".equalsIgnoreCase(qc.getString(15))) {
				ad_property.addProperty("RooftoWallSubValue", 0);
			} else {
				ad_property.addProperty("RooftoWallSubValue",
						qc.getString(15));
			}
			if ("".equalsIgnoreCase(qc.getString(19))) {
				ad_property.addProperty("RooftoWallOtherText", "");
			} else {
				ad_property.addProperty("RooftoWallOtherText",
						cf.getsinglequotes(qc.getString(19)));
			}

			if ("".equalsIgnoreCase(qc.getString(20))) {
				ad_property.addProperty("RoofGeoValue", 0);
			} else {
				ad_property.addProperty("RoofGeoValue",
						qc.getString(20));
			}

			if ("".equalsIgnoreCase(qc.getString(23))) {
				ad_property.addProperty("RoofGeoOtherText", "");
			} else {
				ad_property.addProperty("RoofGeoOtherText",
						cf.getsinglequotes(qc.getString(23)));
			}
			if ("".equalsIgnoreCase(qc.getString(21))) {
				ad_property.addProperty("RoofGeoLength", 0);
			} else {
				ad_property.addProperty("RoofGeoLength",
						qc.getString(21));
			}
			if ("".equalsIgnoreCase(qc.getString(22))) {
				ad_property.addProperty("RoofGeoTotalArea", 0);
			} else {
				ad_property.addProperty("RoofGeoTotalArea",
						qc.getString(22));
			}
			if ("".equalsIgnoreCase(qc.getString(24))) {
				ad_property.addProperty("SWRValue", 0);
			} else {
				ad_property.addProperty("SWRValue", qc.getString(24));
			}

			if ("".equalsIgnoreCase(qc.getString(25))) {
				ad_property.addProperty("OpenProtectType", "");
			} else {
				ad_property.addProperty("OpenProtectType",
						qc.getString(25));
			}
			if ("".equalsIgnoreCase(qc.getString(26))) {
				ad_property.addProperty("OpenProtectValue", 0);
			} else {
				ad_property.addProperty("OpenProtectValue",
						qc.getString(26));
			}
			if ("".equalsIgnoreCase(qc.getString(27))) {
				ad_property.addProperty("OpenProtectSubValue", 0);
			} else {
				ad_property.addProperty("OpenProtectSubValue",
						qc.getString(27));
			}
			if ("".equalsIgnoreCase(qc.getString(28))) {
				ad_property.addProperty("OpenProtectLevelChart", "");
			} else {
				ad_property.addProperty("OpenProtectLevelChart",
						qc.getString(28));
			}
			if ("".equalsIgnoreCase(qc.getString(29))) {
				ad_property.addProperty("GOWindEntryDoorsValue", "");
			} else {
				ad_property.addProperty("GOWindEntryDoorsValue",
						qc.getString(29));
			}
			if ("".equalsIgnoreCase(qc.getString(30))) {
				ad_property.addProperty("GOGarageDoorsValue", "");
			} else {
				ad_property.addProperty("GOGarageDoorsValue",
						qc.getString(30));
			}
			if ("".equalsIgnoreCase(qc.getString(31))) {
				ad_property.addProperty("GOSkylightsValue", "");
			} else {
				ad_property.addProperty("GOSkylightsValue",
						qc.getString(31));
			}
			if ("".equalsIgnoreCase(qc.getString(32))) {
				ad_property.addProperty("NGOGlassBlockValue", "");
			} else {
				ad_property.addProperty("NGOGlassBlockValue",
						qc.getString(32));
			}
			if ("".equalsIgnoreCase(qc.getString(33))) {
				ad_property.addProperty("NGOGarageDoorsValue", "");
			} else {
				ad_property.addProperty("NGOGarageDoorsValue",
						qc.getString(33));
			}
			if ("".equalsIgnoreCase(qc.getString(34))) {
				ad_property.addProperty("NGOEntryDoorsValue", "");
			} else {
				ad_property.addProperty("NGOEntryDoorsValue",
						qc.getString(34));
			}
			if ("".equalsIgnoreCase(qc.getString(35))) {
				ad_property.addProperty("WoodFrameValue", 0);
			} else {
				ad_property.addProperty("WoodFrameValue",
						qc.getString(35));
			}
			if ("".equalsIgnoreCase(qc.getString(36))) {
				ad_property.addProperty("WoodFramePer", 0);
			} else {
				ad_property.addProperty("WoodFramePer",
						qc.getInt(36) / 10);
			}
			if ("".equalsIgnoreCase(qc.getString(37))) {
				ad_property.addProperty("ReinMasonryValue", 0);
			} else {
				ad_property.addProperty("ReinMasonryValue",
						qc.getString(37));
			}
			if ("".equalsIgnoreCase(qc.getString(38))) {
				ad_property.addProperty("ReinMasonryPer", 0);
			} else {
				ad_property.addProperty("ReinMasonryPer",
						qc.getInt(38) / 10);
			}
			if ("".equalsIgnoreCase(qc.getString(39))) {
				ad_property.addProperty("UnReinMasonryValue", 0);
			} else {
				ad_property.addProperty("UnReinMasonryValue",
						qc.getString(39));
			}
			if ("".equalsIgnoreCase(qc.getString(40))) {
				ad_property.addProperty("UnReinMasonryPer", 0);
			} else {
				ad_property.addProperty("UnReinMasonryPer",
						qc.getInt(40) / 10);
			}

			if ("".equalsIgnoreCase(qc.getString(41))) {
				ad_property.addProperty("PouredConcrete", 0);
			} else {
				ad_property.addProperty("PouredConcrete",
						qc.getString(41));
			}
			if ("".equalsIgnoreCase(qc.getString(42))) {
				ad_property.addProperty("PouredConcretePer", 0);
			} else {
				ad_property.addProperty("PouredConcretePer",
						qc.getInt(42) / 10);
			}

			if ("".equalsIgnoreCase(qc.getString(43))) {
				ad_property.addProperty("OtherWallTitle", "");
			} else {
				ad_property.addProperty("OtherWallTitle",
						cf.getsinglequotes(qc.getString(43)));
			}
			if ("".equalsIgnoreCase(qc.getString(44))) {
				ad_property.addProperty("OtherWal", 0);
			} else {
				ad_property.addProperty("OtherWal", qc.getString(44));
			}
			if ("".equalsIgnoreCase(qc.getString(45))) {
				ad_property.addProperty("OtherWallPer", 0);
			} else {
				ad_property.addProperty("OtherWallPer",
						qc.getInt(45) / 10);
			}
			if ("".equalsIgnoreCase(insrendate)) {
				ad_property.addProperty("EffectiveDate", "");
			} else {
				ad_property.addProperty("EffectiveDate", insrendate);
			}
			if ("".equalsIgnoreCase(qc.getString(47))) {
				ad_property.addProperty("OverallComments", "");
			} else {
				ad_property.addProperty("OverallComments",
						cf.getsinglequotes(qc.getString(47)));
			}
			if ("".equalsIgnoreCase(qc.getString(48))) {
				ad_property.addProperty("Completed", 0);
			} else {
				ad_property.addProperty("Completed", qc.getString(48));
			}
			if ("".equalsIgnoreCase(qc.getString(49))) {
				ad_property.addProperty("ScheduleComments", "");
			} else {
				ad_property.addProperty("ScheduleComments",
						cf.getsinglequotes(qc.getString(49)));
			}
			if ("".equalsIgnoreCase(qc.getString(50))) {
				ad_property.addProperty("ModifyDate", "");
			} else {
				ad_property.addProperty("ModifyDate", qc.getString(50));
			}
			if ("".equalsIgnoreCase(qc.getString(51))) {
				ad_property.addProperty("GeneralHazardsInclude", 0);
			} else {
				ad_property.addProperty("GeneralHazardsInclude",
						qc.getString(51));
			}
			incstat = qc.getString(51);
			envelope.setOutputSoapObject(ad_property);System.out.println("ad_propquest="+ad_property);
			HttpTransportSE androidHttpTransport2 = new HttpTransportSE(
					cf.URL);
			androidHttpTransport2.call(cf.SOAP_ACTION22, envelope);
			String result11 = String.valueOf(envelope.getResponse());
			System.out.println("return resul "+result11);
			statuschange[0]=true;
			return cf.check_Status(result11);
			
			
			}
			else
			{
				
				return true;
			}
		}
		protected boolean quescomm() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
	
	String queschk;
	
	cf.Createtablefunction(8);
	Cursor c = cf.SelectTablefunction(cf.tbl_comments," where SRID='" + cf.selectedhomeid + "'");
	System.out.println("SRID"+c.getCount());
	if(c.getCount()>=1)
	{	
	c.moveToFirst();
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
			SoapEnvelope.VER11);
	envelope.dotNet = true;
	SoapObject ad_property = new SoapObject(cf.NAMESPACE,
			cf.METHOD_NAME16);
	ad_property.addProperty("InspectorID", cf.InspectorId);
	ad_property.addProperty("Srid", c.getString(1));
	
	
	System.out.println("ad_property "+ad_property);
	
	if ("".equalsIgnoreCase(c.getString(2))) {
		ad_property.addProperty("i_InspectionTypeID", 0);
	} else {
		ad_property.addProperty("i_InspectionTypeID",
				c.getString(2));
	}
	if ("".equalsIgnoreCase(c.getString(3))) {
		ad_property.addProperty("BuildingCodeComment", "");
	} else {
		cf.methodname="BuildingCodeComment";
		ad_property.addProperty("BuildingCodeComment",cf.getsinglequotes(c.getString(3)));
	}
	if ("".equalsIgnoreCase(c.getString(4))) {
		ad_property.addProperty("BuildingCodeAdminComment", "");
	} else {
		
		ad_property.addProperty("BuildingCodeAdminComment", "");
	}
	if ("".equalsIgnoreCase(c.getString(5))) {
		ad_property.addProperty("RoofCoverComment", "");
	} else {
		cf.methodname="RoofCoverComment";
		ad_property.addProperty("RoofCoverComment",cf.getsinglequotes(c.getString(5)));
		
	}
	if ("".equalsIgnoreCase(c.getString(6))) {
		ad_property.addProperty("RoofCoverAdminComment", "");
	} else {
		ad_property.addProperty("RoofCoverAdminComment", "");
	}
	if ("".equalsIgnoreCase(c.getString(7))) {
		ad_property.addProperty("RoofDeckComment", "");
	} else {
		cf.methodname="RoofDeckComment";
		ad_property.addProperty("RoofDeckComment",cf.getsinglequotes(c.getString(7)));
	}
	if ("".equalsIgnoreCase(c.getString(8))) {
		ad_property.addProperty("RoofDeckAdminComment", "");
	} else {
		ad_property.addProperty("RoofDeckAdminComment", "");
	}
	if ("".equalsIgnoreCase(c.getString(9))) {
		ad_property.addProperty("RoofWallComment", "");
	} else {
		cf.methodname="RoofWallComment";
		ad_property.addProperty("RoofWallComment",cf.getsinglequotes(c.getString(9)));
	}
	if ("".equalsIgnoreCase(c.getString(10))) {
		ad_property.addProperty("RoofWallAdminComment", "");
	} else {
		ad_property.addProperty("RoofWallAdminComment", "");
	}
	if ("".equalsIgnoreCase(c.getString(11))) {
		ad_property.addProperty("RoofGeometryComment", "");
	} else {
		cf.methodname="RoofGeometryComment";
		ad_property.addProperty("RoofGeometryComment",cf.getsinglequotes(c.getString(11)));
	}
	if ("".equalsIgnoreCase(c.getString(12))) {
		ad_property.addProperty("RoofGeometryAdminComment", "");
	} else {
		ad_property.addProperty("RoofGeometryAdminComment", "");
	}
	if ("".equalsIgnoreCase(c.getString(13))) {
		ad_property.addProperty("GableEndBracingComment", "");
	} else {
		ad_property.addProperty("GableEndBracingComment", "");
	}
	if ("".equalsIgnoreCase(c.getString(14))) {
		ad_property.addProperty("GableEndBracingAdminComment",
				"");
	} else {
		ad_property.addProperty("GableEndBracingAdminComment",
				"");
	}
	if ("".equalsIgnoreCase(c.getString(15))) {
		ad_property.addProperty("WallConstructionComment", "");
	} else {
		cf.methodname="WallConstructionComment";
		ad_property.addProperty("WallConstructionComment",cf.getsinglequotes(c.getString(15)));
	}
	if ("".equalsIgnoreCase(c.getString(16))) {
		ad_property.addProperty("WallConstructionAdminComment",
				"");
	} else {
		ad_property.addProperty("WallConstructionAdminComment",
				"");
	}
	if ("".equalsIgnoreCase(c.getString(17))) {
		ad_property.addProperty("SecondaryWaterComment", "");
	} else {
		cf.methodname="SecondaryWaterComment";
		ad_property.addProperty("SecondaryWaterComment",cf.getsinglequotes(c.getString(17)));
	}
	if ("".equalsIgnoreCase(c.getString(18))) {
		ad_property.addProperty("SecondaryWaterAdminComment",
				"");
	} else {
		ad_property.addProperty("SecondaryWaterAdminComment",
				"");
	}
	if ("".equalsIgnoreCase(c.getString(19))) {
		ad_property.addProperty("OpeningProtectionComment", "");
	} else {
		cf.methodname="OpeningProtectionComment";
		ad_property.addProperty("OpeningProtectionComment",cf.getsinglequotes(c.getString(19)));
	}
	if ("".equalsIgnoreCase(c.getString(20))) {
		ad_property.addProperty(
				"OpeningProtectionAdminComment", "");
	} else {
		ad_property.addProperty(
				"OpeningProtectionAdminComment", "");
	}
	if ("".equalsIgnoreCase(c.getString(21))) {
		ad_property.addProperty("InsOverAllComments", "");
	} else {
		cf.methodname="OverAllComments";
		ad_property.addProperty("InsOverAllComments",cf.getsinglequotes(c.getString(21)));
	}
	if ("".equalsIgnoreCase(c.getString(22))) {
		ad_property.addProperty("CreatedOn", "");
	} else {
		ad_property.addProperty("CreatedOn", c.getString(22));
	}
	System.out.println("ad_property comme "+ad_property);
	envelope.setOutputSoapObject(ad_property);
	HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
			cf.URL);
	
	androidHttpTransport1.call(cf.SOAP_ACTION16, envelope);
	
	String hazres = String.valueOf(envelope.getResponse());
	System.out.println("hazres "+hazres);
	c.close();
	statuschange[2] =true;
	
	return cf.check_Status(hazres);
	}else
	{
		return true;
	}
	
}
		private boolean updateroofcover() throws NetworkErrorException,
		IOException, XmlPullParserException,
		SocketTimeoutException, Exception {
			
	cf.methodname=cf.METHOD_NAME17;
	String roofchk;
	cf.Createtablefunction(12);
	Cursor c = cf.SelectTablefunction(cf.quesroofcover,
			"where SRID='" + cf.selectedhomeid + "'");
	System.out.println("updateroofcover="+c.getCount());
	if(c.getCount()>=1)
	{
	c.moveToFirst();
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
			SoapEnvelope.VER11);
	envelope.dotNet = true;
	SoapObject ad_property = new SoapObject(cf.NAMESPACE,
			cf.METHOD_NAME17);
	ad_property.addProperty("InspectorID", cf.InspectorId);
	ad_property.addProperty("SRID", c.getString(1));

	if ("".equalsIgnoreCase(c.getString(2))) {
		ad_property.addProperty("RoofCoverType", "");
	} else {
		ad_property
				.addProperty("RoofCoverType", cf.getsinglequotes(c.getString(2)));
	}

	if ("".equalsIgnoreCase(c.getString(4))) {
		ad_property.addProperty("PermitApplnDate1", "");
	} else {
		ad_property.addProperty("PermitApplnDate1",
				cf.getsinglequotes(c.getString(4)));
	}
	if ("".equalsIgnoreCase(c.getString(5))) {
		ad_property.addProperty("PermitApplnDate2", "");
	} else {
		ad_property.addProperty("PermitApplnDate2",
				cf.getsinglequotes(c.getString(5)));
	}
	if ("".equalsIgnoreCase(c.getString(6))) {
		ad_property.addProperty("PermitApplnDate3", "");
	} else {
		ad_property.addProperty("PermitApplnDate3",
				cf.getsinglequotes(c.getString(6)));
	}
	if ("".equalsIgnoreCase(c.getString(7))) {
		ad_property.addProperty("PermitApplnDate4", "");
	} else {
		ad_property.addProperty("PermitApplnDate4",
				cf.getsinglequotes(c.getString(7)));
	}
	if ("".equalsIgnoreCase(c.getString(8))) {
		ad_property.addProperty("PermitApplnDate5", "");
	} else {
		ad_property.addProperty("PermitApplnDate5",
				cf.getsinglequotes(c.getString(8)));
	}
	if ("".equalsIgnoreCase(c.getString(9))) {
		ad_property.addProperty("PermitApplnDate6", "");
	} else {
		ad_property.addProperty("PermitApplnDate6",
				cf.getsinglequotes(c.getString(9)));
	}

	if ("".equalsIgnoreCase(c.getString(10))) {
		ad_property.addProperty("RoofCoverTypeOther", "");
	} else {
		ad_property.addProperty("RoofCoverTypeOther",
				cf.getsinglequotes(c.getString(10)));
	}
	if ("".equalsIgnoreCase(c.getString(11))) {
		ad_property.addProperty("ProdApproval1", "");
	} else {
		ad_property.addProperty("ProdApproval1",
				cf.getsinglequotes(c.getString(11)));
	}
	if ("".equalsIgnoreCase(c.getString(12))) {
		ad_property.addProperty("ProdApproval2", "");
	} else {
		ad_property.addProperty("ProdApproval2",
				cf.getsinglequotes(c.getString(12)));
	}
	if ("".equalsIgnoreCase(c.getString(13))) {
		ad_property.addProperty("ProdApproval3", "");
	} else {
		ad_property.addProperty("ProdApproval3",
				cf.getsinglequotes(c.getString(13)));
	}
	if ("".equalsIgnoreCase(c.getString(14))) {
		ad_property.addProperty("ProdApproval4", "");
	} else {
		ad_property.addProperty("ProdApproval4",
				cf.getsinglequotes(c.getString(14)));
	}
	if ("".equalsIgnoreCase(c.getString(15))) {
		ad_property.addProperty("ProdApproval5", "");
	} else {
		ad_property.addProperty("ProdApproval5",
				cf.getsinglequotes(c.getString(15)));
	}
	if ("".equalsIgnoreCase(c.getString(16))) {
		ad_property.addProperty("ProdApproval6", "");
	} else {
		ad_property.addProperty("ProdApproval6",
				cf.getsinglequotes(c.getString(16)));
	}

	if ("".equalsIgnoreCase(c.getString(17))) {
		ad_property.addProperty("InstallYear1", "");
	} else {
		ad_property
				.addProperty("InstallYear1", c.getString(17));
	}
	if ("".equalsIgnoreCase(c.getString(18))) {
		ad_property.addProperty("InstallYear2", "");
	} else {
		ad_property
				.addProperty("InstallYear2", c.getString(18));
	}
	if ("".equalsIgnoreCase(c.getString(19))) {
		ad_property.addProperty("InstallYear3", "");
	} else {
		ad_property
				.addProperty("InstallYear3", c.getString(19));
	}
	if ("".equalsIgnoreCase(c.getString(20))) {
		ad_property.addProperty("InstallYear4", "");
	} else {
		ad_property
				.addProperty("InstallYear4", c.getString(20));
	}
	if ("".equalsIgnoreCase(c.getString(21))) {
		ad_property.addProperty("InstallYear5", "");
	} else {
		ad_property
				.addProperty("InstallYear5", c.getString(21));
	}
	if ("".equalsIgnoreCase(c.getString(22))) {
		ad_property.addProperty("InstallYear6", "");
	} else {
		ad_property
				.addProperty("InstallYear6", c.getString(22));
	}

	if ("".equalsIgnoreCase(c.getString(23))) {
		ad_property.addProperty("NoInfo1", 0);
	} else {
		ad_property.addProperty("NoInfo1", c.getString(23));
	}
	if ("".equalsIgnoreCase(c.getString(24))) {
		ad_property.addProperty("NoInfo2", 0);
	} else {
		ad_property.addProperty("NoInfo2", c.getString(24));
	}

	if ("".equalsIgnoreCase(c.getString(25))) {
		ad_property.addProperty("NoInfo3", 0);
	} else {
		ad_property.addProperty("NoInfo3", c.getString(25));
	}

	if ("".equalsIgnoreCase(c.getString(26))) {
		ad_property.addProperty("NoInfo4", 0);
	} else {
		ad_property.addProperty("NoInfo4", c.getString(26));
	}
	if ("".equalsIgnoreCase(c.getString(27))) {
		ad_property.addProperty("NoInfo5", 0);
	} else {
		ad_property.addProperty("NoInfo5", c.getString(27));
	}
	if ("".equalsIgnoreCase(c.getString(28))) {
		ad_property.addProperty("NoInfo6", 0);
	} else {
		ad_property.addProperty("NoInfo6", c.getString(28));
	}
	if ("".equalsIgnoreCase(c.getString(29))) {
		ad_property.addProperty("RoofPreDominant", 0);
	} else {
		ad_property.addProperty("RoofPreDominant",
				c.getString(29));
	}
System.out.println("aD_PRP "+ad_property);
	envelope.setOutputSoapObject(ad_property);
	HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
			cf.URL);
	androidHttpTransport1.call(cf.SOAP_ACTION17, envelope);
	String roofres = String.valueOf(envelope.getResponse());
	System.out.println("roofres "+roofres);
	statuschange[1]=true;
	return cf.check_Status(roofres);
	
	}
	else
	{
		System.out.println("elese");
		statuschange[1]=true;
		return true;
	}
}
	
		protected boolean Status_change() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
			// TODO Auto-generated method stub
					SoapObject request = new SoapObject(cf.NAMESPACE,"UpdateInspectionStatus");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("InspectorID",cf.InspectorId);
						request.addProperty("SRID",cf.selectedhomeid);
						envelope.setOutputSoapObject(request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
						androidHttpTransport.call(cf.NAMESPACE+"UpdateInspectionStatus",envelope);
						String result =  envelope.getResponse().toString();
						System.out.println(" check_Status "+result);
						try
						{
							 cf.onlresult=cf.Calling_WS1(cf.InspectorId, "UpdateMobileDBCount");
							 retrieveonlinedata(cf.onlresult);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						if(cf.check_Status(result))
						{
							status="This inspection was uploaded completely to you online profile under \"pre-inspected\" status. \nPlease log into  www.paperlessinspectors.com and audit and submit this file for QA.";
							cf.db.execSQL("UPDATE "
									+ cf.mbtblInspectionList
									+ " SET IsInspected=2,IsUploaded=1,SubStatus=41 WHERE s_SRID ='"
									+ cf.selectedhomeid
									+ "'");				
							return true;
						}
						else 
						{
							if(complete_enable)
							{
								status="Exporting inspection completed. Inspection in Pre-Inspected Status";
							}
							else
							{
								status="Exporting inspection completed. Inspection in scheduled Status";	
							}
							
							return false;
						}
		}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.exporthome:
			Intent homepage = new Intent(Exportedddata.this, HomeScreen.class);
			startActivity(homepage);
			break;
	
		case R.id.cmpe:
			Intent exportpage = new Intent(Exportedddata.this,Exportedddata.class);
			exportpage.putExtra("completedexport", "false");
			startActivity(exportpage);
			break;
		case R.id.export_list:
			Intent exportpage1 = new Intent(Exportedddata.this,Exportedddata.class); 
			exportpage1.putExtra("completedexport", "true");
			startActivity(exportpage1);
			break;
		case R.id.audit:
			Intent move = new Intent(Exportedddata.this, Exportedddata.class);
			move.putExtra("completedexport", "QA");
			startActivity(move);
			break;
		}
	}
	private void showerrorinspreallocate() {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
            public void run() 
            {
            	finishedHandler.sendEmptyMessage(0);
            }
        }).start();
	}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	    			alertDialog = new AlertDialog.Builder(Exportedddata.this).create();
	    			alertDialog.setMessage("This Inspection is no longer Assigned to you Please Cease all Further Work.");
	    			alertDialog.setButton("OK",	new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) 
					{
						alertDialog.dismiss();						
					}
				});
				alertDialog.show();
	    }
	};
	protected boolean Upload_Photos() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub
	/** set the value get from the policy holder table starts **/
		tochkfileexists[0] = false;String elevname = "";
	cf.Createtablefunction(13);
	Cursor c = cf.SelectTablefunction(cf.ImageTable,
			"where SrID='" + cf.selectedhomeid
					+ "' and Delflag=0 and Elevation in(1,2,3,4,5,6,7,8)");
	
		if(c.getCount()>=1)
		{
			int countchk = c.getCount();
			c.moveToFirst();
			double Totaltmp = 0.0;
			double temp_inc=0.0;
			double Temp_total=total;
			Totaltmp = ((double) increment_unit / (double) countchk);
			int error=0;
			for (int j = 0; j < c.getCount(); j++) {
				Exportbartext="Uploading elevation pictures   "+(j+1)+" / "+countchk;
				String mypath = cf.getsinglequotes(c.getString(c
						.getColumnIndex("ImageName")));
				String elevationname = cf.getsinglequotes(c.getString(c
						.getColumnIndex("Elevation")));
				System.out.println("elevationname "+elevationname);
				
				System.out.println("mypath "+mypath+" J = "+j);
				String temppath[] = mypath.split("/");
				int ss = temppath[temppath.length - 1].lastIndexOf(".");
				String tests = temppath[temppath.length - 1].substring(ss);
				String elevationttype;
				
				
				File f = new File(mypath);
				if (f.exists()) {
					
					
					if (tests.equals("") || tests.equals("null")
							|| tests == null) {
						if (c.getString(3).toString().equals("1")) {
							erromsg = "FrontElevation Tab in the image order" + c.getString(9).toString() ;

						} else if (c.getString(3).toString().equals("2")) {
							erromsg = "RightElevation Tab in the image order" + c.getString(9).toString() ;

						} else if (c.getString(3).toString().equals("3")) {
							erromsg = "BackElevation Tab in the image order" + c.getString(9).toString() ;

						} else if (c.getString(3).toString().equals("4")) {
							erromsg = "LeftElevation Tab in the image order" + c.getString(9).toString() ;

						} else if (c.getString(3).toString().equals("5")) {
							erromsg = "AtticElevation Tab in the image order" + c.getString(9).toString() ;

						} else if (c.getString(3).toString().equals("6")) {
							erromsg = "Homeownersignature in the image order"+ c.getString(9).toString();

						} else if (c.getString(3).toString().equals("7")) {
							erromsg = "Paperworksignature in the image order"+ c.getString(9).toString();

						} else if (c.getString(3).toString().equals("8")) {
							erromsg = "AdditionalElevation in the image order"+ c.getString(9).toString();
							
						} else {
							erromsg = "AdditionalElevation in the image order"+ c.getString(9).toString();
						
						}

						Error_traker("Problem uploading image in "+erromsg);// calling error msg track
						//return false;
					} 
					else
					{
								if (c.getString(3).toString().equals("1")) {
									elevationttype = "FrontElevation"
											+ c.getString(9).toString();
								} else if (c.getString(3).toString().equals("2")) {
									elevationttype = "RightElevation"
											+ c.getString(9).toString();
								} else if (c.getString(3).toString().equals("3")) {
									elevationttype = "BackElevation"
											+ c.getString(9).toString();
								} else if (c.getString(3).toString().equals("4")) {
									elevationttype = "LeftElevation"
											+ c.getString(9).toString();
								} else if (c.getString(3).toString().equals("5")) {
									elevationttype = "AtticElevation"
											+ c.getString(9).toString();
								} else if (c.getString(3).toString().equals("6")) {
									elevationttype = "Homeownersignature"
											+ c.getString(9).toString();
								} else if (c.getString(3).toString().equals("7")) {
									elevationttype = "Paperworksignature"
											+ c.getString(9).toString();
								} else if (c.getString(3).toString().equals("8")) {
		
									elevationttype = "AdditionalElevation"
											+ c.getString(9).toString();
									
								} else {
		
									elevationttype = "AdditionalElevation"
											+ c.getString(9).toString();
							
								}
								elevationttype += tests;
								
								
								bitmap = ShrinkBitmap(cf.getsinglequotes(c.getString(c
										.getColumnIndex("ImageName"))), 400, 400);

								System.out.println(" bimap "+bitmap);
								MarshalBase64 marshal = new MarshalBase64();
								ByteArrayOutputStream out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								byte[] raw = out.toByteArray();
								SoapObject request = new SoapObject(cf.NAMESPACE,
										cf.METHOD_NAME14);
								request.addProperty("InspectorID", cf.InspectorId);
								request.addProperty("SRID", c.getString(1));
								request.addProperty("Ques_Id", 0);
								request.addProperty("Elevation", c.getString(3));
								request.addProperty("Description",
										cf.getsinglequotes(c.getString(6)));
								request.addProperty("CreatedOn", c.getString(7));
								request.addProperty("ModifiedOn", c.getString(8));
								request.addProperty("ImageOrder", c.getString(9));
								request.addProperty("ImageNameWithExtension",
										elevationttype);
								request.addProperty("imgByte", raw);
								System.out.println("request "+request);
								
								SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
										SoapEnvelope.VER11);
								envelope.dotNet = true;
								HttpTransportSE httpTransport = null;
								try
								{
									envelope.setOutputSoapObject(request);
									marshal.register(envelope);
									httpTransport = new HttpTransportSE(
											cf.URL);
									
									httpTransport.call(cf.SOAP_ACTION14, envelope);
									String response = envelope.getResponse().toString();
									System.out.println("response photos= "+response);
									if(response.trim().equals("true"))
									{
										statuschange[3] =true;
									}
									c.moveToNext();
								}
								catch (IOException e) {
									// TODO: handle exception
								}

								try {
									out.close();
								} catch (IOException e) {

								}
						
						
						
					}
				}
				else
				{
					System.out.println(" filpe does not exists" +tests+f);
					if (tests.equals("") || tests.equals("null")
							|| tests == null) {
						System.out.println("test");
						if (c.getString(3).toString().equals("1")) {
							erromsg = "FrontElevation Tab in the image order"
									+ c.getString(9).toString() ;
						} else if (c.getString(3).toString().equals("2")) {
							erromsg = "RightElevation   Tab in the image order"
									+ c.getString(9).toString();
						} else if (c.getString(3).toString().equals("3")) {
							erromsg = "BackElevation  Tab in the image order"
									+ c.getString(9).toString();
						} else if (c.getString(3).toString().equals("4")) {
							erromsg = "LeftElevation   Tab in the image order"
									+ c.getString(9).toString();
						} else if (c.getString(3).toString().equals("5")) {
							erromsg = "AtticElevation   Tab in the image order"
									+ c.getString(9).toString();
						} else if (c.getString(3).toString().equals("6")) {
							erromsg = "Homeownersignature  "
									+ c.getString(9).toString();
						} else if (c.getString(3).toString().equals("7")) {
							erromsg = "Paperworksignature"
									+ c.getString(9).toString();
						} else if (c.getString(3).toString().equals("8")) {
							erromsg = "AdditionalElevation in the image order"+ c.getString(9).toString();
							
						} else {
							erromsg = "AdditionalElevation in the image order"+ c.getString(9).toString();
						}
						
						Error_traker("Please check the image in "+erromsg +" available " );
				}
					else
					{
						
					if(elevationname.equals("1")){elevname = "Front Elevation";}
						else if(elevationname.equals("2")){elevname = "Right Elevation";}
						else if(elevationname.equals("3")){elevname = "Back Elevation";}
						else if(elevationname.equals("4")){elevname = "Left Elevation";}
						else if(elevationname.equals("5")){elevname = "Attic Elevation";}
						else if(elevationname.equals("6")){elevname = "Home Owner Signature";}
						else if(elevationname.equals("8")){elevname = "Additional Elevation";}	
						else if(elevationname.equals("7")){elevname = "Paperwork Signature";}
						tochkfileexists[0]= true;
						Error_traker("Please check the image (" + mypath + ") in "+elevname+" tab is available in your external storage." );
						
					}
					
					// check the file is exist in the device Ends //
					c.moveToNext();	
				}
				
				if (total <= Temp_total+increment_unit) {
					temp_inc +=Totaltmp;
					total = Temp_total + (int) (temp_inc);
					     
				} else {
					total = Temp_total+increment_unit;
				}
			}
			
			//statuschange[3] =true;
			return true;
		}
		else
		{
			return true;
		}
	}
	protected boolean Uploading_feedback() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
		// TODO Auto-generated method stub

		Exportbartext="Uploading Feedback Information.";
		System.out.println("Uploading_feedback");
		tochkfileexists[1]=false;
			if(feedbackinfo())
			{
				System.out.println("feedbackinfo is t");
				cf.methodname=cf.METHOD_NAME20;
				cf.Createtablefunction(11);

				Bitmap bitmap;
			 /** Uploading Feed back document Starts here   **/
				
				
				Cursor c = cf.SelectTablefunction(cf.FeedBackDocumentTable,
						"where SrID='" + cf.selectedhomeid + "'");

				System.out.println("cf.c.getCount = "+c.getCount());
				if(c.getCount()>=1)
				{
					MarshalBase64 marshal = new MarshalBase64();
					ByteArrayOutputStream out = null;				
					c.moveToFirst();
					
					int countchk = c.getCount();
					double Totaltmp = 0.0;
					double Temp_total=total;double temp_inc=0.0;
					Totaltmp = ((double) increment_unit / (double) countchk);
					int error=0;
					for (int j = 0; j < c.getCount(); j++) 
					{
						Exportbartext="Uploading Feedback documents   "+(j+1)+" / "+countchk;
						String substring = cf.getsinglequotes(c.getString(c
								.getColumnIndex("FileName")));
						SoapObject request = new SoapObject(cf.NAMESPACE,
								cf.METHOD_NAME20);
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("InspectorID", cf.InspectorId);
						request.addProperty("SRID", c.getString(1));
						request.addProperty("InspectionTypeId", c.getString(2));
						request.addProperty("DocumentTitle", cf.getsinglequotes(c.getString(3)));
						request.addProperty("IsOfficeUse", c.getString(7));
						request.addProperty("CreatedDate", c.getString(8));
						request.addProperty("ModifiedDate", c.getString(9));
						request.addProperty("ImageOrder", c.getInt(6));

						if (substring.endsWith(".pdf")) 
						{
							File dir = Environment.getExternalStorageDirectory();
							if (substring.startsWith("file:///")) 
							{
								substring = substring.substring(11);
							}
							else
							{
								substring = substring;
							}
							File assist = new File(substring);
							if (assist.exists()) 
							{
								System.out.println("file exists "+assist);
								String mypath = substring;
								String temppath[] = mypath.split("/");
								int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
								String tests = temppath[temppath.length - 1].substring(ss);
								String namedocument;
								if (tests.equals(".pdf")) 
								{
									namedocument = "pdf_document" + j + ".pdf";
									request.addProperty("ImageNameWithExtension",namedocument);
									try
									{
										InputStream fis = new FileInputStream(assist);
										long length = assist.length();
										byte[] bytes = new byte[(int) length];
										int offset = 0;
										int numRead = 0;
										while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
										{
											offset += numRead;
										}
										strBase64 = Base64.encode(bytes);										
										request.addProperty("imgByte", strBase64);										
										try
										{
										envelope.setOutputSoapObject(request);
										HttpTransportSE httpTransport = new HttpTransportSE(
												cf.URL);
										HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
										httpTransport.call(cf.SOAP_ACTION20, envelope);
										String result =  envelope.getResponse().toString();
										System.out.println("Result is "+j+result);
										boolean status = cf.check_Status(result);
										if(status==true)
										{
											statuschange[4]=true;
										}
										
										//error=0;
										}
										catch (IOException e) {
											// TODO: handle exception											
										}
										
									}
									catch (Exception e) 
									{ 
										Error_traker("You have problem in uploading documentin the feedback document tab Document Title"+cf.getsinglequotes(c.getString(2)) );
										/*cf.errorlogmanage(" Problem while opening the file ="+ e.getMessage()+ " path of the file is ="+ substring,selectedItem.toString());
										return "false";*/
									}
								}
								else
								{
									System.out.println("issues in extention missing");
									Error_traker("You have problem in uploading documentin the feedback document tab Document Title"+cf.getsinglequotes(c.getString(2)) );
									
								}

							}
							else
							{
								System.out.println("issues in error file not available"+assist);
								tochkfileexists[1]= true;
								Error_traker("Please check the pdf (" + assist + ") in feedback tab is available in your external storage." );

								
								
								//Error_traker("You have problem in uploading document in the feedback document tab Document Title"+cf.getsinglequotes(c.getString(2)) );
							}
						} 
						else
						{
							String mypath = substring;
							String temppath[] = mypath.split("/");
							int ss = temppath[temppath.length - 1].lastIndexOf(".");// substring(".");
							String tests = temppath[temppath.length - 1].substring(ss);
							System.out.println("tests feed "+tests);
							String elevationttype;
							File f = new File(substring);
							if (f.exists()) 
							{
								/**check weather the extention is not null**/
								if (tests.equals("") || tests.equals("null")|| tests == null) 
								{
									Error_traker("You have problem in uploading documentin the feed back document tab Document Title"+cf.getsinglequotes(c.getString(2)) );
									System.out.println("error file not extention not found");
									
								}
								else
								{
									elevationttype = "feedbackimage" + j + tests;
									request.addProperty("ImageNameWithExtension",
											elevationttype);
									bitmap = ShrinkBitmap(substring, 800, 800);
									
									out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 100, out);
									byte[] raw = out.toByteArray();
									System.out.println("request"+request.toString());
									request.addProperty("imgByte", raw);
									marshal.register(envelope);
								    // System.out.println("request"+request.toString());
									try
									{
									envelope.setOutputSoapObject(request);
									HttpTransportSE httpTransport = new HttpTransportSE(
											cf.URL);
									HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
									httpTransport.call(cf.SOAP_ACTION20, envelope);

									String result =  envelope.getResponse().toString();
									System.out.println("Result is "+j+result);
									cf.check_Status(result);
									boolean status = cf.check_Status(result);
									System.out.println("ststau check "+status);
									if(status==true)
									{
										statuschange[4]=true;
									}
									//error=0;
									}
									catch (IOException e) {
										// TODO: handle exception
										
									}
									try 
									{
										out.close();
									}
									catch (IOException e) 
									{
									}
								}

								
							}
							else
							{
								System.out.println("error file not exist");
								tochkfileexists[1]= true;
								Error_traker("Please check the document (" + mypath + ") in feedback tab is available in your external storage." );

								//Error_traker("You have problem in uploading documentin the feedback document tab Document Title"+cf.getsinglequotes(c.getString(2)) );
							}
							
						}						
						
						c.moveToNext();
						if (total <= Temp_total+increment_unit) {
							temp_inc +=Totaltmp;
							total = Temp_total + (int) (temp_inc);
							 
						} else {
							total = Temp_total+increment_unit;
						}
					}
					
			}
			else
			{
				System.out.println("feed true");
				return true;		
			}
			/** Uploading Feed back document Ends here   **/
			
			}
			
	return true;
	}
	
	
	private boolean feedbackinfo() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
					cf.methodname=cf.METHOD_NAME21;
					String fdinfochk;
					cf.Createtablefunction(10);
					
					Cursor c = cf.SelectTablefunction(cf.FeedBackInfoTable,
							"where Srid='" + cf.selectedhomeid + "'");
					System.out.println("feedback "+c.getCount());
					if(c.getCount()>=1)
					{
					c.moveToFirst();
					
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					envelope.dotNet = true;
					SoapObject ad_property = new SoapObject(cf.NAMESPACE,
							cf.METHOD_NAME21);
					ad_property.addProperty("InspectorID", cf.InspectorId);
					ad_property.addProperty("SRID", c.getString(1));
					ad_property.addProperty("Weather", "");
					ad_property.addProperty("Temperature", "");
					ad_property.addProperty("IsHOSatisfied", 0);
					ad_property.addProperty("IsCusServiceCompltd",
							c.getString(2));
					ad_property.addProperty("IsInspectionPaperAvbl",
							c.getString(4));
					ad_property.addProperty("IsManufacturerInfo",
							c.getString(5));
					ad_property.addProperty("PresentatInspection",
							c.getString(3));
					ad_property.addProperty("FeedbackComments",
							cf.getsinglequotes(c.getString(6)));
					ad_property.addProperty("Addendum", "");
					ad_property.addProperty("OfficeComments", "");
					ad_property.addProperty("CreatedOn", c.getString(7));
					ad_property.addProperty("OtherPresent",
							cf.getsinglequotes(c.getString(8)));
					envelope.setOutputSoapObject(ad_property);
					HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
							cf.URL);
					androidHttpTransport1.call(cf.SOAP_ACTION21, envelope);
					String result1 = String.valueOf(envelope.getResponse());
					c.close();
					return cf.check_Status(result1);
					}
					return true;
					
					}
						
		private boolean generalhazarddata() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
						
							cf.methodname=cf.METHOD_NAME19;
							String hzdchk;
							cf.Createtablefunction(15);
							Cursor c = cf.SelectTablefunction(cf.HazardDataTable,
									"where Srid='" + cf.selectedhomeid + "'");
							if(c.getCount()>=1)
							{

							c.moveToFirst();
						    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope.dotNet = true;
							SoapObject ad_property = new SoapObject(cf.NAMESPACE,
									cf.METHOD_NAME19);
							ad_property.addProperty("InspectorID", cf.InspectorId);
							ad_property.addProperty("Srid", c.getString(1));
							if ("".equalsIgnoreCase(c.getString(2))) {
								ad_property.addProperty("InsuredIs", "");
							} else {
								ad_property.addProperty("InsuredIs", c.getString(2));
							}
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("InsuredOther")))) {
								ad_property.addProperty("OtherInsured", "");
							} else {
								ad_property.addProperty("OtherInsured",  cf.getsinglequotes(c.getString(c.getColumnIndex("InsuredOther"))));
							}
							
							
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("OccupancyType")))) {
								ad_property.addProperty("OccupancyType", "");
							} else {
								ad_property
										.addProperty("OccupancyType", c.getString(c.getColumnIndex("OccupancyType")));
							}
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("OcccupOther")))) {
								ad_property.addProperty("OtherOccupancy", "");
							} else {
								ad_property.addProperty("OtherOccupancy",  cf.getsinglequotes(c.getString(c.getColumnIndex("OcccupOther"))));
							}
							
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("ObservationType")))) {
								ad_property.addProperty("ObservationType", "");
							} else {
								ad_property.addProperty("ObservationType",
										c.getString(c.getColumnIndex("ObservationType")));
							}
							
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("ObservOther")))) {
								ad_property.addProperty("OtherObservation", "");
							} else {
								ad_property.addProperty("OtherObservation", cf.getsinglequotes(c.getString(c.getColumnIndex("ObservOther"))));
							}
							
							
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("OccupiedFlag")))) {
								ad_property.addProperty("Occupied", 0);
							} else {
								ad_property.addProperty("Occupied", c.getString(c.getColumnIndex("OccupiedFlag")));
							}
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("OccupiedPercent")))) {
								ad_property.addProperty("OccupiedPercent", "");
							} else {
								ad_property.addProperty("OccupiedPercent",c.getString(c.getColumnIndex("OccupiedPercent")));
							}
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("Vacant")))) {
								ad_property.addProperty("Vacant", 0);
							} else {
								ad_property.addProperty("Vacant", c.getString(c.getColumnIndex("Vacant")));
							}
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("VacantPercent")))) {
								ad_property.addProperty("VacantPercent", "");
							} else {
								ad_property
										.addProperty("VacantPercent",c.getString(c.getColumnIndex("VacantPercent")));
							}
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("NotDetermined")))) {
								ad_property.addProperty("NotDetermined", 0);
							} else {
								ad_property
										.addProperty("NotDetermined", c.getString(c.getColumnIndex("NotDetermined")));
							}
							if ("".equalsIgnoreCase(c.getString(c.getColumnIndex("PermitConfirmed")))) {
								ad_property.addProperty("PermitConfirmed", "");
							} else {
								ad_property.addProperty("PermitConfirmed",c.getString(c.getColumnIndex("PermitConfirmed")));
							}
							if ("".equalsIgnoreCase(c.getString(14))) {
								ad_property.addProperty("BuildingSize", 0);
							} else {
								ad_property
										.addProperty("BuildingSize", c.getString(14));
							}
							if ("".equalsIgnoreCase(c.getString(15))) {
								ad_property.addProperty("BalconyPresent", "");
							} else {
								ad_property.addProperty("BalconyPresent",
										c.getString(15));
							}
							if ("".equalsIgnoreCase(c.getString(16))) {
								ad_property.addProperty("AdditionalStructure", "");
							} else {
								ad_property.addProperty("AdditionalStructure",
										c.getString(16));
							}
							if ("".equalsIgnoreCase(c.getString(17))) {
								ad_property.addProperty("OtherLocation", "");
							} else {
								ad_property.addProperty("OtherLocation",
										cf.getsinglequotes(c.getString(17)));
							}
							ad_property.addProperty("Location1", c.getString(18));
							ad_property.addProperty("Location2", c.getString(19));
							ad_property.addProperty("Location3", c.getString(20));
							ad_property.addProperty("Location4", c.getString(21));
							ad_property.addProperty("Observation1", c.getString(22));
							ad_property.addProperty("Observation2", c.getString(23));
							ad_property.addProperty("Observation3", c.getString(24));
							ad_property.addProperty("Observation4", c.getString(25));
							ad_property.addProperty("Observation5", c.getString(26));
							ad_property.addProperty("PerimeterPoolFence",
									c.getString(27));
							ad_property.addProperty("PoolFenceDisrepair",
									c.getString(28));
							ad_property.addProperty("SelfLatch", c.getString(29));
							ad_property.addProperty("ProfesInstall", c.getString(30));
							ad_property.addProperty("PoolPresent", c.getString(31));
							ad_property.addProperty("HotTubPresnt", c.getString(32));
							ad_property.addProperty("EmptyGroundPool", c.getString(33));
							ad_property.addProperty("PoolSlide", c.getString(34));
							ad_property.addProperty("DivingBoard", c.getString(35));
							ad_property.addProperty("PerimeterPoolEncl",
									c.getString(36));
							ad_property.addProperty("PoolEnclosure", c.getString(37));
							ad_property.addProperty("Vicious", c.getString(38));
							ad_property.addProperty("LiveStock", c.getString(39));
							ad_property.addProperty("OverHanging", c.getString(40));
							ad_property.addProperty("Trampoline", c.getString(41));
							ad_property.addProperty("SkateBoard", c.getString(42));
							ad_property.addProperty("bicycle", c.getString(43));
							ad_property.addProperty("TripHazardDesc",
									cf.getsinglequotes(c.getString(44)));
							ad_property.addProperty("TripHazardNoted", c.getString(45));
							ad_property.addProperty("UnsafeStairway", c.getString(46));
							ad_property.addProperty("PorchAndDeck", c.getString(47));
							ad_property.addProperty("NonStdConstruction",
									c.getString(48));
							ad_property.addProperty("OutDoorAppliances",
									c.getString(49));
							ad_property.addProperty("OpenFoundation", c.getString(50));
							ad_property.addProperty("WoodShingled", c.getString(51));
							ad_property.addProperty("ExcessDebris", c.getString(52));
							ad_property
									.addProperty("BusinessPremises", c.getString(53));
							ad_property
									.addProperty("GeneralDisrepair", c.getString(54));
							ad_property.addProperty("PropertyDamage", c.getString(55));
							ad_property
									.addProperty("StructurePartial", c.getString(56));
							ad_property.addProperty("InOperative", c.getString(57));
							ad_property.addProperty("RecentDrywall", c.getString(58));
							ad_property.addProperty("ChineseDrywall", c.getString(59));
							ad_property.addProperty("ConfirmDrywall", c.getString(60));
							ad_property.addProperty("NonSecurity", c.getString(61));
							ad_property.addProperty("NonSmoke", c.getString(62));
							ad_property.addProperty("Comments",
									cf.getsinglequotes(c.getString(63)));
							envelope.setOutputSoapObject(ad_property);System.out.println("ad_property "+ad_property);	
							HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL);System.out.println("url ");	

							androidHttpTransport1.call(cf.SOAP_ACTION19, envelope);			System.out.println("envelope ");					
							String hazdatares = String.valueOf(envelope.getResponse());	System.out.println("result genhazard "+hazdatares);						
							c.close();
							return cf.check_Status(hazdatares);

						}
							return true;
							}
						
		protected boolean Uploading_generalinformation() throws IOException, XmlPullParserException,NetworkErrorException,SocketTimeoutException {
							
							// TODO Auto-generated method stub
							Exportbartext="Uploading General Data";							
							if(generalhazarddata()) 
							{
								Cursor c = cf.SelectTablefunction(cf.HazardImageTable,
										"where SrID='" + cf.selectedhomeid
												+ "' and Delflag=0 and Elevation in(1,2,3)");
								int cntrws = c.getCount();
								System.out.println("generalhazarddata "+c.getCount());
								if(cntrws>=1)
								{
									c.moveToFirst();
									double Totaltmp = 0.0;
									double temp_inc=0.0;
									double Temp_total=total;
									Totaltmp = ((double) increment_unit / (double) cntrws);
									int error=0;
									for (int j = 0; j < cntrws; j++) {
										Exportbartext="Uploading General Images   "+(j+1)+" / "+cntrws;
										String mypath = cf.getsinglequotes(c.getString(c
												.getColumnIndex("ImageName"))); // get the full path of the image 
										String elevationnamegh =  c.getString(c.getColumnIndex("Elevation"));
										
										String temppath[] = mypath.split("/");
										int ss = temppath[temppath.length - 1].lastIndexOf(".");
										String tests = temppath[temppath.length - 1].substring(ss);
										String elevationttype;
										File f = new File(mypath);
										if (f.exists()) {
													if (tests.equals("") || tests.equals("null") || tests == null) {
														String elevationttype5;
														if (c.getString(3).toString().equals("1")) {
															elevationttype5 = "Exterior Image oreder  "+ c.getString(9).toString();
														} else if (c.getString(3).toString().equals("2")) {
															elevationttype5 = "Interior Image oreder  "+ c.getString(9).toString();
														} else {
															elevationttype5 = "Aditional  Image oreder  "+ c.getString(9).toString();
														}	
														
														Error_traker("problem in uploading general images in "+elevationttype5);// calling error msg track
													}
													else
													{
														if (c.getString(3).toString().equals("1")) {
															elevationttype = "exteiror"
																	+ c.getString(9).toString();
														} else if (c.getString(3).toString().equals("2")) {
															elevationttype = "interior"
																	+ c.getString(9).toString();
														} else {
															elevationttype = "additonal"
																	+ c.getString(9).toString();
														}

														elevationttype += tests;
														bitmap = ShrinkBitmap(cf.getsinglequotes(c.getString(c
																.getColumnIndex("ImageName"))), 400, 400);

														MarshalBase64 marshal = new MarshalBase64();
														ByteArrayOutputStream out = new ByteArrayOutputStream();
														bitmap.compress(CompressFormat.PNG, 100, out);
														byte[] raw = out.toByteArray();

														SoapObject request = new SoapObject(cf.NAMESPACE,
																cf.METHOD_NAME18);
														request.addProperty("InspectorID", cf.InspectorId);
														request.addProperty("SRID", c.getString(1));
														request.addProperty("Elevation", c.getString(3));
														request.addProperty("Description",
																cf.getsinglequotes(c.getString(5)));
														request.addProperty("CreatedOn", c.getString(6));
														request.addProperty("ModifiedOn", c.getString(7));
														request.addProperty("ImageOrder", c.getString(9));
														request.addProperty("ImageNameWithExtension",
																elevationttype);
														request.addProperty("imgByte", raw);

														SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
																SoapEnvelope.VER11);
														envelope.dotNet = true;
														try
														{
														envelope.setOutputSoapObject(request); 
														marshal.register(envelope);
														HttpTransportSE httpTransport = new HttpTransportSE(
																cf.URL);
														System.out.println("request1 "+request);
														
														httpTransport.call(cf.SOAP_ACTION18, envelope);
														Object response = envelope.getResponse();
														//error=0;
														}
														catch (IOException e) {
															// TODO: handle exception														
														}

													}
										}
										else
										{
											
											String elevationttype5;
											if (tests.equals("") || tests.equals("null") || tests == null) {
											if (c.getString(3).toString().equals("1")) {
												elevationttype5 = "exteiror image"
														+ " image order "
														+ c.getString(9).toString();
											} else if (c.getString(3).toString().equals("2")) {
												elevationttype5 = "interior image"
														+ " image order "
														+ c.getString(9).toString();
											} else {
												elevationttype5 = "additonal image"
														+ " image order "
														+ c.getString(9).toString();
											}
											Error_traker("There is a problem in uploading general images in "+elevationttype5);
											}
											else
											{
												
												if(elevationnamegh.equals("1")){elevnamegh = "Exterior Elevation";}
												else if(elevationnamegh.equals("2")){elevnamegh = "Interior Elevation";}
												else if(elevationnamegh.equals("3")){elevnamegh = "Additional Elevation";}
												
												tochkfileexists[2]= true;
												Error_traker("Please check the image (" + mypath + ") in "+elevnamegh+" tab is available in your external storage." );
												
											}

											
										}
										c.moveToNext();
										if (total <= Temp_total+increment_unit) {
											temp_inc +=Totaltmp;
											total = Temp_total + (int) (temp_inc);
											   
										} else {
											total = Temp_total+increment_unit;
										}
									}
									
								}
								else
								{
									return true;
								}

							}
							else
							{
								
									Error_traker("You have problem in the General Information tables data Exporting" );
							}
							return true;
						}
						
						
	Bitmap ShrinkBitmap(String file, int width, int height) {

		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			cf.errorlogmanage("Problme in opening the image in sring  error="
					+ e.getMessage() + "image paath and name =" + file," "+(cf.rcstr)+
					selectedItem.toString());
			return bitmap;
		}

	}
	private void retrieveonlinedata(SoapObject result) {
		
		int Cnt = result.getPropertyCount();
		int onpre=0,onuts=0,oncan=0,onrr=0,ontyppre=0,ontyputs=0,ontypcan=0,ontyprr=0;
		if (Cnt >= 1) {
			SoapObject obj = (SoapObject) result;

			for (int i = 0; i < obj.getPropertyCount(); i++) {
				SoapObject obj1 = (SoapObject) obj.getProperty(i);
				if (!obj1.getProperty("i_maininspectiontype").toString()
						.equals("")) {

					if (29 == Integer.parseInt(obj1.getProperty(
							"i_maininspectiontype").toString())) {
						
						onpre = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						onuts = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						oncan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						onrr =Integer.parseInt(obj1.getProperty("Completed")
														.toString());
						
                         
					}

					if (28 == Integer.parseInt(obj1.getProperty(
							"i_maininspectiontype").toString())) {
						ontyppre = Integer.parseInt(obj1.getProperty(
								"Comp_Ins_in_Online").toString());
						ontyputs = Integer.parseInt(obj1.getProperty("UTS")
								.toString());
						ontypcan = Integer.parseInt(obj1.getProperty("Can")
								.toString());
						ontyprr =  Integer.parseInt(obj1.getProperty("Completed")
														.toString());
						
					}

				}
			}
			try
			{
				//insp_id varchar(50),C_28_pre varchar(5) Default('0'),C_28_uts varchar(5) Default('0'),C_28_can varchar(5) Default('0'),C_28_RR varchar(5) Default('0')
				Cursor c=cf.SelectTablefunction(cf.count_tbl, " WHERE insp_id='"+cf.InspectorId+"'");
				if(c.getCount()>0)
				{
					cf.db.execSQL(" UPDATE "+cf.count_tbl+" SET C_28_pre='"+ontyppre+"',C_28_uts='"+ontyputs+"',C_28_can='"+ontypcan+"',C_28_RR='"+ontyprr+"',C_29_pre='"+onpre+"',C_29_uts='"+onuts+"',C_29_can='"+oncan+"',C_29_RR='"+onrr+"'  Where  insp_id='"+cf.InspectorId+"'");
				}
				else
				{
					cf.db.execSQL(" INSERT INTO  "+cf.count_tbl+" (insp_id,C_28_pre,C_28_uts,C_28_can,C_28_RR,C_29_pre,C_29_uts,C_29_can,C_29_RR) VALUES " +
							"('"+cf.InspectorId+"','"+ontyppre+"',"+ontyputs+","+ontypcan+","+ontyprr+","+onpre+","+onuts+","+oncan+","+onrr+")");
				}
				c.close();
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg;
			intimg = new Intent(Exportedddata.this, HomeScreen.class);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void getauditflag() {
		try {
			System.out.println("cf.selectedhomeid"+cf.selectedhomeid);
			Cursor cur = cf.SelectTablefunction(cf.mbtblInspectionList," where s_SRID='" + cf.selectedhomeid + "' and InspectorId='"+ cf.InspectorId + "'");
			System.out.println("dfdf"+cur.getCount());
			cur.moveToFirst();
			auditflag = cur.getString(cur.getColumnIndex("AuditFlag"));System.out.println("auditflag"+auditflag);
			
		} catch (Exception e) {
		}
	}
}