package idsoft.inspectiondepot.B11802;



import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class FrontElevation extends Activity implements Runnable {
	String paht, fullname, InspectorId, picname, homeid, updstr,name = "FE ",repidofselbtn1,
			selectedImagePath = "empty", capturedImageFilePath, sgnpath,description="",
			pathdesc, InspectionType, status, ImgDesc, imgtitle,phtodesc,elevtype,elevid,
			imgnum, imgord, j,finaltext="",pathofimage="",bulkinsertstring="";
	ImageView fstimg, sgntick, frnttick,rgttick, lfttick, bactick, atctick, addittick,fullimageview;
	Map<String, String[]> elevationcaption_map = new LinkedHashMap<String, String[]>();
	File[] Currentfile_list=null;
	int curr_orientation=0,l=2,p=1;int max_allowed=8;
	ListAdapter adapter;
	RelativeLayout Lin_Pre_nex=null,Rec_count_lin=null;
	ImageView prev=null,next=null;
	File images=null;
	ScrollView scr2=null;	
	TextView Total_Rec_cnt=null,showing_rec_cnt;
	Bitmap rotated_b;
	String[] pieces1=null,pieces2=null,pieces3=null,photocaption,fldelevationtype,fldelevationid,items = { "gallery", "camera" },str,elevation;
	CheckBox cb1;
	Uri mCapturedImageURI;
	int t, b, quesid, rws, chk, selid, elev=0, f = 0, io = 0, iof = 0, ior = 0,cvrtstr1,globalvar=0,count,count1,
			maxclick, maxlevel = 0,maximumindb=0,selectedcount=0,currnet_rotated=0,limit_start=0,
			id, delimagepos,backclick=0,maxLength = 99,ImgOrder = 0,alreadyExist=0,notExist=0,value, Count, isexist;
	EditText eddesc1, eddesc2, edbrowse, eddesc3, ed1, firsttxt,input;
	private static final int SELECT_PICTURE = 0;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	private static final int visibility = 0;
	private static final String TAG = null;
	LinearLayout lnrlayout,lnrlayoutparent,lnrlayoutchild;
	Button btnbrwse,btncamera ,btnupd, updat, del, changeimageorder,signimg, frntimg, bacimg, rgtimg, lftimg, atcimg,additionalimg;
	View v1,vv;
	Button backfolderwise,cancelfolderwise,selectfolderwise,selectfullimage,cancelfullimage;;
	TextView policyholderinfo,imagepath,txthdr,titleheader,textviewvalue_parent,textviewvalue_child,childtitleheader;
	ProgressDialog pd,pd1;
	String selectedtestcaption[] =null;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	Cursor cursor;
	Bitmap[] thumbnails,thumbnails1;
	boolean[] thumbnailsselection,thumbnailsselection1;	
	private ImageAdapter1 imageAdapter1;
	String patharr[] = new String[8];
	String[] arrPath1, elevationdescription, arrimgord, arrpath,arrpathdesc,arrPath;	
	int[] imageid,imageorder;
	EditText[] et,chan_ed;
	TextView tvstatus[],tvstatusparent[],tvstatuschild[];
	Spinner spnelevation[];	
	File externamstoragepath;
	File listFile[];
	ImageView thumbviewimgchild[],imgviewparent[],elevationimage[];
	CheckBox chkboxchild[];
	Dialog dialogfullimage,dialog;
	CommonFunctions cf;
	Map<String, TextView> mapfolder = new LinkedHashMap<String, TextView>();
	TableRow firstrow;
	ArrayElevation arrelev = new ArrayElevation();
	UploadRestriction upr = new UploadRestriction();

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(13);
		cf.Createtablefunction(9);
		cf.Createtablefunction(18);
		cf.getDeviceDimensions();
		Bundle bunQ1extras1 = getIntent().getExtras();
		Bundle bunQ1extras2 = getIntent().getExtras();
		cf.Homeid = homeid = bunQ1extras1.getString("homeid");
		elev = bunQ1extras2.getInt("elevation");
		cf.InspectionType = InspectionType = bunQ1extras1
				.getString("InspectionType");
		cf.status = status = bunQ1extras1.getString("status");
		cf.value = value = bunQ1extras1.getInt("keyName");

		setContentView(R.layout.frontelevation);
		cf.getInspectorId();
		cf.getinspectiondate();
		
		ScrollView scr = (ScrollView) findViewById(R.id.scr);
		scr.setMinimumHeight(cf.ht);
		
		
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		signimg = (Button) findViewById(R.id.sign);
		frntimg = (Button) findViewById(R.id.front);
		rgtimg = (Button) findViewById(R.id.right);
		bacimg = (Button) findViewById(R.id.back);
		lftimg = (Button) findViewById(R.id.left);
		atcimg = (Button) findViewById(R.id.attic);
		sgntick = (ImageView) findViewById(R.id.signovr);
		frnttick = (ImageView) findViewById(R.id.frontovr);
		rgttick = (ImageView) findViewById(R.id.rightovr);
		bactick = (ImageView) findViewById(R.id.backovr);
		lfttick = (ImageView) findViewById(R.id.leftovr);
		atctick = (ImageView) findViewById(R.id.atticovr);

		edbrowse = (EditText) findViewById(R.id.pathtxt);
		edbrowse.setVisibility(vv.GONE);
		btnbrwse = (Button) findViewById(R.id.browsetxt);
		btncamera = (Button) findViewById(R.id.browsecamera);

		btnupd = (Button) findViewById(R.id.updtxt);
		changeimageorder = (Button) findViewById(R.id.changeimageorder);
		lnrlayout = (LinearLayout) this.findViewById(R.id.linscrollview);
		fstimg = (ImageView) findViewById(R.id.firstimg);
		additionalimg = (Button) findViewById(R.id.additional);
		addittick = (ImageView) findViewById(R.id.additionalovr);
		firsttxt = (EditText) findViewById(R.id.firsttxt);
		updat = (Button) findViewById(R.id.update);
		txthdr = (TextView) findViewById(R.id.titlehdr);
		del = (Button) findViewById(R.id.delete);
		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		TextView rccode = (TextView) findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		
		if (elev == 1) {			
			
			btnshowcommon();
			name = "FE ";
			frntimg.setBackgroundResource(R.drawable.submenusrepeatovr);
		} else if (elev == 2) {
			
			btnshowcommon();
			name = "RE ";
			rgtimg.setBackgroundResource(R.drawable.submenusrepeatovr);
		} else if (elev == 3) {	
			
			btnshowcommon();
			name = "BE ";
			bacimg.setBackgroundResource(R.drawable.submenusrepeatovr);
		} else if (elev == 4) {		
			
			btnshowcommon();
			name = "LE ";
			lftimg.setBackgroundResource(R.drawable.submenusrepeatovr);
		} else if (elev == 5) {		
			
			btnshowcommon();
			name = "AE ";
			atcimg.setBackgroundResource(R.drawable.submenusrepeatovr);
		} else if (elev == 8) {		
			max_allowed=20;
			System.out.println("came   sds");
			btnshowcommon();
			name = "Additional Photo ";
			additionalimg.setBackgroundResource(R.drawable.submenusrepeatovr);
		}
		
		callthread();
		
		
		changeimageorder.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				subview();
			}
		});

		
		 try{
        	 Cursor cur = cf.db.rawQuery("select * from "+cf.mbtblInspectionList + " where s_SRID='"+cf.Homeid+"'",null);
        	 int rws1=cur.getCount();
		     cur.moveToFirst();
		     String ownerfirstname=cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnersNameFirst")));
			    String ownerlastname=cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnersNameLast")));
			    String policyno=cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnerPolicyNumber")));
		    policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "+ownerlastname +"<br/><font color=#bddb00>(Policy No : "+policyno+ ")</font>"));
			
		}	
        catch(Exception e)
        {
        	System.out.println(" errrr "+e.getMessage());
        }
	}
	
	private void callthread() {  
		// TODO Auto-generated method stub
		try
		{
			//cf.db.execSQL("Delete from "+cf.tblphotocaption);
		Cursor c1 = cf.db.rawQuery("SELECT * FROM " + cf.tblphotocaption, null);
		if(c1.getCount()==0)
		
		{
			 pd1 = ProgressDialog.show(FrontElevation.this, "Loading Captions ...", "Please wait...", true, false);
		        new Thread(new Runnable() {
		                public void run() {
		                	System.out.println("rrrun");		       
		                	cf.insertelevation();
		                	finishedHandler.sendEmptyMessage(0);		                
		                }
		            }).start();
		       
		}
		else
		{
			String bulkinsertstring1="";int inc_id=0;int ultimo_id=0;int inid=0;
			Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.tblphotocaption + " where fld_elevtype='Additional Photo ' and fld_elevid in (9,10,11,12,13,14,15,16,17,18,19,20)", null);
			if(c2.getCount()==0)				
			{			
				String query = "SELECT Id from "+cf.tblphotocaption+" order by Id DESC limit 1";
				Cursor c = cf.db.rawQuery(query,null);
				if (c != null && c.moveToFirst()) {					
					ultimo_id=0;
					ultimo_id =   c.getInt(c.getColumnIndex("Id"));
					inid=ultimo_id;
				}

				for(int p=8;p<20;p++) // For all Additional elevation we have the same caption to be inserted. So we have kept in seperate array and inserted it.
				{
					int h =p+1;
					for(int k=0;k<arrelev.addielev.length;k++)
					{
						l=ultimo_id+2;
						elevtype = "Additional Photo ";
						bulkinsertstring1 += " UNION SELECT '"+l+"', '"+cf.convertsinglequotes(arrelev.addielev[k])+"','" + elevtype+ "','" + h + "'";
						ultimo_id++;
						
					}
					ultimo_id=l;
				}
			
				inc_id = inid+1;
				cf.db.execSQL("INSERT INTO "+cf.tblphotocaption
						+ " SELECT '"+inc_id+"' as Id, '' as  fld_photocaption,'' as  fld_elevtype,'' as fld_elevid" +
						bulkinsertstring1);
				changeimage();
				showimages();
			}
			else
			{
				System.out.println("inside else callthred");
				changeimage();
				showimages();
			}
		}
		}catch(Exception e)
		{
			System.out.println(" EEEE "+e.getMessage());
		}
	}                                                      




	private void commonfunction(String imgor, int spin) {
		// TODO Auto-generated method stub
		int imgorder = 0;String[] temp;
		try {
			imgorder = Integer.parseInt(imgor);
		} catch (Exception e) {

		}
		Cursor c1 = cf.SelectTablefunction(cf.tblphotocaption,
				" WHERE fld_elevtype='" + name
						+ "' and fld_elevid='" + imgor + "' and fld_photocaption!=''");
		if (c1.getCount() > 0) {
			
			temp = new String[c1.getCount()];
			
			int i = 0;
			c1.moveToFirst();
			do {  
				
				
				temp[i] = cf.getsinglequotes(c1.getString(c1
						.getColumnIndex("fld_photocaption")));
				i++;
			} while (c1.moveToNext());
		} else {
			temp = new String[2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
		}
		elevationcaption_map.put("spiner" + spin, temp);
	}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	        pd1.dismiss();
	          Intent intimg1 = new Intent(FrontElevation.this, FrontElevation.class);
			intimg1.putExtra("homeid", cf.Homeid);
			intimg1.putExtra("elevation", elev);
			intimg1.putExtra("InspectionType", cf.InspectionType);
			intimg1.putExtra("status", cf.status);			
			intimg1.putExtra("keyName", cf.value);
			startActivity(intimg1);
	    }
	};


	private void changeimage() {
		// TODO Auto-generated method stub
		try {
			Cursor sgncur = cf
					.SelectTablefunction(
							cf.ImageTable,
							"where SrID='"
									+ cf.Homeid
									+ "' and (Elevation=6 or Elevation=7) and Delflag=0");
			int sgnrws = sgncur.getCount();
			if (sgnrws != 0) {
				sgntick.setVisibility(visibility);
			}

			Cursor frncur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=1 and Delflag=0");
			int frnrws = frncur.getCount();
			if (frnrws != 0) {
				frnttick.setVisibility(visibility);
			}

			Cursor rgtcur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=2 and Delflag=0");
			int rgtrws = rgtcur.getCount();
			if (rgtrws != 0) {
				rgttick.setVisibility(visibility);
			}

			Cursor baccur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=3 and Delflag=0");         
			int bacrws = baccur.getCount();
			if (bacrws != 0) {
				bactick.setVisibility(visibility);
			}

			Cursor lftcur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=4 and Delflag=0");
			int lftrws = lftcur.getCount();
			if (lftrws != 0) {
				lfttick.setVisibility(visibility);
			}

			Cursor atccur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=5 and Delflag=0");
			int atcrws = atccur.getCount();
			if (atcrws != 0) {
				atctick.setVisibility(visibility);
			}

			Cursor addcur = cf.SelectTablefunction(cf.ImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=8 and Delflag=0");
			int addrws = addcur.getCount();
			if (addrws != 0) {
				addittick.setVisibility(visibility);
			}

		} catch (Exception e) {

		}

	}
	protected void subview() {
		// TODO Auto-generated method stub
		
		Cursor d11 = cf.db.rawQuery("SELECT * FROM " + cf.ImageTable
				+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
				+ "' and Delflag=0 order by ImageOrder", null);
		rws = d11.getCount();
		if (rws == 0) {
			cf.ShowToast("There is no Image, So please upload the Image.",1);
		} else {
			String Imagepath[] = new String[rws];

			et = new EditText[rws];
			dialog = new Dialog(FrontElevation.this);
 			dialog.setContentView(R.layout.chnageimageorder);
 			dialog.setTitle("Change Image Order");
 			dialog.setCancelable(false);
 			Button selectBtn=(Button) dialog.findViewById(R.id.Save);
 			Button close=(Button) dialog.findViewById(R.id.close);
 			
 			
 			 TableLayout ln_main=(TableLayout) dialog.findViewById(R.id.changeorder);
	 			try {
	 				this.count1 = d11.getCount();
					this.thumbnails1 = new Bitmap[this.count1];
					this.arrPath1 = new String[this.count1];
					this.imageid = new int[this.count1];
					this.imageorder = new int[this.count1];
					this.str = new String[this.count1];
					ImageView chag_img[] = new ImageView[this.count1];
	 				chan_ed= new EditText[this.count1]; 
	 				TableRow ln_sub=null;
	 				d11.moveToFirst();
	 				InputFilter[] FilterArray = new InputFilter[1];  
					FilterArray[0] = new InputFilter.LengthFilter(2);  
					
	 				
	 				
	 				for (int i = 0; i < this.count1; i++) {
	 					/**We create the new check box and edit box for the change image order **/
	 					chag_img[i]=new ImageView(FrontElevation.this); 
	 					TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
	 					TableRow.LayoutParams lp2 = new TableRow.LayoutParams(100, 100);
	 					lp.setMargins(5, 10, 5, 10);
	 					chag_img[i].setLayoutParams(lp2);
	 					chan_ed[i]=new EditText(FrontElevation.this);
	 					chan_ed[i].setFilters(FilterArray); 
	 					//chan_ed[i].setMa))(2);
	 					chan_ed[i].setInputType(InputType.TYPE_CLASS_NUMBER);
	 					chan_ed[i].setLayoutParams(lp);
	 					LinearLayout li_tmp=new LinearLayout(FrontElevation.this);
	 					li_tmp.setLayoutParams(lp);
	 					
	 					/**We create the new check box and edit box for the change image order ends  **/
	 					
	 					this.imageid[i] = Integer.parseInt(d11.getString(0)
								.toString());
						this.imageorder[i] = d11.getInt(d11
								.getColumnIndex("ImageOrder"));System.out.println("imageorder "+this.imageorder[i]);
						arrPath1[i] = cf.getsinglequotes(d11.getString(d11
								.getColumnIndex("ImageName")));
	 					
	 					
	 					Bitmap b = cf.ShrinkBitmap(arrPath1[i], 130, 130);
	 					thumbnails1[i] = b;
	 					if(i==0)
	 					{
	 						
	 						LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
 							ln_sub = new TableRow(FrontElevation.this);
 							ln_sub.setLayoutParams(lp1);  
	 					}
	 					li_tmp.addView(chag_img[i]);
	 					li_tmp.addView(chan_ed[i]);
	 					ln_sub.addView(li_tmp);
	 					chag_img[i].setImageBitmap(b);
	 					chan_ed[i].setText(String.valueOf(this.imageorder[i]));
	 					d11.moveToNext();
	 					if((((i+1)%3)==0 || (i+1)==this.count1)&& ln_sub!=null )
 						{
 							ln_main.addView(ln_sub);
 							LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
 							ln_sub = new TableRow(FrontElevation.this);
 							ln_sub.setLayoutParams(lp1);  
 						}
	 					
	 				}
	 			}
	 			catch(Exception e)
	 			{
	 				System.out.println("E change in image order "+e.getMessage());
	 			}

	 			close.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						// TODO Auto-generated method stub				
						dialog.cancel();
					}
				});
				selectBtn.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						// dialog.cancel();
						Boolean boo = true;System.out.println("count1 "+count1);
						String temp[] = new String[count1];
						try {
							System.out.println("length "+imageid.length);
							for (int i = 0; i < imageid.length; i++) {
								//System.out.println("str"+str[i]);
								temp[i] = chan_ed[i].getText().toString();
								int myorder = 0;
								System.out.println("temp "+temp[i]);
								if (temp[i] != null && !temp[i].equals("")) {
									myorder = Integer.parseInt(temp[i]);
									if (myorder <= imageid.length
											&& myorder != 0) {
           
									} else {
										if((myorder==0) || (myorder==9))
										{
											cf.ShowToast("This is an Invalid Image Order.",1);
										}
										else if(myorder>=imageid.length)
										{
											cf.ShowToast("This is an Invalid Image Order.",1);
										}
										
										boo = false;
										
										
									}

								} else {
									
									cf.ShowToast("Image Order can't be empty. Please enter Image Order.",1);
									boo = false;
								}

							}
							int length = temp.length;
							for (int i = 0; i < length; i++) {
								for (int j = 0; j < length; j++) {
									if (temp[i].equals(temp[j]) && i != j) {
										alreadyExist=1;
										
									}
								}

							}
							if(alreadyExist==1)
							{cf.ShowToast("You cant enter this Image order. This already exists.",1);
								alreadyExist=0;
							}
							else
							{
							
								if (boo) {
									for (int i = 0; i < imageid.length; i++) {
										cf.db.execSQL("UPDATE " + cf.ImageTable
												+ " SET ImageOrder='" + temp[i]
												+ "' WHERE WSID='" + imageid[i]
												+ "'");
									}
	
									dialog.cancel();
									cf.ShowToast("Image Order saved successfully.",1);
									showimages();
								}
							}
						}
						catch (Exception e) {
							//cf.Error_LogFile_Creation("Problem while updating image order error = "+e.getMessage());
			            	
						}
					}
				});
				dialog.show();
			/*} catch (Exception m) {
				
			}*/
		}

	}

	public void clicker(final View v) {
		switch (v.getId()) {
		case R.id.img01:
			Intent i = new Intent();
			i.putExtra("homeid", cf.Homeid);
			i.putExtra("InspectionType", cf.InspectionType);
			i.putExtra("status", cf.status);
			i.putExtra("keyName", cf.value);
			i.putExtra("Count", cf.Count);
			i.setClassName("idsoft.inspectiondepot.B11802",
					"idsoft.inspectiondepot.B11802.PersonalInfo");
			startActivity(i);
			break;
		case R.id.img02:
			Intent intque = new Intent(FrontElevation.this, BuildCode.class);
			intque.putExtra("homeid", cf.Homeid);
			intque.putExtra("iden", "test");
			intque.putExtra("InspectionType", cf.InspectionType);
			intque.putExtra("status", cf.status);
			intque.putExtra("keyName", cf.value);
			intque.putExtra("Count", cf.Count);
			startActivity(intque);
			break;
		case R.id.img03:
			break;
		case R.id.img04:
			Intent fb = new Intent(FrontElevation.this, FeedbackDocuments.class);
			fb.putExtra("homeid", cf.Homeid);
			fb.putExtra("InspectionType", cf.InspectionType);
			fb.putExtra("status", cf.status);
			fb.putExtra("keyName", cf.value);
			fb.putExtra("Count", cf.Count);
			startActivity(fb);
			break;
		case R.id.img06:
			Intent iInspectionList = new Intent(FrontElevation.this,
					OverallComments.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);
			startActivity(iInspectionList);
			break;
		case R.id.img07:
			Intent genimg = new Intent(FrontElevation.this,
					GeneralConditions.class);
			genimg.putExtra("homeid", cf.Homeid);
			genimg.putExtra("InspectionType", cf.InspectionType);
			genimg.putExtra("status", cf.status);
			genimg.putExtra("keyName", cf.value);
			genimg.putExtra("Count", cf.Count);
			startActivity(genimg);
			break;
		case R.id.img08:
			Intent mapimg = new Intent(FrontElevation.this, Maps.class);
			mapimg.putExtra("homeid", cf.Homeid);
			mapimg.putExtra("InspectionType", cf.InspectionType);
			mapimg.putExtra("status", cf.status);
			mapimg.putExtra("keyName", cf.value);
			mapimg.putExtra("Count", cf.Count);
			startActivity(mapimg);
			break;
		case R.id.img09:
			Intent subimg = new Intent(FrontElevation.this, Submit.class);
			subimg.putExtra("homeid", cf.Homeid);
			subimg.putExtra("InspectionType", cf.InspectionType);
			subimg.putExtra("status", cf.status);
			subimg.putExtra("keyName", cf.value);
			subimg.putExtra("Count", cf.Count);
			startActivity(subimg);
			break;
		case R.id.sign:
			Intent intimg = new Intent(FrontElevation.this, ImagesData.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			break;
		case R.id.front:
			elev = 1;
			name = "FE ";			
			btnshowcommon();
			frntimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			showimages();
			break;
		case R.id.right:
			elev = 2;
			name = "RE ";
			btnshowcommon();
			lnrlayout.removeAllViews();
			arrpath = null;
			arrpathdesc = null;
			elevationdescription = null;
			rgtimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			showimages();
			break;
		case R.id.back:
			elev = 3;
			name = "BE ";
			btnshowcommon();
			lnrlayout.removeAllViews();
			arrpath = null;
			arrpathdesc = null;
			elevationdescription = null;
			bacimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			showimages();
			break;
		case R.id.left:
			elev = 4;
			name = "LE ";
			btnshowcommon();
			lnrlayout.removeAllViews();
			arrpath = null;
			arrpathdesc = null;
			elevationdescription = null;
			lftimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			showimages();
			break;
		case R.id.attic:
			elev = 5;
			name = "AE ";
			btnshowcommon();
			lnrlayout.removeAllViews();
			arrpath = null;
			arrpathdesc = null;
			elevationdescription = null;
			atcimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			showimages();
			break;
		case R.id.additional:
			elev = 8;
			name = "Additional Photo ";
			btnshowcommon();
			lnrlayout.removeAllViews();
			arrpath = null;
			arrpathdesc = null;
			elevationdescription = null;
			additionalimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			showimages();
			break;

		case R.id.browsetxt:
			cf.Createtablefunction(13);
			Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "' and Delflag=0  order by ImageOrder", null);
			int chkrws = c11.getCount();
			
			maximumindb=chkrws;
			if (chkrws == 8 || elev==8) {
				if(elev==8)
				{
					System.out.println("if "+elev);
					if(chkrws==20)
					{
						cf.ShowToast("You are allowed to add only 20 images per elevation.",1);
					}
					else
					{
						callprogress();
					}
			
				}
				else
				{
					System.out.println("else "+elev);
					cf.ShowToast("You are allowed to add only 8 images per elevation.",1);
				}
				
				
			} else {
				callprogress();
			}
			break;
			
		case R.id.browsecamera:
			
			Cursor c12 = cf.db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "' and Delflag=0  order by ImageOrder", null);
			int chkrws1 = c12.getCount();
			maximumindb=chkrws1;
			if (chkrws1 == 8 || elev==8) {
				if(elev==8)
				{
					System.out.println("if "+elev);
					if(chkrws1==20)
					{
						cf.ShowToast("You are allowed to add only 20 images per elevation.",1);
					}
					else
					{
						callthread();

						t = 1;
						startCameraActivity();
					}
			
				}
				else
				{
					System.out.println("else "+elev);
					cf.ShowToast("You are allowed to add only 8 images per elevation.",1);
				}
			}	
		else {
			callthread();

			t = 1;
			startCameraActivity();
		}
			System.out.println("completed fully here");
			break;

		case R.id.home:
			cf.gohome();
			break;
		case R.id.save:
		
			if (elev == 1) {
				fullname = "Front Elevation";
				if (rws == 0) {
					cf.ShowToast("Please upload atleast one image of " + fullname+ ".",1);
				} else {
					cf.ShowToast(fullname	+ " has been saved sucessfully.",1);
					nxtintent();
				}
			} else if (elev == 2) {
				fullname = "Right Elevation";
				if (rws != 0) {
					
					cf.ShowToast(fullname	+ " has been saved sucessfully.",1);
					}
				nxtintent();
			} else if (elev == 3) {
				fullname = "Back Elevation";
				if (rws != 0) {cf.ShowToast(fullname	+ " has been saved sucessfully.",1);}
				nxtintent();
			} else if (elev == 4) {
				fullname = "Left Elevation";
				if (rws != 0) {cf.ShowToast(fullname	+ " has been saved sucessfully.",1);}
				nxtintent();
			} else if (elev == 5) {
				fullname = "Attic Elevation";
				if (rws != 0) {cf.ShowToast(fullname	+ " has been saved sucessfully.",1);}
				nxtintent();
			} else if (elev == 8) {
				fullname = "Addional Images";
				if (rws != 0) {cf.ShowToast(fullname	+ " has been saved sucessfully.",1);}
				nxtintent();
			}

			break;

		}
	}
private void callprogress()
{
	t=0;
	 String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
	 pd = ProgressDialog.show(FrontElevation.this, "",
		Html.fromHtml(source), true);
	 Thread thread = new Thread(FrontElevation.this);
	 thread.start();
}

private void walkdir(File images2) {
	File[] current_fold_list =images2.listFiles(directoryFilter);
	
	if(current_fold_list!=null)
	{
		pieces1=new String[current_fold_list.length];
		for (int i = 0; i < current_fold_list.length; i++) {
			   
			   if (current_fold_list[i].isDirectory()) {
            
            
            pieces1[i]=current_fold_list[i].getName();
         }
			   else
			 	 {
			 		
			 	 }
		}
		
	}
	/** Get only the folders form the File array list Ends   **/
	/** Get only the Images  form the File array list Starts   **/
	Currentfile_list=images2.listFiles(imageFilter);
	   if (Currentfile_list != null)
	   {
		  int j=0; 
		  
		  if(Currentfile_list.length<=20)
			 
		  pieces3=new String[Currentfile_list.length];
		  else
		  {
			  pieces3=new String[20];
		  }
		  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
		 
		   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
			   
			 
    		
    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".GIF"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
    	            	 
    			 		 pieces3[j]=Currentfile_list[i].getName();
    	            
            		 }
    			 	 else
    			 	 {
    			 		
    			 	 }
    		 
	   }
		  
	 }
	   /** Get only the Images  form the File array list Ends   **/
	   /** Set the visisbolity to  the Prev and next button layout Starts ***/
	   if(Lin_Pre_nex !=null && Currentfile_list.length>20 )
	{
		
		Lin_Pre_nex.setVisibility(View.VISIBLE);
		Rec_count_lin.setVisibility(View.VISIBLE);
			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("Records Displayed from 1 to 20");
			prev.setVisibility(View.GONE);
			next.setVisibility(View.VISIBLE);
		
	}
	else if(Lin_Pre_nex !=null)
	{

		Lin_Pre_nex.setVisibility(View.GONE);
		Rec_count_lin.setVisibility(View.VISIBLE);
		Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("");
		
	}
	
}

private void dynamicimagesparent() {
	// TODO Auto-generated method stub
	lnrlayoutparent.removeAllViews();
	//titleheader = new TextView(this);
	if(finaltext.equals("")||finaltext.equals("null"))
	{			
		titleheader.setText("/mnt/sdcard/");
		childtitleheader.setText("sdcard");
	}
	else
	{
		if(backclick==1)
		{
			titleheader.setText(pathofimage);backclick=0;
		}
		else
		{
			titleheader.setText(finaltext);
		}
	}
	if(pieces1!=null)
	{
		tvstatusparent = new TextView[pieces1.length];
		imgviewparent = new ImageView[pieces1.length];		
	
	for (int i = 0; i < pieces1.length; i++) {
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayoutparent.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				50, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 10;
		paramschk.topMargin = 5;
		paramschk.bottomMargin = 15;
		l2.addView(lchkbox);

		imgviewparent[i] = new ImageView(this);
		imgviewparent[i].setBackgroundResource(R.drawable.allfilesicon);
		lchkbox.addView(imgviewparent[i], paramschk);

		LinearLayout ltextbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramstext.topMargin = 5;
		paramstext.leftMargin = 15;
		l2.addView(ltextbox,paramstext);
		
		tvstatusparent[i] = new TextView(this);
		tvstatusparent[i].setText(pieces1[i]);
		tvstatusparent[i].setTextColor(Color.BLACK);
		tvstatusparent[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
		//tvstatusparent[i].setTextSize(16);
		tvstatusparent[i].setMinimumWidth(100);
		mapfolder.put("tvstatusparent" + i, tvstatusparent[i]);
		tvstatusparent[i].setTag("tvstatusparent" + i);
		imgviewparent[i].setTag("tvstatusparent" + i);
		ltextbox.addView(tvstatusparent[i], paramstext);
		
		tvstatusparent[i].setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				pieces1=null;pieces3=null;pieces2=null;
				String gettagvaluparent = v.getTag().toString();
				String repidofselected = gettagvaluparent.replace("tvstatusparent","");
				final int inconevalueparent = Integer.parseInt(repidofselected) + 1;
				textviewvalue_parent = mapfolder.get(gettagvaluparent);
				subfoldersclick(inconevalueparent,textviewvalue_parent);
			}
		});
		
		imgviewparent[i].setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				pieces1=null;pieces3=null;pieces2=null;
				String gettagvaluparent = v.getTag().toString();
				String repidofselected = gettagvaluparent.replace("tvstatusparent","");
				final int inconevalueparent = Integer.parseInt(repidofselected) + 1;
				textviewvalue_parent = mapfolder.get(gettagvaluparent);
				if(backclick==1)
				{
				String[] bits = finaltext.split("/");
				String picname1 = bits[bits.length - 1];
				childtitleheader.setText(picname1); // set the chaild title 
				}
				else
				{
					String[] bits = pathofimage.split("/");
					String picname1 = bits[bits.length - 1];
					childtitleheader.setText(picname1); // set the chaild title 
				}
				subfoldersclick(inconevalueparent,textviewvalue_parent);
			}
		});
		
	}
	}
	
	else
	{
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayoutparent.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 15;
		paramschk.topMargin = 20;
		l2.addView(lchkbox);
		
		
		TextView tvnorecordparent = new TextView(this);
		tvnorecordparent.setText("No Sub Folders");
		tvnorecordparent.setTextColor(Color.BLACK);
		tvnorecordparent.setMinimumWidth(200);
		lchkbox.addView(tvnorecordparent,paramschk);
	}
	
}
private void subfoldersclick(int inconevalueparent,TextView textviewvalue_parent) {
	// TODO Auto-generated method stub
	String value = titleheader.getText().toString();	
	if(value=="/mnt/sdcard/")
	{
		titleheader.setText(value+textviewvalue_parent.getText().toString()+"/");
	}
	else
	{
			if ((value.endsWith(".jpg"))||(value.endsWith(".JPG"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")
					 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp")))
			{
				String[] bits = value.split("/");
				String picname = bits[bits.length - 1];
				String val = value.replace(picname, textviewvalue_parent.getText().toString());
				titleheader.setText(val);
			}
			else
			{
				titleheader.setText(value+textviewvalue_parent.getText().toString()+"/");
			}
	} 
		if ((value.endsWith(".jpg"))||(value.endsWith(".JPG"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")
				 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp"))){
			dialog = new Dialog(FrontElevation.this);
	 		dialog.setContentView(R.layout.alertfullimage);
	 		dialog.setTitle("Select Images");
	 		dialog.setCancelable(true);
	 		ImageView fullimageview = (ImageView) dialog.findViewById(R.id.full_image);
	 		Button selectBtnfullimage = (Button) dialog.findViewById(R.id.selectBtn);
	 		Button cancelBtnfullimage = (Button) dialog.findViewById(R.id.cancelBtn);
	 	
	 		BitmapFactory.Options o2 = new BitmapFactory.Options();
			int scale = 0;
			o2.inSampleSize = scale;
			Bitmap bitmap = null;
			try {
				bitmap = BitmapFactory.decodeStream(new FileInputStream(titleheader.getText().toString()), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				fullimageview.setImageDrawable(bmd);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cancelBtnfullimage.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dialog.cancel();
				}
			});
			dialog.show();
		}
		else
		{
			finaltext = titleheader.getText().toString();
			File f = new File(titleheader.getText().toString());
			String[] bits = titleheader.getText().toString().split("/");
			String picname1 = bits[bits.length - 1];
			childtitleheader.setText(picname1);
			walkdir(f);   	
			dynamicimagesparent();
		}
			 dynamicimageschild();

}
private void dynamicimageschild() {
	
	lnrlayoutchild.removeAllViews();
	if((pieces3==null))
	{
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayoutchild.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 15;
		l2.addView(lchkbox);
		
		
		TextView tvnorecordchild = new TextView(this);
		tvnorecordchild.setText("No Records Found");
		//tvnorecordchild.setTextSize(16);
		tvnorecordchild.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
		tvnorecordchild.setTextColor(Color.BLACK);
		tvnorecordchild.setMinimumWidth(200);
		lchkbox.addView(tvnorecordchild,paramschk);
	}
	
	else
	{	
		
		
		
		tvstatuschild = new TextView[pieces3.length];
		thumbviewimgchild = new ImageView[pieces3.length];
		chkboxchild = new CheckBox[pieces3.length];
		//imgview1 = new ImageView[pieces3.length];
		thumbnails1 = new Bitmap[pieces3.length];
	
		for (int i = 0; i < pieces3.length; i++) {
		
			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk1.topMargin = 15;
			lnrlayoutchild.addView(l2,paramschk1);
		
			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					50, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.leftMargin = 5;
			paramschk.topMargin = 5;
			paramschk.bottomMargin = 15;
			l2.addView(lchkbox);
			LinearLayout ltextbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
					300, ViewGroup.LayoutParams.WRAP_CONTENT);
			thumbviewimgchild[i] = new ImageView(this);
			chkboxchild[i] = new CheckBox(this);
			chkboxchild[i].setPadding(30, 0, 0, 0);
				lchkbox.addView(chkboxchild[i], paramschk);
				paramstext.topMargin = 5;
				paramstext.leftMargin = 15;
				l2.addView(ltextbox);
				
				
				LinearLayout ltextbox6= new LinearLayout(this);
				LinearLayout.LayoutParams paramstext6 = new LinearLayout.LayoutParams(
						75, 75);
				//paramstext6.topMargin = 5;
				paramstext6.leftMargin = 10;
				ltextbox.addView(ltextbox6);
				
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				try {
					BitmapFactory.decodeStream(new FileInputStream(titleheader.getText().toString()+pieces3[i]),
							null, o);
					j = "1";
				} catch (FileNotFoundException e) {
					j = "2";
				}
				if (j.equals("1")) {
					final int REQUIRED_SIZE = 100;
					int width_tmp = o.outWidth, height_tmp = o.outHeight;
					int scale = 1;
					while (true) {
						if (width_tmp / 2 < REQUIRED_SIZE
								|| height_tmp / 2 < REQUIRED_SIZE)
							break;
						width_tmp /= 2;
						height_tmp /= 2;
						scale *= 2;
					}
					// Decode with inSampleSize
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					Bitmap bitmap = null;
					try {
						bitmap = BitmapFactory.decodeStream(new FileInputStream(
								titleheader.getText().toString()+pieces3[i]), null, o2);
						
					
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					thumbviewimgchild[i].setImageDrawable(bmd);
					
				ltextbox6.addView(thumbviewimgchild[i], paramstext6);
				
				}
				LinearLayout ltextbox1= new LinearLayout(this);
				LinearLayout.LayoutParams paramstext1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramstext1.topMargin = 5;
				paramstext1.leftMargin = 15;
				ltextbox6.addView(ltextbox1);
				tvstatuschild[i] = new TextView(this);
				tvstatuschild[i].setText(pieces3[i]);
				tvstatuschild[i].setTextColor(Color.BLACK);
				//tvstatuschild[i].setTextSize(16);
				tvstatuschild[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
				tvstatuschild[i].setMinimumWidth(100);
				tvstatuschild[i].setTag("tvstatus" + i);
				thumbviewimgchild[i].setTag("tvstatus" + i);
				chkboxchild[i].setTag("tvstatus" + i);
				
				for(int j=0;j<selectedcount;j++)
				{
					if(selectedtestcaption[j].equals(titleheader.getText().toString()+pieces3[i]))
					{
						chkboxchild[i].setChecked(true);
					}
				}
				mapfolder.put("tvstatus" + i, tvstatuschild[i]);
				ltextbox1.addView(tvstatuschild[i], paramstext1);
				
			chkboxchild[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					String repidofselbtn = getidofselbtn.replace("tvstatus","");
					int chk = Integer.parseInt(repidofselbtn);
					final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
					textviewvalue_child = mapfolder.get(getidofselbtn);
					boolean bu = upr.common(titleheader.getText().toString()+textviewvalue_child.getText().toString());
					if (bu) {
							if(chkboxchild[chk].isChecked()==false)
							{
								selectedtestcaption=Delete_from_array(selectedtestcaption,titleheader.getText().toString()+textviewvalue_child.getText().toString());
								selectedcount=selectedcount-1;
							}
							else
							{
								Bitmap b=ShrinkBitmap(titleheader.getText().toString()+textviewvalue_child.getText().toString(), 400, 400);
								if(b!=null)
								{
									
									if(elev!=8)
									{
										
											if((maximumindb+selectedcount)<8)
											{
												Cursor c11=null;
												try
												{
													 c11 = cf.db
															.rawQuery(
																	"SELECT * FROM "
																			+ cf.ImageTable
																			+ " WHERE SrID='"
																			+ cf.Homeid
																			+ "' and Elevation='"
																			+ elev
																			+ "' and Delflag=0 and ImageName='"
																			+ cf.convertsinglequotes(titleheader.getText().toString()+textviewvalue_child.getText().toString())
																			+ "' order by ImageOrder",
																	null);
												
												}
												catch(Exception e)
												{
													//cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection in galary file multi selection page photos Error="+e.getMessage());
												}
													
																isexist = c11.getCount();
																
																if (isexist == 0) {
																	selectedtestcaption=dynamicarraysetting(titleheader.getText().toString()+pieces3[chk],selectedtestcaption);
																	selectedcount++;
																	
																} else {
																	
																	chkboxchild[chk].setChecked(false);
																	cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
																}
												}
												else
												{
													chkboxchild[chk].setChecked(false);
													cf.ShowToast("You are allowed to select only 8 Images per Elevation.",1);
												}
									}
									else
									{
										if((maximumindb+selectedcount)<20)
										{
											Cursor c11=null;
											try
											{
												 c11 = cf.db
														.rawQuery(
																"SELECT * FROM "
																		+ cf.ImageTable
																		+ " WHERE SrID='"
																		+ cf.Homeid
																		+ "' and Elevation='"
																		+ elev
																		+ "' and Delflag=0 and ImageName='"
																		+ cf.convertsinglequotes(titleheader.getText().toString()+textviewvalue_child.getText().toString())
																		+ "' order by ImageOrder",
																null);
											
											}
											catch(Exception e)
											{
												//cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection in galary file multi selection page photos Error="+e.getMessage());
											}
												
															isexist = c11.getCount();
															
															if (isexist == 0) {
																selectedtestcaption=dynamicarraysetting(titleheader.getText().toString()+pieces3[chk],selectedtestcaption);
																selectedcount++;
																
															} else {
																
																chkboxchild[chk].setChecked(false);
																cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
															}
											}
											else
											{
												chkboxchild[chk].setChecked(false);
												cf.ShowToast("You are allowed to select only 20 Images per Elevation.",1);
											}
									}
									
								}
								else
								{
									cf.ShowToast("This image is not a supported format.You can not upload.",1);
									chkboxchild[chk].setChecked(false);
								}
							}
					}else {
						chkboxchild[chk].setChecked(false);
						cf.ShowToast("Your file size exceeds 2MB.",1);

				}
				}
				
			
				});
				thumbviewimgchild[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					repidofselbtn1= getidofselbtn.replace("tvstatus","");
					cvrtstr1 = Integer.parseInt(repidofselbtn1) + 1;
					textviewvalue_child = mapfolder.get(getidofselbtn);
					
					//boolean bu = upr.common(textviewvalue_child.getText().toString());
					//if (bu) {
					if ((titleheader.getText().toString().endsWith(".jpg"))||(titleheader.getText().toString().endsWith(".JPG"))||(titleheader.getText().toString().endsWith(".jpeg"))||(titleheader.getText().toString().endsWith(".png"))||(titleheader.getText().toString().endsWith(".gif")
			   				 ||(titleheader.getText().toString().endsWith(".JPEG"))||(titleheader.getText().toString().endsWith(".PNG")))||(titleheader.getText().toString().endsWith(".gif"))||(titleheader.getText().toString().endsWith(".bmp"))){
						
						
						String[] bits = titleheader.getText().toString().split("/");	
						String picname = bits[bits.length - 1];
						String val = titleheader.getText().toString().replace(picname, textviewvalue_child.getText().toString());
						
						titleheader.setText(val);
					
					}
					else
					{
						if(titleheader.getText().toString()=="mnt/sdcard/")
						{
							titleheader.setText(titleheader.getText().toString()+textviewvalue_child.getText().toString());
						}
						else
						{
							titleheader.setText(titleheader.getText().toString()+textviewvalue_child.getText().toString());
						}
							
					}
					
					
					dialogfullimage = new Dialog(FrontElevation.this);
					dialogfullimage.setContentView(R.layout.alertfullimage);
					dialogfullimage.setTitle("Select Images");
					dialogfullimage.setCancelable(true);
	    	 		fullimageview = (ImageView) dialogfullimage.findViewById(R.id.full_image);
	    	 		selectfullimage = (Button) dialogfullimage.findViewById(R.id.selectBtnfullimage);
	    	 		cancelfullimage = (Button) dialogfullimage.findViewById(R.id.cancelBtnfullimage);
	    	 		imagepath = (TextView) dialogfullimage.findViewById(R.id.imagepath);
	    	 		imagepath.setText(titleheader.getText().toString());
	    	 		
	    	 		Bitmap b = ShrinkBitmap(titleheader.getText().toString(), 130, 130);
	    	 		fullimageview.setImageBitmap(b);
	    	 		
	    			
	    			cancelfullimage.setOnClickListener(new OnClickListener() {

	    				public void onClick(View v) {

	    					//pieces1=null;pieces2=null;pieces3=null;
	    					String value = titleheader.getText().toString();
	    					String[] bits = value.split("/");
	    					String picname = bits[bits.length - 1];
	    					titleheader.setText(value.replace(picname, ""));
	    					dialogfullimage.setCancelable(true);
	    					dialogfullimage.cancel();
	    				}
	    			});
	    			selectfullimage.setOnClickListener(new OnClickListener() {
	    				public void onClick(View v) {
	    					boolean bu = upr.common(titleheader.getText().toString());
	    					if (bu) 
	    					{
	    						
	    						Bitmap b=ShrinkBitmap(titleheader.getText().toString(), 400, 400);
								if(b!=null)
								{
	    						
	    							if((maximumindb+selectedcount)<8)
	    							{
	    								Cursor c11=null;
	    								try
	    								{
	    									 c11 = cf.db
		    										.rawQuery(
		    												"SELECT * FROM "
		    														+ cf.ImageTable
		    														+ " WHERE SrID='"
		    														+ cf.Homeid
		    														+ "' and Elevation='"
		    														+ elev
		    														+ "' and Delflag=0 and ImageName='"
		    														+ cf.convertsinglequotes(titleheader.getText().toString())//+textviewvalue_child.getText().toString()
		    														+ "' order by ImageOrder",
		    												null);
	    								}catch(Exception e)
										{
											System.out.println("the error was"+e.getMessage());
											//cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection ="+e.getMessage());
										}
		    	    						
		    								isexist = c11.getCount();
		    								if (isexist == 0) {
		    									if(chkboxchild[Integer.parseInt(repidofselbtn1)].isChecked())
		    									{
		    										cf.ShowToast("You have already selected this image.",1);
		    									}
		    									else
		    									{
		    										selectedtestcaption=dynamicarraysetting(imagepath.getText().toString(),selectedtestcaption );
													selectedcount++;
													chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(true);
		    									}
		    							 
		    								} else {
		    									chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(false);
		    									cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
		    								}
	    							}
	    						else
	    						{
	    							chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(false);
	    							cf.ShowToast("You are allowed to select only 8 Images per Elevation.",1);
	    						
	    						}}
	    						
	
							}else {
								chkboxchild[chk].setChecked(false);
								cf.ShowToast("Your file size exceeds 2MB.",1);

							}
	    					String value = titleheader.getText().toString();
	    					String[] bits = value.split("/");
	    					String picname = bits[bits.length - 1];
	    					
	    					titleheader.setText(value.replace(picname, ""));
	    					dialogfullimage.setCancelable(true);
	    					dialogfullimage.cancel();
	    				}
	    			});
	    			dialogfullimage.setCancelable(false);
	    			dialogfullimage.show();
				}
				
			});
		}
		if(scr2!=null)
		{
			scr2.scrollTo(0, 0);
		
		}
		}
	
}
protected String[] Delete_from_array(String[] selectedtestcaption2,
		String txt) {
	// TODO Auto-generated method stub
	String tmp[]=new String[selectedtestcaption2.length-1];
	int j=0;
	if(selectedtestcaption2!=null)
	{
		for(int i=0;i<selectedtestcaption2.length;i++)
		{
			if(selectedtestcaption2[i]!=null)
			{
				if(!selectedtestcaption2[i].equals(txt))
				{
					tmp[j]=selectedtestcaption2[i];
					j++;
				}
				else
				{
					System.out.println("come in else");
				}
			}
		}
		
	}
	return tmp;
}
private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
	// TODO Auto-generated method stub
	 try
	 {
	if(pieces3==null)
	{
		pieces3=new String[1];
		
		pieces3[0]=ErrorMsg;
	}
	else
	{
		
		String tmp[]=new String[pieces3.length+1];
		int i;
		for(i =0;i<pieces3.length;i++)
		{
			
			tmp[i]=pieces3[i];
		}
	
		tmp[tmp.length-1]=ErrorMsg;
		pieces3=null;
		pieces3=tmp.clone();
	}
	 }
	 catch(Exception e)
	 {
		 System.out.println("Exception "+e.getMessage());
		
	 }
	return pieces3;
}
	private void nxtintent() {
		// TODO Auto-generated method stub
		btnshowcommon();
		selid = 0;
		btnbrwse.setVisibility(visibility);
		edbrowse.setText("");
		

		if (elev == 1) {
			elev = 2;
			name = "RE ";
			rgtimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			showimages();
		} else if (elev == 2) {
			elev = 3;
			name = "BE ";
			bacimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			showimages();
		} else if (elev == 3) {
			elev = 4;
			name = "LE ";
			lftimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			
			showimages();
		} else if (elev == 4) {
			elev = 5;
			name = "AE ";
			atcimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			
			showimages();
		} else if (elev == 5) {
			elev = 8;
			name = "Additional Photo ";
			additionalimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			
			showimages();
		} else if (elev == 8) {
			Intent fb1 = new Intent(FrontElevation.this,
					FeedbackDocuments.class);
			fb1.putExtra("homeid", cf.Homeid);
			fb1.putExtra("InspectionType", cf.InspectionType);
			fb1.putExtra("status", cf.status);
			fb1.putExtra("keyName", cf.value);
			fb1.putExtra("Count", cf.Count);
			startActivity(fb1);
		}
	}

	private String getImageDesc(int elev2, String homeid2) {
		// TODO Auto-generated method stub
		if (elev2 == 1) {
			imgtitle = "Front Elevation";
		} else if (elev2 == 2) {
			imgtitle = "Right Elevation";
		} else if (elev2 == 3) {
			imgtitle = "Back Elevation";
		} else if (elev2 == 4) {
			imgtitle = "Left Elevation";
		} else if (elev2 == 5) {
			imgtitle = "Attic Elevation";
		} else if (elev2 == 8) {
			imgtitle = "Additional Photo";
		}

		Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.ImageTable
				+ " WHERE SrID='" + homeid2 + "' and Elevation='" + elev2
				+ "' and Delflag=0 order by CreatedOn DESC", null);
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			ImgOrder = imgrws + 1;
		} else {
			ImgOrder = imgrws + 1;

		}
		ImgDesc = imgtitle + " Elevation " + ImgOrder;
		return ImgDesc;
	}

	public int getImageOrder(int Elevationtype, String srid) {
		Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.ImageTable
				+ " WHERE SrID='" + srid + "' and Elevation='" + Elevationtype
				+ "' and Delflag=0 order by CreatedOn DESC", null);
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			ImgOrder = imgrws + 1;
		} else {
			ImgOrder = imgrws + 1;

		}
		return ImgOrder;
	}

	public void next() {
		Intent intimg1 = new Intent(FrontElevation.this, FrontElevation.class);
		intimg1.putExtra("homeid", cf.Homeid);
		intimg1.putExtra("elevation", elev);
		startActivity(intimg1);
	}

	public void showimages() {
		
		sgnpath = "";
		pathdesc = "";
		try {
			Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "' and Delflag=0 order by ImageOrder", null);
			maxlevel = rws = c11.getCount();
			System.out.println("show images "+elev);
			int rem;
			if(elev==8)
			{
				 rem = 20 - rws;	
			}
			else
			{
				rem = 8 - rws;
			}
			
			arrpath = new String[rws];
			arrpathdesc = new String[rws];
			arrimgord = new String[rws];
			String source = "<b><font color=#000000> Number of uploaded images : "
					+ "</font><font color=#DAA520>"
					+ rws
					+ "</font><font color=#000000> , Remaining : </font><font color=#DAA520>"
					+ rem + "</font></b>";
			txthdr.setText(Html.fromHtml(source));
			if (rws == 0) {

				lnrlayout.removeAllViews();
				firsttxt.setVisibility(v1.GONE);
				fstimg.setVisibility(v1.GONE);
				updat.setVisibility(v1.GONE);
				del.setVisibility(v1.GONE);
			} else {
				int Column1 = c11.getColumnIndex("ImageName");
				int Column2 = c11.getColumnIndex("Description");
				int Column3 = c11.getColumnIndex("ImageOrder");
				c11.moveToFirst();
				if (c11 != null) {
					int i = 0;
					do {						
						commonfunction(cf.getsinglequotes(c11.getString(Column3)), i);
							arrpath[i] = cf.getsinglequotes(c11.getString(Column1));
							arrpathdesc[i] = cf.getsinglequotes(c11.getString(Column2));
							arrimgord[i] = cf.getsinglequotes(c11.getString(Column3));
							
							ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));								
						i++;
					} while (c11.moveToNext());System.out.println("inside show images");
					dynamicview();System.out.println("inside dynamicview images");
					
				}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation("Problem in selecting caption and the images in the elevation "+elev + "where error = "+e.getMessage());
		}
	}

	private void dynamicview() throws FileNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("came here");
		lnrlayout.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lnrlayout.addView(sv);

		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		l1.setMinimumWidth(925);System.out.println("arrpath here"+arrpath.length);
		
		tvstatus = new TextView[arrpath.length];
		elevationimage = new ImageView[arrpath.length];
		spnelevation = new Spinner[arrpath.length];
		for (int i = 0; i < arrpath.length; i++) {
			
			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk1.topMargin = 20;
			l1.addView(l2,paramschk1);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(175, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 0;
			paramschk.leftMargin = 10;
			l2.addView(lchkbox);

		
			tvstatus[i] = new TextView(this);
			tvstatus[i].setMinimumWidth(175);
			tvstatus[i].setMaxWidth(175);
			tvstatus[i].setText(arrpathdesc[i]);
			tvstatus[i].setTextColor(Color.BLACK);
			//tvstatus[i].setTextSize(16);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			lchkbox.addView(tvstatus[i], paramschk);
			tvstatus[i].setTag("imagechange" + i);
			
			System.out.println("arrimagechange.length");
			tvstatus[i].setOnClickListener(new View.OnClickListener() {

 				public void onClick(final View v) {
 					String getidofselbtn = v.getTag().toString();
 					final String repidofselbtn = getidofselbtn.replace(
 							"imagechange", "");
 					final int cvrtstr = Integer.parseInt(repidofselbtn);
 					String selpath = arrpath[cvrtstr];
 					String seltxt = arrpathdesc[cvrtstr];
 				
 					Cursor c11 = cf.db.rawQuery("SELECT ImageOrder FROM "
 							+ cf.ImageTable + " WHERE SrID='" + cf.Homeid
 							+ "' and Elevation='" + elev + "' and ImageName='"
 							+cf.convertsinglequotes(arrpath[cvrtstr]) + "'",
 							null);
 					
 					c11.moveToFirst();
 					imgnum = c11.getString(c11.getColumnIndex("ImageOrder"));
 					String a;
 					chk = 1;
 					
 					dispfirstimg(cvrtstr,seltxt);
 					

 				}
 			});
			
			String j = "0";
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			try {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[i]),
						null, o);
				j = "1";
			} catch (FileNotFoundException e) {
				j = "2";
			}
			if (j.equals("1")) {
				final int REQUIRED_SIZE = 100;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				
				// Decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						arrpath[i]), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);

				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(
						100, 50);
				paramselevimg.topMargin = 5;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);
				
				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageDrawable(bmd);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
			}
			else
			{
				Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(100, 50);
				paramselevimg.topMargin = 10;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);

				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageBitmap(bmp);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
				
			}
			try
			{
			LinearLayout lspinner = new LinearLayout(this);
			LinearLayout.LayoutParams paramsspinn = new LinearLayout.LayoutParams(
					200, 50);
			paramsspinn.topMargin = 5;
			paramsspinn.leftMargin = 20;
			l2.addView(lspinner);
			System.out.println("elevationimage.length");
			elevationimage[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(final View v) {

					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn = getidofselbtn.replace("imagechange", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn);
					String selpath = arrpath[cvrtstr];
					String seltxt = arrpathdesc[cvrtstr];System.out.println("seltxt "+seltxt);
					Cursor c11 = cf.db.rawQuery("SELECT ImageOrder FROM "
							+ cf.ImageTable + " WHERE SrID='" + cf.Homeid
							+ "' and Elevation='" + elev + "' and ImageName='"
							+ cf.convertsinglequotes(arrpath[cvrtstr]) + "' and Delflag=0",
							null);
					c11.moveToFirst();
					imgnum = c11.getString(c11.getColumnIndex("ImageOrder"));
					String a;
					chk = 1;
					dispfirstimg(cvrtstr,seltxt);

				}
			});
			int n=i+1;System.out.println("arrpath herength");
			String[] SH_IM_Elevation = elevationcaption_map.get("spiner" + i);
			spnelevation[i] = new Spinner(this);
			spnelevation[i].setId(i);
			ArrayAdapter adapter2 = new ArrayAdapter(FrontElevation.this,android.R.layout.simple_spinner_item, SH_IM_Elevation);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnelevation[i].setAdapter(adapter2);
			lspinner.addView(spnelevation[i], paramsspinn);
			spnelevation[i].setOnItemSelectedListener(new MyOnItemSelectedListener1(i));
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("ececption"+e.getMessage());
			}
		}

	}

	public class MyOnItemSelectedListener1 implements OnItemSelectedListener {
		public final int spinnerid;

		MyOnItemSelectedListener1(int id) {
			this.spinnerid = id;
		}

		public void onItemSelected(AdapterView<?> parent, View view,final int pos,
				long id) {
			System.out.println("POS "+pos);
			if (pos == 0) {
			} else {
				try {
					
					
					final int j = spinnerid + 1;					
					final String[] photocaption = elevationcaption_map.get("spiner" + spinnerid);
					
					String[] bits = arrpath[spinnerid].split("/");
					picname = bits[bits.length - 1];
					
					if(photocaption[pos].equals("ADD PHOTO CAPTION"))
					{
						final AlertDialog.Builder alert = new AlertDialog.Builder(FrontElevation.this);
						alert.setTitle("ADD PHOTO CAPTION");
						input = new EditText(FrontElevation.this);
						input.setFocusable(false);
						input.setFocusableInTouchMode(true);
						alert.setView(input);
						input.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
						input.setWidth(800);
						
						cf.hidekeyboard();
						InputFilter[] FilterArray = new InputFilter[1];  
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);  
						input.setFilters(FilterArray);  
						System.out.println("setFilters er alte");
						input.setOnTouchListener(new OnTouchListener() {
							
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub
								cf.setFocus((EditText)v);
								return false;
							}
						});
						
						alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								String commentsadd = input.getText().toString();
								if (!"".equals(commentsadd.trim())) {
								cf.db.execSQL("INSERT INTO "
											+ cf.tblphotocaption
											+ " (fld_photocaption,fld_elevtype,fld_elevid)"
											+ " VALUES ('" + cf.convertsinglequotes(commentsadd) + "','" + name + "','" + j + "')");
									
									cf.ShowToast("Photo Caption added successfully.",1);
									 InputMethodManager imm = (InputMethodManager)getSystemService(
										      Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
									showimages();
									
								} else {
									ShowToast toast = new ShowToast(getBaseContext(),
											"Please enter photo caption. It can't be empty.");
									InputMethodManager imm = (InputMethodManager)getSystemService(
										      Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
									spnelevation[spinnerid].setSelection(0);
								}
							}
						});

						alert.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									
									public void onClick(DialogInterface dialog, int whichButton) {
										dialog.cancel();
										spnelevation[spinnerid].setSelection(0);
										cf.hidekeyboard();System.out.println("Cameh er alte");
										input.setFocusable(false);
										input.setFocusableInTouchMode(true);System.out.println("Cameh alte");
									}
								});
						alert.setCancelable(false);
						alert.show();
						
					}
					else
					{
						
						final Dialog dialog = new Dialog(FrontElevation.this);
				        dialog.setContentView(R.layout.alertfront);
						dialog.setTitle("Choose Option");
			            dialog.setCancelable(false);
			            final EditText edit_desc = (EditText) dialog.findViewById(R.id.edittxtdesc);
			          
			            Button button_close = (Button) dialog.findViewById(R.id.Button05);
			            Button button_sel = (Button) dialog.findViewById(R.id.Button01);
			    		Button button_edit = (Button) dialog.findViewById(R.id.Button02);
			    		InputFilter[] FilterArray = new InputFilter[1];  
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);  
						edit_desc.setFilters(FilterArray); 
			    		
			    		Button button_del = (Button) dialog.findViewById(R.id.Button04);
			    		final Button button_upd = (Button) dialog.findViewById(R.id.Button03);
			    		
			    		button_close.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	dialog.cancel();
				           	spnelevation[spinnerid].setSelection(0);
				            }
			    		});
			    		
			    		button_sel.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	dialog.cancel();
				            	try{
				            	cf.db.execSQL("UPDATE " + cf.ImageTable
										+ " SET Description='"
										+ cf.convertsinglequotes(photocaption[pos])
										+ "',Nameext='" + cf.convertsinglequotes(picname)
										+ "',ModifiedOn='" + md + "' WHERE SrID ='"
										+ cf.Homeid + "' and Elevation='" + elev
										+ "' and ImageOrder='" + arrimgord[spinnerid]
										+ "'");
				            	
				            }
			            	catch(Exception e)
			            	{
			            		System.out.println("issues"+e.getMessage());
			            		//cf.Error_LogFile_Creation("Problem in select the caption in the photos at the elevation "+elev+" error  ="+e.getMessage());
			            	}
				            	spnelevation[spinnerid].setSelection(0);
								tvstatus[spinnerid].setText(photocaption[pos]);
				            }
			    		});
			    		
			    		
			   			button_edit.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	
				            	edit_desc.setVisibility(v1.VISIBLE);
				            	edit_desc.setText(photocaption[pos]);
				            	
				            	button_upd.setVisibility(v1.VISIBLE);

				            	button_upd.setOnClickListener(new OnClickListener() {
						          public void onClick(View v) {
										if(!"".equals(edit_desc.getText().toString().trim()))
										{
											cf.db.execSQL("UPDATE " +cf. tblphotocaption + " set fld_photocaption = '"+cf.convertsinglequotes(edit_desc.getText().toString())+"' WHERE fld_elevtype ='" + name + "' and fld_elevid='"+j+"' and fld_photocaption='"+cf.convertsinglequotes(photocaption[pos])+"'");
							           
							             	showimages();
							   			    cf.ShowToast("Photo Caption has been updated sucessfully.",1);
							   			 InputMethodManager imm = (InputMethodManager)getSystemService(
											      Context.INPUT_METHOD_SERVICE);
											imm.hideSoftInputFromWindow(edit_desc.getWindowToken(), 0);
							   			    dialog.cancel();
							   			
										}
										else
										{
											cf.ShowToast("Please enter photo caption. It can't be empty.",1);
												spnelevation[spinnerid].setSelection(0);
												 InputMethodManager imm = (InputMethodManager)getSystemService(
													      Context.INPUT_METHOD_SERVICE);
													imm.hideSoftInputFromWindow(edit_desc.getWindowToken(), 0);
										}
						            }
					    		});
				            }
			    		});
			    		
			    		button_del.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				           	AlertDialog.Builder builder = new AlertDialog.Builder(FrontElevation.this);
				   			builder.setMessage("Are you sure, Do you want to delete?")
			   			       .setCancelable(false)
			   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			        	 cf.db.execSQL("Delete From " +cf. tblphotocaption + "  WHERE fld_photocaption='"+cf.convertsinglequotes(photocaption[pos])+"'");
			   			        	
									 showimages();
									 cf.ShowToast("Photo Caption has been deleted sucessfully.",1);
									// cf.hidekeyboard();
									 InputMethodManager imm = (InputMethodManager)getSystemService(
										      Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(edit_desc.getWindowToken(), 0);

			   			           }
			   			       })
			   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			                dialog.cancel();
			   			             
			   			             spnelevation[spinnerid].setSelection(0);
			   			          cf.hidekeyboard();
			   			           }
			   			       }); 
				            
				            builder.show();
				            dialog.cancel();
				            }
			    		});
			    		dialog.show();
					}
				} catch (Exception e) {
					System.out.println("e err + " + e.getMessage());
				}

			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
			
		}
	}


	public void dispfirstimg(final int selid,String photocaptions) {
		
		//Starts
		System.out.println("photocaptions"+photocaptions);
		System.out.println("selid"+selid);
		 System.gc();
		currnet_rotated=0;
		
		delimagepos = selid;
		phtodesc = photocaptions;

		String k;
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		final Dialog dialog1 = new Dialog(FrontElevation.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);System.out.println("maintable");
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1
				.findViewById(R.id.updateimage);
		Reup.setVisibility(View.VISIBLE);
		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */

		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
		final EditText upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
		upd_Ed.setFocusable(false);
		//upd_Ed.setFocusableInTouchMode(true);
		Button btn_helpclose = (Button) dialog1
				.findViewById(R.id.imagehelpclose);
		Button btn_up = (Button) dialog1.findViewById(R.id.update);
		Button btn_del = (Button) dialog1.findViewById(R.id.delete);
		Button rotateleft = (Button) dialog1.findViewById(R.id.rotateleft);
		Button rotateright = (Button) dialog1.findViewById(R.id.rotateright);
		System.out.println("rotateright");
		 upd_Ed.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 
				cf.setFocus(upd_Ed);
				return false;
			}
		});
		// update and delete button function

		 System.out.println("updstr ");
		btn_up.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				updstr = upd_Ed.getText().toString();

				try {
					if (!updstr.trim().equals("")) {
						dialog1.setCancelable(true);
						dialog1.dismiss();
						cf.db.execSQL("UPDATE " + cf.ImageTable
								+ " SET Description='"
								+ cf.convertsinglequotes(updstr)
								+ "',ModifiedOn='' WHERE SrID ='"
								+ cf.Homeid
								+ "' and Elevation='" + elev
								+ "' and ImageOrder='" + imgnum + "'");
						cf.ShowToast("Updated sucessfully.", 1);

					} else {
						cf.ShowToast("Please enter the caption.", 1);
						 InputMethodManager imm = (InputMethodManager)getSystemService(
							      Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(upd_Ed.getWindowToken(), 0);
					}
					/**Save the rotated value in to the external stroage place **/
					System.out.println("curr "+currnet_rotated);
					if(currnet_rotated>0)
					{ 
						System.out.println("greater a ");
						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							  ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  FrontElevation.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							if(current!=null)
							{
								System.out.println("greater anot null ");
							String path=getPath(Uri.parse(current));
							File fout = new File(arrpath[selid]);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(arrpath[selid]));
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
				} catch (Exception e) {
					System.out.println("erre " + e.getMessage());
				}
				/**Save the rotated value in to the external stroage place ends **/
				//commonfunction();
				showimages();

			}

		});
		
		btn_del.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				AlertDialog.Builder builder = new AlertDialog.Builder(FrontElevation.this);
				builder.setMessage(
						"Are you sure, Do you want to delete the image?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										try {
											Cursor or = cf.db.rawQuery(
													"select ImageOrder from "
															+ cf.ImageTable
															+ " WHERE  SrID ='"
															+ cf.Homeid
															+ "' and Elevation='"
															+ elev
															+ "' and ImageName='"
															+ cf.convertsinglequotes(arrpath[delimagepos])
															+ "'", null);
											or.moveToFirst();
											int image_order = or.getInt(or
													.getColumnIndex("ImageOrder"));

											cf.db.execSQL("UPDATE "
													+ cf.ImageTable
													+ " SET Delflag=1 WHERE SrID ='"
													+ cf.Homeid
													+ "' and Elevation='"
													+ elev
													+ "' and ImageName='"
													+ cf.convertsinglequotes(arrpath[delimagepos])
													+ "'");
											
											cf.db.execSQL("DELETE FROM "
													+ cf.ImageTable 
													+ " WHERE SrID ='"
													+ cf.Homeid
													+ "' and Elevation='"
													+ elev
													+ "' and ImageName='"
													+ cf.convertsinglequotes(arrpath[delimagepos])
													+ "'");
											
											
										
										
										Cursor c3 = cf
												.SelectTablefunction(
														cf.ImageTable,
														" WHERE SrID ='"
																+ cf.Homeid
																+ "' and Elevation='"
																+ elev
																+ "' and Delflag=0");
										
										for (int m = image_order; m <= c3
												.getCount(); m++) {
											int k = m + 1;
											cf.db.execSQL("UPDATE "
													+ cf.ImageTable
													+ " SET ImageOrder='"
													+ m
													+ "' WHERE SrID ='"
													+ cf.Homeid
													+ "' and Elevation='"
													+ elev
													+ "' and ImageOrder='"
													+ k + "'");

										}
										
											/*if (image_order < 8 && image_order > 0) {
												for (int m = image_order; m <= 8; m++) {
													int k = m + 1;
													cf.db.execSQL("UPDATE "
															+ cf.ImageTable
															+ " SET ImageOrder='"
															+ m + "' WHERE SrID ='"
															+ cf.Homeid
															+ "' and Elevation='"
															+ elev
															+ "' and ImageOrder='"
															+ k + "'");

												}
											}*/


										} catch (Exception e) {
											System.out.println("exception e  "
													+ e);
										}
										cf.ShowToast("Image has been deleted sucessfully.",	1);

										chk = 0;
										try {
											Cursor c11 = cf.db.rawQuery(
													"SELECT * FROM "
															+ cf.ImageTable
															+ " WHERE SrID='"
															+ cf.Homeid
															+ "' and Elevation='"
															+ elev
															+ "' and Delflag=0",
													null);
											int delchkrws = c11.getCount();
											if (delchkrws == 0) {
												if (elev == 1) {
													try {

														cf.db.execSQL("UPDATE "
																+ cf.SubmitCheckTable
																+ " SET fld_front='0' WHERE Srid ='"
																+ cf.Homeid
																+ "' and InspectorId='"
																+ cf.InspectorId
																+ "'");

													} catch (Exception e) {
														System.out.println("Exception= "
																+ e.getMessage());
													}
													frnttick.setVisibility(v1.GONE);
												} else if (elev == 2) {
													rgttick.setVisibility(v1.GONE);
												} else if (elev == 3) {
													bactick.setVisibility(v1.GONE);
												} else if (elev == 4) {
													lfttick.setVisibility(v1.GONE);
												} else if (elev == 5) {
													atctick.setVisibility(v1.GONE);
												} else if (elev == 8) {
													addittick
															.setVisibility(v1.GONE);
												}
											}


										} catch (Exception e) {

										}
										showimages();

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.show();
			}
			
		});
		btn_helpclose.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
		});System.out.println("c11 is rotateright");
		rotateright.setOnClickListener(new OnClickListener() {  	
			
			public void onClick(View v) {

				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				Bitmap myImg = null;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b); 
 
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}
			
			}
		});System.out.println("c11 is rotateleft");
		rotateleft.setOnClickListener(new OnClickListener() {  			
			public void onClick(View v) {

				// TODO Auto-generated method stub
			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<0)
				{
					currnet_rotated=270;
				}

				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}

			
			}
		});
		System.out.println("finally");
		// update and delete button function ends
		try {
			if (chk == 1) {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]),
						null, o);
			} else {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[0]),
						null, o);
			}
			k = "1";

		} catch (FileNotFoundException e) {
			k = "2";

		}
		System.out.println("finwhat is k");
		if (k.equals("1")) {
			final int REQUIRED_SIZE = 400;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap = null;
			System.out.println("bitmap is k");
			try {
				Cursor c11 = cf.db.rawQuery(
						"SELECT * FROM " + cf.ImageTable + " WHERE SrID='"
								+ cf.Homeid + "' and Elevation='" + elev
								+ "' and ImageName='"
								+ cf.convertsinglequotes(arrpath[selid])
								+ "' and Delflag=0", null);

				c11.moveToFirst();
				System.out.println("c11 is k"+c11.getCount());
				int Column1 = c11.getColumnIndex("Description");
				description = cf.getsinglequotes(c11.getString(Column1));


				if (chk == 1) {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[selid]), null, o2);
					upd_Ed.setText(description);

				} else {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[0]), null, o2);
					upd_Ed.setText(cf.getsinglequotes(arrpathdesc[0]));
				}
			} catch (FileNotFoundException e) {
				System.out.println("sdsdsdsdsd "+e.getMessage()); 
				
				e.printStackTrace();
			}
			rotated_b=bitmap;
			BitmapDrawable bmd = new BitmapDrawable(bitmap);
			
			upd_img.setImageDrawable(bmd);

			dialog1.setCancelable(false);
			dialog1.show();
		} else {
			dialog1.setCancelable(false);
			dialog1.show();
		}
		upd_Ed.clearFocus();
		//Ends
	}

	private class watcher implements TextWatcher {

		int inde;

		public watcher(int i) {
			// TODO Auto-generated constructor stub
			this.inde = i;
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			str[this.inde] = s.toString();
				
		}
	};

	public class ImageAdapter1 extends BaseAdapter {
		private LayoutInflater mInflater;

		public ImageAdapter1() {
			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return count1;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder1 holder;
			if (convertView == null) {
				holder = new ViewHolder1();
				convertView = mInflater.inflate(R.layout.updateorder, null);
				holder.imageview = (ImageView) convertView
						.findViewById(R.id.thumbImage1);
				holder.edit = (EditText) convertView
						.findViewById(R.id.itemedit);
				holder.r = (RelativeLayout) convertView.findViewById(R.id.Rid);

				convertView.setTag(holder);
				holder.edit.setId(position);
				et[position] = holder.edit;

				et[position].addTextChangedListener(new watcher(position));
				System.out.println("image order "+imageorder[position]);
				et[position].setText(String.valueOf(imageorder[position]));
				holder.imageview.setId(position);
				holder.r.setId(position);
				Bitmap b = thumbnails1[position];
				holder.imageview.setImageBitmap(b);
				holder.id = position;
			} else {

				holder = (ViewHolder1) convertView.getTag();

			}

			return convertView;

		}

	}

	class ViewHolder1 {
		ImageView imageview;
		EditText edit;
		RelativeLayout r;
		int id;
		int k = 0;

	}


	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (t == 0) {
			//System.out.println("onActii"+resultCode + " reques "+requestCode+ " getdata"+data.getData());
			if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					try
					{
					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);

					String[] bits = selectedImagePath.split("/");
					picname = bits[bits.length - 1];
					}
					catch(Exception e)
					{
						System.out.println("EEE "+e.getMessage());
					}
					
				}
			} else {
				selectedImagePath = "";
				edbrowse.setText("");
			}
		} else if (t == 1) {
			switch (resultCode) {
			case 0:
				selectedImagePath = "";

				break;
			case -1:
				/*System.out.println("case 111");
				if (data.equals(null))
				{
					System.out.println("case 1data"+data);
					cf.ShowToast("Intent is null.",1);	
				}
				else
				{
					System.out.println("case 1data not nill"+data);*/
					try {
	
						String[] projection = { MediaStore.Images.Media.DATA };
						Cursor cursor = managedQuery(mCapturedImageURI, projection,
								null, null, null);
						int column_index_data = cursor
								.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
						cursor.moveToFirst();
						capturedImageFilePath = cursor.getString(column_index_data);
						selectedImagePath = capturedImageFilePath;
					
						display_taken_image(selectedImagePath);
						showimages();
					} catch (Exception e) {
						System.out.println("displs "+e.getMessage());
						//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ FrontElevation.this +" problem in inserting images datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}
				//}
				break;

			}

		}

	}

	private void displayphoto() {

		try {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(selectedImagePath),
					null, o);

			final int REQUIRED_SIZE = 100;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap2 = BitmapFactory.decodeStream(new FileInputStream(
					selectedImagePath), null, o2);
			BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
			fstimg.setImageDrawable(bmd2);

		} catch (FileNotFoundException e) {
		}

	}

	public String getPath(Uri uri) {
		System.out.println("uri anot null ");
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		
		
		String[] projection1 = { MediaStore.Images.Media.ORIENTATION};
		Cursor cursor1 = managedQuery(uri, projection1, null, null, null);
		int column_index1 = cursor1
				.getColumnIndexOrThrow(MediaStore.Images.Media.ORIENTATION);
		cursor1.moveToFirst();
		System.out.println("col "+cursor1.getInt(column_index1) + " "+curr_orientation);
		cursor.moveToFirst();
		
		return cursor.getString(column_index);
	}

	private void btnshowcommon() {

		signimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		frntimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		bacimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		rgtimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		lftimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		atcimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		additionalimg.setBackgroundResource(R.drawable.submenusrepeatnor);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			Intent intimg = new Intent(FrontElevation.this, ImagesData.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	Bitmap ShrinkBitmap(String file, int width, int height) {
		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			return null;
		}

	}
	public void run() {
		Looper.prepare();
		try{		// TODO Auto-generated method stub
			File images = Environment.getExternalStorageDirectory(); 
			walkdir(images);
		    handler.sendEmptyMessage(0);

 		}
 		catch(Exception e)
 		{
 			System.out.println("problem occure " + e.getMessage());
 		}
	}
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

	 			dialog = new Dialog(FrontElevation.this);
	 			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

	 			dialog.setContentView(R.layout.showfolderlist);
	 	 		
	 	 		dialog.setCancelable(true);
	 	 		
	 	 		lnrlayoutparent = (LinearLayout) dialog.findViewById(R.id.linscrollviewfolder);
		 		lnrlayoutchild = (LinearLayout) dialog.findViewById(R.id.linscrollviewimages);
		 		backfolderwise = (Button) dialog.findViewById(R.id.backfolderwise);
		 		cancelfolderwise = (Button) dialog.findViewById(R.id.cancelfolderwise);
		 		
		 		
		 		Lin_Pre_nex = (RelativeLayout) dialog.findViewById(R.id.linscrollview_Next);
		 		prev =(ImageView) dialog.findViewById(R.id.S_I_Prev);
	 	 		prev.setVisibility(View.GONE);
	 	 		next =(ImageView) dialog.findViewById(R.id.S_I_Next);
	 	 		scr2 =(ScrollView) dialog.findViewById(R.id.scr2);
	 	 		Rec_count_lin =(RelativeLayout) dialog.findViewById(R.id.Rec_count_lin);
	 	 		Total_Rec_cnt =(TextView) dialog.findViewById(R.id.Total_rec_cnt);
	 	 		showing_rec_cnt =(TextView) dialog.findViewById(R.id.showing_rec_cnt);
	 	 		
	 	 		ImageView Search = (ImageView) dialog.findViewById(R.id.search);
	 	 		Search.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						// TODO Auto-generated method stub
						 //final Dialog dialog1 = new Dialog(photos.this);
						/**Search based on the folder name starts here **/
						Search_by_folder(); 
		    	       /**Search based on the folder name Ends here**/
		    			
					}
				});
	 	 		if(Currentfile_list.length<=20)
	 	 		{
	 	 			Lin_Pre_nex.setVisibility(View.GONE);
	 	 			Rec_count_lin.setVisibility(View.VISIBLE);
	 	 			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
	 	 			showing_rec_cnt.setText("");
	 	 			
	 	 		}
	 	 		else
	 	 		{
	 	 			Lin_Pre_nex.setVisibility(View.VISIBLE);
	 	 			Rec_count_lin.setVisibility(View.VISIBLE);
	 	 			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
	 	 			showing_rec_cnt.setText("Records Displayed from 1 to 20");
	 	 		}
	 	 		prev.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int end =limit_start;
						
						limit_start-=20;
						
						if(limit_start>=0)
						{
						int start =limit_start;
						showing_rec_cnt.setText("Records Displayed from "+(start+1) +" to "+end);
						if((limit_start)<=0)
						{
							prev.setVisibility(View.GONE);
						}
						else
						{
							prev.setVisibility(View.VISIBLE);
						}
						if((limit_start+20)<=Currentfile_list.length)
						{
							next.setVisibility(View.VISIBLE);
						}
						else
						{
							next.setVisibility(View.GONE);
						}	
						if (Currentfile_list != null)
						   {
							  int j=0; 
							   
							
							 
							  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
							  if(limit_start>=0)
							  {
								  pieces3=new String[20];
							   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
								   
								 
					    		
					    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
					            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
					    	            	 
					    			 		 pieces3[j]=Currentfile_list[i].getName();
					    	            
					            		 }
					    		 
						   }
							  }  
						 }
						
						dynamicimagesparent();					
						 dynamicimageschild();
					}
						else
						{
							limit_start+=20;
							end=limit_start+20;
						}
					}
				});
	 	 		next.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						limit_start+=20;
						
						if(Currentfile_list.length>=limit_start)
						{
							
						int start =limit_start;
						int end=0;
						
						System.out.println("elese limit ="+limit_start+20 + " length "+Currentfile_list.length);
						
						if((limit_start+20)<Currentfile_list.length)
						{
							next.setVisibility(View.VISIBLE);
						
						}
						else
						{
							System.out.println("elese nect");
							next.setVisibility(View.GONE);
							
						}
						if((limit_start)<=0)
						{
							prev.setVisibility(View.GONE);
						}
						else
						{
							prev.setVisibility(View.VISIBLE);
						}
							
						if (Currentfile_list != null)
						   {
							  int j=0; 
							   
							
							 if(Currentfile_list.length>(limit_start+20))
							 {
								 pieces3=new String[20];
								 end =limit_start+20;
							 }
							 else
							 {
								 pieces3=new String[(Currentfile_list.length-limit_start)];
								 end =limit_start+( Currentfile_list.length-limit_start);
							 }
							  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
							  if(limit_start>=0)
							  {
								 
							   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
								   
								 
					    		
					    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
					            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
					    	            	 
					    			 		 pieces3[j]=Currentfile_list[i].getName();
					    	            
					            		 }
					    		 
						   }
							  }  
						 }
						
						showing_rec_cnt.setText("Records Displayed from "+(start+1) +" to "+end);
						dynamicimagesparent();
					
						 dynamicimageschild();
						}
						else
						{
							limit_start=limit_start-20;
						}
					}
				});
		 		titleheader = (TextView) dialog.findViewById(R.id.parenttitleheader);
		 		childtitleheader = (TextView) dialog.findViewById(R.id.child_titleheader);
		 		
		 		selectfolderwise = (Button) dialog.findViewById(R.id.selectfolderwise);
	 	 		pd.dismiss();
	 	 		
	 	 		if(dialog.isShowing())
	 	 		{
	 	 			dialog.setCancelable(true);
	 	 			dialog.cancel();
	 	 		}
	 	 			dialog.setCancelable(false);
	 	 			dialog.show();
	 		         dynamicimagesparent();
	 		    	 dynamicimageschild();
	 	 		
	 		    	cancelfolderwise.setOnClickListener(new OnClickListener() {
	 				public void onClick(View v) {
	 					if(!"".equals(finaltext)){
	 						finaltext="";
	 					}
	 					selectedcount=0;
	 	 				selectedtestcaption=null;		
	 	 				maximumindb=0;
	 	 				pieces1=null;
	 	 				limit_start=0;
	 	 				//pieces2=null;
	 	 				pieces3=null;
	 	 				dialog.setCancelable(true);
	 					dialog.cancel();
	 				}
	 	 		});
	 		    	backfolderwise.setOnClickListener(new OnClickListener() {
	 	 			public void onClick(View v) {
	 	 				if(!titleheader.getText().toString().equals("/mnt/sdcard/"))
	 	 				{
	 	 					backclick=1;
	 		 				pieces1=null;pieces2=null;pieces3=null;limit_start=0;
	 		 				
	 		 	 				String value = titleheader.getText().toString();
	 		 					String[] bits = value.split("/");	 		 					
	 		 					String picname = bits[bits.length - 1];
	 		 					if(value.equals("mnt/sdcard/"))
	 		 					{
	 		 						finaltext="";
	 		 						pathofimage  = value.replace(picname+"/", "");
	 		 					
	 		 					}
	 		 					else
	 		 					{
	 		 						String tempstr = value.substring(0,value.length()-1);
	 			 					int bits1 = tempstr.lastIndexOf("/");
	 			 					String pathofimage1=tempstr.substring(0,bits1+1);
	 		 						pathofimage  = pathofimage1;
	 		 						pathofimage = value.replace(picname+"/", "");
	 		 					}
	 		 					
	 					titleheader.setText(pathofimage);
	 					finaltext=pathofimage;
	 					File f = new File(pathofimage );
	 					
	 					
	 					String[] bits1 = pathofimage.split("/");
						String picname1 = bits1[bits1.length - 1];
						childtitleheader.setText(picname1); // set the chaild title
						
	 					walkdir(f);
	 					dynamicimagesparent();
	 					dynamicimageschild();
	 	 						}
	 	 				else
	 	 				{
	 	 					cf.ShowToast("You can not go back.",1);
	 	 				}
	 				}
	 			});

	 		    	selectfolderwise.setOnClickListener(new OnClickListener() {
	 	 			public void onClick(View v) {
	 	 				if(selectedcount!=0){
	 	 				for(int i=0;i<selectedcount;i++)
						{
	 	 					
	 	 					String[] bits = selectedtestcaption[i].split("/");
							picname = bits[bits.length - 1];
							
							System.out.println("INSERT "+"INSERT INTO "
									+ cf.ImageTable
									+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
									+ " VALUES ('"+ cf.Homeid + "','"+ quesid+ "','"+ elev+ "','"+ cf.convertsinglequotes(selectedtestcaption[i])+ "','"
									+ cf.convertsinglequotes(name + (maximumindb+i+1))+ "','"+ cf.convertsinglequotes(picname)+ "','" + cd + "','" + md + "','"
									+ (maximumindb+1+i) + "','" + 0 + "')");
							
							cf.db.execSQL("INSERT INTO "
									+ cf.ImageTable
									+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
									+ " VALUES ('"+ cf.Homeid + "','"+ quesid+ "','"+ elev+ "','"+ cf.convertsinglequotes(selectedtestcaption[i])+ "','"
									+ cf.convertsinglequotes(name + (maximumindb+i+1))+ "','"+ cf.convertsinglequotes(picname)+ "','" + cd + "','" + md + "','"
									+ (maximumindb+1+i) + "','" + 0 + "')");
							
						}
	 	 				if(!"".equals(finaltext)){
	 						finaltext="";
	 					}
	 	 				cf.ShowToast("Image saved successfully", 1);
						selectedcount=0;
	 	 				selectedtestcaption=null;		
	 	 				maximumindb=0;
	 	 				pieces1=null;
	 	 				pieces2=null;
	 	 				pieces3=null;
	 	 				if(!"".equals(finaltext)){
	 						finaltext="";
	 					}
	 	 				dialog.setCancelable(true);
	 					dialog.cancel();
	 					showimages();
	 					
	 					changeimage();
	 	 			}else
 	 				{
 	 					cf.ShowToast("Please select atleast one image to upload .", 1);
 	 				}
	 	 			}
	 	 		}); /**Select ends**/
        }
};
public void display_taken_image(final String slectimage)
{
	 System.gc();
	 globalvar=0;
	BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		final Dialog dialog1 = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
	dialog1.getWindow().setContentView(R.layout.alert);
	
	/** get the help and update relative layou and set the visbilitys for the respective relative layout */
	LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
	Re.setVisibility(View.GONE);
	LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
	Reup.setVisibility(View.GONE);
	LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
	camerapic.setVisibility(View.VISIBLE);
	TextView tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
	tvcamhelp.setText("Save Picture");
	tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
	 ((RelativeLayout)dialog1.findViewById(R.id.cam_photo_update)).setVisibility(View.VISIBLE);
	((RelativeLayout)dialog1.findViewById(R.id.camaddcaption)).setVisibility(View.GONE);
	((TextView) dialog1.findViewById(R.id.elevtxt)).setVisibility(View.GONE);
	((Spinner) dialog1.findViewById(R.id.cameraelev)).setVisibility(View.GONE);
	/** get the help and update relative layou and set the visbilitys for the respective relative layout */
	
	final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
	Button btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
	//((Button) dialog1.findViewById(R.id.camsave)).setVisibility(View.GONE);
	
	final Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
	System.out.println("slectimage pic"+slectimage);
	Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
	Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
	
	
	try {
			BitmapFactory.decodeStream(new FileInputStream(slectimage),
					null, o);
			final int REQUIRED_SIZE = 400;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
				BitmapFactory.Options o2 = new BitmapFactory.Options();
 			o2.inSampleSize = scale;
 			Bitmap bitmap = null;
 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
 	    			slectimage), null, o2);
 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
			   upd_img.setImageDrawable(bmd);
			   rotated_b=bitmap;
			}
}
	catch(Exception e)
	{
		
	}
	btn_save.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
        	if(globalvar==0)
    		{

        	globalvar=1;
        	
        	btn_save.setVisibility(v.GONE);
        	String[] bits = slectimage.split("/");
    		String picname = bits[bits.length - 1];
    		Cursor c15 = cf.db.rawQuery("SELECT * FROM "
					+ cf.ImageTable + " WHERE SrID='" + cf.Homeid
					+ "' and Elevation='" + elev + "' and Delflag=0 order by ImageOrder", null);
    		
    		
    		int count_tot = c15.getCount();
    		int ImgOrder = getImageOrder(elev, cf.Homeid);
    	
    		try {
    			cf.db.execSQL("INSERT INTO "
						+ cf.ImageTable
						+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
						+ " VALUES ('" + cf.Homeid + "','" + quesid + "','"
						+ elev + "','"
						+ cf.convertsinglequotes(slectimage) + "','"
						+ cf.convertsinglequotes(name + (count_tot + 1))
						+ "','" + cf.convertsinglequotes(picname) + "','"
						+ cd + "','" + md + "','" + ImgOrder + "','" + 0
						+ "')");
				if (elev == 1) {
					frnttick.setVisibility(visibility);
				} else if (elev == 2) {
					rgttick.setVisibility(visibility);
				} else if (elev == 3) {
					bactick.setVisibility(visibility);
				} else if (elev == 4) {
					lfttick.setVisibility(visibility);
				} else if (elev == 5) {
					atctick.setVisibility(visibility);
				} else if (elev == 8) {
					addittick.setVisibility(visibility);
				}
    		} catch (Exception e) {
System.out.println("Exception "+e.getMessage());
			}
    		/**Save the rotated value in to the external stroage place **/
			if(currnet_rotated>0)
			{ 

				try
				{
					/**Create the new image with the rotation **/
					String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
					 ContentValues values = new ContentValues();
					  values.put(MediaStore.Images.Media.ORIENTATION, 0);
					  FrontElevation.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
					
					
					if(current!=null)
					{
					String path=getPath(Uri.parse(current));
					File fout = new File(slectimage);
					fout.delete();
					/** delete the selected image **/
					File fin = new File(path);
					/** move the newly created image in the slected image pathe ***/
					fin.renameTo(new File(slectimage));
					
					
				}
				} catch(Exception e)
				{
					System.out.println("Error occure while rotate the image "+e.getMessage());
				}
				
			}
		
		/**Save the rotated value in to the external stroage place ends **/
			cf.ShowToast("Selected image saved successfully", 1);
			showimages();
			dialog1.setCancelable(true);
        	dialog1.dismiss();

    		}
        }
        
    });
	rotatecamright.setOnClickListener(new OnClickListener() {  
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			 System.gc();
			currnet_rotated+=90;
			if(currnet_rotated>=360)
			{
				currnet_rotated=0;
			}
			
			Bitmap myImg;
			try {
				myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
				Matrix matrix =new Matrix();
				matrix.reset();
				matrix.postRotate(currnet_rotated);
				rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
				System.gc();
				upd_img.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e)
			{
				System.out.println("rota eeee "+e.getMessage());
			}

			catch (OutOfMemoryError e) {
				System.out.println("comes in to out ot mem exception");
				System.gc();
				try {
					myImg=null;
					System.gc();
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					myImg= cf.ShrinkBitmap(slectimage, 800, 800);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b); 

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 catch (OutOfMemoryError e1) {
						// TODO Auto-generated catch block
					 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
				}
			}
		}
	});
	rotatecamleft.setOnClickListener(new OnClickListener() {  
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			 System.gc();
			 currnet_rotated-=90;
				if(currnet_rotated<0)
				{
					currnet_rotated=270;
				}	
			
			Bitmap myImg;
			try {
				myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
				Matrix matrix =new Matrix();
				matrix.reset();
				matrix.postRotate(currnet_rotated);
				rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
				System.gc();
				upd_img.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e)
			{
				System.out.println("rota eeee "+e.getMessage());
			}

			catch (OutOfMemoryError e) {
				System.out.println("comes in to out ot mem exception");
				System.gc();
				try {
					myImg=null;
					System.gc();
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					myImg= cf.ShrinkBitmap(slectimage, 800, 800);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b); 

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 catch (OutOfMemoryError e1) {
						// TODO Auto-generated catch block
					 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
				}
			}
		}
	});
	btn_helpclose.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
        	dialog1.setCancelable(true);
        	dialog1.dismiss();
        }
	});
	
	dialog1.setCancelable(false);
	dialog1.show();
}
/** File filter to get only the directories from the folder **/
FileFilter directoryFilter = new FileFilter() {
	public boolean accept(File file) {
		return file.isDirectory();
	}
};
/** File filter to get only the directories from the folder **/
/** File filter to get only the directories from the folder **/
FileFilter imageFilter = new FileFilter() {
	 
	public boolean accept(File file) {
		
		 if ((file.getName().endsWith(".jpg"))||(file.getName().endsWith(".jpeg"))||(file.getName().endsWith(".png"))||(file.getName().endsWith(".gif")||(file.getName().endsWith(".JPG"))
            				 ||(file.getName().endsWith(".JPEG"))||(file.getName().endsWith(".PNG")))||(file.getName().endsWith(".gif"))||(file.getName().endsWith(".bmp"))){
			 
			 
			 
			 return true;
		 }
		 else
		 {
			 return false;
		 }
			 
	}
};
/** File filter to get only the directories from the folder **/
protected void finalize() throws Throwable {
	System.gc();
};
public void Search_by_folder()
{
	 final Dialog dialog1 = new Dialog(FrontElevation.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		((LinearLayout) dialog1.findViewById(R.id.maintable)).setVisibility(View.GONE);
		((LinearLayout) dialog1.findViewById(R.id.Search_folder)).setVisibility(View.VISIBLE);
		dialog1.setCancelable(true);
		final Button selectBtn = (Button) dialog1.findViewById(R.id.SF_Search);
		Button cancelBtn = (Button) dialog1.findViewById(R.id.SF_cancel);
		Button Close = (Button) dialog1.findViewById(R.id.SF_close);
		final AutoCompleteTextView name_txt =(AutoCompleteTextView) dialog1.findViewById(R.id.SF_Name);
		final TextView finale_txt = (TextView) dialog1.findViewById(R.id.finale_txt);
		name_txt.setFocusable(false);
		name_txt.setText((titleheader.getText().toString().substring(12,titleheader.getText().toString().length())));
		name_txt.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				cf.setFocus(name_txt);
				name_txt.setSelection(name_txt.getText().length());
				return false;
			}
		});
		
		cancelBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//pieces1=null;pieces2=null;pieces3=null;
				
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		Close.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//pieces1=null;pieces2=null;pieces3=null;
				
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		selectBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) { 
				selectBtn.setVisibility(View.GONE);
				String Src_Txt =name_txt.getText().toString();
				//System.out.println("The selected folder name was "+name_txt.getText().toString()+"Sub string "+Src_Txt.substring(0, 11));
				System.out.println("The txt "+(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""));
				if(Src_Txt.equals(""))
				{
					titleheader.setText("/mnt/sdcard/");
					finaltext="/mnt/sdcard/";
					 images = new File("/mnt/sdcard/");
					//Currentfile_list=f.listFiles();
						childtitleheader.setText("sdcard"); // set the chaild title
					walkdir(images);
					dynamicimagesparent();
					dynamicimageschild();
				}
				else if((Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""))
				{
					cf.ShowToast("Please enter the valid foldername ", 1);
				}
				else 
				{	Src_Txt="/mnt/sdcard/"+Src_Txt;
					File f = new File(Src_Txt);
					if(f.exists())
					{
						if((Src_Txt.substring(Src_Txt.lastIndexOf("/"), Src_Txt.length())).equals(""))
						{
							titleheader.setText(Src_Txt);
		 					finaltext=Src_Txt;
		 					 images = new File(Src_Txt);
		 					//Currentfile_list=f.listFiles();
		 					Src_Txt=Src_Txt.substring(0, Src_Txt.length()-1);
		 					childtitleheader.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
						}
						else
						{
							titleheader.setText(Src_Txt+"/");
		 					finaltext=Src_Txt+"/";
		 					 images = new File(Src_Txt+"/");
		 					//Currentfile_list=f.listFiles();
		 					
		 					childtitleheader.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
						}
						
	 					walkdir(images);
	 					dynamicimagesparent();
	 					dynamicimageschild();
						
					}
					else
					{
						cf.ShowToast("Invalid Search.", 1);
					}
					
				}
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		name_txt.addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				try
				{
					String s1=s.toString();
					System.out.println("The issues not in the set "+s1);
					String file_name,file_txt,Requested_name;
					 if((s1.substring(0,1)).equals("/") )
						{
							name_txt.setText("");
							cf.ShowToast("Please Enter valid text ", 1);
							System.out.println("The issues not in the set ");
						}
					 else if((s1.length()>1) && (s1.substring(s.length()-1,s.length())).equals("/") && (s1.substring(s.length()-2,s.length()-1)).equals("/"))
					{
						name_txt.setText(s1.substring(0,s1.length()-1));
						name_txt.setSelection(name_txt.getText().length());
						cf.ShowToast("Please Enter valid text ", 1);
					}
					
					else if(name_txt.getText().toString().equals(""))
					{
						file_name="/mnt/sdcard/";
						 file_txt= "/mnt/sdcard";
					}
					else
					{
						file_name="/mnt/sdcard/"+name_txt.getText().toString();
						
						 file_txt= file_name.substring(0,file_name.lastIndexOf("/"));
//						 if(file_txt.equals("/mnt/sdcard"))
//						 {
//							 file_txt+="/"; 
//						 }
						 Requested_name=file_name.substring(file_name.lastIndexOf("/")+1,file_name.length());
						 System.out.println("The file name and the texdt was file_name "+file_name+" file_txt ="+file_txt+"Requested_name ="+Requested_name);
						File f = new File(file_txt);
						if(f.exists())
						{
							System.out.println("File xeists");
							File[] file_list= f.listFiles(directoryFilter);

							String[] autousername = new String[file_list.length];
							String[] filename=null;
							System.out.println("File xeists 1"+file_list.length);
								if(file_list.length>0)
								{
									System.out.println("comes in to if");
								int k=0;
										for(int i=0;i<file_list.length;i++)
										{
											String Tmp=file_list[i].getAbsolutePath();
											
											Tmp=Tmp.substring(Tmp.lastIndexOf("/")+1,Tmp.length());
											
											
											System.out.println(" Tmp = "+Tmp+" Requested_name = "+Requested_name+" (Tmp.substring(0,Requested_name.length())) = "+(Tmp.substring(0,Requested_name.length())));
											if((Tmp.substring(0,Requested_name.length())).toLowerCase().equals(Requested_name.toLowerCase()))
											{
												
												String temp= name_txt.getText().toString();
												if(temp.contains("/"))
												{
													temp= temp.substring(0,name_txt.getText().toString().lastIndexOf("/"));
													autousername[k]=temp+"/"+Tmp;
												}
												else
												{
													autousername[k] = Tmp;	
												}
												
												System.out.println("text for the file was autousername[k]= "+autousername[k]);
												k++;
												
											}
											
											
											
											
										}
										filename=new String[k];
										
										for(int m=0;m<k;m++)
										{
											
											filename[m]=autousername[m];
											System.out.println("text for the file was filename[i]= "+filename[m]);
											
										}
										
										
								}
								 ArrayAdapter<String> adapter = new ArrayAdapter<String>(FrontElevation.this,R.layout.loginnamelist,filename);
								 name_txt.setThreshold(1);
								 name_txt.setAdapter(adapter);
					}
				
				
				
						 
				}
				
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("issues happen in cathce "+e.getMessage());
				}
			}
		});
		dialog1.setCancelable(false);
		dialog1.show();
		

}
protected void onStart() {
    super.onStart();
    showimages();
    
}	
}
