package idsoft.inspectiondepot.B11802;

import android.app.Activity;


import java.util.ArrayList;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Exportedlstview extends Activity {
	String colomnname="",statusname="",headernote="";String statusofdata;
	private TextView welcome, title;
	ArrayList<String> ExportList = new ArrayList<String>();
	private ListView lv1;
	TextView delrec;
	int val = 0, rws;
	Cursor c;
	String[] homelist = new String[100];
	String[] deletelist = new String[100];
	String inspdata, query;
	private Button homepage, search, search_clear_txt,deleteallinspections;
	public EditText search_text;
	String sql, strtit,alerttitle="";
	ScrollView sv;
	LinearLayout inspectionlist;
	String data[];CheckBox challtodelete;
	String countarr[];
	TextView tvstatus[];
	Button deletebtn[];View v;
	String value = "text", inspid, res = "";
	CommonFunctions cf;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getExtras();
		if (b != null) {
			value = b.getString("id");
			inspid = b.getString("InspectionType");
		}
		System.out.println("Exported");
		setContentView(R.layout.expotlistview);
		TextView tvheader = (TextView) findViewById(R.id.information);
		TextView note  = (TextView) findViewById(R.id.note);
		
		if("uts".equals(value)) {
		headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode";
		note.setText(headernote);
		}
		else
		{
			if("rr".equals(value)) 
			{
				 headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time | Status";
			}
			else
			{
				 headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time";	
			}
			
			note.setText(headernote);
		}
		
		
		inspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		if (value.equals("pretab")) {
			alerttitle="Are you sure want to delete all the inspections in the CIT status";
			statusofdata="1";
			colomnname="IsInspected";
			tvheader.setText("Completed Inspection in Tablet");
		} else if (value.equals("preonl")) {
			alerttitle="Are you sure want to delete all the inspections in the CIO status?";
			statusofdata="41";
			colomnname = "SubStatus";
			tvheader.setText("Completed Inspection in online");
		} else if (value.equals("uts")) {
			alerttitle="Are you sure want to delete all the inspections in the UTS status?";
			statusofdata="110";
			colomnname = "Status";
			tvheader.setText("Unable to Schedule Inspection");
		} else if (value.equals("can")) {
			alerttitle="Are you sure want to delete all the inspections in the CAN status?";
			statusofdata="90";
			colomnname = "Status";
			tvheader.setText("Cancelled Inspection");
		} else if (value.equals("rr")) {
			alerttitle="Are you sure want to delete all the inspections in the Reports Ready status?";
			statusofdata="90";
			colomnname = "Status";
			tvheader.setText("Reports Ready");
		}
		cf = new CommonFunctions(this);
		this.welcome = (TextView) this.findViewById(R.id.welcomename);
		this.homepage = (Button) this.findViewById(R.id.deletehome);
		deleteallinspections = (Button) this.findViewById(R.id.deleteallinspections);
		cf.getInspectorId();
		cf.welcomemessage();
		welcome.setText(cf.data);System.out.println("data");
		title = (TextView) this.findViewById(R.id.deleteiinformation);
		if (inspid.equals("28")) {
			strtit = "B1 - 1802 (Rev. 01/12)";
		} else {
			strtit = "B1 - 1802 (Rev. 01/12) Carr.Ord.";
		}
		this.homepage.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				cf.gohome();

			}
		});
		
		try {
			cf.Createtablefunction(2);
			Cursor c = cf.SelectTablefunction(cf.mbtblInspectionList,
					"where InspectorId='" + cf.InspectorId + "'");
			int rws = c.getCount();
			if(rws==0)
			{
				deleteallinspections.setVisibility(v.GONE);
			}
			else
			{
				deleteallinspections.setVisibility(v.VISIBLE);
			}
		} catch (Exception e) {
			System.out.println("eror " + e);

		}
		this.search = (Button) this.findViewById(R.id.search);System.out.println("search");
		search_text = (EditText) findViewById(R.id.search_text);System.out.println("search_text");
		this.search_clear_txt = (Button) this
				.findViewById(R.id.search_clear_txt);
		System.out.println("search_clear_txt");
		this.search_clear_txt.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				search_text.setText("");
				res = "";
				dbquery();

			}

		});System.out.println("xxx");
		this.search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				String temp = cf.convertsinglequotes(search_text.getText()
						.toString());
				if (temp.equals("")) {
					cf.ShowToast("Please enter the Name or Policy Number to search.",1);

					search_text.requestFocus();
				} else {
					res = temp;
					dbquery();
				}

			}

		});System.out.println("dbquery");
		try {
			dbquery();
		} catch (Exception e) {
			System.out.println("Exception"+e.getMessage());
			cf.ShowToast("There is a problem in saving your data due to invalid character.",1);

		}System.out.println("dbquery");
	}

	private void dbquery() {
		System.out.println("dbquery indide");
		int k = 1;
		data = null;
		inspdata = "";
		sql = "select * from " + cf.mbtblInspectionList;
		if (!res.equals("")) {

			sql += " where (s_OwnersNameFirst like '%" + res
					+ "%' or s_OwnersNameLast like '%" + res
					+ "%' or s_OwnerPolicyNumber like '%" + res
					+ "%') and InspectionTypeId='" + inspid
					+ "' and InspectorId='" + cf.InspectorId + "'";
			if (value.equals("uts")) {
				sql += " and  Status=110 and InspectionTypeId='" + inspid + "'";
			} else if (value.equals("can")) {
				sql += " and Status=90 and InspectionTypeId='" + inspid + "'";
			} else if (value.equals("pretab")) {
				sql += " and Status=40 and InspectionTypeId='" + inspid + "'";
			} else if (value.equals("preonl")) {
				sql += " and SubStatus='41' and InspectionTypeId='" + inspid+ "'";
			}
			else if (value.equals("rr")) {
				sql += " and ((Status='2' and SubStatus='0') or (Status='5' and SubStatus='0') or (Status='2' and SubStatus='121') or (Status='2' and SubStatus='122') or (Status='2' and SubStatus='123') or (Status='5' and SubStatus='151') or (Status='5' and SubStatus='153') or (Status='5' and SubStatus='152')  or (Status='70' and SubStatus='71') or (Status='140') or (Status='70'))  and InspectionTypeId='" + inspid+ "'";
			}

		} else {
			if (value.equals("uts")) {
				sql += " where Status=110 and InspectionTypeId='" + inspid
						+ "' and InspectorId='" + cf.InspectorId + "'";
			} else if (value.equals("can")) {
				sql += " where Status=90 and InspectionTypeId='" + inspid
						+ "' and InspectorId='" + cf.InspectorId + "'";
			} else if (value.equals("pretab")) {
				sql += " where Status=40 and InspectionTypeId='" + inspid
						+ "' and InspectorId='" + cf.InspectorId + "'";
			} else if (value.equals("preonl")) {
				sql += " where SubStatus='41' and InspectionTypeId='" + inspid
						+ "' and InspectorId='" + cf.InspectorId + "'";
			}
			else if (value.equals("rr")) {
				sql += " where ((Status='2' and SubStatus='0') or (Status='5' and SubStatus='0') or (Status='2' and SubStatus='121') or (Status='2' and SubStatus='122') or (Status='2' and SubStatus='123') or (Status='5' and SubStatus='151') or (Status='5' and SubStatus='153') or (Status='5' and SubStatus='152')  or (Status='70' and SubStatus='71') or (Status='140') or (Status='70'))  and InspectionTypeId='" + inspid+ "' and InspectorId='" + cf.InspectorId + "'";
			}

		}
		Cursor cur = cf.db.rawQuery(sql, null);
		rws = cur.getCount();System.out.println("rws sql"+sql);
		title.setText(strtit + "\n" + "Total Record : " + rws);
		data = new String[rws];
		countarr = new String[rws];
		int j = 0;
		cur.moveToFirst();System.out.println("rws count"+rws);
		if (cur.getCount() >= 1) {System.out.println("rws ");
			do {
				statusname="";
				if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("0"))
				{
					statusname="IMC Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("0"))
				{
					statusname="IPA Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("121"))
				{
					statusname="Carrier Rejected";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("122"))
				{
					statusname="Carrier Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("123"))
				{
					statusname="Carrier Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("151"))
				{
					statusname="Carrier Rejected";
				}				
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("152"))
				{
					statusname="Carrier Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("153"))
				{
					statusname="Carrier Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("70") && cur.getString(cur.getColumnIndex("SubStatus")).equals("71"))
				{
					statusname="Citizen Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("70"))
				{
					statusname="Citizen Rejected";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("140"))
				{
					statusname="Closed";
				}
				
				
				inspdata += " "
						+ cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_OwnersNameFirst"))) + " ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_OwnersNameLast"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_propertyAddress"))) + " \n ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_City"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_State"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_County"))) + " | ";
				inspdata += cur.getString(cur.getColumnIndex("s_ZipCode"))
						+ " \n ";
				inspdata += cur.getString(cur.getColumnIndex("ScheduleDate"))
						+ " | ";
				inspdata += cur.getString(cur
						.getColumnIndex("InspectionStartTime")) + " - ";
				inspdata += cur.getString(cur
						.getColumnIndex("InspectionEndTime")) + " | " + statusname+"~" ;

				countarr[j] = cur.getString(cur.getColumnIndex("s_SRID"));
				
				System.out.println("schedule dat "+cur.getString(cur.getColumnIndex("ScheduleDate")));
				
				System.out.println("total reco "+inspdata);
				
			
				
				j++;

				if (inspdata.contains("null")) {
					inspdata = inspdata.replace("null", "");
				}
				if (inspdata.contains("N/A |")) {
					inspdata = inspdata.replace("N/A |", "");
				}
				if (inspdata.contains("N/A - N/A")) {
					inspdata = inspdata.replace("N/A - N/A", "");
				}
			} while (cur.moveToNext());
			search_text.setText("");
			display();
		} else {
			
			inspectionlist.removeAllViews();
			if(res.equals(""))
			{
				cf.gohome();
			}
			else
			{
				cf.ShowToast("Sorry, No results found.", 1);
				cf.hidekeyboard();
			}
			
			
		}

	}

	private void display() {
		inspectionlist.removeAllViews();
		sv = new ScrollView(this);
		inspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		System.out.println("inspdata "+inspdata);
		if (!inspdata.equals(null) && !inspdata.equals("null")
				&& !inspdata.equals("")) {
			this.data = inspdata.split("~");System.out.println("data "+data.length);
			for (int i = 0; i < data.length; i++) {
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
			
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				l2.setLayoutParams(mainparamschk);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
				tvstatus[i] = new TextView(this);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

			    lchkbox.addView(tvstatus[i], paramschk);

				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
				ldelbtn.setLayoutParams(mainparamschk);
				ldelbtn.setGravity(Gravity.RIGHT);
			    l2.addView(ldelbtn);
				deletebtn[i] = new Button(this);
				deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
				deletebtn[i].setTag("deletebtn" + i);
				deletebtn[i].setPadding(30, 0, 0, 0);
				ldelbtn.addView(deletebtn[i], paramsdelbtn);

				
				
				deletebtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace(
								"deletebtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						final String dt = countarr[cvrtstr];
						 cf.alerttitle="Delete";
						  cf.alertcontent="Are you sure want to delete?";
						    final Dialog dialog1 = new Dialog(Exportedlstview.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText( cf.alerttitle);
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml( cf.alertcontent));
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							btn_yes.setOnClickListener(new OnClickListener()
							{
				           	public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									cf.fn_delete(dt);
									dbquery();
								}
								
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{

								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									
								}
								
							});
							dialog1.setCancelable(false);
							dialog1.show();
						

					}
				});
			}
		}

	}

	public void list(String k) {
		try {
			int i = 1;
			String sql = "select * from " + cf.mbtblInspectionList;
			if (!k.equals("")) {
				sql += " where (s_OwnersNameFirst like '%" + k
						+ "%' or s_OwnersNameLast like '%" + k
						+ "%' or s_OwnerPolicyNumber like '%" + k
						+ "%') and InspectionTypeId='" + inspid + "'";
				if (value.equals("uts")) {
					sql += " and  Status=110 and InspectionTypeId='" + inspid
							+ "'";
				} else if (value.equals("can")) {
					sql += " and Status=90 and InspectionTypeId='" + inspid
							+ "'";
				} else if (value.equals("pretab")) {
					sql += " and Status=40 and InspectionTypeId='" + inspid
							+ "'";
				} else if (value.equals("preonl")) {
					sql += " and SubStatus='41' and InspectionTypeId='"
							+ inspid + "'";
				}

			} else {
				if (value.equals("uts")) {
					sql += " where Status=110 and InspectionTypeId='" + inspid
							+ "'";
				} else if (value.equals("can")) {
					sql += " where Status=90 and InspectionTypeId='" + inspid
							+ "'";
				} else if (value.equals("pretab")) {
					sql += " where Status=40 and InspectionTypeId='" + inspid
							+ "'";
				} else if (value.equals("preonl")) {
					sql += " where SubStatus='41' and InspectionTypeId='"
							+ inspid + "'";
				}

			}

			Cursor cur = cf.db.rawQuery(sql, null);
			int rws = cur.getCount();

			ExportList.clear();
			if (rws == 0) {
				cf.ShowToast("Sorry, No results found for your search criteria.",1);

			} else {
				cur.moveToFirst();
				if (cur != null) {
					do {

						homelist[i] = cur.getString(1);
						String s = "";
						s = cur.getString(cur
								.getColumnIndex("s_OwnersNameFirst"));
						s += " ";
						s += cur.getString(cur
								.getColumnIndex("s_OwnersNameLast"));
						s += " | ";
						s += cur.getString(cur
								.getColumnIndex("s_propertyAddress"));
						s += " | ";
						s += cur.getString(cur.getColumnIndex("s_City"));
						s += " | ";
						s += cur.getString(cur.getColumnIndex("s_State"));
						s += " | ";
						s += cur.getString(cur.getColumnIndex("s_County"));
						s += " | ";
						s += cur.getString(cur
								.getColumnIndex("InspectionStartTime"));
						s += " - ";
						s += cur.getString(cur
								.getColumnIndex("InspectionEndTime"));
						String scheduleddate = cur.getString(cur
								.getColumnIndex("ScheduleDate"));
						String anytype = "anyType{}";
						if (anytype.equals(scheduleddate)
								|| "Null".equals(scheduleddate)) {
							s += " ";
						} else {
							s += " | ";
							s += scheduleddate;
						}
						if (s.contains("Not Available |")) {
							s = s.replace("Not Available |", "");

						}
						if (s.contains("| Not Available")) {
							s = s.replace("| Not Available", "");
						}
						ExportList.add(s);
						i++;

					} while (cur.moveToNext());
				}
				cur.close();
			}
		} catch (Exception e) {
			cf.ShowToast("Sorry, No results found for your search criteria.",1);
		}

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent loginpage = new Intent(Exportedlstview.this,Startinspections.class);
			startActivity(loginpage);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.deleteallinspections:
				String temp="(";
				if(value.equals("rr"))
				{
					c=cf.db.rawQuery(" Select * from "+cf.mbtblInspectionList+" where (Status =2 or Status =5 or Status =140)  and InspectorId='"+cf.InspectorId+"' and InspectionTypeId ='"+inspid+"'",null);
				}
				else
				{
					c=cf.db.rawQuery(" Select * from "+cf.mbtblInspectionList+" where "+colomnname+" ="+statusofdata+" and InspectorId='"+cf.InspectorId+"' and InspectionTypeId ='"+inspid+"'",null);	
				}
				 
			 	 System.out.println("Cpint "+c.getCount());
				 if( c.getCount()>=1)
				 {
					 c.moveToFirst();
					 for(int i=0;i<c.getCount();i++)
					 {
						 temp+="'"+c.getString(c.getColumnIndex("s_SRID"))+"'";
						 if((i+1)==(c.getCount()))
						 {
							 temp+=")";
							// return;
						 }
						 else
						 {
							 temp+=",";
							 c.moveToNext();
						 }
					 }
					 cf.delete_all(temp,alerttitle); 
			  break;
		  }
	 }
	 }
}