package idsoft.inspectiondepot.B11802;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OverallComments extends Activity {
	EditText comments;
	Intent iInspectionList;
	int updatecnt,commentlength;
	Button saveclose;
	public int focus=0;
	String overallcomm, homeId, InspectionType, status;
	TextView txtoverallheading;
	int value, Count;
	LinearLayout overall_parrant,overall_type1;
	TextView overall_TV_type1;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	TextView policyholderinfo;
	CommonFunctions cf;
	TextWatcher watcher;
	ImageView img1, img2, img3, img4, img6, img7, img8, img9, buildcode,
			roofcover, roofdeck, roofwall, roofgeometry, swr, openprotection,
			wallconstruction;;
	TableLayout exceedslimit1;View v1;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		cf.getInspectorId();

		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.Homeid = homeId = bunhomeId.getString("homeid");
			cf.InspectionType = InspectionType = bunhomeId
					.getString("InspectionType");
			cf.status = status = bunhomeId.getString("status");
			cf.value = value = bunhomeId.getInt("keyName");
			cf.Count = Count = bunhomeId.getInt("Count");

		}

		setContentView(R.layout.overallcomments);
		cf.getInspectorId();
		cf.getDeviceDimensions();
		cf.getinspectiondate();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);		  
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 5, cf, 0));
		ScrollView scr = (ScrollView) findViewById(R.id.scr);
		scr.setMinimumHeight(cf.ht);
		focus=0;
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		
		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		this.txtoverallheading = (TextView) this.findViewById(R.id.txtoverallheading);
		txtoverallheading
				.setText(Html
						.fromHtml("Note: This comment box should be used to describe any other information relating to your inspection that did not directly affect the mitigation features reported. Please Avoid use of any derogatory language as these comments are seen by both carrier and policyholder."));
		int maxwidth = (cf.wd)-50;
		txtoverallheading.setMaxWidth(maxwidth);
		
			overall_parrant = (LinearLayout)this.findViewById(R.id.overall_parrant);
			overall_type1=(LinearLayout) findViewById(R.id.overall_type);
			overall_TV_type1 = (TextView) findViewById(R.id.overall_text);
			
		this.comments = (EditText) this.findViewById(R.id.txtoverallcomments);
		comments.setOnTouchListener(new Touch_Listener());
		comments.addTextChangedListener(new OC_textwatcher());
		
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.tbl_comments
				+ " WHERE SRID='" + cf.Homeid + "'", null);
		int rws = c2.getCount();
		if (rws == 0) {
		} else {
			c2.moveToFirst();
			if (c2 != null) {
				overallcomm = c2.getString(c2
						.getColumnIndex("InsOverAllComments"));
				System.out.println("overall "+overallcomm);
				cf.showing_limit(overallcomm,overall_parrant,overall_type1,overall_TV_type1,"1498");	
				comments.setText(cf.getsinglequotes(overallcomm));
			}
		}
		
		
		try {
			Cursor cur = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
			int rws1 = cur.getCount();
			cur.moveToFirst();
			String ownerfirstname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameFirst")));
			String ownerlastname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameLast")));
			String policyno = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnerPolicyNumber")));
			policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "
					+ ownerlastname + "<br/><font color=#bddb00>(Policy No : "
					+ policyno + ")</font>"));

		} catch (Exception e) {

		}
		

		saveclose = (Button) findViewById(R.id.save);
		saveclose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				overallcomm = comments.getText().toString();

				if (overallcomm.trim().equals("")) {
					cf.ShowToast("Please enter the OverAll Comments.",1);
				} else {
					try {

						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.tbl_comments + " WHERE SRID='" + cf.Homeid
								+ "'", null);
						int rws = c2.getCount();

						if (rws == 0) {

							cf.db.execSQL("INSERT INTO "
									+ cf.tbl_comments
									+ " (SRID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,WallConstructionComment,SecondaryWaterComment,OpeningProtectionComment,InsOverAllComments,CreatedOn)"
									+ " VALUES ('" + cf.Homeid + "','" + null
									+ "','" + null + "','" + null + "','"
									+ null + "','" + null + "','" + null
									+ "','" + null + "','" + null + "','"
									+ cf.convertsinglequotes(overallcomm)
									+ "','" + cd + "')");
							updatecnt = 1;
						} else {

							cf.db.execSQL("UPDATE " + cf.tbl_comments
									+ " SET InsOverAllComments='"
									+ cf.convertsinglequotes(overallcomm)
									+ "', CreatedOn='" + md + "'"
									+ " WHERE SRID ='" + cf.Homeid.toString()
									+ "'");
							cf.ShowToast("OverAll Comments has been saved successfully.",1);

						}

						Cursor c3 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c3.getCount();
						if (subchkrws == 0) {
							cf.db.execSQL("INSERT INTO "
									+ cf.SubmitCheckTable
									+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
									+ "VALUES ('" + cf.InspectorId + "','"
									+ cf.Homeid
									+ "',0,0,0,0,0,0,0,0,0,0,0,0,0,1,0)");
						} else {
							cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
									+ " SET fld_overall=1 WHERE Srid ='"
									+ cf.Homeid + "' and InspectorId='"
									+ cf.InspectorId + "'");
						}

						updatecnt = 1;
						if (updatecnt == 1) {
							cf.ShowToast("OverAll Comments has been saved successfully.",1);
							Intent genimg = new Intent(OverallComments.this,
									GeneralConditions.class);
							genimg.putExtra("homeid", cf.Homeid);
							genimg.putExtra("InspectionType", cf.InspectionType);
							genimg.putExtra("status", cf.status);
							genimg.putExtra("keyName", cf.value);
							genimg.putExtra("Count", cf.Count);
							startActivity(genimg);
						}

					} catch (Exception e) {
						cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
					}
				}
			}
		});
	}
	class Touch_Listener implements OnTouchListener
	{
		   Touch_Listener()
			{
				
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	
		    		cf.setFocus(comments);
				
				return false;
		    }
	}
	class OC_textwatcher implements TextWatcher
	{
	    OC_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
				
				cf.showing_limit(s.toString(),overall_parrant,overall_type1,overall_TV_type1,"1498"); 
			
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;

		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(OverallComments.this,
					InspectionList.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);System.out.println("case projection");
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
					
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
					
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	    if(focus==1)
	    {
	    	focus=0;
	    comments.setText(comments.getText().toString());    
	    }
	    
	 }     
}
