package idsoft.inspectiondepot.B11802;


import idsoft.inspectiondepot.B11802.FrontElevation.MyOnItemSelectedListener1;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class GeneralImages extends Activity implements Runnable {
	private static final int SELECT_PICTURE = 0;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	private static final String TAG = null;
	private static final int visibility = 0;ProgressDialog pd1;
	Map<String, String[]> elevationcaption_map = new LinkedHashMap<String, String[]>();
	String j,homeid, picname, InspectionType, status, sgnpath, pathdesc,finaltext="",pathofimage="",repidofselbtn1,imgnum,phtodesc,
			selectedImagePath = "empty", capturedImageFilePath, updstr, paht,
			strdesc, name = "Exterior and Grounds", str;
	int value, maxLength=99,Count, alreadyExist=0,delimagepos,rws, chk, cvrtstr1,selid, t, f, quesid, elev = 1, isexist,backclick=0,selectedcount=0;;
	Uri mCapturedImageURI;	Bitmap rotated_b;
	int currnet_rotated=0;int globalvar=0;
	Spinner spnelevation[];	
	CheckBox chkboxchild[];        
	String selectedtestcaption[] =null;
	ImageView thumbviewimgchild[],imgviewparent[],elevationimage[];
	TextView tvstatusparent[],tvstatuschild[];
	String[] arrimgord,pieces1=null,pieces2=null,pieces3=null;
	ListAdapter adapter;
	LinearLayout lnrlayout,lnrlayoutparent,lnrlayoutchild;
	TextView txthdr;
	File externamstoragepath;
	TextView imagepath,titleheader,textviewvalue_parent,textviewvalue_child,childtitleheader;
	Button backfolderwise,cancelfolderwise,selectfolderwise,selectfullimage,cancelfullimage;
	File listFile[];
	String[] items = { "gallery", "camera" };
	EditText eddesc1,input, firsttxt, edbrowse, ed1;
	Button btnbrwse, btnupd, updat, del;
	TextView tvstatus[];
	ProgressDialog pd;
	int ImageOrder = 0, id1;
	static String[] arrpath;
	static String[] arrpathdesc;
	TextView policyholderinfo;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	ImageView fullimageview;
	CheckBox cb1;
	View v1;
	private Cursor cursor;
	private int columnIndex;
	private int count;
	Dialog dialog,dialogfullimage;
	private Bitmap[] thumbnails;
	private boolean[] thumbnailsselection;
	private String[] arrPath;
	private ImageAdapter imageAdapter;
	private ImageAdapter1 imageAdapter1;
	UploadRestriction upr = new UploadRestriction();
	public  ScrollView scr2=null;
	File[] Currentfile_list=null;
	public int limit_start=0; 
	protected RelativeLayout Rec_count_lin=null;
	protected TextView Total_Rec_cnt=null;
	protected TextView showing_rec_cnt;
	public LinearLayout sdcardImages;
	public String patharr[] = new String[8];
	int maxclick, maxlevel = 0,maximumindb=0;
	ImageView img1, img2, img3, img4, img6, img7, img8, img9, fstimg,  exttick, intetick, additick;
	Button extimg,inteimg, addiimg;
	public String[] et;
	public Bitmap[] thumbnails1;
	public int[] imageorder;
	public int count1;
	RelativeLayout Lin_Pre_nex=null;
	ImageView prev=null,next=null;
	private File images=null;
	protected int[] imageid;
	private String[] arrPath1;
	static CommonFunctions cf;
	Map<String, TextView> mapfolder = new LinkedHashMap<String, TextView>();
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		/** Creating hazard image table **/
		cf.Createtablefunction(14);
		cf.Createtablefunction(18);
		cf.getDeviceDimensions();
		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		setContentView(R.layout.generalimage);
		
		ScrollView scr = (ScrollView) findViewById(R.id.scr);
		scr.setMinimumHeight(cf.ht);
		
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());

		img1 = (ImageView) findViewById(R.id.img01);
		img2 = (ImageView) findViewById(R.id.img02);
		img3 = (ImageView) findViewById(R.id.img03);
		img4 = (ImageView) findViewById(R.id.img04);
		img6 = (ImageView) findViewById(R.id.img06);
		img7 = (ImageView) findViewById(R.id.img07);
		img8 = (ImageView) findViewById(R.id.img08);
		img9 = (ImageView) findViewById(R.id.img09);
		extimg = (Button) findViewById(R.id.ext);
		inteimg = (Button) findViewById(R.id.inte);
		addiimg = (Button) findViewById(R.id.addi);
		exttick = (ImageView) findViewById(R.id.extovr);
		intetick = (ImageView) findViewById(R.id.inteovr);
		additick = (ImageView) findViewById(R.id.addiovr);
		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		Button changeimageorder = (Button) findViewById(R.id.changeimageorder);
		edbrowse = (EditText) findViewById(R.id.pathtxt);
		edbrowse.setVisibility(v1.GONE);
		btnbrwse = (Button) findViewById(R.id.browsetxt);
		btnupd = (Button) findViewById(R.id.updtxt);
		lnrlayout = (LinearLayout) this.findViewById(R.id.linscrollview);
		fstimg = (ImageView) findViewById(R.id.firstimg);
		firsttxt = (EditText) findViewById(R.id.firsttxt);
		updat = (Button) findViewById(R.id.update);
		txthdr = (TextView) findViewById(R.id.titlehdr);
		del = (Button) findViewById(R.id.delete);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		callthread();
		
		changeimageorder.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				subview();
			}
		});
		try {
			Cursor cur = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
			int rws1 = cur.getCount();
			cur.moveToFirst();
			String ownerfirstname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameFirst")));
			String ownerlastname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameLast")));
			String policyno = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnerPolicyNumber")));
			policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "
					+ ownerlastname + "<br/><font color=#bddb00>(Policy No : "
					+ policyno + ")</font>"));

		} catch (Exception e) {
			System.out.println(" errrr " + e.getMessage());
		}
		if (elev == 1) {			
			
			name = "Exterior and Grounds";
		} else if (elev == 2) {
			
			name = "Interior";
			
		} else if (elev == 3) {	
			
		name = "Additional";
			
		}
	}
	private void callthread()
	{
		try
		{
		Cursor c1 = cf.db.rawQuery("SELECT * FROM " + cf.tblphotocaption, null);
		if(c1.getCount()==0)
		
		{
			 pd1 = ProgressDialog.show(GeneralImages.this, "Loading Captions ...", "Please wait...", true, false);
		        new Thread(new Runnable() {
		                public void run() {
		                	System.out.println("rrrun");		       
		                	cf.insertelevation();
		                	finishedHandler.sendEmptyMessage(0);		                
		                }
		            }).start();
		       
		}
		else
		{
			changeimage();
			showimages();
		}
		}
		catch(Exception e)
		{
			
		}
	}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	        pd1.dismiss();
	         
	    }
	};

	private void changeimage() {
		// TODO Auto-generated method stub
		try {

			Cursor extcur = cf.SelectTablefunction(cf.HazardImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=1 and Delflag=0");
			int extrws = extcur.getCount();
			if (extrws != 0) {
				exttick.setVisibility(visibility);
			}

			Cursor intecur = cf.SelectTablefunction(cf.HazardImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=2 and Delflag=0");
			int interws = intecur.getCount();
			if (interws != 0) {
				intetick.setVisibility(visibility);
			}

			Cursor addicur = cf.SelectTablefunction(cf.HazardImageTable,
					"where SrID='" + cf.Homeid
							+ "' and Elevation=3 and Delflag=0");
			int addirws = addicur.getCount();
			if (addirws != 0) {
				additick.setVisibility(visibility);
			}

		} catch (Exception e) {

		}

	}

	protected void subview() {
		// TODO Auto-generated method stub
		try {
			Cursor d11 = cf.db.rawQuery("SELECT * FROM " + cf.HazardImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "' and Delflag=0 order by ImageOrder", null);
			rws = d11.getCount();

			if (rws == 0) {
				cf.ShowToast("There is no Image, So please upload the Image.",1);
			} else {
				String Imagepath[] = new String[rws];

				this.et = new String[rws];

				final Dialog dialog = new Dialog(GeneralImages.this);
				dialog.setContentView(R.layout.main);
				dialog.setTitle("Change Image Order");
				dialog.setCancelable(true);

				this.count1 = d11.getCount();
				this.thumbnails1 = new Bitmap[this.count1];
				this.arrPath1 = new String[this.count1];
				this.imageid = new int[this.count1];
				this.imageorder = new int[this.count1];
				d11.moveToFirst();

				for (int i = 0; i < this.count1; i++) {

					this.imageid[i] = Integer.parseInt(d11.getString(0)
							.toString());
					this.imageorder[i] = d11.getInt(d11
							.getColumnIndex("ImageOrder"));
					arrPath1[i] = cf.getsinglequotes(d11.getString(d11
							.getColumnIndex("ImageName")));
					Bitmap b = ShrinkBitmap(arrPath1[i], 130, 130);
					thumbnails1[i] = b;

					d11.moveToNext();
				}

				dialog.show();

				GridView imagegrid = (GridView) dialog
						.findViewById(R.id.PhoneImageGrid);
				imagegrid.setNumColumns(3);
				imageAdapter1 = new ImageAdapter1();
				imagegrid.setAdapter(imageAdapter1);

				final Button selectBtn = (Button) dialog
						.findViewById(R.id.selectBtn);
				selectBtn.setText("Save");
				final Button cancelBtn = (Button) dialog
						.findViewById(R.id.cancelBtn);
				cancelBtn.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub

						dialog.cancel();
					}
				});

				selectBtn.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						// dialog.cancel();
						Boolean boo = true;
						String temp[] = new String[count1];
						try {
							for (int i = 0; i < imageid.length; i++) {
								temp[i] = et[i];
								int myorder = 0;
								if (temp[i] != null && !temp[i].equals("")) {
									myorder = Integer.parseInt(temp[i]);
									if (myorder <= imageid.length
											&& myorder != 0) {

									} else {
										if((myorder==0) || (myorder==9))
										{
											cf.ShowToast("This is an Invalid Image Order.",1);
										}
										else if(myorder>=imageid.length)
										{
											cf.ShowToast("This is an Invalid Image Order.",1);
										}
										
										boo = false;
										
										
									}
								} else {
									cf.ShowToast("Image Order can't be empty. Please enter Image Order.",1);
									boo = false;
								}

							}

							int length = temp.length;
							for (int i = 0; i < length; i++) {
								for (int j = 0; j < length; j++) {
									if (temp[i].equals(temp[j]) && i != j) {
										alreadyExist=1;
									}
								}
							}					
							if(alreadyExist==1)
							{
								cf.ShowToast("You cant enter this Image order. This already exists.",1);
								alreadyExist=0;
							}
							else
							{															
								if (boo) {
									for (int i = 0; i < imageid.length; i++) {
										cf.db.execSQL("UPDATE " + cf.HazardImageTable
												+ " SET ImageOrder='" + temp[i]
												+ "' WHERE WSID='" + imageid[i]
												+ "'");
									}	
									dialog.cancel();
									cf.ShowToast("Image Order saved successfully.",1);
									showimages();
								}
							}							
						}
						catch (Exception e) {
							System.out.println("e" + e.getMessage());
						}
					}
				});

			}
		} catch (Exception m) {

			System.out.println("problem occure " + m);
		}
	}

	public class ImageAdapter1 extends BaseAdapter {
		private LayoutInflater mInflater;

		public ImageAdapter1() {
			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return count1;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder1 holder;
			if (convertView == null) {
				holder = new ViewHolder1();
				convertView = mInflater.inflate(R.layout.updateorder, null);
				holder.imageview = (ImageView) convertView
						.findViewById(R.id.thumbImage1);
				holder.edit = (EditText) convertView
						.findViewById(R.id.itemedit);
				holder.r = (RelativeLayout) convertView.findViewById(R.id.Rid);
				holder.edit.setId(position);
				holder.imageview.setId(position);
				holder.r.setId(position);

				Bitmap b = thumbnails1[position];

				holder.imageview.setImageBitmap(b);
				holder.edit.setText(String.valueOf(imageorder[position]));
				holder.edit.setTag("myedit" + position);
				holder.edit.addTextChangedListener(new watcher(position));
				holder.id = position;

				holder.edit.setText(String.valueOf(imageorder[position]));
				convertView.setTag(holder);
			} else {

				holder = (ViewHolder1) convertView.getTag();

			}

			return convertView;

		}

	}

	class ViewHolder1 {
		ImageView imageview;
		EditText edit;
		RelativeLayout r;
		int id;
		int k = 0;

	}

	private class watcher implements TextWatcher {

		int inde;

		public watcher(int i) {
			// TODO Auto-generated constructor stub
			this.inde = i;
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			et[this.inde] = s.toString();

		}

	};

	public void showimages() {
		sgnpath = "";
		pathdesc = "";
		try {
			Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.HazardImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "'and Delflag=0 order by ImageOrder", null);
			
			rws = c11.getCount();
			int rem = 8 - rws;
			String source = "<b><font color=#000000> Number of uploaded images : "
					+ "</font><font color=#DAA520>"
					+ rws
					+ "</font><font color=#000000> , Remaining : </font><font color=#DAA520>"
					+ rem + "</font></b>";
			txthdr.setText(Html.fromHtml(source));
			if (rws == 0) {
				lnrlayout.removeAllViews();
				firsttxt.setVisibility(v1.GONE);
				fstimg.setVisibility(v1.GONE);
				updat.setVisibility(v1.GONE);
				del.setVisibility(v1.GONE);
			} else {
				int Column1 = c11.getColumnIndex("ImageName");
				int Column2 = c11.getColumnIndex("Description");
				int Column3 = c11.getColumnIndex("ImageOrder");
				c11.moveToFirst();
				int i = 0;
				arrpath = new String[c11.getCount()];
				arrpathdesc = new String[c11.getCount()];
				arrimgord = new String[c11.getCount()];
				
				if (c11 != null) {
					do {
						commonfunction(cf.getsinglequotes(c11.getString(Column3)), i);
						arrpath[i] = cf.getsinglequotes(c11.getString(Column1));
						arrpathdesc[i] = cf.getsinglequotes(c11.getString(Column2));
						arrimgord[i] = cf.getsinglequotes(c11.getString(Column3));
						
						ColorDrawable sage = new ColorDrawable(this
								.getResources().getColor(R.color.sage));

						i++;
					} while (c11.moveToNext());
				}
				
				dynamicview();
			}
		} catch (Exception e) {
			Log.i(TAG, "error= " + e.getMessage());
		}
	}
	private void commonfunction(String imgor, int spin) {
		
		int imgorder = 0;String spinelevname="";String[] temp;
		try {
			imgorder = Integer.parseInt(imgor);
		} catch (Exception e) {

		}
		Cursor c1 = cf.SelectTablefunction(cf.tblphotocaption,
				" WHERE fld_elevtype='" + name+" "
						+ "' and fld_elevid='" + imgor + "' and fld_photocaption!=''");
		
		if (c1.getCount() > 0) {
			
			temp = new String[c1.getCount() + 2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
			int i = 2;
			c1.moveToFirst();
			do {  
				
				
				temp[i] = cf.getsinglequotes(c1.getString(c1
						.getColumnIndex("fld_photocaption")));
				i++;
			} while (c1.moveToNext());
		} else {
			temp = new String[2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
		}
		elevationcaption_map.put("spiner" + spin, temp);
		
		
		
	}
	private void dynamicview() throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		lnrlayout.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lnrlayout.addView(sv);
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		l1.setMinimumWidth(925);
		tvstatus = new TextView[arrpath.length];
		elevationimage = new ImageView[arrpath.length];
		spnelevation = new Spinner[arrpath.length];
		for (int i = 0; i < arrpath.length; i++) {

			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk1.topMargin = 20;
			l1.addView(l2,paramschk1);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(175, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 0;
			paramschk.leftMargin = 10;
			l2.addView(lchkbox);
			
			tvstatus[i] = new TextView(this);
			tvstatus[i].setMinimumWidth(175);
			tvstatus[i].setMaxWidth(175);
			tvstatus[i].setText(arrpathdesc[i]);
			tvstatus[i].setTextColor(Color.BLACK);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			lchkbox.addView(tvstatus[i], paramschk);
			tvstatus[i].setTag("imagechange" + i);

			String j = "0";
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			try {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[i]),
						null, o);
				j = "1";
			} catch (FileNotFoundException e) {
				j = "2";
			}
			if (j.equals("1")) {
				final int REQUIRED_SIZE = 100;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				
				// Decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						arrpath[i]), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);

				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(
						100, 50);
				//paramselevimg.topMargin = 5;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);
				
				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageDrawable(bmd);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
			}
			else
			{
				Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(100, 50);
				paramselevimg.topMargin = 10;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);

				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageBitmap(bmp);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
				
			}
			
	try
	{
			elevationimage[i].setOnClickListener(new View.OnClickListener() {

 				public void onClick(final View v) {
 					String getidofselbtn = v.getTag().toString();
 					final String repidofselbtn = getidofselbtn.replace(
 							"imagechange", "");
 					final int cvrtstr = Integer.parseInt(repidofselbtn);
 					String selpath = arrpath[cvrtstr];
 					String seltxt = arrpathdesc[cvrtstr];
 				
 					Cursor c11 = cf.db.rawQuery("SELECT ImageOrder FROM "
 							+ cf.HazardImageTable + " WHERE SrID='" + cf.Homeid
 							+ "' and Elevation='" + elev + "' and ImageName='"
 							+cf.convertsinglequotes(arrpath[cvrtstr]) + "'",
 							null);
 					c11.moveToFirst();
 					imgnum = c11.getString(c11.getColumnIndex("ImageOrder"));
 					
 					String a;
 					chk = 1;
 					dispfirstimg(cvrtstr,seltxt);
 					

 				}
 			});
			LinearLayout lspinner = new LinearLayout(this);
			LinearLayout.LayoutParams paramsspinn = new LinearLayout.LayoutParams(
					200, 50);
			paramsspinn.topMargin = 5;
			paramsspinn.leftMargin = 20;
			l2.addView(lspinner);
			int n=i+1;
			String[] GCH_Elevation = elevationcaption_map.get("spiner" + i);
			spnelevation[i] = new Spinner(this);
			spnelevation[i].setId(i);
			ArrayAdapter adapter2 = new ArrayAdapter(GeneralImages.this,android.R.layout.simple_spinner_item, GCH_Elevation);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnelevation[i].setAdapter(adapter2);
			lspinner.addView(spnelevation[i], paramsspinn);
			spnelevation[i].setOnItemSelectedListener(new MyOnItemSelectedListener1(i));
	}
	catch(Exception e)
	{
		System.out.println("file not found "+e.getMessage());
	}
		}
	}

	public class MyOnItemSelectedListener1 implements OnItemSelectedListener {
		public final int spinnerid;

		MyOnItemSelectedListener1(int id) {
			this.spinnerid = id;
		}

		public void onItemSelected(AdapterView<?> parent, View view,final int pos,
				long id) {
			
			if (pos == 0) {
			} else {
				try {
					
					
					final int j = spinnerid + 1;					
					final String[] photocaption = elevationcaption_map.get("spiner" + spinnerid);
					
					String[] bits = arrpath[spinnerid].split("/");
					picname = bits[bits.length - 1];
					
					if(photocaption[pos].equals("ADD PHOTO CAPTION"))
					{
						final AlertDialog.Builder alert = new AlertDialog.Builder(GeneralImages.this);
						alert.setTitle("ADD PHOTO CAPTION");
						input = new EditText(GeneralImages.this);
						alert.setView(input);
						//input.setTextSize(14);
						input.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
						input.setWidth(800);
						
						InputFilter[] FilterArray = new InputFilter[1];  
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);  
						input.setFilters(FilterArray);  
						
						alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								String commentsadd = input.getText().toString();
								if (!"".equals(commentsadd.trim())) {
									cf.db.execSQL("INSERT INTO "
											+ cf.tblphotocaption
											+ " (fld_photocaption,fld_elevtype,fld_elevid)"
											+ " VALUES ('" + cf.convertsinglequotes(commentsadd) + "','" + name+" " + "','" + j + "')");
									
									showimages();
									cf.ShowToast("Photo Caption added successfully.",1);
									InputMethodManager imm = (InputMethodManager)getSystemService(
										      Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
								} else {
									ShowToast toast = new ShowToast(getBaseContext(),
											"Please enter photo caption. It can't be empty.");
									InputMethodManager imm = (InputMethodManager)getSystemService(
										      Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
										spnelevation[spinnerid].setSelection(0);

								}
							}
						});

						alert.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									
									public void onClick(DialogInterface dialog, int whichButton) {
										dialog.cancel();
										spnelevation[spinnerid].setSelection(0);
										cf.hidekeyboard();
									}
								});
						alert.show();
					}
					else
					{
						final Dialog dialog = new Dialog(GeneralImages.this);
				        dialog.setContentView(R.layout.alertfront);
						dialog.setTitle("Choose Option");
			            dialog.setCancelable(false);
			            final EditText edit_desc = (EditText) dialog.findViewById(R.id.edittxtdesc);
			          
			            Button button_close = (Button) dialog.findViewById(R.id.Button05);
			            Button button_sel = (Button) dialog.findViewById(R.id.Button01);
			    		Button button_edit = (Button) dialog.findViewById(R.id.Button02);
			    		InputFilter[] FilterArray = new InputFilter[1];  
						FilterArray[0] = new InputFilter.LengthFilter(maxLength);  
						edit_desc.setFilters(FilterArray); 
			    		
			    		Button button_del = (Button) dialog.findViewById(R.id.Button04);
			    		final Button button_upd = (Button) dialog.findViewById(R.id.Button03);
			    		
			    		button_close.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	dialog.cancel();
				           	spnelevation[spinnerid].setSelection(0);
				            }
			    		});
			    		
			    		button_sel.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	dialog.cancel();
				            	try{
				            	cf.db.execSQL("UPDATE " + cf.HazardImageTable
										+ " SET Description='"
										+ cf.convertsinglequotes(photocaption[pos])
										+ "',Nameext='" + cf.convertsinglequotes(picname)
										+ "',ModifiedOn='" + md + "' WHERE SrID ='"
										+ cf.Homeid + "' and Elevation='" + elev
										+ "' and ImageOrder='" + arrimgord[spinnerid]
										+ "'");
				            	
				            }
			            	catch(Exception e)
			            	{
			            		System.out.println("issues"+e.getMessage());
			            		//cf.Error_LogFile_Creation("Problem in select the caption in the photos at the elevation "+elev+" error  ="+e.getMessage());
			            	}
				            	spnelevation[spinnerid].setSelection(0);
								tvstatus[spinnerid].setText(photocaption[pos]);
				            }
			    		});
			    		
			    		
			   			button_edit.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	
				            	edit_desc.setVisibility(v1.VISIBLE);
				            	edit_desc.setText(photocaption[pos]);
				            	
				            	button_upd.setVisibility(v1.VISIBLE);

				            	button_upd.setOnClickListener(new OnClickListener() {
						          public void onClick(View v) {
										if(!"".equals(edit_desc.getText().toString().trim()))
										{
											cf.db.execSQL("UPDATE " +cf. tblphotocaption + " set fld_photocaption = '"+cf.convertsinglequotes(edit_desc.getText().toString())+"' WHERE fld_elevtype ='" + name +" "+"' and fld_elevid='"+j+"' and fld_photocaption='"+cf.convertsinglequotes(photocaption[pos])+"'");
											showimages();
							   			    cf.ShowToast("Photo Caption has been updated sucessfully.",1);
							   			    dialog.cancel();
							   				cf.hidekeyboard();
										}
										else
										{
											cf.ShowToast("Please enter photo caption. It can't be empty.",1);
												spnelevation[spinnerid].setSelection(0);
												cf.hidekeyboard();
										}
						            }
					    		});
				            }
			    		});
			    		
			    		button_del.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				           	AlertDialog.Builder builder = new AlertDialog.Builder(GeneralImages.this);
				   			builder.setMessage("Are you sure, Do you want to delete?")
			   			       .setCancelable(false)
			   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			        	 cf.db.execSQL("Delete From " +cf. tblphotocaption + "  WHERE fld_photocaption='"+cf.convertsinglequotes(photocaption[pos])+"'");
			   			        	
									 showimages();
									 cf.ShowToast("Photo Caption has been deleted sucessfully.",1);
									 InputMethodManager imm = (InputMethodManager)getSystemService(
										      Context.INPUT_METHOD_SERVICE);
										imm.hideSoftInputFromWindow(edit_desc.getWindowToken(), 0);
			   			        	
			   			           }
			   			       })
			   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			                dialog.cancel();
			   			         	cf.hidekeyboard();
			   			             spnelevation[spinnerid].setSelection(0);
			   			           }
			   			       }); 
				            
				            builder.show();
				            dialog.cancel();
				            }
			    		});
			    		dialog.show();
					}
				} catch (Exception e) {
					System.out.println("e err + " + e.getMessage());
				}

			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
			
		}
	}

	
	
	public void dispfirstimg(final int selid,String photocaptions) {
		
		
		System.gc();
		currnet_rotated=0;
		
		delimagepos = selid;
		phtodesc = photocaptions;

		String k,description="";
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		final Dialog dialog1 = new Dialog(GeneralImages.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1
				.findViewById(R.id.updateimage);
		Reup.setVisibility(View.VISIBLE);
		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */

		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
		final EditText upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
		upd_Ed.setFocusable(false);
		//upd_Ed.setFocusableInTouchMode(true);
		Button btn_helpclose = (Button) dialog1
				.findViewById(R.id.imagehelpclose);
		Button btn_up = (Button) dialog1.findViewById(R.id.update);
		Button btn_del = (Button) dialog1.findViewById(R.id.delete);
		Button rotateleft = (Button) dialog1.findViewById(R.id.rotateleft);
		Button rotateright = (Button) dialog1.findViewById(R.id.rotateright);
		 upd_Ed.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 
				cf.setFocus(upd_Ed);
				return false;
			}
		});
		// update and delete button function

	
		btn_up.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				updstr = upd_Ed.getText().toString();

				try {
					if (!updstr.trim().equals("")) {
						dialog1.setCancelable(true);
						dialog1.dismiss();
						cf.db.execSQL("UPDATE " + cf.HazardImageTable
								+ " SET Description='"
								+ cf.convertsinglequotes(updstr) + "',ModifiedOn='"
								+ md + "',Nameext='"
								+ cf.convertsinglequotes(arrpath[delimagepos])
								+ "' WHERE SrID ='" + cf.Homeid
								+ "' and Elevation='" + elev + "' and ImageName='"
								+ cf.convertsinglequotes(arrpath[delimagepos]) + "'");

						cf.ShowToast("Updated sucessfully.", 1);

					} else {
						cf.ShowToast("Please enter the caption.", 1);cf.hidekeyboard();
					}
					/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							 ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  GeneralImages.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							File fout = new File(arrpath[selid]);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(arrpath[selid]));
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
				} catch (Exception e) {
					System.out.println("erre " + e.getMessage());
				}
				/**Save the rotated value in to the external stroage place ends **/
				//commonfunction();
				showimages();

			}

		});
		
		btn_del.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				AlertDialog.Builder builder = new AlertDialog.Builder(GeneralImages.this);
				builder.setMessage(
						"Are you sure, Do you want to delete the image?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										try {
											Cursor or = cf.db.rawQuery(
													"select ImageOrder from "
															+ cf.HazardImageTable
															+ " WHERE  SrID ='"
															+ cf.Homeid
															+ "' and Elevation='"
															+ elev
															+ "' and ImageName='"
															+ cf.convertsinglequotes(arrpath[delimagepos])
															+ "'", null);
											or.moveToFirst();
											int image_order = or.getInt(or.getColumnIndex("ImageOrder"));

											cf.db.execSQL("UPDATE "
													+ cf.HazardImageTable
													+ " SET Delflag=1,ImageOrder=10 WHERE SrID ='"
													+ cf.Homeid
													+ "' and Elevation='"
													+ elev
													+ "' and ImageName='"
													+ cf.convertsinglequotes(arrpath[delimagepos])
													+ "'");

											if (image_order < 8 && image_order > 0) {
												for (int m = image_order; m <= 8; m++) {
													int k = m + 1;
													cf.db.execSQL("UPDATE "
															+ cf.HazardImageTable
															+ " SET ImageOrder='"
															+ m + "' WHERE SrID ='"
															+ cf.Homeid
															+ "' and Elevation='"
															+ elev
															+ "' and ImageOrder='"
															+ k + "'");

												}
											}




										} catch (Exception e) {
											System.out.println("exception e  "
													+ e);
										}
										cf.ShowToast("Image has been deleted sucessfully.",	1);

										chk = 0;
										try {
											Cursor c11 = cf.db.rawQuery(
													"SELECT * FROM "
															+ cf.HazardImageTable
															+ " WHERE SrID='"
															+ cf.Homeid
															+ "' and Elevation='"
															+ elev
															+ "' and Delflag=0 ",
													null);

											int delchkrws = c11.getCount();
											if (delchkrws == 0) {
												if (elev == 1) {
													exttick.setVisibility(v1.GONE);
												} else if (elev == 2) {
													intetick.setVisibility(v1.GONE);
												} else if (elev == 3) {
													additick.setVisibility(v1.GONE);
												}
											}



										} catch (Exception e) {

										}
										showimages();

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.show();
			}
			
		});
		btn_helpclose.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
		});
		
		rotateleft.setOnClickListener(new OnClickListener() {  			
			public void onClick(View v) {

				// TODO Auto-generated method stub			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<=0)
				{
					currnet_rotated=270;
				}				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}

			
			}
		});
		rotateright.setOnClickListener(new OnClickListener() {  	
			public void onClick(View v) {

				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				Bitmap myImg = null;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b); 
 
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}
			
			}
		});
		// update and delete button function ends
		try {
			if (chk == 1) {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]),
						null, o);
			} else {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[0]),
						null, o);
			}
			k = "1";

		} catch (FileNotFoundException e) {
			k = "2";

		}
		if (k.equals("1")) {
			final int REQUIRED_SIZE = 400;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap = null;
			try {
				Cursor c11 = cf.db.rawQuery(
						"SELECT * FROM " + cf.HazardImageTable + " WHERE SrID='"
								+ cf.Homeid + "' and Elevation='" + elev
								+ "' and ImageName='"
								+ cf.convertsinglequotes(arrpath[selid])
								+ "' and Delflag=0", null);

				c11.moveToFirst();
				
				int Column1 = c11.getColumnIndex("Description");
				description = cf.getsinglequotes(c11.getString(Column1));


				if (chk == 1) {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[selid]), null, o2);
					upd_Ed.setText(description);

				} else {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[0]), null, o2);
					upd_Ed.setText(cf.getsinglequotes(arrpathdesc[0]));
				}
			} catch (FileNotFoundException e) {
				System.out.println("sdsdsdsdsd "+e.getMessage()); 
				
				e.printStackTrace();
			} 
			rotated_b=bitmap;
			BitmapDrawable bmd = new BitmapDrawable(bitmap);
			
			upd_img.setImageDrawable(bmd);

			dialog1.setCancelable(false);
			dialog1.show();
		} else {
			dialog1.setCancelable(false);
			dialog1.show();
		}
		upd_Ed.clearFocus();
		//Ends
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.img01:
			Intent persint = new Intent(GeneralImages.this, PersonalInfo.class);
			persint.putExtra("homeid", cf.Homeid);
			persint.putExtra("InspectionType", cf.InspectionType);
			persint.putExtra("status", cf.status);
			persint.putExtra("keyName", cf.value);
			persint.putExtra("Count", cf.Count);
			startActivity(persint);
			break;
		case R.id.img02:
			Intent intque = new Intent(GeneralImages.this, BuildCode.class);
			intque.putExtra("homeid", cf.Homeid);
			intque.putExtra("iden", "test");
			intque.putExtra("InspectionType", cf.InspectionType);
			intque.putExtra("status", cf.status);
			intque.putExtra("keyName", cf.value);
			intque.putExtra("Count", cf.Count);
			startActivity(intque);
			break;
		case R.id.img03:
			Intent intimg = new Intent(GeneralImages.this, ImagesData.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);

			break;
		case R.id.img04:
			Intent fb = new Intent(GeneralImages.this, FeedbackDocuments.class);
			fb.putExtra("homeid", cf.Homeid);
			fb.putExtra("InspectionType", cf.InspectionType);
			fb.putExtra("status", cf.status);
			fb.putExtra("keyName", cf.value);
			fb.putExtra("Count", cf.Count);
			startActivity(fb);
			break;
		case R.id.img06:
			Intent iInspectionList = new Intent(GeneralImages.this,
					OverallComments.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);
			startActivity(iInspectionList);
			break;
		case R.id.img07:
			break;
		case R.id.img08:
			Intent mapimg = new Intent(GeneralImages.this, Maps.class);
			mapimg.putExtra("homeid", cf.Homeid);
			mapimg.putExtra("InspectionType", cf.InspectionType);
			mapimg.putExtra("status", cf.status);
			mapimg.putExtra("keyName", cf.value);
			mapimg.putExtra("Count", cf.Count);
			startActivity(mapimg);
			break;
		case R.id.img09:
			Intent subimg = new Intent(GeneralImages.this, Submit.class);
			subimg.putExtra("homeid", cf.Homeid);
			subimg.putExtra("InspectionType", cf.InspectionType);
			subimg.putExtra("status", cf.status);
			subimg.putExtra("keyName", cf.value);
			subimg.putExtra("Count", cf.Count);
			startActivity(subimg);
			break;
		case R.id.home:
			cf.gohome();
			break;
		case R.id.save:
			Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.HazardImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "' and Delflag=0", null);
			int chkgenrws = c11.getCount();
				if (elev == 1) {
					elev = 2;
					name = "Exterior and Grounds";
					if (chkgenrws != 0) {
						cf.ShowToast(name + " image has been saved sucessfully",1);
					}
					btnshowcommon();
					inteimg.setBackgroundResource(R.drawable.submenusrepeatovr);
					selid = 0;
					btnbrwse.setVisibility(visibility);
					edbrowse.setText("");
					btnupd.setVisibility(v1.GONE);
					showimages();
				} else if (elev == 2) {
					
					elev = 3;
					name = "Interior";
					if (chkgenrws != 0) {
						cf.ShowToast(name + " image has been saved sucessfully",1);
					}
					btnshowcommon();
					addiimg.setBackgroundResource(R.drawable.submenusrepeatovr);
					selid = 0;
					btnbrwse.setVisibility(visibility);
					edbrowse.setText("");
					btnupd.setVisibility(v1.GONE);
					showimages();
				} else if (elev == 3) {
					name = "Additional";
					
					if (chkgenrws != 0) {
						cf.ShowToast(name + " image has been saved sucessfully",1);
					}
					Intent fb1 = new Intent(GeneralImages.this, Maps.class);
					fb1.putExtra("homeid", cf.Homeid);
					fb1.putExtra("InspectionType", cf.InspectionType);
					fb1.putExtra("status", cf.status);
					fb1.putExtra("keyName", cf.value);
					fb1.putExtra("Count", cf.Count);
					startActivity(fb1);
				}

			break;
		case R.id.data:
			Intent genimg = new Intent(GeneralImages.this,
					GeneralConditions.class);
			genimg.putExtra("homeid", cf.Homeid);
			genimg.putExtra("InspectionType", cf.InspectionType);
			genimg.putExtra("status", cf.status);
			genimg.putExtra("keyName", cf.value);
			genimg.putExtra("Count", cf.Count);
			startActivity(genimg);
			break;
		case R.id.additional:

			break;
		case R.id.browsetxt:
			Cursor gendat = cf.db.rawQuery("SELECT * FROM " + cf.HazardImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "' and Delflag=0 ", null);
			int chkrws = gendat.getCount();
			maximumindb=chkrws;
			if (chkrws == 8) {
				cf.ShowToast("Exceed Limit",1);
			} else {
				t = 0;
				String source = "<b><font color=#00FF33>Loading Images. Please wait..."+ "</font></b>";
				 pd = ProgressDialog.show(GeneralImages.this, "",
					Html.fromHtml(source), true);
				 Thread thread = new Thread(GeneralImages.this);
				 thread.start();
				//showgalleryimages();
			}
			break;
		case R.id.browsecamera:
			Cursor c12 = cf.db.rawQuery("SELECT * FROM " + cf.HazardImageTable
					+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
					+ "' and Delflag=0 ", null);
			int chkrws1 = c12.getCount();
			if (chkrws1 == 8) {
				cf.ShowToast("Exceed Limit",1);

			} else {
				t = 1;
				startCameraActivity();
			}
			break;

		
		case R.id.delete:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Are you sure you want to delete the image?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									try
									{
									Cursor or = cf.db.rawQuery(
											"select ImageOrder from "
													+ cf.HazardImageTable
													+ " WHERE  SrID ='"
													+ cf.Homeid
													+ "' and Elevation='"
													+ elev
													+ "' and ImageName='"
													+ cf.convertsinglequotes(arrpath[delimagepos])
													+ "'", null);
									or.moveToFirst();
									int image_order = or.getInt(or.getColumnIndex("ImageOrder"));

									cf.db.execSQL("UPDATE "
											+ cf.HazardImageTable
											+ " SET Delflag=1,ImageOrder=10 WHERE SrID ='"
											+ cf.Homeid
											+ "' and Elevation='"
											+ elev
											+ "' and ImageName='"
											+ cf.convertsinglequotes(arrpath[delimagepos])
											+ "'");
									
									if (image_order < 8 && image_order > 0) {
										for (int m = image_order; m <= 8; m++) {
											int k = m + 1;
											cf.db.execSQL("UPDATE "
													+ cf.HazardImageTable
													+ " SET ImageOrder='"
													+ m + "' WHERE SrID ='"
													+ cf.Homeid
													+ "' and Elevation='"
													+ elev
													+ "' and ImageOrder='"
													+ k + "'");

										}
									}

									} catch (Exception e) {
										System.out.println("exception e  " + e);
									}

									
									
									cf.ShowToast("Image has been deleted sucessfully.",1);
									fstimg.setVisibility(v1.GONE);
									firsttxt.setVisibility(v1.GONE);
									updat.setVisibility(v1.GONE);
									del.setVisibility(v1.GONE);
									chk = 0;
									try {
										Cursor c11 = cf.db.rawQuery(
												"SELECT * FROM "
														+ cf.HazardImageTable
														+ " WHERE SrID='"
														+ cf.Homeid
														+ "' and Elevation='"
														+ elev
														+ "' and Delflag=0 ",
												null);
										int delchkrws = c11.getCount();
										if (delchkrws == 0) {
											if (elev == 1) {
												exttick.setVisibility(v1.GONE);
											} else if (elev == 2) {
												intetick.setVisibility(v1.GONE);
											} else if (elev == 3) {
												additick.setVisibility(v1.GONE);
											}
										}
									} catch (Exception e) {

									}
									showimages();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			builder.show();
			break;
		case R.id.ext:
			elev = 1;
			name = "Exterior and Grounds";
			btnshowcommon();
			extimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			btnupd.setVisibility(v1.GONE);
			showimages();
			break;
		case R.id.inte:
			elev = 2;
			lnrlayout.removeAllViews();
			name = "Interior";
			btnshowcommon();
			inteimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			btnupd.setVisibility(v1.GONE);
			showimages();
			break;
		case R.id.addi:
			elev = 3;
			lnrlayout.removeAllViews();
			name = "Additional";
			btnshowcommon();
			addiimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			selid = 0;
			btnbrwse.setVisibility(visibility);
			edbrowse.setText("");
			btnupd.setVisibility(v1.GONE);
			showimages();
			break;

		}
	}

	public int getImageOrder(int Elevationtype, String srid) {
		Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.HazardImageTable
				+ " WHERE SrID='" + srid + "' and Elevation='" + Elevationtype
				+ "' and Delflag=0 order by CreatedOn DESC", null);
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			ImageOrder = imgrws + 1;
		} else {
			ImageOrder = imgrws + 1;

		}
		return ImageOrder;
	}

	private void btnshowcommon() {
		// TODO Auto-generated method stub
		extimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		inteimg.setBackgroundResource(R.drawable.submenusrepeatnor);
		addiimg.setBackgroundResource(R.drawable.submenusrepeatnor);

	}

	public void show() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				GeneralImages.this);
		builder.setTitle("Pick an option");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("gallery")) {
					t = 0;
					showgalleryimages();
				} else {
					t = 1;
					startCameraActivity();
				}
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	protected void showgalleryimages() {
		// TODO Auto-generated method stub
		patharr = null;
		Cursor c11 = cf.db.rawQuery("SELECT * FROM " + cf.HazardImageTable
				+ " WHERE SrID='" + cf.Homeid + "' and Elevation='" + elev
				+ "' and Delflag=0", null);
		maxlevel = rws = c11.getCount();
		patharr = new String[8];
		maxclick = 0;
		final Dialog dialog = new Dialog(GeneralImages.this);
		dialog.setContentView(R.layout.main);
		dialog.setTitle("Select Images");
		dialog.setCancelable(true);
		try {

			dialog.show();

			final GridView imagegrid = (GridView) dialog
					.findViewById(R.id.PhoneImageGrid);
			final ImageView immain = (ImageView) dialog
					.findViewById(R.id.full_image);
			final Button selectBtn = (Button) dialog
					.findViewById(R.id.selectBtn);
			final Button cancelBtn = (Button) dialog
					.findViewById(R.id.cancelBtn);
			final TextView tv1 = (TextView) dialog.findViewById(R.id.path);
			imageAdapter = new ImageAdapter(imagegrid, immain, selectBtn,
					cancelBtn, tv1);
			imagegrid.setAdapter(imageAdapter);

			cancelBtn.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (immain.isShown()) {
						cancelBtn.setText("Cancel");
						selectBtn.setVisibility(View.VISIBLE);
						tv1.setVisibility(View.GONE);
						immain.setVisibility(View.GONE);
						imagegrid.setVisibility(View.VISIBLE);
					} else {
						dialog.cancel();
					}
				}
			});

			selectBtn.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					try {
						if (immain.isShown()) {
							if (cb1.isChecked() == true) {
								cf.ShowToast("Image has been selected already.",1);
								cancelBtn.setText("Cancel");
								selectBtn.setText("Upload");
								dialog.setTitle("Select Images");
								selectBtn.setVisibility(View.VISIBLE);
								tv1.setVisibility(View.GONE);
								immain.setVisibility(View.GONE);
								imagegrid.setVisibility(View.VISIBLE);
							} else

								cancelBtn.setText("Cancel");
							selectBtn.setText("Upload");
							dialog.setTitle("Select Images");
							selectBtn.setVisibility(View.VISIBLE);
							tv1.setVisibility(View.GONE);
							immain.setVisibility(View.GONE);
							imagegrid.setVisibility(View.VISIBLE);
							boolean bu = upr.common(paht);
							if (bu) {
								if (maxlevel < 8) {
									try {
										cb1.setChecked(true);
										Cursor c11 = cf.db
												.rawQuery(
														"SELECT * FROM "
																+ cf.HazardImageTable
																+ " WHERE SrID='"
																+ cf.Homeid
																+ "' and Elevation='"
																+ elev
																+ "' and Delflag=0 and Nameext='"
																+ cf.convertsinglequotes(picname)
																+ "'", null);
										isexist = c11.getCount();
										if (isexist == 0) {
											thumbnailsselection[id1] = true;
											patharr[maxclick] = paht;
											maxclick++;
											maxlevel++;
										} else {
											cb1.setChecked(false);
											cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
										}

									} catch (Exception e) {
										System.out.println("rror in else " + e);
									}
								} else {

									cb1.setChecked(false);
									cf.ShowToast("You are allowed to select only 8 Images per Elevation.",1);

								}
							} else {
								cb1.setChecked(false);
								cf.ShowToast("Your file size exceeds 2MB.",1);

							}
						} else {
							
							dialog.cancel();
							if(elev==1){name ="Exterior and Grounds";}
    	 	 				else if(elev==2){name ="Interior";}
    	 	 				else if(elev==3){name ="Additional";}
							for (int i = 0; i < patharr.length; i++) {
								if (patharr[i] != null) {
									Cursor c14 = cf.db.rawQuery(
											"SELECT * FROM "
													+ cf.HazardImageTable
													+ " WHERE SrID='"
													+ cf.Homeid
													+ "' and Elevation='"
													+ elev + "'", null);
									int count_tot = c14.getCount();
									String[] bits = patharr[i].split("/");
									picname = bits[bits.length - 1];
									ImageOrder = getImageOrder(elev, cf.Homeid);
									cf.db.execSQL("INSERT INTO "
											+ cf.HazardImageTable
											+ " (SrID,Ques_Id,Elevation,ImageName,Description,CreatedOn,ModifiedOn,Nameext,ImageOrder,Delflag)"
											+ " VALUES ('"
											+ cf.Homeid
											+ "','"
											+ quesid
											+ "','"
											+ elev
											+ "','"
											+ cf.convertsinglequotes(patharr[i])
											+ "','"
											+ cf.convertsinglequotes(name
													+ " ELevation "
													+ (count_tot + 1)) + "','"
											+ cd + "','" + md + "','"
											+ cf.convertsinglequotes(picname)
											+ "','" + ImageOrder + "','" + 0
											+ "')");
									if (elev == 1) {
										exttick.setVisibility(visibility);
									} else if (elev == 2) {
										intetick.setVisibility(visibility);
									} else if (elev == 3) {
										additick.setVisibility(visibility);
									}
								}

							}
							showimages();
						}
					} catch (Exception e) {
						System.out.println("e" + e.getMessage());
					}
				}
			});

		} catch (Exception m) {

			System.out.println("problem occure " + m);
		}

	}

	public class ImageAdapter extends BaseAdapter {
		private LayoutInflater mInflater;
		GridView gi;
		ImageView immain;
		Button s, c;

		private TextView edt;

		public ImageAdapter(GridView imagegrid, ImageView immain,
				Button selectBtn, Button cancelBtn, TextView ed) {
			c = cancelBtn;
			s = selectBtn;
			gi = imagegrid;
			this.immain = immain;
			this.edt = ed;
			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return count;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.galleryitem, null);
				holder.imageview = (ImageView) convertView
						.findViewById(R.id.thumbImage);
				holder.checkbox = (CheckBox) convertView
						.findViewById(R.id.itemCheckBox);
				holder.r = (RelativeLayout) convertView.findViewById(R.id.Rid);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.checkbox.setId(position);
			holder.imageview.setId(position);
			holder.r.setId(position);
			holder.checkbox.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					CheckBox cb = (CheckBox) v;
					int id = cb.getId();
					if (thumbnailsselection[id]) {
						cb.setChecked(false);
						try {
							thumbnailsselection[id] = false;

							String temp[] = new String[8];
							;

							int j = 0;
							for (int i = 0; i < patharr.length; i++) {
								if (patharr[i] != null) {
									if (arrPath[id] != patharr[i]) {

										temp[i] = patharr[j];

										j++;
									}
								}
							}
							patharr = null;
							patharr = temp.clone();
							maxclick--;
							maxlevel--;
						} catch (Exception e) {
							System.out.println("this shows " + e);
						}
					} else {
						if (maxlevel < 8) {
							try {

								boolean bu = upr.common(arrPath[id]);
								if (bu) {
									cb.setChecked(true);
									String[] bits = arrPath[id].split("/");
									picname = bits[bits.length - 1];
									Cursor c11 = cf.db
											.rawQuery(
													"SELECT * FROM "
															+ cf.HazardImageTable
															+ " WHERE SrID='"
															+ cf.Homeid
															+ "' and Elevation='"
															+ elev
															+ "' and Delflag=0 and Nameext='"
															+ cf.convertsinglequotes(picname)
															+ "'", null);
									isexist = c11.getCount();
									if (isexist == 0) {
										thumbnailsselection[id] = true;
										patharr[maxclick] = arrPath[id];
										maxclick++;
										maxlevel++;
									} else {
										cb.setChecked(false);
										cf.ShowToast("Selected image has been uploaded already. Please select another image",1);

									}
								} else {
									cb.setChecked(false);
									cf.ShowToast("Your file size exceeds 2MB.",1);
								}
							} catch (Exception e) {
								System.out.println("rror in else " + e);
							}
						} else {
							cb.setChecked(false);
							cf.ShowToast("You are allowed to upload only 8 images",1);

						}
					}

				}
			});
			holder.imageview.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {
						ImageView im = (ImageView) v;
						int id = im.getId();
						id1 = im.getId();
						paht = arrPath[id];
						cb1 = (CheckBox) holder.checkbox.findViewById(id);

						Uri uri = Uri.parse(paht);
						immain.setImageURI(uri);
						String[] bits = paht.split("/");
						picname = bits[bits.length - 1];
						s.setText("Select");
						c.setText("Cancel");
						edt.setVisibility(View.VISIBLE);
						edt.setText(paht);
						immain.setVisibility(View.VISIBLE);
						gi.setVisibility(View.GONE);

					} catch (Exception e) {
						System.out.println("proiblem" + e);
					}
					// /dialog.cancel();
				}
			});
			Bitmap b = thumbnails[position];
			holder.imageview.setImageBitmap(b);
			holder.checkbox.setChecked(thumbnailsselection[position]);
			holder.id = position;
			if (b == null) {

				holder.r.setVisibility(View.GONE);

			} else {
				holder.r.setVisibility(View.VISIBLE);
			}
			return convertView;

		}

	}

	class ViewHolder {
		ImageView imageview;
		CheckBox checkbox;
		RelativeLayout r;
		int id;
	}

	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);

	}

	protected void pickfromgallery() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				SELECT_PICTURE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (t == 0) {
			if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);
					String[] bits = selectedImagePath.split("/");
					picname = bits[bits.length - 1];
				}
			} else {
				selectedImagePath = "";
				edbrowse.setText("");
			}
		} else if (t == 1) {
			switch (resultCode) {
			case 0:
				break;
			case -1:
				try {

					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					capturedImageFilePath = cursor.getString(column_index_data);
					selectedImagePath = capturedImageFilePath;
					display_taken_image(selectedImagePath);
					
					showimages();
				} catch (Exception e) {
					System.out.println("my exception " + e);
				}
				break;

			}

		}

	}
	public void display_taken_image(final String slectimage)
	{
		 System.gc();
		 globalvar=0;
		BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;

			final Dialog dialog1 = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
		Reup.setVisibility(View.GONE);
		LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
		camerapic.setVisibility(View.VISIBLE);
		TextView tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
		tvcamhelp.setText("Save Picture");
		tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
		 ((RelativeLayout)dialog1.findViewById(R.id.cam_photo_update)).setVisibility(View.VISIBLE);
		((RelativeLayout)dialog1.findViewById(R.id.camaddcaption)).setVisibility(View.GONE);
		((TextView) dialog1.findViewById(R.id.elevtxt)).setVisibility(View.GONE);
		((Spinner) dialog1.findViewById(R.id.cameraelev)).setVisibility(View.GONE);
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		
		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
		//((Button) dialog1.findViewById(R.id.camsave)).setVisibility(View.GONE);
		
		final Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
		
		Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
		Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
		
		try {
				BitmapFactory.decodeStream(new FileInputStream(slectimage),
						null, o);
				final int REQUIRED_SIZE = 400;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
					BitmapFactory.Options o2 = new BitmapFactory.Options();
	 			o2.inSampleSize = scale;
	 			Bitmap bitmap = null;
	 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
	 	    			slectimage), null, o2);
	 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
				   upd_img.setImageDrawable(bmd);
				   rotated_b=bitmap;
				}
	}
		catch(Exception e)
		{
			
		}
		btn_save.setOnClickListener(new OnClickListener() {
	        public void onClick(View v) {
	        	if(globalvar==0)
	    		{

	        	globalvar=1;
	        	
	        	btn_save.setVisibility(v.GONE);
	        	String[] bits = slectimage.split("/");
	    		String picname = bits[bits.length - 1];
	    			Cursor c15 = cf.db.rawQuery("SELECT * FROM "
						+ cf.HazardImageTable + " WHERE SrID='" + cf.Homeid
						+ "' and Elevation='" + elev + "' and Delflag=0", null);
				int count_tot = c15.getCount();
				int ImgOrder = getImageOrder(elev, cf.Homeid);
				if(elev==1){name ="Exterior and Grounds";}
	 				else if(elev==2){name ="Interior";}
	 				else if(elev==3){name ="Additional";}
	    		try {
	    			cf.db.execSQL("INSERT INTO "
							+ cf.HazardImageTable
							+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','"
							+ quesid
							+ "','"
							+ elev
							+ "','"
							+ cf.convertsinglequotes(selectedImagePath)
							+ "','"
							+ cf.convertsinglequotes(name + " Elevation "
									+ (count_tot + 1)) + "','"
							+ cf.convertsinglequotes(picname) + "','" + cd
							+ "','" + md + "','" + ImageOrder + "','" + 0
							+ "')");
					if (elev == 1) {
						exttick.setVisibility(visibility);
					} else if (elev == 2) {
						intetick.setVisibility(visibility);
					} else if (elev == 3) {
						additick.setVisibility(visibility);
					}

	    		} catch (Exception e) {
	System.out.println("Exception "+e.getMessage());
				}
	    		/**Save the rotated value in to the external stroage place **/
				if(currnet_rotated>0)
				{ 

					try
					{
						/**Create the new image with the rotation **/
						String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
						 ContentValues values = new ContentValues();
						  values.put(MediaStore.Images.Media.ORIENTATION, 0);
						  GeneralImages.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
						
						
						if(current!=null)
						{
						String path=getPath(Uri.parse(current));
						File fout = new File(slectimage);
						fout.delete();
						/** delete the selected image **/
						File fin = new File(path);
						/** move the newly created image in the slected image pathe ***/
						fin.renameTo(new File(slectimage));
						
						
					}
					} catch(Exception e)
					{
						System.out.println("Error occure while rotate the image "+e.getMessage());
					}
					
				}
			
			/**Save the rotated value in to the external stroage place ends **/
				cf.ShowToast("Selected image saved successfully", 1);
				showimages();
				dialog1.setCancelable(true);
	        	dialog1.dismiss();

	    		}
	        }
	        
	    });
	rotatecamright.setOnClickListener(new OnClickListener() {  
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					System.out.println("rota eeee "+e.getMessage());
				}

				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(slectimage, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
					}
				}
			}
		});
	rotatecamleft.setOnClickListener(new OnClickListener() {  
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			 System.gc();
			 currnet_rotated-=90;
				if(currnet_rotated<=0)
				{
					currnet_rotated=270;
				}	
			
			Bitmap myImg;
			try {
				myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
				Matrix matrix =new Matrix();
				matrix.reset();
				matrix.postRotate(currnet_rotated);
				rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
				System.gc();
				upd_img.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e)
			{
				System.out.println("rota eeee "+e.getMessage());
			}

			catch (OutOfMemoryError e) {
				System.out.println("comes in to out ot mem exception");
				System.gc();
				try {
					myImg=null;
					System.gc();
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.postRotate(currnet_rotated);
					myImg= cf.ShrinkBitmap(slectimage, 800, 800);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b); 

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 catch (OutOfMemoryError e1) {
						// TODO Auto-generated catch block
					 cf.ShowToast("You can not rotate this image. Image size is too large.",1);
				}
			}
		}
	});
		btn_helpclose.setOnClickListener(new OnClickListener() {
	        public void onClick(View v) {
	        	dialog1.setCancelable(true);
	        	dialog1.dismiss();
	        }
		});
		
		dialog1.setCancelable(false);
		dialog1.show();
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	private void dynamicimagesparent() {
		// TODO Auto-generated method stub
		lnrlayoutparent.removeAllViews();
		//titleheader = new TextView(this);
		if(finaltext.equals("")||finaltext.equals("null"))
		{			
			titleheader.setText("/mnt/sdcard/");
			childtitleheader.setText("sdcard");
		}
		else
		{
			if(backclick==1)
			{
				titleheader.setText(pathofimage);backclick=0;
			}
			else
			{
				titleheader.setText(finaltext);
			}
		}
		if(pieces1!=null)
		{
			tvstatusparent = new TextView[pieces1.length];
			imgviewparent = new ImageView[pieces1.length];		
		
		for (int i = 0; i < pieces1.length; i++) {
			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			lnrlayoutparent.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					50, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.leftMargin = 10;
			paramschk.topMargin = 5;
			paramschk.bottomMargin = 15;
			l2.addView(lchkbox);

			imgviewparent[i] = new ImageView(this);
			imgviewparent[i].setBackgroundResource(R.drawable.allfilesicon);
			lchkbox.addView(imgviewparent[i], paramschk);

			LinearLayout ltextbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramstext.topMargin = 5;
			paramstext.leftMargin = 15;
			l2.addView(ltextbox,paramstext);
			
			tvstatusparent[i] = new TextView(this);
			tvstatusparent[i].setText(pieces1[i]);
			tvstatusparent[i].setTextColor(Color.BLACK);
			tvstatusparent[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			//tvstatusparent[i].setTextSize(16);
			tvstatusparent[i].setMinimumWidth(100);
			mapfolder.put("tvstatusparent" + i, tvstatusparent[i]);
			tvstatusparent[i].setTag("tvstatusparent" + i);
			imgviewparent[i].setTag("tvstatusparent" + i);
			ltextbox.addView(tvstatusparent[i], paramstext);
			
			tvstatusparent[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					
					pieces1=null;pieces3=null;pieces2=null;
					String gettagvaluparent = v.getTag().toString();
					String repidofselected = gettagvaluparent.replace("tvstatusparent","");
					final int inconevalueparent = Integer.parseInt(repidofselected) + 1;
					textviewvalue_parent = mapfolder.get(gettagvaluparent);
					subfoldersclick(inconevalueparent,textviewvalue_parent);
				}
			});
			
			imgviewparent[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					pieces1=null;pieces3=null;pieces2=null;
					String gettagvaluparent = v.getTag().toString();
					String repidofselected = gettagvaluparent.replace("tvstatusparent","");
					final int inconevalueparent = Integer.parseInt(repidofselected) + 1;
					textviewvalue_parent = mapfolder.get(gettagvaluparent);
					if(backclick==1)
					{
					String[] bits = finaltext.split("/");
					String picname1 = bits[bits.length - 1];
					childtitleheader.setText(picname1); // set the chaild title 
					}
					else
					{
						String[] bits = pathofimage.split("/");
						String picname1 = bits[bits.length - 1];
						childtitleheader.setText(picname1); // set the chaild title 
					}
					subfoldersclick(inconevalueparent,textviewvalue_parent);
				}
			});
			
		}
		}
		
		else
		{
			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			lnrlayoutparent.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.leftMargin = 15;
			paramschk.topMargin = 20;
			l2.addView(lchkbox);
			
			
			TextView tvnorecordparent = new TextView(this);
			tvnorecordparent.setText("No Sub Folders");
			tvnorecordparent.setTextColor(Color.BLACK);
			tvnorecordparent.setMinimumWidth(200);
			lchkbox.addView(tvnorecordparent,paramschk);
		}
		
	}
	private void dynamicimageschild() {
		
		lnrlayoutchild.removeAllViews();
		if((pieces3==null))
		{
			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			lnrlayoutchild.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.leftMargin = 15;
			l2.addView(lchkbox);
			
			
			TextView tvnorecordchild = new TextView(this);
			tvnorecordchild.setText("No Records Found");
			tvnorecordchild.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			//tvnorecordchild.setTextSize(16);
			tvnorecordchild.setTextColor(Color.BLACK);
			tvnorecordchild.setMinimumWidth(200);
			lchkbox.addView(tvnorecordchild,paramschk);
		}
		
		else
		{	
			tvstatuschild = new TextView[pieces3.length];
			thumbviewimgchild = new ImageView[pieces3.length];
			chkboxchild = new CheckBox[pieces3.length];
			//imgview1 = new ImageView[pieces3.length];
			thumbnails1 = new Bitmap[pieces3.length];
		
			for (int i = 0; i < pieces3.length; i++) {
			
					
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				LinearLayout.LayoutParams paramschk1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk1.topMargin = 15;
				lnrlayoutchild.addView(l2,paramschk1);
				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
						50, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.leftMargin = 5;
				paramschk.topMargin = 5;
				paramschk.bottomMargin = 15;
				l2.addView(lchkbox);
				LinearLayout ltextbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
						300, ViewGroup.LayoutParams.WRAP_CONTENT);
				thumbviewimgchild[i] = new ImageView(this);
				chkboxchild[i] = new CheckBox(this);
			
				chkboxchild[i].setPadding(30, 0, 0, 0);
					lchkbox.addView(chkboxchild[i], paramschk);
					paramstext.topMargin = 5;
					paramstext.leftMargin = 15;
					l2.addView(ltextbox);
					
					
					LinearLayout ltextbox6= new LinearLayout(this);
					LinearLayout.LayoutParams paramstext6 = new LinearLayout.LayoutParams(
							75, 75);
					//paramstext6.topMargin = 5;
					paramstext6.leftMargin = 10;
					ltextbox.addView(ltextbox6);
					
					BitmapFactory.Options o = new BitmapFactory.Options();
					o.inJustDecodeBounds = true;
					try {
						BitmapFactory.decodeStream(new FileInputStream(titleheader.getText().toString()+pieces3[i]),
								null, o);
						j = "1";
					} catch (FileNotFoundException e) {
						j = "2";
					}
					if (j.equals("1")) {
						final int REQUIRED_SIZE = 100;
						int width_tmp = o.outWidth, height_tmp = o.outHeight;
						int scale = 1;
						while (true) {
							if (width_tmp / 2 < REQUIRED_SIZE
									|| height_tmp / 2 < REQUIRED_SIZE)
								break;
							width_tmp /= 2;
							height_tmp /= 2;
							scale *= 2;
						}
						// Decode with inSampleSize
						BitmapFactory.Options o2 = new BitmapFactory.Options();
						o2.inSampleSize = scale;
						Bitmap bitmap = null;
						try {
							bitmap = BitmapFactory.decodeStream(new FileInputStream(
									titleheader.getText().toString()+pieces3[i]), null, o2);
							
						
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						BitmapDrawable bmd = new BitmapDrawable(bitmap);
						thumbviewimgchild[i].setImageDrawable(bmd);
						
					ltextbox6.addView(thumbviewimgchild[i], paramstext6);
					
					}
					LinearLayout ltextbox1= new LinearLayout(this);
					LinearLayout.LayoutParams paramstext1 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramstext1.topMargin = 5;
					paramstext1.leftMargin = 15;
					ltextbox6.addView(ltextbox1);
					
					tvstatuschild[i] = new TextView(this);
					tvstatuschild[i].setText(pieces3[i]);
					tvstatuschild[i].setTextColor(Color.BLACK);
					tvstatuschild[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
					//tvstatuschild[i].setTextSize(16);
					tvstatuschild[i].setMinimumWidth(100);
					tvstatuschild[i].setTag("tvstatus" + i);
					thumbviewimgchild[i].setTag("tvstatus" + i);
					chkboxchild[i].setTag("tvstatus" + i);
					
				
					for(int j=0;j<selectedcount;j++)
					{
						if(selectedtestcaption[j].equals(titleheader.getText().toString()+pieces3[i]))
						{
							chkboxchild[i].setChecked(true);
						}
					}
					mapfolder.put("tvstatus" + i, tvstatuschild[i]);
					ltextbox1.addView(tvstatuschild[i], paramstext1);
					
				chkboxchild[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						String getidofselbtn = v.getTag().toString();
						String repidofselbtn = getidofselbtn.replace("tvstatus","");
						int chk = Integer.parseInt(repidofselbtn);
						final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
						textviewvalue_child = mapfolder.get(getidofselbtn);
						boolean bu = upr.common(titleheader.getText().toString()+textviewvalue_child.getText().toString());
						if (bu) {
								if(chkboxchild[chk].isChecked()==false)
								{
									selectedtestcaption=Delete_from_array(selectedtestcaption,titleheader.getText().toString()+textviewvalue_child.getText().toString());
									selectedcount=selectedcount-1;
								}
								else
								{
									Bitmap b=ShrinkBitmap(titleheader.getText().toString()+textviewvalue_child.getText().toString(), 400, 400);
									if(b!=null)
									{
										if((maximumindb+selectedcount)<8)
										{
											Cursor c11=null;
											try
											{
												 c11 = cf.db
														.rawQuery(
																"SELECT * FROM "
																		+ cf.HazardImageTable
																		+ " WHERE SrID='"
																		+ cf.Homeid
																		+ "' and Elevation='"
																		+ elev
																		+ "' and Delflag=0 and ImageName='"
																		+ cf.convertsinglequotes(titleheader.getText().toString()+textviewvalue_child.getText().toString())
																		+ "' order by ImageOrder",
																null);
											
											}
											catch(Exception e)
											{
												//cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection in galary file multi selection page photos Error="+e.getMessage());
											}
												
															isexist = c11.getCount();
															
															if (isexist == 0) {
																selectedtestcaption=dynamicarraysetting(titleheader.getText().toString()+pieces3[chk],selectedtestcaption);
																selectedcount++;
																
															} else {
																
																chkboxchild[chk].setChecked(false);
																cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
															}
											}
											else
											{
												chkboxchild[chk].setChecked(false);
												cf.ShowToast("You are allowed to select only 8 Images per Elevation.",1);
											}
									}
									else
									{
										cf.ShowToast("This image is not a supported format.You can not upload.",1);
										chkboxchild[chk].setChecked(false);
									}
								}
						}else {
							chkboxchild[chk].setChecked(false);
							cf.ShowToast("Your file size exceeds 2MB.",1);

					}
					}
					
				
					});
					thumbviewimgchild[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						String getidofselbtn = v.getTag().toString();
						repidofselbtn1= getidofselbtn.replace("tvstatus","");
						cvrtstr1 = Integer.parseInt(repidofselbtn1) + 1;
						textviewvalue_child = mapfolder.get(getidofselbtn);
						
						//boolean bu = upr.common(textviewvalue_child.getText().toString());
						//if (bu) {
						if ((titleheader.getText().toString().endsWith(".jpg"))||(titleheader.getText().toString().endsWith(".JPG"))||(titleheader.getText().toString().endsWith(".jpeg"))||(titleheader.getText().toString().endsWith(".png"))||(titleheader.getText().toString().endsWith(".gif")
				   				 ||(titleheader.getText().toString().endsWith(".JPEG"))||(titleheader.getText().toString().endsWith(".PNG")))||(titleheader.getText().toString().endsWith(".gif"))||(titleheader.getText().toString().endsWith(".bmp"))){
							
							
							String[] bits = titleheader.getText().toString().split("/");	
							String picname = bits[bits.length - 1];
							String val = titleheader.getText().toString().replace(picname, textviewvalue_child.getText().toString());
							
							titleheader.setText(val);
						
						}
						else
						{
							if(titleheader.getText().toString()=="mnt/sdcard/")
							{
								titleheader.setText(titleheader.getText().toString()+textviewvalue_child.getText().toString());
							}
							else
							{
								titleheader.setText(titleheader.getText().toString()+textviewvalue_child.getText().toString());
							}
								
						}
						
						
						dialogfullimage = new Dialog(GeneralImages.this);
						dialogfullimage.setContentView(R.layout.alertfullimage);
						dialogfullimage.setTitle("Select Images");
						dialogfullimage.setCancelable(true);
		    	 		fullimageview = (ImageView) dialogfullimage.findViewById(R.id.full_image);
		    	 		selectfullimage = (Button) dialogfullimage.findViewById(R.id.selectBtnfullimage);
		    	 		cancelfullimage = (Button) dialogfullimage.findViewById(R.id.cancelBtnfullimage);
		    	 		imagepath = (TextView) dialogfullimage.findViewById(R.id.imagepath);
		    	 		imagepath.setText(titleheader.getText().toString());
		    	 		
		    	 		Bitmap b = ShrinkBitmap(titleheader.getText().toString(), 130, 130);
		    	 		fullimageview.setImageBitmap(b);
		    	 		
		    			
		    			cancelfullimage.setOnClickListener(new OnClickListener() {

		    				public void onClick(View v) {

		    					//pieces1=null;pieces2=null;pieces3=null;
		    					String value = titleheader.getText().toString();
		    					String[] bits = value.split("/");
		    					String picname = bits[bits.length - 1];
		    					titleheader.setText(value.replace(picname, ""));
		    					dialogfullimage.setCancelable(true);
		    					dialogfullimage.cancel();
		    				}
		    			});
		    			selectfullimage.setOnClickListener(new OnClickListener() {
		    				public void onClick(View v) {
		    					boolean bu = upr.common(titleheader.getText().toString());
		    					if (bu) 
		    					{
		    						
		    						Bitmap b=ShrinkBitmap(titleheader.getText().toString(), 400, 400);
									if(b!=null)
									{
		    						
		    							if((maximumindb+selectedcount)<8)
		    							{
		    								Cursor c11=null;
		    								try
		    								{
		    									 c11 = cf.db
			    										.rawQuery(
			    												"SELECT * FROM "
			    														+ cf.HazardImageTable
			    														+ " WHERE SrID='"
			    														+ cf.Homeid
			    														+ "' and Elevation='"
			    														+ elev
			    														+ "' and Delflag=0 and ImageName='"
			    														+ cf.convertsinglequotes(titleheader.getText().toString())//+textviewvalue_child.getText().toString()
			    														+ "' order by ImageOrder",
			    												null);
		    								}catch(Exception e)
											{
												System.out.println("the error was"+e.getMessage());
												//cf.Error_LogFile_Creation("Problem in checking for the image is exisit or not in image selection ="+e.getMessage());
											}
			    	    						
			    								isexist = c11.getCount();
			    								if (isexist == 0) {
			    									if(chkboxchild[Integer.parseInt(repidofselbtn1)].isChecked())
			    									{
			    										cf.ShowToast("You have already selected this image.",1);
			    									}
			    									else
			    									{
			    										selectedtestcaption=dynamicarraysetting(imagepath.getText().toString(),selectedtestcaption );
														selectedcount++;
														chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(true);
			    									}
			    							 
			    								} else {
			    									chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(false);
			    									cf.ShowToast("Selected Image has been uploaded already. Please select another Image.",1);
			    								}
		    							}
		    						else
		    						{
		    							chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(false);
		    							cf.ShowToast("You are allowed to select only 8 Images per Elevation.",1);
		    						
		    						}}
		    						
		
								}else {
									chkboxchild[chk].setChecked(false);
									cf.ShowToast("Your file size exceeds 2MB.",1);

								}
		    					String value = titleheader.getText().toString();
		    					String[] bits = value.split("/");
		    					String picname = bits[bits.length - 1];
		    					
		    					titleheader.setText(value.replace(picname, ""));
		    					dialogfullimage.setCancelable(true);
		    					dialogfullimage.cancel();
		    				}
		    			});
		    			dialogfullimage.setCancelable(false);
		    			dialogfullimage.show();
					}
					
				});
			}
				
			}
	}
	private void subfoldersclick(int inconevalueparent,TextView textviewvalue_parent) {
		// TODO Auto-generated method stub
		String value = titleheader.getText().toString();	
		if(value=="/mnt/sdcard/")
		{
			titleheader.setText(value+textviewvalue_parent.getText().toString()+"/");
		}
		else
		{
				if ((value.endsWith(".jpg"))||(value.endsWith(".JPG"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")
						 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp")))
				{
					String[] bits = value.split("/");
					String picname = bits[bits.length - 1];
					String val = value.replace(picname, textviewvalue_parent.getText().toString());
					titleheader.setText(val);
				}
				else
				{
					titleheader.setText(value+textviewvalue_parent.getText().toString()+"/");
				}
		} 
			if ((value.endsWith(".jpg"))||(value.endsWith(".JPG"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")
					 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp"))){
				dialog = new Dialog(GeneralImages.this);
		 		dialog.setContentView(R.layout.alertfullimage);
		 		dialog.setTitle("Select Images");
		 		dialog.setCancelable(true);
		 		ImageView fullimageview = (ImageView) dialog.findViewById(R.id.full_image);
		 		Button selectBtnfullimage = (Button) dialog.findViewById(R.id.selectBtn);
		 		Button cancelBtnfullimage = (Button) dialog.findViewById(R.id.cancelBtn);
		 	
		 		BitmapFactory.Options o2 = new BitmapFactory.Options();
				int scale = 0;
				o2.inSampleSize = scale;
				Bitmap bitmap = null;
				try {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(titleheader.getText().toString()), null, o2);
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					fullimageview.setImageDrawable(bmd);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				cancelBtnfullimage.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dialog.cancel();
					}
				});
				dialog.show();
			}
			else
			{
				finaltext = titleheader.getText().toString();
				File f = new File(titleheader.getText().toString());
				String[] bits = titleheader.getText().toString().split("/");
				String picname1 = bits[bits.length - 1];
				childtitleheader.setText(picname1);
				walkdir(f);   	
				dynamicimagesparent();
			}
				 dynamicimageschild();

	}
	public void common() {
		img1.setBackgroundResource(R.drawable.policyholdernor);
		img2.setBackgroundResource(R.drawable.questionsnor);
		img3.setBackgroundResource(R.drawable.imagesnor);
		img4.setBackgroundResource(R.drawable.feedbacknor);
		img6.setBackgroundResource(R.drawable.overallcommentsnor);
		img7.setBackgroundResource(R.drawable.generalconditionnor);
		img8.setBackgroundResource(R.drawable.mapnor);
		img9.setBackgroundResource(R.drawable.submitnor);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(GeneralImages.this,
					GeneralConditions.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	Bitmap ShrinkBitmap(String file, int width, int height) {
		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			return null;
		}

	}
	public void run() {
		Looper.prepare();
		try{		// TODO Auto-generated method stub
 			externamstoragepath = Environment.getExternalStorageDirectory(); 
 		     walkdir(externamstoragepath);
	    	 	 		
 			 handler.sendEmptyMessage(0);

 		}
 		catch(Exception e)
 		{
 			System.out.println("problem occure " + e.getMessage());
 		}
	

	}
	private void walkdir(File images2) {
		File[] current_fold_list =images2.listFiles(directoryFilter);
	
	if(current_fold_list!=null)
	{
		pieces1=new String[current_fold_list.length];
		for (int i = 0; i < current_fold_list.length; i++) {
			   
			   if (current_fold_list[i].isDirectory()) {
            
            
            pieces1[i]=current_fold_list[i].getName();
         }
			   else
			 	 {
			 		
			 	 }
		}
		
	}
	/** Get only the folders form the File array list Ends   **/
	/** Get only the Images  form the File array list Starts   **/
	Currentfile_list=images2.listFiles(imageFilter);
	   if (Currentfile_list != null)
	   {
		  int j=0; 
		  
		  if(Currentfile_list.length<=20)
			 
		  pieces3=new String[Currentfile_list.length];
		  else
		  {
			  pieces3=new String[20];
		  }
		  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
		 
		   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
			   
			 
    		
    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".GIF"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
    	            	 
    			 		 pieces3[j]=Currentfile_list[i].getName();
    	            
            		 }
    			 	 else
    			 	 {
    			 		
    			 	 }
    		 
	   }
		  
	 }
	   /** Get only the Images  form the File array list Ends   **/
	   /** Set the visisbolity to  the Prev and next button layout Starts ***/
	   if(Lin_Pre_nex !=null && Currentfile_list.length>20 )
	{
		
		Lin_Pre_nex.setVisibility(View.VISIBLE);
		Rec_count_lin.setVisibility(View.VISIBLE);
			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("Records Displayed from 1 to 20");
			prev.setVisibility(View.GONE);
			next.setVisibility(View.VISIBLE);
		
	}
	else if(Lin_Pre_nex !=null)
	{

		Lin_Pre_nex.setVisibility(View.GONE);
		Rec_count_lin.setVisibility(View.VISIBLE);
		Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("");
		
	}}
	/** File filter to get only the directories from the folder **/
	FileFilter directoryFilter = new FileFilter() {
		public boolean accept(File file) {
			return file.isDirectory();
		}
	};
	/** File filter to get only the directories from the folder **/
	/** File filter to get only the directories from the folder **/
	FileFilter imageFilter = new FileFilter() { 
		 
		public boolean accept(File file) {
			
			 if ((file.getName().endsWith(".jpg"))||(file.getName().endsWith(".jpeg"))||(file.getName().endsWith(".png"))||(file.getName().endsWith(".gif")||(file.getName().endsWith(".JPG"))
	            				 ||(file.getName().endsWith(".JPEG"))||(file.getName().endsWith(".PNG")))||(file.getName().endsWith(".gif"))||(file.getName().endsWith(".bmp"))){
				 
				 
				 
				 return true;
			 }
			 else
			 {
				 return false;
			 }
				 
		}
	};
	/** File filter to get only the directories from the folder **/
	private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
		// TODO Auto-generated method stub
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new String[1];
			
			pieces3[0]=ErrorMsg;
		}
		else
		{
			
			String tmp[]=new String[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
			}
		
			tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 System.out.println("Exception "+e.getMessage());
			
		 }
		return pieces3;
	}
	protected String[] Delete_from_array(String[] selectedtestcaption2,
			String txt) {
		// TODO Auto-generated method stub
		String tmp[]=new String[selectedtestcaption2.length-1];
		int j=0;
		if(selectedtestcaption2!=null)
		{
			for(int i=0;i<selectedtestcaption2.length;i++)
			{
				if(selectedtestcaption2[i]!=null)
				{
					if(!selectedtestcaption2[i].equals(txt))
					{
						tmp[j]=selectedtestcaption2[i];
						j++;
					}
					else
					{
						System.out.println("come in else");
					}
				}
			}
			
		}
		return tmp;
	}
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	//showgalleryimages();pd.dismiss();
        		dialog = new Dialog(GeneralImages.this);
    	 			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

    	 			dialog.setContentView(R.layout.showfolderlist);
    	 	 		
    	 	 		dialog.setCancelable(true);
    	 	 		
    	 	 		lnrlayoutparent = (LinearLayout) dialog.findViewById(R.id.linscrollviewfolder);
    		 		lnrlayoutchild = (LinearLayout) dialog.findViewById(R.id.linscrollviewimages);
    		 		backfolderwise = (Button) dialog.findViewById(R.id.backfolderwise);
    		 		cancelfolderwise = (Button) dialog.findViewById(R.id.cancelfolderwise);
    		 		
    		 		Lin_Pre_nex = (RelativeLayout) dialog.findViewById(R.id.linscrollview_Next);
    		 		prev =(ImageView) dialog.findViewById(R.id.S_I_Prev);
    	 	 		prev.setVisibility(View.GONE);
    	 	 		next =(ImageView) dialog.findViewById(R.id.S_I_Next);
    	 	 		scr2 =(ScrollView) dialog.findViewById(R.id.scr2);
    	 	 		Rec_count_lin =(RelativeLayout) dialog.findViewById(R.id.Rec_count_lin);
    	 	 		Total_Rec_cnt =(TextView) dialog.findViewById(R.id.Total_rec_cnt);
    	 	 		showing_rec_cnt =(TextView) dialog.findViewById(R.id.showing_rec_cnt);
    	 	 		
    	 	 		ImageView Search = (ImageView) dialog.findViewById(R.id.search);
    	 	 		Search.setOnClickListener(new OnClickListener() {
    					public void onClick(View v) {
    						// TODO Auto-generated method stub
    						 //final Dialog dialog1 = new Dialog(photos.this);
    						/**Search based on the folder name starts here **/
    						Search_by_folder(); 
    		    	       /**Search based on the folder name Ends here**/
    		    			
    					}
    				});
    	 	 		if(Currentfile_list.length<=20)
    	 	 		{
    	 	 			Lin_Pre_nex.setVisibility(View.GONE);
    	 	 			Rec_count_lin.setVisibility(View.VISIBLE);
    	 	 			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
    	 	 			showing_rec_cnt.setText("");
    	 	 			
    	 	 		}
    	 	 		else
    	 	 		{
    	 	 			Lin_Pre_nex.setVisibility(View.VISIBLE);
    	 	 			Rec_count_lin.setVisibility(View.VISIBLE);
    	 	 			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
    	 	 			showing_rec_cnt.setText("Records Displayed from 1 to 20");
    	 	 		}
    	 	 		prev.setOnClickListener(new OnClickListener() {
    					
    					public void onClick(View v) {
    						// TODO Auto-generated method stub
    						int end =limit_start;
    						
    						limit_start-=20;
    						
    						if(limit_start>=0)
    						{
    						int start =limit_start;
    						showing_rec_cnt.setText("Records Displayed from "+(start+1) +" to "+end);
    						if((limit_start)<=0)
    						{
    							prev.setVisibility(View.GONE);
    						}
    						else
    						{
    							prev.setVisibility(View.VISIBLE);
    						}
    						if((limit_start+20)<=Currentfile_list.length)
    						{
    							next.setVisibility(View.VISIBLE);
    						}
    						else
    						{
    							next.setVisibility(View.GONE);
    						}	
    						if (Currentfile_list != null)
    						   {
    							  int j=0; 
    							   
    							
    							 
    							  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
    							  if(limit_start>=0)
    							  {
    								  pieces3=new String[20];
    							   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
    								   
    								 
    					    		
    					    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
    					            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
    					    	            	 
    					    			 		 pieces3[j]=Currentfile_list[i].getName();
    					    	            
    					            		 }
    					    		 
    						   }
    							  }  
    						 }
    						
    						dynamicimagesparent();					
    						 dynamicimageschild();
    					}
    						else
    						{
    							limit_start+=20;
    							end=limit_start+20;
    						}
    					}
    				});
    	 	 		next.setOnClickListener(new OnClickListener() {
    					
    					public void onClick(View v) {
    						// TODO Auto-generated method stub
    						
    						limit_start+=20;
    						
    						if(Currentfile_list.length>=limit_start)
    						{
    							
    						int start =limit_start;
    						int end=0;
    						
    						
    						
    						if((limit_start+20)<Currentfile_list.length)
    						{
    							next.setVisibility(View.VISIBLE);
    						
    						}
    						else
    						{
    							next.setVisibility(View.GONE);
    						}
    						if((limit_start)<=0)
    						{
    							prev.setVisibility(View.GONE);
    						}
    						else
    						{
    							prev.setVisibility(View.VISIBLE);
    						}
    							
    						if (Currentfile_list != null)
    						   {
    							  int j=0; 
    							   
    							
    							 if(Currentfile_list.length>(limit_start+20))
    							 {
    								 pieces3=new String[20];
    								 end =limit_start+20;
    							 }
    							 else
    							 {
    								 pieces3=new String[(Currentfile_list.length-limit_start)];
    								 end =limit_start+( Currentfile_list.length-limit_start);
    							 }
    							  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
    							  if(limit_start>=0)
    							  {
    								 
    							   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
    								   
    								 
    					    		
    					    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
    					            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
    					    	            	 
    					    			 		 pieces3[j]=Currentfile_list[i].getName();
    					    	            
    					            		 }
    					    		 
    						   }
    							  }  
    						 }
    						
    						showing_rec_cnt.setText("Records Displayed from "+(start+1) +" to "+end);
    						dynamicimagesparent();
    					
    						 dynamicimageschild();
    						}
    						else
    						{
    							limit_start=limit_start-20;
    						}
    					}
    				});
    		 		
    		 		
    		 		titleheader = (TextView) dialog.findViewById(R.id.parenttitleheader);
    		 		childtitleheader = (TextView) dialog.findViewById(R.id.child_titleheader);
    		 		
    		 		selectfolderwise = (Button) dialog.findViewById(R.id.selectfolderwise);
    	 	 		pd.dismiss();
    	 	 		
    	 	 		if(dialog.isShowing())
    	 	 		{
    	 	 			dialog.setCancelable(true);
    	 	 			dialog.cancel();
    	 	 		}
    	 	 			dialog.setCancelable(false);
    	 	 			dialog.show();
    	 		         dynamicimagesparent();
    	 		    	 dynamicimageschild();
    	 	 		
    	 		    	cancelfolderwise.setOnClickListener(new OnClickListener() {
    	 				public void onClick(View v) {
    	 					if(!"".equals(finaltext)){
    	 						finaltext="";
    	 					}
    	 					selectedcount=0;
    	 	 				selectedtestcaption=null;		
    	 	 				maximumindb=0;
    	 	 				pieces1=null;
    	 	 				limit_start=0;
    	 	 				//pieces2=null;
    	 	 				pieces3=null;
    	 	 				dialog.setCancelable(true);
    	 					dialog.cancel();
    	 				}
    	 	 		});
    	 		    	backfolderwise.setOnClickListener(new OnClickListener() {
    	 	 			public void onClick(View v) {
    	 	 				if(!titleheader.getText().toString().equals("/mnt/sdcard/"))
    	 	 				{
    	 	 					backclick=1;
    	 		 				pieces1=null;pieces2=null;pieces3=null;limit_start=0;
    	 		 				
    	 		 	 				String value = titleheader.getText().toString();
    	 		 					String[] bits = value.split("/");	 		 					
    	 		 					String picname = bits[bits.length - 1];
    	 		 					if(value.equals("mnt/sdcard/"))
    	 		 					{
    	 		 						finaltext="";
    	 		 						pathofimage  = value.replace(picname+"/", "");
    	 		 					}
    	 		 					else
    	 		 					{
    	 		 						String tempstr = value.substring(0,value.length()-1);
    	 			 					int bits1 = tempstr.lastIndexOf("/");
    	 			 					String pathofimage1=tempstr.substring(0,bits1+1);
    	 		 						pathofimage  = pathofimage1;
    	 		 					} 
    	 					
    	 					titleheader.setText(pathofimage);
    	 					finaltext=pathofimage;
    	 					File f = new File(pathofimage );
    	 					walkdir(f);
    	 					dynamicimagesparent();
    	 					dynamicimageschild();
    	 	 						}
    	 	 				else
    	 	 				{
    	 	 					cf.ShowToast("You can not go back.",1);
    	 	 				}
    	 				}
    	 			});

    	 		    	selectfolderwise.setOnClickListener(new OnClickListener() {
    	 	 			public void onClick(View v) {
    	 	 				
    	 	 				if(elev==1){name ="Exterior and Grounds";}
    	 	 				else if(elev==2){name ="Interior";}
    	 	 				else if(elev==3){name ="Additional";}

    	 	 				if(selectedcount!=0){
    	 	 				for(int i=0;i<selectedcount;i++)
    						{
    	 	 					String[] bits = selectedtestcaption[i].split("/");
    							picname = bits[bits.length - 1];
    							cf.db.execSQL("INSERT INTO "
    									+ cf.HazardImageTable
    									+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
    									+ " VALUES ('"+ cf.Homeid + "','"+ quesid+ "','"+ elev+ "','"+ cf.convertsinglequotes(selectedtestcaption[i])+ "','"
    									+ cf.convertsinglequotes(name + " Elevation "+(maximumindb+i+1))+ "','"+ cf.convertsinglequotes(picname)+ "','" + cd + "','" + md + "','"
    									+ (maximumindb+1+i) + "','" + 0 + "')");
    							
    									
    						}
    	 	 				
    	 	 				
    	 	 				if(!"".equals(finaltext)){
    	 						finaltext="";
    	 					}
    						selectedcount=0;
    	 	 				selectedtestcaption=null;		
    	 	 				maximumindb=0;
    	 	 				pieces1=null;
    	 	 				pieces2=null;
    	 	 				pieces3=null;
    	 	 				if(!"".equals(finaltext)){
    	 						finaltext="";
    	 					}
    	 	 				dialog.setCancelable(true);
    	 					dialog.cancel();
    	 					showimages();
    	 					cf.ShowToast("Images saved successfully.",1);
    	 					changeimage();
    	 	 				}
    	 	 				else
    	 	 				{
    	 	 					cf.ShowToast("Please select atleast one image to upload .", 1);
    	 	 				}
    	 	 			}
    	 	 		}); /**Select ends**/
           

        }
};
public void Search_by_folder()
{
	 final Dialog dialog1 = new Dialog(GeneralImages.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		((LinearLayout) dialog1.findViewById(R.id.maintable)).setVisibility(View.GONE);
		((LinearLayout) dialog1.findViewById(R.id.Search_folder)).setVisibility(View.VISIBLE);
		dialog1.setCancelable(true);
		final Button selectBtn = (Button) dialog1.findViewById(R.id.SF_Search);
		Button cancelBtn = (Button) dialog1.findViewById(R.id.SF_cancel);
		Button Close = (Button) dialog1.findViewById(R.id.SF_close);
		final AutoCompleteTextView name_txt =(AutoCompleteTextView) dialog1.findViewById(R.id.SF_Name);
		final TextView finale_txt = (TextView) dialog1.findViewById(R.id.finale_txt);
		name_txt.setFocusable(false);
		name_txt.setText((titleheader.getText().toString().substring(12,titleheader.getText().toString().length())));
		name_txt.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				cf.setFocus(name_txt);
				name_txt.setSelection(name_txt.getText().length());
				return false;
			}
		});
		
		cancelBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//pieces1=null;pieces2=null;pieces3=null;
				
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		Close.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//pieces1=null;pieces2=null;pieces3=null;
				
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		selectBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) { 
				selectBtn.setVisibility(View.GONE);
				String Src_Txt =name_txt.getText().toString();
				//System.out.println("The selected folder name was "+name_txt.getText().toString()+"Sub string "+Src_Txt.substring(0, 11));
				System.out.println("The txt "+(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""));
				if(Src_Txt.equals(""))
				{
					titleheader.setText("/mnt/sdcard/");
					finaltext="/mnt/sdcard/";
					 images = new File("/mnt/sdcard/");
					//Currentfile_list=f.listFiles();
						childtitleheader.setText("sdcard"); // set the chaild title
					walkdir(images);
					dynamicimagesparent();
					dynamicimageschild();
				}
				else if((Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""))
				{
					cf.ShowToast("Please enter the valid foldername ", 1);
				}
				else 
				{	Src_Txt="/mnt/sdcard/"+Src_Txt;
					File f = new File(Src_Txt);
					if(f.exists())
					{
						if((Src_Txt.substring(Src_Txt.lastIndexOf("/"), Src_Txt.length())).equals(""))
						{
							titleheader.setText(Src_Txt);
		 					finaltext=Src_Txt;
		 					 images = new File(Src_Txt);
		 					//Currentfile_list=f.listFiles();
		 					Src_Txt=Src_Txt.substring(0, Src_Txt.length()-1);
		 					childtitleheader.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
						}
						else
						{
							titleheader.setText(Src_Txt+"/");
		 					finaltext=Src_Txt+"/";
		 					 images = new File(Src_Txt+"/");
		 					//Currentfile_list=f.listFiles();
		 					
		 					childtitleheader.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
						}
						
	 					walkdir(images);
	 					dynamicimagesparent();
	 					dynamicimageschild();
						
					}
					else
					{
						cf.ShowToast("Sorry no files found", 1);
					}
					
				}
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		name_txt.addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				try
				{
					String s1=s.toString();
					System.out.println("The issues not in the set "+s1);
					String file_name,file_txt,Requested_name;
					 if((s1.substring(0,1)).equals("/") )
						{
							name_txt.setText("");
							cf.ShowToast("Please Enter valid text ", 1);
							System.out.println("The issues not in the set ");
						}
					 else if((s1.length()>1) && (s1.substring(s.length()-1,s.length())).equals("/") && (s1.substring(s.length()-2,s.length()-1)).equals("/"))
					{
						name_txt.setText(s1.substring(0,s1.length()-1));
						name_txt.setSelection(name_txt.getText().length());
						cf.ShowToast("Please Enter valid text ", 1);
					}
					
					else if(name_txt.getText().toString().equals(""))
					{
						file_name="/mnt/sdcard/";
						 file_txt= "/mnt/sdcard";
					}
					else
					{
						file_name="/mnt/sdcard/"+name_txt.getText().toString();
						
						 file_txt= file_name.substring(0,file_name.lastIndexOf("/"));

						 Requested_name=file_name.substring(file_name.lastIndexOf("/")+1,file_name.length());
						 System.out.println("The file name and the texdt was file_name "+file_name+" file_txt ="+file_txt+"Requested_name ="+Requested_name);
						File f = new File(file_txt);
						if(f.exists())
						{
							System.out.println("File xeists");
							File[] file_list= f.listFiles(directoryFilter);

							String[] autousername = new String[file_list.length];
							String[] filename=null;
							System.out.println("File xeists 1"+file_list.length);
								if(file_list.length>0)
								{
									System.out.println("comes in to if");
								int k=0;
										for(int i=0;i<file_list.length;i++)
										{
											String Tmp=file_list[i].getAbsolutePath();
											
											Tmp=Tmp.substring(Tmp.lastIndexOf("/")+1,Tmp.length());
											
											
											System.out.println(" Tmp = "+Tmp+" Requested_name = "+Requested_name+" (Tmp.substring(0,Requested_name.length())) = "+(Tmp.substring(0,Requested_name.length())));
											if((Tmp.substring(0,Requested_name.length())).toLowerCase().equals(Requested_name.toLowerCase()))
											{
												
												String temp= name_txt.getText().toString();
												if(temp.contains("/"))
												{
													temp= temp.substring(0,name_txt.getText().toString().lastIndexOf("/"));
													autousername[k]=temp+"/"+Tmp;
												}
												else
												{
													autousername[k] = Tmp;	
												}
												
												System.out.println("text for the file was autousername[k]= "+autousername[k]);
												k++;
												
											}
											
											
											
											
										}
										filename=new String[k];
										
										for(int m=0;m<k;m++)
										{
											
											filename[m]=autousername[m];
											System.out.println("text for the file was filename[i]= "+filename[m]);
											
										}
										
										
								}
								 ArrayAdapter<String> adapter = new ArrayAdapter<String>(GeneralImages.this,R.layout.loginnamelist,filename);
								 name_txt.setThreshold(1);
								 name_txt.setAdapter(adapter);
					}
				
				
				
						 
				}
				
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("issues happen in cathce "+e.getMessage());
				}
			}
		});
		dialog1.setCancelable(false);
		dialog1.show();
		

}
}