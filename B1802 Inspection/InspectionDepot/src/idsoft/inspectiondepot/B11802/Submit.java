package idsoft.inspectiondepot.B11802;


import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class Submit extends Activity {
	String homeid, flag,InspectionType, status,  word,overallcomm="",ipadd;
	TextView policyholderinfo;
	int value, Count, policyrws, quesrws, quescomtrws, imgrws, fbdocrws,
			fbimgrws, hzdatarws, hzimgrws, rfcvrrws,chk,policy, bcode, rfcover, 
			rfdeck, rfwall, rfgeo, swr, open, wall, sign,
			frnt, fdinfo, fddoc, over, hazdat;
	RadioButton accept, decline;
	EditText etsetword, etgetword;
	Resources res;
	private String[] myString;
	TextView tv1, tv2;
	CommonFunctions cf;
	boolean completed=false;
	String  showstr="",erromsg="",fe="",be="",le="",re="",ae="",roof="",off="",sup="",addiphot="",homesig="",papersign="",geexter="",ininter="",additional="";
	private static final Random rgenerator = new Random();

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(8);cf.Createtablefunction(9);cf.Createtablefunction(10);cf.Createtablefunction(11);
		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");
		}

		setContentView(R.layout.submit);
		cf.getInspectorId();
		cf.getinspectiondate();
		/** menu **/
		
		
		cf.getDeviceDimensions();
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 8, cf, 0));
		res = getResources();
		myString = res.getStringArray(R.array.myArray);
		accept = (RadioButton) this.findViewById(R.id.accept);
		decline = (RadioButton) this.findViewById(R.id.decline);
		tv1 = (TextView) findViewById(R.id.textView2);
		tv2 = (TextView) findViewById(R.id.textView1);

		etsetword = (EditText) findViewById(R.id.wrdedt);
		etgetword = (EditText) findViewById(R.id.wrdedt1);
		word = myString[rgenerator.nextInt(myString.length)];
		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		etsetword.setText(word);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		cf.setRcvalue(rccode);
		if (word.contains(" ")) {
			word = word.replace(" ", "");
		}
		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		try {
			Cursor cur = cf.db.rawQuery("select * from " + cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid + "'", null);
			int rws1 = cur.getCount();
			cur.moveToFirst();
			String ownerfirstname = cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnersNameFirst")));
			String ownerlastname = cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnersNameLast")));
			String policyno = cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnerPolicyNumber")));
			policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "+ ownerlastname + "<br/><font color=#bddb00>(Policy No : "+ policyno + ")</font>"));
			String s = welcomemessage();
			tv1.setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ s
							+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
							+ "</font>"));
			tv2.setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ s
							+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
							+ "</font>"));

		} catch (Exception e) {

		}
		accept.setOnClickListener(onClickAnswer1);
		decline.setOnClickListener(onClickAnswer2);
		
	}
	public OnClickListener onClickAnswer1 = new OnClickListener() {

		public void onClick(View arg0) {
			accept.setSelected(true);
			decline.setChecked(false);
		}

	};
	public OnClickListener onClickAnswer2 = new OnClickListener() {

		public void onClick(View arg0) {
			accept.setSelected(false);
			decline.setChecked(true);
		}

	};

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.submit:
			erromsg="";fe="";be="";le="";re="";ae="";roof="";off="";sup="";addiphot="";homesig="";papersign="";geexter="";ininter="";additional="";
			export();
			break;
		case R.id.home:
			Intent homepage = new Intent(Submit.this, HomeScreen.class);
			startActivity(homepage);
			break;
		case R.id.S_refresh:
			
			word = myString[rgenerator.nextInt(myString.length)];
			
			etsetword.setText(word);
			if (word.contains(" ")) {
				word = word.replace(" ", "");
			}
			
			break;
		
		}
	}

	private void export() {
		// TODO Auto-generated method stub
		if (accept.isChecked() == true) {
			
			if (etgetword.getText().toString().equals("")) {

				cf.ShowToast("Please enter Word Verification.",1);
				etgetword.requestFocus();
			} else {

				if (etgetword.getText().toString()
						.equals(word.trim().toString())) {
					if(checkforImageAvailability())
					{
						chkifauditprocess();
					}
					else
					{
						ErrorAlert();						
					}
					
					
				} else {
					cf.ShowToast("Please enter valid Word Verification(case sensitive).",1);
					etgetword.setText("");
					etgetword.requestFocus();
				}
			}

		} else {
			cf.ShowToast("Please select Accept Radio Button.",1);

		}
	}
	private void chkifauditprocess() {
		// TODO Auto-generated method stub
		cf.Createtablefunction(2);
		Cursor c1 = cf.db.rawQuery("select * from "+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid+ "'", null);
		System.out.println("CCC="+c1.getCount());
		if(c1.getCount()>0)
		{
			c1.moveToFirst();System.out.println("ins"+c1.getString(c1.getColumnIndex("InspectionTypeId")));
			if(c1.getString(c1.getColumnIndex("InspectionTypeId")).equals("28"))
			{
				System.out.println("TPQA="+c1.getString(c1.getColumnIndex("TPQA")));
				System.out.println("Additionalservice="+c1.getString(c1.getColumnIndex("Additionalservice")));
				System.out.println("QA="+c1.getString(c1.getColumnIndex("QAInspector")));
				if(c1.getString(c1.getColumnIndex("TPQA")).equals("False") && c1.getString(c1.getColumnIndex("Additionalservice")).equals("False") && c1.getString(c1.getColumnIndex("QAInspector")).equals("True"))
				{
					final Dialog dialog2 = new Dialog(Submit.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog2.getWindow().setContentView(R.layout.alert);
					TextView txttitle = (TextView) dialog2.findViewById(R.id.txthelp);
					txttitle.setText("Audit Process");
					TextView txt = (TextView) dialog2.findViewById(R.id.txtid);
					txt.setText(Html.fromHtml("Do you want to follow the audit process?"));
					Button btn_helpclose = (Button) dialog2.findViewById(R.id.helpclose);
					btn_helpclose.setOnClickListener(new OnClickListener()
					{
					public void onClick(View arg0) {
							// TODO Auto-generated method stub												
						dialog2.dismiss();
						}
					});
					Button btn_ok = (Button) dialog2.findViewById(R.id.ok);
					btn_ok.setVisibility(cf.v1.VISIBLE);btn_ok.setText("Yes");
					btn_ok.setOnClickListener(new OnClickListener()
					{
					public void onClick(View arg0) {
							// TODO Auto-generated method stub												
						dialog2.dismiss();
						flag="1";
						chkpartial();
						}
					});
					Button btn_dash = (Button) dialog2.findViewById(R.id.dash);
					btn_dash.setVisibility(cf.v1.VISIBLE);btn_dash.setText("No");
					btn_dash.setOnClickListener(new OnClickListener()
					{
					public void onClick(View arg0) {
							// TODO Auto-generated method stub
						dialog2.dismiss();
						flag="2";
						chkifallsubmit();
					
					 }												
					});
					dialog2.setCancelable(false);
					dialog2.show();
				}
				else
				{
					chkpartial();
				}
			}
			else
			{
				chkpartial();
			}
		}	
	}
	public void chkifallsubmit()
	{
		Cursor qc = cf.SelectTablefunction(cf.tbl_comments, " where SRID='" + cf.Homeid + "'");
		if(qc.getCount()>0)
		{
			qc.moveToFirst();
			if((!cf.getsinglequotes(qc.getString(qc.getColumnIndex("BuildingCodeComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofCoverComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofDeckComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("SecondaryWaterComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofWallComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofGeometryComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("OpeningProtectionComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("WallConstructionComment"))).equals("")))
			{				
				completed=true;
			}
			else
			{
				completed=false;
			}
		}
		
		 Cursor imgcur = cf.SelectTablefunction(cf.ImageTable, " where SRID='" + cf.Homeid + "'");
		 Cursor	fdinfocur = cf.SelectTablefunction(cf.FeedBackInfoTable, " where SRID='" + cf.Homeid + "'");
		 Cursor	fddocucur = cf.SelectTablefunction(cf.FeedBackDocumentTable, " where SRID='" + cf.Homeid + "'");
		 Cursor overcur =  cf.SelectTablefunction(cf.tbl_comments, " WHERE SRID='" + cf.Homeid+ "'");
		 if(overcur.getCount()>0)
		 {
			 overcur.moveToFirst();
			 overallcomm = cf.getsinglequotes(overcur.getString(overcur.getColumnIndex("InsOverAllComments")));
		 }
		 
			
		 if(qc.getCount()==0 || completed==false)
		 {
			 cf.ShowToast("Please Fill B1-1802 Question Section.",1);
		 }
		 else if(imgcur.getCount()==0)
		 { 
			 cf.ShowToast("Please Fill Images Section.",1);
		 }
		 else if(fdinfocur.getCount()==0 || fddocucur.getCount()==0)
		 {
			 cf.ShowToast("Please Fill Feedback Section.",1);
		 }
		 else if(overallcomm.equals(""))
		 {
			 cf.ShowToast("Please enter the Overall Comments.",1);
		 }
		 else
		 {
			System.out.println("UPDATE " + cf.mbtblInspectionList
						+ " SET IsInspected=1,Status=1,AuditFlag='"+flag+"' WHERE s_SRID ='"
						+ cf.Homeid + "' and InspectorId = '"
						+ cf.InspectorId + "'");
			 cf.db.execSQL("UPDATE " + cf.mbtblInspectionList
						+ " SET IsInspected=1,Status=1,AuditFlag='"+flag+"' WHERE s_SRID ='"
						+ cf.Homeid + "' and InspectorId = '"
						+ cf.InspectorId + "'");
				cf.ShowToast("Saved sucessfully.",1);		
				Intent ielevationback = new Intent(Submit.this,HomeScreen.class);		
				startActivity(ielevationback);
		 }	
	}

	
	private void ErrorAlert()
	{
		final Dialog dialog1 = new Dialog(Submit.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1.findViewById(R.id.delete_img);
		Reup.setVisibility(View.VISIBLE);
		TextView tv =(TextView) dialog1.findViewById(R.id.DI_report);
		String tst="The images/PDFs have been corrupted while saving. Please re upload the image/PDF in the following section.<br><br>";
	
		if(!fe.trim().equals(""))
		{
			tst +="   Front Elevation images : "+fe.substring(0,fe.length()-1)+"<br><br>";
		}
		if(!re.trim().equals(""))
		{
			tst +="   Right Elevation images : "+re.substring(0,re.length()-1)+"<br><br>";
			
		}
		if(!be.trim().equals(""))
		{
			tst +="   Back Elevation images : "+be.substring(0,be.length()-1)+"<br><br>";
			
		}
		if(!le.trim().equals(""))
		{
			tst +="   Left Elevation images : "+le.substring(0,le.length()-1)+"<br><br>";
		}
		if(!ae.trim().equals(""))
		{
			tst +="   Attic Elevation images : "+ae.substring(0,ae.length()-1)+"<br><br>";
		}
		if(!addiphot.trim().equals(""))
		{
			tst +="   Additional Photographs : "+addiphot.substring(0,addiphot.length()-1)+"<br><br>";
		}
		if(!homesig.trim().equals(""))
		{
			tst +="  HomeOwner Signature image : "+homesig.substring(0,homesig.length()-1)+"<br><br>";
		}
		if(!papersign.trim().equals(""))
		{
			tst +="  Paperwork Signature image : "+papersign.substring(0,papersign.length()-1)+"<br><br>";
		}
		if(off.trim().equals("true"))
		{
			tst +="   Feedback Document Office Use <br><br>";
		}if(sup.trim().equals("true"))
		{
			tst +="   Feedback Document Supplemental <br><br>";
		}
		if(!geexter.trim().equals(""))
		{
			tst +="  Exterior and Grounds images : "+geexter.substring(0,geexter.length()-1)+"<br><br>";
		}
		if(!ininter.trim().equals(""))
		{
			tst +="  Interior images : "+ininter.substring(0,ininter.length()-1)+"<br><br>";
		}
		if(!additional.trim().equals(""))
		{
			tst +="  Additional images : "+additional.substring(0,additional.length()-1)+"<br><br>";
		}
		tv.setText(Html.fromHtml(tst));
		Button 	b1 =(Button) dialog1.findViewById(R.id.DI_yes);
		b1.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chkifauditprocess();
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		Button 	b2 =(Button) dialog1.findViewById(R.id.DI_close);
		b2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//update();
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		Button 	b3 =(Button) dialog1.findViewById(R.id.DI_no);
		b3.setOnClickListener(new OnClickListener() {			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//update();
				dialog1.setCancelable(true);
				dialog1.cancel();
			}
		});
		dialog1.setCancelable(false);
		dialog1.show();
	}
	public void chkpartial()
	{
		Cursor qc = cf.SelectTablefunction(cf.tbl_comments, " where SRID='" + cf.Homeid + "'");System.out.println("chkpartila");
		if(qc.getCount()>0)
		{
			qc.moveToFirst();
			if((!cf.getsinglequotes(qc.getString(qc.getColumnIndex("BuildingCodeComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofCoverComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofDeckComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("SecondaryWaterComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofWallComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("RoofGeometryComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("OpeningProtectionComment"))).equals(""))
					&& (!cf.getsinglequotes(qc.getString(qc.getColumnIndex("WallConstructionComment"))).equals("")))
			{
				completed=true;			
			}
			else
			{
				completed=false;
			}
		}
		
		 Cursor imgcur = cf.SelectTablefunction(cf.ImageTable, " where SRID='" + cf.Homeid + "'");
		 Cursor	fdinfocur = cf.SelectTablefunction(cf.FeedBackInfoTable, " where SRID='" + cf.Homeid + "'");
		 Cursor	fddocucur = cf.SelectTablefunction(cf.FeedBackDocumentTable, " where SRID='" + cf.Homeid + "'");
		 
		 if(qc.getCount()==0 && imgcur.getCount()==0  && (fdinfocur.getCount()==0 || fddocucur.getCount()==0))
		 {
			 cf.ShowToast("Please Fill B1-1802 Question or Images or Feedback Section.",1);
		 }
		 else if((completed==false) && (imgcur.getCount()==0 && (fdinfocur.getCount()==0 && fddocucur.getCount()==0)))
		 { 
			 cf.ShowToast("Please Fill B1-1802 Question or Images or Feedback Section.",1);
		 }
		 else
		 {
			if(completed==true || imgcur.getCount()!=0 || (fdinfocur.getCount()!=0 && fddocucur.getCount()!=0))
			 {
				System.out.println("UPDATE " + cf.mbtblInspectionList
							+ " SET IsInspected=1,Status=1,AuditFlag='"+flag+"' WHERE s_SRID ='"
							+ cf.Homeid + "' and InspectorId = '"
							+ cf.InspectorId + "'");
				
				 cf.db.execSQL("UPDATE " + cf.mbtblInspectionList
							+ " SET IsInspected=1,Status=1,AuditFlag='"+flag+"' WHERE s_SRID ='"
							+ cf.Homeid + "' and InspectorId = '"
							+ cf.InspectorId + "'");
					cf.ShowToast("Saved sucessfully.",1);
			
					Intent ielevationback = new Intent(Submit.this,
							HomeScreen.class);
			
					startActivity(ielevationback);
			 }
			 else
			 {
				
			 }
		 }
	}
	
	private boolean checkforImageAvailability() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		cf.Createtablefunction(11);
		cf.Createtablefunction(13);
		cf.Createtablefunction(14);
		erromsg="";
		try
		{
		Cursor c=cf.SelectTablefunction(cf.ImageTable, " Where SrID='"+cf.Homeid+"'");
		
		if(c.getCount()>0)
		{
			c.moveToFirst();
		for (int i=0;i<c.getCount();i++)
		{
			String mypath = cf.getsinglequotes(c.getString(c.getColumnIndex("ImageName"))); // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
		System.out.println("filename "+f);
			/** check the file is exist in the device **/
			if(f!=null)
			{
			if (!f.exists()) {
				if (c.getString(3).toString().equals("1")) {
					fe += c.getString(9).toString()+",";
					erromsg += "Front Elevation Image Orders : "
							+ c.getString(9).toString() +"<br>";
				} else if (c.getString(3).toString().equals("2")) {
					re += c.getString(9).toString()+",";
					erromsg += "Right Elevation Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("3")) {
					be += c.getString(9).toString()+",";
					erromsg += "Back Elevation Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("4")) {
					le += c.getString(9).toString()+",";
					erromsg += "Left Elevation Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("5")) {
					ae += c.getString(9).toString()+",";
					erromsg += "Attic Elevation Image Orders : "
							+ c.getString(9).toString()+"<br>";
				}else if (c.getString(3).toString().equals("8")) {
					addiphot += c.getString(9).toString()+",";
					erromsg += "Additional Photographs Image Orders : "
						+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("6")) {
					homesig += c.getString(9).toString()+",";
					erromsg += "Home Owner Signature : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("7")) {
					papersign += c.getString(9).toString()+",";
					erromsg += "Paperwork Signature : "
							+ c.getString(9).toString()+"<br>";
				}else  {
				erromsg += "Image not available in the Orders : "
						+ c.getString(9).toString()+"<br>";
				}
			}
			
			
				
		}
			else
			{
				if (c.getString(3).toString().equals("1")) {
					fe += c.getString(9).toString()+",";
					erromsg += "Front Elevation Image Order : "
							+ c.getString(9).toString() +"<br>";
				} else if (c.getString(3).toString().equals("2")) {
					re += c.getString(9).toString()+",";
					erromsg += "Right Elevation Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("3")) {
					be += c.getString(9).toString()+",";
					erromsg += "Back Elevation Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("4")) {
					le += c.getString(9).toString()+",";
					erromsg += "Left Elevation Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("5")) {
					ae += c.getString(9).toString()+",";
					erromsg += "Attic Photograps Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("5")) {
					addiphot += c.getString(9).toString()+",";
					erromsg += "Additional Photograps Image Order : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("6")) {
					homesig += c.getString(9).toString()+",";
					erromsg += "Home Owner Signature : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("7")) {
					papersign += c.getString(9).toString()+",";
					erromsg += "Paperwork Signature : "
						+ c.getString(9).toString()+"<br>";
				}else  {
						erromsg += "Image not available in the Order : "
								+ c.getString(9).toString()+"<br>";
					}
			}
		
			c.moveToNext();
		}
		
		}
		else
		{
			
		}
		}
		catch(Exception e)
		{
			System.out.println("image "+e.getMessage());
		}
		
		try
		{
		Cursor c1=cf.SelectTablefunction(cf.FeedBackDocumentTable, " Where SRID='"+cf.Homeid+"'");
		
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
		for (int i=0;i<c1.getCount();i++)
		{
			String mypath = cf.getsinglequotes(c1.getString(c1.getColumnIndex("FileName"))); // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
			
			/** check the file is exist in the device **/
			if(f!=null)
			{
			if (!f.exists()) {
				if (c1.getString(c1.getColumnIndex("IsOfficeUse")).toString().equals("1")) {
					off="true";
					erromsg += "Feed back Document Office Use Title : "
							+ cf.getsinglequotes(c1.getString(c1.getColumnIndex("DocumentTitle")).toString()) +"<br>";
				} else {
					sup="true";
					erromsg += "Feed back Document Supplemental  Title : "
							+ cf.getsinglequotes(c1.getString(c1.getColumnIndex("DocumentTitle")).toString()) +"<br>";
				} 
				
			}
		}
		
			c1.moveToNext();
		}
		}
		else
		{}
		}
		catch(Exception e)
		{
			System.out.println("feedback "+e.getMessage());
		}
		
		try{
		Cursor c=cf.SelectTablefunction(cf.HazardImageTable, " Where SrID='"+cf.Homeid+"' and Delflag=0");
		System.out.println("cf.get"+c.getCount());
		if(c.getCount()>0)
		{
			c.moveToFirst();
		for (int i=0;i<c.getCount();i++)
		{
			String mypath = cf.getsinglequotes(c.getString(c.getColumnIndex("ImageName"))); // get the full path of the image 
			String temppath[] = mypath.split("/");
			int ss = temppath[temppath.length - 1].lastIndexOf(".");
			String tests = temppath[temppath.length - 1].substring(ss);
			String elevationttype;
			File f = new File(mypath);
		
			/** check the file is exist in the device **/
			if(f!=null)
			{
			if (!f.exists()) {
				
				if (c.getString(3).toString().equals("1")) {System.out.println("exter");
					geexter += c.getString(9).toString()+",";
					erromsg += "Exterior and Grounds Image Orders : "
							+ c.getString(9).toString() +"<br>";
				} else if (c.getString(3).toString().equals("2")) {System.out.println("inter");
					ininter += c.getString(9).toString()+",";
					erromsg += "Interior Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("3")) {System.out.println("addition");
					additional += c.getString(9).toString()+",";
					erromsg += "Additional Image Orders : "
							+ c.getString(9).toString()+"<br>";
				}else  {System.out.println("imgnot");
				erromsg += "Image not available in the Orders : "
						+ c.getString(9).toString()+"<br>";
				}
			}
			
			System.out.println("erromsg not exists"+erromsg);
				
		}
			else
			{
				if (c.getString(3).toString().equals("1")) {System.out.println("exter");
					geexter += c.getString(9).toString()+",";
					erromsg += "Exterior and Grounds Image Orders : "
							+ c.getString(9).toString() +"<br>";
				} else if (c.getString(3).toString().equals("2")) {System.out.println("exter");
					ininter += c.getString(9).toString()+",";
					erromsg += "Interior Image Orders : "
							+ c.getString(9).toString()+"<br>";
				} else if (c.getString(3).toString().equals("3")) {System.out.println("exter");
					additional += c.getString(9).toString()+",";
					erromsg += "Additional Image Orders : "
							+ c.getString(9).toString()+"<br>";
				}else  {					System.out.println("exter");
				erromsg += "Image not available in the Orders : "
						+ c.getString(9).toString()+"<br>";
				}
			}
		
			c.moveToNext();
		}
		
		}
		else{}
		}
		catch(Exception e)
		{
			System.out.println("general image "+e.getMessage());
		}
		System.out.println("erro mo"+erromsg);
			if(erromsg.trim().equals(""))
			{
				return true;
			}
			else
			{
				return false;
			}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(Submit.this, InspectionList.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public String welcomemessage() {
		try {

			cf.getInspectorId();
			Cursor cm = cf.SelectTablefunction(cf.inspectorlogin,
					" where Ins_Id='" + cf.InspectorId.toString() + "'");
			int rws1 = cm.getCount();
			cm.moveToFirst();
			String data = cf.getsinglequotes(
					cm.getString(cm.getColumnIndex("Ins_FirstName")))
					.toLowerCase();
			data += " ";
			return data += cf.getsinglequotes(
					cm.getString(cm.getColumnIndex("Ins_LastName")))
					.toLowerCase();

		} catch (Exception e) {
			return "";
		}

	}
}