package idsoft.inspectiondepot.B11802;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ShowingDeleteAlert extends Activity{
int requestcode=54321;
String val="idma";
ProgressDialog pd;
CommonFunctions cf;
private int k;
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showingdeletealrt);
		TextView tv=(TextView) findViewById(R.id.notify_txt);
		Bundle b=getIntent().getExtras();
		cf=new  CommonFunctions(this);
		//cf.getInspectorId();
		/*if(b!=null)
		{
			val=b.getString("application");
		}
		if(val.equals("idma"))
		{
			tv.setText("")
		}*/
		if(cf.application_sta){
			Intent loginpage = new Intent(Intent.ACTION_MAIN);
			loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
			startActivity(loginpage);
		}
		
	}
	public void clicker(View v)
	{
		/*Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:com.idinspection"));
        startActivityForResult(intent, requestcode);*/
		if(cf.isInternetOn())
		{
		String source1 = "<font color=#FFFFFF>Downloading IDMA application ... Please wait...</font>";
		 pd = ProgressDialog.show(ShowingDeleteAlert.this, "", Html.fromHtml(source1),true);
	        new Thread(new Runnable() {
	                public void run() {
	                	Looper.prepare();
	                	downloadingapp();
	                	
	                	handler2.sendEmptyMessage(0);
	                }
	            }).start();
		}
		else
		{
			cf.ShowToast("Please enable internet connection and try again", 1);
		}
	}
	private void downloadingapp()
	{
		
		SoapObject webresult;			
		SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;	
		//request.addProperty("InspectorID",cf.InspectorId);
		envelope.setOutputSoapObject(request);	
		System.out.println("request downlod "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

		try {
			androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);
			System.out.println("RESPONSE "+envelope.getResponse());
			Object response = envelope.getResponse();
			
			byte[] b;
			if (cf.checkresponce(response)) // check the response is valid or
											// not
			{
				b = Base64.decode(response.toString());

				try {
					String PATH = Environment.getExternalStorageDirectory()
							+ "/Download/IDMA";
					File file = new File(PATH);
					file.mkdirs();

					File outputFile = new File(PATH + "/IDMA.apk");
					FileOutputStream fileOuputStream = new FileOutputStream(outputFile);
					fileOuputStream.write(b);
					fileOuputStream.close();
					//cf.fn_logout(cf.InspectorId);  //FOR LOGOUT
					k=11;
				} catch (IOException e) {
					cf.ShowToast("Update error!",1);
				}
			}
			else {
				k=22;
				
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (XmlPullParserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	private Handler handler2 = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			if(k==11)
			{
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(new File(Environment
						.getExternalStorageDirectory()
						+ "/Download/IDMA/"
						+ "IDMA.apk")),
						"application/vnd.android.package-archive");
				startActivityForResult(intent,12);
			}
			else if(k==22)
			{
				cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
			}
			
		}
	};

	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				//cf.goback(0);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			
		 if(requestCode==12 && resultCode==RESULT_CANCELED)
			{
				startActivity(new Intent(this,ShowingDeleteAlert.class));
				
			}
		 super.onActivityResult(requestCode, resultCode, data);
			
	 }

}
