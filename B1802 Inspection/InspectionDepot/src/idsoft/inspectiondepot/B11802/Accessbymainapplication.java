package idsoft.inspectiondepot.B11802;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
 
public class Accessbymainapplication extends Activity{
	
	private static final String TAG = null;
	protected static final int request = 8196;
	CommonFunctions cf;  String V_name="0";
	int V_code=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		System.out.println("onCreate");
		Bundle extras = getIntent().getExtras();
		System.out.println("extras "+extras);
		
		cf = new CommonFunctions(this);
		System.out.println("CommonFunctions");
		cf.Createtablefunction(1);
		cf.Createtablefunction(2);
		cf.Createtablefunction(15);
		try {
			Cursor cur = cf.db.rawQuery("select * from " + cf.inspectorlogin, null);
			cur.moveToFirst();
			int result1 = cur.getColumnIndex("Ins_ImageName");
			int result2 = cur.getColumnIndex("Ins_rememberpwd");
			int result3 = cur.getColumnIndex("Ins_Email");
			int result4 = cur.getColumnIndex("Ins_PhotoExtn");
			if (result1 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_ImageName VARCHAR");				
			}
			if (result2 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_rememberpwd VARCHAR");
			}
			if (result3 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_Email VARCHAR DEFAULT ''");
			}
			if (result4 == -1) 
			{
			
				cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_PhotoExtn VARCHAR DEFAULT ''");
			}
		} catch (Exception e) {
			System.out.println("altering table" + e.getMessage());
		}
		try {
			System.out.println("came her");
			Cursor cur = cf.db.rawQuery("select * from " + cf.mbtblInspectionList, null);
			cur.moveToFirst();
			int result1 = cur.getColumnIndex("BuildingSize");
			int result2 = cur.getColumnIndex("TPQA");
			int result3 = cur.getColumnIndex("Additionalservice");
			int result4 = cur.getColumnIndex("QAInspector");
			int result5 = cur.getColumnIndex("Licenceexpirydate");
			int result6 = cur.getColumnIndex("AuditFlag");
			
			System.out.println("camres "+result1+"result2="+result2+"result3="+result3);
			if (result1 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.mbtblInspectionList +" ADD COLUMN BuildingSize VARCHAR(50)");				
			}
			if (result2 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.mbtblInspectionList +" ADD COLUMN TPQA VARCHAR(50) DEFAULT ''");				
			}
			if (result3 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.mbtblInspectionList +" ADD COLUMN Additionalservice VARCHAR(50) DEFAULT ''");				
			}
			if (result4 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.mbtblInspectionList +" ADD COLUMN QAInspector VARCHAR(50) DEFAULT ''");				
			}
			if (result5 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.mbtblInspectionList +" ADD COLUMN Licenceexpirydate VARCHAR(50) DEFAULT ''");				
			}
			if (result6 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.mbtblInspectionList +" ADD COLUMN AuditFlag VARCHAR(25) DEFAULT '0'");				
			}
			
		} catch (Exception e) {
			System.out.println("altering table policyholder" + e.getMessage());
		}
		
		try {
			Cursor cur = cf.db.rawQuery("select * from " + cf.HazardDataTable, null);
			cur.moveToFirst();
			int result1 = cur.getColumnIndex("InsuredOther");
			int result2 = cur.getColumnIndex("ObservOther");
			int result3 = cur.getColumnIndex("OcccupOther");
			if (result1 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.HazardDataTable +" ADD COLUMN InsuredOther VARCHAR(50)");				
			}
			if (result2 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.HazardDataTable +" ADD COLUMN ObservOther VARCHAR(50)");
			}
			if (result3 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.HazardDataTable +" ADD COLUMN OcccupOther VARCHAR(50) DEFAULT ''");
			}
			
		} catch (Exception e) {
			System.out.println("altering table" + e.getMessage());
		}
		
		if (extras != null) {
			System.out.println("comes in the bunndel");
			 // we are used to save the data in tthe data base only so we dont want to put the get single codes 
			    String Ins_Id=extras.getString("Ins_Id");
			    String Ins_FirstName=extras.getString("Ins_FirstName");
			    String Ins_LastName=extras.getString("Ins_LastName");
			    String Ins_Address=extras.getString("Ins_Address");
			    String Ins_CompanyName=extras.getString("Ins_CompanyName");
			    String Ins_CompanyId= extras.getString("Ins_CompanyId");
			    String Ins_UserName=extras.getString("Ins_UserName");
			    String Ins_Password= extras.getString("Ins_Password");
			    String Ins_Email= extras.getString("Ins_Email");
			    String Ins_PhotoExtn= extras.getString("Ins_PhotoExtn");
			    String inspext =  Ins_PhotoExtn;
			    String  FILENAME=Ins_Id+inspext;

			    try{
					   
				    
					File outputFile = new File("/data/data/idsoft.inspectiondepot.IDMA/files"+"/"+Ins_Id+inspext);
					File f =new File(getFilesDir(),FILENAME);
					System.out.println("its not exist"+f.exists());
					//byte b[]=outputFile.to
							if (outputFile.exists() && !f.exists()) 
							{
								FileOutputStream fos = openFileOutput(FILENAME, this.MODE_WORLD_READABLE);
										
										InputStream fis = new FileInputStream(outputFile);
										long length = outputFile.length();
										byte[] bytes = new byte[(int) length];
										int offset = 0;
										int numRead = 0;
										while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
										{
											offset += numRead;
										}
										
									fos.write(bytes);	
									
										
							
						}
							
							
				   }catch (Exception e) {
					// TODO: handle exception
					   System.out.println("no more issues"+e.getMessage());
				}
			  
			    try
			    {
			     V_code= extras.getInt("IDMA_Version_code");
			    
			     V_name= extras.getString("IDMA_Version_name");
			     
			    }
			    catch(Exception e)
			    {
			    	V_code=0;
			    	V_name="0";
			    	
			    }
			     if(V_name==null)
			     {
			    	 V_name="";
			     }
			   
			   
				System.out.println("vcode");
				
			     try
				    {
			    	 System.out.println("vcode try");
			    	 
			    	/**update the version of the IDMA application for getting updated version **/
			    	cf.Createtablefunction(20);
			    	System.out.println("vcode create");
			    	Cursor cs=cf.SelectTablefunction(cf.IDMAVersion," Where  ID_VersionType='IDMA_Old' ");
			    	System.out.println("IDMAVersion create"+cs.getCount());
			    	
			    	if(cs.getCount()>=1)
			    	{
			    		System.out.println("IDMAVe");
			    		
			    		cf.db.execSQL("UPDATE "+cf.IDMAVersion+" set ID_VersionCode='"+V_code+"' ,ID_VersionName='"+V_name+"' Where  ID_VersionType='IDMA_Old' "); 
			    	}
			    	else
			    	{
			    		System.out.println("IDMAVe update");
			    		cf.db.execSQL("INSERT INTO "+ cf.IDMAVersion+" (ID_VersionCode,ID_VersionName,ID_VersionType) VALUES ('" + V_code + "','"+V_name + "','IDMA_Old')");
			    		
			    	}
				    }
				    catch(Exception e)
				    {
				    	System.out.println("e update"+e.getMessage());
				    	
				    }
			    	/**update the version of the IDMA application for getting updated version  Ends**/
 try
			    {
			    FileOutputStream fos = openFileOutput(FILENAME, this.MODE_WORLD_READABLE);
				File outputFile = new File("/data/data/idsoft.inspectiondepot.IDMA/files"+"/"+Ins_Id+inspext);
				//byte b[]=outputFile.to
						if (outputFile.exists()) 
						{
				    		
									
									InputStream fis = new FileInputStream(outputFile);
									long length = outputFile.length();
									byte[] bytes = new byte[(int) length];
									int offset = 0;
									int numRead = 0;
									while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
									{
										offset += numRead;
									}
									
								fos.write(bytes);	
								
									
						
					}
			    }
			    catch(Exception e)
			    {
			    	
			    }
 			try
 			{
 				cf.db.execSQL("UPDATE "+cf.inspectorlogin+" set Ins_Flag1=0 where Ins_Id!='"+Ins_Id+"'"); 
 			}
 			catch(Exception e)
 			{
	 			System.out.println("Exception in "+e.getMessage());
 			}


			    Cursor log= cf.SelectTablefunction(cf.inspectorlogin, " where Ins_Id='"+Ins_Id+"'");
			    
				log.moveToFirst();
				System.out.println("upda "+log.getCount());
				if(log.getCount()>=1)
				{
					System.out.println("update flag "+log.getCount());
					if(log.getString(log.getColumnIndex("Ins_Flag1")).equals("1"))
					{
						System.out.println("update inside");
						
						Intent Star = new Intent(Accessbymainapplication.this,HomeScreen.class);
						//Star.putExtra("keyName", 1);
						if(cf.db_avb)
						{
							final AlertDialog.Builder b =new Builder(this);
					     	b.setTitle("Update Notification");
					     	b.setMessage("You will be asked to uninstall an older version(1.0.19) of the B1 - 1802 (Rev. 01/12) Inspection  application. During this process no data will be lost..");
					     	b.setPositiveButton("OK", new  DialogInterface.OnClickListener() {
									
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										b.setCancelable(true);
										 Intent intent = new Intent(Intent.ACTION_DELETE);
							             intent.setData(Uri.parse("package:com.inspectiondepot"));
							             startActivityForResult(intent,request);
									}
									}); 
					     	b.setNegativeButton("Remind me later",  new  DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									b.setCancelable(true);
									 Intent Star = new Intent(Accessbymainapplication.this,HomeScreen.class);
							    	  startActivity(Star);
								}
								}); 
					     	AlertDialog a= b.create();
					     	b.setCancelable(false);
					     	b.show();
					     	
						}
						else
						{
							startActivity(Star);
						}
						
						
					}
					else 
					{
						System.out.println("UPDATE "+"UPDATE "+cf.inspectorlogin+" set Ins_Flag1=1,Ins_Email='"+Ins_Email+"',Ins_PhotoExtn='"+Ins_PhotoExtn+"' where Ins_Id='"+Ins_Id+"'");
						cf.db.execSQL("UPDATE "+cf.inspectorlogin+" set Ins_Flag1=1,Ins_Email='"+Ins_Email+"',Ins_PhotoExtn='"+Ins_PhotoExtn+"' where Ins_Id='"+Ins_Id+"'"); // set the flag one for the inspector
						System.out.println("UPDATE success");
						Intent Star = new Intent(Accessbymainapplication.this,
								HomeScreen.class);
						//Star.putExtra("keyName", 1);
						if(cf.db_avb)
						{
							final AlertDialog.Builder b =new Builder(this);
					     	b.setTitle("Update Notification");
					     	b.setMessage("You will be asked to uninstall an older version(1.0.19) of the B1 - 1802 (Rev. 01/12) Inspection  application. During this process no data will be lost..");
					     	b.setPositiveButton("OK", new  DialogInterface.OnClickListener() {
									
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										b.setCancelable(true);
										 Intent intent = new Intent(Intent.ACTION_DELETE);
							             intent.setData(Uri.parse("package:com.inspectiondepot"));
							             startActivityForResult(intent,request);
									}
									});
					     	b.setNegativeButton("Remind me later",  new  DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									b.setCancelable(true);
									 Intent Star = new Intent(Accessbymainapplication.this,HomeScreen.class);
							    	  startActivity(Star);
								}
								}); 
					     	AlertDialog a= b.create();
					     	b.setCancelable(false);
					     	b.show();
						}
						else
						{
							startActivity(Star);
						}
						
						
					}
					
					
				} 
				else
				{
				    try {
	
						cf.db.execSQL("INSERT INTO "
								+ cf.inspectorlogin
								+ " (Ins_Id,Ins_FirstName,Ins_MiddleName,Ins_LastName,Ins_Address,Ins_CompanyName,Ins_CompanyId,Ins_UserName,Ins_Password,Ins_Flag1,Ins_Flag2,Android_status,Ins_ImageName,Ins_rememberpwd,Ins_Email,Ins_PhotoExtn)"
								+ " VALUES ('" + Ins_Id + "','"
								+Ins_FirstName + "','"
								+ "''" + "','"
								+ Ins_LastName + "','"
								+ Ins_Address + "','"
								+ Ins_CompanyName + "','"
								+Ins_CompanyId + "','"
								+ Ins_UserName.toLowerCase()
								+ "','" + Ins_Password + "','"
								+ "1" + "','" +"0" + "','" +"true"+ "','','','"+Ins_Email+"','"+Ins_PhotoExtn+"')"); // insert the inspector and set the flag as login 
						
						Intent Star = new Intent(Accessbymainapplication.this,
								HomeScreen.class);
						//Star.putExtra("keyName", 1);
						if(cf.db_avb)
						{
							final AlertDialog.Builder b =new Builder(this);
					     	b.setTitle("Update Notification");
					     	b.setMessage("You will be asked to uninstall an older version(1.0.19) of the B1 - 1802 (Rev. 01/12) Inspection  application. During this process no data will be lost..");
					     	b.setPositiveButton("OK", new  DialogInterface.OnClickListener() {
									
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										b.setCancelable(true);
										 Intent intent = new Intent(Intent.ACTION_DELETE);
							             intent.setData(Uri.parse("package:com.inspectiondepot"));
							             startActivityForResult(intent,request);
									}
									}); 
					     	b.setNegativeButton("Remind me later",  new  DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									b.setCancelable(true);
									 Intent Star = new Intent(Accessbymainapplication.this,HomeScreen.class);
							    	  startActivity(Star);
								}
								}); 
					     	
					     	AlertDialog a= b.create();
					     	b.setCancelable(false);
					     	b.show();
					     	
						}
						else
						{
							startActivity(Star);
						}
						
						
					} catch (Exception e) {
						Log.i(TAG, "categorytableerror=" + e.getMessage());
						if(cf.application_sta)
						{
							Intent intent = new Intent(Intent.ACTION_MAIN);
							intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
							startActivity(intent);
						}else
						{
							Intent Star = new Intent(Accessbymainapplication.this,
									InspectionDepot.class);
							//Star.putExtra("keyName", 1);
							startActivity(Star);
						}
					}
		   
				}
		    
		   
		    //extras.getString("keyName", 1);
		    // we are used to save the data in tthe data base only so we dont want to put the get single codes ends 
		}
		else
		{ // call the id inspection main for the application menu 
	
			
			   Cursor log= cf.SelectTablefunction(cf.inspectorlogin, " where Ins_Flag1='1'");
			   System.out.println("comes in the if "+log.getCount());
			   if(cf.application_sta)
				{
					Intent intent = new Intent(Intent.ACTION_MAIN);
					intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
					startActivity(intent);
				}  
				else if(log.getCount()==1)
			   {
				   Intent Star = new Intent(Accessbymainapplication.this,
							HomeScreen.class);
					//Star.putExtra("keyName", 1);
					startActivity(Star); 
			   }
			   else
			   {
				   System.out.println("comes in the else");
				   try
					{
							// for read the strored file 
						/*File f = new File(Environment.getExternalStorageDirectory()+"/Inspectiondepot_WSI_logininfo.txt"); // storing login information in to the txt fil for access the login information
						 System.out.println("f else"+f);
						if (f.exists()) {
							System.out.println("f exists");
						    FileInputStream fis = new FileInputStream(f);
						    DataInputStream in = new DataInputStream(fis);
						    String Ins_Id="",Ins_FirstName="",Ins_LastName="",Ins_Address="",Ins_CompanyName="",Ins_CompanyId="",Ins_UserName="",Ins_PhotoExtn="",Ins_Password="",Ins_Email="";
						    String s[]= new String[10];
						 
						    try {
						    	
						    	 
						    	int i=0;
						    	
						        for (;;) {
						        	
						        	s[i]=in.readUTF();
						        	i++;
						          //Log.i("Data Input Sample", in.readUTF());
						         }
						      } catch (EOFException e) {
						        Log.i("Data Input Sample", "End of file reached");
						    }
						    System.out.println("f Sample");
						    if(s[0].startsWith("Ins_Id="))
					        {
					        Ins_Id=s[0].substring(s[0].indexOf("="));
					        }
						    
					        if(s[1].startsWith("Ins_FirstName="))
						    Ins_FirstName=s[1].substring(s[1].indexOf("=")+1);
					        
					        if(s[2].startsWith("Ins_LastName="))
						    Ins_LastName=s[2].substring(s[2].indexOf("=")+1);
					        if(s[3].startsWith("Ins_Address="))
						    Ins_Address=s[3].substring(s[3].indexOf("=")+1);
					        System.out.println("sdfsf");
					       // System.out.println("last name = "+Ins_LastName+"sdfsf"+Ins_Address);
						    if(s[4].startsWith("Ins_CompanyName="))
						    Ins_CompanyName=s[4].substring(s[4].indexOf("=")+1);
						    System.out.println(" s 4"+s[4]);
						    if(s[5].startsWith("Ins_CompanyId="))
						    Ins_CompanyId= s[5].substring(s[5].indexOf("=")+1);
						    if(s[6].startsWith("Ins_UserName="))
						    Ins_UserName=s[6].substring(s[6].indexOf("=")+1);
						    if(s[7].startsWith("Ins_Password="))
						    	System.out.println(" s 7"+s[7]);
						    Ins_Password= s[7].substring(s[7].indexOf("=")+1);
						    
						    if(s[8].startsWith("Ins_PhotoExtn="))
						    	Ins_PhotoExtn= s[8].substring(s[8].indexOf("=")+1);
						    if(s[9].startsWith("Ins_Email="))
						    	Ins_Email= s[9].substring(s[9].indexOf("=")+1);

						    
						    System.out.println(" in close bf");
						    in.close();
						    f.delete(); // delete the file once we update in to the tabel
						
						    Intent Star = new Intent(Accessbymainapplication.this,Accessbymainapplication.class);
						    Star.putExtra("Ins_Id",Ins_Id);
						    Star.putExtra("Ins_FirstName",Ins_FirstName);
						    Star.putExtra("Ins_LastName",Ins_LastName);
						    Star.putExtra("Ins_Address",Ins_Address);
						    Star.putExtra("Ins_CompanyName",Ins_CompanyName);
						    Star.putExtra("Ins_CompanyId",Ins_CompanyId);
						    Star.putExtra("Ins_UserName",Ins_UserName);
						    Star.putExtra("Ins_Password",Ins_Password);
						    Star.putExtra("Ins_Email",Ins_Email);
						    Star.putExtra("Ins_PhotoExtn",Ins_PhotoExtn);
							startActivity(Star);
							
						    
						}
						else
						{*/
							if(cf.application_sta)
							{
								Intent intent = new Intent(Intent.ACTION_MAIN);
								intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
								startActivity(intent);
							}  
							else
							{
								cf.db.execSQL("UPDATE "+cf.inspectorlogin+" set Ins_Flag1=0 "); // set the flag one for the inspector
								 Intent Star = new Intent(Accessbymainapplication.this,InspectionDepot.class);
								 startActivity(Star);
							}
							
						//}
						
					}catch(Exception e)
					{
						System.out.println("problem not availabel in aft f erorr "+e);
					}
		
			   }
		}
		
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	      if (requestCode == request) {
	    	  Intent Star = new Intent(Accessbymainapplication.this,HomeScreen.class);
	    	  startActivity(Star);
	      }
	}
@Override
protected void onStop() {
	// TODO Auto-generated method stub
	//finish();
	super.onStop();
}
}
