package idsoft.inspectiondepot.B11802;

import java.net.URLDecoder;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PersonalInfo extends Activity {
	String homeid, fname, lname, addr, cty, zpcde, statnam, cntry, yrhme,
			wrkphn, cellphn, policynum, storiesnum, compname,schedate;
	String chkstatus[] = { "true", "true", "true", "true", "true", "true","true" };
	int value, Count, otheryear,mc = 0, f,updrws, yrchk,spinnerPosition;
	String inspstarttime, inspendtime, insurancedate, insname, schdat, schcmt,
			insdate, strfname, strlname, straddr, strcity, strzip, strstate,
			strcountry, stryrofhme, InspectionType, status, streffdat,updatecnt,
			strcontactperson, strhmephn, strwrkphn, strcellphn, strpolicy,anytype = "anyType{}", flag, homephn,
			strstories, stremail, strcompname,YearofBuilt;
	static final int DATE_DIALOG_ID = 0;
	private static final String TAG = null;
	private int mYear, mMonth, mDay;
	Button getdate, getinspdate;
	CheckBox mailchk;
	View v1;
	ArrayAdapter adapter;
	TextView viewqainspector;
	String yearb[] = { "Select", "Other", "1964", "1965", "1966", "1967",
			"1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975",
			"1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983",
			"1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991",
			"1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999",
			"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007",
			"2008", "2009", "2010", "2011", "2012","2013" };

	Spinner yearofhome;	
	private static final int visibility = 0;
	TextView inspectiondate, txtinsdat, txtfirstname, txtlastname, txtaddress,
			txtcity, txtzip, txtstate, txtcounty, txtyearofhome, txthomephone,policyholderinfo,originaldatayear,
			txtnoofstories, txtemail, txtinsudate,scheduledate, dateview, companyname,scheddate, starttime, endtime;
	EditText firstname, effdat, schedulecomment, contactperson, lastname,otheryearhome,
			homephone, address, workphone, city, cellphone, zip, policy, state,
			country, stories, email;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	CommonFunctions cf;
	
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());

		setContentView(R.layout.homeownerinfo);
		cf.Createtablefunction(9);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.getDeviceDimensions();
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 1, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 11,
				cf, 1));
		getinsurancerenewaldate();

		txtfirstname = (TextView) this.findViewById(R.id.txtfirst);
		txtfirstname.setText(Html.fromHtml("<font color=red> *"
				+ "</font>First Name"));
		txtlastname = (TextView) this.findViewById(R.id.txtlast);
		txtlastname.setText(Html.fromHtml("<font color=red> *"
				+ "</font>Last Name"));
		txtaddress = (TextView) this.findViewById(R.id.txtaddr);
		txtaddress.setText(Html.fromHtml("<font color=red> *"
				+ "</font>Address"));
		txtcity = (TextView) this.findViewById(R.id.txtcity);
		txtcity.setText(Html.fromHtml("<font color=red> *" + "</font>City"));
		txtzip = (TextView) this.findViewById(R.id.txtzip);
		txtzip.setText(Html.fromHtml("<font color=red> *" + "</font>Zip"));

		txtstate = (TextView) this.findViewById(R.id.txtstate);
		txtstate.setText(Html.fromHtml("<font color=red> *" + "</font>State"));
		txtcounty = (TextView) this.findViewById(R.id.txtcountry);
		txtcounty.setText(Html.fromHtml("<font color=red> *"
				+ "</font>County"));
		txtyearofhome = (TextView) this.findViewById(R.id.txtyear);
		txtyearofhome.setText(Html.fromHtml("<font color=red> *"
				+ "</font>Year of Home"));

		starttime = (TextView) this.findViewById(R.id.viewstartime);
		endtime = (TextView) this.findViewById(R.id.viewendtime);
		scheddate = (TextView) this.findViewById(R.id.viewsscheduledate);
		txthomephone = (TextView) this.findViewById(R.id.txthomephn);
		txthomephone.setText(Html.fromHtml("<font color=red> *"
				+ "</font>Home Phone"));
		txtnoofstories = (TextView) this.findViewById(R.id.txtstory);
		txtnoofstories.setText(Html.fromHtml("<font color=red> *"
				+ "</font># of stories"));
		txtemail = (TextView) this.findViewById(R.id.txtmail);
		txtemail.setText(Html.fromHtml("<font color=red> *" + "</font>Email"));
		txtinsudate = (TextView) this.findViewById(R.id.txtinscomp);
		txtinsudate.setText(Html.fromHtml("Insurance Company"));

		txtinsdat = (TextView) this.findViewById(R.id.txtinspdate);
		txtinsdat.setText(Html.fromHtml("Inspection Date"));

		firstname = (EditText) this.findViewById(R.id.efirstname);
		contactperson = (EditText) this.findViewById(R.id.econtactperson);
		lastname = (EditText) this.findViewById(R.id.elastname);
		homephone = (EditText) this.findViewById(R.id.ehomephone);
		address = (EditText) this.findViewById(R.id.eaddress);
		workphone = (EditText) this.findViewById(R.id.eworkphone);
		city = (EditText) this.findViewById(R.id.ecity);
		cellphone = (EditText) this.findViewById(R.id.ecellphone);
		zip = (EditText) this.findViewById(R.id.ezip);
		policy = (EditText) this.findViewById(R.id.epolicy);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		viewqainspector = (TextView) this.findViewById(R.id.viewqainspector);
		
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		try {
			Cursor cur = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
				if(cur.getCount()>0){
					cur.moveToFirst();			
					YearofBuilt = cur.getString(cur.getColumnIndex("s_YearBuilt"));
				//	originaldatayear.setText(YearofBuilt);
				}
		} catch (Exception e) {

		}
		state = (EditText) this.findViewById(R.id.estate);
		stories = (EditText) this.findViewById(R.id.estories);
		country = (EditText) this.findViewById(R.id.ecountry);
		email = (EditText) this.findViewById(R.id.email);
		yearofhome = (Spinner) this.findViewById(R.id.eyearofhome);
		otheryearhome = (EditText) this.findViewById(R.id.otheryearhome);
		adapter = new ArrayAdapter(PersonalInfo.this,
				android.R.layout.simple_spinner_item, yearb);
		adapter.setDropDownViewResource(R.layout.my_layout);
		

		yearofhome.setAdapter(adapter);
		

		yearofhome
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> parentView,
							View selectedItemView, int position, long id) {
						// your code here

						if (yearb[position].equals("Other")) {
							// otheryearhome.setText("");
							otheryearhome.setVisibility(View.VISIBLE);

						} else {
							otheryearhome.setVisibility(View.GONE);

						}
					}

					public void onNothingSelected(AdapterView<?> parentView) {
						// your code here
					}
				});
		companyname = (TextView) this.findViewById(R.id.viewinsurance); 
		effdat = (EditText) this.findViewById(R.id.eeffdate);
		schedulecomment = (EditText) this.findViewById(R.id.eschcomment);
		mailchk = (CheckBox) findViewById(R.id.valid);
		getdate = (Button) findViewById(R.id.getdate);
		inspectiondate = (TextView) findViewById(R.id.inspdate);
		//getinspdate = (Button) findViewById(R.id.getinsdate);
		if (cf.InspectionType.equals("28")) {
			policy.setEnabled(true);
			effdat.setEnabled(true);
			getdate.setVisibility(visibility);
		} else {
			policy.setEnabled(false);
			effdat.setEnabled(false);
			getdate.setVisibility(v1.GONE);
		}
		getdate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				f = 1;
				showDialog(DATE_DIALOG_ID);

			}
		});
		
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		cf.Createtablefunction(7);

		mailchk.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					mc = 1;
					email.setEnabled(false);
				} else {
					mc = 0;
					email.setEnabled(true);
				}
			}
		});
		viewownerinfo();
	}

	private void getinsurancerenewaldate() {
		// TODO Auto-generated method stub
		try {
			Cursor cur = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				if (cur.getString(cur.getColumnIndex("Insurancerenewaldate")).equals("")) 
				{
					Cursor cur1 = cf.db.rawQuery("select * from " + cf.cloudtable + " where Homeid='" + cf.Homeid + "'", null);
					if(cur1.getCount()>0)
					{
						cur1.moveToFirst();
						if (cur1 != null) {
							do {
								insurancedate = cur1.getString(cur1.getColumnIndex("policyEffectiveDate"));
								cf.db.execSQL("UPDATE " + cf.mbtblInspectionList
										+ " SET Insurancerenewaldate='" + insurancedate
										+ "' WHERE s_SRID ='" + cf.Homeid + "'");
		
							} while (cur1.moveToNext());
						}
					}
				} 
				else 
				{
	
				}
			}

		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" "+" problem in fetching Insurance renewal date at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.home:
			cf.gohome();
			break;
		case R.id.save:
			strfname = firstname.getText().toString();
			strlname = lastname.getText().toString();
			straddr = address.getText().toString();
			strcity = city.getText().toString();
			strzip = zip.getText().toString();
			strstate = state.getText().toString();
			strcountry = country.getText().toString();
			stryrofhme = yearofhome.getSelectedItem().toString();
			if (stryrofhme.equals("Other"))
				stryrofhme = otheryearhome.getText().toString();
			streffdat = inspectiondate.getText().toString();
			insurancedate = effdat.getText().toString();
			strcontactperson = contactperson.getText().toString();
			strhmephn = homephone.getText().toString();
			strwrkphn = workphone.getText().toString();
			strcellphn = cellphone.getText().toString();
			strpolicy = policy.getText().toString();
			strstories = stories.getText().toString();
			stremail = email.getText().toString();
			strcompname = companyname.getText().toString();
			schcmt = schedulecomment.getText().toString();
			schedate = scheddate.getText().toString();
			try {
				otheryear = Integer.parseInt(stryrofhme);
			} catch (Exception e) {
				otheryear = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" problem in converting datatypes on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			}
			if (!strwrkphn.equals("")) {
				if (strwrkphnvalid(strwrkphn) != "yes") {
					cf.ShowToast("Please enter the Work Phone in 10 digit Number.",1);
					workphone.setText("");
					workphone.requestFocus();
					chkstatus[0] = "false";
				} else {
					chkstatus[0] = "true";
				}
			} else {
				chkstatus[0] = "true";
			}
			if (!strcellphn.equals("")) {
				if (strcellphnvalid(strcellphn) != "yes") {
					cf.ShowToast("Please enter the Cell Phone in 10 digit Number.",1);
					cellphone.setText("");
					cellphone.requestFocus();
					chkstatus[1] = "false";
				} else {
					chkstatus[1] = "true";

				}
			} else {
				chkstatus[1] = "true";
			}
			if (!"".equals(insurancedate)) {
				try {

					if (cf.InspectionType.equals("28")) {
						if (checkforpolicyrenewaldate(insurancedate).equals(
								"true")) {
							chkstatus[5] = "true";
							if (checkforinsurancedate(insurancedate, streffdat)
									.equals("true")) {
								chkstatus[6] = "true";
							} else {
								cf.ShowToast("Insurance Renewal Date should not be greater than Inspection Date.",1);
								chkstatus[6] = "false";
							}
						} else {
							cf.ShowToast("Insurance Renewal Date should not be greater than Today's Date.",1);
							chkstatus[5] = "false";
						}
					}
				} catch (Exception e) {
					chkstatus[6] = "false";
					//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" problem in comparing insurance renewal date & policy effecive date on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					/*
					 * ShowToast toast = new ShowToast(getBaseContext(),
					 * "Insurance renewal date should be the format mm/dd/yyyy "
					 * );
					 */
				}
			}
			//if (!"".equals(streffdat)) {

				//if (!"".equals(schedate)) {
					/*
					 * chkstatus[4] = "true"; update();
					 */
					if (!"".equals(strfname)) {
						if (!"".equals(strlname)) {
							if (!"".equals(straddr)) {
								if (!"".equals(strcity)) {
									if (!"".equals(strzip)) {
										if (strzip.length() == 5) {
											if (!yearofhome.getSelectedItem()
													.toString()
													.equals("Select")) {
												if ((!yearofhome
														.getSelectedItem()
														.toString()
														.equals("Other"))
														|| (!otheryearhome
																.getText()
																.toString()
																.equals("") && 4 == otheryearhome
																.getText()
																.length())) {
													if (otheryear >= 1900
															&& otheryear <= 2013) {
														if (!"".equals(strhmephn)) {
															if (strhmephnvalid(
																	strhmephn)
																	.equals("yes")) {
																if (strstories
																		.length() != 0) {
																	if (!"0".equals(strstories)) {

																		if (stremail
																				.equals("")
																				&& (mc == 0)) {
																			cf.ShowToast("Please enter the Email.",1);
																			email.requestFocus();
																			chkstatus[2] = "false";
																		} else {
																			if (mc == 1) {
																				chkstatus[2] = "true";
																				update();
																			} else if (mc == 0) {
																				if (emailvalid(stremail) != "yes") {
																					cf.ShowToast("Please enter the valid Email.",1);
																					email.setText("");
																					email.requestFocus();
																					chkstatus[2] = "false";
																				} else {
																					chkstatus[2] = "true";
																					update();
																				}
																			}

																		}
																	} else {
																		cf.ShowToast("Please enter the No. of Stories.",1);
																		stories.setText("");
																		stories.requestFocus();
																	}
																}

																else {
																	cf.ShowToast("Please enter the No. of Stories.",1);
																	stories.requestFocus();
																}

															} else {
																cf.ShowToast("Please enter the Home Phone in 10 digit Number.",1);
																homephone
																		.setText("");
																homephone
																		.requestFocus();
															}
														} else {
															cf.ShowToast("Please enter the Home Phone Number.",1);
															homephone
																	.requestFocus();
														}
													} else {
														cf.ShowToast("Please enter year of home greater than 1900 and less than 2013.",1);
														otheryearhome
																.setText("");
														otheryearhome
																.requestFocus();
													}
												} else {
													
													if(otheryearhome.getText().toString().trim().equals(""))
													{
														cf.ShowToast("Please enter the other text for year of home.",1);	
													}
													else if(otheryearhome.getText().length()<4)
													{
														cf.ShowToast("Please enter Year of home in four digits.",1);	
													}
													otheryearhome.requestFocus();
												}

											} else {
												cf.ShowToast("Please select the Year of Home.",1);
												yearofhome.requestFocus();
											}

										} else {
											cf.ShowToast("Please enter the ZipCode.",1);
											zip.setText("");
											zip.requestFocus();
										}
									} else {
										cf.ShowToast("Please enter the ZipCode.",1);
										zip.requestFocus();
									}
								} else {
									cf.ShowToast("Please enter the City.",1);
									city.requestFocus();
								}
							} else {
								cf.ShowToast("Please enter the Address.",1);
								address.requestFocus();
							}
						} else {
							cf.ShowToast("Please enter the Last Name.",1);
							lastname.requestFocus();
						}
					}

					else {
						cf.ShowToast("Please enter the First Name.",1);
						firstname.requestFocus();
					}
				//}

			//} else {
				//cf.ShowToast("You cannot submit Policy Holder Information without Scheduling.",1);
			//}

			break;
		case R.id.cancl:
			cf.gohome();
			break;

		}
	}

	private String checkforgreaterinspectiondate(String getinspecdate,
			String getscheduleddate) {

		String chkdate = null;
		int i1 = getinspecdate.indexOf("/");
		String result = getinspecdate.substring(0, i1);
		int i2 = getinspecdate.lastIndexOf("/");
		String result1 = getinspecdate.substring(i1 + 1, i2);
		String result2 = getinspecdate.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);

		int i3 = getscheduleddate.indexOf("/");
		String result3 = getscheduleddate.substring(0, i3);
		int i4 = getscheduleddate.lastIndexOf("/");
		String result4 = getscheduleddate.substring(i3 + 1, i4);
		String result5 = getscheduleddate.substring(i4 + 1);
		result5 = result5.trim();
		int k1 = Integer.parseInt(result3);
		int k2 = Integer.parseInt(result4);
		int k = Integer.parseInt(result5);

		if (j > k) {
			chkdate = "true";
		} else if (j < k) {
			chkdate = "false";
		} else if (j == k) {

			if (j1 > k1) {

				chkdate = "true";
			} else if (j1 < k1) {
				chkdate = "false";
			} else if (j1 == k1) {
				if (j2 > k2) {
					chkdate = "true";
				} else if (j2 < k2) {
					chkdate = "false";
				} else if (j2 == k2) {

					chkdate = "true";
				}

			}

		}

		return chkdate;
	}

	private String checkforpolicyrenewaldate(String insurancedate) {
		String chkdate = "false";
		int i1 = insurancedate.indexOf("/");
		String result = insurancedate.substring(0, i1);
		int i2 = insurancedate.lastIndexOf("/");
		String result1 = insurancedate.substring(i1 + 1, i2);
		String result2 = insurancedate.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;
		if (j < thsyr || (j1 < curmnth && j <= thsyr)
				|| (j2 <= curdate && j1 <= curmnth && j <= thsyr)) {
			chkdate = "true";
		} else {
			chkdate = "false";
		}
		return chkdate;

	}

	private String checkfordateistoday(String getinspecdate2) {
		// TODO Auto-generated method stub
		String chkdate = null;
		int i1 = getinspecdate2.indexOf("/");
		String result = getinspecdate2.substring(0, i1);
		int i2 = getinspecdate2.lastIndexOf("/");
		String result1 = getinspecdate2.substring(i1 + 1, i2);
		String result2 = getinspecdate2.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;
		if (j < thsyr) {
			chkdate = "true";
		} else if (j == thsyr) {
			if (j1 < curmnth) {

				chkdate = "true";
			} else if (j1 > curmnth) {
				chkdate = "false";
			} else if (j1 == curmnth) {
				if (j2 < curdate) {
					chkdate = "true";
				} else if (j2 > curdate) {
					chkdate = "false";
				} else if (j2 == curdate) {

					chkdate = "true";
				}

			}
		}

		return chkdate;

	}

	private String checkforinsurancedate(String getinsurancedate,
			String getinspectiondate) {

		String chkdate = null;
		int i1 = getinsurancedate.indexOf("/");
		String result = getinsurancedate.substring(0, i1);
		int i2 = getinsurancedate.lastIndexOf("/");
		String result1 = getinsurancedate.substring(i1 + 1, i2);
		String result2 = getinsurancedate.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);

		int i3 = getinspectiondate.indexOf("/");
		String result3 = getinspectiondate.substring(0, i3);
		int i4 = getinspectiondate.lastIndexOf("/");
		String result4 = getinspectiondate.substring(i3 + 1, i4);
		String result5 = getinspectiondate.substring(i4 + 1);
		result5 = result5.trim();
		int k1 = Integer.parseInt(result3);
		int k2 = Integer.parseInt(result4);
		int k = Integer.parseInt(result5);

		if (j > k) {
			chkdate = "false";
		} else if (j < k) {
			chkdate = "true";
		} else if (j == k) {

			if (j1 > k1) {

				chkdate = "false";
			} else if (j1 < k1) {
				chkdate = "true";
			} else if (j1 == k1) {
				if (j2 > k2) {
					chkdate = "false";
				} else if (j2 < k2) {
					chkdate = "true";
				} else if (j2 == k2) {

					chkdate = "true";
				}

			}

		}

		return chkdate;
	}

	private void update() {
		// TODO Auto-generated method stub
		updatecnt = "1";
		if (chkstatus[0] == "true" && chkstatus[1] == "true"
				&& chkstatus[2] == "true" && chkstatus[3] == "true"
				&& chkstatus[5] == "true" && chkstatus[6] == "true") {

			try {
				Cursor c2 = cf.db.rawQuery("SELECT * FROM "
						+ cf.tbl_questionsdata + " WHERE SRID='" + cf.Homeid
						+ "'", null);
				int rws = c2.getCount();
				cf.db.execSQL("UPDATE " + cf.mbtblInspectionList
						+ " SET InspectionDate='" + streffdat
						+ "',s_OwnersNameFirst='"
						+ cf.convertsinglequotes(strfname)
						+ "',s_OwnersNameLast='"
						+ cf.convertsinglequotes(strlname)
						+ "',s_propertyAddress='"
						+ cf.convertsinglequotes(straddr) + "',s_City='"
						+ cf.convertsinglequotes(strcity) + "',s_ZipCode='"
						+ strzip + "',s_State='"
						+ cf.convertsinglequotes(strstate) + "',s_County='"
						+ cf.convertsinglequotes(strcountry)
						+ "',s_YearBuilt='" + stryrofhme
						+ "',s_ContactPerson='"
						+ cf.convertsinglequotes(strcontactperson)
						+ "',s_ContactHomePhone='" + strhmephn
						+ "',s_ContactWorkPhone='" + strwrkphn
						+ "',s_ContactCellPhone='" + strcellphn
						+ "',s_OwnerPolicyNumber='"
						+ cf.convertsinglequotes(strpolicy) + "',s_Nstories='"
						+ strstories + "',s_ContactEmail='" + stremail
						+ "',s_InsuranceCompany='"
						+ cf.convertsinglequotes(strcompname)
						+ "',Insurancerenewaldate='" + insurancedate
						+ "',chkbx='" + mc + "' WHERE s_SRID ='" + cf.Homeid
						+ "'");

				if (rws == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.tbl_questionsdata
							+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
							+ "VALUES ('" + cf.Homeid + "','','','" + 0 + "','"
							+ 0 + "','" + 0 + "','','','','','" + 0 + "','" + 0
							+ "','','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
							+ "','" + 0 + "','','" + 0 + "','" + 0 + "','" + 0
							+ "','','" + 0 + "','','" + 0 + "','" + 0
							+ "','','','','','','','','" + 0 + "','" + 0
							+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
							+ "','" + 0 + "','" + 0 + "','','" + 0 + "','" + 0
							+ "','','" + 0 + "','','"
							+ cf.convertsinglequotes(schcmt) + "','" + cd
							+ "','" + 0 + "')");
				} else {
					cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
							+ " SET ScheduleComments='"
							+ cf.convertsinglequotes(schcmt)
							+ "',ModifyDate ='" + md + "' WHERE SRID ='"
							+ cf.Homeid + "'");

				}

				Cursor c3 = cf.db.rawQuery("SELECT * FROM "
						+ cf.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
						+ "'", null);
				int subchkrws = c3.getCount();
				if (subchkrws == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.SubmitCheckTable
							+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
							+ "VALUES ('" + cf.InspectorId + "','" + cf.Homeid
							+ "',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");

				} else {
					cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
							+ " SET fld_policy='1' WHERE Srid ='" + cf.Homeid
							+ "' and InspectorId='" + cf.InspectorId + "'");
				}
				updatecnt = "1";
			} catch (Exception e) {

				updatecnt = "0";
				cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" "+" in inserting or updating policyholder info on "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				

			}
			if (!updatecnt.equals("0")) {
				cf.ShowToast("Policy Holder Information has been updated sucessfully.",1);
			}

		} else {

		}

	}

	private String validchar(String strlname2) {
		// TODO Auto-generated method stub
		String achk;
		String expression = "^[a-z_A-Z ]-*$";
		CharSequence inputStr = strlname2;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			achk = "yes";
		} else {
			achk = "no";
		}
		return achk;
	}

	private String strcellphnvalid(String strcellphn2) {
		// TODO Auto-generated method stub
		String cchk;
		if (strcellphn2.length() == 10) {
			StringBuilder sVowelBuilder = new StringBuilder(strcellphn2);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			strcellphn = sVowelBuilder.toString();
			cchk = "yes";

		} else {
			cchk = "no";
		}

		return cchk;

	}

	private String strwrkphnvalid(String strwrkphn2) {
		// TODO Auto-generated method stub
		String wchk;
		if (strwrkphn2.length() == 10) {
			StringBuilder sVowelBuilder = new StringBuilder(strwrkphn2);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			strwrkphn = sVowelBuilder.toString();
			wchk = "yes";

		} else {
			wchk = "no";
		}

		return wchk;
	}

	private String emailvalid(String stremail2) {
		// TODO Auto-generated method stub
		String echk = "no";
		Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher emailMatcher = emailPattern.matcher(stremail2);
		if (emailMatcher.matches()) {

			echk = "yes";
		} else {
			echk = "no";
		}
		return echk;

	}

	private String strhmephnvalid(String strhmephn2) {
		// TODO Auto-generated method stub
		String chk;
		if (strhmephn2.length() == 10) {
			StringBuilder sVowelBuilder = new StringBuilder(strhmephn2);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			strhmephn = sVowelBuilder.toString();
			chk = "yes";

		} else {
			chk = "no";
		}

		return chk;
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;

			if (f == 1) {
				if (cf.InspectionType.equals("28")) {
					effdat.setText(new StringBuilder()
							// Month is 0 based so add 1
							.append(mMonth + 1).append("/").append(mDay)
							.append("/").append(mYear).append(" "));
				} else {

				}

			} else {
				inspectiondate.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}

	public void viewownerinfo() {
		schdat = "";
		schcmt = "";
		insdate = "";
		inspstarttime = "";
		inspendtime = "";
		fname = "";
		lname = "";
		addr = "";
		cty = "";
		zpcde = "";
		statnam = "";
		cntry = "";
		yrhme = "";
		wrkphn = "";
		cellphn = "";
		policynum = "";
		storiesnum = "";
		compname = "";
		insname="";
		try {
			Cursor c2 = cf.db.rawQuery("SELECT a.* , b.Ins_CompanyName  FROM "
					+ cf.mbtblInspectionList + " as a Inner join  "
					+ cf.inspectorlogin
					+ " as b on b.Ins_Id = a.InspectorId  WHERE a.s_SRID='"
					+ cf.Homeid + "'", null);
			int rws = c2.getCount();
			int Column = c2.getColumnIndex("ScheduleDate");
			int Column1 = c2.getColumnIndex("InspectionComment");
			int Column2 = c2.getColumnIndex("ScheduleDate");
			int Column3 = c2.getColumnIndex("s_OwnersNameFirst");
			int Column4 = c2.getColumnIndex("s_OwnersNameLast");
			int Column5 = c2.getColumnIndex("s_propertyAddress");
			int Column6 = c2.getColumnIndex("s_City");
			int Column7 = c2.getColumnIndex("s_ZipCode");
			int Column8 = c2.getColumnIndex("s_State");
			int Column9 = c2.getColumnIndex("s_County");
			int Column10 = c2.getColumnIndex("s_YearBuilt");
			int Column11 = c2.getColumnIndex("s_ContactWorkPhone");
			int Column12 = c2.getColumnIndex("s_ContactCellPhone");
			int Column13 = c2.getColumnIndex("s_OwnerPolicyNumber");
			int Column14 = c2.getColumnIndex("s_Nstories");
			int Column15 = c2.getColumnIndex("InsurcarrierId");
			int Column16 = c2.getColumnIndex("s_ContactEmail");
			int Column17 = c2.getColumnIndex("s_ContactHomePhone");
			int Column18 = c2.getColumnIndex("Ins_CompanyName");
			int Column19 = c2.getColumnIndex("s_ContactPerson");
			int Column20 = c2.getColumnIndex("Insurancerenewaldate");
			int Column21 = c2.getColumnIndex("InsuranceCompanyname");
			int Column22 = c2.getColumnIndex("InspectionStartTime");
			int Column23 = c2.getColumnIndex("InspectionEndTime");
			int Column24 = c2.getColumnIndex("chkbx");
			c2.moveToFirst();
			if (c2 != null) {
				do {
					
					schdat = c2.getString(Column);
					schcmt = cf.getsinglequotes(c2.getString(Column1));
					insdate = c2.getString(Column2);
					fname = cf.getsinglequotes(c2.getString(Column3));
					lname = cf.getsinglequotes(c2.getString(Column4));
					addr = cf.getsinglequotes(c2.getString(Column5));
					cty = cf.getsinglequotes(c2.getString(Column6));
					zpcde = c2.getString(Column7);
					statnam = cf.getsinglequotes(c2.getString(Column8));
					cntry = cf.getsinglequotes(c2.getString(Column9));
					yrhme = c2.getString(Column10);
					wrkphn = cf.getsinglequotes(c2.getString(Column11));
					cellphn = cf.getsinglequotes(c2.getString(Column12));
					policynum = cf.getsinglequotes(c2.getString(Column13));
					storiesnum = c2.getString(Column14);
					compname = cf.getsinglequotes(c2.getString(Column15));
					schedulecomment.setText(schcmt);
					stremail = c2.getString(Column16);
					homephn =  cf.getsinglequotes(c2.getString(Column17));
					insname = c2.getString(Column21);
					System.out.println("InsuranceCompanyname is "+homephn);
					System.out.println("witho is "+c2.getString(Column17));
					
					inspstarttime = c2.getString(Column22);
					inspendtime = c2.getString(Column23);
					insurancedate = c2.getString(Column20);
					mc = c2.getInt(Column24);
					
					if (cf.getsinglequotes(
							c2.getString(c2.getColumnIndex("s_ContactPerson")))
							.equals("N/A")) {
						contactperson.setText("");
					} else {
						contactperson
								.setText(cf.getsinglequotes(c2.getString(c2
										.getColumnIndex("s_ContactPerson"))));
					}
					effdat.setText(insurancedate);
					if (insdate.equals(anytype) || insdate.equals("Null")
							|| insdate.equals("Not Available")
							|| insdate.equals("") || insdate.equals("N/A")) {
						inspectiondate.setText("N/A");
					} else {
						inspectiondate.setText(insdate);
					}

					if (schdat.equals(anytype) || schdat.equals("Null")
							|| schdat.equals("Not Available")
							|| schdat.equals("") || schdat.equals("N/A")) {
						scheddate.setText("N/A");
					} else {
						scheddate.setText(schdat);
					}
					if (inspstarttime.equals(anytype)
							|| inspstarttime.equals("Null")
							|| inspstarttime.equals("Not Available")
							|| inspstarttime.equals("")
							|| inspstarttime.equals("N/A")) {
						starttime.setText("N/A");
					} else {
						starttime.setText(inspstarttime);
					}

					if (inspendtime.equals(anytype)
							|| inspendtime.equals("Null")
							|| inspendtime.equals("Not Available")
							|| inspendtime.equals("")
							|| inspendtime.equals("N/A")) {
						endtime.setText("N/A");
					} else {
						endtime.setText(inspendtime);
					}
					if (homephn.equals(anytype) || homephn.equals("Null")
							|| homephn.equals("Not Available")
							|| homephn.equals("N/A")) {
						homephone.setText("");
					} else {
						StringBuilder sVowelBuilder1 = new StringBuilder(
								homephn);
						sVowelBuilder1.deleteCharAt(0);
						sVowelBuilder1.deleteCharAt(3);
						sVowelBuilder1.deleteCharAt(6);
						homephn = sVowelBuilder1.toString();
						homephone.setText(homephn);
					}
					if (wrkphn.equals(anytype) || wrkphn.equals("Null")
							|| wrkphn.equals("Not Available")
							|| wrkphn.equals("") || wrkphn.equals("N/A")
							|| wrkphn.equals("(___)___-____")) {
						workphone.setText("");
					} else {
						StringBuilder sVowelBuilder1 = new StringBuilder(wrkphn);
						sVowelBuilder1.deleteCharAt(0);
						sVowelBuilder1.deleteCharAt(3);
						sVowelBuilder1.deleteCharAt(6);
						wrkphn = sVowelBuilder1.toString();
						workphone.setText(wrkphn);
					}
					if (cellphn.equals(anytype) || cellphn.equals("Null")
							|| cellphn.equals("Not Available")
							|| cellphn.equals("") || cellphn.equals("N/A")
							|| cellphn.equals("(___)___-____")) {
						cellphone.setText("");
					} else {
						StringBuilder sVowelBuilder1 = new StringBuilder(
								cellphn);
						sVowelBuilder1.deleteCharAt(0);
						sVowelBuilder1.deleteCharAt(3);
						sVowelBuilder1.deleteCharAt(6);
						cellphn = sVowelBuilder1.toString();
						cellphone.setText(cellphn);
					}
					if (fname.equals(anytype) || fname.equals("Null")
							|| fname.equals("Not Available")
							|| fname.equals("") || fname.equals("N/A")) {
						firstname.setText("");
					} else {
						
						firstname.setText(fname);
					}
					if (lname.equals(anytype) || lname.equals("Null")
							|| lname.equals("Not Available")
							|| lname.equals("") || lname.equals("N/A")) {
						lastname.setText("");
					} else {
						lastname.setText(lname);
					}
					if (addr.equals(anytype) || addr.equals("Null")
							|| addr.equals("Not Available") || addr.equals("")
							|| addr.equals("N/A")) {
						address.setText("");
					} else {
						address.setText(addr);
					}
					if (cty.equals(anytype) || cty.equals("Null")
							|| cty.equals("Not Available") || cty.equals("")
							|| cty.equals("N/A")) {
						city.setText("");
					} else {
						city.setText(cty);
					}
					if (zpcde.equals(anytype) || zpcde.equals("Null")
							|| zpcde.equals("Not Available")
							|| zpcde.equals("") || zpcde.equals("N/A")) {
						zip.setText("");
					} else {
						zip.setText(zpcde);
					}
					if (yrhme.equals(anytype) || yrhme.equals("Null")
							|| yrhme.equals("Not Available")
							|| yrhme.equals("") || yrhme.equals("N/A")) {
						yearofhome.setSelection(0);
					} else {
						int y1;
						try {
							y1 = Integer.parseInt(yrhme);
						} catch (Exception e) {
							yrhme = "0";
							y1 = 0;
							//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" problem in coverting integer datatypes on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
						}

						if (y1 > 2013 || y1 < 1964) {
							yearofhome.setSelection(1);
							otheryearhome.setText(yrhme.toString());

							otheryearhome.setVisibility(View.VISIBLE);

						} else {
							spinnerPosition = adapter.getPosition(yrhme);
							yearofhome.setSelection(spinnerPosition);
						}
					}

					if (statnam.equals(anytype) || statnam.equals("Null")
							|| statnam.equals("Not Available")
							|| statnam.equals("") || statnam.equals("N/A")) {
						state.setText("");
					} else {
						state.setText(statnam);
					}
					if (cntry.equals(anytype) || cntry.equals("Null")
							|| cntry.equals("Not Available")
							|| cntry.equals("") || cntry.equals("N/A")) {
						country.setText("");
					} else {
						country.setText(cntry);
					}

					if (policynum.equals(anytype) || policynum.equals("Null")
							|| policynum.equals("Not Available")
							|| policynum.equals("") || policynum.equals("N/A")) {
						policy.setText("");
					} else {

						policy.setText(policynum);
						if (cf.InspectionType.equals("29")) {
							if (policynum.equals("")) {
								policy.setEnabled(true);
							} else {
								policy.setEnabled(false);
							}
						}
					}
					if (storiesnum.equals(anytype) || storiesnum.equals("Null")
							|| storiesnum.equals("Not Available")
							|| storiesnum.equals("")
							|| storiesnum.equals("N/A")) {
						stories.setText("");
					} else {
						stories.setText(storiesnum);
					}
					if (schcmt.equals(anytype) || schcmt.equals("Null")
							|| schcmt.equals("Not Available")
							|| schcmt.equals("") || schcmt.equals("N/A")) {
						schedulecomment.setText("");
					} else {
						schedulecomment.setText(schcmt);
					}
					if (insname.equals(anytype) || insname.equals("Null")
							|| insname.equals("Not Available")
							|| insname.equals("") || insname.equals("N/A")) {
						companyname.setText("");
					} else {
						companyname.setText(cf.getsinglequotes(insname));

					}

					if (stremail.equals(anytype) || stremail.equals("Null")
							|| stremail.equals("Not Available")
							|| stremail.equals("") || stremail.equals("N/A")) {
						email.setText("");
					} else {
						email.setText(stremail);

					}
					if (mc == 1) {
						mailchk.setChecked(true);
						email.setEnabled(false);
					} else {
						mailchk.setChecked(false);
						email.setEnabled(true);
					}
					if(c2.getString(c2.getColumnIndex("QAInspector")).equals("True"))
					{
						viewqainspector.setText("Yes");	
					}
					else
					{
						viewqainspector.setText("No");
					}
					
					
					
					
				} while (c2.moveToNext());
			}
			c2.close();
			// cf.db.close();

		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" "+" problem in setting the policyholder info on "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg;

			intimg = new Intent(PersonalInfo.this, InspectionList.class);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				//System.out.println("zereereer");
				break;
			case -1:
				//System.out.println("coomme");
				//System.out.println("coommedata "+data);
				
				try {
					String[] projection = { MediaStore.Images.Media.DATA };//System.out.println("DATA");
					System.out.println("cf.mCapturedImageURI "+cf.mCapturedImageURI);
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
					//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" "+" in take photo "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					
				}
				
				break;

		}

	}
	
}
