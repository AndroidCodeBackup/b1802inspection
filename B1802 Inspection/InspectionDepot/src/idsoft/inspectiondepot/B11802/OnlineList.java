package idsoft.inspectiondepot.B11802;

import java.io.IOException;
import java.net.SocketTimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class OnlineList extends Activity {
	String status,insptype,statusname="",strtit,substatus,anytype = "anyType{}",flag="";
	CommonFunctions cf;
	private TextView welcome, title;
	private Button homepage, logout, search, search_clear_txt;
	EditText search_text;
	String data[],countarr[];
	int rws;
	String inspdata, query,res="",sql;
	LinearLayout inspectionlist;
	TextView tvstatus[];
	ScrollView sv;
	ProgressDialog pd1;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			status = extras.getString("status");
			substatus = extras.getString("substatus");
			insptype = extras.getString("InspectionType");
			flag = extras.getString("flag");
		}System.out.println("test");
		setContentView(R.layout.online);
		
		TextView tvheader = (TextView) findViewById(R.id.information);
		inspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		TextView headernote = (TextView) findViewById(R.id.note);
		if (status.equals("30")&&substatus.equals("0")) {
			
			tvheader.setText("Assigned Records");
			headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode");
			
		} else if ((status.equals("40")&&substatus.equals("0")) || status.equals("2")) {
			headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time");
			tvheader.setText("Schedule");
		} else if (status.equals("110")&&substatus.equals("111")) {
			headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time");
			tvheader.setText("Unable to Schedule Inspection");
		} else if (status.equals("90")&&substatus.equals("0")) {
			headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time");
			tvheader.setText("Cancelled Inspection");
		}
		else if (status.equals("40")&&substatus.equals("41")) {
			headernote.setText("Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time");
			tvheader.setText("Completed in Online");
		}
		cf = new CommonFunctions(this);
		cf.Createtablefunction(17);
		this.welcome = (TextView) this.findViewById(R.id.welcomename);
		this.homepage = (Button) this.findViewById(R.id.home);
		
		cf.getInspectorId();
		cf.welcomemessage();
		welcome.setText(cf.data);
		title = (TextView) this.findViewById(R.id.deleteiinformation);
		if (insptype.equals("28")) {
			strtit = "B1 - 1802 (Rev. 01/12)";
		} else {
			strtit = "B1 - 1802 (Rev. 01/12) Carr.Ord.";
		}
		this.homepage.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				Intent homepage = new Intent(OnlineList.this,
						HomeScreen.class);
				startActivity(homepage);

			}
		});
		this.search = (Button) this.findViewById(R.id.search);
		search_text = (EditText) findViewById(R.id.search_text);
		this.search_clear_txt = (Button) this.findViewById(R.id.search_clear_txt);
		this.search_clear_txt.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				search_text.setText("");
				res = "";
				dbquery();

			}

		});
		this.search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				String temp = cf.convertsinglequotes(search_text.getText()
						.toString());
				if (temp.equals("")) {
					cf.ShowToast("Please enter the Name or Policy Number to search.",1);

					search_text.requestFocus();
				} else {
					res = temp;
					
				}

			}

		});
		 String source = "<font color=#FFFFFF>Loading data. Please wait..."	+ "</font>";
		 pd1 = ProgressDialog.show(OnlineList.this,"", Html.fromHtml(source), true);
		 new Thread(new Runnable() {
                public void run() {
                	gethomeownerdetails();
                	finishedHandler.sendEmptyMessage(0);
                }
            }).start();
	}
	private void gethomeownerdetails()
	{
		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME29);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectionStatus",status);
		request.addProperty("SubStatus",substatus);
		request.addProperty("InspectiontypeID",insptype);		
		request.addProperty("inspectorid",cf.InspectorId.toString());
		request.addProperty("compflag",flag);
		System.out.println("REQUESTT "+request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		SoapObject result = null;
		try
		{
			androidHttpTransport.call(cf.SOAP_ACTION29, envelope);
			result = (SoapObject) envelope.getResponse();
			System.out.println("result inside"+result);
			if (result.equals(anytype)) {
				cf.ShowToast("Server is busy. Please try again later",1);
				cf.gohome();
			} else {
				System.out.println("else inside");
					InsertDate(result);
					//dbquery();
			}
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}
	private void dbquery() {

		int k = 1;
		data = null;
		inspdata = "";
		
		System.out.println("dbquery"+res);
		sql = "select * from " + cf.OnlineTable;
		if (!res.equals("")) {

			sql += " where (s_OwnersNameFirst like '%" + res
					+ "%' or s_OwnersNameLast like '%" + res
					+ "%' or s_OwnerPolicyNumber like '%" + res
					+ "%') and InspectionTypeId='" + insptype
					+ "' and InspectorId='" + cf.InspectorId + "'";
			if (status.equals("110")&& substatus.equals("111")) {
				sql += " and  Status=110 and SubStatus=111 and InspectionTypeId='" + insptype + "'";
			} else if (status.equals("90")&& substatus.equals("0")) {
				sql += " and Status=90 and SubStatus=0 and InspectionTypeId='" + insptype + "'";
			} else if (status.equals("40")&& substatus.equals("41")) {
				sql += " and Status=40 and SubStatus='41' and InspectionTypeId='" + insptype
						+ "'";
			}
			 else if (status.equals("40")&& substatus.equals("0")) {
					sql += " and Status=40 and SubStatus='0' and InspectionTypeId='" + insptype
							+ "'";
				}
			 else if (status.equals("30")&& substatus.equals("0")) {
					sql += " and Status=30 and SubStatus='0' and InspectionTypeId='" + insptype
							+ "'";
			}
			 else if(status.equals("2"))
			 {
					sql += " and ((Status='2' and SubStatus='0') or (Status='5' and SubStatus='0') or (Status='2' and SubStatus='121') or (Status='2' and SubStatus='122') or (Status='2' and SubStatus='123') or (Status='5' and SubStatus='151') or (Status='5' and SubStatus='153') or (Status='5' and SubStatus='152')  or (Status='70' and SubStatus='71') or (Status='140') or (Status='70')) and InspectionTypeId='" + insptype
							+ "'";
							
			 }

		} else {
			if (status.equals("110")&& substatus.equals("111")) {
				sql += " where Status=110 and SubStatus=111 and InspectionTypeId='" + insptype
						+ "' and InspectorId='" + cf.InspectorId + "'";
			} else if (status.equals("90")&& substatus.equals("0")) {
				sql += " where Status=90  and SubStatus=0 and InspectionTypeId='" + insptype
						+ "' and InspectorId='" + cf.InspectorId + "'";
			}  else if (status.equals("40")&& substatus.equals("41")) {
				sql += " where Status=40 and SubStatus='41' and InspectionTypeId='" + insptype
						+ "' and InspectorId='" + cf.InspectorId + "'";
			}
			 else if (status.equals("40")&& substatus.equals("0")) {
					sql += " where Status=40 and SubStatus='0' and InspectionTypeId='" + insptype
							+ "' and InspectorId='" + cf.InspectorId + "'";
				}
			 else if (status.equals("30")&& substatus.equals("0")) {
					sql += " where Status=30 and SubStatus='0' and InspectionTypeId='" + insptype
							+ "' and InspectorId='" + cf.InspectorId + "'";
				}
			 else if (status.equals("2")) {
				 sql += " where ((Status='2' and SubStatus='0') or (Status='5' and SubStatus='0') or (Status='2' and SubStatus='121') or (Status='2' and SubStatus='122') or (Status='2' and SubStatus='123') or (Status='5' and SubStatus='151') or (Status='5' and SubStatus='153') or (Status='5' and SubStatus='152')  or (Status='70' and SubStatus='71') or (Status='140') or (Status='70')) and InspectionTypeId='" + insptype
							+ "' and InspectorId='" + cf.InspectorId + "'";
			}
		}
		System.out.println("sql "+sql);
		Cursor cur = cf.db.rawQuery(sql, null);
		rws = cur.getCount();
		System.out.println("rws "+rws);
		title.setText(strtit + "\n" + "Total Record : " + rws);
		data = new String[rws];
		int j = 0;
		cur.moveToFirst();
		if (cur.getCount() >0) {
			do {
				System.out.println("do");
				statusname="";
				
				System.out.println("NAme="+cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_OwnersNameFirst")))+"Status="+cur.getString(cur.getColumnIndex("Status"))+"Subststus="+cur.getString(cur.getColumnIndex("SubStatus")));
				
				if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("0"))
				{
					statusname="IMC Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("0"))
				{
					statusname="IPA Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("121"))
				{
					statusname="Carrier Rejected";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("122"))
				{
					statusname="Carrier Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("123"))
				{
					statusname="Carrier Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("151"))
				{
					statusname="Carrier Rejected";
				}				
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("152"))
				{
					statusname="Carrier Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("153"))
				{
					statusname="Carrier Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("70") && cur.getString(cur.getColumnIndex("SubStatus")).equals("71"))
				{
					statusname="Citizen Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("70"))
				{
					statusname="Citizen Rejected";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("140"))
				{
					statusname="Closed";
				}
				
				inspdata += " "
						+ cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_OwnersNameFirst"))) + " ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_OwnersNameLast"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_OwnerPolicyNumber"))) + " \n ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_propertyAddress"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_City"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_State"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_County"))) + " | ";
				inspdata += cur.getString(cur.getColumnIndex("s_ZipCode"))
						+ " \n ";
				inspdata += cur.getString(cur.getColumnIndex("ScheduleDate"))
						+ " | ";
				inspdata += cur.getString(cur
						.getColumnIndex("InspectionStartTime")) + " - ";
				inspdata += cur.getString(cur
						.getColumnIndex("InspectionEndTime")) + " | "+statusname+"~";

				j++;

				if (inspdata.contains("null")) {
					inspdata = inspdata.replace("null", "");
				}
				if (inspdata.contains("N/A |")) {
					inspdata = inspdata.replace("N/A |", "");
				}
				if (inspdata.contains("N/A - N/A")) {
					inspdata = inspdata.replace("N/A - N/A", "");
				}
			} while (cur.moveToNext());
			System.out.println("move to next");
			search_text.setText("");
			display();
		} else {
			cf.ShowToast("Sorry, No results found for your search criteria.",1);
			inspectionlist.removeAllViews();
		}

	}

	private void InsertDate(SoapObject objInsert) throws NetworkErrorException,
	IOException, XmlPullParserException, SocketTimeoutException {
	 cf.dropTable(17);	
     cf.Createtablefunction(17);
 
int Cnt = objInsert.getPropertyCount();

for (int i = 0; i < Cnt; i++) {

	SoapObject obj = (SoapObject) objInsert.getProperty(i);
	try {
		String  dbstatus="",dbsubstatus="",email, cperson, IsInspected = "0", insurancecompanyname, IsUploaded = "0", homeid, firstname, lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname, scheduleddate, yearbuilt, nstories, inspectionstarttime, inspectionendtime, inspectioncomment, inspectiontypeid;
		homeid = String.valueOf(obj.getProperty("SRID"));
		firstname = String.valueOf(obj.getProperty("FirstName"));
		lastname = String.valueOf(obj.getProperty("LastName"));
		addr1 = String.valueOf(obj.getProperty("Address1"));
		city = String.valueOf(obj.getProperty("City"));
		state = String.valueOf(obj.getProperty("State"));
		country = String.valueOf(obj.getProperty("Country"));
		zip = String.valueOf(obj.getProperty("Zip"));
		ownerpolicyno = String
				.valueOf(obj.getProperty("OwnerPolicyNo"));
		scheduleddate = String
				.valueOf(obj.getProperty("ScheduledDate"));
    	inspectionstarttime = String.valueOf(obj
				.getProperty("InspectionStartTime"));
		inspectionendtime = String.valueOf(obj
				.getProperty("InspectionEndTime"));
		
		dbstatus = String.valueOf(obj
				.getProperty("Status"));
		dbsubstatus = String.valueOf(obj
				.getProperty("SubStatusID"));
		
		cf.db.execSQL("INSERT INTO "
				+ cf.OnlineTable
				+ " (s_SRID,InspectorId,Status,SubStatus,InspectionTypeId,s_OwnersNameFirst,s_OwnersNameLast,s_propertyAddress,s_City,s_State,s_County,s_ZipCode,s_OwnerPolicyNumber,ScheduleDate,InspectionStartTime,InspectionEndTime)"
				+ " VALUES ('" + homeid + "','"+ cf.InspectorId+"','"
				+ dbstatus+"','"
				+ dbsubstatus+"','"+insptype+"','"
				+ cf.convertsinglequotes(firstname) + "','"
				+ cf.convertsinglequotes(lastname) + "','"
				+ cf.convertsinglequotes(addr1) + "','"
				+ cf.convertsinglequotes(city) + "','"
				+ cf.convertsinglequotes(state) + "','"
				+ cf.convertsinglequotes(country) + "','" + zip
				+ "','" + cf.convertsinglequotes(ownerpolicyno) + "','"
				+ scheduleddate + "','" + inspectionstarttime + "','"
				+ inspectionendtime+"')");
		
		System.out.println("INSERT "+"INSERT INTO "
				+ cf.OnlineTable
				+ " (s_SRID,InspectorId,Status,SubStatus,InspectionTypeId,s_OwnersNameFirst,s_OwnersNameLast,s_propertyAddress,s_City,s_State,s_County,s_ZipCode,s_OwnerPolicyNumber,ScheduleDate,InspectionStartTime,InspectionEndTime)"
				+ " VALUES ('" + homeid + "','"+ cf.InspectorId+"','"
				+ dbstatus+"','"
				+ dbsubstatus+"','"+insptype+"','"
				+ cf.convertsinglequotes(firstname) + "','"
				+ cf.convertsinglequotes(lastname) + "','"
				+ cf.convertsinglequotes(addr1) + "','"
				+ cf.convertsinglequotes(city) + "','"
				+ cf.convertsinglequotes(state) + "','"
				+ cf.convertsinglequotes(country) + "','" + zip
				+ "','" + cf.convertsinglequotes(ownerpolicyno) + "','"
				+ scheduleddate + "','" + inspectionstarttime + "','"
				+ inspectionendtime+"')");
	} catch (Exception e) {
		System.out.println("e=" + e.getMessage());
	}

	
}

}
	private void display() {
		System.out.println("DISPALSA ");
		inspectionlist.removeAllViews();
		sv = new ScrollView(this);
		inspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		if (!inspdata.equals(null) && !inspdata.equals("null")
				&& !inspdata.equals("")) {
			this.data = inspdata.split("~");
			for (int i = 0; i < data.length; i++) {
				tvstatus = new TextView[rws];
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
						900, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);

				tvstatus[i] = new TextView(this);
				tvstatus[i].setMinimumWidth(580);
				tvstatus[i].setMaxWidth(580);
				tvstatus[i].setTag("textbtn" + i);
				System.out.println("data[i] "+data[i]);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
				lchkbox.addView(tvstatus[i], paramschk);

				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						93, 37);
				paramsdelbtn.rightMargin = 10;
				paramsdelbtn.bottomMargin = 10;
				l2.addView(ldelbtn);
		
			}
		}

	}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	        pd1.dismiss();
	        System.out.println("fnidnidi");
	        dbquery();
	    }
	};

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent loginpage = new Intent();
			loginpage.setClassName("idsoft.inspectiondepot.B11802",
					"idsoft.inspectiondepot.B11802.Startinspections");
			startActivity(loginpage);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
