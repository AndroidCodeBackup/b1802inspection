package idsoft.inspectiondepot.B11802;



import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

public class List_view_limit extends Activity {

	String colomnname="",statusname="",headernote="";String statusofdata;
	private TextView welcome, title;
	ArrayList<String> ExportList = new ArrayList<String>();
	private ListView lv1;
	TextView delrec;
	int val = 0, rws;
	Cursor c;
	//String[] homelist = new String[100];
	//String[] deletelist = new String[100];
	String inspdata, query;
	private Button homepage, search, search_clear_txt,deleteallinspections;
	public EditText search_text;
	String sql, strtit,alerttitle="";
	ScrollView sv;
	LinearLayout inspectionlist;
	String data[];CheckBox challtodelete;
	String countarr[];
	TextView tvstatus[];
	Button deletebtn[];View v;
	String value = "text", inspid, res = "";
	CommonFunctions cf;
	private ProgressDialog pd1;
	 int Status;
     int Substatus;
     int  total=0,show_handler=0;
     boolean online_rec=false;
     SoapObject result = null;
     private int rec_per_page;
 	private int numbeofpage;
     //int Inspectiontype>int</Inspectiontype>
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview_limit);
		Bundle b = getIntent().getExtras();
		
		
		if (b != null) {
			value = b.getString("id");
			inspid = b.getString("InspectionType");
			total = b.getInt("Total");
			online_rec = b.getBoolean("Online");
			
		}
		//((TextView) findViewById(R.id.total_rec)).setText(total+"");
		current=1;
		rec_per_page=50;
		if(total>rec_per_page)
		{
			numbeofpage=(int) (total/rec_per_page);
			findViewById(R.id.page_prev).setVisibility(View.GONE);
			if(total%rec_per_page!=0)
			{
				numbeofpage++;
			}
		}
		else
		{
			numbeofpage=1;
			findViewById(R.id.pre_next).setVisibility(View.GONE);
			
		}
		
		//setContentView(R.layout.expotlistview);
		TextView tvheader = (TextView) findViewById(R.id.information);
		TextView note  = (TextView) findViewById(R.id.note);
		
		if(value.equals("preonl"))
		{
			Status=40;
			Substatus=41;
		}
		else if(value.equals("uts"))
		{
			Status=110;
			Substatus=111;
		}
		else if(value.equals("can"))
		{
			Status=90;
			Substatus=0;
		}
		else if(value.equals("rr"))
		{
			Status=777;
			Substatus=0;
		}
		if("uts".equals(value)) {
		headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode";
		note.setText(headernote);
		}
		else
		{
			if("rr".equals(value)) 
			{
				 headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time | Status";
			}
			else
			{
				 headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time";	
			}
			
			note.setText(headernote);
		}
		
		
		inspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		if (value.equals("pretab")) {
			alerttitle="Are you sure want to delete all the inspections in the CIT status";
			statusofdata="1";
			colomnname="IsInspected";
			tvheader.setText("Completed Inspection in Tablet");
		} else if (value.equals("preonl")) {
			alerttitle="Are you sure want to delete all the inspections in the CIO status?";
			statusofdata="41";
			colomnname = "SubStatus";
			tvheader.setText("Completed Inspection in online");
		} else if (value.equals("uts")) {
			alerttitle="Are you sure want to delete all the inspections in the UTS status?";
			statusofdata="110";
			colomnname = "Status";
			tvheader.setText("Unable to Schedule Inspection");
		} else if (value.equals("can")) {
			alerttitle="Are you sure want to delete all the inspections in the CAN status?";
			statusofdata="90";
			colomnname = "Status";
			tvheader.setText("Cancelled Inspection");
		} else if (value.equals("rr")) {
			alerttitle="Are you sure want to delete all the inspections in the Reports Ready status?";
			statusofdata="90";
			colomnname = "Status";
			tvheader.setText("Reports Ready");
		}
		cf = new CommonFunctions(this);
		this.welcome = (TextView) this.findViewById(R.id.welcomename);
		this.homepage = (Button) this.findViewById(R.id.deletehome);
		deleteallinspections = (Button) this.findViewById(R.id.deleteallinspections);
		cf.getInspectorId();
		cf.welcomemessage();
		welcome.setText(cf.data);
		title = (TextView) this.findViewById(R.id.deleteiinformation);
		if (inspid.equals("28")) {
			strtit = "B1 - 1802 (Rev. 01/12)";
		} else {
			strtit = "B1 - 1802 (Rev. 01/12) Carr.Ord.";
		}
		this.homepage.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				cf.gohome();

			}
		});
		
		try {
			cf.Createtablefunction(2);
			Cursor c = cf.SelectTablefunction(cf.mbtblInspectionList,
					"where InspectorId='" + cf.InspectorId + "'");
			int rws = c.getCount();
			if(rws==0)
			{
				deleteallinspections.setVisibility(v.GONE);
			}
			else
			{
				deleteallinspections.setVisibility(v.VISIBLE);
			}
		} catch (Exception e) {
			System.out.println("eror " + e);

		}
		this.search = (Button) this.findViewById(R.id.search);
		search_text = (EditText) findViewById(R.id.search_text);
		search_text.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				//System.out.println("called correct");
				search_text.setFocusable(true);
				search_text.setFocusableInTouchMode(true);
				return false;
			}
		});
		findViewById(R.id.page_ed_go_to).setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				//System.out.println("called correct");
				findViewById(R.id.page_ed_go_to).setFocusable(true);
				findViewById(R.id.page_ed_go_to).setFocusableInTouchMode(true);
				return false;
			}
		});
		this.search_clear_txt = (Button) this
				.findViewById(R.id.search_clear_txt);
		
		this.search_clear_txt.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				search_text.setText("");
				res = "";
				dbquery();

			}

		});
		this.search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				String temp = cf.convertsinglequotes(search_text.getText()
						.toString());
				if (temp.equals("")) {
					cf.ShowToast("Please enter the Name or Policy Number to search.",1);

					search_text.requestFocus();
				} else {
					res = temp;
					dbquery();
				}

			}

		});
		try {
			get_fromweb(1,rec_per_page);
			
		} catch (Exception e) {
			System.out.println("Exception"+e.getMessage());
			cf.ShowToast("There is a problem in saving your data due to invalid character.",1);

		}
		cf.hidekeyboard(search_text);
	}

	private void get_fromweb(final int start,final int end) {
		// TODO Auto-generated method stub
		 String source = "<font color=#FFFFFF>Loading data. Please wait..."	+ "</font>";
		 pd1 = ProgressDialog.show(List_view_limit.this,"", Html.fromHtml(source), true);
		 new Thread(new Runnable() {
             public void run() {
             	gethomeownerdetails(start,end);
             	
             }
         }).start();
		//dbquery();
	}
	private void gethomeownerdetails(int start,int end)
	{
		if(end>total)
		{
			end=total;
		}
		SoapObject request = new SoapObject(cf.NAMESPACE, "UpdateMobileDB_Paging");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Status",Status);
		request.addProperty("Substatus",Substatus);
		request.addProperty("Inspectiontype",inspid);
		request.addProperty("Startid",start);
		request.addProperty("Endid",end);
		request.addProperty("inspectorid",cf.InspectorId.toString());
		System.out.println(" the property="+request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		result = null;
		try
		{
			androidHttpTransport.call("http://tempuri.org/UpdateMobileDB_Paging", envelope);
			result = (SoapObject) envelope.getResponse();
			
			if (result.equals("anyType{}")) {
				//cf.ShowToast("Server is busy. Please try again later",1);
				cf.gohome();
				show_handler=0;
			} else {
				show_handler=1;
				finishedHandler.sendEmptyMessage(0);
					
					
			}
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}
	private void InsertData(SoapObject objInsert) {
	 cf.dropTable(17);	
     cf.Createtablefunction(17);
     
int Cnt = objInsert.getPropertyCount();
if(current*rec_per_page>=total)
{
	findViewById(R.id.page_next).setVisibility(View.GONE);
}
else
{
	findViewById(R.id.page_next).setVisibility(View.VISIBLE);
}
//((TextView) findViewById(R.id.no_of_rec)).setText(Cnt+"");
for (int i = 0; i < Cnt; i++) {

	SoapObject obj = (SoapObject) objInsert.getProperty(i);
	try {
		
	
      
		String  dbstatus="",dbsubstatus="",email, cperson, IsInspected = "0", insurancecompanyname, IsUploaded = "0", homeid, firstname, lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname, scheduleddate, yearbuilt, nstories, inspectionstarttime, inspectionendtime, inspectioncomment, inspectiontypeid;
		homeid = String.valueOf(obj.getProperty("SRID"));
		firstname = String.valueOf(obj.getProperty("FirstName"));
		lastname = String.valueOf(obj.getProperty("LastName"));
		addr1 = String.valueOf(obj.getProperty("Address1"));
		city = String.valueOf(obj.getProperty("City"));
		state = String.valueOf(obj.getProperty("State"));
		country = String.valueOf(obj.getProperty("Country"));
		zip = String.valueOf(obj.getProperty("Zip"));
		ownerpolicyno = String
				.valueOf(obj.getProperty("OwnerPolicyNo"));
		scheduleddate = String
				.valueOf(obj.getProperty("ScheduledDate"));
    	inspectionstarttime = String.valueOf(obj
				.getProperty("InspectionStartTime"));
		inspectionendtime = String.valueOf(obj
				.getProperty("InspectionEndTime"));
		
		dbstatus = String.valueOf(obj
				.getProperty("Status"));
		dbsubstatus = String.valueOf(obj
				.getProperty("SubStatusID"));
		
		cf.db.execSQL("INSERT INTO "
				+ cf.OnlineTable
				+ " (s_SRID,InspectorId,Status,SubStatus,InspectionTypeId,s_OwnersNameFirst,s_OwnersNameLast,s_propertyAddress,s_City,s_State,s_County,s_ZipCode,s_OwnerPolicyNumber,ScheduleDate,InspectionStartTime,InspectionEndTime)"
				+ " VALUES ('" + homeid + "','"+ cf.InspectorId+"','"
				+ dbstatus+"','"
				+ dbsubstatus+"','"+inspid+"','"
				+ cf.convertsinglequotes(firstname) + "','"
				+ cf.convertsinglequotes(lastname) + "','"
				+ cf.convertsinglequotes(addr1) + "','"
				+ cf.convertsinglequotes(city) + "','"
				+ cf.convertsinglequotes(state) + "','"
				+ cf.convertsinglequotes(country) + "','" + zip
				+ "','" + cf.convertsinglequotes(ownerpolicyno) + "','"
				+ scheduleddate + "','" + inspectionstarttime + "','"
				+ inspectionendtime+"')");
		
		
	} catch (Exception e) {
		System.out.println("e=" + e.getMessage());
	}

	
}

}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	        
	        //System.out.println("fnidnidi");
	    	if(show_handler==1)
	    	{
	    		InsertData(result);
	    		dbquery();
	    	}
	    	else
	    	{
	    		cf.ShowToast("Server is busy. Please try again later",1);
	    	}
	        pd1.dismiss();
	    }
	};
	private int current;
	

	private void dbquery() {
		int k = 1;
		data = null;
		inspdata = "";
		
		
		sql = "select * from " + cf.OnlineTable;
		if (!res.equals("")) {

			sql += " where (s_OwnersNameFirst like '%" + res
					+ "%' or s_OwnersNameLast like '%" + res
					+ "%' or s_OwnerPolicyNumber like '%" + res
					+ "%') and InspectionTypeId='" + inspid
					+ "' and InspectorId='" + cf.InspectorId + "'";
			

		} 
		((TextView)findViewById(R.id.page_txt_no_fo_page)).setText("Current Page/Total Pages: "+current+"/"+numbeofpage);
		if(current>1)
		{
			findViewById(R.id.page_prev).setVisibility(View.VISIBLE);
		}
		
		Cursor cur = cf.db.rawQuery(sql, null);
		rws = cur.getCount();
		
		title.setText(strtit + "\n" + "Showing Records/Total Records : " + rws+"/"+total);
		data = new String[rws];
		countarr=new String[rws];
		int j = 0;
		cur.moveToFirst();
		if (cur.getCount() >0) {
			do {
				
				statusname="";
				if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("0"))
				{
					statusname="IMC Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("0"))
				{
					statusname="IPA Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("121"))
				{
					statusname="Carrier Rejected";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("122"))
				{
					statusname="Carrier Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("2") && cur.getString(cur.getColumnIndex("SubStatus")).equals("123"))
				{
					statusname="Carrier Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("151"))
				{
					statusname="Carrier Rejected";
				}				
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("152"))
				{
					statusname="Carrier Accepted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("5") && cur.getString(cur.getColumnIndex("SubStatus")).equals("153"))
				{
					statusname="Carrier Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("70") && cur.getString(cur.getColumnIndex("SubStatus")).equals("71"))
				{
					statusname="Citizen Resubmitted";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("70"))
				{
					statusname="Citizen Rejected";
				}
				else if(cur.getString(cur.getColumnIndex("Status")).equals("140"))
				{
					statusname="Closed";
				}
				
				inspdata += " "
						+ cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_OwnersNameFirst"))) + " ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_OwnersNameLast"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_OwnerPolicyNumber"))) + " \n ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_propertyAddress"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_City"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_State"))) + " | ";
				inspdata += cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("s_County"))) + " | ";
				inspdata += cur.getString(cur.getColumnIndex("s_ZipCode"))
						+ " \n ";
				inspdata += cur.getString(cur.getColumnIndex("ScheduleDate"))
						+ " | ";
				inspdata += cur.getString(cur
						.getColumnIndex("InspectionStartTime")) + " - ";
				inspdata += cur.getString(cur
						.getColumnIndex("InspectionEndTime")) + " | "+statusname+"~";
				countarr[j]=cur.getString(cur.getColumnIndex("s_SRID"));
				j++;

				if (inspdata.contains("null")) {
					inspdata = inspdata.replace("null", "");
				}
				if (inspdata.contains("N/A |")) {
					inspdata = inspdata.replace("N/A |", "");
				}
				if (inspdata.contains("N/A - N/A")) {
					inspdata = inspdata.replace("N/A - N/A", "");
				}
			} while (cur.moveToNext());
			
			search_text.setText("");
			display();
		} else {
			cf.ShowToast("Sorry, No results found for your search criteria.",1);
			inspectionlist.removeAllViews();
		}


	}

	private void display() {
		
		inspectionlist.removeAllViews();
		sv = new ScrollView(this);
		inspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
	
		if (!inspdata.equals(null) && !inspdata.equals("null")
				&& !inspdata.equals("")) {
			this.data = inspdata.split("~");
			System.out.println("comes correct"+data.length);
			for (int i = 0; i < data.length; i++) {
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
			
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				l2.setLayoutParams(mainparamschk);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
				tvstatus[i] = new TextView(this);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

			    lchkbox.addView(tvstatus[i], paramschk);

				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
				ldelbtn.setLayoutParams(mainparamschk);
				ldelbtn.setGravity(Gravity.RIGHT);
			    l2.addView(ldelbtn);
				deletebtn[i] = new Button(this);
				deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
				deletebtn[i].setTag("deletebtn" + i);
				deletebtn[i].setPadding(30, 0, 0, 0);
				if(!online_rec)
				{
					ldelbtn.addView(deletebtn[i], paramsdelbtn);
				}
				
				
				deletebtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace(
								"deletebtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						final String dt = countarr[cvrtstr];
						 cf.alerttitle="Delete";
						  cf.alertcontent="Are you sure want to delete?";
						    final Dialog dialog1 = new Dialog(List_view_limit.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText( cf.alerttitle);
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml( cf.alertcontent));
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							btn_yes.setOnClickListener(new OnClickListener()
							{
				           	public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									cf.fn_delete(dt);
									dbquery();
								}
								
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{

								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									
								}
								
							});
							dialog1.setCancelable(false);
							dialog1.show();
						

					}
				});
			}
		}

	}



	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent loginpage = new Intent(List_view_limit.this,Startinspections.class);
			startActivity(loginpage);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	{ 
		  switch(v.getId())
		  {
		  case R.id.deleteallinspections:
				String temp="(";
				if(value.equals("rr"))
				{
					c=cf.db.rawQuery(" Select * from "+cf.mbtblInspectionList+" where (Status =2 or Status =5 or Status =140)  and InspectorId='"+cf.InspectorId+"' and InspectionTypeId ='"+inspid+"'",null);
				}
				else
				{
					c=cf.db.rawQuery(" Select * from "+cf.mbtblInspectionList+" where "+colomnname+" ="+statusofdata+" and InspectorId='"+cf.InspectorId+"' and InspectionTypeId ='"+inspid+"'",null);	
				}
				 
			 	 
				 if( c.getCount()>=1)
				 {
					 c.moveToFirst();
					 for(int i=0;i<c.getCount();i++)
					 {
						 temp+="'"+c.getString(c.getColumnIndex("s_SRID"))+"'";
						 if((i+1)==(c.getCount()))
						 {
							 temp+=")";
							// return;
						 }
						 else
						 {
							 temp+=",";
							 c.moveToNext();
						 }
					 }
					 cf.delete_all(temp,alerttitle); 
			
			  
		  }
				 break;
		  case R.id.page_next:
				/*Intent in =new Intent(this,View_pdf.class);
				in.putExtra("type", status);
				in.putExtra("classidentifier", classidentifier);
				in.putExtra("total_record", no_of_record);
				in.putExtra("current", current+1);
				startActivity(in);*/
				current++;
				get_fromweb(((rec_per_page*(current-1))+1),(rec_per_page*current));
			break;
			case R.id.page_prev:
				/*Intent in_prev =new Intent(this,View_pdf.class);
				in_prev.putExtra("type", status);
				in_prev.putExtra("classidentifier", classidentifier);
				in_prev.putExtra("total_record", no_of_record);
				in_prev.putExtra("current", current-1);
				startActivity(in_prev);*/
				current--;
				get_fromweb(((rec_per_page*(current-1))+1),(rec_per_page*current));
			break;
			case R.id.page_goto:
				String go=((EditText) findViewById(R.id.page_ed_go_to)).getText().toString();
				if(!go.equals(""))
				{
					if(Integer.parseInt(go)<=numbeofpage)
					{
						if(Integer.parseInt(go)>0)
						{

						if(Integer.parseInt(go)!=current)
						{
							/*Intent in_goto =new Intent(this,View_pdf.class);
							in_goto.putExtra("type", status);
							in_goto.putExtra("classidentifier", classidentifier);
							in_goto.putExtra("total_record", no_of_record);
							in_goto.putExtra("current", Integer.parseInt(go));
							startActivity(in_goto);*/
							current=Integer.parseInt(go);
							((EditText) findViewById(R.id.page_ed_go_to)).setText("");
							get_fromweb(((rec_per_page*(current-1))+1),(rec_per_page*current));
						}else
						{
							cf.ShowToast("Request page and current page are same ", 0);
						}
						}else
						{
							cf.ShowToast("Enter valid page no ", 0);
						}
					}
					else
					{
						cf.ShowToast("Invalid page no", 0);
					}
				}else
				{
					cf.ShowToast("Invalid page no", 0);
				}
						
			break;
	 }
	 }
	
}
