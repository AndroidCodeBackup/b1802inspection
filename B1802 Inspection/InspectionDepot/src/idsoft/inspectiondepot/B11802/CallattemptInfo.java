package idsoft.inspectiondepot.B11802;


import idsoft.inspectiondepot.B11802.SWR.QUES_textwatcher;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Calendar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class CallattemptInfo extends Activity implements Runnable {
	private int mYear, mMonth, mDay;
	static final int DATE_DIALOG_ID = 0;
	protected static final int visibility = 0;
	EditText etgen, Calldate, Callday, Calltime, Numbercalled, Personanswered,resul_other,
			Initialcomments, OtherTitle;
	TextView tvgen,generalcmt;
	SoapObject res;
	View v1;TableLayout RCT_ShowValue;
	android.text.format.DateFormat df;
	RadioButton chkbx1, chkbx2,h_ph,w_ph,c_ph,a_ph,a_c_ph;
	CharSequence cd, md;
	final Calendar cal = Calendar.getInstance();
	Button Submitcallattempt, getdate, Currenttime, Submitandcancelinspection,
			seldate;
	TableLayout perstbl, tittbl, comttbl, lsttblhdr;
	TextView viewhomeph, txtPersonanswered, txtInitialcomments, txtTitle,
			txtOther, txtnolist;
	LinearLayout callatt_parent,callatt_type;
	TextView callatt_txt;
	String titlespin, resultspin, phnnum, phnnum1,homeid, InspectionType, status,
			selitem, selresitem, seltitle, strcaldat, strcalday, strcaltime,
			strcalcom, strcalnum, strcalres, strcaltit, chkalert, assdate;
	static String anytype = "anyType{}";
	String[] array_unable, array_insp, array_res, array_title;
	int value, Count, c, selitmflg, selresitmflg, seltitflg, k, verify = 0,
			day, cnt, ichk;
	TextView edit_link[]=new TextView[5];
	static String[] arrtit, arrnum, arrres, arrcom, arrdat, arrday, arrtime;
	private ArrayAdapter<CharSequence> loUnableschedule, loInspectionrefused,
			loResult, loTitle;
	Spinner Unableschedule, Inspectionrefused, Result, Title;
	TableRow tr1, tr2, tr3, tr4;
	Cursor cur1;
	ProgressDialog pd;
	CommonFunctions cf;
	
	TextView policyholderinfo;
	private AlertDialog alertDialog;
	String W_phone,H_phone,C_phone, A_phone,A_C_phone; 
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);

		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		setContentView(R.layout.callattemptinfo);
		cf.getDeviceDimensions();
		cf.getInspectorId();
		cf.getinspectiondate();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 1, cf, 0));
		/** Ph submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 13,cf, 1));
		ScrollView scr=(ScrollView)findViewById(R.id.scr);
		   scr.setMinimumHeight(cf.ht);
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());

		Cursor cur = cf.db.rawQuery("SELECT InspectorId FROM "
				+ cf.mbtblInspectionList + " WHERE s_SRID='" + cf.Homeid + "'",
				null);
		cur.moveToFirst();
		cur1 = cf.db.rawQuery(
				"SELECT Ins_FirstName,INS_LastName FROM " + cf.inspectorlogin
						+ " WHERE Ins_Id='"
						+ cur.getString(cur.getColumnIndex("InspectorId"))
						+ "'", null);
		cur1.moveToFirst();
		this.chkbx1 = (RadioButton) this.findViewById(R.id.chk1);
		chkbx1.setOnClickListener(OnClickListener);
		this.chkbx2 = (RadioButton) this.findViewById(R.id.chk2);
		chkbx2.setOnClickListener(OnClickListener);

		this.Unableschedule = (Spinner) this
				.findViewById(R.id.spunableschedule);
		this.Inspectionrefused = (Spinner) this
				.findViewById(R.id.spinspectionrefused);

		Unableschedule.setEnabled(false);
		Inspectionrefused.setEnabled(false);

	//	viewhomeph = (TextView) this.findViewById(R.id.viewhomephone);

		this.tvgen = (TextView) this.findViewById(R.id.gencmt);
		this.etgen = (EditText) this.findViewById(R.id.ed1);
		this.Calldate = (EditText) this.findViewById(R.id.calldate);
		this.getdate = (Button) this.findViewById(R.id.currentdate);
		this.seldate = (Button) this.findViewById(R.id.seldate);
		this.Callday = (EditText) this.findViewById(R.id.callday);
		this.Calltime = (EditText) this.findViewById(R.id.calltime);
		this.Currenttime = (Button) this.findViewById(R.id.currenttime);
		this.Numbercalled = (EditText) this.findViewById(R.id.numbercalled);
		this.Result = (Spinner) this.findViewById(R.id.spresult);
		resul_other = (EditText) findViewById(R.id.resul_other);
		h_ph=(RadioButton) findViewById(R.id.Call_H_ph);
		w_ph=(RadioButton) findViewById(R.id.Call_W_ph);
		c_ph=(RadioButton) findViewById(R.id.Call_C_ph);
		a_ph=(RadioButton) findViewById(R.id.Call_A_ph);
		a_c_ph=(RadioButton) findViewById(R.id.Call_A_C_ph);
		edit_link[0]=(TextView) findViewById(R.id.c_edit_H_ph);
		edit_link[1]=(TextView) findViewById(R.id.c_edit_W_ph);
		edit_link[2]=(TextView) findViewById(R.id.c_edit_C_ph);
		edit_link[3]=(TextView) findViewById(R.id.c_edit_A_ph);
		edit_link[4]=(TextView) findViewById(R.id.c_edit_A_C_ph);
		RCT_ShowValue=(TableLayout) findViewById(R.id.call_ShowValue);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		cf.setRcvalue(rccode);
		try
		{
			
			Cursor cur1=cf.SelectTablefunction(cf.mbtblInspectionList, " where s_SRID='" + cf.Homeid + "' and InspectorId='"+cf.InspectorId+"'");
			
			if(cur1.getCount()>0)
			{
				cur1.moveToFirst();
			
                  H_phone=cf.getsinglequotes(cur1.getString(cur1.getColumnIndex("s_ContactHomePhone"))).trim();
                  W_phone=cf.getsinglequotes(cur1.getString(cur1.getColumnIndex("s_ContactWorkPhone"))).trim();
                  C_phone=cf.getsinglequotes(cur1.getString(cur1.getColumnIndex("s_ContactCellPhone"))).trim();
            
                  
                  if(H_phone.equals("") || H_phone.equals("(000)000-0000") || H_phone.equals("N/A"))
                  {
                	  h_ph.setText("");
                	  
                	  h_ph.setVisibility(View.INVISIBLE);
                	  //edit_link[0].setVisibility(View.INVISIBLE);
                	  edit_link[0].setText("Add");
                	  h_ph.setOnClickListener(new OnClickListener() {
    						
    						public void onClick(View v) {
    							
    							// TODO Auto-generated method stub
    							
    							if(h_ph.isChecked())
    							{
    								w_ph.setChecked(false);
    								c_ph.setChecked(false);
    								a_ph.setChecked(false);
    								a_c_ph.setChecked(false);
    								H_phone=H_phone.replace("(", "");
    								H_phone=H_phone.replace(")", "");
    								H_phone=H_phone.replace("-", "");
    								
    								Numbercalled.setText(H_phone);
    							}
    						}
                	  });
                  }
                  else
                  {
                	  h_ph.setText(H_phone);
                	 // edit_link[0].setVisibility(View.VISIBLE);
                	  h_ph.setOnClickListener(new OnClickListener() {
						
                		  public void onClick(View v) {
							
							// TODO Auto-generated method stub
							
							if(h_ph.isChecked())
							{
								w_ph.setChecked(false);
								c_ph.setChecked(false);
								a_ph.setChecked(false);
								a_c_ph.setChecked(false);
								H_phone=H_phone.replace("(", "");
								H_phone=H_phone.replace(")", "");
								H_phone=H_phone.replace("-", "");
								
								Numbercalled.setText(H_phone);
							}
						}
					});
                  }
                  if(W_phone.equals("") || W_phone.equals("(000)000-0000") || W_phone.equals("N/A"))
                  {                	  
                	  w_ph.setText("");
                	  w_ph.setVisibility(View.INVISIBLE);
                	  //edit_link[1].setVisibility(View.INVISIBLE);
                	  edit_link[1].setText("Add");
                	  w_ph.setOnClickListener(new OnClickListener() {
    						
    						public void onClick(View v) {
    							// TODO Auto-generated method stub
    							
    							if(w_ph.isChecked())
    							{
    								
    								h_ph.setChecked(false);
  								c_ph.setChecked(false);
  								a_ph.setChecked(false);
  								a_c_ph.setChecked(false);
    								W_phone=W_phone.replace("(", "");
    								W_phone=W_phone.replace(")", "");
    								W_phone=W_phone.replace("-", "");
    								Numbercalled.setText(W_phone);
    							}
    						}
    					});
                	  
                  }
                  
                  else
                  {
                	  w_ph.setText(W_phone);   
                	 // edit_link[1].setVisibility(View.VISIBLE);
                	  w_ph.setOnClickListener(new OnClickListener() {
  						
  						public void onClick(View v) {
  							// TODO Auto-generated method stub
  							
  							if(w_ph.isChecked())
  							{
  								
  								h_ph.setChecked(false);
								c_ph.setChecked(false);
								a_ph.setChecked(false);
  								W_phone=W_phone.replace("(", "");
  								W_phone=W_phone.replace(")", "");
  								W_phone=W_phone.replace("-", "");
  								Numbercalled.setText(W_phone);
  								a_c_ph.setChecked(false);
  							}
  						}
  					});
                  }
                  if(C_phone.equals("") || C_phone.equals("(000)000-0000") || C_phone.equals("N/A"))
                  {
                	  c_ph.setText("");
                	 // edit_link[2].setVisibility(View.INVISIBLE);
                	  c_ph.setVisibility(View.INVISIBLE);
                	  edit_link[2].setText("Add");
                	  c_ph.setOnClickListener(new OnClickListener() {
    						public void onClick(View v) {
    							// TODO Auto-generated method stub
    							if(c_ph.isChecked())
    							{
    								h_ph.setChecked(false);
  								w_ph.setChecked(false);   
  								a_ph.setChecked(false);
  								a_c_ph.setChecked(false);
    								C_phone=C_phone.replace("(", "");
    								C_phone=C_phone.replace(")", "");
    								C_phone=C_phone.replace("-", "");
    								Numbercalled.setText(C_phone);
    							}
    						}
    					});
                  }
                  else
                  {
                	  c_ph.setText(C_phone);
                	  edit_link[2].setVisibility(View.VISIBLE);
                	  c_ph.setOnClickListener(new OnClickListener() {
  						
  					public void onClick(View v) {
  							// TODO Auto-generated method stub
  							if(c_ph.isChecked())
  							{
  								h_ph.setChecked(false);
								w_ph.setChecked(false);   
								a_ph.setChecked(false);
								a_c_ph.setChecked(false);
  								C_phone=C_phone.replace("(", "");
  								C_phone=C_phone.replace(")", "");
  								C_phone=C_phone.replace("-", "");
  								Numbercalled.setText(C_phone);
  							}
  						}
  					});
                  }
                  
                  
                  
                  
                  
			}
			cf.Createtablefunction(4);
			Cursor c2=cf.SelectTablefunction(cf.agentinfo, " where Homeid='" + cf.Homeid + "'");
			if(c2.getCount()>0)
			{
				

				c2.moveToFirst();
				A_phone=c2.getString(c2.getColumnIndex("offphone")).trim();System.out.println("A_phone " +A_phone);
				
				A_C_phone=c2.getString(c2.getColumnIndex("contactphone")).trim();System.out.println("A_C_phone "+A_C_phone);
				 
				 if(A_phone.equals("") || A_phone.equals("(000)000-0000") || A_phone.equals("N/A"))
			     {  
               	  a_ph.setText("");
               	  //edit_link[3].setVisibility(View.INVISIBLE);               	  
               	 edit_link[3].setText("Add");
               	a_ph.setVisibility(View.INVISIBLE);
               	 	a_ph.setOnClickListener(new OnClickListener() {
 					
 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						if(a_ph.isChecked())
 						{
 							h_ph.setChecked(false);
 							c_ph.setChecked(false);
 							w_ph.setChecked(false);
 							a_c_ph.setChecked(false);
 							A_phone=A_phone.replace("(", "");    
 							A_phone=A_phone.replace(")", "");
 							A_phone=A_phone.replace("-", "");
 							Numbercalled.setText(A_phone);
 						}
 					}
 				});
                 }
                 else
                 {
               	  a_ph.setText(A_phone);
               	  edit_link[3].setVisibility(View.VISIBLE);
               	  a_ph.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(a_ph.isChecked())
						{
							h_ph.setChecked(false);
							c_ph.setChecked(false);
							w_ph.setChecked(false);
							a_c_ph.setChecked(false);
							A_phone=A_phone.replace("(", "");
							A_phone=A_phone.replace(")", "");
							A_phone=A_phone.replace("-", "");
							Numbercalled.setText(A_phone);
						}
					}
				});
                 }
				 if(A_C_phone.equals("") || A_C_phone.equals("(000)000-0000") || A_C_phone.equals("N/A"))
                 {  
               	  a_c_ph.setText("");
               	 // edit_link[4].setVisibility(View.INVISIBLE);
               	  a_c_ph.setVisibility(View.INVISIBLE);
               	edit_link[4].setText("Add");
               		a_c_ph.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(a_c_ph.isChecked())
						{
							h_ph.setChecked(false);
							c_ph.setChecked(false);
							w_ph.setChecked(false);
							a_ph.setChecked(false);
							A_C_phone=A_C_phone.replace("(", "");
							A_C_phone=A_C_phone.replace(")", "");
							A_C_phone=A_C_phone.replace("-", "");
							Numbercalled.setText(A_C_phone);
						}
					}
				});
                 }
                 else
                 {
               	  a_c_ph.setText(A_C_phone);
               	  edit_link[4].setVisibility(View.VISIBLE);
               	  a_c_ph.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(a_c_ph.isChecked())
						{
							h_ph.setChecked(false);
							c_ph.setChecked(false);
							w_ph.setChecked(false);
							a_ph.setChecked(false);
							A_C_phone=A_C_phone.replace("(", "");
							A_C_phone=A_C_phone.replace(")", "");
							A_C_phone=A_C_phone.replace("-", "");
							Numbercalled.setText(A_C_phone);
						}
					}
				});
                 }
				
				
			}
			else
			{
				A_phone="0";
				 a_ph.setVisibility(View.INVISIBLE);
			}
		}
		catch(Exception e)
		{
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ CallattemptInfo.this +" "+" in the processing stage of retrieving data from PH table  at call attempton create  "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}
		
		
		TextView calldatetxt = (TextView) findViewById(R.id.txtcalldate);
		calldatetxt.setText(Html.fromHtml(cf.redcolor+" "+"Call Date :"));
		
		TextView calltimetxt = (TextView) findViewById(R.id.txtcalltime);
		calltimetxt.setText(Html.fromHtml(cf.redcolor+" "+"Call Time :"));
		
		TextView numtxt = (TextView) findViewById(R.id.txtnumbercalled);
		numtxt.setText(Html.fromHtml(cf.redcolor+" "+"Number Called :"));
		
		TextView restxt = (TextView) findViewById(R.id.txtresult);
		restxt.setText(Html.fromHtml(cf.redcolor+" "+"Result :"));
		
		
		TextView tittxt = (TextView) findViewById(R.id.txttitle);
		tittxt.setText(Html.fromHtml(cf.redcolor+" "+"Title :"));
		
		generalcmt = (TextView) findViewById(R.id.gencmt);
		generalcmt.setText(Html.fromHtml(cf.redcolor+" "+"General Comments :"));
		
		
		
		this.txtPersonanswered = (TextView) this.findViewById(R.id.txtpersonanswered);
		txtPersonanswered.setText(Html.fromHtml(cf.redcolor+" "+"Person Answered :"));
		
		this.Personanswered = (EditText) this.findViewById(R.id.personanswered);
		this.txtTitle = (TextView) this.findViewById(R.id.txttitle);
		this.Title = (Spinner) this.findViewById(R.id.sptitle);
		this.txtOther = (TextView) this.findViewById(R.id.txttitle11);
		this.OtherTitle = (EditText) this.findViewById(R.id.othertitle);
		this.txtInitialcomments = (TextView) this
				.findViewById(R.id.txtinitialcomment);
		this.Initialcomments = (EditText) this
				.findViewById(R.id.initialcomment);
		Initialcomments.setOnTouchListener(new Touch_Listener(1));
		Initialcomments.addTextChangedListener(new CALL_textwatcher());
		//Initialcomments.addTextChangedListener(new Callatt_textwatcher(1));
		/*Initialcomments.setText(cur1.getString(cur1
				.getColumnIndex("Ins_FirstName"))
				+ " "
				+ cur1.getString(cur1.getColumnIndex("Ins_LastName")));*/
		callatt_parent = (LinearLayout)this.findViewById(R.id.call_parrent);
		callatt_type=(LinearLayout) findViewById(R.id.callatt_type);
		callatt_txt = (TextView) findViewById(R.id.callatt_txt);
		
		
		
		this.Submitcallattempt = (Button) this.findViewById(R.id.submt);
		this.Submitandcancelinspection = (Button) this.findViewById(R.id.cancl);
		this.txtnolist = (TextView) this.findViewById(R.id.nolist);
		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		try {
			Cursor cur1 = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
			int rws1 = cur1.getCount();
			cur1.moveToFirst();
			String ownerfirstname = cf.getsinglequotes(cur1.getString(cur1
					.getColumnIndex("s_OwnersNameFirst")));
			String ownerlastname = cf.getsinglequotes(cur1.getString(cur1
					.getColumnIndex("s_OwnersNameLast")));
			String policyno = cf.getsinglequotes(cur1.getString(cur1
					.getColumnIndex("s_OwnerPolicyNumber")));
			String homephone = cf.getsinglequotes(cur1.getString(cur1
					.getColumnIndex("s_ContactHomePhone")));
			assdate = cur1.getString(cur1.getColumnIndex("MiddleName"));
			policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "
					+ ownerlastname + "<br/><font color=#bddb00>(Policy No : "
					+ policyno + ")</font>"));
			StringBuilder sVowelBuilder1 = new StringBuilder(
					homephone);
			sVowelBuilder1.deleteCharAt(0);
			sVowelBuilder1.deleteCharAt(3);
			sVowelBuilder1.deleteCharAt(6);
			homephone = sVowelBuilder1.toString();
			//Numbercalled.setText(homephone);
		} catch (Exception e) {

		}

		UnablescheduleList();
		InspectionrefusedList();
		ResultList();
		TitleList();

		final Calendar cal = Calendar.getInstance();
		mYear = cal.get(Calendar.YEAR);
		mMonth = cal.get(Calendar.MONTH);
		mDay = cal.get(Calendar.DAY_OF_MONTH);
		String source = "<font color=#FFFFFF>Loading data. Please wait..."
				+ "</font>";
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		
		
		if(cf.isInternetOn()==true)
		{
            source = "<font color=#FFFFFF>Loading data. Please wait..."
					+ "</font>";
            k=1;
			pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
			Thread thread = new Thread(this);
			thread.start();
		}
		else
		{
			k=2;
			cf.ShowToast("Internet connection is not available.",1);
		}
		/*if (info != null && info.isConnected()) {
			ichk = 0;
			String source1 = "<font color=#FFFFFF>Loading data. Please wait..."
					+ "</font>";
			pd = ProgressDialog.show(CallattemptInfo.this, "",
					Html.fromHtml(source1), true);

			CallattemptDownload();
			k = 1;
			handler.sendEmptyMessage(0);


		} else {
			ichk = 1;
		}*/

	

		this.getdate.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				final Calendar c = Calendar.getInstance();
				mYear = c.get(Calendar.YEAR);
				mMonth = c.get(Calendar.MONTH);
				mDay = c.get(Calendar.DAY_OF_MONTH);
				int day = c.get(Calendar.DAY_OF_WEEK);

				Calldate.setText(new StringBuilder().append(mMonth + 1)
						.append("/").append(mDay).append("/").append(mYear)
						.append(" "));
				if (day == 1) {
					Callday.setText(new StringBuilder().append("Sunday"));
				}
				if (day == 2) {
					Callday.setText(new StringBuilder().append("Monday"));
				}
				if (day == 3) {
					Callday.setText(new StringBuilder().append("Tuesday"));
				}
				if (day == 4) {
					Callday.setText(new StringBuilder().append("Wednesday"));
				}
				if (day == 5) {
					Callday.setText(new StringBuilder().append("Thursday"));
				}
				if (day == 6) {
					Callday.setText(new StringBuilder().append("Friday"));
				}
				if (day == 7) {
					Callday.setText(new StringBuilder().append("Saturday"));
				}
			}
		});
		this.Currenttime.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				Calendar c = Calendar.getInstance();
				int hours = c.get(Calendar.HOUR);
				int minutes = c.get(Calendar.MINUTE);
				int amorpm = c.get(Calendar.AM_PM);

				if (amorpm == 0) {
					Calltime.setText(new StringBuilder()
							// Month is 0 based so add 1
							.append(hours).append(":").append(minutes)
							.append(" ").append("AM").append(" "));
				}
				if (amorpm == 1) {
					Calltime.setText(new StringBuilder()
							// Month is 0 based so add 1
							.append(hours).append(":").append(minutes)
							.append(" ").append("PM").append(" "));
				}
			}
		});
		this.seldate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Calldate.setText("");
				Callday.setText("");
				showDialog(DATE_DIALOG_ID);

			}
		});

		this.Result
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						if (!"--Select--".equals(Result.getSelectedItem()
								.toString())) {
							resultspin = Result.getSelectedItem().toString();

							if ("Answered".equals(Result.getSelectedItem()
									.toString())
									|| "Re Order Left voice mail".equals(Result
											.getSelectedItem().toString())
									|| "Others".equals(Result.getSelectedItem()
											.toString())) {
								txtPersonanswered.setVisibility(View.VISIBLE);
								Personanswered.setVisibility(View.VISIBLE);
								txtTitle.setVisibility(View.VISIBLE);
								Title.setVisibility(View.VISIBLE);
								
							} else {
								Noanswered();
							}
							if("No Answer".equals(Result.getSelectedItem()
									.toString())){
								Initialcomments.setText("NILL");
							}
							else
							{
								if(Initialcomments.getText().toString().equals("NILL"))
								{
									Initialcomments.setText("");
								}
								else
								{
								Initialcomments.setText(Initialcomments.getText().toString());
								}
							}
						}
						else
						{
							txtPersonanswered.setVisibility(View.GONE);
							Personanswered.setVisibility(View.GONE);
						}
						if("Others".equals(Result.getSelectedItem()
								.toString())){
							resul_other.setOnTouchListener(new Touch_Listener(2));
							resul_other.setVisibility(View.VISIBLE);
							txtPersonanswered.setVisibility(View.GONE);
							Personanswered.setVisibility(View.GONE);
						}
						else
						{
							resul_other.setVisibility(View.GONE);
							resul_other.setText("");
						}
					}

					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});
		this.Title
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						if (!"--Select--".equals(Title.getSelectedItem().toString())) 
						{
							titlespin = Title.getSelectedItem().toString();
							if ("Other".equals(Title.getSelectedItem()
									.toString())) {
								OtherTitle.setOnTouchListener(new Touch_Listener(3));
								txtOther.setVisibility(View.VISIBLE);
								OtherTitle.setVisibility(View.VISIBLE);

							} else {
								txtOther.setVisibility(View.GONE);
								OtherTitle.setVisibility(View.GONE);
							}
						}
						else
						{
							txtOther.setVisibility(View.GONE);
							OtherTitle.setVisibility(View.GONE);
						}

					}

					public void onNothingSelected(AdapterView<?> arg0) {

					}
				});
		this.Submitcallattempt.setOnClickListener(new OnClickListener() {

		  public void onClick(View arg0) {
			// try
			 //{
				// cf.checkInspectorAvail = cf.IsCurrentInspector();
				//if (cf.checkInspectorAvail.equals("true"))
				//{
					try {
						
						Cursor c12 = cf.db.rawQuery("SELECT * FROM "
								+ cf.mbtblInspectionList + " WHERE s_SRID='" + cf.Homeid
								+ "' and InspectorId='" + cf.InspectorId + "'", null);
						c12.moveToFirst();
						 String isinspected = c12.getString(c12.getColumnIndex("IsInspected"));
						
						
							
							if(isinspected.equals("1"))
							{ 
								cf.ShowToast("You cannot schedule this record. This inspection has been moved to CIT status.", 1);
							}
							else
							{
						
								if (!"".equals(Calldate.getText().toString())
										&& !"".equals(Calltime.getText().toString())
										&& !"--Select--".equals(Result.getSelectedItem()
												.toString())
										&& !"".equals(Numbercalled.getText().toString())) {
									if (checkfordateistoday(Calldate.getText().toString()) == "true") {
										if (checkforassigndate(assdate, Calldate.getText()
												.toString()) == "true") {
											if (strhmephnvalid(Numbercalled.getText()
													.toString()) == "yes") {
												
												phnnum1=Numbercalled.getText().toString();
												long phonenumber;
												try
												{
												
												phonenumber = Long.parseLong(phnnum1);
												System.out.println("phone "+phonenumber);
												}
												catch(Exception e)
												{
													phonenumber=0;
														
												}
												
												if(phonenumber>=1)
												{
												if ("Answered".equals(Result.getSelectedItem()
														.toString())) {
													if (!"".equals(Calldate.getText().toString()) && !"--Select--".equals(Title.getSelectedItem().toString())
															&& !Personanswered.getText().toString().trim().equals("")) {
														if (!"Other".equals(Title.getSelectedItem()
																.toString()) || !OtherTitle.getText().toString().trim().equals("")) {
															//savephonenumbers();
															submitprocess1();
														}
														else
														{
															cf.ShowToast("Please enter other text value for title", 1);
														}
														
			
													} else {
														
														if ("--Select--".equals(Title.getSelectedItem().toString()))
														{
															cf.ShowToast("Please select Title.",1);
														}
														if(Personanswered.getText().toString().trim().equals(""))
														{
															cf.ShowToast("Please enter Person answered.",1);
														}
														
														
													}
			
												} else {
													if(("Others".equals(Result.getSelectedItem()
															.toString())) && resul_other.getText().toString().trim().equals(""))
													{
														cf.ShowToast("Please enter other text value for Result", 1);
														
													}
													else if ((!"Other".equals(Title.getSelectedItem()
															.toString()) || !OtherTitle.getText().toString().trim().equals(""))) {
														Noanswered();
														//savephonenumbers();
														submitprocess1();
													}
													else
													{
														cf.ShowToast("Please enter other text value for title", 1);
													}
													
			
												}
												}
												else
												{
													Numbercalled.setText("");
													Numbercalled.requestFocus();
													
													cf.ShowToast("Please enter the Called Number in Correct format.",1);
												}
											} else {
												Numbercalled.setText("");
												Numbercalled.requestFocus();
												cf.ShowToast("Please enter the Called Number in 10 digits.",1);
			
											}
										} else {
											cf.ShowToast("Call Date should be greater than or equal to Assign Date.",1);
			
											Calldate.setText("");
											Calldate.requestFocus();
										}
									} else {
										cf.ShowToast("Call Date should not be greater than Today's Date.",1);
										Calldate.setText("");
										Calldate.requestFocus();
									}
								} else {
									if ("".equals(Calldate.getText().toString()))
									{
										cf.ShowToast("Please enter Call Date.",1);
									}
									else if("".equals(Calltime.getText().toString()))
									{
										cf.ShowToast("Please enter Call Time.",1);
									}
									else if("--Select--".equals(Result.getSelectedItem().toString()))
									{
										cf.ShowToast("Please select Result",1);
									}
									else if("".equals(Numbercalled.getText().toString()))
									{
										cf.ShowToast("Please enter Number Called.",1);
									}
			
								}
							}
									}
									catch(Exception e)
									{
										System.out.println("e "+e.getMessage());
									}
		
		}
			
		
		});
		this.Submitandcancelinspection.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) 
			{
					//try {
						 //cf.checkInspectorAvail = cf.IsCurrentInspector();
							//if (cf.checkInspectorAvail.equals("true"))
							//{
				try {			
				Cursor c12 = cf.db.rawQuery("SELECT * FROM "
						+ cf.mbtblInspectionList + " WHERE s_SRID='" + cf.Homeid
						+ "' and InspectorId='" + cf.InspectorId + "'", null);
				c12.moveToFirst();
				 String isinspected = c12.getString(c12.getColumnIndex("IsInspected"));
				
				
					
					if(isinspected.equals("1"))
					{ 
						cf.ShowToast("You cannot schedule this record. This inspection has been moved to CIT status.", 1);
					}
					else
					{
				
				
				
				
							if (chkbx1.isChecked() && !"".equals(Initialcomments.getText().toString())) {
								
								if("--Select--".equals(Unableschedule.getSelectedItem().toString()))
								{
									
									cf.ShowToast("Please select Unable to Schedule option.",1);
								}
								else
								{
										if(!etgen.getText().toString().trim().equals(""))
										{	 
										
											if ("No Contact with Policyholder"
													.equals(Unableschedule.getSelectedItem()
															.toString())
													|| "Scheduled then Cancelled"
															.equals(Unableschedule
																	.getSelectedItem()
																	.toString())) {
												if (cnt >= 2) {
			
													validation();
												} else {
													cf.ShowToast("Please submit minimum two Call Attempts.",1);
			
												}
			
											} else {
												validation();
											}
										}
										else
										{
											cf.ShowToast("Please enter general comment.",1);
										}
								}
							} 
							else if (chkbx2.isChecked() && !"".equals(Initialcomments.getText().toString())) 
							{
								
								if("--Select--".equals(Inspectionrefused.getSelectedItem().toString()))
								{
								
									cf.ShowToast("Please select Inspection Refused option.",1);
								}
								else
								{
								
									if(!etgen.getText().toString().trim().equals(""))
									{
								
									validation();
									}
									else
									{
										cf.ShowToast("Please enter general comment.",1);
									}
								}
							} 
							
							else {
								cf.ShowToast("Please select either Unable to Schedule or Inspection Refused option.",1);

							}
					}
					}
					catch(Exception e)
					{
						System.out.println("gdetting inspections status Exception "+e.getMessage());
						
					}
						
							/*}
							else
							{
								showerror();
							}*/
					/*} catch (IOException e) {
						// TODO Auto-generated catch block
						
						showerror();
						e.printStackTrace();
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						showerror();
						e.printStackTrace();
					}*/
					}
				
				});
		OtherTitle.setOnTouchListener(new Touch_Listener(3));
	}

	protected void showerror() {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
            public void run() 
            {
            	finishedHandler.sendEmptyMessage(0);
            }
        }).start();
	}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	    	
	    	cf.ShowToast("This Inspection is no longer Assigned to you Please Cease all Further Work..", 1);
	    }
	};


	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.chk1:
				c = 1;
				chkbx1.setChecked(true);
				chkbx2.setChecked(false);
				chkbx2.setSelected(false);
				Unableschedule.setEnabled(true);
				Inspectionrefused.setEnabled(false);
				tvgen.setVisibility(visibility);
				etgen.setVisibility(visibility);
				Inspectionrefused.setSelection(0);

				break;

			case R.id.chk2:
				c = 2;
				chkbx2.setChecked(true);
				chkbx1.setSelected(false);
				chkbx1.setChecked(false);
				Unableschedule.setEnabled(false);
				Inspectionrefused.setEnabled(true);
				tvgen.setVisibility(visibility);
				etgen.setVisibility(visibility);
				Unableschedule.setSelection(0);

				break;
			}
		}
	};

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;

			Calldate.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			int no_day = mDay + Month(mMonth + 1, mYear);
			int y1 = mYear - 1900;
			int leap = (y1 - 1) / 4;
			y1 = (y1 * 365) + leap;
			no_day += y1;
			int day = (no_day) % 7;
			switch (day) {
			case 0:
				day = 1;
				break;
			case 1:
				day = 2;
				break;
			case 2:
				day = 3;
				break;
			case 3:
				day = 4;
				break;
			case 4:
				day = 5;
				break;
			case 5:
				day = 6;
				break;
			case 6:
				day = 7;
				break;
			}
			if (day == 1) {
				Callday.setText(new StringBuilder().append("Sunday"));
			}
			if (day == 2) {
				Callday.setText(new StringBuilder().append("Monday"));
			}
			if (day == 3) {
				Callday.setText(new StringBuilder().append("Tuesday"));
			}
			if (day == 4) {
				Callday.setText(new StringBuilder().append("Wednesday"));
			}
			if (day == 5) {
				Callday.setText(new StringBuilder().append("Thursday"));
			}
			if (day == 6) {
				Callday.setText(new StringBuilder().append("Friday"));
			}
			if (day == 7) {
				Callday.setText(new StringBuilder().append("Saturday"));
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}

	protected int Month(int month, int year) {
		// TODO Auto-generated method stub
		int days = 0, m = month, y = year;
		if (m > 2) {
			if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
				days += 1;
			}
		}
		for (int i = 1; i < m; i++) {
			if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10
					|| i == 12) {
				days += 31;
			} else {
				if (i == 4 || i == 6 || i == 9 || i == 11) {
					days += 30;
				} else {
					if (i == 2) {
						days += 28;
					}
				}
			}
		} // no of days in months

		return days;
	}

	private void validation() {
		if (!"".equals(Calldate.getText().toString())
				&& !"".equals(Calltime.getText().toString())
				&& !"--Select--".equals(Result.getSelectedItem().toString())
				&& !"".equals(Numbercalled.getText().toString())) {
			if (checkfordateistoday(Calldate.getText().toString()) == "true") {
				if ("Answered".equals(Result.getSelectedItem().toString())) {
					if (!"".equals(Calldate.getText().toString())
							&& !"--Select--".equals(Title.getSelectedItem()
									.toString())) {
						if (strhmephnvalid(Numbercalled.getText().toString()) == "yes") {
							phnnum = Numbercalled.getText().toString();
							long phonenumber;
							try
							{
								phonenumber = Long.parseLong(phnnum);
							}
							catch(Exception e)
							{
								phonenumber=0;
									System.out.println("E "+e.getMessage());
							}
							
							if(phonenumber>=1)
							{

							
							
							if ("Answered".equals(Result.getSelectedItem()
									.toString())) {
								if (!"".equals(Calldate.getText().toString())
										&& !"--Select--".equals(Title
												.getSelectedItem().toString()) && !Personanswered.getText().toString().trim().equals("")) {
									if (!"Other".equals(Title.getSelectedItem()
											.toString()) || !OtherTitle.getText().toString().trim().equals("")) {
									//	savephonenumbers();
										submitprocess();
									}
									else
									{
										cf.ShowToast("Please enter other text for title.", 1);
										OtherTitle.requestFocus();
									}

								} else {
									if(Personanswered.getText().toString().trim().equals(""))
									{
										cf.ShowToast("Please enter Person answered.",1);
										Personanswered.requestFocus();
									}
									if ("--Select--".equals(Title.getSelectedItem().toString()))
									{
										cf.ShowToast("Please select Title.",1);
									}
									
								}

							} else {
								if(("Others".equals(Result.getSelectedItem()
										.toString())) && resul_other.getText().toString().trim().equals(""))
								{
									cf.ShowToast("Please enter other text value for Result", 1);
									
								}
								else if ((!"Other".equals(Title.getSelectedItem()
										.toString()) || !OtherTitle.getText().toString().trim().equals(""))){
									
									Noanswered();
									//savephonenumbers();
									submitprocess();

								}
								else
								{
									cf.ShowToast("Please enter other text value for title", 1);
								}
							}
							} else {
								Numbercalled.setText("");
								Numbercalled.requestFocus();
								cf.ShowToast("Please enter the Called Number in Correct format.",1);


							}

							
						} else {
							Numbercalled.setText("");
							Numbercalled.requestFocus();
							cf.ShowToast("Please enter the Called Number in 10 digits.",1);

						}

					} else {
						/*ShowToast toast = new ShowToast(getBaseContext(),
								"Please enter * field.");*/
						if ("--Select--".equals(Title.getSelectedItem().toString()))
						{
							cf.ShowToast("Please select Title.",1);
						}
						
					}

				} else {
					if(!("Others".equals(Result.getSelectedItem()
							.toString())) || !resul_other.getText().toString().trim().equals(""))
					{

					if (strhmephnvalid(Numbercalled.getText().toString()) == "yes") {
						phnnum = Numbercalled.getText().toString();
					//	savephonenumbers();
						submitprocess();
					} 
					
					else {
						Numbercalled.setText("");
						Numbercalled.requestFocus();
						cf.ShowToast("Please enter the Called Number in 10 digits.",1);
					}
					}
					else
					{
						cf.ShowToast("Please enter other text value for Result", 1);
					}

				}

			} else {
				cf.ShowToast("Call Date should not be greater than Today's Date.",1);
			}
		} else {
			/*ShowToast toast = new ShowToast(getBaseContext(),
					"Please enter * field.");*/
			if ("".equals(Calldate.getText().toString()))
			{
				cf.ShowToast("Please enter Call Date.",1);
			}
			else if("".equals(Calltime.getText().toString()))
			{
				cf.ShowToast("Please enter Call Time.",1);
			}
			else if("--Select--".equals(Result.getSelectedItem().toString()))
			{
				cf.ShowToast("Please select Result",1);
			}
			else if("".equals(Numbercalled.getText().toString()))
			{
				cf.ShowToast("Please enter Number Called.",1);
			}
		}

	}
	class CALL_textwatcher implements TextWatcher
	{
		CALL_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			cf.showing_limit(s.toString(),callatt_parent,callatt_type,callatt_txt,"499");	
					
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		
	private String checkfordateistoday(String getinspecdate2) {
		// TODO Auto-generated method stub
		String chkdate = null;
		int i1 = getinspecdate2.indexOf("/");
		String result = getinspecdate2.substring(0, i1);
		int i2 = getinspecdate2.lastIndexOf("/");
		String result1 = getinspecdate2.substring(i1 + 1, i2);
		String result2 = getinspecdate2.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;

		if (j < thsyr || (j1 < curmnth && j <= thsyr)

		|| (j2 <= curdate && j1 <= curmnth && j <= thsyr)) {
			chkdate = "true";
		} else {
			chkdate = "false";
		}

		return chkdate;

	}

	private String checkforassigndate(String getinsurancedate,
			String getinspectiondate) {
		if (!getinsurancedate.trim().equals("")
				|| getinsurancedate.equals("N/A")
				|| getinsurancedate.equals("Not Available")
				|| getinsurancedate.equals("anytype")
				|| getinsurancedate.equals("Null")) {
			String chkdate = null;
			int i1 = getinsurancedate.indexOf("/");
			String result = getinsurancedate.substring(0, i1);
			int i2 = getinsurancedate.lastIndexOf("/");
			String result1 = getinsurancedate.substring(i1 + 1, i2);
			String result2 = getinsurancedate.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);

			int i3 = getinspectiondate.indexOf("/");
			String result3 = getinspectiondate.substring(0, i3);
			int i4 = getinspectiondate.lastIndexOf("/");
			String result4 = getinspectiondate.substring(i3 + 1, i4);
			String result5 = getinspectiondate.substring(i4 + 1);
			result5 = result5.trim();
			int k1 = Integer.parseInt(result3);
			int k2 = Integer.parseInt(result4);
			int k = Integer.parseInt(result5);

			if (j > k) {
				chkdate = "false";
			} else if (j < k) {
				chkdate = "true";
			} else if (j == k) {

				if (j1 > k1) {

					chkdate = "false";
				} else if (j1 < k1) {
					chkdate = "true";
				} else if (j1 == k1) {
					if (j2 > k2) {
						chkdate = "false";
					} else if (j2 < k2) {
						chkdate = "true";
					} else if (j2 == k2) {

						chkdate = "true";
					}

				}

			}

			return chkdate;
		} else {
			return "true";
		}
	}

	private String strhmephnvalid(String strhmephn2) {

		String chk;
		if (strhmephn2.length() == 10) {
			StringBuilder sVowelBuilder = new StringBuilder(strhmephn2);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ") ");
			sVowelBuilder.insert(9, " ");
			phnnum = sVowelBuilder.toString();

			chk = "yes";

		} else {
			chk = "no";
		}

		return chk;
	}

	private void newCallattemptDownload() {
		View v = null;
		try {
			SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME11);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID", cf.Homeid.toString());
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			androidHttpTransport.call(cf.SOAP_ACTION11, envelope);
			res = (SoapObject) envelope.getResponse();

			if (res != null) {
				cnt = res.getPropertyCount();
				if (cnt != 0) {
					show_callattempt_Value();	
				} else {
					txtnolist.setVisibility(visibility);
					txtnolist.setText("No call attempt information yet");
				}
			} else {
				pd.dismiss();
				verify = 3;
				cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	private void show_callattempt_Value() {
		// TODO Auto-generated method stub
		if(cnt>0)
		{	try
			{
			RCT_ShowValue.removeAllViews();
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("Not issue"+e.getLocalizedMessage());
			}
		System.out.println("No moer issues comes in ");
			TableRow th= (TableRow) getLayoutInflater().inflate(R.layout.call_attempt_inflat, null); System.out.println("Not issues in here ater inflat");
			TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT); System.out.println("Not issues in here ater inflat1");
		 	lp.setMargins(2, 0, 2, 2); System.out.println("Not issues in here ater inflat2");
		 	th.setPadding(10, 0, 0, 0); System.out.println("Not issues in here ater inflat3");
		 	th.setBackgroundColor(0xff6B6D6B);
		 	
		 	RCT_ShowValue.addView(th,lp);
			RCT_ShowValue.setVisibility(View.VISIBLE);
			for (int i = 0; i < cnt; i++) {
				SoapObject obj = (SoapObject) res.getProperty(i);
				System.out.println("The downloaded call attempt "+obj.toString());
				
				TextView no,RCT,PAD,YIR,RS,RSh,RSt,PD;
				 ImageView edit,delete;
				String CADate_s="",CADay_s="",CAT_s="",CAN_s="",CAR_s="",CATi_s="",CAC_s="";
				
				CADate_s=String.valueOf(obj.getProperty("CallAttemptDate"));
				CADay_s=String.valueOf(obj.getProperty("CallAttemptDay"));
				CAT_s= String.valueOf(obj.getProperty("CallAttemptTime"));
				CAN_s=String.valueOf(obj.getProperty("CallAttemptNumberCalled"));
				CAR_s=String.valueOf(obj.getProperty("CallAttemptResult"));
				CATi_s=String.valueOf(obj.getProperty("CallAttemptTitle"));
				CAC_s=String.valueOf(obj.getProperty("CallAttemptIntialsComment"));
				

				if (CADate_s.contains(anytype)) {
					CADate_s = CADate_s.replace(anytype, "NIL");
				}
				if (CADay_s.contains(anytype)) {
					CADay_s = CADay_s.replace(anytype, "NIL");
				}
				if (CAT_s.contains(anytype)) {
					CAT_s = CAT_s.replace(anytype, "NIL");
				}
				if (CAN_s.contains(anytype)) {
					CAN_s = CAN_s.replace(anytype, "NIL");
				}
				if (CAR_s.contains(anytype)) {
					CAR_s = CAR_s.replace(anytype, "NIL");
				}
				if (CATi_s.contains(anytype)) {
					CATi_s = CATi_s.replace(anytype, "NIL");
				}
				if (CAC_s.contains(anytype)) {
					CAC_s = CAC_s.replace(anytype, "NIL");
				}
				
				
				if (CADate_s.contains("null")) {
					CADate_s = CADate_s.replace("null", "");
				}
				if (CADay_s.contains("null")) {
					CADay_s = CADay_s.replace("null", "");
				}
				if (CAT_s.contains("null")) {
					CAT_s = CAT_s.replace("null", "");
				}
				if (CAN_s.contains("null")) {
					CAN_s = CAN_s.replace("null", "");
				}
				if (CAR_s.contains("null")) {
					CAR_s = CAR_s.replace("null", "");
				}
				if (CATi_s.contains("null")) {
					CATi_s = CATi_s.replace("null", "");
				}
				if (CAC_s.contains("null")) {
					CAC_s = CAC_s.replace("null", "");
				}
				
		
				
				
				TableRow t= (TableRow) getLayoutInflater().inflate(R.layout.call_attempt_inflat, null);
			 	t.setId(44444+i);/// Set some id for further use
			 	
			 	
			 	no= (TextView) t.findViewWithTag("RC_RCT_No_1");
			 	no.setText(String.valueOf(i+1));no.setTextColor(0xff000000);
			 	RCT= (TextView) t.findViewWithTag("RC_RCT_RCT_1");
			 	RCT.setText(CADate_s);RCT.setTextColor(0xff000000);
			 				 	
			 	PAD= (TextView) t.findViewWithTag("RC_RCT_PAD_1");
			 	PAD.setTextColor(0xff000000);
			 	if (CADay_s.equals("1")) {
			 		CADay_s = "Monday";
				} else if (CADay_s.equals("2")) {
					CADay_s = "Tuesday";
				} else if (CADay_s.equals("3")) {
					CADay_s = "Wednesday";
				} else if (CADay_s.equals("4")) {
					CADay_s = "Thursday";
				} else if (CADay_s.equals("5")) {
					CADay_s = "Friday";
				} else if (CADay_s.equals("6")) {
					CADay_s = "Saturday";
				} else if (CADay_s.equals("7")) {
					CADay_s = "Sunday";
				}
			
			 	PAD.setText(CADay_s);
			 	
			 	YIR= (TextView) t.findViewWithTag("RC_RCT_YIR_1");
			 	YIR.setText(CAT_s);YIR.setTextColor(0xff000000);
			 	
			 	RS= (TextView) t.findViewWithTag("RC_RCT_RS_1");
			 	RS.setText(CAN_s);RS.setTextColor(0xff000000);
			 	
			 	RSh= (TextView) t.findViewWithTag("RC_RCT_RSh_1");
			 	RSh.setText(CAR_s);RSh.setTextColor(0xff000000);
			 	
			 	RSt= (TextView) t.findViewWithTag("RC_RCT_RSt_1");
			 	RSt.setText(CATi_s);RSt.setTextColor(0xff000000);
			 	
			 	PD= (TextView) t.findViewWithTag("RC_RCT_PD_1");
			 	PD.setText(CAC_s);PD.setTextColor(0xff000000);
			 	
			 	
			 	
			 	t.setPadding(10, 0, 0, 0);
			 	RCT_ShowValue.addView(t,lp);
				
			}
			
			
		}else
		{
			
			RCT_ShowValue.setVisibility(View.GONE);
			
			
		}
		
	 	
	}
	private void CallattemptDownload() {
		View v = null;
		try {
			SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME11);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;System.out.println("vca"+cf.Homeid);
			request.addProperty("SRID", cf.Homeid.toString());System.out.println("vca d");
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			try {
				androidHttpTransport.call(cf.SOAP_ACTION11, envelope);
			} catch (Exception e) {
				throw e;
			}
			res = (SoapObject) envelope.getResponse();
			System.out.println("vca d res "+res);
			cnt = res.getPropertyCount();
			System.out.println("cnt d res "+cnt);
		} catch (SocketTimeoutException s) {
			cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",1);

		} catch (NetworkErrorException n) {
			cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",1);
		} catch (IOException io) {
			cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",1);
		} catch (XmlPullParserException x) {
			cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",1);
		} catch (Exception e) {
			cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",1);
		}
	}
/*
	public static class EfficientAdapter extends BaseAdapter {
		private static final String TAG = null;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;

		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int retlen = 0;
			retlen = arrdat.length;

			return retlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.scorelistview, null);
				holder.tv = (TextView) convertView
						.findViewById(R.id.grid_item_text);
				holder.tv1 = (TextView) convertView
						.findViewById(R.id.grid_item_text1);
				holder.tv2 = (TextView) convertView
						.findViewById(R.id.grid_item_text2);
				holder.tv3 = (TextView) convertView
						.findViewById(R.id.grid_item_text3);
				holder.tv4 = (TextView) convertView
						.findViewById(R.id.grid_item_text4);
				holder.tv5 = (TextView) convertView
						.findViewById(R.id.grid_item_text5);
				holder.tv6 = (TextView) convertView
						.findViewById(R.id.grid_item_text6);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if ((arrdat[position]).contains("null")
						|| arrday[position].contains("null")
						|| arrtime[position].contains("null")
						|| arrnum[position].contains("null")
						|| arrres[position].contains("null")
						|| arrcom[position].contains("null")
						|| arrtit[position].contains("null")) {
					arrdat[position] = arrdat[position].replace("null", "");
					arrday[position] = arrday[position].replace("null", "");
					arrtime[position] = arrtime[position].replace("null", "");
					arrnum[position] = arrnum[position].replace("null", "");
					arrres[position] = arrres[position].replace("null", "");
					arrcom[position] = arrcom[position].replace("null", "");
					arrtit[position] = arrtit[position].replace("null", "");

				}
				if (arrday[position].equals("1")) {
					arrday[position] = "Monday";
				} else if (arrday[position].equals("2")) {
					arrday[position] = "Tuesday";
				} else if (arrday[position].equals("3")) {
					arrday[position] = "Wednesday";
				} else if (arrday[position].equals("4")) {
					arrday[position] = "Thursday";
				} else if (arrday[position].equals("5")) {
					arrday[position] = "Friday";
				} else if (arrday[position].equals("6")) {
					arrday[position] = "Saturday";
				} else if (arrday[position].equals("7")) {
					arrday[position] = "Sunday";
				}
				if (arrres[position].equals("Answered")) {
					arrtit[position] = arrtit[position];
				} else {
					arrtit[position] = "NILL";
				}
				holder.tv.setText(arrdat[position]);
				holder.tv1.setText(arrday[position]);
				holder.tv2.setText(arrtime[position]);
				holder.tv3.setText(arrnum[position]);
				holder.tv4.setText(arrres[position]);
				holder.tv5.setText(arrtit[position]);
				holder.tv6.setText(arrcom[position]);

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			return convertView;
		}

		static class ViewHolder {
			TextView tv, tv1, tv2, tv3, tv4, tv5, tv6;

		}

	}
*/
	protected void submitprocess() {
		/*cf.getInspecionTypeid(cf.Homeid);
		if(cf.checklicenceexpiry(cf.LicenceExpiryDate).equals("true"))
		{
			System.out.println("licene 222is true");
			cf.ShowToast("Your licence date has been expired.",1);
		}
		else
		{*/
				if (ichk == 0) {
					pd = ProgressDialog.show(CallattemptInfo.this, "",
							"Submitting Call Attempt...");
		
					new Thread() {
						private boolean toastst = true;
		
						public void run() {
							Looper.prepare();
							try {
								
								
								cf.checkInspectorAvail = cf.IsCurrentInspector();System.out.println("Inspe Avail "+cf.checkInspectorAvail);
								if (cf.checkInspectorAvail.equals("true"))
								{
									System.out.println("Inspe true ");
										if (CancelStatusCheck() == "true") {System.out.println("sd true ");
											verify = 2;		
											handler.sendEmptyMessage(0);
											pd.dismiss();
										} else {System.out.println("Inspe df ");
											handler.sendEmptyMessage(0);
											pd.dismiss();
										}
								}
								else
								{System.out.println("else true ");
									System.out.println("current inspector not available "+cf.checkInspectorAvail);
									verify = 5;
									
									handler.sendEmptyMessage(0);
									pd.dismiss();
							
								}
							} catch (Exception e) {
		
							}
						}
		
						private String CancelStatusCheck() {
		
							String chk;
							try {
		
								SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
										SoapEnvelope.VER11);
								envelope.dotNet = true;
								SoapObject ad_property = new SoapObject(cf.NAMESPACE,
										cf.METHOD_NAME12);
		
								ad_property.addProperty("InspectorID", cf.InspectorId);
								ad_property.addProperty("Srid", cf.Homeid);
								ad_property.addProperty("CallDate", Calldate.getText()
										.toString());
								ad_property.addProperty("CallTime", Calltime.getText()
										.toString());
								if (phnnum.length() == 10) {
									StringBuilder sVowelBuilder = new StringBuilder(
											phnnum);
									sVowelBuilder.insert(3, " ");
									sVowelBuilder.insert(7, " ");
									phnnum = sVowelBuilder.toString();
		
								}
								ad_property.addProperty("NumberCalled", phnnum);
								int start = Result.getSelectedItemPosition();
								ad_property.addProperty("CallAttemptResultId", start);
								if (start == 1 || start == 7) {
									
									ad_property.addProperty("PersonAnswered",
											Personanswered.getText().toString());
									int end = Title.getSelectedItemPosition();
									//ad_property.addProperty("CallAttemptTitleId", end);
									if (end == 7) {
										ad_property.addProperty("PersonAnswered", "");
										ad_property.addProperty("CallAttemptTitleId", end);
										ad_property.addProperty("OtherTitle",
												OtherTitle.getText().toString());
									} else if(start == 7) {
										try
										{
											ad_property.addProperty("PersonAnswered", "");
											ad_property.addProperty("CallAttemptTitleId", 7);
											ad_property.addProperty("OtherTitle", resul_other.getText().toString());
										}
										catch (Exception e)
										{
											ad_property.addProperty("OtherTitle", "");	
											cf.Error_LogFile_Creation("error tracking at the callattempt when i send the result as oters text value ");
										}
									}
									else
									{
										ad_property.addProperty("PersonAnswered", "");
										ad_property.addProperty("CallAttemptTitleId", 8);
										ad_property.addProperty("OtherTitle", "");
									}
									
									
								} else {
									ad_property.addProperty("PersonAnswered", "");
									ad_property.addProperty("CallAttemptTitleId", 8);
									ad_property.addProperty("OtherTitle", "");
								}
		
								ad_property.addProperty("Comments", Initialcomments
										.getText().toString());
								ad_property.addProperty("CreatedDate", cd.toString());
								ad_property.addProperty("ModifiedDate", md.toString());
								if (c == 0) {
									ad_property.addProperty("UnableToSchedule", false);
									ad_property.addProperty("InspectionRefused", false);
									ad_property.addProperty("ddlReason", "");
									ad_property.addProperty("ddlRefusedReason", "");
									ad_property.addProperty("GeneralComment", "");
		
								} else if (c == 1) {
									ad_property.addProperty("UnableToSchedule", true);
									ad_property.addProperty("ddlReason", Unableschedule
											.getSelectedItem().toString());
									ad_property.addProperty("GeneralComment", etgen
											.getText().toString());
									ad_property.addProperty("InspectionRefused", false);
									ad_property.addProperty("ddlRefusedReason", "");
		
								} else if (c == 2) {
									ad_property.addProperty("UnableToSchedule", false);
									ad_property.addProperty("ddlReason", "");
									ad_property.addProperty("GeneralComment", etgen
											.getText().toString());
									ad_property.addProperty("InspectionRefused", true);
									ad_property.addProperty("ddlRefusedReason",
											Inspectionrefused.getSelectedItem()
													.toString());
								}
								envelope.setOutputSoapObject(ad_property);
								HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
										cf.URL);
								try {
									androidHttpTransport1.call(cf.SOAP_ACTION12,
											envelope);
								} catch (Exception e) {
									throw e;
								}
								SoapObject result = (SoapObject) envelope.bodyIn;
								String result1 = String.valueOf(envelope.getResponse());
								
								if (result1
										.contains("Inspection Status updated successfully")) {
									if (c == 1) { // Unable to schedule
										cf.db.execSQL("UPDATE "
												+ cf.mbtblInspectionList
												+ " SET  Status='110' where s_SRID='"
												+ cf.Homeid + "'");
									} else if (c == 2) { // Cancelled
										cf.db.execSQL("UPDATE "
												+ cf.mbtblInspectionList
												+ " SET  Status='110' where s_SRID='"
												+ cf.Homeid + "'");
									}
		
									Toast toast = Toast.makeText(
											getApplicationContext(),
											Html.fromHtml(result1.toString()),
											Toast.LENGTH_LONG);
									TextView v = (TextView) toast.getView()
											.findViewById(android.R.id.message);
									v.setTextColor(Color.YELLOW);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
									Intent Star = new Intent(CallattemptInfo.this,
											Startinspections.class);
									Star.putExtra("keyName", 1);
									startActivity(Star);
								}
		
								else {
									Toast toast = Toast.makeText(
											getApplicationContext(),
											Html.fromHtml(result1.toString()),
											Toast.LENGTH_LONG);
									TextView v = (TextView) toast.getView()
											.findViewById(android.R.id.message);
									v.setTextColor(Color.YELLOW);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
									toastst = false;
								}
								chk = "true";
							} catch (SocketTimeoutException s) {
								verify = 1;
								chk = "false";
		
							} catch (NetworkErrorException n) {
		
								verify = 1;
								chk = "false";
							} catch (IOException io) {
								verify = 1;
								chk = "false";
							} catch (XmlPullParserException x) {
								verify = 1;
								chk = "false";
							} catch (Exception e) {
								chk = "false";
		
							}
		
							return chk;
						}
		
						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
		
								if (verify == 2 && toastst) {
									cf.ShowToast("Call Attempt has been submitted sucessfully.",1);
									Calldate.setText("");
									Callday.setText("");
									Calltime.setText("");
									Numbercalled.setText("");
									Personanswered.setText("");
									etgen.setText("");
									Inspectionrefused.setSelection(0);
									Unableschedule.setSelection(0);
									Unableschedule.setEnabled(false);
									Inspectionrefused.setEnabled(false);
									chkbx1.setChecked(false);
									chkbx2.setChecked(false);
									Result.setSelection(0);
									Title.setSelection(0);
									etgen.setVisibility(View.GONE);
									generalcmt.setVisibility(View.GONE);
									h_ph.setChecked(false);
									w_ph.setChecked(false);
									c_ph.setChecked(false);
									a_ph.setChecked(false);
									Noanswered();
									newCallattemptDownload();
								} else if (verify == 1) {
									cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",1);
		
								}
								else if (verify == 5) {
									cf.ShowToast("This Inspection is no longer Assigned to you Please Cease all Further Work.",1);
		
								} 
								else {
									if (toastst) {
										cf.ShowToast("Call Attempt has not been submitted. Please try again later.",1);
										verify = 0;
										newCallattemptDownload();
									}
		
								}
		
							}
						};
					}.start();
				} else {
					cf.ShowToast("Internet connection is not available.",1);
		
				}
		//}
	}

	protected void submitprocess1() {
		/*cf.getInspecionTypeid(cf.Homeid);
		if(cf.checklicenceexpiry(cf.LicenceExpiryDate).equals("true"))
		{
			System.out.println("licene is true");
			cf.ShowToast("Your licence date has been expired.",1);
		}
		else
		{*/
			if (ichk == 0) {
				pd = ProgressDialog.show(CallattemptInfo.this, "",
						"Submitting Call Attempt...");
	
				new Thread() {
					public void run() {
						Looper.prepare();
						try {
							cf.checkInspectorAvail = cf.IsCurrentInspector();
							if (cf.checkInspectorAvail.equals("true"))
							{
	
								if (CancelStatusCheck1() == "true") {
									verify = 2;
									handler1.sendEmptyMessage(0);
									pd.dismiss();
								} else {
									pd.dismiss();
									handler1.sendEmptyMessage(0);
		
								}
							}
							else
							{
								verify = 5;
								handler1.sendEmptyMessage(0);
								pd.dismiss();							
							}
						} catch (Exception e) {
	
						}
					}
	
					private String CancelStatusCheck1() throws SoapFault {
	
						String chk,resultres;
						
						
						
						
						try {
	
							SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope.dotNet = true;
							SoapObject ad_property = new SoapObject(cf.NAMESPACE,
									cf.METHOD_NAME13);
	
							ad_property.addProperty("InspectorID", cf.InspectorId);
							ad_property.addProperty("Srid", cf.Homeid);
							ad_property.addProperty("CallDate", Calldate.getText()
									.toString());
							ad_property.addProperty("CallTime", Calltime.getText()
									.toString());
							if (phnnum.length() == 10) {
								StringBuilder sVowelBuilder = new StringBuilder(
										phnnum);
								sVowelBuilder.insert(3, " ");
								sVowelBuilder.insert(7, " ");
								phnnum = sVowelBuilder.toString();
	
							}
							ad_property.addProperty("NumberCalled", phnnum);
							int start = Result.getSelectedItemPosition();
							ad_property.addProperty("CallAttemptResultId", start);
							if (start == 1 || start == 6) {
								ad_property.addProperty("PersonAnswered",
										Personanswered.getText().toString());
								int end = Title.getSelectedItemPosition();
								ad_property.addProperty("CallAttemptTitleId", end);
								if (end == 7) {
									ad_property.addProperty("OtherTitle",
											OtherTitle.getText().toString());
								} else {
									ad_property.addProperty("OtherTitle", "");
								}
	
							}
							else if(start == 7) {
								try
								{
								ad_property.addProperty("PersonAnswered", "");
								ad_property.addProperty("CallAttemptTitleId", 7);
								ad_property.addProperty("OtherTitle", resul_other.getText().toString());
								}
								catch (Exception e)
								{
									ad_property.addProperty("OtherTitle", "");	
									cf.Error_LogFile_Creation("error tracking at the callattempt when i send the result as oters text value ");
								}
							} 
							else {
								ad_property.addProperty("PersonAnswered", "");
								ad_property.addProperty("CallAttemptTitleId", 8);
								ad_property.addProperty("OtherTitle", "");
							}
	
							ad_property.addProperty("Comments", Initialcomments
									.getText().toString());
							ad_property.addProperty("CreatedDate", cd.toString());
							ad_property.addProperty("ModifiedDate", md.toString());
							System.out.println("ad_property res "+ad_property);
							envelope.setOutputSoapObject(ad_property);
							HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
									cf.URL);
							try {
								androidHttpTransport1.call(cf.SOAP_ACTION13,
										envelope);
								SoapObject result = (SoapObject) envelope.bodyIn;
								String result1 = String.valueOf(envelope
										.getResponse());
								System.out.println("envelope res "+result1);
								chk = "true";
							} catch (Exception e) {
								throw e;
							}
	
						}
	
						catch (SocketTimeoutException s) {
							verify = 1;
							chk = "false";
	
						} catch (NetworkErrorException n) {
	
							verify = 1;
							chk = "false";
						} catch (IOException io) {
							verify = 1;
							chk = "false";
						} catch (XmlPullParserException x) {
							verify = 1;
							chk = "false";
						} catch (Exception e) {
							chk = "false";
	
						}
	
						return chk;
						
					}
	
					private Handler handler1 = new Handler() {
						@Override
						public void handleMessage(Message msg) {
	
							if (verify == 2) {
								cf.ShowToast("Call Attempt has been submitted sucessfully.",1);
								Calldate.setText("");
								Callday.setText("");
								Calltime.setText("");
								Numbercalled.setText("");
								Personanswered.setText("");
								etgen.setText("");
								Inspectionrefused.setSelection(0);
								Unableschedule.setSelection(0);
								Unableschedule.setEnabled(false);
								Inspectionrefused.setEnabled(false);
								chkbx1.setChecked(false);
								chkbx2.setChecked(false);
								Result.setSelection(0);
								Title.setSelection(0);
								OtherTitle.setText("");
								etgen.setVisibility(View.GONE);
								generalcmt.setVisibility(View.GONE);
								h_ph.setChecked(false);
								w_ph.setChecked(false);
								c_ph.setChecked(false);
								a_ph.setChecked(false);
								Noanswered();
								newCallattemptDownload();
							}else if(verify == 5)
							{
									cf.ShowToast("This Inspection is no longer Assigned to you Please Cease all Further Work.",1);
							}
							 else {
								cf.ShowToast("Call Attempt has not been submitted. Please try again later.",1);
								verify = 0;
	
							}
	
						}
					};
				}.start();
			} else {
				cf.ShowToast("Internet connection is not available.",1);
	
			}
		//}
	}

	private void UnablescheduleList() {
		Unableschedule = (Spinner) findViewById(R.id.spunableschedule);
		loUnableschedule = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loUnableschedule
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Unableschedule.setAdapter(loUnableschedule);
		loUnableschedule.add("--Select--");
		loUnableschedule.add("Bad Contact Information");
		loUnableschedule.add("No Contact with Policyholder");
		loUnableschedule.add("Scheduled then Cancelled");
		loUnableschedule.add("Seasonal Resident");
		loUnableschedule.add("Policyholder on Military Deployment");
		loUnableschedule.add("Not Insured by Carrier Anymore");
	}

	private void InspectionrefusedList() {
		Inspectionrefused = (Spinner) findViewById(R.id.spinspectionrefused);
		loInspectionrefused = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loInspectionrefused
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Inspectionrefused.setAdapter(loInspectionrefused);
		loInspectionrefused.add("--Select--");
		loInspectionrefused.add("Out of Coverage Area");
		loInspectionrefused.add("Unable to meet Cycle Times");
		loInspectionrefused.add("Conflict � Did Original Inspection");

	}

	private void ResultList() {
		Result = (Spinner) findViewById(R.id.spresult);
		loResult = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loResult.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Result.setAdapter(loResult);
		loResult.add("--Select--");
		loResult.add("Answered");
		loResult.add("No Answer");
		loResult.add("Left Voicemail");
		loResult.add("Number Disconnected");
		loResult.add("Answered & Disconnected");
		loResult.add("Re Order Left voice mail");
		loResult.add("Others");

	}

	private void TitleList() {
		Title = (Spinner) findViewById(R.id.sptitle);
		loTitle = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loTitle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Title.setAdapter(loTitle);
		loTitle.add("--Select--");
		loTitle.add("Policyholder");
		loTitle.add("Agent");
		loTitle.add("Agency CSR");
		loTitle.add("Property Manager");
		loTitle.add("Family Member");
		loTitle.add("Realtor");
		loTitle.add("Other");
	}

	private void Noanswered() {
		txtPersonanswered.setVisibility(View.GONE);
		Personanswered.setVisibility(View.GONE);
		Personanswered.setText("");
		txtTitle.setVisibility(View.GONE);
		Title.setVisibility(View.GONE);
		Title.setSelection(0);
		OtherTitle.setText("");
		txtOther.setVisibility(View.GONE);
		OtherTitle.setVisibility(View.GONE);

	}
	private Dialog showalert() {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(CallattemptInfo.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout maintable= (LinearLayout) dialog1.findViewById(R.id.maintable);	
		LinearLayout currenttable= (LinearLayout) dialog1.findViewById(R.id.Edit_number);
		maintable.setVisibility(View.GONE);
		currenttable.setVisibility(View.VISIBLE);
		 Button bt_cancel=(Button) dialog1.findViewById(R.id.EN_cancel);
		  Button bt_close=(Button) dialog1.findViewById(R.id.EN_close);	
		   bt_close.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				cf.ShowToast("Phone number not updated", 1);
			}
		});
		  bt_cancel.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					cf.ShowToast("Phone number not updated", 1);
				}
			});  
		return dialog1;
	}
	public void clicker(View v) {
		final Dialog dialog1;
		TextView txt_v;
		Button bt_save;
		final EditText ed_number;
		switch (v.getId()) {
	
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.c_edit_H_ph:
			
			  dialog1=showalert();
			   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
			    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
			   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
			  txt_v.setText("Please Enter the Home phone number ");
			  H_phone=H_phone.replace("(", "");
				H_phone=H_phone.replace(")", "");
				H_phone=H_phone.replace("-", "");
				System.out.println("H_h"+H_phone);
				if(H_phone.equals("N/A"))
				{
					ed_number.setText("");
				}
				else
				{
					ed_number.setText(H_phone);
				}
			  bt_save.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						if(!ed_number.getText().toString().trim().equals("") )
						{
							if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
							{
								
								cf.db.execSQL("update "+cf.mbtblInspectionList+" set s_ContactHomePhone='"+cf.convertsinglequotes(cf.newphone)+"' where s_SRID='"+cf.Homeid+"'");
								h_ph.setText(cf.newphone);
								cf.newphone=cf.newphone.replace("(", "");
								cf.newphone=cf.newphone.replace(")", "");
								cf.newphone=cf.newphone.replace("-", "");
								H_phone=cf.newphone;
								dialog1.setCancelable(true);
								dialog1.dismiss();
								h_ph.setVisibility(View.VISIBLE);
								cf.ShowToast("Phone number Updated Successfully  ", 1);
								 if(edit_link[0].getText().toString().equals("Add"))
								 {
									 edit_link[0].setText("edit");
								 }
								 
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
							
						}
						else
						{
							cf.ShowToast("Please enter valid phone number  ", 1);
						}
					}
					catch(Exception e)
					{
						cf.ShowToast("Phone number not updated  ", 1);
						dialog1.setCancelable(true);
						dialog1.dismiss();
						System.out.println("Error in the query"+e.getMessage());
						cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
					}
				}
			});
			  dialog1.setCancelable(false);
			  dialog1.show();
			break;
			case R.id.c_edit_W_ph:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				  txt_v.setText("Please Enter the Work phone number ");
				  	W_phone=W_phone.replace("(", "");
					W_phone=W_phone.replace(")", "");
					W_phone=W_phone.replace("-", "");
					System.out.println("W_phone"+W_phone);
					if(W_phone.equals("N/A"))
					{
						ed_number.setText("");
					}
					else
					{					
						ed_number.setText(W_phone);
					}
				  bt_save.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.db.execSQL("update "+cf.mbtblInspectionList+" set s_ContactWorkPhone='"+cf.convertsinglequotes(cf.newphone)+"' where s_SRID='"+cf.Homeid+"'");
									System.out.println("contac phoe "+"update "+cf.mbtblInspectionList+" set s_ContactWorkPhone='"+cf.convertsinglequotes(cf.newphone)+"' where s_SRID='"+cf.Homeid+"'");
									w_ph.setText(cf.newphone);
									cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");
									W_phone=cf.newphone;
									dialog1.setCancelable(true);
									dialog1.dismiss();
									w_ph.setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number Updated Successfully  ", 1);
									if(edit_link[1].getText().toString().equals("Add"))
									 {
										 edit_link[1].setText("edit");
									 }
									
								}
								else
								{
									cf.ShowToast("Please enter valid phone number  ", 1);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated  ", 1);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.c_edit_C_ph:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				  txt_v.setText("Please Enter the Cell phone number ");
				  	C_phone=C_phone.replace("(", "");
					C_phone=C_phone.replace(")", "");
					C_phone=C_phone.replace("-", "");
					System.out.println("C_phone"+C_phone);
					if(C_phone.equals("N/A"))
					{
						ed_number.setText("");
					}
					else{
						ed_number.setText(C_phone);
					}
				  bt_save.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{	
									cf.db.execSQL("update "+cf.mbtblInspectionList+" set s_ContactCellPhone='"+cf.convertsinglequotes(cf.newphone)+"' where s_SRID='"+cf.Homeid+"'");
									System.out.println("s_ContactCellPhone ph"+"update "+cf.mbtblInspectionList+" set s_ContactCellPhone='"+cf.convertsinglequotes(cf.newphone)+"' where s_SRID='"+cf.Homeid+"'");
									c_ph.setText(cf.newphone);
									cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");
									C_phone=cf.newphone;
									dialog1.setCancelable(true);
									dialog1.dismiss();
									c_ph.setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number Updated Successfully  ", 1);
									if(edit_link[2].getText().toString().equals("Add"))
									 {
										 edit_link[2].setText("edit");
									 }
									
								}
								else
								{
									cf.ShowToast("Please enter valid phone number  ", 1);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated  ", 1);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.c_edit_A_C_ph:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				  txt_v.setText("Please Enter the Agent contact phone number ");
				  A_C_phone=A_C_phone.replace("(", "");
					A_C_phone=A_C_phone.replace(")", "");
					A_C_phone=A_C_phone.replace("-", "");
					System.out.println("A_C_phone"+A_C_phone);
					if(A_C_phone.equals("N/A"))
					{
						ed_number.setText("");
					}
					else
					{
						ed_number.setText(A_C_phone);
					}
				  bt_save.setOnClickListener(new OnClickListener() {
					
				public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									cf.db.execSQL("update "+cf.agentinfo+" set contactphone='"+cf.newphone+"' where Homeid ='"+cf.Homeid+"'");
									System.out.println("contactphone ph"+"update "+cf.agentinfo+" set contactphone='"+cf.newphone+"' where Homeid ='"+cf.Homeid+"'");
									a_c_ph.setText(cf.newphone);
									cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");
									A_C_phone=cf.newphone;
									dialog1.setCancelable(true);
									dialog1.dismiss();
									a_c_ph.setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number Updated Successfully  ", 1);
									if(edit_link[3].getText().toString().equals("Add"))
									 {
										 edit_link[3].setText("edit");
									 }
									
								}
								else
								{
									cf.ShowToast("Please enter valid phone number  ", 1);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated  ", 1);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.c_edit_A_ph:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				   	A_phone=A_phone.replace("(", "");
					A_phone=A_phone.replace(")", "");
					A_phone=A_phone.replace("-", "");
					System.out.println("A_phone"+A_phone);
					if(A_phone.equals("N/A"))
					{
						ed_number.setText("");
					}
					else
					{
						ed_number.setText(A_phone);
					}
				  txt_v.setText("Please Enter the Agent office phone number ");
				  bt_save.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.db.execSQL("update "+cf.agentinfo+" set offphone='"+cf.newphone+"' where Homeid ='"+cf.Homeid+"'");
									System.out.println("agentinfo ph"+"update "+cf.agentinfo+" set offphone='"+cf.newphone+"' where Homeid ='"+cf.Homeid+"'");
									a_ph.setText(cf.newphone);
									cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");
									A_phone=cf.newphone;
									dialog1.setCancelable(true);
									dialog1.dismiss();
									a_ph.setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number Updated Successfully  ", 1);
									if(edit_link[4].getText().toString().equals("Add"))
									 {
										 edit_link[4].setText("edit");
									 }
									
								}
								else
								{
									cf.ShowToast("Please enter valid phone number  ", 1);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number  ", 1);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated  ", 1);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
		}
	}
	class Touch_Listener implements OnTouchListener
	{
		   public int type;
		   Touch_Listener(int type)
			{
				this.type=type;
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	if(this.type==1)
				{
		    		cf.setFocus(Initialcomments);
				}
		    	else if(this.type==2)
		    	{
		    		cf.setFocus(resul_other);
		    	}
		    	else if(this.type==3)
		    	{
		    		cf.setFocus(OtherTitle);
		    	}
				return false;
			}
		   
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(CallattemptInfo.this, AgentInfo.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
			if (k == 1) {
				View v = null;
				if (cnt != 0) {
					show_callattempt_Value();

				} else {
					txtnolist.setVisibility(visibility);
					txtnolist.setText("No call attempt information.");
				}

			} else if (k == 2) {
				cf.ShowToast("Internet connection is not available.",1);
			}

		}
	};
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
					
				}
				
				break;

		}

	}

	public void run(){
		// TODO Auto-generated method stub
		try
		{
		CallattemptDownload();k=1;
		}catch (Exception ex) 
		{
			System.out.println("issues happent ex"+ex.getMessage());
		}
		//cf.pd.dismiss();
		handler.sendEmptyMessage(0);
	}
	
}