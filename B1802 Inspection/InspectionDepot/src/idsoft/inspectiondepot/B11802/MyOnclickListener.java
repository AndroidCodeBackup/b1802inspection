package idsoft.inspectiondepot.B11802;



import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyOnclickListener extends LinearLayout {
	private static final int visibility = 0;
	private Context context = null;
	ImageView takephoto,img1, img2, img3, img4, img6, img7, img8, img9, 
			buildtick, roofcovertick, roofdecktick, roofwalltick, roofgeotick,
			swrtick, opentick, walltick;
	Button b;View v1,poicyholdimg,agntimg, callimg, schimg, addimg,addiservice,buildcode, roofcover, roofdeck,
	roofwall, roofgeometry, swr, openprotection, wallconstruction;
	Context activityname;
	TextView policyholderinfo,txtviewtakephoto;
	public CommonFunctions cf;
	int app, sub;
	RelativeLayout phrel, qrel, imgrel;
	View V;private static final int SELECT_PICTURE = 0;
	public static int count =0;

	public MyOnclickListener(Context context, int appname, CommonFunctions cf,
			int sub) {
		super(context);
		this.context = context;
		this.app = appname;
		this.cf = cf;
		this.sub = sub;
		create();
	}

	public MyOnclickListener(Context context, AttributeSet attrs, int appname,
			CommonFunctions cf, int sub) {
		super(context, attrs);
		this.context = context;
		this.app = appname;
		this.cf = cf;
		this.sub = sub;
		create();
	}

	private void create() {
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		/** submenu **/
		if (this.sub == 1) {
			/** PH SUB MENU **/
			if (this.app == 11 || this.app == 12 || this.app == 13
					|| this.app == 14 || this.app == 15 || this.app == 16) {
				
				layoutInflater.inflate(R.layout.phsubmenu, this, true);
				phrel = (RelativeLayout) findViewById(R.id.relativeLayout4);
				poicyholdimg = (Button) findViewById(R.id.policyhold);
				agntimg = (Button) findViewById(R.id.agnt);
				callimg = (Button) findViewById(R.id.call);
				schimg = (Button) findViewById(R.id.sch);
				addimg = (Button) findViewById(R.id.cloudinfo);				
				addiservice = (Button) findViewById(R.id.additional);
				

				poicyholdimg.setOnClickListener(new clicker());
				agntimg.setOnClickListener(new clicker());
				callimg.setOnClickListener(new clicker());
				schimg.setOnClickListener(new clicker());
				addimg.setOnClickListener(new clicker());
				addiservice.setOnClickListener(new clicker());
				try {
					Cursor cur = cf.db.rawQuery("SELECT * FROM "+ cf.additionalservice + " where SRID='" + cf.Homeid + "' " +
							"and InspectorId='"+ cf.InspectorId + "'", null);
					
					
					if(cur.getCount()==0)
					{
						addiservice.setVisibility(v1.GONE);
					}
					else
					{
						cur.moveToFirst();
						if(cur.getString(cur.getColumnIndex("IsRecord")).equals("false"))
						{
							addiservice.setVisibility(v1.GONE);
						}
						else
						{
							addiservice.setVisibility(v1.VISIBLE);
						}
					}
					
					
				} catch (Exception e) {
					System.out.println("catch ");
				}
				
				
				

			}
			/** QUESTIONS SUB MENU **/
			else if (this.app == 21 || this.app == 22 || this.app == 23
					|| this.app == 24 || this.app == 25 || this.app == 26
					|| this.app == 27 || this.app == 28) {
				System.out.println("2inside");
				layoutInflater.inflate(R.layout.quessubmenu, this, true);
				qrel = (RelativeLayout) findViewById(R.id.quest);
				buildcode = (Button) findViewById(R.id.build);
				buildtick = (ImageView) findViewById(R.id.buildovr);
				roofcover = (Button) findViewById(R.id.roofcover);
				roofcovertick = (ImageView) findViewById(R.id.roofcoverovr);
				roofdeck = (Button) findViewById(R.id.roofdeck);
				roofwall = (Button) findViewById(R.id.roofwall);
				roofgeometry = (Button) findViewById(R.id.roofgeo);
				swr = (Button) findViewById(R.id.swr);
				openprotection = (Button) findViewById(R.id.openpro);
				wallconstruction = (Button) findViewById(R.id.wall);
				roofdecktick = (ImageView) findViewById(R.id.roofdeckovr);
				roofwalltick = (ImageView) findViewById(R.id.roofwallovr);
				roofgeotick = (ImageView) findViewById(R.id.roofgeoovr);
				swrtick = (ImageView) findViewById(R.id.swrovr);
				opentick = (ImageView) findViewById(R.id.openproovr);
				walltick = (ImageView) findViewById(R.id.wallovr);
				/*if (cf.InspectionType.equals("28")) {
					wallconstruction.setVisibility(V.GONE);

				} else {
					wallconstruction.setVisibility(visibility);
				}*/
				if (!cf.buildcodevalue.equals("0")) {
					buildtick.setVisibility(visibility);
				}

				if (!cf.roofdeckvalueprev.equals("0")) {
					roofdecktick.setVisibility(visibility);
				}
				if (!cf.rooftowallvalueprev.equals("0")) {
					roofwalltick.setVisibility(visibility);
				}
				if (!cf.roofgeovalue.equals("0")) {
					roofgeotick.setVisibility(visibility);
				}
				if (!cf.roofswrvalue.equals("0")) {
					swrtick.setVisibility(visibility);
				}
				if (!cf.openprovalue.equals("0")) {
					opentick.setVisibility(visibility);
				}
				if (!cf.woodframeper.equals("0") || !cf.reper.equals("0")
						|| !cf.unreper.equals("0") || !cf.pcnper.equals("0")
						|| !cf.otrper.equals("0")) {
					walltick.setVisibility(visibility);
				}
				if (cf.chkrwss == 0) {
					roofcovertick.setVisibility(V.GONE);
				} else {
					roofcovertick.setVisibility(visibility);
				}
				System.out.println("gfdf");
				buildcode.setOnClickListener(new clicker());
				roofcover.setOnClickListener(new clicker());
				roofdeck.setOnClickListener(new clicker());
				roofwall.setOnClickListener(new clicker());
				roofgeometry.setOnClickListener(new clicker());
				swr.setOnClickListener(new clicker());
				openprotection.setOnClickListener(new clicker());
				wallconstruction.setOnClickListener(new clicker());
			}

		}
		/** menu **/
		else {
			layoutInflater.inflate(R.layout.menuclick, this, true);
			img1 = (ImageView) findViewById(R.id.img01);
			img2 = (ImageView) findViewById(R.id.img02);
			img3 = (ImageView) findViewById(R.id.img03);
			img4 = (ImageView) findViewById(R.id.img04);
			img6 = (ImageView) findViewById(R.id.img06);
			img7 = (ImageView) findViewById(R.id.img07);
			img8 = (ImageView) findViewById(R.id.img08);
			img9 = (ImageView) findViewById(R.id.img09);
			
			txtviewtakephoto = (TextView) findViewById(R.id.txttakephoto);
			takephoto = (ImageView) findViewById(R.id.takephoto);

			img1.setOnClickListener(new clicker());
			img2.setOnClickListener(new clicker());
			img3.setOnClickListener(new clicker());
			img4.setOnClickListener(new clicker());
			img6.setOnClickListener(new clicker());
			img7.setOnClickListener(new clicker());
			img8.setOnClickListener(new clicker());
			img9.setOnClickListener(new clicker());
			takephoto.setOnClickListener(new clicker());
			policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
			
			
			try {
				Cursor cur = cf.SelectTablefunction(cf.mbtblInspectionList,
						"where s_SRID='" + cf.Homeid + "'");
				int rws1 = cur.getCount();
				cur.moveToFirst();
				String ownerfirstname = cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnersNameFirst")));
				String ownerlastname = cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnersNameLast")));
				String policyno = cf.getsinglequotes(cur.getString(cur.getColumnIndex("s_OwnerPolicyNumber")));
				this.policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "
						+ ownerlastname
						+ "<br/><font color=#bddb00>(Policy No : " + policyno
						+ ")</font>"));

			} catch (Exception e) {
				
			}
			
		}

		switch (this.app) {

		case 1:
			img1.setBackgroundResource(R.drawable.policyholderovr);
			break;
		case 2:
			img2.setBackgroundResource(R.drawable.questionsovr);
			break;
		case 3:
			img3.setBackgroundResource(R.drawable.imagesovr);
			
			txtviewtakephoto.setVisibility(v1.GONE);
			takephoto.setVisibility(v1.GONE);
			break;
		case 4:
			img4.setBackgroundResource(R.drawable.feedbackovr);
			txtviewtakephoto.setVisibility(v1.GONE);
			takephoto.setVisibility(v1.GONE);
			break;
		case 5:
			img6.setBackgroundResource(R.drawable.overallcommentsovr);
			break;
		case 6:
			img7.setBackgroundResource(R.drawable.generalconditionovr);
			txtviewtakephoto.setVisibility(v1.GONE);
			takephoto.setVisibility(v1.GONE);
			break;
		case 7:
			img8.setBackgroundResource(R.drawable.mapovr);
			txtviewtakephoto.setVisibility(v1.GONE);
			takephoto.setVisibility(v1.GONE);
			
			break;
		case 8:
			img9.setBackgroundResource(R.drawable.submitovr);
			txtviewtakephoto.setVisibility(v1.GONE);
			takephoto.setVisibility(v1.GONE);
			break;
		case 11:
			poicyholdimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 12:
			agntimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 13:
			callimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 14:
			schimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 15:
			addimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 16:
			addiservice.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 21:
			buildcode.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 22:
			roofcover.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 23:
			roofdeck.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 24:
			roofwall.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 25:
			roofgeometry.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 26:
			swr.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 27:
			openprotection.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 28:
			wallconstruction.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;

		}

	}

	class clicker implements OnClickListener {

		public void onClick(View v) {
			// TODO Auto-generated method stub

			switch (v.getId()) {
			case R.id.img01:
				Intent i = new Intent(context, PersonalInfo.class);
				i.putExtra("homeid", cf.Homeid);
				i.putExtra("InspectionType", cf.InspectionType);
				i.putExtra("status", cf.status);
				i.putExtra("keyName", cf.value);
				i.putExtra("Count", cf.Count);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(i);
				break;
			case R.id.img02:
				System.out.println("2nd");
				System.out.println("cf.status" + cf.status + "\n"
						+ cf.inspectiondate);
				if (!cf.status.equals("assign")
						&& !cf.inspectiondate.equals("")
						&& !cf.inspectiondate.equals("N/A")
						&& !cf.inspectiondate.equals(null)) {
					Intent intque = new Intent(context, BuildCode.class);
					System.out.println("zsf");
					intque.putExtra("homeid", cf.Homeid);
					intque.putExtra("iden", "test");
					System.out.println("zsfasf");
					intque.putExtra("InspectionType", cf.InspectionType);
					System.out.println("fasf");
					intque.putExtra("status", cf.status);
					intque.putExtra("keyName", cf.value);
					intque.putExtra("Count", cf.Count);
					intque.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(intque);
					System.out.println("2ndends");
				} else {
					cf.ShowToast("Editing Information without Scheduling is not allowed.",1);

				}
				break;
			case R.id.img03:
				if (!cf.status.equals("assign")
						&& !cf.inspectiondate.equals("")
						&& !cf.inspectiondate.equals("N/A")) {
					Intent intimg = new Intent(context, ImagesData.class);
					intimg.putExtra("homeid", cf.Homeid);
					intimg.putExtra("iden", "test");
					intimg.putExtra("InspectionType", cf.InspectionType);
					intimg.putExtra("status", cf.status);
					intimg.putExtra("keyName", cf.value);
					intimg.putExtra("Count", cf.Count);
					intimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(intimg);
				} else {
					cf.ShowToast("Editing Information without Scheduling is not allowed.",1);

				}
				break;
			case R.id.img04:
				if (!cf.status.equals("assign")
						&& !cf.inspectiondate.equals("")
						&& !cf.inspectiondate.equals("N/A")) {
					Intent iInspectionList = new Intent(context,
							FeedbackDocuments.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList.putExtra("iden", "test");
					iInspectionList.putExtra("InspectionType",
							cf.InspectionType);
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.putExtra("keyName", cf.value);
					iInspectionList.putExtra("Count", cf.Count);
					iInspectionList.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(iInspectionList);
				} else {
					cf.ShowToast("Editing Information without Scheduling is not allowed.",1);

				}
				break;
			case R.id.img06:
				if (!cf.status.equals("assign")
						&& !cf.inspectiondate.equals("")
						&& !cf.inspectiondate.equals("N/A")) {
					Intent fb = new Intent(context, OverallComments.class);
					fb.putExtra("homeid", cf.Homeid);
					fb.putExtra("iden", "test");
					fb.putExtra("InspectionType", cf.InspectionType);
					fb.putExtra("status", cf.status);
					fb.putExtra("keyName", cf.value);
					fb.putExtra("Count", cf.Count);
					fb.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(fb);
				} else {
					cf.ShowToast("Editing Information without Scheduling is not allowed.",1);

				}
				break;
			case R.id.img07:
				if (!cf.status.equals("assign")
						&& !cf.inspectiondate.equals("")
						&& !cf.inspectiondate.equals("N/A")) {
					Intent genimg = new Intent(context, GeneralConditions.class);
					genimg.putExtra("homeid", cf.Homeid);
					genimg.putExtra("iden", "test");
					genimg.putExtra("InspectionType", cf.InspectionType);
					genimg.putExtra("status", cf.status);
					genimg.putExtra("keyName", cf.value);
					genimg.putExtra("Count", cf.Count);
					genimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(genimg);
				} else {
					cf.ShowToast("Editing Information without Scheduling is not allowed.",1);

				}
				break;
			case R.id.img08:
				/*if (!cf.status.equals("assign")
						&& !cf.inspectiondate.equals("")
						&& !cf.inspectiondate.equals("N/A")) {*/
					Intent mapimg = new Intent(context, Maps.class);
					mapimg.putExtra("homeid", cf.Homeid);
					mapimg.putExtra("iden", "test");
					mapimg.putExtra("InspectionType", cf.InspectionType);
					mapimg.putExtra("status", cf.status);
					mapimg.putExtra("keyName", cf.value);
					mapimg.putExtra("Count", cf.Count);
					mapimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(mapimg);
				/*} else {
					cf.ShowToast("Editing Information without Scheduling is not allowed.",1);

				}*/
				break;
			case R.id.img09:
				if (!cf.status.equals("assign")
						&& !cf.inspectiondate.equals("")
						&& !cf.inspectiondate.equals("N/A")) {
					Intent subimg = new Intent(context, Submit.class);
					subimg.putExtra("homeid", cf.Homeid);
					subimg.putExtra("iden", "test");
					subimg.putExtra("InspectionType", cf.InspectionType);
					subimg.putExtra("status", cf.status);
					subimg.putExtra("keyName", cf.value);
					subimg.putExtra("Count", cf.Count);
					subimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					if(count==0)
					{
						count++;
						call_dummy();
					
					}
					context.startActivity(subimg);
				} else {
					cf.ShowToast("Editing Information without Scheduling is not allowed.",1);

				}
				break;
			 case R.id.takephoto:
				 if(!cf.status.equals("assign")
							&& !cf.inspectiondate.equals("")
							&& !cf.inspectiondate.equals("N/A")) {
				 
					 cf.Createtablefunction(18);
					 Cursor cur = cf.db.rawQuery("SELECT * FROM "+ cf.tblphotocaption , null);
					 if(cur.getCount()>0)
					 {
						/* if(count==0)
							{
								count++;
								call_dummy();
							
							}*/
						cf.startCameraActivity();
					 }
					 else
					 {
						Intent intimg = new Intent(context, FrontElevation.class);
						intimg.putExtra("homeid", cf.Homeid);
						intimg.putExtra("elevation", 1);
						intimg.putExtra("InspectionType", cf.InspectionType);
						intimg.putExtra("status", cf.status);
						intimg.putExtra("keyName", cf.value);
						intimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						if(count==0)
						{
							count++;
							call_dummy();
						
						}
						context.startActivity(intimg);
					 }
				 }
				 else
				 {
					 cf.ShowToast("Take photo camera option without scheduling is not allowed.",1);

				 }
				 break;
			case R.id.policyhold:
				Intent ii = new Intent(context,PersonalInfo.class);
				ii.putExtra("homeid", cf.Homeid);
				ii.putExtra("InspectionType", cf.InspectionType);
				ii.putExtra("status", cf.status);
				ii.putExtra("keyName", cf.value);
				ii.putExtra("Count", cf.Count);
				ii.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(ii);
				
				break;
			case R.id.agnt:
				Intent agnt = new Intent(context,AgentInfo.class);
				agnt.putExtra("homeid", cf.Homeid);
				agnt.putExtra("InspectionType", cf.InspectionType);
				agnt.putExtra("status", cf.status);
				agnt.putExtra("keyName", cf.value);
				agnt.putExtra("Count", cf.Count);
				agnt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(agnt);
				break;
			case R.id.call:
				Intent i1 = new Intent();
				i1.putExtra("homeid", cf.Homeid);
				i1.putExtra("InspectionType", cf.InspectionType);
				i1.putExtra("status", cf.status);
				i1.putExtra("keyName", cf.value);
				i1.putExtra("Count", cf.Count);
				i1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i1.setClassName("idsoft.inspectiondepot.B11802",
						"idsoft.inspectiondepot.B11802.CallattemptInfo");
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(i1);
				break;
			case R.id.sch:
				Intent i12 = new Intent();
				i12.putExtra("homeid", cf.Homeid);
				i12.putExtra("InspectionType", cf.InspectionType);
				i12.putExtra("status", cf.status);
				i12.putExtra("keyName", cf.value);
				i12.putExtra("Count", cf.Count);
				i12.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i12.setClassName("idsoft.inspectiondepot.B11802",
						"idsoft.inspectiondepot.B11802.ScheduleInfo");
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(i12);
				break;
			case R.id.cloudinfo:
				Intent i13 = new Intent();
				i13.putExtra("homeid", cf.Homeid);
				i13.putExtra("InspectionType", cf.InspectionType);
				i13.putExtra("status", cf.status);
				i13.putExtra("keyName", cf.value);
				i13.putExtra("Count", cf.Count);
				i13.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i13.setClassName("idsoft.inspectiondepot.B11802",
						"idsoft.inspectiondepot.B11802.AdditionalInfo");
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(i13);
				break;
			case R.id.additional:
				
				Intent i14 = new Intent();
				i14.putExtra("homeid", cf.Homeid);
				i14.putExtra("InspectionType", cf.InspectionType);
				i14.putExtra("status", cf.status);
				i14.putExtra("keyName", cf.value);
				i14.putExtra("Count", cf.Count);
				i14.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i14.setClassName("idsoft.inspectiondepot.B11802",
						"idsoft.inspectiondepot.B11802.AdditionalService");
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(i14);
				break;
				
			case R.id.build:
				Intent bcint = new Intent(context, BuildCode.class);
				bcint.putExtra("homeid", cf.Homeid);
				bcint.putExtra("iden", "test");
				bcint.putExtra("InspectionType", cf.InspectionType);
				bcint.putExtra("status", cf.status);
				bcint.putExtra("keyName", cf.value);
				bcint.putExtra("Count", cf.Count);
				bcint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(bcint);
				break;
			case R.id.roofcover:
				Intent rcint = new Intent(context, RoofCover.class);
				rcint.putExtra("homeid", cf.Homeid);
				rcint.putExtra("iden", "test");
				rcint.putExtra("InspectionType", cf.InspectionType);
				rcint.putExtra("status", cf.status);
				rcint.putExtra("keyName", cf.value);
				rcint.putExtra("Count", cf.Count);
				rcint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(rcint);
				break;
			case R.id.roofdeck:
				Intent rdint = new Intent(context, RoofDeck.class);
				rdint.putExtra("homeid", cf.Homeid);
				rdint.putExtra("iden", "test");
				rdint.putExtra("InspectionType", cf.InspectionType);
				rdint.putExtra("status", cf.status);
				rdint.putExtra("keyName", cf.value);
				rdint.putExtra("Count", cf.Count);
				rdint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(rdint);
				break;
			case R.id.roofwall:
				Intent rwint = new Intent(context, RoofWall.class);
				rwint.putExtra("homeid", cf.Homeid);
				rwint.putExtra("iden", "test");
				rwint.putExtra("InspectionType", cf.InspectionType);
				rwint.putExtra("status", cf.status);
				rwint.putExtra("keyName", cf.value);
				rwint.putExtra("Count", cf.Count);
				rwint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(rwint);
				break;
			case R.id.roofgeo:
				Intent rgint = new Intent(context, RoofGeometry.class);
				rgint.putExtra("homeid", cf.Homeid);
				rgint.putExtra("iden", "test");
				rgint.putExtra("InspectionType", cf.InspectionType);
				rgint.putExtra("status", cf.status);
				rgint.putExtra("keyName", cf.value);
				rgint.putExtra("Count", cf.Count);
				rgint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(rgint);
				break;
			case R.id.swr:
				Intent sint = new Intent(context, SWR.class);
				sint.putExtra("homeid", cf.Homeid);
				sint.putExtra("iden", "test");
				sint.putExtra("InspectionType", cf.InspectionType);
				sint.putExtra("status", cf.status);
				sint.putExtra("keyName", cf.value);
				sint.putExtra("Count", cf.Count);
				sint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(sint);
				break;
			case R.id.openpro:
				Intent oint = new Intent(context, OpenProtect.class);
				oint.putExtra("homeid", cf.Homeid);
				oint.putExtra("iden", "test");
				oint.putExtra("InspectionType", cf.InspectionType);
				oint.putExtra("status", cf.status);
				oint.putExtra("keyName", cf.value);
				oint.putExtra("Count", cf.Count);
				oint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(oint);
				break;
			case R.id.wall:
				Intent wint = new Intent(context, WallConstruction.class);
				wint.putExtra("homeid", cf.Homeid);
				wint.putExtra("iden", "test");
				wint.putExtra("InspectionType", cf.InspectionType);
				wint.putExtra("status", cf.status);
				wint.putExtra("keyName", cf.value);
				wint.putExtra("Count", cf.Count);
				wint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				if(count==0)
				{
					count++;
					call_dummy();
				
				}
				context.startActivity(wint);
				break;

			}
		}

		
	}

	public void call_dummy() {
		// TODO Auto-generated method stub
		System.out.println("Called the dummy activity ="+count);
		
		Intent dummy = new Intent(context,Dummy.class);
		dummy.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(dummy);
	}
	
}
