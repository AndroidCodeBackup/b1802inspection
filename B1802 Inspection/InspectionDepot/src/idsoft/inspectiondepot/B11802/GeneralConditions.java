package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.OverallComments.OC_textwatcher;
import idsoft.inspectiondepot.B11802.OverallComments.Touch_Listener;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class GeneralConditions extends Activity {
	private static final android.view.View.OnClickListener OnClickListener = null;
	EditText s7;
	Button additionalbtn,gendatabtn;
	private static final int visibility = 0;
	LinearLayout lin081,general_parrant,general_type1,general_lin;
	private static final String TAG = null;
	EditText etinsuredother,etobservother,etoccupancyother;
	String insuredothertxt="",observothertxt="",occupancyothertxt="";
	int rws,focus=0;
	String homeid, InspectionType, status, strloc1, strloc2, strloc3, strloc4,
			strpf1, strpf2, strpf3, strpf4, strob1, strob2, strob3, strob4,
			strob5, strpp1, strpp2, strpp3, strpp4, strpp5, strpp6, strpp7,
			strhz1, strhz2, strhz3, strhz4, strhz5, strhz6, strhz7, strhz8,
			strhz9, strhz10, strhz11, strhz12, strhz13, strhz14, strhz15,
			strhz16, strhz17, strhz18, strhz19, strhz20, strhz21, strhz22,
			strhz23, strhz24, strhz25, strdata1, strdata2, strdata3, strdata4,
			strdata5, strdata6, stroccup, strvacant, strbuildsz, strother,
			selvac, selvacflg, selnd, strcomm;
	ImageView gentick, loctick, plfntick, pptick, obstick, haztick;
	int value, Count, c, b = 0, nd, ii, inc;
	String[] array_spinner, array_insured, array_obser, array_occ;
	int commentlength,data1flg, data2flg, data3flg, data4flg, data5flg, data6flg, qrws,
			loc1flg, loc2flg, loc3flg, loc4flg, obs1flg, obs2flg, obs3flg,
			obs4flg, obs5flg, pf1flg, pf2flg, pf3flg, pf4flg, pp1flg, pp2flg,
			pp3flg, pp4flg, pp5flg, pp6flg, pp7flg, hz1flg, hz2flg, hz3flg,
			hz4flg, hz5flg, hz6flg, hz7flg, hz8flg, hz9flg, hz10flg, hz11flg,
			hz12flg, hz13flg, hz14flg, hz15flg, hz16flg, hz17flg, hz18flg,
			hz19flg, hz20flg, hz21flg, hz22flg, hz23flg, hz24flg, hz25flg,
			occflg, vacflg;
	String seldata1, seldata2, seldata3, seldata4, seldata5, seldata6,
			seloccup, selbuildsz, selother, selflg, selloc1, selloc2, selloc3,
			selloc4;
	Cursor c2;
	View v1;
	int o, va;
	TextView policyholderinfo,general_TV_type1;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	RelativeLayout rellayt;
	CheckBox chkinclude;
	EditText etcomments;
	CommonFunctions cf;
	TextWatcher watcher;
	TableLayout exceedslimit1;
	Button home;
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		/** Creating Genral conditon table **/
		cf.Createtablefunction(7);
		cf.Createtablefunction(9);
		cf.Createtablefunction(15);
		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");
		}

		setContentView(R.layout.general);
		cf.getInspectorId();
		cf.getDeviceDimensions();
		cf.getinspectiondate();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 6, cf, 0));
		TableRow tblrw = (TableRow)findViewById(R.id.row2);
	    tblrw.setMinimumHeight(cf.ht);
	    focus=1;

		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());

		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		etcomments = (EditText) findViewById(R.id.etcomment);
		
		general_parrant = (LinearLayout)this.findViewById(R.id.general_parrant);
		general_type1=(LinearLayout) findViewById(R.id.general_type);
		general_TV_type1 = (TextView) findViewById(R.id.general_text);
		etcomments.setOnTouchListener(new Touch_Listener());
		etcomments.addTextChangedListener(new GC_textwatcher());
		
		general_lin = (LinearLayout) this.findViewById(R.id.general_lin);
		home = (Button) findViewById(R.id.hme1);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		
		array_spinner = new String[5];
		array_spinner[0] = "Not Applicable";
		array_spinner[1] = "No Access";
		array_spinner[2] = "Not Determined";
		array_spinner[3] = "Yes";
		array_spinner[4] = "No";

		array_insured = new String[6];
		array_insured[0] = "Owner";
		array_insured[1] = "Corporation";
		array_insured[2] = "Individual";
		array_insured[3] = "Tenant";
		array_insured[4] = "Partnership";
		array_insured[5] = "Other";

		array_obser = new String[5];
		array_obser[0] = "Interior Only";
		array_obser[1] = "Exterior Only";
		array_obser[2] = "Interior & Exterior";
		array_obser[3] = "Common Areas Only";
		array_obser[4] = "Other";

		array_occ = new String[8];
		array_occ[0] = "Single Family Home";
		array_occ[1] = "Condominium";
		array_occ[2] = "Town Home";
		array_occ[3] = "Duplex";
		array_occ[4] = "Triples";
		array_occ[5] = "Quadplex";
		array_occ[6] = "Apartment";
		array_occ[7] = "Other";

		rellayt = (RelativeLayout) findViewById(R.id.LinearLayout02);
		additionalbtn = (Button)findViewById(R.id.additional);
		gendatabtn = (Button)findViewById(R.id.data);

		chkinclude = (CheckBox) findViewById(R.id.include);
		try {
			Cursor cur = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
			int rws1 = cur.getCount();
			cur.moveToFirst();
			String ownerfirstname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameFirst")));
			String ownerlastname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameLast")));
			String policyno = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnerPolicyNumber")));
			policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "
					+ ownerlastname + "<br/><font color=#bddb00>(Policy No : "
					+ policyno + ")</font>"));

		} catch (Exception e) {
			System.out.println(" errrr " + e.getMessage());
		}
		try {
			Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.tbl_questionsdata
					+ " WHERE SRID='" + cf.Homeid + "'", null);
			qrws = c2.getCount();
			c2.moveToFirst();
			if (c2.getString(c2.getColumnIndex("GeneralHazardInclude")).equals(
					"1")) {
				inc = 1;
				chkinclude.setChecked(true);
				home.setVisibility(v1.GONE);
				
				rellayt.setVisibility(visibility);
				additionalbtn.setVisibility(v1.VISIBLE);
				gendatabtn.setVisibility(v1.VISIBLE);

				show();
			} else {
				inc = 2;
				chkinclude.setChecked(false);
				home.setVisibility(visibility);
				rellayt.setVisibility(v1.GONE);
				additionalbtn.setVisibility(v1.GONE);
				gendatabtn.setVisibility(v1.GONE);

			}
		} catch (Exception e) {
			System.out.println("Error = " + e.getMessage());
		}
		chkinclude.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					inc = 1;
					chkinclude.setChecked(true);
					home.setVisibility(v1.GONE);
					show();

				} else {
					inc = 2;
					home.setVisibility(visibility);
					chkinclude.setChecked(false);
					rellayt.setVisibility(v.GONE);
					additionalbtn.setVisibility(v1.GONE);
					gendatabtn.setVisibility(v1.GONE);

					if (qrws == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.tbl_questionsdata
								+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
								+ "VALUES ('" + cf.Homeid + "','','','" + 0
								+ "','" + 0 + "','" + 0 + "','','','','','" + 0
								+ "','" + 0 + "','','" + 0 + "','" + 0 + "','"
								+ 0 + "','" + 0 + "','" + 0 + "','','" + 0
								+ "','" + 0 + "','" + 0 + "','','" + 0
								+ "','','" + 0 + "','" + 0
								+ "','','','','','','','','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','','" + 0 + "','"
								+ 0 + "','','" + 0 + "','','','" + cd + "','"
								+ inc + "')");
					} else {
						cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
								+ " SET GeneralHazardInclude='" + inc
								+ "',ModifyDate ='" + md + "' WHERE SRID ='"
								+ cf.Homeid + "'");

					}

				}
			}
		});
		if (inc == 1) {
			show();
		} else {
			rellayt.setVisibility(v1.GONE);
			additionalbtn.setVisibility(v1.GONE);
			gendatabtn.setVisibility(v1.GONE);
			if (qrws == 0) {
				cf.db.execSQL("INSERT INTO "
						+ cf.tbl_questionsdata
						+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
						+ "VALUES ('" + cf.Homeid + "','','','" + 0 + "','" + 0
						+ "','" + 0 + "','','','','','" + 0 + "','" + 0
						+ "','','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','" + 0 + "','','" + 0 + "','" + 0 + "','" + 0
						+ "','','" + 0 + "','','" + 0 + "','" + 0
						+ "','','','','','','','','" + 0 + "','" + 0 + "','"
						+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','" + 0 + "','','" + 0 + "','" + 0 + "','','" + 0
						+ "','','','" + cd + "','" + inc + "')");
			} else {
				cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
						+ " SET GeneralHazardInclude='" + inc
						+ "',ModifyDate ='" + md + "' WHERE SRID ='"
						+ cf.Homeid + "'");

			}
		}

	}
	class Touch_Listener implements OnTouchListener
	{
		   Touch_Listener()
			{
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	cf.setFocus(etcomments);
				
				return false;
		    }
	}
	class GC_textwatcher implements TextWatcher
	{
	    GC_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
				
				cf.showing_limit(s.toString(),general_parrant,general_type1,general_TV_type1,"499"); 
			
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	protected void show() {
		// TODO Auto-generated method stub
		rellayt.setVisibility(visibility);
		additionalbtn.setVisibility(v1.VISIBLE);
		gendatabtn.setVisibility(v1.VISIBLE);
		if (qrws == 0) {
			cf.db.execSQL("INSERT INTO "
					+ cf.tbl_questionsdata
					+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
					+ "VALUES ('" + cf.Homeid + "','','','" + 0 + "','" + 0
					+ "','" + 0 + "','','','','','" + 0 + "','" + 0 + "','','"
					+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
					+ "','','" + 0 + "','" + 0 + "','" + 0 + "','','" + 0
					+ "','','" + 0 + "','" + 0 + "','','','','','','','','" + 0
					+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
					+ "','" + 0 + "','" + 0 + "','','" + 0 + "','" + 0
					+ "','','" + 0 + "','','','" + cd + "','" + inc + "')");
		} else {
			cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
					+ " SET GeneralHazardInclude='" + inc + "',ModifyDate ='"
					+ md + "' WHERE SRID ='" + cf.Homeid + "'");

		}
		selrow();
		if (rws != 0) {
			retrievedata();
			b = 1;
			etcomments.setText(cf.getsinglequotes(strcomm));

		}
		TextView genimg = (TextView) findViewById(R.id.Vimage);
		genimg.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				System.out.println("genimg");
				//cf.hidekeyboard();
				b = 1;
				
				data();

			}
		});
		gentick = (ImageView) findViewById(R.id.genovr);
		TextView gentxt = (TextView) findViewById(R.id.gendata);
		gentxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				b = 1;
				data();
			}
		});
		TextView locimg = (TextView) findViewById(R.id.locvimg);
		loctick = (ImageView) findViewById(R.id.locovr);
		locimg.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				location();

			}
		});
		TextView locatxt = (TextView) findViewById(R.id.loca);
		locatxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				location();
			}
		});

		TextView plfnimg = (TextView) findViewById(R.id.plfvimg);
		plfntick = (ImageView) findViewById(R.id.polfnovr);
		plfnimg.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				poolfence();

			}
		});
		TextView plfntxt = (TextView) findViewById(R.id.poolfenc);
		plfntxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				poolfence();
			}
		});

		TextView obsimg = (TextView) findViewById(R.id.obsvimg);
		obstick = (ImageView) findViewById(R.id.obsovr);
		obsimg.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				observation();

			}
		});
		TextView obstxt = (TextView) findViewById(R.id.obs);
		obstxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				observation();
			}
		});

		TextView ppimg = (TextView) findViewById(R.id.ppvimg);
		pptick = (ImageView) findViewById(R.id.ppovr);
		ppimg.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				poolpresent();

			}
		});
		TextView pptxt = (TextView) findViewById(R.id.poolpres);
		pptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				poolpresent();
			}
		});

		TextView hazimg = (TextView) findViewById(R.id.hazvimg);
		haztick = (ImageView) findViewById(R.id.hazovr);
		hazimg.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				hazard();

			}
		});
		TextView haztxt = (TextView) findViewById(R.id.haz);
		haztxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				b = 1;
				hazard();
			}
		});
		changeimage();
	}

	private void changeimage() {
		// TODO Auto-generated method stub
		try {
			Cursor cur6 = cf.db.rawQuery("select * from " + cf.HazardDataTable
					+ " where Srid='" + cf.Homeid + "'", null);
			cur6.moveToFirst();
			if (cur6 != null) {
				String insured = cur6.getString(cur6
						.getColumnIndex("InsuredIs"));
				if (!insured.equals("")) {
					gentick.setVisibility(visibility);
				}
			}
			Cursor cur = cf.db.rawQuery("select * from " + cf.HazardDataTable
					+ " where Srid='" + cf.Homeid + "'", null);
			cur.moveToFirst();
			if (cur != null) {
				String loc = cur.getString(cur.getColumnIndex("Location1"));
				if (!loc.equals("")) {
					loctick.setVisibility(visibility);
				}
			}
			Cursor cur1 = cf.db.rawQuery("select * from " + cf.HazardDataTable
					+ " where Srid='" + cf.Homeid + "'", null);
			cur1.moveToFirst();
			if (cur1 != null) {
				String obs = cur1
						.getString(cur1.getColumnIndex("Observation1"));
				if (!obs.equals("")) {
					obstick.setVisibility(visibility);
				}
			}
			Cursor cur11 = cf.db.rawQuery("select * from " + cf.HazardDataTable
					+ " where Srid='" + cf.Homeid + "'", null);
			cur11.moveToFirst();
			if (cur11 != null) {
				String plfn = cur11.getString(cur11
						.getColumnIndex("PerimeterPoolFence"));
				if (!plfn.equals("")) {
					plfntick.setVisibility(visibility);
				}
			}
			Cursor cur21 = cf.db.rawQuery("select * from " + cf.HazardDataTable
					+ " where Srid='" + cf.Homeid + "'", null);
			cur21.moveToFirst();
			if (cur21 != null) {
				String pp = cur21
						.getString(cur21.getColumnIndex("PoolPresent"));
				if (!pp.equals("")) {
					pptick.setVisibility(visibility);
				}
			}
			Cursor cur211 = cf.db.rawQuery("select * from "
					+ cf.HazardDataTable + " where Srid='" + cf.Homeid + "'",
					null);
			cur211.moveToFirst();
			if (cur211 != null) {
				String haz = cur211.getString(cur211.getColumnIndex("Vicious"));
				if (!haz.equals("")) {
					haztick.setVisibility(visibility);
				}
			}
		} catch (Exception e) {

		}
	}

	private void selrow() {

		try {
			Cursor c1 = cf.db.rawQuery("SELECT * FROM " + cf.HazardDataTable
					+ " WHERE Srid='" + cf.Homeid + "'", null);
			rws = c1.getCount();
		} catch (Exception e) {
			Log.i(TAG, "error= " + e.getMessage());
		}
	}

	private void retrievedata() {
		try {
			c2 = cf.db.rawQuery("SELECT * FROM  " + cf.HazardDataTable
					+ " WHERE Srid='" + cf.Homeid + "'", null);
			int rws = c2.getCount();
			
			System.out.println("ds rws e here"+rws);
			
			int Column = c2.getColumnIndex("InsuredIs");
			int Column1 = c2.getColumnIndex("OccupancyType");
			int Column2 = c2.getColumnIndex("ObservationType");
			int Column3 = c2.getColumnIndex("OccupiedPercent");
			int Column4 = c2.getColumnIndex("OccupiedFlag");
			int Column56 = c2.getColumnIndex("Vacant");
			int Column57 = c2.getColumnIndex("VacantPercent");
			int Column58 = c2.getColumnIndex("NotDetermined");
			int Column5 = c2.getColumnIndex("PermitConfirmed");
			int Column6 = c2.getColumnIndex("BuildingSize");
			int Column7 = c2.getColumnIndex("BalconyPresent");
			int Column8 = c2.getColumnIndex("AdditionalStructure");
			int Column9 = c2.getColumnIndex("OtherLocation");
			int Column10 = c2.getColumnIndex("Location1");
			int Column11 = c2.getColumnIndex("Location2");
			int Column12 = c2.getColumnIndex("Location3");
			int Column13 = c2.getColumnIndex("Location4");
			int Column14 = c2.getColumnIndex("Observation1");
			int Column15 = c2.getColumnIndex("Observation2");
			int Column16 = c2.getColumnIndex("Observation3");
			int Column17 = c2.getColumnIndex("Observation4");
			int Column18 = c2.getColumnIndex("Observation5");
			int Column19 = c2.getColumnIndex("PerimeterPoolFence");
			int Column20 = c2.getColumnIndex("PoolFenceDisrepair");
			int Column21 = c2.getColumnIndex("SelfLatch");
			int Column22 = c2.getColumnIndex("ProfesInstall");
			int Column23 = c2.getColumnIndex("PoolPresent");
			int Column24 = c2.getColumnIndex("HotTubPresnt");
			int Column25 = c2.getColumnIndex("EmptyGroundPool");
			int Column26 = c2.getColumnIndex("PoolSlide");
			int Column27 = c2.getColumnIndex("DivingBoardPoolSlide");
			int Column28 = c2.getColumnIndex("PerimeterPoolEncl");
			int Column29 = c2.getColumnIndex("PoolEnclosure");
			int Column30 = c2.getColumnIndex("Vicious");
			int Column31 = c2.getColumnIndex("LiveStock");
			int Column32 = c2.getColumnIndex("OverHanging");
			int Column33 = c2.getColumnIndex("Trampoline");
			int Column34 = c2.getColumnIndex("SkateBoard");
			int Column36 = c2.getColumnIndex("bicycle");
			int Column37 = c2.getColumnIndex("TripHazardDesc");
			int Column38 = c2.getColumnIndex("TripHazardNoted");
			int Column39 = c2.getColumnIndex("UnsafeStairway");
			int Column40 = c2.getColumnIndex("PorchAndDeck");
			int Column41 = c2.getColumnIndex("NonStdConstruction");
			int Column42 = c2.getColumnIndex("OutDoorAppliances");
			int Column43 = c2.getColumnIndex("OpenFoundation");
			int Column44 = c2.getColumnIndex("WoodShingled");
			int Column45 = c2.getColumnIndex("ExcessDebris");
			int Column46 = c2.getColumnIndex("BusinessPremises");
			int Column47 = c2.getColumnIndex("GeneralDisrepair");
			int Column48 = c2.getColumnIndex("PropertyDamage");
			int Column49 = c2.getColumnIndex("StructurePartial");
			int Column50 = c2.getColumnIndex("InOperative");
			int Column51 = c2.getColumnIndex("RecentDrywall");
			int Column52 = c2.getColumnIndex("ChineseDrywall");
			int Column53 = c2.getColumnIndex("ConfirmDrywall");
			int Column54 = c2.getColumnIndex("NonSecurity");
			int Column55 = c2.getColumnIndex("NonSmoke");

			int Column60 = c2.getColumnIndex("Comments");
			c2.moveToFirst();
			if (c2 != null) {
				do {
					strdata1 = c2.getString(Column);
					
					insuredothertxt = c2.getString(c2.getColumnIndex("InsuredOther"));
					observothertxt = c2.getString(c2.getColumnIndex("ObservOther"));
					occupancyothertxt = c2.getString(c2.getColumnIndex("OcccupOther"));
					
					strdata3 = c2.getString(Column1);
					strdata2 = c2.getString(Column2);
					seloccup = c2.getString(Column3);
					selflg = c2.getString(Column4);
					selvac = c2.getString(Column56);
					selvacflg = c2.getString(Column57);
					selnd = c2.getString(Column58);System.out.println("sleec "+selnd);
					strdata4 = c2.getString(Column5);
					selbuildsz = c2.getString(Column6);
					strdata5 = c2.getString(Column7);
					strdata6 = c2.getString(Column8);
					selother = c2.getString(Column9);
					strloc1 = c2.getString(Column10);
					strloc2 = c2.getString(Column11);
					strloc3 = c2.getString(Column12);
					strloc4 = c2.getString(Column13);
					strob1 = c2.getString(Column14);
					strob2 = c2.getString(Column15);
					strob3 = c2.getString(Column16);
					strob4 = c2.getString(Column17);
					strob5 = c2.getString(Column18);
					strpf1 = c2.getString(Column19);
					strpf4 = c2.getString(Column20);
					strpf2 = c2.getString(Column21);
					strpf3 = c2.getString(Column22);
					strpp1 = c2.getString(Column23);
					strpp2 = c2.getString(Column24);
					strpp3 = c2.getString(Column25);
					strpp4 = c2.getString(Column26);
					strpp5 = c2.getString(Column27);
					strpp6 = c2.getString(Column28);
					strpp7 = c2.getString(Column29);

					strhz1 = c2.getString(Column30);
					strhz2 = c2.getString(Column31);
					strhz3 = c2.getString(Column32);
					strhz4 = c2.getString(Column33);
					strhz5 = c2.getString(Column34);
					strhz6 = c2.getString(Column36);
					strhz8 = c2.getString(Column37);
					strhz7 = c2.getString(Column38);
					strhz9 = c2.getString(Column39);
					strhz10 = c2.getString(Column40);
					strhz11 = c2.getString(Column41);
					strhz12 = c2.getString(Column42);
					strhz13 = c2.getString(Column43);
					strhz14 = c2.getString(Column44);
					strhz15 = c2.getString(Column45);
					strhz16 = c2.getString(Column46);
					strhz17 = c2.getString(Column47);
					strhz18 = c2.getString(Column48);
					strhz19 = c2.getString(Column49);
					strhz20 = c2.getString(Column50);
					strhz21 = c2.getString(Column51);
					strhz22 = c2.getString(Column52);
					strhz23 = c2.getString(Column53);
					strhz24 = c2.getString(Column54);
					strhz25 = c2.getString(Column55);
					strcomm = c2.getString(Column60);

				} while (c2.moveToNext());
			}

		} catch (Exception e) {
			Log.i(TAG, "error = " + e.getMessage());
		}
	}

	public void data() {
		
		final Dialog dialog = new Dialog(GeneralConditions.this);
		dialog.setContentView(R.layout.generaldata);
		dialog.setTitle("GENERAL DATA");
		dialog.setCancelable(true);
		//cf.hidekeyboard();
		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		etinsuredother = (EditText) dialog.findViewById(R.id.etinsuredother);
		
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_insured);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		s.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());

		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		etobservother = (EditText) dialog.findViewById(R.id.etobservother);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_obser);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerdata1());

		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		etoccupancyother = (EditText) dialog.findViewById(R.id.etoccupancyother);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_occ);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerdata2());

		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner07);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner08);
		Spinner s5 = (Spinner) dialog.findViewById(R.id.Spinner09);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerdata3());
		ArrayAdapter adapter4 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		s4.setAdapter(adapter4);
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerdata4());
		ArrayAdapter adapter5 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		s5.setAdapter(adapter5);
		s5.setOnItemSelectedListener(new MyOnItemSelectedListenerdata5());

		final CheckBox chkbx1 = (CheckBox) dialog.findViewById(R.id.chk1);
		final CheckBox chkbx2 = (CheckBox) dialog.findViewById(R.id.chk2);
		final CheckBox chkbx3 = (CheckBox) dialog.findViewById(R.id.chk3);

		final EditText et1 = (EditText) dialog.findViewById(R.id.ed1);
		final EditText et2 = (EditText) dialog.findViewById(R.id.ed2);
		final EditText et3 = (EditText) dialog.findViewById(R.id.ed3);
		et3.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				cf.setFocus(et3);
				return false;
			}
		});
		
		
		
		final EditText et4 = (EditText) dialog.findViewById(R.id.ed4);
		et4.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				cf.setFocus(et4);
				return false;
			}
		});
		et1.setEnabled(false);
		et2.setEnabled(false);
		selrow();
		if (rws > 0) {
			retrievedata();
			if(strdata1.equals("Other")){
				etinsuredother.setText(cf.getsinglequotes(insuredothertxt));
			}
			if(strdata2.equals("Other")){
				etobservother.setText(cf.getsinglequotes(observothertxt));
			}
			if(strdata3.equals("Other")){
				etoccupancyother.setText(cf.getsinglequotes(occupancyothertxt));
			}
			int spinnerPosition = adapter.getPosition(strdata1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strdata2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strdata3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strdata4);
			s3.setSelection(spinnerPosition3);
			int spinnerPosition4 = adapter4.getPosition(strdata5);
			s4.setSelection(spinnerPosition4);
			int spinnerPosition5 = adapter5.getPosition(strdata6);
			s5.setSelection(spinnerPosition5);
			if (selflg.equals("1")) {
				if (!seloccup.equals("")) {
					chkbx1.setChecked(true);
				} else {
					chkbx1.setChecked(false);
				}
				et1.setText(seloccup);
				et1.setEnabled(true);
				chkbx3.setChecked(false);
			} else {

			}
			if (selvac.equals("1")) {
				if (!selvacflg.equals("")) {
					chkbx2.setChecked(true);
				} else {
					chkbx2.setChecked(false);
				}
				et2.setText(selvacflg);
				et2.setEnabled(true);
				chkbx3.setChecked(false);
			} else {

			}
			if (selnd.equals("1")) {
				System.out.println("chkbox 3 is checked");
				chkbx3.setChecked(true);
				et2.setText("");
				et1.setText("");
				chkbx1.setChecked(false);
				chkbx2.setChecked(false);
			} else {
				System.out.println("chkbox 3 is unchecked");
			}
			System.out.println("slee "+selbuildsz);
			if(selbuildsz.equals(""))
			{
				try
				{
					cf.Createtablefunction(2);
					Cursor c = cf.db.rawQuery("select * from "
							+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
							+ "' and InspectorId='"
						+ cf.InspectorId + "'", null);
					
				int policyrws = c.getCount();System.out.println("RRRR "+policyrws);
				if(policyrws>0)
				{
					c.moveToFirst();
					String buidingsize = c.getString(c
							.getColumnIndex("BuildingSize"));
					System.out.println("buidingsize "+buidingsize);
					if(!"".equals(buidingsize) || !"null".equals(buidingsize))
					{
						et3.setText(buidingsize);
					}
					
				}
				}
				catch(Exception e)
				{
					System.out.println("Exception rows"+e.getMessage());
				}
			}
			else
			{
				System.out.println("slee not null"+selbuildsz);
			et3.setText(selbuildsz);
			}
			et4.setText(cf.getsinglequotes(selother));

		}
		else
		{
			System.out.println("no rows");
			try
			{
				cf.Createtablefunction(2);
				Cursor c = cf.db.rawQuery("select * from "
						+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
						+ "' and InspectorId='"
					+ cf.InspectorId + "'", null);
				
			int policyrws = c.getCount();System.out.println("RRRR "+policyrws);
			if(policyrws>0)
			{
				c.moveToFirst();
				String buidingsize = cf.getsinglequotes(c.getString(c
						.getColumnIndex("BuildingSize")));
				System.out.println("buidingsize "+buidingsize);
				if(!"".equals(buidingsize) || !"null".equals(buidingsize))
				{
					et3.setText(buidingsize);
				}
			}
			}
			catch(Exception e)
			{
				System.out.println("Exception rows"+e.getMessage());
			}
			
		}

		chkbx1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					c = 1;
					chkbx1.setChecked(true);
					nd = 0;
					cf.setFocus(et1);
					chkbx3.setChecked(false);
					et1.setEnabled(true);
					et1.setCursorVisible(true);

				} else {
					c = 0;
					et1.setEnabled(false);
					et1.setText("");
				}
			}
		});
		chkbx2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					c = 2;
					chkbx2.setChecked(true);
					nd = 0;
					chkbx3.setChecked(false);
					cf.setFocus(et2);
					et2.setEnabled(true);
					et2.setCursorVisible(true);

				} else {
					c = 0;
					et2.setEnabled(false);
					et2.setText("");
				}
			}
		});
		chkbx3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					c = 3;
					chkbx1.setChecked(false);
					nd = 1;
					chkbx2.setChecked(false);
					chkbx3.setChecked(true);
					et1.setEnabled(false);
					et2.setEnabled(false);
					et1.setText("");
					et2.setText("");
				} else {
					c = 0;
				}
			}
		});
		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
System.out.println("creer "+c + " nd = "+nd);
				if ((c == 3) || chkbx3.isChecked()==true)  {
					System.out.println("ccc "+c);
					nd = 1;
					occflg = 0;
					vacflg = 0;
					stroccup = "";
					strvacant = "";
				} else {
					System.out.println("this is");
					if (chkbx1.isChecked() == true) {
						occflg = 1;
						stroccup = et1.getText().toString();
					} else {
						occflg = 0;
						stroccup = "";
					}
					if (chkbx2.isChecked() == true) {

						vacflg = 1;
						strvacant = et2.getText().toString();
					} else {
						System.out.println("n else "+nd);
						vacflg = 0;
						strvacant = "";
					}
				}
				strbuildsz = et3.getText().toString();
				strother = et4.getText().toString();
				
				insuredothertxt=etinsuredother.getText().toString();
				observothertxt=etobservother.getText().toString();
				occupancyothertxt=etoccupancyother.getText().toString();
				
				boolean[] othertext =new boolean[3];
				othertext[0]=(strdata1.equals("Other"))? ((insuredothertxt.trim().equals(""))? false:true):true;
				othertext[1]=(strdata2.equals("Other"))? ((observothertxt.trim().equals(""))? false:true):true;
				othertext[2]=(strdata3.equals("Other"))? ((occupancyothertxt.trim().equals(""))? false:true):true;
				
				if (rws == 0) {
					if(othertext[0]==true){
						if(othertext[1]==true){
							if(othertext[2]==true){
					if (chkbx1.isChecked() == true
							|| chkbx2.isChecked() == true
							|| chkbx3.isChecked() == true) {
						if (chkbx1.isChecked() == true
								|| chkbx2.isChecked() == true) {
							try {
								if (!et1.getText().toString().equals("")) {
									try
									{
										o = Integer.parseInt(stroccup);									
									}
									catch(Exception e)
									{
										o=0;											
									}									
								}
								if (!et2.getText().toString().equals("")) {
									try
									{
										va = Integer.parseInt(strvacant);							
									}
									catch(Exception e)
									{
										va=0;											
									}		
									
								}
							} catch (Exception e) {

							}
							if (stroccup.equals("") && strvacant.equals("")) {
								cf.ShowToast("Building Occupancy Percentage should not be empty.",1);
							} else {
								if ((o + va) == 100) {
									if (o == 100) {
										chkbx2.setChecked(false);
									} else if (va == 100) {
										chkbx1.setChecked(false);
									}
System.out.println("Camerher ");
									cf.db.execSQL("INSERT INTO "
											+ cf.HazardDataTable
											+ " (Srid,InsuredIs,InsuredOther,OccupancyType,OcccupOther,ObservationType,ObservOther,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
											+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
											+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
											+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
											+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
											+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
											+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
											+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
											+ " VALUES ('"
											+ cf.Homeid
											+ "','"
											+ strdata1
											+ "','"+cf.convertsinglequotes(insuredothertxt)+"','"
											+ strdata3
											+ "','"+cf.convertsinglequotes(occupancyothertxt)+"','"
											+ strdata2
											+ "','"+cf.convertsinglequotes(observothertxt)+"','"
											+ stroccup
											+ "','"
											+ occflg
											+ "','"
											+ vacflg
											+ "','"
											+ strvacant
											+ "','"
											+ nd
											+ "','"
											+ strdata4
											+ "','"
											+ strbuildsz
											+ "','"
											+ strdata5
											+ "','"
											+ strdata6
											+ "','"
											+ cf.convertsinglequotes(strother)
											+ "',"
											+ "'','','','',"
											+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
									try {
										Cursor c2 = cf.db
												.rawQuery("SELECT * FROM "
														+ cf.SubmitCheckTable
														+ " WHERE Srid='"
														+ cf.Homeid + "'", null);
										int subchkrws = c2.getCount();
										if (subchkrws == 0) {
											cf.db.execSQL("INSERT INTO "
													+ cf.SubmitCheckTable
													+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
													+ "VALUES ('"
													+ cf.InspectorId
													+ "','"
													+ cf.Homeid
													+ "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1)");
										} else {
											cf.db.execSQL("UPDATE "
													+ cf.SubmitCheckTable
													+ " SET fld_hazarddata='1' WHERE Srid ='"
													+ cf.Homeid
													+ "' and InspectorId='"
													+ cf.InspectorId + "'");
										}
									} catch (Exception e) {

									}
									cf.ShowToast("Saved suceesfully.",1);
									dialog.dismiss();
									gentick.setVisibility(visibility);
								} else if ((o + va) < 100) {
									cf.ShowToast("Building Occupancy Percentage should be equal to 100%.",1);
								} else {
									cf.ShowToast("Building Occupancy Percentage should not be greater than 100%.",1);
								}

							}
						} else {
							cf.db.execSQL("INSERT INTO "
									+ cf.HazardDataTable
									+ " (Srid,InsuredIs,InsuredOther,OccupancyType,OcccupOther,ObservationType,ObservOther,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
									+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
									+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
									+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
									+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
									+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
									+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
									+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
									+ " VALUES ('"
									+ cf.Homeid
									+ "','"
									+ strdata1
									+ "','"+cf.convertsinglequotes(insuredothertxt)+"','"
									+ strdata3
									+ "','"+cf.convertsinglequotes(occupancyothertxt)+"','"
									+ strdata2
									+ "','"+cf.convertsinglequotes(observothertxt)+"','"
									+ stroccup
									+ "','"
									+ occflg
									+ "','"
									+ vacflg
									+ "','"
									+ strvacant
									+ "','"
									+ nd
									+ "','"
									+ strdata4
									+ "','"
									+ strbuildsz
									+ "','"
									+ strdata5
									+ "','"
									+ strdata6
									+ "','"
									+ cf.convertsinglequotes(strother)
									+ "',"
									+ "'','','','',"
									+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
							try {
								Cursor c2 = cf.db.rawQuery("SELECT * FROM "
										+ cf.SubmitCheckTable + " WHERE Srid='"
										+ cf.Homeid + "'", null);
								int subchkrws = c2.getCount();
								if (subchkrws == 0) {

								} else {

								}
							} catch (Exception e) {

							}
							cf.ShowToast("Saved suceesfully.",1);
							dialog.dismiss();
						}
						gentick.setVisibility(visibility);
					} else {
						cf.ShowToast("Please select Building Occupancy.",1);

					}
					} else {
						cf.ShowToast("Please enter the other text for occupancy type.",1);
					}
  				   } else {
						cf.ShowToast("Please enter the other text for observation type.",1);

  				   }
					}else{
						cf.ShowToast("Please enter the other text for insured is.",1);
					}
					
				} else {
					if(othertext[0]==true){
						if(othertext[1]==true){
							if(othertext[2]==true){
					if (chkbx1.isChecked() == true
							|| chkbx2.isChecked() == true
							|| chkbx3.isChecked() == true) {
						if (chkbx1.isChecked() == true
								|| chkbx2.isChecked() == true) {
							o = 0;
							va = 0;
							try {
								if (!et1.getText().toString().equals("")) {
									try
									{
										o = Integer.parseInt(et1.getText()
												.toString());								
									}
									catch(Exception e)
									{
										o=0;											
									}	
									
									
								}
								if (!et2.getText().toString().equals("")) {
									try
									{
										va = Integer.parseInt(et2.getText()
												.toString());							
									}
									catch(Exception e)
									{
										va=0;											
									}	
									
								}
							} catch (Exception e) {

							}

							if (et1.getText().toString().equals("")
									&& et2.getText().toString().equals("")) {
								cf.ShowToast("Building Occupancy Percentage should not be empty.",1);
							} else {
								if ((o + va) == 100) {
									if (o == 100) {
										chkbx2.setChecked(false);
									} else if (va == 100) {
										chkbx1.setChecked(false);
									}
									System.out.println("its 111");
									cf.db.execSQL("UPDATE "
											+ cf.HazardDataTable
											+ " SET InsuredIs='" + strdata1
											+ "',InsuredOther='" + cf.convertsinglequotes(insuredothertxt)
											+ "',OccupancyType='" + strdata3
											+ "',OcccupOther='" + cf.convertsinglequotes(occupancyothertxt)
											+ "',ObservationType='" + strdata2
											+ "',ObservOther='" + cf.convertsinglequotes(observothertxt)
											+ "'," + "OccupiedPercent='"
											+ stroccup + "',OccupiedFlag='"
											+ occflg + "',Vacant='" + vacflg
											+ "',VacantPercent='" + strvacant
											+ "',NotDetermined='" + nd
											+ "',PermitConfirmed='" + strdata4
											+ "',BuildingSize='" + strbuildsz
											+ "'," + "BalconyPresent='"
											+ strdata5
											+ "',AdditionalStructure='"
											+ strdata6 + "',OtherLocation='"
											+ cf.convertsinglequotes(strother)
											+ "' WHERE Srid ='" + cf.Homeid
											+ "'");
									cf.ShowToast("Saved suceesfully.",1);
									dialog.dismiss();
								} else if ((o + va) < 100) {
									cf.ShowToast("Building Occupancy Percentage should be equal to 100%.",1);
								} else {
									cf.ShowToast("Building Occupancy Percentage should not be greater than 100%.",1);
								}

							}

						} else {System.out.println("its 2222");
							cf.db.execSQL("UPDATE " + cf.HazardDataTable
									+ " SET InsuredIs='" + strdata1
									+ "',InsuredOther='" + cf.convertsinglequotes(insuredothertxt)
									+ "',OccupancyType='" + strdata3
									+ "',OcccupOther='" + cf.convertsinglequotes(occupancyothertxt)
									+ "',ObservationType='" + strdata2 
									+ "',ObservOther='" + cf.convertsinglequotes(observothertxt)
									+ "',OccupiedPercent='" + stroccup
									+ "',OccupiedFlag='" + occflg
									+ "',Vacant='" + vacflg
									+ "',VacantPercent='" + strvacant
									+ "',NotDetermined='" + nd
									+ "',PermitConfirmed='" + strdata4
									+ "',BuildingSize='" + strbuildsz + "',"
									+ "BalconyPresent='" + strdata5
									+ "',AdditionalStructure='" + strdata6
									+ "',OtherLocation='"
									+ cf.convertsinglequotes(strother)
									+ "' WHERE Srid ='" + cf.Homeid + "'");
							cf.ShowToast("Saved suceesfully.",1);
							dialog.dismiss();
						}
					} else {
						cf.ShowToast("Please select Building Occupancy.",1);

					}
					} else {
						cf.ShowToast("Please enter the other text for occupancy type.",1);
					}
  				   } else {
						cf.ShowToast("Please enter the other text for observation type.",1);

  				   }
					}else{
						cf.ShowToast("Please enter the other text for insured is.",1);
					}
				}

			}

		});

		dialog.show();
	}

	public class MyOnItemSelectedListenerdata implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata1 = parent.getItemAtPosition(pos).toString();
			data1flg = parent.getSelectedItemPosition();
			if(strdata1.equals("Other"))
			{
				etinsuredother.setVisibility(view.VISIBLE);
				insuredothertxt=etinsuredother.getText().toString();
			}
			else
			{
				etinsuredother.setVisibility(view.GONE);
				//etinsuredother.setText("");
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata1 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata2 = parent.getItemAtPosition(pos).toString();
			data2flg = parent.getSelectedItemPosition();
			if(strdata2.equals("Other"))
			{
				etobservother.setVisibility(view.VISIBLE);
				observothertxt=etobservother.getText().toString();
			}
			else
			{
				etobservother.setVisibility(view.GONE);
				//etobservother.setText("");
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata2 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata3 = parent.getItemAtPosition(pos).toString();
			data3flg = parent.getSelectedItemPosition();
			if(strdata3.equals("Other"))
			{
				etoccupancyother.setVisibility(view.VISIBLE);
				occupancyothertxt=etoccupancyother.getText().toString();
			}
			else
			{
				etoccupancyother.setVisibility(view.GONE);
				//etoccupancyother.setText("");
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata3 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata4 = parent.getItemAtPosition(pos).toString();
			data4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata4 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata5 = parent.getItemAtPosition(pos).toString();
			data5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata5 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata6 = parent.getItemAtPosition(pos).toString();
			data6flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void location() {
		final Dialog dialog = new Dialog(GeneralConditions.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("LOCATION");
		dialog.setCancelable(true);

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		s.setOnItemSelectedListener(new MyOnItemSelectedListener());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListener1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListener2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListener3());
		selrow();
		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.HazardDataTable
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','"
							+ strloc1
							+ "','"
							+ strloc2
							+ "','"
							+ strloc3
							+ "','"
							+ strloc4
							+ "',"
							+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c2.getCount();
						if (subchkrws == 0) {

						} else {

						}
					} catch (Exception e) {

					}

				} else {
					cf.db.execSQL("UPDATE " + cf.HazardDataTable
							+ " SET Location1='" + strloc1 + "',Location2='"
							+ strloc2 + "',Location3='" + strloc3
							+ "',Location4='" + strloc4 + "'WHERE Srid ='"
							+ cf.Homeid + "'");

				}
				cf.ShowToast("Saved suceesfully.",1);
				changeimage();
				dialog.dismiss();
			}

		});

		if (rws > 0) {
			retrievedata();

			int spinnerPosition = adapter.getPosition(strloc1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strloc2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strloc3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strloc4);
			s3.setSelection(spinnerPosition3);

		}
		dialog.show();
	}

	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc1 = parent.getItemAtPosition(pos).toString();
			loc1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc2 = parent.getItemAtPosition(pos).toString();
			loc2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc3 = parent.getItemAtPosition(pos).toString();
			loc3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc4 = parent.getItemAtPosition(pos).toString();
			loc4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void poolfence() {
		final Dialog dialog = new Dialog(GeneralConditions.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("PERIMETER POOL FENCE");
		dialog.setCancelable(true);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Perimeter Pool Fence");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Self Latches / locks to Gate");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Professionally Installed");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Pool Fence in Disrepair");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);

		s.setOnItemSelectedListener(new MyOnItemSelectedListenerpf());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerpf1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerpf2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerpf3());
		selrow();
		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.HazardDataTable
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','','','','','','','','','',"
							+ "'"
							+ strpf1
							+ "','"
							+ strpf4
							+ "','"
							+ strpf2
							+ "','"
							+ strpf3
							+ "','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c2.getCount();
						if (subchkrws == 0) {

						} else {

						}
					} catch (Exception e) {

					}

				} else {
					cf.db.execSQL("UPDATE " + cf.HazardDataTable
							+ " SET PerimeterPoolFence='" + strpf1
							+ "',PoolFenceDisrepair='" + strpf4 + "',"
							+ "SelfLatch='" + strpf2 + "',ProfesInstall='"
							+ strpf3 + "' WHERE Srid ='" + cf.Homeid + "'");

				}
				cf.ShowToast("Saved suceesfully.",1);
				changeimage();
				dialog.dismiss();
			}

		});
		if (rws > 0) {
			retrievedata();
			int spinnerPosition = adapter.getPosition(strpf1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strpf2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strpf3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strpf4);
			s3.setSelection(spinnerPosition3);

		}
		dialog.show();
	}

	public class MyOnItemSelectedListenerpf implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf1 = parent.getItemAtPosition(pos).toString();
			pf1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpf1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf2 = parent.getItemAtPosition(pos).toString();
			pf2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpf2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf3 = parent.getItemAtPosition(pos).toString();
			pf3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpf3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf4 = parent.getItemAtPosition(pos).toString();
			pf4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void observation() {
		final Dialog dialog = new Dialog(GeneralConditions.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("OBSERVATION");
		dialog.setCancelable(true);
		LinearLayout lin1 = (LinearLayout) dialog.findViewById(R.id.lin05);
		lin1.setVisibility(visibility);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Construction originally intended for current use");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Alterations / Remodeling noted since original construction");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Description");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Visible non-standard / home made / do it yourself alterations");

		TextView txt5 = (TextView) dialog.findViewById(R.id.text5);
		txt5.setText("Permit information for alterations confirmed");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner05);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter4 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s4.setAdapter(adapter4);

		s.setOnItemSelectedListener(new MyOnItemSelectedListenerob());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerob1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerob2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerob3());
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerob4());
		selrow();

		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.HazardDataTable
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','','','','',"
							+ "'"
							+ strob1
							+ "','"
							+ strob2
							+ "','"
							+ strob3
							+ "','"
							+ strob4
							+ "','"
							+ strob5
							+ "','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c2.getCount();
						if (subchkrws == 0) {

						} else {

						}
					} catch (Exception e) {

					}

				} else {
					cf.db.execSQL("UPDATE " + cf.HazardDataTable
							+ " SET Observation1='" + strob1
							+ "',Observation2='" + strob2 + "',"
							+ "Observation3='" + strob3 + "',Observation4='"
							+ strob4 + "',Observation5='" + strob5
							+ "' WHERE Srid ='" + cf.Homeid + "'");

				}
				cf.ShowToast("Saved suceesfully.",1);
				changeimage();
				dialog.dismiss();
			}

		});

		if (rws > 0) {
			retrievedata();
			int spinnerPosition = adapter.getPosition(strob1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strob2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strob3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strob4);
			s3.setSelection(spinnerPosition3);
			int spinnerPosition4 = adapter4.getPosition(strob5);
			s4.setSelection(spinnerPosition4);

		}
		dialog.show();
	}

	public class MyOnItemSelectedListenerob implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob1 = parent.getItemAtPosition(pos).toString();
			obs1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob2 = parent.getItemAtPosition(pos).toString();
			obs2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob3 = parent.getItemAtPosition(pos).toString();
			obs3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob4 = parent.getItemAtPosition(pos).toString();
			obs4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob5 = parent.getItemAtPosition(pos).toString();
			obs5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void poolpresent() {
		final Dialog dialog = new Dialog(GeneralConditions.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("POOL PRESENT");
		dialog.setCancelable(true);
		LinearLayout lin1 = (LinearLayout) dialog.findViewById(R.id.lin05);
		lin1.setVisibility(visibility);

		LinearLayout lin2 = (LinearLayout) dialog.findViewById(R.id.lin06);
		lin2.setVisibility(visibility);

		LinearLayout lin3 = (LinearLayout) dialog.findViewById(R.id.lin07);
		lin3.setVisibility(visibility);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Pool Present");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Hot tub Present");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Empty in Ground Pool Present");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Pool Slide present");

		TextView txt5 = (TextView) dialog.findViewById(R.id.text5);
		txt5.setText("Diving board present");

		TextView txt6 = (TextView) dialog.findViewById(R.id.text6);
		txt6.setText("Perimeter Pool Enclosure");

		TextView txt7 = (TextView) dialog.findViewById(R.id.text7);
		txt7.setText("Pool enclosure in disrepair");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner05);
		Spinner s5 = (Spinner) dialog.findViewById(R.id.Spinner06);
		Spinner s6 = (Spinner) dialog.findViewById(R.id.Spinner07);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter4 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter5 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter6 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s4.setAdapter(adapter4);
		adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s5.setAdapter(adapter5);
		adapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s6.setAdapter(adapter6);

		s.setOnItemSelectedListener(new MyOnItemSelectedListenerpp());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerpp1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerpp2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerpp3());
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerpp4());
		s5.setOnItemSelectedListener(new MyOnItemSelectedListenerpp5());
		s6.setOnItemSelectedListener(new MyOnItemSelectedListenerpp6());
		selrow();
		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.HazardDataTable
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','','','','','','','','','',"
							+ "'','','','','"
							+ strpp1
							+ "','"
							+ strpp2
							+ "','"
							+ strpp3
							+ "','"
							+ strpp4
							+ "','"
							+ strpp5
							+ "','"
							+ strpp6
							+ "','"
							+ strpp7
							+ "','','','','','','','','','','','','','','','','','','','','','','','','','','')");
					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c2.getCount();
						if (subchkrws == 0) {

						} else {

						}
					} catch (Exception e) {

					}

				} else {
					cf.db.execSQL("UPDATE " + cf.HazardDataTable
							+ " SET PoolPresent='" + strpp1
							+ "',HotTubPresnt='" + strpp2
							+ "',EmptyGroundPool='" + strpp3 + "',"
							+ "PoolSlide='" + strpp4
							+ "',DivingBoardPoolSlide='" + strpp5
							+ "',PerimeterPoolEncl='" + strpp6
							+ "',PoolEnclosure='" + strpp7 + "' WHERE Srid ='"
							+ cf.Homeid + "'");

				}
				cf.ShowToast("Saved suceesfully.",1);
				changeimage();
				dialog.dismiss();
			}

		});

		if (rws > 0) {
			retrievedata();
			int spinnerPosition = adapter.getPosition(strpp1);
			int spinnerPosition1 = adapter1.getPosition(strpp2);
			int spinnerPosition2 = adapter2.getPosition(strpp3);
			int spinnerPosition3 = adapter3.getPosition(strpp4);
			int spinnerPosition4 = adapter4.getPosition(strpp5);
			int spinnerPosition5 = adapter5.getPosition(strpp6);
			int spinnerPosition6 = adapter6.getPosition(strpp7);

			s.setSelection(spinnerPosition);
			s1.setSelection(spinnerPosition1);
			s2.setSelection(spinnerPosition2);
			s3.setSelection(spinnerPosition3);
			s4.setSelection(spinnerPosition4);
			s5.setSelection(spinnerPosition5);
			s6.setSelection(spinnerPosition6);

		}
		dialog.show();
	}

	public class MyOnItemSelectedListenerpp implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp1 = parent.getItemAtPosition(pos).toString();
			pp1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp2 = parent.getItemAtPosition(pos).toString();
			pp2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp3 = parent.getItemAtPosition(pos).toString();
			pp3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp4 = parent.getItemAtPosition(pos).toString();
			pp4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp5 = parent.getItemAtPosition(pos).toString();
			pp5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp5 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp6 = parent.getItemAtPosition(pos).toString();
			pp6flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp6 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp7 = parent.getItemAtPosition(pos).toString();
			pp7flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void hazard() {

		final Dialog dialog = new Dialog(GeneralConditions.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("SUMMARY OF HAZARDS");
		dialog.setCancelable(true);
		LinearLayout lin1 = (LinearLayout) dialog.findViewById(R.id.lin05);
		lin1.setVisibility(visibility);

		LinearLayout lin2 = (LinearLayout) dialog.findViewById(R.id.lin06);
		lin2.setVisibility(visibility);

		LinearLayout lin3 = (LinearLayout) dialog.findViewById(R.id.lin07);
		lin3.setVisibility(visibility);

		LinearLayout lin4 = (LinearLayout) dialog.findViewById(R.id.lin08);

		LinearLayout lin5 = (LinearLayout) dialog.findViewById(R.id.lin09);
		lin5.setVisibility(visibility);

		LinearLayout lin6 = (LinearLayout) dialog.findViewById(R.id.lin010);
		lin6.setVisibility(visibility);

		LinearLayout lin7 = (LinearLayout) dialog.findViewById(R.id.lin011);
		lin7.setVisibility(visibility);

		LinearLayout lin8 = (LinearLayout) dialog.findViewById(R.id.lin012);
		lin8.setVisibility(visibility);

		LinearLayout lin9 = (LinearLayout) dialog.findViewById(R.id.lin013);
		lin9.setVisibility(visibility);

		LinearLayout lin10 = (LinearLayout) dialog.findViewById(R.id.lin014);
		lin10.setVisibility(visibility);

		LinearLayout lin11 = (LinearLayout) dialog.findViewById(R.id.lin015);
		lin11.setVisibility(visibility);

		LinearLayout lin13 = (LinearLayout) dialog.findViewById(R.id.lin017);
		lin13.setVisibility(visibility);

		LinearLayout lin14 = (LinearLayout) dialog.findViewById(R.id.lin018);
		lin14.setVisibility(visibility);

		LinearLayout lin15 = (LinearLayout) dialog.findViewById(R.id.lin019);
		lin15.setVisibility(visibility);

		LinearLayout lin16 = (LinearLayout) dialog.findViewById(R.id.lin020);
		lin16.setVisibility(visibility);

		LinearLayout lin17 = (LinearLayout) dialog.findViewById(R.id.lin021);
		lin17.setVisibility(visibility);

		LinearLayout lin18 = (LinearLayout) dialog.findViewById(R.id.lin022);
		lin18.setVisibility(visibility);

		LinearLayout lin19 = (LinearLayout) dialog.findViewById(R.id.lin023);
		lin19.setVisibility(visibility);

		LinearLayout lin20 = (LinearLayout) dialog.findViewById(R.id.lin024);
		lin20.setVisibility(visibility);

		LinearLayout lin21 = (LinearLayout) dialog.findViewById(R.id.lin025);
		lin21.setVisibility(visibility);

		LinearLayout lin12 = (LinearLayout) dialog.findViewById(R.id.lin0116);
		lin12.setVisibility(visibility);

		lin081 = (LinearLayout) dialog.findViewById(R.id.lin081);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Vicious / Exotic Pets");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Horses/Livestock for Business On premises");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Over hanging Limbs to Roof");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Trampoline Present");

		TextView txt5 = (TextView) dialog.findViewById(R.id.text5);
		txt5.setText("Skateboard Ramp present");

		TextView txt6 = (TextView) dialog.findViewById(R.id.text6);
		txt6.setText("Bicycle Ramp present");

		TextView txt7 = (TextView) dialog.findViewById(R.id.text7);
		txt7.setText("Trip Hazards Noted");

		TextView txt8 = (TextView) dialog.findViewById(R.id.text81);
		txt8.setText("Trip Hazards Description");

		TextView txt9 = (TextView) dialog.findViewById(R.id.text9);
		txt9.setText("Unsafe Stairway Noted");

		TextView txt10 = (TextView) dialog.findViewById(R.id.text10);
		txt10.setText("Porch/deck >2� off grade (or 3 steps) no hand railings");

		TextView txt11 = (TextView) dialog.findViewById(R.id.text11);
		txt11.setText("Non-Standard construction Noted");

		TextView txt12 = (TextView) dialog.findViewById(R.id.text12);
		txt12.setText("Outdoor Appliances Noted");

		TextView txt13 = (TextView) dialog.findViewById(R.id.text13);
		txt13.setText("Open foundation present");

		TextView txt14 = (TextView) dialog.findViewById(R.id.text14);
		txt14.setText("Wood shingled roofs");

		TextView txt15 = (TextView) dialog.findViewById(R.id.text15);
		txt15.setText("Excess debris / Garbage /Trash");

		TextView txt17 = (TextView) dialog.findViewById(R.id.text17);
		txt17.setText("Building in general Disrepair");

		TextView txt18 = (TextView) dialog.findViewById(R.id.text18);
		txt18.setText("Property Damage Noted");

		TextView txt19 = (TextView) dialog.findViewById(R.id.text19);
		txt19.setText("Structure partially / Entirely under water");

		TextView txt20 = (TextView) dialog.findViewById(R.id.text20);
		txt20.setText("Inoperative / Non-Starting motor vehicle(s) Noted");

		TextView txt21 = (TextView) dialog.findViewById(R.id.text21);
		txt21.setText("Recent drywall repair");

		TextView txt22 = (TextView) dialog.findViewById(R.id.text22);
		txt22.setText("Possible Chinese drywall product");

		TextView txt23 = (TextView) dialog.findViewById(R.id.text23);
		txt23.setText("Owner Confirmed Chinese drywall product present");

		TextView txt24 = (TextView) dialog.findViewById(R.id.text24);
		txt24.setText("Non working security system noted");

		TextView txt25 = (TextView) dialog.findViewById(R.id.text25);
		txt25.setText("Non working smoke dedicator noted");

		TextView txt16 = (TextView) dialog.findViewById(R.id.text16);
		txt16.setText("Business on premises");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner05);
		Spinner s5 = (Spinner) dialog.findViewById(R.id.Spinner06);
		Spinner s6 = (Spinner) dialog.findViewById(R.id.Spinner07);
		s7 = (EditText) dialog.findViewById(R.id.edit08);
		Spinner s8 = (Spinner) dialog.findViewById(R.id.Spinner09);
		Spinner s9 = (Spinner) dialog.findViewById(R.id.Spinner010);
		Spinner s10 = (Spinner) dialog.findViewById(R.id.Spinner011);
		Spinner s11 = (Spinner) dialog.findViewById(R.id.Spinner012);
		Spinner s12 = (Spinner) dialog.findViewById(R.id.Spinner013);
		Spinner s13 = (Spinner) dialog.findViewById(R.id.Spinner014);
		Spinner s14 = (Spinner) dialog.findViewById(R.id.Spinner015);
		Spinner s16 = (Spinner) dialog.findViewById(R.id.Spinner017);
		Spinner s17 = (Spinner) dialog.findViewById(R.id.Spinner018);
		Spinner s18 = (Spinner) dialog.findViewById(R.id.Spinner019);
		Spinner s19 = (Spinner) dialog.findViewById(R.id.Spinner020);
		Spinner s20 = (Spinner) dialog.findViewById(R.id.Spinner021);
		Spinner s21 = (Spinner) dialog.findViewById(R.id.Spinner022);
		Spinner s22 = (Spinner) dialog.findViewById(R.id.Spinner023);
		Spinner s23 = (Spinner) dialog.findViewById(R.id.Spinner024);
		Spinner s24 = (Spinner) dialog.findViewById(R.id.Spinner025);
		Spinner s15 = (Spinner) dialog.findViewById(R.id.Spinner016);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		s.setOnItemSelectedListener(new MyOnItemSelectedListenerhz());
		s1.setAdapter(adapter);
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerhz1());
		s2.setAdapter(adapter);
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerhz2());
		s3.setAdapter(adapter);
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerhz3());
		s4.setAdapter(adapter);
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerhz4());
		s5.setAdapter(adapter);
		s5.setOnItemSelectedListener(new MyOnItemSelectedListenerhz5());
		s6.setAdapter(adapter);
		s6.setOnItemSelectedListener(new MyOnItemSelectedListenerhz6());
		s8.setAdapter(adapter);
		s8.setOnItemSelectedListener(new MyOnItemSelectedListenerhz8());
		s9.setAdapter(adapter);
		s9.setOnItemSelectedListener(new MyOnItemSelectedListenerhz9());
		s10.setAdapter(adapter);
		s10.setOnItemSelectedListener(new MyOnItemSelectedListenerhz10());
		s11.setAdapter(adapter);
		s11.setOnItemSelectedListener(new MyOnItemSelectedListenerhz11());
		s12.setAdapter(adapter);
		s12.setOnItemSelectedListener(new MyOnItemSelectedListenerhz12());
		s13.setAdapter(adapter);
		s13.setOnItemSelectedListener(new MyOnItemSelectedListenerhz13());
		s14.setAdapter(adapter);
		s14.setOnItemSelectedListener(new MyOnItemSelectedListenerhz14());
		s15.setAdapter(adapter);
		s15.setOnItemSelectedListener(new MyOnItemSelectedListenerhz15());
		s17.setAdapter(adapter);
		s17.setOnItemSelectedListener(new MyOnItemSelectedListenerhz17());
		s18.setAdapter(adapter);
		s18.setOnItemSelectedListener(new MyOnItemSelectedListenerhz18());
		s19.setAdapter(adapter);
		s19.setOnItemSelectedListener(new MyOnItemSelectedListenerhz19());
		s20.setAdapter(adapter);
		s20.setOnItemSelectedListener(new MyOnItemSelectedListenerhz20());
		s21.setAdapter(adapter);
		s21.setOnItemSelectedListener(new MyOnItemSelectedListenerhz21());
		s22.setAdapter(adapter);
		s22.setOnItemSelectedListener(new MyOnItemSelectedListenerhz22());
		s23.setAdapter(adapter);
		s23.setOnItemSelectedListener(new MyOnItemSelectedListenerhz23());
		s24.setAdapter(adapter);
		s24.setOnItemSelectedListener(new MyOnItemSelectedListenerhz24());
		s16.setAdapter(adapter);
		s16.setOnItemSelectedListener(new MyOnItemSelectedListenerhz16());

		selrow();

		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					strhz8 = s7.getText().toString();
					cf.db.execSQL("INSERT INTO "
							+ cf.HazardDataTable
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('" + cf.Homeid + "','','','','','" + 0
							+ "','" + 0 + "','','" + 0 + "','','" + 0
							+ "','','','','','','','','','','','','',"
							+ "'','','','','','','','','','',''," + "'"
							+ strhz1 + "','" + strhz2 + "','" + strhz3 + "','"
							+ strhz4 + "','" + strhz5 + "','" + strhz6 + "','"
							+ cf.convertsinglequotes(strhz8) + "','" + strhz7
							+ "','" + strhz9 + "'," + "'" + strhz10 + "','"
							+ strhz11 + "','" + strhz12 + "','" + strhz13
							+ "','" + strhz14 + "','" + strhz15 + "','"
							+ strhz16 + "','" + strhz17 + "','" + strhz18
							+ "'," + "'" + strhz19 + "','" + strhz20 + "','"
							+ strhz21 + "','" + strhz22 + "','" + strhz23
							+ "','" + strhz24 + "','" + strhz25 + "','')");
					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c2.getCount();
						if (subchkrws == 0) {

						} else {

						}
					} catch (Exception e) {

					}

				} else {
					strhz8 = s7.getText().toString();System.out.println("its 33333");
					cf.db.execSQL("UPDATE " + cf.HazardDataTable
							+ " SET Vicious='" + strhz1 + "',LiveStock='"
							+ strhz2 + "',OverHanging='" + strhz3
							+ "',Trampoline='" + strhz4 + "',SkateBoard='"
							+ strhz5 + "'," + "bicycle='" + strhz6
							+ "',TripHazardDesc='"
							+ cf.convertsinglequotes(strhz8)
							+ "',TripHazardNoted='" + strhz7
							+ "',UnsafeStairway='" + strhz9
							+ "',PorchAndDeck='" + strhz10 + "',"
							+ "NonStdConstruction='" + strhz11
							+ "',OutDoorAppliances='" + strhz12
							+ "',OpenFoundation='" + strhz13
							+ "',WoodShingled='" + strhz14 + "',"
							+ "ExcessDebris='" + strhz15
							+ "',BusinessPremises='" + strhz16
							+ "',GeneralDisrepair='" + strhz17
							+ "',PropertyDamage='" + strhz18 + "',"
							+ "StructurePartial='" + strhz19
							+ "',InOperative='" + strhz20 + "',RecentDrywall='"
							+ strhz21 + "',ChineseDrywall='" + strhz22 + "',"
							+ "ConfirmDrywall='" + strhz23 + "',NonSecurity='"
							+ strhz24 + "',NonSmoke='" + strhz25
							+ "' WHERE Srid ='" + cf.Homeid + "'");

				}
				cf.ShowToast("Saved suceesfully.",1);
				changeimage();
				dialog.dismiss();
			}

		});
		if (rws > 0) {
			retrievedata();

			int sp = adapter.getPosition(strhz1);
			int sp1 = adapter.getPosition(strhz2);
			int sp2 = adapter.getPosition(strhz3);
			int sp3 = adapter.getPosition(strhz4);
			int sp4 = adapter.getPosition(strhz5);
			int sp5 = adapter.getPosition(strhz6);
			int sp6 = adapter.getPosition(strhz7);
			int sp8 = adapter.getPosition(strhz9);
			int sp9 = adapter.getPosition(strhz10);
			int sp10 = adapter.getPosition(strhz11);
			int sp11 = adapter.getPosition(strhz12);
			int sp12 = adapter.getPosition(strhz13);
			int sp13 = adapter.getPosition(strhz14);
			int sp14 = adapter.getPosition(strhz15);
			int sp15 = adapter.getPosition(strhz16);
			int sp16 = adapter.getPosition(strhz17);
			int sp17 = adapter.getPosition(strhz18);
			int sp18 = adapter.getPosition(strhz19);
			int sp19 = adapter.getPosition(strhz20);
			int sp20 = adapter.getPosition(strhz21);
			int sp21 = adapter.getPosition(strhz22);
			int sp22 = adapter.getPosition(strhz23);
			int sp23 = adapter.getPosition(strhz24);
			int sp24 = adapter.getPosition(strhz25);
			s.setSelection(sp);
			s1.setSelection(sp1);
			s2.setSelection(sp2);
			s3.setSelection(sp3);
			s4.setSelection(sp4);
			s5.setSelection(sp5);
			s6.setSelection(sp6);
			if (strhz7.equals("Yes")) {
				lin081.setVisibility(visibility);
				s7.setText(cf.getsinglequotes(strhz8));
			}

			s8.setSelection(sp8);
			s9.setSelection(sp9);
			s10.setSelection(sp10);
			s11.setSelection(sp11);
			s12.setSelection(sp12);
			s13.setSelection(sp13);
			s14.setSelection(sp14);
			s15.setSelection(sp15);
			s16.setSelection(sp16);
			s17.setSelection(sp17);
			s18.setSelection(sp18);
			s19.setSelection(sp19);
			s20.setSelection(sp20);
			s21.setSelection(sp21);
			s22.setSelection(sp22);
			s23.setSelection(sp23);
			s24.setSelection(sp24);

		}
		dialog.show();
	}

	public class MyOnItemSelectedListenerhz implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz1 = parent.getItemAtPosition(pos).toString();
			hz1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz2 = parent.getItemAtPosition(pos).toString();
			hz2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz3 = parent.getItemAtPosition(pos).toString();
			hz3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz4 = parent.getItemAtPosition(pos).toString();
			hz4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz5 = parent.getItemAtPosition(pos).toString();
			hz5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz5 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz6 = parent.getItemAtPosition(pos).toString();
			hz6flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz6 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz7 = parent.getItemAtPosition(pos).toString();
			hz7flg = parent.getSelectedItemPosition();
			if (strhz7.equals("Yes")) {
				lin081.setVisibility(visibility);
			} else {
				lin081.setVisibility(view.GONE);
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz8 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz9 = parent.getItemAtPosition(pos).toString();
			hz9flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz9 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz10 = parent.getItemAtPosition(pos).toString();
			hz10flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz10 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz11 = parent.getItemAtPosition(pos).toString();
			hz11flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz11 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz12 = parent.getItemAtPosition(pos).toString();
			hz12flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz12 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz13 = parent.getItemAtPosition(pos).toString();
			hz13flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz13 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz14 = parent.getItemAtPosition(pos).toString();
			hz14flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz14 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz15 = parent.getItemAtPosition(pos).toString();
			hz15flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz15 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz16 = parent.getItemAtPosition(pos).toString();
			hz16flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz16 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz17 = parent.getItemAtPosition(pos).toString();
			hz17flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz17 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz18 = parent.getItemAtPosition(pos).toString();
			hz18flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz18 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz19 = parent.getItemAtPosition(pos).toString();
			hz19flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz19 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz20 = parent.getItemAtPosition(pos).toString();
			hz20flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz20 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz21 = parent.getItemAtPosition(pos).toString();
			hz21flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz21 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz22 = parent.getItemAtPosition(pos).toString();
			hz22flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz22 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz23 = parent.getItemAtPosition(pos).toString();
			hz23flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz23 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz24 = parent.getItemAtPosition(pos).toString();
			hz24flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz24 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz25 = parent.getItemAtPosition(pos).toString();
			hz25flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.hme1:
			cf.gohome();
			break;
		case R.id.save:

			if (b == 0) {
				comentfill();
				cf.ShowToast("Please click above tabs before save.",1);

			} else {
				comentfill();
				try {
					Cursor c2 = cf.db.rawQuery("SELECT * FROM "
							+ cf.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
							+ "'", null);
					int subchkrws = c2.getCount();
					if (subchkrws == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.SubmitCheckTable
								+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
								+ "VALUES ('" + cf.InspectorId + "','"
								+ cf.Homeid
								+ "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1)");
					} else {
						cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
								+ " SET fld_hazarddata='1' WHERE Srid ='"
								+ cf.Homeid + "' and InspectorId='"
								+ cf.InspectorId + "'");
					}
				} catch (Exception e) {

				}
				cf.ShowToast("General Hazard Data saved sucessfully.",1);

			}
			break;
		case R.id.data:
			break;
		case R.id.additional:
			Intent genimg = new Intent(GeneralConditions.this,
					GeneralImages.class);
			genimg.putExtra("homeid", cf.Homeid);
			genimg.putExtra("InspectionType", cf.InspectionType);
			genimg.putExtra("status", cf.status);
			genimg.putExtra("keyName", cf.value);
			genimg.putExtra("Count", cf.Count);
			startActivity(genimg);
			break;

		}
	}

	private void comentfill() {
		// TODO Auto-generated method stub
		if (!etcomments.getText().toString().equals("")) {
			selrow();
			if (rws == 0) {
				cf.db.execSQL("INSERT INTO "
						+ cf.HazardDataTable
						+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
						+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
						+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
						+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
						+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
						+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
						+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
						+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
						+ " VALUES ('"
						+ cf.Homeid
						+ "','','','','','"
						+ 0
						+ "','"
						+ 0
						+ "','','"
						+ 0
						+ "','','"
						+ 0
						+ "','','','','','','','',"
						+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','"
						+ cf.convertsinglequotes(etcomments.getText()
								.toString()) + "')");

			} else {
				cf.db.execSQL("UPDATE "
						+ cf.HazardDataTable
						+ " SET Comments='"
						+ cf.convertsinglequotes(etcomments.getText()
								.toString()) + "' WHERE Srid ='" + cf.Homeid
						+ "'");

			}
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(GeneralConditions.this,
					InspectionList.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	    if(focus==1)
	    {
	    	focus=0;
	    	if(general_lin.getVisibility()==View.VISIBLE){
	    		etcomments.setText(etcomments.getText().toString());
	    	}
	    }
	 }     
}