package idsoft.inspectiondepot.B11802;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SearchInspection extends Activity {
	String fstname, lstname, pn;
	TextView welcome;
	Button homepage, search;
	CommonFunctions cf;
	LinearLayout lst;
	String[] data;
	ScrollView sv;
	String dta = "";
	int Cnt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getExtras();
		if (b != null) {
			fstname = b.getString("first");
			lstname = b.getString("last");
			pn = b.getString("policy");

		}
		cf = new CommonFunctions(this);
		setContentView(R.layout.searchinspection);
		
		this.search = (Button) this.findViewById(R.id.srch);
		lst = (LinearLayout) findViewById(R.id.linlayoutdyn);
		cf.getInspectorId();
		cf.welcomemessage();
		
		((TextView)findViewById(R.id.welcomename)).setText(Html.fromHtml("<font color=#f7a218>" + "Welcome : "+"</font>"+ cf.data));
		
		fetchdata();
		this.search.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				
				final EditText tvxtstname;
				final EditText tvlstname;
				final EditText tvploicy;
				final Dialog dialog = new Dialog(SearchInspection.this);
				dialog.setContentView(R.layout.search);
				dialog.setTitle(Html.fromHtml("<font color=yellow>Internet connection is required to search online.</font>"));
				dialog.setCancelable(true);
				tvxtstname = (EditText) dialog.findViewById(R.id.txtfirstname);
				tvlstname = (EditText) dialog.findViewById(R.id.txtlastname);
				tvploicy = (EditText) dialog.findViewById(R.id.txtpolicy);
				tvxtstname.requestFocusFromTouch();
				Button btnsrch = (Button) dialog
						.findViewById(R.id.search);
				btnsrch.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						cf.hidekeyboard();
						Cnt = 0;
						lst.removeAllViews();
						dta = "";
						if (tvxtstname.getText().toString().equals("")
								&& tvlstname.getText().toString().equals("")
								&& tvploicy.getText().toString().equals("")) {
							cf.ShowToast("Please enter any one of the fields to search.",1);
						} else {
							fstname = tvxtstname.getText().toString();
							lstname = tvlstname.getText().toString();
							pn = tvploicy.getText().toString();
							ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
							NetworkInfo info = conMgr.getActiveNetworkInfo();
							if (info != null && info.isConnected()) {
								fetchdata();
								dialog.cancel();
							} else {
								cf.ShowToast("Internet connection is not available.",1);
							}
						}

					}

				});
				Button btncls = (Button) dialog.findViewById(R.id.cls);
				btncls.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						/* InputMethodManager imm = (InputMethodManager)getSystemService(
							      Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(tvxtstname.getWindowToken(), 0);
							imm.hideSoftInputFromWindow(tvlstname.getWindowToken(), 0);
							imm.hideSoftInputFromWindow(tvploicy.getWindowToken(), 0);*/
						dialog.dismiss();
					}

				});
				dialog.show();

			}
		});

	
	
	}

	private void fetchdata() {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME25);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID", cf.InspectorId.toString());
		request.addProperty("OwnerFirstName", fstname);
		request.addProperty("OwnerLastName", lstname);
		request.addProperty("OwnerPolicyNumber", pn);
		envelope.setOutputSoapObject(request);
		System.out.println("REQUEt "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		try {
			androidHttpTransport.call(cf.SOAP_ACTION25, envelope);
			SoapObject result = (SoapObject) envelope.getResponse();System.out.println("result "+result);
			if (result != null) {
				if (result.toString().equals("null")
						|| result.toString().equals("")) {
					cf.ShowToast("Sorry, No record found for your search criteria.",1);

				} else {
					Cnt = result.getPropertyCount();

					String[] data = new String[Cnt];

					for (int j = 0; j < Cnt; j++) {
						SoapObject result1 = (SoapObject) result.getProperty(j);

						try {

							dta += " "
									+ result1.getProperty("FirstName")
											.toString() + " ";
							dta += result1.getProperty("LastName").toString()
									+ " | ";
							dta += result1.getProperty("PolicyNumber")
									.toString() + " \n ";
							dta += result1.getProperty("Address1").toString()
									+ " | ";
							dta += result1.getProperty("City").toString()
									+ " | ";
							dta += result1.getProperty("State").toString()
									+ " | ";
							dta += result1.getProperty("County").toString()
									+ " | ";
							dta += result1.getProperty("StatusName").toString()
									+ "~";
						} catch (Exception e) {

						}
					}
					display();
				}
			} else {
				cf.ShowToast("Sorry, No records found for your search criteria.",1);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void display() {
		// TODO Auto-generated method stub
		lst.removeAllViews();
		sv = new ScrollView(this);
		lst.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		this.data = dta.split("~");
		for (int i = 0; i < data.length; i++) {
			TextView[] tvstatus = new TextView[Cnt];

			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l1.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					900, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 8;
			paramschk.leftMargin = 20;
			paramschk.bottomMargin = 10;
			l2.addView(lchkbox);

			tvstatus[i] = new TextView(this);
			tvstatus[i].setMinimumWidth(580);
			tvstatus[i].setMaxWidth(580);
			tvstatus[i].setTag("textbtn" + i);
			tvstatus[i].setText(data[i]);
			tvstatus[i].setTextColor(Color.WHITE);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			lchkbox.addView(tvstatus[i], paramschk);

			LinearLayout ldelbtn = new LinearLayout(this);
			LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
					93, 37);
			paramsdelbtn.rightMargin = 10;
			paramsdelbtn.bottomMargin = 10;
			l2.addView(ldelbtn);

		}

	}
	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.deletehome:
		cf.gohome();
			break;
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.gohome();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}