package idsoft.inspectiondepot.B11802;


import android.app.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.os.Bundle;

import android.os.Message;
import android.provider.MediaStore;

import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AgentInfo extends Activity {
	String homeid, InspectionType, status;
	int value, Count;
	public CommonFunctions cf;
	TextView policyholderinfo;

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(4);
		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		setContentView(R.layout.agent);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.getDeviceDimensions();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 1, cf, 0));
		/** Ph submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 12,
				cf, 1));

		policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		cf.setRcvalue(rccode);
		try {
			Cursor cur = cf.SelectTablefunction(cf.mbtblInspectionList,
					"where s_SRID='" + cf.Homeid + "'");

			int rws1 = cur.getCount();
			cur.moveToFirst();
			String ownerfirstname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameFirst")));
			String ownerlastname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameLast")));
			String policyno = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnerPolicyNumber")));
			policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "
					+ ownerlastname + "<br/><font color=#bddb00>(Policy No : "
					+ policyno + ")</font>"));

		} catch (Exception e) {
			System.out.println(" errrr " + e.getMessage());
		}
		try {
			Cursor cur = cf.SelectTablefunction(cf.agentinfo, "where Homeid='"
					+ cf.Homeid + "'");
			int rws = cur.getCount();
			cur.moveToFirst();
			if (cur != null) {
				do {
					TextView tvname = (TextView) findViewById(R.id.efirstname);
					if (cur.getString(cur.getColumnIndex("agentname")).equals(
							"N/A")
							|| cur.getString(cur.getColumnIndex("agentname"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("agentname"))
									.equals("anyType{}")) {
						tvname.setText("Not Available");
					} else {
						tvname.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("agentname"))));
					}

					TextView tvaddress = (TextView) findViewById(R.id.txtaddr1);
					if (cur.getString(cur.getColumnIndex("address1")).equals(
							"N/A")
							|| cur.getString(cur.getColumnIndex("address1"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("address1"))
									.equals("anyType{}")) {
						tvaddress.setText("Not Available");
					} else {
						tvaddress.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("address1"))));
					}

					TextView tvaddress2 = (TextView) findViewById(R.id.txtaddr2);
					if (cur.getString(cur.getColumnIndex("address2")).equals(
							"N/A")
							|| cur.getString(cur.getColumnIndex("address2"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("address2"))
									.equals("anyType{}")) {
						tvaddress2.setText("Not Available");
					} else {
						tvaddress2.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("address2"))));
					}

					TextView tvcity = (TextView) findViewById(R.id.txtcity);
					if (cur.getString(cur.getColumnIndex("city")).equals("N/A")
							|| cur.getString(cur.getColumnIndex("city"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("city"))
									.equals("anyType{}")) {
						tvcity.setText("Not Available");
					} else {
						tvcity.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("city"))));
					}

					TextView tvcountry = (TextView) findViewById(R.id.txtcountry);
					if (cur.getString(cur.getColumnIndex("country")).equals(
							"N/A")
							|| cur.getString(cur.getColumnIndex("country"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("country"))
									.equals("anyType{}")) {
						tvcountry.setText("Not Available");
					} else {

						tvcountry.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("country"))));
					}

					TextView tvrole = (TextView) findViewById(R.id.econtactperson);
					
					if (cur.getString(cur.getColumnIndex("role")).equals("N/A")
							|| cur.getString(cur.getColumnIndex("role"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("role")).trim()
									.equals("")
							|| cur.getString(cur.getColumnIndex("role")).equals("anyType{}")) {
						tvrole.setText("Not Available");
					} else {

						tvrole.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("role"))));
					}

					TextView tvstate = (TextView) findViewById(R.id.txtstate);
					if (cur.getString(cur.getColumnIndex("state"))
							.equals("N/A")
							|| cur.getString(cur.getColumnIndex("state"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("state"))
									.equals("anyType{}")) {
						tvstate.setText("Not Available");
					} else {

						tvstate.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("state"))));
					}

					TextView tvzip = (TextView) findViewById(R.id.txtzip);

					if (cur.getString(cur.getColumnIndex("zip")).equals("N/A")
							|| cur.getString(cur.getColumnIndex("zip")).equals(
									"Null")
							|| cur.getString(cur.getColumnIndex("zip")).equals(
									"anyType{}")) {
						tvzip.setText("Not Available");
					} else {
						tvzip.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("zip"))));
					}

					TextView tvoffphn = (TextView) findViewById(R.id.txtoffphn);
					if (cur.getString(cur.getColumnIndex("offphone")).equals(
							"N/A")
							|| cur.getString(cur.getColumnIndex("offphone"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("offphone"))
									.equals("anyType{}")) {
						tvoffphn.setText("Not Available");
					} else {
						tvoffphn.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("offphone"))));
					}

					TextView tvconphn = (TextView) findViewById(R.id.txtconphn);
					if (cur.getString(cur.getColumnIndex("contactphone"))
							.equals("N/A")
							|| cur.getString(cur.getColumnIndex("contactphone"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("contactphone"))
									.equals("anyType{}")) {
						tvconphn.setText("Not Available");
					} else {
						tvconphn.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("contactphone"))));
					}

					TextView tvfax = (TextView) findViewById(R.id.txtfax);
					if (cur.getString(cur.getColumnIndex("fax")).equals("N/A")
							|| cur.getString(cur.getColumnIndex("fax")).equals(
									"Null")
							|| cur.getString(cur.getColumnIndex("fax")).equals(
									"anyType{}")) {
						tvfax.setText("Not Available");
					} else {
						tvfax.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("fax"))));
					}

					TextView tvmail = (TextView) findViewById(R.id.txtmail);
					if (cur.getString(cur.getColumnIndex("email"))
							.equals("N/A")
							|| cur.getString(cur.getColumnIndex("email"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("email"))
									.equals("anyType{}")) {
						tvmail.setText("Not Available");
					} else {
						tvmail.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("email"))));
					}

					TextView tvweb = (TextView) findViewById(R.id.txtweb);
					if (cur.getString(cur.getColumnIndex("website")).equals(
							"N/A")
							|| cur.getString(cur.getColumnIndex("website"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("website"))
									.equals("anyType{}")) {
						tvweb.setText("Not Available");
					} else {
						tvweb.setText(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("website"))));
					}
					TextView tvagncyname = (TextView) findViewById(R.id.txtagncyname);
					if (cur.getString(cur.getColumnIndex("agencyname")).equals(
							"N/A")
							|| cur.getString(cur.getColumnIndex("agencyname"))
									.equals("Null")
							|| cur.getString(cur.getColumnIndex("agencyname"))
									.equals("anyType{}")) {
						tvagncyname.setText("Not Available");
					} else {
						tvagncyname.setText(cf.getsinglequotes(cur
								.getString(cur.getColumnIndex("agencyname"))));
					}
				} while (cur.moveToNext());
			}
		} catch (Exception e) {
		}

	}
	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.hme:
		cf.gohome();
			break;
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(AgentInfo.this, PersonalInfo.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
					
				}
				
				break;

		}

	}
}
