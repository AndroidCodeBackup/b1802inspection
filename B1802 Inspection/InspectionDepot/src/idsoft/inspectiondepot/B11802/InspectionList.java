package idsoft.inspectiondepot.B11802;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import java.util.Locale;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class InspectionList extends Activity {
	ScrollView sv;Button deleteallinspections;
	String alerttitle="";
	LinearLayout inspectionlist;
	String countarr[],data[];
	String colomnname="";
	String[] homelist = new String[100];
	int i = 1, value = 0, Count = 0, cnt, columnvalue, rws = 0;
	Cursor cur;
	String statusofdata, InspectionType, status = "", keyword, statuschk,
			query, strtype, strtit,dta = " ",s = "";
	private Button homepage, search, search_clear_txt;
	EditText search_text;
	SQLiteDatabase db;
	TextView welcome, title,t;
	TextView tvstatus[];
	Button deletebtn[];
	CommonFunctions cf;
	View v;
	Map<String, TextView> map1 = new LinkedHashMap<String, TextView>();

	public void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			InspectionType = extras.getString("InspectionType");
			status = extras.getString("status");
			value = extras.getInt("keyName");
			Count = extras.getInt("Count");

		}
		cf = new CommonFunctions(this);
		setContentView(R.layout.inspectionlist);
		welcome = (TextView) findViewById(R.id.welcomename);
		inspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		TextView note  = (TextView) findViewById(R.id.note);
		
		if("assign".equals(status)) {
		String headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode";
		note.setText(headernote);
		}
		else
		{
			String headernote="Note : Owner First Name Last Name | Policy Number | Address | City | State | County | Zipcode | Inspection date | Inspection Start time  - End time";
			note.setText(headernote);
		}
		
		
		this.search = (Button) this.findViewById(R.id.search);
		this.search_text = (EditText) this.findViewById(R.id.search_text);
		this.search_clear_txt = (Button) this
				.findViewById(R.id.search_clear_txt);

		cf.getInspectorId();
		welcomemessage();
		t = (TextView) this.findViewById(R.id.information);
		title = (TextView) this.findViewById(R.id.deleteiinformation);
		if ("1".equals(String.valueOf(value))) {
			columnvalue = 15;
			statusofdata = "0";
			strtit = "Start Inspection List :";

		}
		if ("4".equals(String.valueOf(value))) {
			columnvalue = 15;
			statusofdata = "0";
			strtit = "Schedule (or) Assign Inspection List :";

		}

		if ("6".equals(String.valueOf(value))) {
			columnvalue = 15;
			statusofdata = "0";
			t.setText("Call Attempt : ");
		}

		if ("assign".equals(status)) {
			alerttitle="Are you sure want to delete all the inspections in the Assign status?";
			colomnname = "Status";
			columnvalue = 22;
			statusofdata = "30";
			strtit = "Assigned Records";

		}
		if ("schedule".equals(status)) {
			alerttitle="Are you sure want to delete all the inspections in the Schedule status?";
			colomnname = "Status";
			columnvalue = 22;
			statusofdata = "40";
			strtit = "Schedule";

		}
		if ("inspected".equals(status)) {
			alerttitle="Are you sure want to delete all the inspections in the CIT status?";
			colomnname="IsInspected";
			columnvalue = 20;
			statusofdata = "1";
			strtit = "Edit(Inspected)";

		}

		if (InspectionType.equals("28")) {
			strtype = "B1 - 1802 (Rev. 01/12)";

		}
		if (InspectionType.equals("29")) {
			strtype = "B1 - 1802 (Rev. 01/12) Carr.Ord.";

		}
		t.setText(strtit);
		dbquery();

		this.homepage = (Button) this.findViewById(R.id.starthome);
		this.homepage.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				s = "";
				Intent homepage = new Intent(InspectionList.this,
						HomeScreen.class);
				startActivity(homepage);
			}
		});

		this.search_clear_txt.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				search_text.setText("");
				s = "";
				dbquery();

			}

		});
		this.search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				String temp = cf.convertsinglequotes(search_text.getText()
						.toString());
				if (temp.equals("")) {
					cf.ShowToast("Please enter the Name or Policy Number to search.",1);
					search_text.requestFocus();
				} else {
					s = temp;
					try {
						dbquery();
					} catch (Exception e) {

						cf.ShowToast("There is a problem in saving your data due to invalid character.",1);

					}
				}

			}

		});

	}

	private void dbquery() {
		data = null;
		dta = "";
		countarr = null;
		rws = 0;
		query = "select * from " + cf.mbtblInspectionList
				+ " where InspectionTypeId='" + InspectionType
				+ "' and Status='" + statusofdata + "' and InspectorId = '"
				+ cf.InspectorId + "' and SubStatus!=41";
		if (!s.trim().equals("")) {
			query += " and (s_OwnersNameFirst like '%" + s
					+ "%' or s_OwnersNameLast like '%" + s
					+ "%' or s_OwnerPolicyNumber like '%" + s + "%' ) ";
			if (status.equals("inspected")) {
				query += " and IsInspected=1 ";
			}
		} else if (status.equals("inspected")) {
			query += " and  IsInspected=1 ";
		}
		query += " order by InspectionDate";
		cur = cf.db.rawQuery(query, null);
		rws = cur.getCount();
		title.setText(strtype + "\n" + "Total Record : " + rws);
		data = new String[rws];
		countarr = new String[rws];
		int j = 0;
		cur.moveToFirst();
		if (cur.getCount() >= 1) {

			do {
				String dbinspid = cur.getString(cur
						.getColumnIndex("InspectorId"));
				String dbinsptypeid = cur.getString(cur
						.getColumnIndex("InspectionTypeId"));

				String scheduleddate = cur.getString(cur
						.getColumnIndex("ScheduleDate"));
				if (cf.InspectorId.equals(dbinspid)
						&& InspectionType.equals(dbinsptypeid)) {
					if (cur.getString(columnvalue).equals(statusofdata)) {

						dta += " "
								+ cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("s_OwnersNameFirst")))
								+ " ";
						dta += cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_OwnersNameLast"))) + " | ";
						dta += cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_OwnerPolicyNumber")))
								+ " \n ";
						dta += cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_propertyAddress"))) + " | ";
						dta += cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_City"))) + " | ";
						dta += cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_State"))) + " | ";
						dta += cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("s_County"))) + " | ";
						dta += cur.getString(cur.getColumnIndex("s_ZipCode"))
								+ " \n ";
						dta += cur.getString(cur
								.getColumnIndex("ScheduleDate")) + " | ";
						dta += cur.getString(cur
								.getColumnIndex("InspectionStartTime")) + " - ";
						dta += cur.getString(cur
								.getColumnIndex("InspectionEndTime")) + "~";

						countarr[j] = cur.getString(cur
								.getColumnIndex("s_SRID"));
						j++;

						if (dta.contains("null")) {
							dta = dta.replace("null", "");
						}
						if (dta.contains("N/A | ")) {
							dta = dta.replace("N/A |", "");
						}
						if (dta.contains("N/A - N/A")) {
							dta = dta.replace("N/A - N/A", "");
						}
					}
				}
			} while (cur.moveToNext());
			search_text.setText("");
			display();
		} else {
			
			inspectionlist.removeAllViews();
			if(s.equals(""))
			{
				cf.gohome();
			}
			else
			{
				cf.ShowToast("Sorry, No results found.", 1);
				cf.hidekeyboard();
			}
		}

	}

	private void display() {

		inspectionlist.removeAllViews();
		sv = new ScrollView(this);
		inspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		if (!dta.equals(null) && !dta.equals("null") && !dta.equals("")) {
			this.data = dta.split("~");
			for (int i = 0; i < data.length; i++) {
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
				
				
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				l2.setLayoutParams(mainparamschk);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);
				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
				tvstatus[i] = new TextView(this);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

			    lchkbox.addView(tvstatus[i], paramschk);
			    LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
				ldelbtn.setLayoutParams(mainparamschk);
				ldelbtn.setGravity(Gravity.RIGHT);
			    l2.addView(ldelbtn);
			    deletebtn[i] = new Button(this);
				deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
				deletebtn[i].setTag("deletebtn" + i);
				deletebtn[i].setPadding(30, 0, 0, 0);
				ldelbtn.addView(deletebtn[i], paramsdelbtn);
			
				tvstatus[i].setOnClickListener(new View.OnClickListener() {

					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace(
								"textbtn", "");
						final int s = Integer.parseInt(repidofselbtn);

						String select = countarr[s];
						String keyword = select.toString();
						String ScheduleDate;
						// cf.passvalues(keyword,InspectionType,status,value,Count);
						if (status.equals("assign")) {
							try {
								Cursor cur = cf.db.rawQuery("select * from "
										+ cf.mbtblInspectionList
										+ " where s_SRID='" + keyword + "'",
										null);
								int Column1 = cur
										.getColumnIndex("ScheduleDate");
								cur.moveToFirst();
								if (cur != null) {
									do {
										ScheduleDate = cur.getString(Column1);
										if (ScheduleDate.equals("Null")
												|| ScheduleDate
														.equals("Not Available")
												|| ScheduleDate.equals("")
												|| ScheduleDate.equals("N/A")) {
											Intent intimg = new Intent(
													InspectionList.this,
													CallattemptInfo.class);
											intimg.putExtra("homeid", keyword);
											intimg.putExtra("InspectionType",
													InspectionType);
											intimg.putExtra("status", status);
											intimg.putExtra("keyName", value);
											intimg.putExtra("Count", Count);
											startActivity(intimg);
										} else {
											Intent intimg = new Intent(
													InspectionList.this,
													PersonalInfo.class);
											intimg.putExtra("homeid", keyword);
											intimg.putExtra("InspectionType",
													InspectionType);
											intimg.putExtra("status", status);
											intimg.putExtra("keyName", value);
											intimg.putExtra("Count", Count);
											startActivity(intimg);
										}
									} while (cur.moveToNext());
								}
							} catch (Exception e) {
							}
						} else {
							Intent intimg = new Intent(InspectionList.this,
									PersonalInfo.class);
							intimg.putExtra("homeid", keyword);
							intimg.putExtra("InspectionType", InspectionType);
							intimg.putExtra("status", status);
							intimg.putExtra("keyName", value);
							intimg.putExtra("Count", Count);
							startActivity(intimg);
						}

					}

				});
				deletebtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace(
								"deletebtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						final String dt = countarr[cvrtstr];

						AlertDialog.Builder alert = new AlertDialog.Builder(
								InspectionList.this);

						alert.setTitle("Are you sure want to delete this inspection?");

						alert.setPositiveButton("Delete",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {

										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.mbtblInspectionList
													+ " WHERE s_SRID ='" + dt
													+ "'");
										} catch (Exception e) {

										}
										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.tbl_questionsdata
													+ " WHERE SRID ='" + dt
													+ "'");
										} catch (Exception e) {

										}
										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.tbl_comments
													+ " WHERE SRID ='" + dt
													+ "'");
										} catch (Exception e) {
										}
										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.ImageTable
													+ " WHERE SrID ='" + dt
													+ "'");
										} catch (Exception e) {
										}
										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.FeedBackInfoTable
													+ " WHERE Srid ='" + dt
													+ "'");
										} catch (Exception e) {

										}
										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.FeedBackDocumentTable
													+ " WHERE SRID ='" + dt
													+ "'");
										} catch (Exception e) {

										}
										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.HazardDataTable
													+ " WHERE Srid ='" + dt
													+ "'");
										} catch (Exception e) {

										}
										try {
											cf.db.execSQL("DELETE FROM "
													+ cf.HazardImageTable
													+ " WHERE SrID ='" + dt
													+ "'");
										} catch (Exception e) {

										}
										cf.ShowToast("Deleted successfully.",1);
										dbquery();
									}
								});
						alert.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										dialog.cancel();
									}
								});

						alert.show();

					}
				});
			}
		}

	}

	private void welcomemessage() {
		try {
			Cursor c1 = cf.db
					.rawQuery("SELECT * FROM " + cf.inspectorlogin
							+ " WHERE Ins_Id='" + cf.InspectorId.toString()
							+ "'", null);
			c1.moveToFirst();

			String data = c1.getString(c1.getColumnIndex("Ins_FirstName"))
					.toLowerCase();
			data += " ";
			data += c1.getString(c1.getColumnIndex("Ins_LastName"))
					.toLowerCase();
			welcome.setText(cf.getsinglequotes(data).toUpperCase());
			c1.close();

		} catch (Exception e) {

		}
	}
	public void clicker(View v) {
		switch (v.getId()) {
	
		case R.id.deleteallinspections:
						String temp="(";
						  Cursor c=cf.db.rawQuery(" Select * from "+cf.mbtblInspectionList+" where "+colomnname+" ="+statusofdata+" and InspectorId='"+cf.InspectorId+"' and InspectionTypeId ='"+InspectionType+"' and SubStatus<>41",null);
						  System.out.println("Cpint "+c.getCount());
						 if( c.getCount()>=1)
						 {
							 c.moveToFirst();
							 for(int i=0;i<c.getCount();i++)
							 {
								 temp+="'"+c.getString(c.getColumnIndex("s_SRID"))+"'";
								 if((i+1)==(c.getCount()))
								 {
									 temp+=")";
									// return;
								 }
								 else
								 {
									 temp+=",";
									 c.moveToNext();
								 }
							 }
							 System.out.println("the where "+temp);
							 cf.delete_all(temp,alerttitle);
						 }
			break;
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			s = "";
			Intent Star = new Intent(InspectionList.this,
					Startinspections.class);
			Star.putExtra("keyName", 1);
			startActivity(Star);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}