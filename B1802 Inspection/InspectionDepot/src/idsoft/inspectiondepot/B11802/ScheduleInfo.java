package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.GeneralConditions.MyOnItemSelectedListenerdata;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Calendar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class ScheduleInfo extends Activity {

	static final int DATE_DIALOG_ID = 0;
	private ArrayAdapter<CharSequence> mRangeadapter;
	ProgressDialog progressDialog;
	String isinspected;
	private int mYear, mMonth, mDay;
	TextWatcher watcher;
	private static final String TAG = null;
	ImageView polimg, schimg;
	String homeid, InspectionType, res, status, inspectorfname, inspectorlname,
			startspin, endspin, getinspecdate, assigntoinspdate;
	int value, commentsch, Count, cnt = 0, chk, spin1, spin2, verify;
	RelativeLayout relsch;
	TextView viewassigndate;
	TableLayout exceedslimit1;
	Button getdate;
	Spinner mSpinnerstart, mSpinnerend;
	TextView inspectorname;
	EditText etinspdate, comment;
	Button getinspectiondate;
	View v1;
	CheckBox schchk;
	
	CommonFunctions cf;
	CharSequence cd;
	android.text.format.DateFormat df;
	private AlertDialog alertDialog;
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		setContentView(R.layout.schedule);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.getDeviceDimensions();

		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 1, cf, 0));
		/** Ph submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 14,
				cf, 1));
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());

		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		schchk = (CheckBox) findViewById(R.id.resch);
		cf.policyholderinfo = (TextView) findViewById(R.id.policyholderinfo);

		inspectorname = (TextView) findViewById(R.id.inspector_name);
		inspectorname.setText(cf.insfname.toUpperCase() + " "
				+ cf.inslname.toUpperCase());
		etinspdate = (EditText) findViewById(R.id.inspdate);
		getinspectiondate = (Button) findViewById(R.id.getinspdate);
		mSpinnerstart = (Spinner) findViewById(R.id.spinstarttime);
		mSpinnerend = (Spinner) findViewById(R.id.spinendtime);
		viewassigndate = (TextView) this.findViewById(R.id.viewassigndate);

		
		TextView inspectorname = (TextView) findViewById(R.id.inspector_nametitle);
		inspectorname.setText(Html.fromHtml(cf.redcolor+" "+"Inspector Name "));
		
		TextView inspectionscheduledon = (TextView) findViewById(R.id.inspection_date);
		inspectionscheduledon.setText(Html.fromHtml(cf.redcolor+" "+"Inspection Schedule on "));
		
		TextView starttime = (TextView) findViewById(R.id.start_time);
		starttime.setText(Html.fromHtml(cf.redcolor+" "+"Start Time "));
		
		TextView endtime = (TextView) findViewById(R.id.end_time);
		endtime.setText(Html.fromHtml(cf.redcolor+" "+"End Time "));
		
		TextView comments = (TextView) findViewById(R.id.comment);
		comments.setText(Html.fromHtml(cf.redcolor+" "+"Comments "));
		
		comment = (EditText) findViewById(R.id.commenttxt);
		cf.SQ_TV_type2 = (TextView) findViewById(R.id.SH_TV_ED);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		cf.SQ_ED_type2_parrant = (LinearLayout) findViewById(R.id.SH_ED_parrent);
	      cf.SQ_ED_type2 = (LinearLayout) findViewById(R.id.SH_ED1);
	      comment.setOnTouchListener(new OnTouchListener() {
				
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					System.out.println("inside ontouch");
					cf.setFocus(comment);
					
					return false;
				}
			});
			
	      comment.addTextChangedListener(new SH_textwatcher(1));
		

		try {
			Cursor cur = cf.db.rawQuery("select * from "
					+ cf.mbtblInspectionList + " where s_SRID='" + cf.Homeid
					+ "'", null);
			int rws1 = cur.getCount();
			cur.moveToFirst();
			String ownerfirstname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameFirst")));
			String ownerlastname = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnersNameLast")));
			String policyno = cf.getsinglequotes(cur.getString(cur
					.getColumnIndex("s_OwnerPolicyNumber")));
			cf.policyholderinfo.setText(Html.fromHtml(ownerfirstname + " "
					+ ownerlastname + "<br/><font color=#bddb00>(Policy No : "
					+ policyno + ")</font>"));

		} catch (Exception e) {
			System.out.println(" errrr " + e.getMessage());
		}

		
		setstarttime();
		mSpinnerstart
				.setOnItemSelectedListener(new MyOnItemSelectedListenerstart());
		setendtime();
		mSpinnerend
				.setOnItemSelectedListener(new MyOnItemSelectedListenerend());
		schchk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					etinspdate.setEnabled(true);
					getinspectiondate.setEnabled(true);
					mSpinnerstart.setEnabled(true);
					mSpinnerend.setEnabled(true);
					comment.setEnabled(true);
				} else if (!cf.inspectiondate.equals("")) {
					etinspdate.setEnabled(false);
					getinspectiondate.setEnabled(false);
					mSpinnerstart.setEnabled(false);
					mSpinnerend.setEnabled(false);
					comment.setEnabled(false);
				}

			}

		});
		try {
			Cursor c2 = cf.db.rawQuery("SELECT * FROM "
					+ cf.mbtblInspectionList + " WHERE s_SRID='" + cf.Homeid
					+ "' and InspectorId='" + cf.InspectorId + "'", null);
			int rws = c2.getCount();
			int Column4 = c2.getColumnIndex("MiddleName");
			int Column = c2.getColumnIndex("ScheduleDate");
			int Column1 = c2.getColumnIndex("InspectionStartTime");
			int Column2 = c2.getColumnIndex("InspectionEndTime");
			int Column3 = c2.getColumnIndex("InspectionComment");

			c2.moveToFirst();
			if (c2 != null) {
				do {
					cf.inspectiondate = c2.getString(Column);
					assigntoinspdate = c2.getString(Column4);
					if (cf.inspectiondate.equals("")
							|| cf.inspectiondate.equals("Not Available")
							|| cf.inspectiondate.equals("N/A")
							|| cf.inspectiondate.equals("anyType{}")) {
						schchk.setVisibility(v1.GONE);

					} else {
						schchk.setVisibility(v1.VISIBLE);
					}
					startspin = c2.getString(Column1);
					endspin = c2.getString(Column2);
					if (cf.inspectiondate.equals("N/A")) {
						cf.inspectiondate = "";
					}
					etinspdate.setText(cf.inspectiondate);
					int sspinpos = mRangeadapter.getPosition(startspin);
					mSpinnerstart.setSelection(sspinpos);

					int espinpos = mRangeadapter.getPosition(endspin);
					mSpinnerend.setSelection(espinpos);
					if (c2.getString(c2.getColumnIndex("InspectionComment"))
							.equals("N/A")
							|| c2.getString(
									c2.getColumnIndex("InspectionComment"))
									.equals("Not Available")
							|| c2.getString(
									c2.getColumnIndex("InspectionComment"))
									.equals("anyType{}")) {
						comment.setText("");
						comment.setEnabled(true);
					} else {
						comment.setText(cf.getsinglequotes(c2.getString(c2
								.getColumnIndex("InspectionComment"))));
						comment.setEnabled(false);
						viewassigndate.setText(assigntoinspdate);

					}
				} while (c2.moveToNext());
			}

		} catch (Exception e) {

		}
		getinspectiondate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);

			}
		});

		try {
			if (!cf.inspectiondate.equals("")) {
				etinspdate.setEnabled(false);
				getinspectiondate.setEnabled(false);
				mSpinnerstart.setEnabled(false);
				mSpinnerend.setEnabled(false);
				comment.setEnabled(false);

			} else {
				etinspdate.setEnabled(true);
				getinspectiondate.setEnabled(true);
				mSpinnerstart.setEnabled(true);
				mSpinnerend.setEnabled(true);
				comment.setEnabled(true);
			}
		} catch (Exception e) {
			System.out.println("my data " + e.getMessage());
		}

	}
	 class SH_textwatcher implements TextWatcher
		{
	         public int type;
	        
	         SH_textwatcher(int type)
			{
				this.type=type; 
			}
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(),cf.SQ_ED_type2_parrant,cf.SQ_ED_type2,cf.SQ_TV_type2,"499");
			 
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
			
		}
	public class MyOnItemSelectedListenerstart implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			startspin = parent.getItemAtPosition(pos).toString();
			spin1 = parent.getSelectedItemPosition();

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerend implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			endspin = parent.getItemAtPosition(pos).toString();
			spin2 = parent.getSelectedItemPosition();

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	private void setstarttime() {
		mRangeadapter = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		mRangeadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerstart.setAdapter(mRangeadapter);
		mRangeadapter.add("Select");
		mRangeadapter.add("8:00 AM");
		mRangeadapter.add("8:30 AM");
		mRangeadapter.add("9:00 AM");
		mRangeadapter.add("9:30 AM");
		mRangeadapter.add("10:00 AM");
		mRangeadapter.add("10:30 AM");
		mRangeadapter.add("11:00 AM");
		mRangeadapter.add("11:30 AM");
		mRangeadapter.add("12:00 PM");
		mRangeadapter.add("12:30 PM");
		mRangeadapter.add("1:00 PM");
		mRangeadapter.add("1:30 PM");
		mRangeadapter.add("2:00 PM");
		mRangeadapter.add("2:30 PM");
		mRangeadapter.add("3:00 PM");
		mRangeadapter.add("3:30 PM");
		mRangeadapter.add("4:00 PM");
		mRangeadapter.add("4:30 PM");
		mRangeadapter.add("5:00 PM");
		mRangeadapter.add("5:30 PM");
		mRangeadapter.add("6:00 PM");
		mRangeadapter.add("6:30 PM");
		mRangeadapter.add("7:00 PM");

	}

	private void setendtime() {
		mRangeadapter = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		mRangeadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerend.setAdapter(mRangeadapter);
		mRangeadapter.add("Select");
		mRangeadapter.add("8:00 AM");
		mRangeadapter.add("8:30 AM");
		mRangeadapter.add("9:00 AM");
		mRangeadapter.add("9:30 AM");
		mRangeadapter.add("10:00 AM");
		mRangeadapter.add("10:30 AM");
		mRangeadapter.add("11:00 AM");
		mRangeadapter.add("11:30 AM");
		mRangeadapter.add("12:00 PM");
		mRangeadapter.add("12:30 PM");
		mRangeadapter.add("1:00 PM");
		mRangeadapter.add("1:30 PM");
		mRangeadapter.add("2:00 PM");
		mRangeadapter.add("2:30 PM");
		mRangeadapter.add("3:00 PM");
		mRangeadapter.add("3:30 PM");
		mRangeadapter.add("4:00 PM");
		mRangeadapter.add("4:30 PM");
		mRangeadapter.add("5:00 PM");
		mRangeadapter.add("5:30 PM");
		mRangeadapter.add("6:00 PM");
		mRangeadapter.add("6:30 PM");
		mRangeadapter.add("7:00 PM");

	}

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.hme:
			cf.gohome();
			break;

		case R.id.submt:
	try
	{
		/* cf.getInspecionTypeid(cf.Homeid);
			if(cf.checklicenceexpiry(cf.LicenceExpiryDate).equals("true"))
			{
				cf.ShowToast("Your licence date has been expired.",1);
			}
			else
			{*/
				cf.checkInspectorAvail = cf.IsCurrentInspector();
				System.out.println(" Schedule  "+cf.checkInspectorAvail);
		
					if (cf.checkInspectorAvail.equals("true"))
					{
		
					getinspecdate = etinspdate.getText().toString();
		
					if (!"".equals(getinspecdate)) {
						if (checkfordateistoday(getinspecdate) == "true") {
		
							if (checkforassigndate(assigntoinspdate, getinspecdate) == "true") {
								if (!"Select".equals(mSpinnerstart.getSelectedItem()
										.toString())) {
									if (!"Select".equals(mSpinnerend.getSelectedItem()
											.toString())) {
										if (spinvalidation(spin1, spin2) == "true") {
											
											  if (comment.getText().toString().trim().equals("") || !comment.isEnabled()) {
												  if(comment.getText().toString().trim().equals(""))
												  {
													  cf.ShowToast("Please enter the Scheduling Comments.",1);
													 comment.requestFocus();
												  }
												  else if(!comment.isEnabled())
												  {
													  cf.ShowToast("Please check the reschedule option.",1);
												  }
											  	   	
												}
		
											
											
											/*if (comment.getText().toString().trim().equals("") && schchk.isChecked()) {									
												ShowToast toast = new ShowToast(
														getBaseContext(),
														"Please enter the Scheduling Comments.");
		
												comment.requestFocus();	
											}*/
											else
											{
												try
												{
												Cursor c12 = cf.db.rawQuery("SELECT * FROM "
														+ cf.mbtblInspectionList + " WHERE s_SRID='" + cf.Homeid
														+ "' and InspectorId='" + cf.InspectorId + "'", null);
												c12.moveToFirst();
												 isinspected = c12.getString(c12.getColumnIndex("IsInspected"));
												}
												catch(Exception e)
												{
													System.out.println("isinpecte getting "+e.getMessage());
												}
												
											if(isinspected.equals("1"))
											{ 
												cf.ShowToast("You cannot schedule this record. This inspection has been moved to CIT status.", 1);
											}
											else
											{
												if (isInternetOn()) {
													progressDialog = ProgressDialog
															.show(ScheduleInfo.this,
																	"", "Scheduling...");
													new Thread() {
														public void run() {
															try {
																if (sendschedule() == "true") {
																	/*if (!comment
																			.getText()
																			.toString()
																			.equals("")) {*/
																		cf.db.execSQL("UPDATE "
																				+ cf.mbtblInspectionList
																				+ " SET Status='40',ScheduleDate='"
																				+ getinspecdate
																				+ "',"
																				+ "InspectionComment='"
																				+ cf.convertsinglequotes(comment
																						.getText()
																						.toString())
																				+ "',InspectionStartTime='"
																				+ mSpinnerstart
																						.getSelectedItem()
																						.toString()
																				+ "',"
																				+ "InspectionEndTime='"
																				+ mSpinnerend
																						.getSelectedItem()
																						.toString()
																				+ "',SchFlag='1' WHERE InspectorId ='"
																				+ cf.InspectorId
																				+ "' and s_SRID='"
																				+ cf.Homeid
																				+ "'");
																		Cursor c2 = cf.db
																				.rawQuery(
																						"SELECT * FROM "
																								+ cf.tbl_questionsdata
																								+ " WHERE SRID='"
																								+ cf.Homeid
																								+ "'",
																						null);
																		int rws = c2
																				.getCount();
																		if (rws == 0) {
																			cf.db.execSQL("INSERT INTO "
																					+ cf.tbl_questionsdata
																					+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
																					+ "VALUES ('"
																					+ cf.Homeid
																					+ "','','','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','','','','','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','','"
																					+ 0
																					+ "','','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','','','','','','','','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','','"
																					+ 0
																					+ "','"
																					+ 0
																					+ "','','"
																					+ 0
																					+ "','','"
																					+ cf.convertsinglequotes(comment
																							.getText()
																							.toString())
																					+ "','"
																					+ cd
																					+ "','"
																					+ 0
																					+ "')");
																		} else {
																			cf.db.execSQL("UPDATE "
																					+ cf.tbl_questionsdata
																					+ " SET ScheduleComments='"
																					+ cf.convertsinglequotes(comment
																							.getText()
																							.toString())
																					+ "',ModifyDate ='"
																					+ cd
																					+ "' WHERE SRID ='"
																					+ cf.Homeid
																					+ "'");
		
																		}
		
																		verify = 0;
																		handler.sendEmptyMessage(0);
																		progressDialog
																				.dismiss();
																		if (status
																				.equals("assign")) {
																			Intent Star = new Intent(
																					ScheduleInfo.this,
																					Startinspections.class);
																			Star.putExtra(
																					"keyName",
																					1);
																			startActivity(Star);
		
																		}
																	/*} else {
																		ShowToast toast = new ShowToast(
																				getBaseContext(),
																				"Please enter the Comments.");
																	}*/
																} else {
																	showerror();
		
																}
															} catch (SocketTimeoutException s) {
																verify = 5;
																handler.sendEmptyMessage(0);
																progressDialog
																		.dismiss();
															} catch (NetworkErrorException n) {
																verify = 5;
																handler.sendEmptyMessage(0);
																progressDialog
																		.dismiss();
															} catch (IOException io) {
																verify = 5;
																handler.sendEmptyMessage(0);
																progressDialog
																		.dismiss();
															} catch (XmlPullParserException x) {
																verify = 5;
																handler.sendEmptyMessage(0);
																progressDialog
																		.dismiss();
															} catch (Exception e) {
																verify = 5;
																handler.sendEmptyMessage(0);
																progressDialog
																		.dismiss();
															}
		
														}
		
														private void showerror() {
															verify = 1;
															handler.sendEmptyMessage(0);
															progressDialog.dismiss();
														}
		
														private Handler handler = new Handler() {
															@Override
															public void handleMessage(
																	Message msg) {
																// schedulecomplete.setText("Schedule complete!!");
																
																if (verify == 0) {
																	if(schchk.isChecked()){
																		cf.ShowToast("Your inspection has been successfully re-scheduled.",1);
																	}
																	else
																	{
																		cf.ShowToast("Your inspection has been successfully scheduled.",1);
																	}
		
																} else if (verify == 1) {
																	cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",1);
		
																} else if (verify == 5) {
																	cf.ShowToast("There is a problem on your Network. Please try again later with better Network.. ",1);
																}
															}
														};
													}.start();
												} else {
													cf.ShowToast("Internet connection is not available.",1);
		
													cf.db.execSQL("UPDATE "
															+ cf.mbtblInspectionList
															+ " SET Status='40',ScheduleDate='"
															+ getinspecdate
															+ "',"
															+ "InspectionComment='"
															+ cf.convertsinglequotes(comment
																	.getText()
																	.toString())
															+ "',InspectionStartTime='"
															+ mSpinnerstart
																	.getSelectedItem()
																	.toString()
															+ "',"
															+ "InspectionEndTime='"
															+ mSpinnerend
																	.getSelectedItem()
																	.toString()
															+ "',SchFlag='0' WHERE InspectorId ='"
															+ cf.InspectorId
															+ "' and s_SRID='"
															+ cf.Homeid + "'");
													if (status.equals("assign")) {
														Intent Star = new Intent(
																ScheduleInfo.this,
																Startinspections.class);
														Star.putExtra("keyName", 1);
														startActivity(Star);
		
													}
												}
											}
												
												
												
											}
										} else {
											cf.ShowToast("End Time should be greater than Start Time.",1);
		
										}
									} else {
										cf.ShowToast("Please select End Time.",1);
		
										mSpinnerend.requestFocus();
									}
								} else {
									cf.ShowToast("Please select Start Time.",1);
		
									mSpinnerstart.requestFocus();
								}
							} else {
								cf.ShowToast("Schedule Date should be greater than or equal to Assigned Date.",1);
		
								etinspdate.setText("");
								etinspdate.requestFocus();
							}
						} else {
							cf.ShowToast("Schedule Date should be greater than Today's Date.",1);
							etinspdate.setText("");
							etinspdate.requestFocus();
						}
					} else {
						cf.ShowToast("Please enter the Inspection Date.",1);
		
						etinspdate.requestFocus();
					}
			}
			else
			{
				showerrorinspreallocate();
			}
		//	}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			showerrorinspreallocate();
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			showerrorinspreallocate();
			e.printStackTrace();
		}
	
	break;
		}

	}


	private void showerrorinspreallocate() {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
            public void run() 
            {
            	finishedHandler.sendEmptyMessage(0);
            }
        }).start();
	}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	    			alertDialog = new AlertDialog.Builder(ScheduleInfo.this).create();
	    			alertDialog.setMessage("This Inspection is no longer Assigned to you Please Cease all Further Work.");
	    			alertDialog.setButton("Ok",	new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) 
					{
						alertDialog.dismiss();						
					}
				});
				alertDialog.show();
	    }
	};
	private String sendschedule() throws NetworkErrorException, IOException,
			SocketTimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub

		// try {
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject ad_property = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME27);

		ad_property.addProperty("Srid", cf.Homeid);
		ad_property.addProperty("InspectorID", cf.InspectorId);
		ad_property.addProperty("ScheduleDate", getinspecdate);
		int spinnerstatPosition = mRangeadapter.getPosition(startspin);
		ad_property.addProperty("ScheduleStartTime", spinnerstatPosition);
		int spinnerendPosition = mRangeadapter.getPosition(endspin);
		ad_property.addProperty("ScheduleEndTime", spinnerendPosition);
		ad_property.addProperty("Comments", comment.getText().toString());

		envelope.setOutputSoapObject(ad_property);
		HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL);
		System.out.println("ad_property"+ad_property);

		androidHttpTransport1.call(cf.SOAP_ACTION27, envelope);
		Object response = envelope.getResponse();System.out.println("response"+response);
		if (response == null || response.toString().equals("")
				|| response.toString().equals("null")) {
			verify = 1;
			res = "false";
		} else if (response.toString().equals("true")) {
			verify = 0;
			res = "true";
		} else {
			verify = 1;
			res = "false";
		}
		return res;
	}

	private String spinvalidation(int spin12, int spin22) {
		// TODO Auto-generated method stub
		String chkspin;
		if (spin22 > spin12) {
			chkspin = "true";
		} else {
			chkspin = "false";
		}
		return chkspin;
	}

	private String checkforassigndate(String getinsurancedate,
			String getinspectiondate) {
		if (!getinsurancedate.trim().equals("")
				|| getinsurancedate.equals("N/A")
				|| getinsurancedate.equals("Not Available")
				|| getinsurancedate.equals("anytype")
				|| getinsurancedate.equals("Null")) {
			String chkdate = null;
			int i1 = getinsurancedate.indexOf("/");
			String result = getinsurancedate.substring(0, i1);
			int i2 = getinsurancedate.lastIndexOf("/");
			String result1 = getinsurancedate.substring(i1 + 1, i2);
			String result2 = getinsurancedate.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);

			int i3 = getinspectiondate.indexOf("/");
			String result3 = getinspectiondate.substring(0, i3);
			int i4 = getinspectiondate.lastIndexOf("/");
			String result4 = getinspectiondate.substring(i3 + 1, i4);
			String result5 = getinspectiondate.substring(i4 + 1);
			result5 = result5.trim();
			int k1 = Integer.parseInt(result3);
			int k2 = Integer.parseInt(result4);
			int k = Integer.parseInt(result5);
			

			if (j > k) {
				chkdate = "false";
			} else if (j < k) {
				chkdate = "true";
			} else if (j == k) {

				if (j1 > k1) {

					chkdate = "false";
				} else if (j1 < k1) {
					chkdate = "true";
				} else if (j1 == k1) {
					if (j2 > k2) {
						chkdate = "false";
					} else if (j2 < k2) {
						chkdate = "true";
					} else if (j2 == k2) {

						chkdate = "true";
					}

				}

			}

			return chkdate;
		} else {
			return "true";
		}
	}

	private String checkfordateistoday(String getinspecdate2) {
		// TODO Auto-generated method stub
		String chkdate = null;
		int i1 = getinspecdate2.indexOf("/");
		String result = getinspecdate2.substring(0, i1);
		int i2 = getinspecdate2.lastIndexOf("/");
		String result1 = getinspecdate2.substring(i1 + 1, i2);
		String result2 = getinspecdate2.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;

		if (j > thsyr || (j1 > curmnth && j >= thsyr)
				|| (j2 >= curdate && j1 >= curmnth && j >= thsyr)) {
			chkdate = "true";
		} else {
			chkdate = "false";
		}

		return chkdate;

	}


	public final boolean isInternetOn() {
		boolean chk = false;
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			chk = true;
		} else {
			chk = false;
		}

		return chk;
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			etinspdate.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(ScheduleInfo.this, CallattemptInfo.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
					
				}
				
				break;

		}

	}
}
