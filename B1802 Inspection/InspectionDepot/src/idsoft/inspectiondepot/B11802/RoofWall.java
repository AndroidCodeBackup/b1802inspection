package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.OverallComments.OC_textwatcher;
import idsoft.inspectiondepot.B11802.OverallComments.Touch_Listener;

import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class RoofWall extends Activity {

	private static final int visibility = 0;
	private TableLayout tbllayout8, exceedslimit1;
	String inspectortypeid, helpcontent;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	CheckBox temp_st;
	String commdescrip = "";
	ListView list;
	ImageView Vimage;
	TextWatcher watcher;
	int viewimage = 1,commentsch;
	RelativeLayout tblcommentschange;
	String buildcommadmin1, buildcommadmin2, buildcommadmin3;
	TextView nocommentsdisp, txroofwallheading,helptxt,roofwall_TV_type1;
	Button saveclose, prev;
	String rooftowallvalueprev, rooftowallsubvalueprev, rooftowallothrtxtprev,
			rooftowallclipsminvalueprev, rooftowallsingleminvalueprev,
			rooftowalldoubleminvalueprev;
	RadioButton rdioToenail, rdiotoenailoption1, rdiotoenailoption2, rdioClips,
			rdioclipoption1, rdioclipoption2, rdioSinglewraps,
			rdiosinglewrapoption1, rdioDoubleWraps, rdiodoublewrapoption1,
			rdiodoublewrapoption2, rdioE, rdioF, rdioG, rdioH;
	String commentsfill, InspectionType, status, rdiochk, comm, homeId,
			suboption = "0", othertext = "", updatecnt, identity,
			roofwallcomment;
	EditText comments, txtroofwallother;
	Intent iInspectionList;
	int value, Count, roofclipssingleminvalue;
	String descrip, comm2, conchkbox, status1, status2, status3, buildcomment1,
			buildcomment2, buildcomment3, builcodeadmin;
	String[] arrcomment1;
	CheckBox rdiotoenailoption3, rdiotoenailoption4;
	int clipsminvalue1, clipsminvalue2, roofclipsminvalue, clipdoubleminvalue1,
			clipsingleminvalue1, clipsingleminvalue2, roofclipsdoubleminvalue,
			clipdoubleminvalue2;
	CheckBox chkclipsmincond1, chkclipsmincond2, chksingleclipsmincond1,
			chksingleclipsmincond2, chkdoubleclipsmincond1,
			chkdoubleclipsmincond2;
	public LinearLayout ln[] = new LinearLayout[4];
	int optionid;
	public boolean che_status = true;
	CheckBox[] cb;
	View vv;public int focus=0;
	int minvalue1, minvalue2;
	LinearLayout lincomments,roofwall_parrant,roofwall_type1;
	View v1;
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();
	AlertDialog alertDialog;
	TextView prevmitidata;
	CommonFunctions cf;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.Homeid = homeId = bunhomeId.getString("homeid");
			cf.Identity = identity = bunhomeId.getString("iden");
			cf.InspectionType = InspectionType = bunhomeId
					.getString("InspectionType");
			cf.status = status = bunhomeId.getString("status");
			cf.value = value = bunhomeId.getInt("keyName");
			cf.Count = Count = bunhomeId.getInt("Count");

		}
		setContentView(R.layout.roofwall);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.getDeviceDimensions();
		cf.changeimage();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 24,
				cf, 1));
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		focus=1;
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==null)
				{
					cf.alertcontent = "To see help comments, please select Roof Wall options.";
				}
				else if (rdiochk.equals("1") || rdioToenail.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A Toe Nail, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("2") || rdioClips.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B Clips, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("3") || rdioSinglewraps.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C single wraps, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("4") || rdioDoubleWraps.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D double wraps, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("5") || rdioE.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, structural connection, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("6") || rdioF.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("7") || rdioG.isChecked()) {
					cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home.";
				} else if (rdiochk.equals("8") || rdioH.isChecked()) {
					cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				} else {
					cf.alertcontent = "To see help comments, please select Roof Wall options.";
				}
				cf.showhelp("HELP",cf.alertcontent);


			}
		});
		txroofwallheading = (TextView) findViewById(R.id.txtheadingroofwall);
		txroofwallheading
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>What is the "
								+ "<u>"
								+ "WEAKEST"
								+ "</u>"
								+ " roof to wall connection? (Do not include attachment of hip/valley jacks within 5 feet of the inside or outside corner of the roof in determination of WEAKEST type) "));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		
			try {

			Cursor c2 = cf.db.rawQuery("SELECT * FROM "
					+ cf.quetsionsoriginaldata + " WHERE homeid='" + cf.Homeid
					+ "'", null);
			if(c2.getCount()>0){
				c2.moveToFirst();
				String bcoriddata = cf.getsinglequotes(c2.getString(c2.getColumnIndex("rworiginal")));
				prevmitidata.setText(Html
					.fromHtml("<font color=blue>Original Value : " + "</font>"
							+ "<font color=red>" + bcoriddata + "</font>"));
			}
			else
			{
				prevmitidata.setText(Html
						.fromHtml("<font color=blue>Original Value : "
								+ "</font>" + "<font color=red>Not Available</font>"));
			}

		} catch (Exception e) {

		}
		
		

		this.tbllayout8 = (TableLayout) this.findViewById(R.id.tableLayoutComm);

		this.lincomments = (LinearLayout) this
				.findViewById(R.id.linearlayoutcomm);
		Vimage = (ImageView) findViewById(R.id.Vimage);
		tblcommentschange = (RelativeLayout) findViewById(R.id.tbllayoutrwcomments);
		this.nocommentsdisp = (TextView) this.findViewById(R.id.notxtcomments);
		exceedslimit1 = (TableLayout) this.findViewById(R.id.exceedslimit);
		this.rdioToenail = (RadioButton) this.findViewById(R.id.toenail);
		this.rdioToenail.setOnClickListener(OnClickListener);
		this.rdiotoenailoption1 = (RadioButton) this
				.findViewById(R.id.toenailoption1);
		this.rdiotoenailoption1.setOnClickListener(OnClickListener);
		this.rdiotoenailoption2 = (RadioButton) this
				.findViewById(R.id.toenailoption2);
		this.rdiotoenailoption2.setOnClickListener(OnClickListener);

		this.chkclipsmincond1 = (CheckBox) this.findViewById(R.id.chkclipsmin1);
		this.chkclipsmincond2 = (CheckBox) this.findViewById(R.id.chkclipsmin2);

		this.chksingleclipsmincond1 = (CheckBox) this
				.findViewById(R.id.chksinglewrapsmincond1);
		this.chksingleclipsmincond2 = (CheckBox) this
				.findViewById(R.id.chksinglewrapsmincond2);

		this.chkdoubleclipsmincond1 = (CheckBox) this
				.findViewById(R.id.chkdoublewrapmincond1);
		this.chkdoubleclipsmincond2 = (CheckBox) this
				.findViewById(R.id.chkdoublewrapmincond2);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		// start to create object for the lenear layout

		ln[0] = (LinearLayout) findViewById(R.id.linLayoutA);
		ln[1] = (LinearLayout) findViewById(R.id.linLayoutB);
		ln[2] = (LinearLayout) findViewById(R.id.linLayoutC);
		ln[3] = (LinearLayout) findViewById(R.id.linLayoutD);

		// start to create object for the lenear layout

		chkdoubleclipsmincond1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					clipdoubleminvalue1 = 1;
				} else {
					clipdoubleminvalue1 = 0;
				}
			}
		});
		chkdoubleclipsmincond2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					clipdoubleminvalue2 = 1;
				} else {
					clipdoubleminvalue2 = 0;
				}
			}
		});

		this.rdioClips = (RadioButton) this.findViewById(R.id.clips);
		this.rdioClips.setOnClickListener(OnClickListener);
		this.rdioclipoption1 = (RadioButton) this
				.findViewById(R.id.clipsoption1);
		this.rdioclipoption1.setOnClickListener(OnClickListener);
		this.rdioclipoption2 = (RadioButton) this
				.findViewById(R.id.clipsoption2);
		this.rdioclipoption2.setOnClickListener(OnClickListener);

		this.rdioSinglewraps = (RadioButton) this
				.findViewById(R.id.rdiosinglewraps);
		this.rdioSinglewraps.setOnClickListener(OnClickListener);
		this.rdiosinglewrapoption1 = (RadioButton) this
				.findViewById(R.id.singlewrapsoption1);
		this.rdiosinglewrapoption1.setOnClickListener(OnClickListener);

		this.rdioDoubleWraps = (RadioButton) this
				.findViewById(R.id.rdiodoublewraps);
		this.rdioDoubleWraps.setOnClickListener(OnClickListener);
		this.rdiodoublewrapoption1 = (RadioButton) this
				.findViewById(R.id.doublewrapsoption1);
		this.rdiodoublewrapoption1.setOnClickListener(OnClickListener);
		this.rdiodoublewrapoption2 = (RadioButton) this
				.findViewById(R.id.doublewrapsoption2);
		this.rdiodoublewrapoption2.setOnClickListener(OnClickListener);

		this.rdioE = (RadioButton) this.findViewById(R.id.rdioE);
		this.rdioE.setOnClickListener(OnClickListener);

		this.rdioF = (RadioButton) this.findViewById(R.id.rdioF);
		this.rdioF.setOnClickListener(OnClickListener);

		this.rdioG = (RadioButton) this.findViewById(R.id.rdioG);
		this.rdioG.setOnClickListener(OnClickListener);

		this.rdioH = (RadioButton) this.findViewById(R.id.rdioH);
		this.rdioH.setOnClickListener(OnClickListener);
		
		roofwall_parrant = (LinearLayout)this.findViewById(R.id.roofwall_parrent);
		roofwall_type1=(LinearLayout) findViewById(R.id.roofwall_type);
		roofwall_TV_type1 = (TextView) findViewById(R.id.roofwall_txt);
		comments = (EditText) this.findViewById(R.id.txtroofwallcomments);
		comments.setOnTouchListener(new Touch_Listener());
		comments.addTextChangedListener(new QUES_textwatcher());
	
		txtroofwallother = (EditText) this.findViewById(R.id.txtroofwallother);
		saveclose = (Button) findViewById(R.id.savenext);
		prev = (Button) findViewById(R.id.previous);

		try {
			Cursor c2 = cf.db
					.rawQuery(
							"SELECT RooftoWallValue,RooftoWallSubValue,RooftoWallOtherText,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue FROM "
									+ cf.tbl_questionsdata
									+ " WHERE SRID='"
									+ cf.Homeid + "'", null);
			int chkrws = c2.getCount();
			if (chkrws == 0) {
				identity = "test";
			} else {
				identity = "prev";
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in retrieving data from roofwall table on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}

		if (identity.equals("prev")) {
			try {
				Cursor cur = cf.db.rawQuery("select * from "
						+ cf.tbl_questionsdata + " where SRID='" + cf.Homeid
						+ "'", null);
				cur.moveToFirst();
				if (cur != null) {
					rooftowallvalueprev = cur.getString(cur
							.getColumnIndex("RooftoWallValue"));
					rooftowallsubvalueprev = cur.getString(cur
							.getColumnIndex("RooftoWallSubValue"));
					rooftowallothrtxtprev = cf.getsinglequotes(cur
							.getString(cur
									.getColumnIndex("RooftoWallOtherText")));
					rooftowallclipsminvalueprev = cur.getString(cur
							.getColumnIndex("RooftoWallClipsMinValue"));
					rooftowallsingleminvalueprev = cur.getString(cur
							.getColumnIndex("RooftoWallSingleMinValue"));
					rooftowalldoubleminvalueprev = cur.getString(cur
							.getColumnIndex("RooftoWallDoubleMinValue"));

					if (rooftowallvalueprev.equals("1")) {
						rdiochk = "1";
						ln[0].setVisibility(View.VISIBLE);
						rdioToenail.setChecked(true);
						if (rooftowallsubvalueprev.equals("1")) {
							rdiotoenailoption1.setChecked(true);
						} else if (rooftowallsubvalueprev.equals("2")) {
							rdiotoenailoption2.setChecked(true);
						} else {
						}
						suboption = rooftowallsubvalueprev;

					} else if (rooftowallvalueprev.equals("2")) {
						rdiochk = "2";
						ln[1].setVisibility(View.VISIBLE);
						rdioClips.setChecked(true);
						chkclipsmincond1.setChecked(true);
						chkclipsmincond2.setChecked(true);

						if (rooftowallsubvalueprev.equals("1")) {
							rdioclipoption1.setChecked(true);
						} else if (rooftowallsubvalueprev.equals("2")) {
							rdioclipoption2.setChecked(true);
						}

						else {
						}
						suboption = rooftowallsubvalueprev;
					} else if (rooftowallvalueprev.equals("3")) {
						rdiochk = "3";
						suboption = "1";
						ln[2].setVisibility(View.VISIBLE);
						rdioSinglewraps.setChecked(true);
						chksingleclipsmincond1.setChecked(true);
						chksingleclipsmincond2.setChecked(true);
						rdiosinglewrapoption1.setChecked(true);

					}

					else if (rooftowallvalueprev.equals("4")) {
						rdiochk = "4";
						ln[3].setVisibility(View.VISIBLE);
						rdioDoubleWraps.setChecked(true);
						chkdoubleclipsmincond1.setChecked(true);
						chkdoubleclipsmincond2.setChecked(true);

						if (rooftowallsubvalueprev.equals("1")) {
							rdiodoublewrapoption1.setChecked(true);
						} else if (rooftowallsubvalueprev.equals("2")) {
							rdiodoublewrapoption2.setChecked(true);
						}

						else {
						}
						suboption = rooftowallsubvalueprev;
					}

					else if (rooftowallvalueprev.equals("5")) {
						rdioE.setChecked(true);
						rdiochk = "5";
					} else if (rooftowallvalueprev.equals("6")) {
						rdioF.setChecked(true);
						txtroofwallother.setText(rooftowallothrtxtprev);
						rdiochk = "6";
					} else if (rooftowallvalueprev.equals("7")) {
						rdioG.setChecked(true);
						rdiochk = "7";
					} else if (rooftowallvalueprev.equals("8")) {
						rdioH.setChecked(true);
						rdiochk = "8";
					} else {
					}
				}
			} catch (Exception e) {
				System.out.println("e" + e);
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in retrieving data and setting roofwall value on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}

			try {
				Cursor cur = cf.db.rawQuery("select * from " + cf.tbl_comments
						+ " where SRID='" + cf.Homeid + "'", null);
				cur.moveToFirst();
				if (cur != null) {
					roofwallcomment = cf.getsinglequotes(cur.getString(cur
							.getColumnIndex("RoofWallComment")));
					
					cf.showing_limit(roofwallcomment,roofwall_parrant,roofwall_type1,roofwall_TV_type1,"474");	
					comments.setText(roofwallcomment);
				}
			} catch (Exception e) {
				System.out.println("e" + e);
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in retrieving roofwall comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
		}
		this.Vimage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}

		});
		this.tblcommentschange.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}
		});

	}
	class Touch_Listener implements OnTouchListener
	{
		   Touch_Listener()
			{
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	
		    		cf.setFocus(comments);
				
				return false;
		    }
	}
	class QUES_textwatcher implements TextWatcher
	{
	    QUES_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
				
				cf.showing_limit(s.toString(),roofwall_parrant,roofwall_type1,roofwall_TV_type1,"474"); 
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	private void arrowcommentschange() {
		// TODO Auto-generated method stub
		Vimage.setBackgroundResource(R.drawable.arrowup);
		if (viewimage == 1) {
			Vimage.setBackgroundResource(R.drawable.arrowup);
			if (rdioToenail.isChecked()) {
				optionid = 1;
			}

			else if (rdioClips.isChecked()) {
				optionid = 2;
			} else if (rdioSinglewraps.isChecked()) {
				optionid = 3;
			} else if (rdioDoubleWraps.isChecked()) {
				optionid = 4;
			}

			else if (rdioE.isChecked()) {
				optionid = 5;
			} else if (rdioF.isChecked()) {
				optionid = 6;
			} else if (rdioG.isChecked()) {
				optionid = 7;
			} else if (rdioH.isChecked()) {
				optionid = 8;
			}

			else {
				cf.ShowToast("Please select the Roof Wall options to view Comments.",1);
			}
			try {
				descrip = "";
				arrcomment1 = null;
				Cursor c2 = cf.db.rawQuery("SELECT * FROM "
						+ cf.tbl_admcomments
						+ " WHERE questionid='4' and optionid='" + optionid
						+ "' and status='Active' and InspectorId='"
						+ cf.InspectionType + "'", null);
				int rws = c2.getCount();
				arrcomment1 = new String[rws];
				c2.moveToFirst();
				if (rws == 0) {
					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.VISIBLE);
					lincomments.setVisibility(v1.GONE);
					Vimage.setBackgroundResource(R.drawable.arrowdown);
				} else {

					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.VISIBLE);
					if (c2 != null) {
						int i=0;
						do {
							arrcomment1[i]=cf.getsinglequotes(c2.getString(c2.getColumnIndex("description")));
							if(arrcomment1[i].contains("null"))
							{
								arrcomment1[i] = arrcomment1[i].replace("null", "");
							}							
							i++;
						} while (c2.moveToNext());						
					}
					c2.close();

					addcomments();
					viewimage = 0;
				}

			} catch (Exception e) {
				/*
				 * ShowToast toast = new ShowToast(getBaseContext(),
				 * "Please add atleast one roof wall comment in dashboard");
				 */
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in retrieving roofwall comments on arrow buttton click on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}

		} else if (viewimage == 0) {
			Vimage.setBackgroundResource(R.drawable.arrowdown);
			tbllayout8.setVisibility(v1.GONE);
			viewimage = 1;
		}

	}

	private void addcomments() {
		// TODO Auto-generated method stub
		lincomments.removeAllViews();
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		lincomments.addView(l1);

		cb = new CheckBox[arrcomment1.length];

		for (int i = 0; i < arrcomment1.length; i++) {

			LinearLayout l2 = new LinearLayout(this);			
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.setMargins(0, 0, 20, 0);
			l1.addView(l2);			
			
			cb[i] = new CheckBox(this);
			cb[i].setText(arrcomment1[i].trim());
			cb[i].setTextColor(Color.GRAY);
			cb[i].setSingleLine(false);
			
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
			cb[i].setMaxWidth(padding);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,12);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);

			map1.put(conchkbox, cb[i]);
			l2.addView(cb[i],paramschk);

			cb[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					if(comments.getText().length()>=474)
					{
						cf.ShowToast("Comment text exceeds the limit.", 1);
						map1.get(getidofselbtn).setChecked(false);
					}
					else
					{
					temp_st = map1.get(getidofselbtn);
					setcomments(temp_st);
					}
				}
			});
		}

	}

	private void setcomments(final CheckBox tempSt) {
		// TODO Auto-generated method stub
		comm2 = "";
		String comm1 = tempSt.getText().toString();
		if (tempSt.isChecked() == true) {
			comm2 += comm1 + " ";
			int commenttextlength = comments.getText().length();
			comments.setText(comments.getText().toString() + " " + comm2);

			if (commenttextlength >= 474) {
				alertDialog = new AlertDialog.Builder(RoofWall.this).create();
				alertDialog.setTitle("Exceeds Limit");
				alertDialog.setMessage("Comment text exceeds the limit");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								tempSt.setChecked(false);
								tempSt.setEnabled(true);
							}
						});
				alertDialog.show();
			} else {
				tempSt.setEnabled(false);
			}
		} else {
		}
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.toenail:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A Toe Nail, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "1";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(true);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.clips:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B Clips, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "2";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(true);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);

				// chkclipsmincond1.setChecked(true);chkclipsmincond2.setChecked(true);
				chksingleclipsmincond1.setChecked(false);
				chksingleclipsmincond2.setChecked(false);
				chkdoubleclipsmincond1.setChecked(false);
				chkdoubleclipsmincond2.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;

				break;

			case R.id.rdiosinglewraps:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C single wraps, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "3";
				suboption = "1";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(true);
				rdiosinglewrapoption1.setChecked(true);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);

				chkclipsmincond1.setChecked(false);
				chkclipsmincond2.setChecked(false);
				// chksingleclipsmincond1.setChecked(true);chksingleclipsmincond2.setChecked(true);
				chkdoubleclipsmincond1.setChecked(false);
				chkdoubleclipsmincond2.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;

				break;

			case R.id.rdiodoublewraps:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D double wraps, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "4";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(true);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);

				chkclipsmincond1.setChecked(false);
				chkclipsmincond2.setChecked(false);
				chksingleclipsmincond1.setChecked(false);
				chksingleclipsmincond2.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				// chkdoubleclipsmincond1.setChecked(true);chkdoubleclipsmincond2.setChecked(true);

				break;

			case R.id.rdioE:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, structural connection, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);

				rdiochk = "5";
				set_invisible();
				txtroofwallother.setEnabled(false);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(true);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.rdioF:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "6";
				set_invisible();
				txtroofwallother.setEnabled(true);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				othertext = txtroofwallother.getText().toString();
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(true);
				rdioG.setChecked(false);
				rdioH.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.rdioG:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "We were unable to verify the weakest form or type of roof wall connection to this home.";
				comments.setText(commentsfill);
				rdiochk = "7";
				set_invisible();
				txtroofwallother.setEnabled(false);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(true);
				rdioH.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;
			case R.id.rdioH:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				comments.setText(commentsfill);
				rdiochk = "8";
				set_invisible();
				txtroofwallother.setEnabled(false);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(true);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.toenailoption1:
				suboption = "1";
				rdiotoenailoption1.setChecked(true);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				break;
			case R.id.toenailoption2:
				suboption = "2";
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(true);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				break;
			case R.id.chkclipsmin1:
				// suboption="3";
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(true);rdiotoenailoption4.setChecked(false);
				break;
			case R.id.chkclipsmin2:
				// suboption="4";
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(true);
				break;
			case R.id.clipsoption1:
				suboption = "1";
				rdioclipoption1.setChecked(true);
				rdioclipoption2.setChecked(false);
				break;

			case R.id.clipsoption2:
				suboption = "2";
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(true);
				break;
			case R.id.singlewrapsoption1:
				suboption = "1";
				rdiosinglewrapoption1.setChecked(true);
				break;
			case R.id.doublewrapsoption1:
				suboption = "1";
				rdiodoublewrapoption1.setChecked(true);
				rdiodoublewrapoption2.setChecked(false);
				break;

			case R.id.doublewrapsoption2:
				suboption = "2";
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(true);
				break;

			}
		}
	};

	public void set_invisible() {

		for (int i = 0; i <= 3; i++) {
			ln[i].setVisibility(View.GONE);
		}
	}

	public void set_visible() {
		// function for set cisible once the radio button clicked
		for (int i = 0; i <= 3; i++) {
			if ((i + 1) != (Integer.parseInt(rdiochk))) {
				ln[i].setVisibility(View.GONE);
			} else {
				ln[i].setVisibility(View.VISIBLE);
			}
		}
		// function for set cisible once the radio button clicked
	}

	public void clicker(View v) {
		switch (v.getId()) {
		
		  case R.id.txthelpcontentoptionA: 
			  cf.alerttitle="A - Toe Nail";
			  cf.alertcontent="Rafter/truss anchored to top plate of wall using nails driven at an angle through the rafter/truss and attached to the top plate of the wall.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionB: 
			  cf.alerttitle="B - Clips";
			  cf.alertcontent="Metal attachments on every rafter/truss that are nailed to one side (or both sides in the case of a diamond type clip) of the rafter/truss and attached to the top plate of the wall frame or embedded in the bond beam.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionC: 
			  cf.alerttitle="C - Single Wraps";
			  cf.alertcontent="Metal Straps must be secured to every rafter/truss with a minimum of 3 nails, wrapping over and securing to the opposite side of the rafter/truss with a minimum of 1 nail. The Strap must be attached to the top plate of the wall frame or embedded in the bond beam in at least one place.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionD: 
			  cf.alerttitle="D - Double Wraps";
			  cf.alertcontent="Both Metal Straps must be secured to every rafter/truss with a minimum of 3 nails, wrapping over and securing to the opposite side of the rafter/truss with a minimum of 1 nail. Each Strap must be attached to the top plate of the wall frame or embedded in the bond beam in at least one place.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.txthelpcontentoptionE: 
			  cf.alerttitle="E - Structural";
			  cf.alertcontent="Anchor bolts, structurally connected or reinforced concrete roof.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionF: 
			  cf.alerttitle="F - other";
			  cf.alertcontent="Other.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.txthelpcontentoptionG: 
			  cf.alerttitle="G - Unknown";
			  cf.alertcontent="Unknown or unidentified.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.txthelpcontentoptionH: 
			  cf.alerttitle="H - No Attic";
			  cf.alertcontent="No Attic Access";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.previous:
			iInspectionList = new Intent(RoofWall.this, RoofDeck.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("iden", "prev");
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);
			startActivity(iInspectionList);
			break;
		case R.id.savenext:
			comm = comments.getText().toString();
			String title_error = "";

			if (rdiochk == "1" || rdiochk == "2" || rdiochk == "3"
					|| rdiochk == "4") {
				switch (Integer.parseInt(rdiochk)) {
				case 1:
					if (rdiotoenailoption1.isChecked()
							|| rdiotoenailoption2.isChecked()) {
						che_status = true;

					} else {
						che_status = false;
						title_error = "Please select option for Toenail";
					}

					break;
				case 2:
					if (chkclipsmincond1.isChecked()
							&& chkclipsmincond2.isChecked()
							&& (rdioclipoption1.isChecked() || rdioclipoption2
									.isChecked())) {
						che_status = true;
						roofclipsminvalue = 2;
						roofclipssingleminvalue = 0;
						roofclipsdoubleminvalue = 0;
					} else if (chkclipsmincond1.isChecked()
							^ chkclipsmincond2.isChecked()) {
						che_status = false;
						title_error = "Please select minimum option for Clips";

					} else if (!(rdioclipoption1.isChecked() || rdioclipoption2
							.isChecked())) {
						che_status = false;
						title_error = "Please select option for Clips";
					} else {
						che_status = false;
						title_error = "Please select minimum option for Single Wraps  ";
					}

					break;
				case 3:
					if (chksingleclipsmincond1.isChecked()
							&& chksingleclipsmincond2.isChecked()
							&& rdiosinglewrapoption1.isChecked()) {
						che_status = true;
						roofclipsminvalue = 0;
						roofclipssingleminvalue = 2;
						roofclipsdoubleminvalue = 0;
					} else if (chksingleclipsmincond1.isChecked()
							^ chksingleclipsmincond2.isChecked()) {
						che_status = false;
						title_error = "Please select minimum option for Single Wraps";

					} else if (!rdiosinglewrapoption1.isChecked()) {
						che_status = false;
						title_error = "Please select option for Single Wraps";
					} else {
						che_status = false;
						title_error = "Please select minimum option for Single Wraps  ";
					}

					break;
				case 4:
					if (chkdoubleclipsmincond1.isChecked()
							&& chkdoubleclipsmincond2.isChecked()
							&& (rdiodoublewrapoption1.isChecked() || rdiodoublewrapoption2
									.isChecked())) {
						che_status = true;
						roofclipsminvalue = 0;
						roofclipssingleminvalue = 0;
						roofclipsdoubleminvalue = 2;
					} else if (chkdoubleclipsmincond1.isChecked()
							^ chkdoubleclipsmincond2.isChecked()) {
						che_status = false;
						title_error = "Please select minimum option for Double Wraps";

					} else if (!(rdiodoublewrapoption1.isChecked() || rdiodoublewrapoption2
							.isChecked())) {
						che_status = false;
						title_error = "Please select option for Double Wraps";
					} else {
						che_status = false;
						title_error = "Please select minimum option for Double Wraps";
					}

					break;
				}
			}
			if (rdioToenail.isChecked() == true
					|| rdioClips.isChecked() == true
					|| rdioSinglewraps.isChecked() == true
					|| rdioDoubleWraps.isChecked() == true
					|| rdioE.isChecked() == true || rdioF.isChecked() == true
					|| rdioG.isChecked() == true || rdioH.isChecked() == true) {

				if (rdioF.isChecked() == true) {
					othertext = txtroofwallother.getText().toString();
					if (othertext.trim().equals("")) {
						che_status = false;
						title_error = "Please enter the text for Other Roof Wall attachment.";
					} else {
						che_status = true;
					}
				}
				if (comm.trim().equals("")) {
					cf.ShowToast("Please enter the Comments for Roof Wall attachment.",1);
				} else if (!che_status) {
					cf.ShowToast(title_error,1);
					title_error = "";
					che_status = true;
				} else {
					try {
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.tbl_questionsdata + " WHERE SRID='"
								+ cf.Homeid + "'", null);
						int rws = c2.getCount();
						if (rws == 0) {

							cf.db.execSQL("INSERT INTO "
									+ cf.tbl_questionsdata
									+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
									+ "VALUES ('" + cf.Homeid + "','','','" + 0
									+ "','" + 0 + "','" + 0 + "','','','','','"
									+ 0 + "','" + 0 + "','','" + rdiochk
									+ "','" + suboption + "','"
									+ roofclipsminvalue + "','"
									+ roofclipssingleminvalue + "','"
									+ roofclipsdoubleminvalue + "','"
									+ cf.convertsinglequotes(othertext) + "','"
									+ 0 + "','" + 0 + "','" + 0 + "','','" + 0
									+ "','','" + 0 + "','" + 0
									+ "','','','','','','','','" + 0 + "','"
									+ 0 + "','" + 0 + "','" + 0 + "','" + 0
									+ "','" + 0 + "','" + 0 + "','" + 0
									+ "','','" + 0 + "','" + 0 + "','','" + 0
									+ "','','','" + cd + "','" + 0 + "')");

						} else {

							cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
									+ " SET RooftoWallValue ='" + rdiochk
									+ "',RooftoWallSubValue='" + suboption
									+ "',RooftoWallClipsMinValue='"
									+ roofclipsminvalue
									+ "',RooftoWallSingleMinValue='"
									+ roofclipssingleminvalue
									+ "',RooftoWallDoubleMinValue='"
									+ roofclipsdoubleminvalue
									+ "',RooftoWallOtherText='"
									+ cf.convertsinglequotes(othertext) + "'"
									+ " WHERE SRID ='" + cf.Homeid.toString()
									+ "'");
						}
						updatecnt = "1";

						Cursor c5 = cf.db.rawQuery("SELECT * FROM "
								+ cf.tbl_comments + " WHERE SRID='" + cf.Homeid
								+ "'", null);
						int rws5 = c5.getCount();
						if (rws5 == 0) {
							cf.db.execSQL("INSERT INTO "
									+ cf.tbl_comments
									+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment,RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
									+ " VALUES ('" + cf.Homeid + "','"
									+ inspectortypeid + "','','','','','','','"
									+ cf.convertsinglequotes(comm)
									+ "','','','','','','','','','','','','','"
									+ cd + "')");
						} else {
							cf.db.execSQL("UPDATE " + cf.tbl_comments
									+ " SET RoofWallComment='"
									+ cf.convertsinglequotes(comm)
									+ "',RoofWallAdminComment='',CreatedOn ='"
									+ md + "'" + " WHERE SRID ='"
									+ cf.Homeid.toString() + "'");
						}
						cf.getInspectorId();

						Cursor c3 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c3.getCount();
						if (subchkrws == 0) {
							cf.db.execSQL("INSERT INTO "
									+ cf.SubmitCheckTable
									+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
									+ "VALUES ('" + cf.InspectorId + "','"
									+ cf.Homeid
									+ "',0,0,0,0,1,0,0,0,0,0,0,0,0,0,0)");
						} else {
							cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
									+ " SET fld_roofwall='1' WHERE Srid ='"
									+ cf.Homeid + "' and InspectorId='"
									+ cf.InspectorId + "'");

						}
						updatecnt = "1";
						cf.changeimage();
						// roofwalltick.setVisibility(visibility);
					} catch (Exception e) {

						updatecnt = "0";
						cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
						//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in inserting or updating roofwall comments on arrow buttton click on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

					}

				}
			} else {
				cf.ShowToast("Please select the Roof Wall attachment.",1);

			}
			if (updatecnt == "1") {
				cf.ShowToast("Roof Wall attachment has been saved successfully.",1);
				iInspectionList = new Intent(RoofWall.this, RoofGeometry.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("iden", "test");
				iInspectionList.putExtra("InspectionType", cf.InspectionType);
				iInspectionList.putExtra("status", cf.status);
				iInspectionList.putExtra("keyName", cf.value);
				iInspectionList.putExtra("Count", cf.Count);
				startActivity(iInspectionList);
			}

			break;

		}

	}

	private void getInspectortypeid() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.mbtblInspectionList,
				null);
		cur.moveToFirst();
		if (cur != null) {
			inspectortypeid = cur.getString(cur
					.getColumnIndex("InspectionTypeId"));
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(RoofWall.this, RoofDeck.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("iden", "prev");
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	   
	    if(focus==1)
	    {
	    	focus=0;
	    comments.setText(comments.getText().toString());    
	    }
	 }    
}