package idsoft.inspectiondepot.B11802;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HomeScreen extends Activity {
	public static final int NETWORK_TYPE_EHRPD = 14; // Level 11
	public static final int NETWORK_TYPE_EVDO_B = 12; // Level 9
	public static final int NETWORK_TYPE_HSPAP = 15; // Level 13
	public static final int NETWORK_TYPE_IDEN = 11; // Level 8
	public static final int NETWORK_TYPE_LTE = 13; // Level 11
	RelativeLayout rl;
	ImageView expandimage,min,home_inspectorphoto,check_new_insp;
	ScaleAnimation intial;
	AnimationSet animSetpar;
	ImageView log_expand,Inspimg, start, impor, comment, call, logo, expo, complete,inspector_photo,
			clsimg, search,inspectorphoto,reporttoit;
	String formattedDate2, insfname, inslname;
	TextView version, welcome, releasecode;
	protected static final int visibility = 0;
	private static final int DISPLAY_SHOW_HOME = 0;
	private static final String TAG = null;
	Button capt;
	TextView fname, lname, vname;
	RelativeLayout lin;TextView f_name,l_name,address,email,cmp;
	ImageView app_main_menu,img_update,exp;
	public CommonFunctions cf;
	int vcode;
	Thread myThread = null;
	ImageView onlineupdateforver,img_InspectorPhoto;
	String apk, newcode, newversion, error = "", dbhomeid,
			anytype = "anyType{}";
	ProgressDialog progressDialog, progressDialog1,pd;
	private AlertDialog alertDialog;
	private boolean active = true;
	public int k;
	String fld_firstname="",fld_lastname="",fld_address="",fld_companyname="",fld_email="";
    

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(1);
		cf.getInspectorId();
		cf.versionname=cf.getdeviceversionname();
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df2 = new SimpleDateFormat("MM-dd-yyyy");
		formattedDate2 = df2.format(c.getTime());
		this.releasecode = (TextView) this.findViewById(R.id.releasecode);
		cf.setRcvalue(releasecode);
		//this.releasecode.setText(cf.rcstr);
       
        this.version = (TextView) this.findViewById(R.id.versionname);
		cf.welcomemessage();
		((TextView)findViewById(R.id.welcomename)).setText(Html.fromHtml("<font color=#f7a218>" + "Welcome  "+"</font>"+ cf.data.toUpperCase()));
		((TextView)findViewById(R.id.insptypename)).setText(Html.fromHtml("<font color=#f7a218>" + "Inspection Type : "+"</font> 1802(Rev. 01/12)"));
		
		System.out.println("TextView.rcstr");
		home_inspectorphoto = (ImageView) findViewById(R.id.inspectorphoto1);
		img_InspectorPhoto = (ImageView) findViewById(R.id.inspectorphoto);
		f_name=(TextView) findViewById(R.id.insp_frst_name);
        l_name=(TextView) findViewById(R.id.insp_last_name);
        address=(TextView) findViewById(R.id.insp_address);
        email=(TextView) findViewById(R.id.insp_email);
        cmp=(TextView) findViewById(R.id.insp_cmp);
	        
	        
	        f_name.setText(cf.getsinglequotes(cf.insfname.toUpperCase()));
	        l_name.setText(cf.getsinglequotes(cf.inslname.toUpperCase()));
	        
	        if(cf.insaddress.equals("")){
	        	address.setText("N/A");	
	        }else{
	        	address.setText(cf.insaddress.toUpperCase());
	        }
	        
	        if(cf.insemail.equals("")){
	        	email.setText("N/A");	
	        }else{
	        	email.setText(cf.insemail.toLowerCase());
	        }
	        
	        if(cf.inscompname.equals("")){
	        	cmp.setText("N/A");	
	        }else{
	        	cmp.setText(cf.inscompname.toUpperCase());
	        }
	        System.out.println("displayinspimage");
	        
		displayinspimage();
		// for upgrade the application function Ends here
		
			Runnable runnable = new CountDownRunner();
			myThread = new Thread(runnable);
			myThread.start();
			try {
				// vcode = getPackageManager().getPackageInfo(getPackageName(),
				// 0).versionCode;
				String vn = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
				version.setText("Version "+vn);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rl=(RelativeLayout) findViewById(R.id.animating);
			 rl.setVisibility(View.INVISIBLE);   System.out.println("application_sta");
		if(cf.application_sta){
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			try {
				String value = extras.get("upgrade").toString();
				final String newversion1 = extras.get("newversion").toString();
				if (value.equals("true")) {
					alertDialog = new AlertDialog.Builder(HomeScreen.this)
							.create();
					alertDialog
					.setMessage("New Version "+newversion1+" of B1-1802 inspection is available.The Previous data will not be affected, Would you like to upgrade?");
					alertDialog.setButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									try {
										if (alertDialog.isShowing()) {

											alertDialog.cancel();

											alertDialog.dismiss();
  
										}
										progressDialog1 = ProgressDialog
												.show(HomeScreen.this,
														"",
														"Please wait Upgrading B1-1802 Inspection Version "
																+ newversion1
																+ " will take a few minutes ...");
										new Thread() {
											public void run() {
												// Looper.prepare();
												try {
													Update();
													
												} catch (SocketTimeoutException s) {
													k = 5;
													handler.sendEmptyMessage(0);
												} catch (NetworkErrorException n) {
													k = 5;
													handler.sendEmptyMessage(0);

												} catch (IOException io) {
													k = 5;
													handler.sendEmptyMessage(0);

												} catch (XmlPullParserException x) {
													k = 5;
													handler.sendEmptyMessage(0);

												} catch (Exception e) {
													System.out
															.println("eee="+e.getMessage());
													k = 8;
													handler.sendEmptyMessage(0);
												}
											};

											private Handler handler = new Handler() {
												public void handleMessage(
														Message msg) {
													progressDialog1.dismiss();

													if (k == 2) {
														cf.ShowToast("Internet connection is not available.",1);

													} else if (k == 8) {
														cf.ShowToast("Error in login please try again latter .",1);

													} else if (k == 5) {
														cf.ShowToast("There is a problem on your Network. Please try again later with better Network..",1);

													}

												}
											};
										}.start();

									} catch (Exception e) {
										System.out
												.println("problem while upgrade"
														+ e);
									}
								}
							});
					
					  alertDialog.setButton2("No", new DialogInterface.OnClickListener() {  
					      public void onClick(DialogInterface dialog, int which) 
					      {  
					    	  alertDialog.cancel();
					      }
					    }); 
					
					alertDialog.show();
				}

			} catch (Exception e) {
				System.out.println("errorsdsd" + e.getMessage());
			}
		}
		start = (ImageView) findViewById(R.id.Buttonstartinspect);
		start.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent Star = new Intent(HomeScreen.this,
						Startinspections.class);
				Star.putExtra("keyName", 1);
				startActivity(Star);
			}

		});

		impor = (ImageView) findViewById(R.id.Buttonimport);
		impor.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				Intent imp = new Intent(HomeScreen.this, Import.class);
				startActivity(imp);
				
			}

		});

		expo = (ImageView) findViewById(R.id.Buttontexport);
		expo.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent exp1 = new Intent(HomeScreen.this, Exportedddata.class);
				exp1.putExtra("completedexport", "true");
				startActivity(exp1);
			}
		});

		complete = (ImageView) findViewById(R.id.Buttoncompletedinspect);
		complete.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				 cf.Createtablefunction(2);
				 cf.getInspectorId();
				 String sql="";
				  System.out.println("Ins "+cf.InspectorId);
				  sql = "select * from " + cf.mbtblInspectionList
					+ " where InspectorId = '"
					+ cf.InspectorId + "'";
				  Cursor cur = cf.db.rawQuery(sql, null);
				  int rws = cur.getCount();
				  if(rws>0)
				  {
					  startActivity(new Intent(HomeScreen.this,Deleteinspection.class));
				  }
				  else
				  {
					  cf.ShowToast("Sorry, No Inspection found please Import.", 1); 
				  }
				
			}

		});
		reporttoit = (ImageView) findViewById(R.id.ButtonReporttoIT);
		reporttoit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				Intent reptoit = new Intent(HomeScreen.this, ReporttoIT.class);
				startActivity(reptoit);

			}

		});

		comment = (ImageView) findViewById(R.id.Buttontoaddcomm);
		comment.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent addcomm = new Intent(HomeScreen.this, ToaddComment.class);
				startActivity(addcomm);
			}
		});
		
		app_main_menu= (ImageView) findViewById(R.id.app_main_menu); 	
		 if(cf.application_sta)
		 {	
			 app_main_menu.setVisibility(View.VISIBLE);
		 }
		 else
		 {
			 app_main_menu.setVisibility(View.GONE);
		 }
		 
		app_main_menu.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				 if(cf.application_sta)
				 {			 
					

						  Intent loginpage = new Intent(Intent.ACTION_MAIN);
							loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
							startActivity(loginpage);
							overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
				 }  
				 else 
				 {
					 cf.ShowToast("Plese install the application than press for the main menu",1);
					 
				 }
			}
			});
		
		check_new_insp=(ImageView) findViewById(R.id.check_new_insp);
		check_new_insp.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				System.out.println("check for new isp");
				getIDMA_application();
			}
			});
		
		onlineupdateforver = (ImageView) findViewById(R.id.onlineupdateforver);
		vcode = getcurrentversioncode();
		cf.Createtablefunction(16);
		
		onlineupdateforver.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cf.isInternetOn()) {
						
						progressDialog = ProgressDialog.show(HomeScreen.this, "",
								"Please wait...");
						new Thread() {
							private int usercheck = 0;

							public void run() {
								// Looper.prepare();
								try {
									if (getversioncodefromweb() == true) {
										if (k != 5 && k != 8) {
											usercheck = 1;
											
										}
										handler.sendEmptyMessage(0);
										progressDialog.dismiss();
									} else {
										
										progressDialog.dismiss();
										Intent imp = new Intent(HomeScreen.this,
												HomeScreen.class);
										imp.putExtra("newversion", newversion);
										imp.putExtra("upgrade", "true");
										imp.putExtra("userpermitted", "false");
										startActivity(imp);
										
									}

								} catch (SocketTimeoutException s) {
									k = 5;
									handler.sendEmptyMessage(0);
								} catch (NetworkErrorException n) {
									k = 5;
									handler.sendEmptyMessage(0);
								} catch (IOException io) {
									k = 5;
									handler.sendEmptyMessage(0);
								} catch (XmlPullParserException x) {
									
									k = 5;
									handler.sendEmptyMessage(0);
								} catch (Exception e) {
									progressDialog.dismiss();
									k = 8;
									handler.sendEmptyMessage(0);
								}
							};

							private Handler handler = new Handler() {
								@Override
								public void handleMessage(Message msg) {
								if (k == 5 || k == 8) {
										
									cf.ShowToast("There is a problem on your Network. Please try again later with better Network.. ",1);
									} else if (usercheck == 1) {
										usercheck = 0;
										cf.ShowToast("You don�t have any updates to upgrade your B1-1802 inspection.",1);
									}

								}
							};
						}.start();
						
					
					
				} else {
					cf.ShowToast("Please enable the Internet connection.",1);
				}
			}
		});
		
		
		search = (ImageView) findViewById(R.id.Buttonsearchinspec);
		search.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				final EditText tvxtstname,tvlstname,tvploicy;
				final Dialog dialog = new Dialog(HomeScreen.this);
				dialog.setContentView(R.layout.search);
				dialog.setTitle("This is an online search. Internet connection is required.");

				dialog.setCancelable(true);
				tvxtstname = (EditText) dialog.findViewById(R.id.txtfirstname);
				tvlstname = (EditText) dialog.findViewById(R.id.txtlastname);
				tvploicy = (EditText) dialog.findViewById(R.id.txtpolicy);
				tvxtstname.requestFocusFromTouch();
				Button btnsrch = (Button) dialog
						.findViewById(R.id.search);
				btnsrch.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if (tvxtstname.getText().toString().equals("")
								&& tvlstname.getText().toString().equals("")
								&& tvploicy.getText().toString().equals("")) {
							cf.ShowToast("Please enter any one of the fields to search.",1);
						} else {
							cf.hidekeyboard(tvxtstname);
							cf.hidekeyboard(tvlstname);
							cf.hidekeyboard(tvploicy);
							dialog.dismiss();
							show();
						}
					}

					private void show() {
						// TODO Auto-generated method stub
						AlertDialog.Builder builder = new AlertDialog.Builder(
								HomeScreen.this);
						builder.setTitle("Network Connection")
								.setMessage(
										" To search inspection, requires an internet connection. Do you have internet connection? ");
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
										NetworkInfo info = conMgr
												.getActiveNetworkInfo();
										if (info != null && info.isConnected()) {
											Intent srch = new Intent(
													HomeScreen.this,
													SearchInspection.class);
											srch.putExtra("first", tvxtstname
													.getText().toString());
											srch.putExtra("last", tvlstname
													.getText().toString());
											srch.putExtra("policy", tvploicy
													.getText().toString());
											startActivity(srch);
										} else {
											cf.ShowToast("Internet connection is not available.",1);
											cf.hidekeyboard();
										}
									}
								});
						builder.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										//cf.hidekeyboard();
									}
								});
						builder.show();
					}

				});
				Button btncls = (Button) dialog.findViewById(R.id.cls);
				btncls.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						cf.hidekeyboard(tvxtstname);
						cf.hidekeyboard(tvlstname);
						cf.hidekeyboard(tvploicy);
						dialog.dismiss();
					}

				});
				dialog.show();
				 
			}
			 
		});
		
		Animating_button();
		}
		else
        { //System.out.println("application_sta else" );
			AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(HomeScreen.this);
				alertDialog1
			.setMessage("In order to use the latest version of B1-1802 application, You must update IDMA application. Please click below to update IDMA application.");
			
          	alertDialog1.setPositiveButton("Download",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int which) {
					
					String source1 = "<font color=#FFFFFF>Please wait downloading IDMA will take few minutes....</font>";
					 pd = ProgressDialog.show(HomeScreen.this, "", Html.fromHtml(source1),true);
				        new Thread(new Runnable() {
				                public void run() {
				                	Looper.prepare();
				                	System.out.println("rrrun");
				                	downloadingapp();
				                	k=11;
				                	handler2.sendEmptyMessage(0);
				                }
				            }).start();


				}
			});
          
          	alertDialog1.setCancelable(false);
          	alertDialog1.show();
          
        }

		
	}
	public void Animating_button() {
		// TODO Auto-generated method stub
		/** Start animating for the  Check for updates**/

		 cf.Createtablefunction(2);

			Cursor c12 = cf.SelectTablefunction(cf.VersionshowTable, " ");   
			c12.moveToFirst();System.out.println("c12 "+c12.getCount());
			if (c12.getCount() >= 1) {
				
				   
				if (!c12.getString(1).trim().equals(String.valueOf(vcode).trim())) {
					try
					{
						int v_c=Integer.parseInt(c12.getString(1).trim());System.out.println("v_c "+v_c + " vcode" +vcode);
						if(v_c>vcode)
						{
							final ScaleAnimation zoom = new ScaleAnimation((float)1.00,(float)0.50, (float)1.00,(float)0.50,(float) 100,(float) 20); 
							final AnimationSet animSet = new AnimationSet(false);
							zoom.setRepeatMode(Animation.REVERSE);
							zoom.setRepeatCount(Animation.INFINITE);
							animSet.addAnimation(zoom);
							animSet.setDuration(1500);
							AlphaAnimation alpha = new AlphaAnimation(1, (float)0.5);
					        alpha.setDuration(1500); // Make animation instant
					        alpha.setRepeatMode(Animation.REVERSE);
					        alpha.setRepeatCount(Animation.INFINITE);
					        animSet.addAnimation(alpha);
					        
					        onlineupdateforver.setAnimation(animSet);
							animSet.start();
						}
					}
					catch(Exception e)
					{
						
					}
				
				} 
			}
			/*else
			{
				final ScaleAnimation zoom = new ScaleAnimation((float)1.00,(float)0.50, (float)1.00,(float)0.50,(float) 100,(float) 20); 
				final AnimationSet animSet = new AnimationSet(false);
				zoom.setRepeatMode(Animation.REVERSE);
				zoom.setRepeatCount(Animation.INFINITE);
				animSet.addAnimation(zoom);
				animSet.setDuration(1500);
				AlphaAnimation alpha = new AlphaAnimation(1, (float)0.5);
		        alpha.setDuration(1500); // Make animation instant
		        alpha.setRepeatMode(Animation.REVERSE);
		        alpha.setRepeatCount(Animation.INFINITE);
		        animSet.addAnimation(alpha);
		        
		        onlineupdateforver.setAnimation(animSet);
				animSet.start();
			}*/
			/** End animating for the  Check foru pdates**/
			/** Start animating for the  Check for new inspection starts **/
			 cf.Createtablefunction(20);
				Cursor c20 = cf.SelectTablefunction(cf.IDMAVersion, " ");   
				c20.moveToFirst();
				if(c20.getCount()>=1)
				{
				int old_v_c=0,new_v_c=0;
				for(int i=0;i<c20.getCount();i++)
				{/**New for the Version code in the applicaation **/
					 if(c20.getString(c20.getColumnIndex("ID_VersionType")).equals("IDMA_Old")) 
					 {
						 old_v_c=c20.getInt(c20.getColumnIndex("ID_VersionCode")); 
						 
					 } /**Old  for the Version code in the application which in the server to update**/
					 else if(c20.getString(c20.getColumnIndex("ID_VersionType")).equals("IDMA_New"))
					 {
						 new_v_c=c20.getInt(c20.getColumnIndex("ID_VersionCode")); 
					 }
					 System.out.println("i vallue ="+i);
					 c20.moveToNext();
			    }
				
					if (old_v_c<new_v_c || old_v_c==0) {
						try
						{
							
							
								//animatebutton(); // use the function for change the image for a
								final ScaleAnimation zoom1 = new ScaleAnimation((float)1.00,(float)0.50, (float)1.00,(float)0.50,(float) 100,(float) 20); 
								final AnimationSet animSet1 = new AnimationSet(false);
								zoom1.setRepeatMode(Animation.REVERSE);
								zoom1.setRepeatCount(Animation.INFINITE);
								animSet1.addAnimation(zoom1);
								animSet1.setDuration(1500);
								AlphaAnimation alpha1 = new AlphaAnimation(1, (float)0.5);
						        alpha1.setDuration(1500); // Make animation instant
						        alpha1.setRepeatMode(Animation.REVERSE);
						        alpha1.setRepeatCount(Animation.INFINITE);
						        animSet1.addAnimation(alpha1);
						        
								check_new_insp.setAnimation(animSet1);
								animSet1.start();
								System.out.println("No more issues ended");
							
						}
						catch(Exception e)  
						{
							
						}
					
				 
				}
			}
				else
				{
					check_new_insp.setVisibility(View.INVISIBLE);
				}
			/** Ends animating for the  Check for new inspection starts **/
/** Start animating for the  Inspector information  **/
				
				
				
			min=(ImageView)	findViewById(R.id.minimize);
			// rl=(RelativeLayout) findViewById(R.id.animating);
			 rl.setVisibility(View.VISIBLE);
			 TranslateAnimation slide = new TranslateAnimation(0, 0, 0,-1300 );   
			 slide.setDuration(0);   
			 slide.setFillAfter(true);   
			 rl.startAnimation(slide); 
			 exp=(ImageView) findViewById(R.id.expand);
			 exp.setVisibility(View.VISIBLE);
			 ImageView log_expand=(ImageView) findViewById(R.id.ImageView01);
			 exp.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						// TODO Auto-generated method stub
						rl.setVisibility(View.VISIBLE);  
						 TranslateAnimation slide = new TranslateAnimation(0, 0, -(rl.getHeight()+50),0 );   
						 slide.setDuration(1500);   
						 slide.setFillAfter(true);   
						 rl.startAnimation(slide); 
						 exp.setVisibility(View.INVISIBLE);
						 min.setVisibility(View.VISIBLE);
					}
				});
				min.setOnClickListener(new OnClickListener() {

					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						rl.setVisibility(View.VISIBLE);  
						 TranslateAnimation slide1 = new TranslateAnimation(0, 0, 0,-(rl.getHeight()+50));   
						 slide1.setDuration(1500);   
						 slide1.setFillAfter(true);   
						 rl.startAnimation(slide1); 

						 slide1.setAnimationListener(new AnimationListener() {
								
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub
									min.setVisibility(View.INVISIBLE);
								}

								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									exp.setVisibility(View.VISIBLE);

								}

								public void onAnimationRepeat(
										Animation animation) {
									// TODO Auto-generated method stub
									
								}
						 });		


					}

				 });
				
			 log_expand.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(exp.isShown())
					{
					 rl.setVisibility(View.VISIBLE);  
					 TranslateAnimation slide = new TranslateAnimation(0, 0, -(rl.getHeight()+50),0 );   
					 slide.setDuration(1500);   
					 slide.setFillAfter(true);   
					 rl.startAnimation(slide); 
					 exp.setVisibility(View.INVISIBLE);
					 min.setVisibility(View.VISIBLE);
					}
				}
			});
			 /** Ends animating for the  Inspector information  **/	
	}
	 private void getIDMA_application() {
	// TODO Auto-generated method stub

		if (cf.isInternetOn()) {
			progressDialog = ProgressDialog.show(this, "",
					"Please wait...");
			new Thread() {
				private int usercheck = 0;

				public void run() {
					// Looper.prepare();
					try {
						//System.out.println("check getIDMAversioncodefromweb new isp"+getIDMAversioncodefromweb());
						if (getIDMAversioncodefromweb() == true) {
							if (k != 5 && k != 8) {
								usercheck = 1;
							}
							handler.sendEmptyMessage(0);
							progressDialog.dismiss();
						} else {
							
							progressDialog.dismiss();  
							
							k=20;
							handler.sendEmptyMessage(0);

						}

					} catch (SocketTimeoutException s) {
						k = 5;
						handler.sendEmptyMessage(0);
					} catch (NetworkErrorException n) {
						k = 5;
						handler.sendEmptyMessage(0);
					} catch (IOException io) {
						k = 5;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException x) {
						k = 5;
						handler.sendEmptyMessage(0);
					} catch (Exception e) {
						progressDialog.dismiss();
						k = 8;
						handler.sendEmptyMessage(0);
					}
					
				};

				private boolean getIDMAversioncodefromweb() throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception {
					// TODO Auto-generated method stub
					 SoapObject request1 = new SoapObject(cf.NAMESPACE, "GetVersionInformation_IDMS");
						SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope1.dotNet = true;
						envelope1.setOutputSoapObject(request1);
						HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_IDMA);
				    	androidHttpTransport1.call(cf.NAMESPACE+"GetVersionInformation_IDMS", envelope1);
				    	
				    	SoapObject result1 = (SoapObject) envelope1.getResponse();
						
						SoapObject obj1 = (SoapObject) result1.getProperty(0);
						cf.newcode = String.valueOf(obj1.getProperty("VersionCode"));
						cf.newversion = String.valueOf(obj1.getProperty("VersionName"));
						System.out.println("version from service "+cf.newcode + " "+cf.newversion);
						int newvcode=0;
						try
						{
						newvcode=Integer.parseInt(cf.newcode);
						}catch (Exception e) {
							// TODO: handle exception
						}	
						if (!"null".equals(cf.newcode) && null != cf.newcode && !cf.newcode.equals("")) {
							System.out.println("inside updaate version");
							cf.Createtablefunction(20);
							Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," Where ID_VersionType='IDMA_Old' ");
							if(c12.getCount()>=1)
							{
								System.out.println("count version"+c12.getCount());
								c12.moveToFirst();
								int V_code=c12.getInt(c12.getColumnIndex("ID_VersionCode"));
								System.out.println("issues not here in "+V_code+"<"+newvcode);
								if(V_code<newvcode)
								{
									System.out.println("issues not here in ");
									k=20;
									return true;
									
								}
								else
								{
									System.out.println("issues camer here in ");
									usercheck=1;
									k=0;
									return true;
								}
								
							}
							else
							{
								k=5;
								return false;
							}
							
						}else
						{
							k=5;
							return false;
							
						}
						
				}

				private Handler handler = new Handler() {
				public int m=0;
				private AlertDialog alertDialog;

					@Override
					public void handleMessage(Message msg) {
						
						if (k == 5 || k == 8) {
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network.. ",1);
							progressDialog.dismiss();
						}else if(k==20)
						{
						
							alertDialog = new AlertDialog.Builder(HomeScreen.this)
							.create();
							alertDialog
							.setMessage("New IDMA Version of "
											+ cf.newversion
											+ " for IDMA is available,The Previous data will not be affected, Would you like to Upgrade.");
							
					alertDialog.setButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
						
							progressDialog1 = ProgressDialog
									.show(HomeScreen.this,
											"",
											"Please wait Upgrading Version  "
													+ cf.newversion
													+ " of IDMA application will take few minutes ...");
							new Thread() {
								public void run() {
									// Looper.prepare();
									
							try
							{
								
								upgrdingIDMS();
							} catch (SocketTimeoutException s) {
								m=1;
								//cf.ShowToast("You have problem in your server connection please conntact paperless inspection admin.",1);
							
							} catch (NetworkErrorException n) {
								m=2;
								//cf.ShowToast("You have internet problem please try again later.",1);
							
							} catch (IOException io) {
								m=1;
							//	cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
							
							} catch (XmlPullParserException x) {
								m=1;
								//cf.ShowToast("You have problem in your server connection please conntact paperless inspection admin.",1);
							} catch (Exception e) {
								m=8;
								//cf.ShowToast("You have problem in your upgrading .",1);
							}
								};

								

								private Handler handler = new Handler() {
									public void handleMessage(
											Message msg) {
										progressDialog1.dismiss();

										if (m == 2) {
											cf.ShowToast("Internet connection is not available.",1);

										} else if (m == 8) {cf.ShowToast("Error in login please try again latter .",1);

										} else if (m == 5) {
											cf.ShowToast("There is a problem on your Network. Please try again later with better Network..",1);

										}
										else if(m==1)
										{
											cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
										}

									}
								};
							}.start();
								}
					});
					  alertDialog.setButton2("No", new DialogInterface.OnClickListener() {  
					      public void onClick(DialogInterface dialog, int which) 
					      {  
					    	  alertDialog.cancel();
					      }
					    }); 
					alertDialog.show();
							
						}
						else if (usercheck == 1 && k!=20 ) {
						usercheck = 0;
							cf.ShowToast("You don�t have any new inspections.",1);
						}
						}

					};
				
			}.start();
		} else {
			cf.ShowToast("Please enable the Internet connection.",1);
		}
		
	 }
	 public void upgrdingIDMS() throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception
		{
			SoapObject webresult;
			SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);

			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

			androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);

			Object response = envelope.getResponse();

			byte[] b;
			if (cf.checkresponce(response)) // check the response is valid or
											// not
			{
				b = Base64.decode(response.toString());

				try {
					String PATH = Environment.getExternalStorageDirectory()
							+ "/Download/IDMS";
					File file = new File(PATH);
					file.mkdirs();

					File outputFile = new File(PATH + "/IDMS.apk");
					FileOutputStream fileOuputStream = new FileOutputStream(
							outputFile);
					fileOuputStream.write(b);
					fileOuputStream.close();

					//cf.fn_logout(cf.InspectorId); /* FOR LOGOUT*/
					Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," ");

					if (c12.getCount() < 1) {
						try {
							
							
							cf.db.execSQL("INSERT INTO "
									+ cf.IDMAVersion
									+ "(VId,ID_VersionCode,ID_VersionName,ID_VersionType)"
									+ " VALUES ('2','"+cf.newcode+"','" + cf.newversion
									+ "','IDMA_New')");

						} catch (Exception e) {
							
						}

					} else {
						try {
							cf.db.execSQL("UPDATE " + cf.IDMAVersion
									+ " set ID_VersionCode='"+cf.newcode+"',ID_VersionName='"
									+ cf.newversion
									+ "' Where  ID_VersionType='IDMA_New'");
						} catch (Exception e) {
						
						}
					}
					
					progressDialog1.dismiss();
					
					Intent intent = new Intent(Intent.ACTION_VIEW);  
					intent.setDataAndType(Uri.fromFile(new File(Environment
							.getExternalStorageDirectory()
							+ "/Download/IDMS/"
							+ "IDMS.apk")),
							"application/vnd.android.package-archive");
				//	startActivity(intent);
					startActivityForResult(intent, 0);
					
				} catch (IOException e) {
					cf.ShowToast("Update error!",1);
				}
			} else {
				progressDialog1.dismiss();
				cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
			}	
		}
	private void downloadingapp()
{
	
	SoapObject webresult;			
	SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.dotNet = true;	
	System.out.println("Inp "+cf.InspectorId);
	//request.addProperty("InspectorID",cf.InspectorId);
	envelope.setOutputSoapObject(request);	
	System.out.println("request downlod "+request);
	HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

	try {
		androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);
		System.out.println("RESPONSE "+envelope.getResponse());
		Object response = envelope.getResponse();
		
		byte[] b;
		if (cf.checkresponce(response)) // check the response is valid or
										// not
		{
			b = Base64.decode(response.toString());

			try {
				String PATH = Environment.getExternalStorageDirectory()
						+ "/Download/IDMA";
				File file = new File(PATH);
				file.mkdirs();

				File outputFile = new File(PATH + "/IDMA.apk");
				FileOutputStream fileOuputStream = new FileOutputStream(outputFile);
				fileOuputStream.write(b);
				fileOuputStream.close();
				cf.fn_logout(cf.InspectorId);  //FOR LOGOUT
				
			} catch (IOException e) {
				cf.ShowToast("Update error!",1);
			}
		}
		else {
			k=22;
			
		}
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (XmlPullParserException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

}



private Handler handler2 = new Handler() {
	public void handleMessage(Message msg) {
		pd.dismiss();
		if(k==11)
		{
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(Environment
					.getExternalStorageDirectory()
					+ "/Download/IDMA/"
					+ "IDMA.apk")),
					"application/vnd.android.package-archive");
			startActivity(intent);
		}
		else if(k==22)
		{
			cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
		}
		
	}
};


	private void displayinspimage() { //Display the inspector photo from the external storage.
		// TODO Auto-generated method stub
		
			try
			{
				Cursor c = cf.SelectTablefunction(cf.inspectorlogin,
						" where Ins_Id='" + cf.InspectorId.toString() + "'");
		
				if(c.getCount()>0)
				{
					
		
					c.moveToFirst();
					if(c!=null)
					{
						String inspext = cf.getsinglequotes(c.getString(c.getColumnIndex("Ins_PhotoExtn")));
						File outputFile = new File(this.getFilesDir()+"/"+cf.InspectorId.toString()+inspext);
					
						BitmapFactory.Options o = new BitmapFactory.Options();
						o.inJustDecodeBounds = true;
						BitmapFactory.decodeStream(new FileInputStream(outputFile),
								null, o);
						final int REQUIRED_SIZE = 200;
						int width_tmp = o.outWidth, height_tmp = o.outHeight;
						int scale = 1;
						while (true) {
							if (width_tmp / 2 < REQUIRED_SIZE
									|| height_tmp / 2 < REQUIRED_SIZE)
								break;
							width_tmp /= 2;
							height_tmp /= 2;
							scale *= 2;
						}
						BitmapFactory.Options o2 = new BitmapFactory.Options();
						o2.inSampleSize = scale;
						Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
								outputFile), null, o2);
						BitmapDrawable bmd = new BitmapDrawable(bitmap);
						
						home_inspectorphoto.setImageDrawable(bmd);
						img_InspectorPhoto.setImageDrawable(bmd);
					}
				}
				
				
		}
		catch (IOException e)
		{
			System.out.println("display image cac "+e.getMessage());
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ HomeScreen.this+" "+" in the stage of retrieving inspector image at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}
		
	}

	private void animatebutton() {
		// TODO Auto-generated method stub
		try {

			new Thread() {
				private boolean img1;

				public void run() {
					int count = 1;
					while (active) {
						try {
							this.sleep(500);
						} catch (InterruptedException e) {

							e.printStackTrace();

						}

						if ((count % 2) == 1) {

							img1 = false;

							handler.sendEmptyMessage(0);

						} else {
							img1 = true;
							handler.sendEmptyMessage(0);
						}

						count++;
					}
				};

				private Handler handler = new Handler() {

					public void handleMessage(Message msg) {

						if (img1) {
							onlineupdateforver
									.setImageResource(R.drawable.checkforupdateversion1);
						} else {
							onlineupdateforver
									.setImageResource(R.drawable.blink);
						}

					}
				};

			}.start();
		} catch (Exception e) {

		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}
	}

	public void doWork() {
		runOnUiThread(new Runnable() {
			public void run() {
				try {
					TextView txtCurrentTime = (TextView) findViewById(R.id.txtdate);
					Date dt = new Date();
					int hours = dt.getHours();
					int minutes = dt.getMinutes();
					int seconds = dt.getSeconds();
					String curTime = hours + ":" + minutes + ":" + seconds;

					txtCurrentTime.setText(formattedDate2 + " " + curTime);
				} catch (Exception e) {
				}
			}
		});
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent loginpage = new Intent();
			if(cf.application_sta)
			 {			 
				  loginpage = new Intent(Intent.ACTION_MAIN);
				  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
					
			 }  
			 else 
			 {
				 loginpage.setClassName("idsoft.inspectiondepot.B11802","idsoft.inspectiondepot.B11802.InspectionDepot");
				 loginpage.putExtra("back", "exit");				 
			 }
			startActivity(loginpage);
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}


	public boolean getversioncodefromweb() throws NetworkErrorException,
			XmlPullParserException, IOException, SocketTimeoutException,
			Exception {
		try {
			SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME3);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
			try {
				androidHttpTransport.call(cf.SOAP_ACTION3, envelope);
				SoapObject result = (SoapObject) envelope.getResponse();
				SoapObject obj = (SoapObject) result.getProperty(0);
				newcode = String.valueOf(obj.getProperty("VersionCode"));
				newversion = String.valueOf(obj.getProperty("VersionName"));
				
				if (!"null".equals(newcode) && null != newcode
						&& !newcode.equals("")) {
					
					int v_c=Integer.parseInt(newcode);
					System.out.println("v_code Db ="+v_c+"Version code ="+vcode);     
					
					if(v_c>vcode)
					{
						return false;
					}
					else 
					{
						progressDialog.dismiss();
						return true;
					}
					
				} else {
					return true;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw e;

			}
		} catch (SocketTimeoutException s) {
			k = 5;
			return true;
		} catch (NetworkErrorException n) {
			k = 5;
			return true;
		} catch (IOException io) {
			k = 5;
			return true;
		} catch (XmlPullParserException x) {
			k = 5;
			return true;
		} catch (Exception e) {
			k = 8;
			return true;
		}

	}
	
	private int getcurrentversioncode() {
		// TODO Auto-generated method stub

		try {
			vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vcode;
	}

	

	public void Update() throws NetworkErrorException, IOException,
			SocketTimeoutException, XmlPullParserException, Exception {
		SoapObject webresult;
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME32);		
		request.addProperty("InspectorID", Integer.parseInt(cf.InspectorId));		
		envelope.setOutputSoapObject(request);System.out.println("request"+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.SOAP_ACTION32, envelope);
		String response = envelope.getResponse().toString();System.out.println("reespo "+response);
		
		
		
		byte[] b;
	
		if (cf.checkresponce(response)) // check the response is valid or
										// not
		{
			b = Base64.decode(response.toString());
			try {
				String PATH = Environment.getExternalStorageDirectory()
						+ "/Download";
				File file = new File(PATH);
				file.mkdirs();

				File outputFile = new File(PATH + "/InspectionDepot.apk");
				FileOutputStream fileOuputStream = new FileOutputStream(
						outputFile);
				fileOuputStream.write(b);
				fileOuputStream.close();
					try
					{	cf.db.execSQL("UPDATE " + cf.inspectorlogin
											+ " SET Ins_Flag1=0" + " WHERE Ins_Id ='"
										+ cf.InspectorId + "'");
					}
					catch(Exception e)
					{
						
						System.out.println("dsfshjgjgs"+e.getMessage());
					}
				Cursor c12 = cf.SelectTablefunction(cf.VersionshowTable," ");
				if (c12.getCount() < 1) {
					
					try {
						cf.db.execSQL("INSERT INTO "
								+ cf.VersionshowTable
								+ "(VId,VersionCode,VersionName,ModifiedDate)"
								+ " VALUES ('1','"+newcode+"','" + newversion
								+ "',date('now'))");

					} catch (Exception e) {
						System.out.println("INSERT "+e.getMessage());
					}
					
				} else {
					try {
						cf.db.execSQL("UPDATE " + cf.VersionshowTable
								+ " set VersionCode='"+newcode+"',VersionName='"
								+ newversion
								+ "'");
					} catch (Exception e) {
						System.out.println(" UPDATE "+e.getMessage());
					}
				}

				
				progressDialog1.dismiss();
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(new File(Environment
						.getExternalStorageDirectory()
						+ "/Download/"
						+ "InspectionDepot.apk")),
						"application/vnd.android.package-archive");
				System.out.println("heer ");
				//startActivity(intent);
				startActivityForResult(intent, 0);
				System.out.println("ends comp ");
			} catch (Exception e) {
				cf.ShowToast("Update error!",1);
			}
		} else {
			progressDialog1.dismiss();
			cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
		}
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				System.out.println("reques");
				System.out.println("requestCode "+requestCode+" data "+data);
				System.out.println("insde case 0"+cf.application_sta);
				try
				{
				SoapObject request = new SoapObject(cf.NAMESPACE, cf.METHOD_NAME3);
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);

				androidHttpTransport.call(cf.SOAP_ACTION3, envelope);
				SoapObject result = (SoapObject) envelope.getResponse();
				SoapObject obj = (SoapObject) result.getProperty(0);
				newcode = String.valueOf(obj.getProperty("VersionCode"));
				newversion = String.valueOf(obj.getProperty("VersionName"));
				if (!"null".equals(newcode) && null != newcode && !newcode.equals("")) {
					if (Integer.toString(vcode).equals(newcode)) {
						
					} else {
						System.out.println("inside else"+newcode+" "+vcode);
						try {
							cf.Createtablefunction(16);
							Cursor c12 = cf.SelectTablefunction(cf.VersionshowTable,
									" ");
							System.out.println("inside count"+c12.getCount());
							if (c12.getCount() < 1) {
								
								try {
									cf.db.execSQL("INSERT INTO "
											+ cf.VersionshowTable
											+ "(VId,VersionCode,VersionName,ModifiedDate)"
											+ " VALUES ('1','"+newcode+"','" + newversion
											+ "',date('now'))");

								} catch (Exception e) {
									Log.i(TAG, "categorytableerror=" + e.getMessage());
								}

							} else {
								try {
									cf.db

									.execSQL("UPDATE " + cf.VersionshowTable
											+ " set VersionCode='"+newcode+"',VersionName='"
											+ newversion
											+ "',ModifiedDate =date('now')");
								} catch (Exception e) {
									Log.i(TAG, "categorytableerror=" + e.getMessage());
								}

							}							
						} catch (Exception e) {}
					}
				} else {
					
				}
				}
				catch(Exception e)
				{
					System.out.println("cancel replsace");
				}
				if(cf.application_sta)
				{
					System.out.println("came here");
					Intent intent1 = new Intent(Intent.ACTION_MAIN);
					intent1.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
					startActivity(intent1);System.out.println("ends here");
				}else
				{
					Intent Star = new Intent(HomeScreen.this,
							InspectionDepot.class);
					//Star.putExtra("keyName", 1);
					startActivity(Star);
				}
				
				break;
		}
		}
	public void onStop() {
		active = false;
	/*	onDestroy();
		finish();
	*/	
		super.onStop();
	
	}

	public void onResume() {
		active = true;
		super.onResume();
		//  cf.getInspectorId();
		  //  System.out.println("Inds "+cf.Insp_id);
	}
}