package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.OverallComments.OC_textwatcher;
import idsoft.inspectiondepot.B11802.OverallComments.Touch_Listener;

import java.util.Calendar;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
//import android.net.sip.SipAudioCall.Listener;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RoofCover extends Activity {
	protected static final int DATE_DIALOG_ID = 0;
	int chk;
	public int focus=0;
	EditText comments, txtother;
	TableLayout tblcommentschange;
LinearLayout roofcover_parrant,roofcover_type;
	Intent iInspectionList;
	String roofcovertypeprev, roofcovervalueprev, roofcoverothrtxtprev,
			roofcoverappindateprev, roofcoveryearofinsprev,
			roofcoverprodappprev, roofcovernoofinfoprovprev, helpcontent;

	TextView nocommentsdisp, txtproductasphalt, txtproductother,roofcover_TV_type1,
			txtproductmembrane, txtproductbuiltup, txtproductmetal,
			txtproductconcrete, txroofheading, txtpermitasphalt,
			txtpermitconcrete, txtpermitmetal, txtpermitbuiltup,
			txtpermitmembrane, txtpermitother,helptxt;
	Spinner txtyearasphalt, txtyearconcrete, txtyearmetal, txtyearbuiltup,
			txtyearmembrane, txtyearother;
	RadioButton rdioasphalt, rdioconcrete, rdiometal, rdiobuiltup,
			rdiomembrane, rdioother, rdioFBC, rdioSFBC, rdioAorB, rdioNone;
	Button getdate1, getdate2, getdate3, getdate4, getdate5, getdate6,
			saveclose, prev;
	String roofcomment1, roofcomment2, roofcomment3;
	String roofcoveradmin, commdescrip = "";
	int mYear, mMonth, mDay, value, Count, chkrws;
	int viewimage = 1;
	CheckBox chkasphalt, chkconcrete, chkmetal, chkbuiltup, chkmembrane,
			chkother;
	String inspectortypeid;
	CheckBox chkasphaltroofcovertype, chkconcreteroofcovertype,
			chkmetalroofcovertype, chkbuiltuproofcovertype,
			chkmembraneroofcovertype, chkotherroofcovertype;
	String count_value, othercovertext = "", InspectionType, status, rdiochk,
			comm, permitdate, prodapproval, yearorigins, homeId,
			roofcovervalue, othertext = "", updatecnt, identity,
			roofcovercomment;
	TextView Vimage;
	int commentsch;
	CheckBox chkasphaltcovertype, chkconcretecovertype, chkmetalcovertype,
			chkbuiltcovertype, chkmembranecovertype, chkothercovertype;
	int optionid, roofasphaltvalue, roofconcretevalue, roofmetalvalue,
			roofbuiltvalue, roofmembranevalue, roofothervalue;
	String commentsfill, yearvalasp = "", yearvalconc = "", yearvalmetal = "",
			yearvalbuiltup = "", yearvalmemb = "", yearvalother = "";
	int yearinfo1 = 0, yearinfo2 = 0, yearinfo3 = 0, yearinfo4 = 0,
			yearinfo5 = 0, yearinfo6 = 0;
	boolean staus_autoselect[] = { false, false, false };
	RadioButton radio_most[] = new RadioButton[6];
	TextView miameweblinkopen, fbcweblinkopen;
	AlertDialog alertDialog;
	View vv;
	Map<String, boolean[]> set_auto_select = new HashMap<String, boolean[]>();
	public String selected_check_list;
	CheckBox temp_st;
	final boolean[] b = { false, false, false, false };
	String roofdatevalue = "", permitdateasphalt = "",
			prodapprovalasphalt = "", yearasphalt = "",
			permitdateconcrete = "", prodapprovalconcrete = "",
			yearconcrete = "", permitdatemetal = "", prodapprovalmetal = "",
			yearmetal = "", permitdatebuiltup = "", prodapprovalbuiltup = "",
			yearbuiltup = "", permitdatemembrane = "",
			prodapprovalmembrane = "", yearmembrane = "", permitdateother = "",
			prodapprovalother = "", yearother = "";
	private TableLayout tbllayout8;
	CheckBox[] cb;
	private TableLayout exceedslimit1;
	ListView list;
	int inc, predominant = 0;
	LinearLayout lincomments;
	String descrip, status1, status2, status3, buildcomment1, buildcomment2,
			buildcomment3, builcodeadmin = "";
	String[] arrcomment1;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	private static final String inspectorlogin = "tbl_inspectorlogin";
	String miamelink, fbclink, conchkbox, comm2;
	String roofconcrete, roofasphalt, roofmetal, roofbuiltup, roofother,
			roofmembrane;
	public String chkstatus[] = { "true", "true", "true", "true", "true",
			"true", "true" }, chkstatus1[] = { "true", "true", "true", "true",
			"true", "true", "true" }, chkdatestatus[] = { "true", "true",
			"true", "true", "true", "true", "true" };

	String yearb[] = { "Select", "1990", "1991", "1992", "1993", "1994",
			"1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002",
			"2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010",
			"2011", "2012","2013" };
	View v1;
	public String chkboxvalidation[] = { "", "", "", "", "", "" };
	public int RoofCoverValue[] = { 0, 0, 0, 0, 0, 0 };
	TextView prevmitidata;
	CommonFunctions cf;

	private static final int visibility = 0;
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		cf.Createtablefunction(12);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.Homeid = homeId = bunhomeId.getString("homeid");
			cf.Identity = identity = bunhomeId.getString("iden");
			cf.InspectionType = InspectionType = bunhomeId
					.getString("InspectionType");
			cf.status = status = bunhomeId.getString("status");
			cf.value = value = bunhomeId.getInt("keyName");
			cf.Count = Count = bunhomeId.getInt("Count");

		}
		setContentView(R.layout.roofcover);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.changeimage();
		cf.getDeviceDimensions();focus=1;
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 22,
				cf, 1));
		
		ScrollView scr=(ScrollView)findViewById(R.id.scr);
	    scr.setSmoothScrollingEnabled(true);
	    scr.smoothScrollTo(0,0);
		  HorizontalScrollView hscr=(HorizontalScrollView)findViewById(R.id.HorizontalScrollView01);
		   hscr.smoothScrollTo(0,0);
		
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		
		getcountname();
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			inc = 1;
		} else {
			inc = 2;
		}

		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				 if(roofcovervalue==null) {
					 cf.alertcontent = "To see help comments, please select Roof Covering options.";
					}
					else if (roofcovervalue.equals("1") || rdioFBC.isChecked()) {
						cf.alertcontent = "This home was verified as meeting the requirements of selection A of the OIR B1 -1802 Question 2, Roof Covering.";
					} else if (roofcovervalue.equals("2") || rdioSFBC.isChecked()) {
						cf.alertcontent = "This home was verified as meeting the requirements of selection B of the OIR B1 -1802 Question 2 Roof Covering.";
					} else if (roofcovervalue.equals("3") || rdioAorB.isChecked()) {
						cf.alertcontent = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
					} else if (roofcovervalue.equals("4") || rdioNone.isChecked()) {
						cf.alertcontent = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
					} else {
						cf.alertcontent = "To see help comments, please select Roof Covering options.";
					}

				 	cf.showhelp("HELP",cf.alertcontent);
				
			}
		});
		
		
		txroofheading = (TextView) findViewById(R.id.txtheadingroofcover);
		txroofheading
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>Select all roof covering types in use. Provide the permit application date OR FBC/MDC Product Approval number OR Year of Original Installation/Replacement OR indicate that no information was available to verify compliance for each roof covering identified."));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		

		try {

			Cursor c2 = cf.db.rawQuery("SELECT * FROM "
					+ cf.quetsionsoriginaldata + " WHERE homeid='" + cf.Homeid
					+ "'", null);
			if(c2.getCount()>0){
				c2.moveToFirst();
				String bcoriddata = cf.getsinglequotes(c2.getString(c2.getColumnIndex("rcoriginal")));
				prevmitidata.setText(Html
					.fromHtml("<font color=blue>Original Value : " + "</font>"
							+ "<font color=red>" + bcoriddata + "</font>"));
			}
			else
			{
				prevmitidata.setText(Html
						.fromHtml("<font color=blue>Original Value : "
								+ "</font>" + "<font color=red>Not Available</font>"));
			}

		} catch (Exception e) {
			//cf.Error_LogFile_Creation(RoofCover.this +" problem in getting roof cover original value on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);


		}
		this.tbllayout8 = (TableLayout) this.findViewById(R.id.tableLayoutComm);
		this.lincomments = (LinearLayout) this
				.findViewById(R.id.linearlayoutcomm);
		// set the object for the text radio button in
		radio_most[0] = (RadioButton) findViewById(R.id.most_rdio1);
		radio_most[1] = (RadioButton) findViewById(R.id.most_rdio2);
		radio_most[2] = (RadioButton) findViewById(R.id.most_rdio3);
		radio_most[3] = (RadioButton) findViewById(R.id.most_rdio4);
		radio_most[4] = (RadioButton) findViewById(R.id.most_rdio5);
		radio_most[5] = (RadioButton) findViewById(R.id.most_rdio6);
		for (int i = 0; i <= 5; i++) {
			radio_most[i].setEnabled(false);
			radio_most[i].setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					int check = 7;
					switch (arg0.getId()) {
					case R.id.most_rdio1:
						check = 0;
						predominant = 1;
						break;
					case R.id.most_rdio2:
						check = 1;
						predominant = 2;
						break;
					case R.id.most_rdio3:
						check = 2;
						predominant = 3;
						break;
					case R.id.most_rdio4:
						check = 3;
						predominant = 4;
						break;
					case R.id.most_rdio5:
						check = 4;
						predominant = 5;
						break;
					case R.id.most_rdio6:
						check = 5;
						predominant = 6;
						break;

					}
					for (int i = 0; i <= 5; i++) {
						if (check == i) {
							radio_most[i].setChecked(true);
						} else {
							radio_most[i].setChecked(false);
						}

					}

				}

			});
		}

		miameweblinkopen = (TextView) this
				.findViewById(R.id.miamedadeprodapprlink);
		fbcweblinkopen = (TextView) this.findViewById(R.id.fbcprodapprlink);

		miamelink = "<a href=''>" + "Miame-Dade Product Approval" + "</a>";
		miameweblinkopen.setText(Html.fromHtml(miamelink));

		fbclink = "<a href=''>" + "FBC Product Approval" + "</a>";
		fbcweblinkopen.setText(Html.fromHtml(fbclink));
		miameweblinkopen.setOnClickListener((OnClickListener) new clicker1());
		fbcweblinkopen.setOnClickListener((OnClickListener) new clicker1());

		Vimage = (TextView) findViewById(R.id.Vimage);
		tblcommentschange = (TableLayout) findViewById(R.id.tbllayoutrccomments);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		// for the permit date auto selection starts here
		TextWatcher watcher = new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			public void afterTextChanged(Editable e) {
				// TODO Auto-generated method stub
				int id = 0;
				try {

					View v = getCurrentFocus();
					id = v.getId();
					int select = 0;
					;
					switch (id) {
					case R.id.txtpermitasphalt:
						select = 1;
						break;
					case R.id.txtpermitconcrete:
						select = 2;
						break;
					case R.id.txtpermitmetal:
						select = 3;
						break;
					case R.id.txtpermitbuiltup:
						select = 4;
						break;
					case R.id.txtpermitmembrane:
						select = 5;
						break;
					case R.id.txtpermitother:
						select = 6;
						break;
					case R.id.txtproductasphalt:
						select = 1;
						break;
					case R.id.txtproductconcrete:
						select = 2;
						break;
					case R.id.txtproductmetal:
						select = 3;
						break;
					case R.id.txtproductbuiltup:
						select = 4;
						break;
					case R.id.txtproductmembrane:
						select = 5;
						break;
					case R.id.txtproductother:
						select = 6;
						break;

					}
					auto_select(select, 1);
				} catch (Exception m) {

				}
			}

		};

		this.txtpermitasphalt = (EditText) this
				.findViewById(R.id.txtpermitasphalt);
		txtpermitasphalt.addTextChangedListener(watcher);

		this.txtpermitconcrete = (EditText) this
				.findViewById(R.id.txtpermitconcrete);
		txtpermitconcrete.addTextChangedListener(watcher);

		this.txtpermitmetal = (EditText) this.findViewById(R.id.txtpermitmetal);
		txtpermitmetal.addTextChangedListener(watcher);
		this.txtpermitbuiltup = (EditText) this
				.findViewById(R.id.txtpermitbuiltup);
		txtpermitbuiltup.addTextChangedListener(watcher);
		this.txtpermitmembrane = (EditText) this
				.findViewById(R.id.txtpermitmembrane);
		txtpermitmembrane.addTextChangedListener(watcher);
		this.txtpermitother = (EditText) this.findViewById(R.id.txtpermitother);
		txtpermitother.addTextChangedListener(watcher);
		txtpermitasphalt.setEnabled(false);
		txtpermitconcrete.setEnabled(false);
		txtpermitmetal.setEnabled(false);
		txtpermitbuiltup.setEnabled(false);
		txtpermitmembrane.setEnabled(false);
		txtpermitother.setEnabled(false);

		this.txtproductasphalt = (EditText) this
				.findViewById(R.id.txtproductasphalt);
		this.txtproductconcrete = (EditText) this
				.findViewById(R.id.txtproductconcrete);
		this.txtproductmetal = (EditText) this
				.findViewById(R.id.txtproductmetal);
		this.txtproductbuiltup = (EditText) this
				.findViewById(R.id.txtproductbuiltup);
		this.txtproductmembrane = (EditText) this
				.findViewById(R.id.txtproductmembrane);
		this.txtproductother = (EditText) this
				.findViewById(R.id.txtproductother);

		txtproductasphalt.addTextChangedListener(watcher);
		txtproductconcrete.addTextChangedListener(watcher);
		txtproductmetal.addTextChangedListener(watcher);
		txtproductbuiltup.addTextChangedListener(watcher);
		txtproductmembrane.addTextChangedListener(watcher);
		txtproductother.addTextChangedListener(watcher);
		txtproductasphalt.setEnabled(false);
		txtproductconcrete.setEnabled(false);
		txtproductmetal.setEnabled(false);
		txtproductbuiltup.setEnabled(false);
		txtproductmembrane.setEnabled(false);
		txtproductother.setEnabled(false);

		OnItemSelectedListener changelistener = new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				int id = 0;
				try {

					View v = getCurrentFocus();
					id = v.getId();
					int select = 0;
					switch (id) {
					case R.id.txtyearasphalt:
						select = 1;

						break;
					case R.id.txtyearconcrete:
						select = 2;

						break;
					case R.id.txtyearmetal:
						select = 3;

						break;
					case R.id.txtyearbuiltup:
						select = 4;

						break;
					case R.id.txtyearmembrane:
						select = 5;

						break;
					case R.id.txtyearother:
						select = 6;
						break;
					}
					auto_select(select, 1);
				} catch (Exception e) {
				}

			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		};

		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, yearb);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		this.txtyearasphalt = (Spinner) this.findViewById(R.id.txtyearasphalt);
		txtyearasphalt.setAdapter(adapter2);
		txtyearasphalt.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// your code here
				if (position != 0) {
					auto_select(1, 1);
				}

			}

			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
		this.txtyearconcrete = (Spinner) this
				.findViewById(R.id.txtyearconcrete);

		txtyearconcrete.setAdapter(adapter2);
		txtyearconcrete.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// your code here
				if (position != 0) {
					auto_select(2, 1);
				}
			}

			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
		this.txtyearmetal = (Spinner) this.findViewById(R.id.txtyearmetal);

		txtyearmetal.setAdapter(adapter2);
		txtyearmetal.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// your code here
				if (position != 0) {
					auto_select(3, 1);
				}
			}

			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
		this.txtyearbuiltup = (Spinner) this.findViewById(R.id.txtyearbuiltup);

		txtyearbuiltup.setAdapter(adapter2);
		txtyearbuiltup.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// your code here

				if (position != 0) {
					auto_select(4, 1);
				}

			}

			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
		this.txtyearmembrane = (Spinner) this
				.findViewById(R.id.txtyearmembrane);

		txtyearmembrane.setAdapter(adapter2);
		txtyearmembrane.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// your code here

				if (position != 0) {
					auto_select(5, 1);
				}
			}

			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}
		});
		this.txtyearother = (Spinner) this.findViewById(R.id.txtyearother);

		txtyearother.setAdapter(adapter2);
		txtyearother.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// your code here

				if (position != 0) {
					auto_select(6, 1);
				}
			}

			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
		// ends here

		txtyearasphalt.setEnabled(false);
		txtyearconcrete.setEnabled(false);
		txtyearmetal.setEnabled(false);
		txtyearbuiltup.setEnabled(false);
		txtyearmembrane.setEnabled(false);
		txtyearother.setEnabled(false);

		OnClickListener myOnClickListener = new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				int select;
				switch (v.getId()) {
				case R.id.chkasphalt:
					if (chkasphalt.isChecked()) {// ||
													// chkconcreteroofcovertype.isChecked()||
													// chkmetalroofcovertype.isChecked()
													// ||
													// chkbuiltuproofcovertype.isChecked()
													// ||
													// chkmembraneroofcovertype.isChecked()
													// ||
													// chkotherroofcovertype.isChecked()
						if (chkasphaltroofcovertype.isChecked()) {
							// auto_select(select);
							// select = 1;
							/*txtyearasphalt.setEnabled(false);
							txtproductasphalt.setEnabled(false);
							txtpermitasphalt.setEnabled(false);*/
							boolean[] bl = set_auto_select.get("1").clone();
							set_auto_select.remove("1");
							bl[3] = true;
							set_auto_select.put("1", bl);
							select_radio();

						}
					} else {
						/*txtyearasphalt.setEnabled(true);
						txtproductasphalt.setEnabled(true);
						txtpermitasphalt.setEnabled(true);*/
						boolean[] bl = set_auto_select.get("1").clone();
						set_auto_select.remove("1");
						bl[3] = false;
						set_auto_select.put("1", bl);
						select_radio();

					}
					break;
				case R.id.chkconcrete:

					if (chkconcrete.isChecked()
							&& (chkconcreteroofcovertype.isChecked()))

					{
						/*txtyearconcrete.setEnabled(false);
						txtproductconcrete.setEnabled(false);
						txtpermitconcrete.setEnabled(false);*/
						boolean[] bl = set_auto_select.get("2").clone();
						set_auto_select.remove("2");
						bl[3] = true;
						set_auto_select.put("2", bl);
						select_radio();

					} else {
						/*txtyearconcrete.setEnabled(true);
						txtproductconcrete.setEnabled(true);
						txtpermitconcrete.setEnabled(true);*/
						boolean[] bl = set_auto_select.get("2").clone();
						set_auto_select.remove("2");
						bl[3] = false;
						set_auto_select.put("2", bl);
						select_radio();

					}
					break;
				case R.id.chkmetal:
					if (chkmetal.isChecked()
							&& (chkmetalroofcovertype.isChecked())) {
						/*txtyearmetal.setEnabled(false);
						txtproductmetal.setEnabled(false);
						txtpermitmetal.setEnabled(false);*/
						
						boolean[] bl = set_auto_select.get("3").clone();
						set_auto_select.remove("3");
						bl[3] = true;
						set_auto_select.put("3", bl);
						select_radio();
					} else {
						/*txtyearmetal.setEnabled(true);
						txtproductmetal.setEnabled(true);
						txtpermitmetal.setEnabled(true);*/
						boolean[] bl = set_auto_select.get("3").clone();
						set_auto_select.remove("3");
						bl[3] = false;
						set_auto_select.put("3", bl);
						select_radio();

					}
					break;
				case R.id.chkbuiltup:
					if (chkbuiltup.isChecked()
							&& (chkbuiltuproofcovertype.isChecked())) {
						
					/*	txtyearbuiltup.setEnabled(false);
						txtproductbuiltup.setEnabled(false);
						txtpermitbuiltup.setEnabled(false);*/
						boolean[] bl = set_auto_select.get("4").clone();
						set_auto_select.remove("4");
						bl[3] = true;
						set_auto_select.put("4", bl);
						select_radio();

					} else {
					/*	txtyearbuiltup.setEnabled(true);
						txtproductbuiltup.setEnabled(true);
						txtpermitbuiltup.setEnabled(true);*/
						boolean[] bl = set_auto_select.get("4").clone();
						set_auto_select.remove("4");
						bl[3] = false;
						set_auto_select.put("4", bl);
						select_radio();

					}
					break;
				case R.id.chkmembrane:
					if (chkmembrane.isChecked()
							&& (chkmembraneroofcovertype.isChecked())) {
					/*	txtyearmembrane.setEnabled(false);
						txtproductmembrane.setEnabled(false);
						txtpermitmembrane.setEnabled(false);*/
						
						boolean[] bl = set_auto_select.get("5").clone();
						set_auto_select.remove("5");
						bl[3] = true;
						set_auto_select.put("5", bl);
						select_radio();

					} else {
						/*txtyearmembrane.setEnabled(true);
						txtproductmembrane.setEnabled(true);
						txtpermitmembrane.setEnabled(true);*/
						boolean[] bl = set_auto_select.get("5").clone();
						set_auto_select.remove("5");
						bl[3] = false;
						set_auto_select.put("5", bl);
						select_radio();

					}
					break;
				case R.id.chkother:
					if (chkother.isChecked()
							&& (chkotherroofcovertype.isChecked())) {
						
						/*txtyearother.setEnabled(false);
						txtproductother.setEnabled(false);
						txtpermitother.setEnabled(false);
						txtother.setEnabled(false);*/
						
						boolean[] bl = set_auto_select.get("6").clone();
						set_auto_select.remove("6");
						bl[3] = true;
						set_auto_select.put("6", bl);
						select_radio();
					} else {
						/*txtyearother.setEnabled(true);
						txtproductother.setEnabled(true);
						txtpermitother.setEnabled(true);
						txtother.setEnabled(true);*/
						boolean[] bl = set_auto_select.get("6").clone();
						set_auto_select.remove("6");
						bl[3] = false;
						set_auto_select.put("6", bl);
						txtother.setText("");
						select_radio();

					}
					break;

				}
			}

		};

		exceedslimit1 = (TableLayout) this.findViewById(R.id.exceedslimit);
		this.nocommentsdisp = (TextView) this.findViewById(R.id.notxtcomments);
		this.chkasphalt = (CheckBox) this.findViewById(R.id.chkasphalt);
		chkasphalt.setOnClickListener(myOnClickListener);
		this.chkconcrete = (CheckBox) this.findViewById(R.id.chkconcrete);
		chkconcrete.setOnClickListener(myOnClickListener);
		this.chkmetal = (CheckBox) this.findViewById(R.id.chkmetal);
		chkmetal.setOnClickListener(myOnClickListener);
		this.chkbuiltup = (CheckBox) this.findViewById(R.id.chkbuiltup);
		chkbuiltup.setOnClickListener(myOnClickListener);
		this.chkmembrane = (CheckBox) this.findViewById(R.id.chkmembrane);
		chkmembrane.setOnClickListener(myOnClickListener);
		this.chkother = (CheckBox) this.findViewById(R.id.chkother);
		chkother.setOnClickListener(myOnClickListener);

		chkasphalt.setEnabled(false);
		chkconcrete.setEnabled(false);
		chkmetal.setEnabled(false);
		chkbuiltup.setEnabled(false);
		chkmembrane.setEnabled(false);
		chkother.setEnabled(false);

		this.rdioFBC = (RadioButton) this.findViewById(R.id.rdioFBC);
		this.rdioFBC.setOnClickListener(OnClickListener);
		this.rdioSFBC = (RadioButton) this.findViewById(R.id.rdioSFBC);
		this.rdioSFBC.setOnClickListener(OnClickListener);
		this.rdioAorB = (RadioButton) this.findViewById(R.id.rdioAorB);
		this.rdioAorB.setOnClickListener(OnClickListener);
		this.rdioNone = (RadioButton) this.findViewById(R.id.rdioNone);
		this.rdioNone.setOnClickListener(OnClickListener);

		this.txtother = (EditText) this.findViewById(R.id.txtother);
		txtother.setEnabled(false);
		getdate1 = (Button) findViewById(R.id.dateasphalt);
		getdate2 = (Button) findViewById(R.id.dateconcrete);
		getdate3 = (Button) findViewById(R.id.datemetal);
		getdate4 = (Button) findViewById(R.id.datebuiltup);
		getdate5 = (Button) findViewById(R.id.datemembrane);
		getdate6 = (Button) findViewById(R.id.dateother);

		getdate1.setEnabled(false);
		getdate2.setEnabled(false);
		getdate3.setEnabled(false);
		getdate4.setEnabled(false);
		getdate5.setEnabled(false);
		getdate6.setEnabled(false);
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		getdate1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				chkboxvalidation[0] = "1";
				chkboxvalidation[1] = "";
				chkboxvalidation[2] = "";
				chkboxvalidation[3] = "";
				chkboxvalidation[4] = "";
				chkboxvalidation[5] = "";
				txtpermitasphalt.requestFocus();
				showDialog(DATE_DIALOG_ID);

			}
		});
		getdate2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				chkboxvalidation[0] = "";
				chkboxvalidation[1] = "1";
				chkboxvalidation[2] = "";
				chkboxvalidation[3] = "";
				chkboxvalidation[4] = "";
				chkboxvalidation[5] = "";
				txtpermitconcrete.requestFocus();
				showDialog(DATE_DIALOG_ID);

			}
		});
		getdate3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				chkboxvalidation[0] = "";
				chkboxvalidation[1] = "";
				chkboxvalidation[2] = "1";
				chkboxvalidation[3] = "";
				chkboxvalidation[4] = "";
				chkboxvalidation[5] = "";
				txtpermitmetal.requestFocus();
				showDialog(DATE_DIALOG_ID);

			}
		});
		getdate4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				chkboxvalidation[0] = "";
				chkboxvalidation[1] = "";
				chkboxvalidation[2] = "";
				chkboxvalidation[3] = "1";
				chkboxvalidation[4] = "";
				chkboxvalidation[5] = "";
				txtpermitbuiltup.requestFocus();
				showDialog(DATE_DIALOG_ID);

			}
		});
		getdate5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				chkboxvalidation[0] = "";
				chkboxvalidation[1] = "";
				chkboxvalidation[2] = "";
				chkboxvalidation[3] = "";
				chkboxvalidation[4] = "1";
				chkboxvalidation[5] = "";
				txtpermitmembrane.requestFocus();
				showDialog(DATE_DIALOG_ID);

			}
		});
		getdate6.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				chkboxvalidation[0] = "";
				chkboxvalidation[1] = "";
				chkboxvalidation[2] = "";
				chkboxvalidation[3] = "";
				chkboxvalidation[4] = "";
				chkboxvalidation[5] = "1";
				txtpermitother.requestFocus();
				showDialog(DATE_DIALOG_ID);

			}
		});
		try {
			Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.quesroofcover
					+ " WHERE SRID='" + cf.Homeid + "'", null);
			int chkrws = c2.getCount();
			if (chkrws == 0) {
				identity = "test";
			} else {
				// roofcovertick.setVisibility(visibility);
				identity = "prev";
			}
		} catch (Exception e) {
		
		}

		String select_radio[] = { "false", "false", "false", "false", "false" };
		this.chkasphaltroofcovertype = (CheckBox) this
				.findViewById(R.id.rdioasphalt);

		chkasphaltroofcovertype.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					RoofCoverValue[0] = 1;
					set_auto_select.put("1", b);
					selected_check_list += "chk0~";
					staus_autoselect[0] = false;
					staus_autoselect[1] = false;
					staus_autoselect[2] = false;
					roofasphalt = "1";
					txtpermitasphalt.setEnabled(true);
					txtproductasphalt.setEnabled(true);
					txtyearasphalt.setEnabled(true);
					chkasphalt.setEnabled(true);
					getdate1.setEnabled(true);
					setPreDominant(0, true);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;

				} else {

					RoofCoverValue[0] = 0;
					set_auto_select.remove("1");
					roofasphalt = "0";
					chkstatus[0] = "true";
					chkstatus1[0] = "false";
					txtpermitasphalt.setEnabled(false);
					txtproductasphalt.setEnabled(false);
					txtyearasphalt.setEnabled(false);
					chkasphalt.setEnabled(false);
					getdate1.setEnabled(false);
					txtproductasphalt.setText("");
					txtpermitasphalt.setText("");
					txtyearasphalt.setSelection(0);
					chkasphalt.setChecked(false);
					permitdateasphalt = "";
					prodapprovalasphalt = "";
					yearasphalt = "";
					select_radio();
					setPreDominant(0, false);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;

				}
			}

		});
		this.chkconcreteroofcovertype = (CheckBox) this
				.findViewById(R.id.rdioconcrete);
		chkconcreteroofcovertype.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					RoofCoverValue[1] = 1;
					set_auto_select.put("2", b);
					selected_check_list += "chk1~";
					staus_autoselect[0] = false;
					staus_autoselect[1] = false;
					staus_autoselect[2] = false;
					roofconcrete = "1";
					txtpermitconcrete.setEnabled(true);
					txtproductconcrete.setEnabled(true);
					txtyearconcrete.setEnabled(true);
					chkconcrete.setEnabled(true);
					getdate2.setEnabled(true);
					radio_most[1].setEnabled(true);
					setPreDominant(1, true);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;
				} else {
					RoofCoverValue[1] = 0;
					set_auto_select.remove("2");
					roofconcrete = "0";
					chkstatus[1] = "true";
					chkstatus1[1] = "false";
					txtpermitconcrete.setEnabled(false);
					txtproductconcrete.setEnabled(false);
					txtyearconcrete.setEnabled(false);
					chkconcrete.setEnabled(false);
					getdate2.setEnabled(false);
					txtproductconcrete.setText("");
					txtpermitconcrete.setText("");
					txtyearconcrete.setSelection(0);
					chkconcrete.setChecked(false);
					permitdateconcrete = "";
					prodapprovalconcrete = "";
					yearconcrete = "";
					select_radio();
					setPreDominant(1, false);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;

				}
			}
		});

		this.chkmetalroofcovertype = (CheckBox) this
				.findViewById(R.id.rdiometal);
		chkmetalroofcovertype.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					RoofCoverValue[2] = 1;
					set_auto_select.put("3", b);
					selected_check_list += "chk2~";
					staus_autoselect[0] = false;
					staus_autoselect[1] = false;
					staus_autoselect[2] = false;
					roofmetal = "1";
					txtpermitmetal.setEnabled(true);
					txtproductmetal.setEnabled(true);
					txtyearmetal.setEnabled(true);
					chkmetal.setEnabled(true);
					getdate3.setEnabled(true);
					setPreDominant(2, true);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;

				} else {
					RoofCoverValue[2] = 0;
					set_auto_select.remove("3");
					roofmetal = "0";
					chkstatus[2] = "true";
					chkstatus1[2] = "false";
					txtpermitmetal.setEnabled(false);
					txtproductmetal.setEnabled(false);
					txtyearmetal.setEnabled(false);
					chkmetal.setEnabled(false);
					getdate3.setEnabled(false);
					txtproductmetal.setText("");
					txtpermitmetal.setText("");
					txtyearmetal.setSelection(0);
					chkmetal.setChecked(false);
					permitdatemetal = "";
					prodapprovalmetal = "";
					yearmetal = "";
					select_radio();
					setPreDominant(2, false);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;

				}
			}
		});

		this.chkbuiltuproofcovertype = (CheckBox) this
				.findViewById(R.id.rdiobuiltup);
		chkbuiltuproofcovertype.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					RoofCoverValue[3] = 1;
					set_auto_select.put("4", b);
					selected_check_list += "chk3~";
					staus_autoselect[0] = false;
					staus_autoselect[1] = false;
					staus_autoselect[2] = false;
					roofbuiltup = "1";
					txtpermitbuiltup.setEnabled(true);
					txtproductbuiltup.setEnabled(true);
					txtyearbuiltup.setEnabled(true);
					chkbuiltup.setEnabled(true);
					getdate4.setEnabled(true);
					setPreDominant(3, true);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;

				} else {
					RoofCoverValue[3] = 0;
					set_auto_select.remove("4");
					roofbuiltup = "0";
					chkstatus[3] = "true";
					chkstatus1[3] = "false";
					txtpermitbuiltup.setEnabled(false);
					txtproductbuiltup.setEnabled(false);
					txtyearbuiltup.setEnabled(false);
					chkbuiltup.setEnabled(false);
					getdate4.setEnabled(false);
					txtproductbuiltup.setText("");
					txtpermitbuiltup.setText("");
					txtyearbuiltup.setSelection(0);
					chkbuiltup.setChecked(false);
					permitdatebuiltup = "";
					prodapprovalbuiltup = "";
					yearbuiltup = "";
					select_radio();
					setPreDominant(3, false);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;

				}
			}
		});
		this.chkmembraneroofcovertype = (CheckBox) this
				.findViewById(R.id.rdiomembrane);
		chkmembraneroofcovertype.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					RoofCoverValue[4] = 1;
					set_auto_select.put("5", b);
					selected_check_list += "chk4~";
					staus_autoselect[0] = false;
					staus_autoselect[1] = false;
					staus_autoselect[2] = false;
					roofmembrane = "1";
					txtpermitmembrane.setEnabled(true);
					txtproductmembrane.setEnabled(true);
					txtyearmembrane.setEnabled(true);
					chkmembrane.setEnabled(true);
					getdate5.setEnabled(true);
					setPreDominant(4, true);

				} else {
					RoofCoverValue[4] = 0;
					set_auto_select.remove("5");
					roofmembrane = "0";
					chkstatus[4] = "true";
					chkstatus1[4] = "false";
					txtpermitmembrane.setEnabled(false);
					txtproductmembrane.setEnabled(false);
					txtyearmembrane.setEnabled(false);
					chkmembrane.setEnabled(false);
					getdate5.setEnabled(false);
					txtproductmembrane.setText("");
					txtpermitmembrane.setText("");
					txtyearmembrane.setSelection(0);
					chkmembrane.setChecked(false);
					permitdatemembrane = "";
					prodapprovalmembrane = "";
					yearmembrane = "";
					select_radio();
					setPreDominant(4, false);

				}
			}
		});
		this.chkotherroofcovertype = (CheckBox) this
				.findViewById(R.id.rdioother);

		chkotherroofcovertype.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {

					RoofCoverValue[5] = 1;
					set_auto_select.put("6", b);
					selected_check_list += "chk5~";
					staus_autoselect[0] = false;
					staus_autoselect[1] = false;
					staus_autoselect[2] = false;
					roofother = "1";
					// start the enable the check box and all
					txtpermitother.setEnabled(true);
					txtproductother.setEnabled(true);
					txtyearother.setEnabled(true);
					chkother.setEnabled(true);
					getdate6.setEnabled(true);
					txtother.setEnabled(true);
					setPreDominant(5, true);
					// ends the enable the check box and all

				} else {
					RoofCoverValue[5] = 0;
					set_auto_select.remove("6");
					roofother = "0";
					chkstatus[5] = "true";
					chkstatus1[5] = "false";
					// start the enable the check box and all
					txtpermitother.setEnabled(false);
					txtproductother.setEnabled(false);
					txtyearother.setEnabled(false);
					chkother.setEnabled(false);
					getdate6.setEnabled(false);
					txtother.setEnabled(false);
					txtproductother.setText("");
					txtpermitother.setText("");
					txtyearother.setSelection(0);
					txtother.setText("");
					chkother.setChecked(false);
					permitdateother = "";
					prodapprovalother = "";
					yearother = "";
					// ends the enable the check box and all
					select_radio();
					setPreDominant(5, false);

				}
			}
		});
		
		this.comments = (EditText) this.findViewById(R.id.txtcomments);
		comments.setOnTouchListener(new Touch_Listener());
		comments.addTextChangedListener(new QUES_textwatcher());
		
		roofcover_parrant = (LinearLayout)this.findViewById(R.id.roofcover_parrent);
		roofcover_type=(LinearLayout) findViewById(R.id.roofcover_type);
		roofcover_TV_type1 = (TextView) findViewById(R.id.roofcover_txt);
		
		
		saveclose = (Button) findViewById(R.id.savenext);
		prev = (Button) findViewById(R.id.previous);
		this.Vimage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				 tbllayout8.setVisibility(visibility);
				 arrowcommentschange();
			}
		});

		this.tblcommentschange.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}
		});

		if (identity.equals("prev")) {
			try {
				Cursor cur = cf.db.rawQuery("select * from " + cf.quesroofcover
						+ " where SRID='" + cf.Homeid + "'", null);
				int rws = cur.getCount();
				if (rws == 0) {

				} else {
					cur.moveToFirst();

					if (cur != null) {

						predominant = cur.getInt(cur
								.getColumnIndex("RoofPreDominant"));
						if (predominant != 0) {
							for (int i = 0; i <= 5; i++) {
								radio_most[i].setEnabled(false);
								if (predominant - 1 == i) {
									radio_most[i].setChecked(true);
								} else {
									radio_most[i].setChecked(false);
								}

							}
						}
						if (!"".equals(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("PermitApplnDate1"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("ProdApproval1"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("InstallYear1"))))
								|| !"0".equals(cf.getsinglequotes(cur
										.getString(cur
												.getColumnIndex("NoInfo1"))))) {

							txtpermitasphalt.setEnabled(true);
							txtproductasphalt.setEnabled(true);
							txtyearasphalt.setEnabled(true);
							chkasphalt.setEnabled(true);
							getdate1.setEnabled(true);
							radio_most[0].setEnabled(true);
							RoofCoverValue[0] = 1;

							chkasphaltroofcovertype.setChecked(true);

							if (!"".equals(cf.getsinglequotes(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("PermitApplnDate1")))))) {
								txtpermitasphalt
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("PermitApplnDate1"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("ProdApproval1"))))) {
								txtproductasphalt
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("ProdApproval1"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InstallYear1"))))) {
								txtyearasphalt
										.setSelection(getkeyarray(
												yearb,
												cf.getsinglequotes(cur.getString(cur
														.getColumnIndex("InstallYear1")))));
								// yearb.
							}
							if (!"0".equals(cf.getsinglequotes(cur
									.getString(cur.getColumnIndex("NoInfo1"))))) {
								chkasphalt.setChecked(true);
							}
							set_auto_select.put("1", this.b);

							auto_select(1, 0);
						}

						if (!"".equals(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("PermitApplnDate2"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("ProdApproval2"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("InstallYear2"))))
								|| !"0".equals(cf.getsinglequotes(cur
										.getString(cur
												.getColumnIndex("NoInfo2"))))) {
							// start the enable the check box and all
							txtpermitconcrete.setEnabled(true);
							txtproductconcrete.setEnabled(true);
							txtyearconcrete.setEnabled(true);
							chkconcrete.setEnabled(true);
							getdate2.setEnabled(true);
							radio_most[1].setEnabled(true);
							RoofCoverValue[1] = 1;
							// ends the enable the check box and all
							chkconcreteroofcovertype.setChecked(true);
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("PermitApplnDate2"))))) {
								txtpermitconcrete
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("PermitApplnDate2"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("ProdApproval2"))))) {
								txtproductconcrete
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("ProdApproval2"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InstallYear2"))))) {
								txtyearconcrete
										.setSelection(getkeyarray(
												yearb,
												cf.getsinglequotes(cur.getString(cur
														.getColumnIndex("InstallYear2")))));
							}
							if (!"0".equals(cf.getsinglequotes(cur
									.getString(cur.getColumnIndex("NoInfo2"))))) {
								chkconcrete.setChecked(true);
							}
							set_auto_select.put("2", this.b);
							auto_select(2, 0);
						}

						if (!"".equals(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("PermitApplnDate3"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("ProdApproval3"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("InstallYear3"))))
								|| !"0".equals(cf.getsinglequotes(cur
										.getString(cur
												.getColumnIndex("NoInfo3"))))) {
							chkmetalroofcovertype.setChecked(true);
							// start the enable the check box and all
							txtpermitmetal.setEnabled(true);
							txtproductmetal.setEnabled(true);
							txtyearmetal.setEnabled(true);
							chkmetal.setEnabled(true);
							getdate3.setEnabled(true);
							radio_most[2].setEnabled(true);
							RoofCoverValue[2] = 1;
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("PermitApplnDate3"))))) {
								txtpermitmetal
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("PermitApplnDate3"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("ProdApproval3"))))) {
								txtproductmetal
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("ProdApproval3"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InstallYear3"))))) {
								txtyearmetal
										.setSelection(getkeyarray(
												yearb,
												cf.getsinglequotes(cur.getString(cur
														.getColumnIndex("InstallYear3")))));
							}
							if (!"0".equals(cf.getsinglequotes(cur
									.getString(cur.getColumnIndex("NoInfo3"))))) {
								chkmetal.setChecked(true);
							}
							set_auto_select.put("3", this.b);
							auto_select(3, 0);
						}

						if (!"".equals(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("PermitApplnDate4"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("ProdApproval4"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("InstallYear4"))))
								|| !"0".equals(cf.getsinglequotes(cur
										.getString(cur
												.getColumnIndex("NoInfo4"))))) {
							RoofCoverValue[3] = 1;
							chkbuiltuproofcovertype.setChecked(true);
							// start the enable the check box and all
							txtpermitbuiltup.setEnabled(true);
							txtproductbuiltup.setEnabled(true);
							txtyearbuiltup.setEnabled(true);
							chkbuiltup.setEnabled(true);
							getdate4.setEnabled(true);
							radio_most[3].setEnabled(true);
							// auto_select(4,0);

							// ends the enable the check box and all
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("PermitApplnDate4"))))) {
								txtpermitbuiltup
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("PermitApplnDate4"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("ProdApproval4"))))) {
								txtproductbuiltup
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("ProdApproval4"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InstallYear4"))))) {
								txtyearbuiltup
										.setSelection(getkeyarray(
												yearb,
												cf.getsinglequotes(cur.getString(cur
														.getColumnIndex("InstallYear4")))));
							}
							if (!"0".equals(cf.getsinglequotes(cur
									.getString(cur.getColumnIndex("NoInfo4"))))) {
								chkbuiltup.setChecked(true);
							}
							set_auto_select.put("4", this.b);
							auto_select(4, 0);
						}

						if (!"".equals(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("PermitApplnDate5"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("ProdApproval5"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("InstallYear5"))))
								|| !"0".equals(cf.getsinglequotes(cur
										.getString(cur
												.getColumnIndex("NoInfo5"))))) {
							RoofCoverValue[4] = 1;
							chkmembraneroofcovertype.setChecked(true);
							// start the enable the check box and all
							txtpermitmembrane.setEnabled(true);
							txtproductmembrane.setEnabled(true);
							txtyearmembrane.setEnabled(true);
							chkmembrane.setEnabled(true);
							getdate5.setEnabled(true);
							radio_most[4].setEnabled(true);
							// ends the enable the check box and all
							// PermitApplnDate5
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("PermitApplnDate5"))))) {
								txtpermitmembrane
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("PermitApplnDate5"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("ProdApproval5"))))) {
								txtproductmembrane
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("ProdApproval5"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InstallYear5"))))) {
								txtyearmembrane
										.setSelection(getkeyarray(
												yearb,
												cf.getsinglequotes(cur.getString(cur
														.getColumnIndex("InstallYear5")))));
							}
							if (!"0".equals(cf.getsinglequotes(cur
									.getString(cur.getColumnIndex("NoInfo5"))))) {
								chkmembrane.setChecked(true);
							}
							set_auto_select.put("5", this.b);
							auto_select(5, 0);
						}

						if (!"".equals(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("PermitApplnDate6"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("ProdApproval6"))))
								|| !"".equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("InstallYear6"))))
								|| !"0".equals(cf.getsinglequotes(cur
										.getString(cur
												.getColumnIndex("NoInfo6"))))) {
							RoofCoverValue[5] = 1;
							chkotherroofcovertype.setChecked(true);
							// start the enable the check box and all
							txtpermitother.setEnabled(true);
							txtproductother.setEnabled(true);
							txtyearother.setEnabled(true);
							chkother.setEnabled(true);
							getdate6.setEnabled(true);
							txtother.setEnabled(true);
							radio_most[5].setEnabled(true);
							// ends the enable the check box and all
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("PermitApplnDate6"))))) {
								txtpermitother
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("PermitApplnDate6"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("ProdApproval6"))))) {
								txtproductother
										.setText(cf.getsinglequotes(cur.getString(cur
												.getColumnIndex("ProdApproval6"))));
							}
							if (!"".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("InstallYear6"))))) {
								txtyearother
										.setSelection(getkeyarray(
												yearb,
												cf.getsinglequotes(cur.getString(cur
														.getColumnIndex("InstallYear6")))));
							}
							if (!"0".equals(cf.getsinglequotes(cur
									.getString(cur.getColumnIndex("NoInfo6"))))) {
								chkother.setChecked(true);
							}
							if (!"0".equals(cf.getsinglequotes(cur.getString(cur
									.getColumnIndex("RoofCoverTypeOther"))))) {
								txtother.setText(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("RoofCoverTypeOther"))));
							}
							set_auto_select.put("6", this.b);
							auto_select(6, 0);
						}

						if ("1".equals(cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("RoofCoverValue"))))) {
							roofcovervalue = "1";
							rdioFBC.setChecked(true);
						} else if ("2"
								.equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("RoofCoverValue"))))) {
							roofcovervalue = "2";
							rdioSFBC.setChecked(true);
						} else if ("3"
								.equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("RoofCoverValue"))))) {
							roofcovervalue = "3";
							rdioAorB.setChecked(true);
						} else if ("4"
								.equals(cf.getsinglequotes(cur.getString(cur
										.getColumnIndex("RoofCoverValue"))))) {
							roofcovervalue = "4";
							rdioNone.setChecked(true);
						} else {
							roofcovervalue = "0";
						}

					}

				}
			} catch (Exception e) {
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" problem in fetching roof cover data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
			try {

				Cursor cur = cf.db.rawQuery("select * from " + cf.tbl_comments
						+ " where SRID='" + cf.Homeid + "'", null);
				int rws = cur.getCount();

				cur.moveToFirst();
				if (cur != null) {
					roofcovercomment = cf.getsinglequotes(cur.getString(cur
							.getColumnIndex("RoofCoverComment")));
					cf.showing_limit(roofcovercomment,roofcover_parrant,roofcover_type,roofcover_TV_type1,"474");	
					
					comments.setText(roofcovercomment);
				}
			} catch (Exception e) {
				//cf.Error_LogFile_Creation(RoofCover.this +" problem in fetching roof cover comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
		}
	}
	class Touch_Listener implements OnTouchListener
	{
		  Touch_Listener()
			{
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    		cf.setFocus(comments);
		
				return false;
		    }
	}
	class QUES_textwatcher implements TextWatcher
	{
	     QUES_textwatcher()
		{
		
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
				
				cf.showing_limit(s.toString(),roofcover_parrant,roofcover_type,roofcover_TV_type1,"474"); 
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	protected void arrowcommentschange() {
		// TODO Auto-generated method stub
		System.out.println("viewimag="+viewimage);Vimage.setBackgroundResource(R.drawable.arrowup);
		if (viewimage == 1) {
			Vimage.setBackgroundResource(R.drawable.arrowup);
			lincomments.setVisibility(v1.VISIBLE);
			if (rdioFBC.isChecked()) {
				optionid = 1;
			} else if (rdioSFBC.isChecked()) {
				optionid = 2;
			} else if (rdioAorB.isChecked()) {
				optionid = 3;
			} else if (rdioNone.isChecked()) {
				optionid = 4;
			} else {
				cf.ShowToast("Please select Roof Covering options to view Comments.",1);
			}
			try {
				descrip = "";
				arrcomment1 = null;
				Cursor c2 = cf.db.rawQuery("SELECT * FROM "
						+ cf.tbl_admcomments
						+ " WHERE questionid='2' and optionid='" + optionid
						+ "' and status='Active' and InspectorId='"
						+ cf.InspectionType + "'", null);
				int rws = c2.getCount();
				arrcomment1 = new String[rws];
				c2.moveToFirst();
				System.out.println("rws= "+rws);
				if (rws == 0) {
					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.VISIBLE);
					lincomments.setVisibility(v1.GONE);
					Vimage.setBackgroundResource(R.drawable.arrowdown);
				} else {

					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.GONE);

					lincomments.setVisibility(v1.VISIBLE);
					if (c2 != null) {
						int i=0;
						do {
							arrcomment1[i]=cf.getsinglequotes(c2.getString(c2.getColumnIndex("description")));
							if(arrcomment1[i].contains("null"))
							{
								arrcomment1[i] = arrcomment1[i].replace("null", "");
							}							
							i++;
						} while (c2.moveToNext());						
					}
					c2.close();

					addcomments();
					viewimage = 0;
				}

			} catch (Exception e) {
				/*
				 * ShowToast toast = new ShowToast(getBaseContext(),
				 * "Please add atleast one roof cover comment in dashboard");
				 */
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" problem in retrieving comments data in arrow click on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}

		} else if (viewimage == 0) {
			Vimage.setBackgroundResource(R.drawable.arrowdown);
			tbllayout8.setVisibility(v1.GONE);
			viewimage = 1;
		}
	}

	private void addcomments() {
		// TODO Auto-generated method stub
		lincomments.removeAllViews();
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		lincomments.addView(l1);

		cb = new CheckBox[arrcomment1.length];

		for (int i = 0; i < arrcomment1.length; i++) {

			LinearLayout l2 = new LinearLayout(this);			
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.setMargins(0, 0, 20, 0);
			l1.addView(l2);			
			
			cb[i] = new CheckBox(this);
			cb[i].setText(arrcomment1[i].trim());
			cb[i].setTextColor(Color.GRAY);
			cb[i].setSingleLine(false);
			
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
			cb[i].setMaxWidth(padding);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,12);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);

			map1.put(conchkbox, cb[i]);
			l2.addView(cb[i],paramschk);

			cb[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					System.out.println("leneeene "+comments.getText().length());
					if(comments.getText().length()>=474)
					{
						cf.ShowToast("Comment text exceeds the limit.", 1);
						map1.get(getidofselbtn).setChecked(false);
					}
					else
					{
					temp_st = map1.get(getidofselbtn);
					setcomments(temp_st);
					}
				}
			});
		}

	}

	private void setcomments(final CheckBox tempSt) {
		// TODO Auto-generated method stub
		comm2 = "";
		String comm1 = tempSt.getText().toString();
		if (tempSt.isChecked() == true) {
			comm2 += comm1 + " ";
			int commenttextlength = comments.getText().length();
			comments.setText(comments.getText().toString() + " " + comm2);

			if (commenttextlength >= 474) {
				alertDialog = new AlertDialog.Builder(RoofCover.this).create();
				alertDialog.setTitle("Exceeds Limit");
				alertDialog.setMessage("Comment text exceeds the limit");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								tempSt.setChecked(false);
								tempSt.setEnabled(true);
							}
						});
				alertDialog.show();
			} else {
				tempSt.setEnabled(false);
			}
		} else {
		}
	}

	protected void auto_select(int select, int radioslect) {
		// TODO Auto-generated method stub
		try {
			String year = "", product = "", permit = "", stat;
			CheckBox ch = null;

			switch (select) {
			case 1:
				year = txtyearasphalt.getSelectedItem().toString();
				ch = chkasphalt;
				product = txtproductasphalt.getText().toString();
				permit = txtpermitasphalt.getText().toString();
				stat = "true";
				break;
			case 2:
				year = txtyearconcrete.getSelectedItem().toString();
				ch = chkconcrete;
				product = txtproductconcrete.getText().toString();
				permit = txtpermitconcrete.getText().toString();
				stat = "true";
				break;
			case 3:
				year = txtyearmetal.getSelectedItem().toString();
				ch = chkmetal;
				product = txtproductmetal.getText().toString();
				permit = txtpermitmetal.getText().toString();
				stat = "true";
				break;
			case 4:
				year = txtyearbuiltup.getSelectedItem().toString();
				ch = chkbuiltup;
				product = txtproductbuiltup.getText().toString();
				permit = txtpermitbuiltup.getText().toString();
				stat = "true";
				break;
			case 5:
				year = txtyearmembrane.getSelectedItem().toString();
				ch = chkmembrane;
				product = txtproductmembrane.getText().toString();
				permit = txtpermitmembrane.getText().toString();
				stat = "true";
				break;
			case 6:
				year = txtyearother.getSelectedItem().toString();
				ch = chkother;
				product = txtproductother.getText().toString();
				permit = txtpermitother.getText().toString();
				stat = "true";
				break;
			}

			int i1 = 0, i2 = 0, j = 0, j1 = 0, j2 = 0, install = 0;
			boolean[] z = { false, false, false, false };
			String re;
			if (!permit.equals("")) {
				i1 = permit.indexOf("/");
				String result = permit.substring(0, i1);
				i2 = permit.lastIndexOf("/");
				String result1 = permit.substring(i1 + 1, i2);
				String result2 = permit.substring(i2 + 1);
				result2 = result2.trim();
				j = Integer.parseInt(result2);
				j1 = Integer.parseInt(result1);
				j2 = Integer.parseInt(result);
			} else {
				j = 0;
				j1 = 0;
				j2 = 0;
			}
			if (!year.equals("") && !year.equals("Select")) {
				install = Integer.parseInt(year);
			} else {
				install = 0;
			}
			if (((j >= 2002) && (j2 >= 3 || j > 2002) && (j1 > 1 || j > 2002 || j2 > 3))
					|| (install >= 2004)) {

				boolean tmp[];

				z = set_auto_select.get(String.valueOf(select)).clone();
				set_auto_select.remove(String.valueOf(select));

				z[0] = true;
				z[1] = false;
				z[2] = false;

				set_auto_select.put(String.valueOf(select), z);

			} else if ((j <= 2002 && j >= 1994)
					&& ((j2 <= 3 || j < 2002) && (j2 >= 9 || j > 1994))
					&& ((j1 < 2 || j < 2002 || j2 < 3) && (j1 > 1 || j > 1994 || j2 > 9))
					&& ((count_value.toLowerCase().trim().equals("miami-dade")) || (count_value
							.toLowerCase().trim().equals("broward")))
					&& !(j == 0 && j1 == 0 && j2 == 0)) {
				z = set_auto_select.get(String.valueOf(select)).clone();
				z[1] = true;
				z[2] = false;
				z[0] = false;
				set_auto_select.put(String.valueOf(select), z);
			} else if (product.equals("")
					&& ((j <= 1994) && (j2 <= 9 || j < 1994) && (j1 <= 1
							|| j < 1994 || j2 < 9))) {
				z = set_auto_select.get(String.valueOf(select)).clone();
				z[2] = true;
				z[1] = false;
				z[0] = false;
				set_auto_select.put(String.valueOf(select), z);

			} else {
				z = set_auto_select.get(String.valueOf(select)).clone();
				z[2] = false;
				z[1] = false;
				z[0] = false;
				set_auto_select.put(String.valueOf(select), z);
			}

			if (radioslect == 1) {
				select_radio();
			} else {
				if (ch.isChecked()) {
					z = set_auto_select.get(String.valueOf(select)).clone();
					z[3] = true;
					set_auto_select.put(String.valueOf(select), z);
				} else {
					z = set_auto_select.get(String.valueOf(select)).clone();
					z[3] = false;
					set_auto_select.put(String.valueOf(select), z);

				}

			}

		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" problem in autoslecting roof cover option on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
	}

	private void select_radio() {
		// TODO Auto-generated method stub
		// if()

		boolean tmp[], sta = true, stb = true, stc = true, std = true;
		for (Iterator it = set_auto_select.keySet().iterator(); it.hasNext();) {
			String key = it.next().toString();
			tmp = set_auto_select.get(key).clone();
			if ((tmp[3] == false)) {
				std = false;

			} else {
				tmp[0] = false;
				tmp[1] = false;
				tmp[2] = false;
			}
			if ((tmp[0] == false)) {
				sta = false;
			}
			if ((tmp[1] == false)) {
				stb = false;
			}
			if ((tmp[2] == false) || tmp[1] == true || tmp[0] == true
					|| tmp[3] == true) {
				stc = false;
			}

		}
		if (sta && !stb && !std && !stc) {
			roofcovervalue = "1";
			rdioFBC.setChecked(true);
			rdioAorB.setChecked(false);
			rdioSFBC.setChecked(false);
			rdioNone.setChecked(false);
			commentsfill = "This home was verified as meeting the requirements of selection A of the OIR B1 -1802 Question 2, Roof Covering.";
			comments.setText(commentsfill);

		} else if (stb && !sta && !std && !stc) {
			roofcovervalue = "2";
			rdioFBC.setChecked(false);
			rdioAorB.setChecked(false);
			rdioSFBC.setChecked(true);
			rdioNone.setChecked(false);
			commentsfill = "This home was verified as meeting the requirements of selection B of the OIR B1 -1802 Question 2 Roof Covering.";
			comments.setText(commentsfill);
		} else if (stc && !stb && !sta && !std) {
			roofcovervalue = "3";
			rdioFBC.setChecked(false);
			rdioAorB.setChecked(true);
			rdioSFBC.setChecked(false);
			rdioNone.setChecked(false);
			commentsfill = "This home was verified as meeting the requirements of selection C of the OIR B1 -1802 Question 2 Roof Covering.";
			comments.setText(commentsfill);

		} else if (std && !stb && !sta && !stc) {
			roofcovervalue = "4";
			rdioFBC.setChecked(false);
			rdioAorB.setChecked(false);
			rdioSFBC.setChecked(false);
			rdioNone.setChecked(true);
			commentsfill = "This home was verified as meeting the requirements of selection D of the OIR B1 -1802 Question 2, Roof Covering.";
			comments.setText(commentsfill);

		} else {
			rdioFBC.setChecked(false);
			rdioAorB.setChecked(false);
			rdioSFBC.setChecked(false);
			rdioNone.setChecked(false);
			comments.setText("");

		}
	}

	private void setPreDominant(int i, boolean b1) {
		if (b1) {
			for (int k = 0; k <= i; k++) {
				if (i == k) {
					radio_most[i].setEnabled(true);

				}
			}
		} else {
			for (int k = 0; k <= i; k++) {
				if (i == k) {
					radio_most[i].setEnabled(false);
					radio_most[i].setChecked(false);
					if (predominant == i + 1) {
						predominant = 0;
					}
				}
			}
		}
	}

	class clicker1 implements OnClickListener {
		public void onClick(View v) {
			if (v == miameweblinkopen) {
				if (inc == 1) {
					iInspectionList = new Intent(RoofCover.this,
							WeblinkOpen.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList
							.putExtra("weburl",
									"http://www.miamidade.gov/building/pc-search_app.asp");
					iInspectionList.putExtra("iden", "test");
					iInspectionList.putExtra("InspectionType",
							cf.InspectionType);
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.putExtra("keyName", cf.value);
					iInspectionList.putExtra("Count", cf.Count);
					iInspectionList.putExtra("page", "roof");
					startActivity(iInspectionList);
				} else {
					alert();
				}
			} else if (v == fbcweblinkopen) {
				if (inc == 1) {

					iInspectionList = new Intent(RoofCover.this,
							WeblinkOpen.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList
							.putExtra("weburl",
									"http://www.floridabuilding.org/pr/pr_app_srch.aspx");
					iInspectionList.putExtra("iden", "test");
					iInspectionList.putExtra("InspectionType",
							cf.InspectionType);
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.putExtra("keyName", cf.value);
					iInspectionList.putExtra("Count", cf.Count);
					iInspectionList.putExtra("page", "roof");
					startActivity(iInspectionList);

				} else {
					alert();
				}
			}
		}
	}

	public void alert() {
		Builder builder = new AlertDialog.Builder(RoofCover.this);
		builder.setTitle("No Internet Connection");
		builder.setMessage("Please Turn On Wifi");
		builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		builder.show();
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.rdioFBC:
				viewimage=1;Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);
				
				commentsfill = "This home was verified as meeting the requirements of selection A of the OIR B1 -1802 Question 2, Roof Covering.";
				comments.setText(commentsfill);

				roofcovervalue = "1";
				rdioFBC.setChecked(true);
				rdioSFBC.setChecked(false);
				rdioAorB.setChecked(false);
				rdioNone.setChecked(false);
				break;

			case R.id.rdioSFBC:
				viewimage=1;Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);
				commentsfill = "This home was verified as meeting the requirements of selection B of the OIR B1 -1802 Question 2 Roof Covering.";
				comments.setText(commentsfill);
				roofcovervalue = "2";
				rdioFBC.setChecked(false);
				rdioSFBC.setChecked(true);
				rdioAorB.setChecked(false);
				rdioNone.setChecked(false);

				break;
			case R.id.rdioAorB:
				viewimage=1;Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);
				commentsfill = "This home was verified as meeting the requirements of selection C of the OIR B1 -1802 Question 2 Roof Covering.";
				comments.setText(commentsfill);
				roofcovervalue = "3";
				rdioFBC.setChecked(false);
				rdioSFBC.setChecked(false);
				rdioAorB.setChecked(true);
				rdioNone.setChecked(false);
				break;

			case R.id.rdioNone:
				viewimage=1;Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);
				commentsfill = "This home was verified as meeting the requirements of selection D of the OIR B1 -1802 Question 2, Roof Covering.";
				comments.setText(commentsfill);

				roofcovervalue = "4";
				rdioFBC.setChecked(false);
				rdioSFBC.setChecked(false);
				rdioAorB.setChecked(false);
				rdioNone.setChecked(true);
				break;

			}

		}
	};

	public void clicker(View v) {
		switch (v.getId()) {
	
		 case R.id.txthelpcontentoptionA: 
			 	cf.alerttitle="A - Meets FBC(PD -03/2/02) or Original (after 2004)";
				cf.alertcontent="All roof coverings listed above meet the FBC with an FBC or Miami-Dade product approval listing current at time of installation OR have a roofing permit application dated on or after 3/1/02 Or the roof is original and built in 2004 or later.";
				cf.showhelp(cf.alerttitle,cf.alertcontent);
				break;
		 case R.id.txthelpcontentoptionB: 
			 	cf.alerttitle="B - Meets SFBC (PD 09/01/1994) or Post 1997";
				cf.alertcontent="All roof coverings have a Miami-Dade product approval listing current at time of installation OR (for the HVHZ only) a roofing permit application dated after 9/1/1994 and before 3/1/2002 OR the roof is original and built in 1997 or later.";
				cf.showhelp(cf.alerttitle,cf.alertcontent);
				break;
		 case R.id.txthelpcontentoptionC: 
			 	cf.alerttitle="C - One or more does not meet A or B";
				cf.alertcontent="One or more roof coverings do not meet the requirements of answer A or B.";
				cf.showhelp(cf.alerttitle,cf.alertcontent);
				break;
		 case R.id.txthelpcontentoptionD: 
			 	cf.alerttitle="D - None Meet A or B";				
				cf.alertcontent="No roof coverings meet the requirements of answer A or B."; 
				cf.showhelp(cf.alerttitle,cf.alertcontent);
				break;
		case R.id.miamedadeprodapprlink:
			if (inc == 1) {
				iInspectionList = new Intent(RoofCover.this, WeblinkOpen.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("weburl",
						"http://www.miamidade.gov/building/pc-search_app.asp");
				iInspectionList.putExtra("iden", "test");
				iInspectionList.putExtra("InspectionType", cf.InspectionType);
				iInspectionList.putExtra("status", cf.status);
				iInspectionList.putExtra("keyName", cf.value);
				iInspectionList.putExtra("Count", cf.Count);
				startActivity(iInspectionList);
			} else {
				alert();
			}

			break;

		case R.id.previous:
			iInspectionList = new Intent(RoofCover.this, BuildCode.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("iden", "prev");
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);
			startActivity(iInspectionList);
			break;

		case R.id.savenext:
			boolean chkglobal = false;
			comm = comments.getText().toString();
			if ((roofasphalt == "1" || chkasphaltroofcovertype.isChecked() == true)
					&& (chkglobal == false)) {
				permitdateasphalt = txtpermitasphalt.getText().toString();
				prodapprovalasphalt = txtproductasphalt.getText().toString();
				yearasphalt = txtyearasphalt.getSelectedItem().toString();

				if (("".equals(permitdateasphalt))
						&& ("".equals(prodapprovalasphalt))
						&& ("Select".equals(yearasphalt))
						&& (chkasphalt.isChecked() == false)) {
					cf.ShowToast("Please enter any one of the Required value for Roof Covering.",1);
					chkglobal = true;
					chkstatus[0] = "false";
				} else {
					chkstatus[0] = "true";

					if (!"".equals(yearasphalt)
							&& !"Select".equals(yearasphalt)
							&& !"".equals(permitdateasphalt)) {
						int year = Integer.parseInt(yearasphalt);
						if (checkpermitdategraterthanbuild(yearasphalt,
								permitdateasphalt) == true) {
							chkdatestatus[0] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[0] = "false";
							txtpermitasphalt.setEnabled(true);
							txtpermitasphalt.setText("");
							txtpermitasphalt.requestFocus();
						}

					} else {
						chkdatestatus[0] = "true";
					}

					if (!"".equals(yearasphalt)
							&& !"Select".equals(yearasphalt)) {
						int year = Integer.parseInt(yearasphalt);
						if (year >= 1990 && year <= cf.Current_year) {
							yearvalasp = yearasphalt;
							chkstatus[0] = "true";
						} else {
							cf.ShowToast("Please enter the Date in valid format.",1);
							chkglobal = true;
							txtyearasphalt.requestFocus();
							chkglobal = true;
							chkstatus[0] = "false";
						}
					} else {
						chkstatus[0] = "true";
					}

				}
			}
			if ((roofconcrete == "1" || chkconcreteroofcovertype.isChecked() == true)
					&& (chkglobal == false)) {
				permitdateconcrete = txtpermitconcrete.getText().toString();
				prodapprovalconcrete = txtproductconcrete.getText().toString();
				yearconcrete = txtyearconcrete.getSelectedItem().toString();

				if (("".equals(permitdateconcrete))
						&& ("".equals(prodapprovalconcrete))
						&& ("Select".equals(yearconcrete))
						&& (chkconcrete.isChecked() == false)) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please select any one of the Required value for Roof Covering.");
					chkglobal = true;
					chkstatus[1] = "false";
				} else {

					if (!"".equals(yearconcrete)
							&& !"Select".equals(yearconcrete)
							&& !"".equals(permitdateconcrete)) {
						int year = Integer.parseInt(yearconcrete);
						if (checkpermitdategraterthanbuild(yearconcrete,
								permitdateconcrete) == true) {
							chkdatestatus[1] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[1] = "false";
							txtpermitconcrete.setEnabled(true);
							txtpermitconcrete.setText("");
							txtpermitconcrete.requestFocus();
						}

					} else {
						chkdatestatus[1] = "true";
					}

					chkstatus[1] = "true";
					if (!"".equals(yearconcrete)
							&& !"Select".equals(yearconcrete)) {
						int year = Integer.parseInt(yearconcrete);
						if (year >= 1990 && year <= cf.Current_year) {
							yearvalconc = yearconcrete;
							chkstatus[1] = "true";
						} else {
							ShowToast toast = new ShowToast(getBaseContext(),
									"Please enter the Date in valid format.");
							txtyearconcrete.requestFocus();
							chkglobal = true;
							chkstatus[1] = "false";
						}
					} else {
						chkstatus[1] = "true";
					}

				}
			}
			if ((roofmetal == "1" || chkmetalroofcovertype.isChecked() == true)
					&& (chkglobal == false)) {
				permitdatemetal = txtpermitmetal.getText().toString();
				prodapprovalmetal = txtproductmetal.getText().toString();
				yearmetal = txtyearmetal.getSelectedItem().toString();

				if (("".equals(permitdatemetal))
						&& ("".equals(prodapprovalmetal))
						&& ("Select".equals(yearmetal))
						&& (chkmetal.isChecked() == false)) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please select any one of the Required value for Roof Covering.");
					chkglobal = true;
					chkstatus[2] = "false";
				} else {

					if (!"".equals(yearmetal) && !"Select".equals(yearmetal)
							&& !"".equals(permitdatemetal)) {
						int year = Integer.parseInt(yearmetal);
						if (checkpermitdategraterthanbuild(yearmetal,
								permitdatemetal) == true) {
							chkdatestatus[2] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[2] = "false";
							txtpermitmetal.setEnabled(true);
							txtpermitmetal.setText("");
							txtpermitmetal.requestFocus();
						}

					} else {
						chkdatestatus[2] = "true";
					}

					if (!"".equals(yearmetal) && !"Select".equals(yearmetal)) {
						int year = Integer.parseInt(yearmetal);
						if (year >= 1990 && year <= cf.Current_year) {
							yearvalmetal = yearmetal;
							chkstatus[2] = "true";
						} else {
							ShowToast toast = new ShowToast(getBaseContext(),
									"Please enter the Date in valid format.");
							txtyearmetal.requestFocus();
							chkglobal = true;
							chkstatus[2] = "false";
						}
					} else {
						chkstatus[2] = "true";
					}
				}
			}
			if ((roofbuiltup == "1" || chkbuiltuproofcovertype.isChecked() == true)
					&& (chkglobal == false)) {
				permitdatebuiltup = txtpermitbuiltup.getText().toString();
				prodapprovalbuiltup = txtproductbuiltup.getText().toString();
				yearbuiltup = txtyearbuiltup.getSelectedItem().toString();

				if (("".equals(permitdatebuiltup))
						&& ("".equals(prodapprovalbuiltup))
						&& ("Select".equals(yearbuiltup))
						&& (chkbuiltup.isChecked() == false)) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please select any one of the Required value for Roof Covering.");
					chkglobal = true;
					chkstatus[3] = "false";
				} else {

					if (!"".equals(yearbuiltup)
							&& !"Select".equals(yearbuiltup)
							&& !"".equals(permitdatebuiltup)) {
						int year = Integer.parseInt(yearbuiltup);
						if (checkpermitdategraterthanbuild(yearbuiltup,
								permitdatebuiltup) == true) {
							chkdatestatus[3] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[3] = "false";
							txtpermitbuiltup.setEnabled(true);
							txtpermitbuiltup.setText("");
							txtpermitbuiltup.requestFocus();
						}

					} else {
						chkdatestatus[3] = "true";
					}

					if (!"".equals(yearbuiltup)
							&& !"Select".equals(yearbuiltup)) {
						int year = Integer.parseInt(yearbuiltup);
						if (year >= 1990 && year <= cf.Current_year) {
							yearvalbuiltup = yearbuiltup;
							chkstatus[3] = "true";
						} else {
							ShowToast toast = new ShowToast(getBaseContext(),
									"Please enter the Date in valid format.");
							txtyearbuiltup.requestFocus();
							chkglobal = true;
							chkstatus[3] = "false";
						}
					} else {
						chkstatus[3] = "true";
					}
				}
			}
			if ((roofmembrane == "1" || chkmembraneroofcovertype.isChecked() == true)
					&& (chkglobal == false)) {
				permitdatemembrane = txtpermitmembrane.getText().toString();
				prodapprovalmembrane = txtproductmembrane.getText().toString();
				yearmembrane = txtyearmembrane.getSelectedItem().toString();

				if (("".equals(permitdatemembrane))
						&& ("".equals(prodapprovalmembrane))
						&& ("Select".equals(yearmembrane))
						&& (chkmembrane.isChecked() == false)) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please select any one of the Required value for Roof Covering.");
					chkglobal = true;
					chkstatus[4] = "false";
				} else {

					if (!"".equals(yearmembrane)
							&& !"Select".equals(yearmembrane)
							&& !"".equals(permitdatemembrane)) {
						int year = Integer.parseInt(yearmembrane);
						if (checkpermitdategraterthanbuild(yearmembrane,
								permitdatemembrane) == true) {
							chkdatestatus[4] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[4] = "false";
							txtpermitmembrane.setEnabled(true);
							txtpermitmembrane.setText("");
							txtpermitmembrane.requestFocus();
						}

					} else {
						chkdatestatus[4] = "true";
					}

					if (!"".equals(yearmembrane)
							&& !"Select".equals(yearmembrane)) {
						int year = Integer.parseInt(yearmembrane);
						if (year >= 1990 && year <= cf.Current_year) {
							yearvalmemb = yearmembrane;
							chkstatus[4] = "true";
						} else {
							ShowToast toast = new ShowToast(getBaseContext(),
									"Please enter the Date in valid format.");
							txtyearmembrane.requestFocus();
							chkglobal = true;
							chkstatus[4] = "false";
						}
					} else {
						chkstatus[4] = "true";
					}
				}
			}
			if ((roofother == "1" || chkotherroofcovertype.isChecked() == true)
					&& (chkglobal == false)) {
				permitdateother = txtpermitother.getText().toString();
				prodapprovalother = txtproductother.getText().toString();
				yearother = txtyearother.getSelectedItem().toString();
				othercovertext = txtother.getText().toString();

				if (("".equals(permitdateother))
						&& ("".equals(prodapprovalother))
						&& ("Select".equals(yearother))
						&& (chkother.isChecked() == false)) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please select any one of the Required value for Roof Covering.");
					chkglobal = true;
					chkstatus[5] = "false";
				} else {

					if (!"".equals(yearother) && !"Select".equals(yearother)
							&& !"".equals(permitdateother)) {
						int year = Integer.parseInt(yearother);
						if (checkpermitdategraterthanbuild(yearother,
								permitdateother) == true) {
							chkdatestatus[5] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[5] = "false";
							txtpermitother.setEnabled(true);
							txtpermitother.setText("");
							txtpermitother.requestFocus();
						}

					} else {
						chkdatestatus[5] = "true";
					}

					if (("".equals(othercovertext.trim()) && (chkglobal == false))) {
						ShowToast toast = new ShowToast(getBaseContext(),
								"Please enter the text for Other Roof Covering type.");
						txtother.requestFocus();
						chkstatus[6] = "false";
						chkglobal = true;
					} else {
						chkstatus[6] = "true";
					}

					if (!"".equals(yearother) && !"Select".equals(yearother)) {
						int year = Integer.parseInt(yearother);
						if (year >= 1990 && year <= cf.Current_year) {
							yearvalother = yearother;
							chkstatus[5] = "true";
						} else {
							ShowToast toast = new ShowToast(getBaseContext(),
									"Please enter the Date in valid format.");
							txtyearother.requestFocus();
							chkglobal = true;
							chkstatus[5] = "false";
						}
					} else {
						chkstatus[5] = "true";
					}
				}
			} else {
				chkstatus[5] = "true";
				chkstatus[6] = "true";
			}

			if ((chkasphaltroofcovertype.isChecked() == true)
					|| (chkconcreteroofcovertype.isChecked() == true)
					|| (chkmetalroofcovertype.isChecked() == true)
					|| (chkbuiltuproofcovertype.isChecked() == true)
					|| (chkmembraneroofcovertype.isChecked() == true)
					|| (chkotherroofcovertype.isChecked() == true)) {

				if ((!rdioFBC.isChecked()) && (!rdioSFBC.isChecked())
						&& (!rdioAorB.isChecked())
						&& (!rdioNone.isChecked() && (chkglobal == false))) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please select Roof Covering option.");
					chkglobal = true;

				} else if (comm.trim().equals("") && (chkglobal == false)) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please enter the Comments for Roof Covering.");
					chkglobal = true;

				} else if ((!radio_most[0].isChecked())
						&& (!radio_most[1].isChecked())
						&& (!radio_most[2].isChecked())
						&& (!radio_most[3].isChecked())
						&& (!radio_most[4].isChecked())
						&& (!radio_most[5].isChecked() && (chkglobal == false))) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"Please select Predominant Type for Roof Covering.");
					chkglobal = true;

				} else {
					if ((chkstatus[0] == "true") && (chkstatus[1] == "true")
							&& (chkstatus[2] == "true")
							&& (chkstatus[3] == "true")
							&& (chkstatus[4] == "true")
							&& (chkstatus[5] == "true")
							&& (chkstatus[6] == "true")
							&& (chkdatestatus[0] == "true")
							&& (chkdatestatus[1] == "true")
							&& (chkdatestatus[2] == "true")
							&& (chkdatestatus[3] == "true")
							&& (chkdatestatus[4] == "true")
							&& (chkdatestatus[5] == "true")
							&& (chkglobal = true)) {
						dbinsert();
					}
				}
			}

			else {
				ShowToast toast = new ShowToast(getBaseContext(),
						"Please select Roof Covering Type.");
				chkglobal = true;
			}

			break;

		case R.id.hme:
			cf.gohome();
			break;
		}
	}

	private void dbinsert() {
		// TODO Auto-generated method stub
		if ((chkstatus[0] == "true") && (chkstatus[1] == "true")
				&& (chkstatus[2] == "true") && (chkstatus[3] == "true")
				&& (chkstatus[4] == "true") && (chkstatus[5] == "true")
				&& (chkstatus[6] == "true")) {

			if (chkasphalt.isChecked() == true) {
				yearinfo1 = 1;
			} else if (chkasphalt.isChecked() == false) {
				yearinfo1 = 0;
			}
			if (chkconcrete.isChecked() == true) {
				yearinfo2 = 1;
			} else if (chkconcrete.isChecked() == false) {
				yearinfo2 = 0;
			}
			if (chkmetal.isChecked() == true) {
				yearinfo3 = 1;
			} else if (chkmetal.isChecked() == false) {
				yearinfo3 = 0;
			}
			if (chkbuiltup.isChecked() == true) {
				yearinfo4 = 1;
			} else if (chkbuiltup.isChecked() == false) {
				yearinfo4 = 0;
			}
			if (chkmembrane.isChecked() == true) {
				yearinfo5 = 1;
			} else if (chkmembrane.isChecked() == false) {
				yearinfo5 = 0;
			}
			if (chkother.isChecked() == true) {
				yearinfo6 = 1;
			} else if (chkother.isChecked() == false) {
				yearinfo6 = 0;
			}
			updatecnt = "1";
			try {
				String RoofCoverValue_f = "";
				System.out.println("RoofCoverValue.length "+RoofCoverValue.length);
				for (int i = 0; i < RoofCoverValue.length; i++) {
					if (RoofCoverValue[i] != 0) {
						RoofCoverValue_f += String.valueOf(i + 1) + "^";
						System.out.println("RoofCoverValue_f "+RoofCoverValue_f);
					}
				}
				RoofCoverValue_f = RoofCoverValue_f.substring(0,
						RoofCoverValue_f.length() - 1);
				Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.quesroofcover
						+ " WHERE SRID='" + cf.Homeid + "'", null);
				int rws = c2.getCount();
				if (rws == 0) {

					cf.db.execSQL("INSERT INTO "
							+ cf.quesroofcover
							+ " (SRID,RoofCoverType,RoofCoverValue,RoofPreDominant,PermitApplnDate1,PermitApplnDate2,PermitApplnDate3,PermitApplnDate4,PermitApplnDate5,PermitApplnDate6,RoofCoverTypeOther,ProdApproval1,ProdApproval2,ProdApproval3,ProdApproval4,ProdApproval5,ProdApproval6,InstallYear1,InstallYear2,InstallYear3,InstallYear4,InstallYear5,InstallYear6,NoInfo1,NoInfo2,NoInfo3,NoInfo4,NoInfo5,NoInfo6)"
							+ "VALUES ('" + cf.Homeid + "','"
							+ cf.convertsinglequotes(RoofCoverValue_f) + "','"
							+ cf.convertsinglequotes(roofcovervalue) + "','"
							+ predominant + "','"
							+ cf.convertsinglequotes(permitdateasphalt) + "','"
							+ cf.convertsinglequotes(permitdateconcrete)
							+ "','" + cf.convertsinglequotes(permitdatemetal)
							+ "','" + cf.convertsinglequotes(permitdatebuiltup)
							+ "','"
							+ cf.convertsinglequotes(permitdatemembrane)
							+ "','" + cf.convertsinglequotes(permitdateother)
							+ "','" + cf.convertsinglequotes(othercovertext)
							+ "','"
							+ cf.convertsinglequotes(prodapprovalasphalt)
							+ "','"
							+ cf.convertsinglequotes(prodapprovalconcrete)
							+ "','" + cf.convertsinglequotes(prodapprovalmetal)
							+ "','"
							+ cf.convertsinglequotes(prodapprovalbuiltup)
							+ "','"
							+ cf.convertsinglequotes(prodapprovalmembrane)
							+ "','" + cf.convertsinglequotes(prodapprovalother)
							+ "','" + cf.convertsinglequotes(yearvalasp)
							+ "','" + cf.convertsinglequotes(yearvalconc)
							+ "','" + cf.convertsinglequotes(yearvalmetal)
							+ "','" + cf.convertsinglequotes(yearvalbuiltup)
							+ "','" + cf.convertsinglequotes(yearvalmemb)
							+ "','" + cf.convertsinglequotes(yearvalother)
							+ "','" + yearinfo1 + "','" + yearinfo2 + "','"
							+ yearinfo3 + "','" + yearinfo4 + "','" + yearinfo5
							+ "','" + yearinfo6 + "')");
				} else {

					if (roofasphalt == "1"
							|| chkasphaltroofcovertype.isChecked() == true
							|| chkstatus1[0] == "false") {
						cf.db.execSQL("UPDATE " + cf.quesroofcover
								+ " SET RoofCoverType='" + cf.convertsinglequotes(RoofCoverValue_f)
								+ "',RoofCoverValue='" + roofcovervalue
								+ "',RoofPreDominant='" + predominant
								+ "',PermitApplnDate1='" + cf.convertsinglequotes(permitdateasphalt)
								+ "',ProdApproval1 ='"
								+ cf.convertsinglequotes(prodapprovalasphalt)
								+ "',InstallYear1='" + yearvalasp
								+ "',NoInfo1='" + yearinfo1 + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");

					}
					if (roofconcrete == "1"
							|| chkconcreteroofcovertype.isChecked() == true
							|| chkstatus1[1] == "false") {
						cf.db.execSQL("UPDATE " + cf.quesroofcover
								+ " SET RoofCoverType='" + cf.convertsinglequotes(RoofCoverValue_f)
								+ "',RoofCoverValue='" + roofcovervalue
								+ "',RoofPreDominant='" + predominant
								+ "',PermitApplnDate2='" + cf.convertsinglequotes(permitdateconcrete)
								+ "',ProdApproval2 ='"
								+ cf.convertsinglequotes(prodapprovalconcrete)
								+ "',InstallYear2='" + yearvalconc
								+ "',NoInfo2='" + yearinfo2 + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");

					}
					if (roofmetal == "1"
							|| chkmetalroofcovertype.isChecked() == true
							|| chkstatus1[2] == "false") {
						cf.db.execSQL("UPDATE " + cf.quesroofcover
								+ " SET RoofCoverType='" + cf.convertsinglequotes(RoofCoverValue_f)
								+ "',RoofCoverValue='" + roofcovervalue
								+ "',RoofPreDominant='" + predominant
								+ "',PermitApplnDate3='" + cf.convertsinglequotes(permitdatemetal)
								+ "',ProdApproval3 ='"
								+ cf.convertsinglequotes(prodapprovalmetal)
								+ "',InstallYear3='" + yearvalmetal
								+ "',NoInfo3='" + yearinfo3 + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");

					}
					if (roofbuiltup == "1"
							|| chkbuiltuproofcovertype.isChecked() == true
							|| chkstatus1[3] == "false") {
						cf.db.execSQL("UPDATE " + cf.quesroofcover
								+ " SET RoofCoverType='" + cf.convertsinglequotes(RoofCoverValue_f)
								+ "',RoofCoverValue='" + roofcovervalue
								+ "',RoofPreDominant='" + predominant
								+ "',PermitApplnDate4='" + cf.convertsinglequotes(permitdatebuiltup)
								+ "',ProdApproval4 ='"
								+ cf.convertsinglequotes(prodapprovalbuiltup)
								+ "',InstallYear4='" + yearvalbuiltup
								+ "',NoInfo4='" + yearinfo4 + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");

					}
					if (roofmembrane == "1"
							|| chkmembraneroofcovertype.isChecked() == true
							|| chkstatus1[4] == "false") {
						cf.db.execSQL("UPDATE " + cf.quesroofcover
								+ " SET RoofCoverType='" + cf.convertsinglequotes(RoofCoverValue_f)
								+ "',RoofCoverValue='" + roofcovervalue
								+ "',RoofPreDominant='" + predominant
								+ "',PermitApplnDate5='" + cf.convertsinglequotes(permitdatemembrane)
								+ "',ProdApproval5='"
								+ cf.convertsinglequotes(prodapprovalmembrane)
								+ "',InstallYear5='" + yearvalmemb
								+ "',NoInfo5='" + yearinfo5 + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");
					}
					if (roofother == "1"
							|| chkotherroofcovertype.isChecked() == true
							|| chkstatus1[5] == "false") {
						cf.db.execSQL("UPDATE " + cf.quesroofcover
								+ " SET RoofCoverType='" + cf.convertsinglequotes(RoofCoverValue_f)
								+ "',RoofCoverValue='" + roofcovervalue
								+ "',RoofPreDominant='" + predominant
								+ "',PermitApplnDate6='" + cf.convertsinglequotes(permitdateother)
								+ "',ProdApproval6='"
								+ cf.convertsinglequotes(prodapprovalother)
								+ "',InstallYear6='" + yearvalother
								+ "',NoInfo6='" + yearinfo6
								+ "',RoofCoverTypeOther='"
								+ cf.convertsinglequotes(othercovertext) + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");
					}

				}
			} catch (Exception e) {

				updatecnt = "0";
				ShowToast toast = new ShowToast(getBaseContext(),
						"There is a problem in saving your data due to invalid character.");
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" Problem in inserting or updating roof cover information on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
			try {

				Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.tbl_comments
						+ " WHERE SRID='" + cf.Homeid + "'", null);
				int rws = c2.getCount();

				if (rws == 0) {

					cf.db.execSQL("INSERT INTO "
							+ cf.tbl_comments
							+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment,RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','"
							+ cf.InspectionType
							+ "','','','"
							+ cf.convertsinglequotes(comments.getText()
									.toString())
							+ "','','','','','','','','','','','','','','','','','"
							+ cd + "')");

				} else {

					cf.db.execSQL("UPDATE "
							+ cf.tbl_comments
							+ " SET RoofCoverComment='"
							+ cf.convertsinglequotes(comments.getText()
									.toString())
							+ "',RoofCoverAdminComment='',CreatedOn ='" + md
							+ "'" + " WHERE SRID ='" + cf.Homeid.toString()
							+ "'");
				}

			} catch (Exception e) {

				updatecnt = "0";
				ShowToast toast = new ShowToast(getBaseContext(),
						"There is a problem in saving your data due to invalid character.");
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" problem in saving roofcover comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
			try {
				cf.getInspectorId();
				Cursor c3 = cf.db.rawQuery("SELECT * FROM "
						+ cf.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
						+ "'", null);
				int subchkrws = c3.getCount();
				if (subchkrws == 0) {
					cf.db.execSQL("INSERT INTO "
							+ cf.SubmitCheckTable
							+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
							+ "VALUES ('" + cf.InspectorId + "','" + cf.Homeid
							+ "',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)");
				} else {
					cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
							+ " SET fld_roofcover='1' WHERE Srid ='"
							+ cf.Homeid + "' and InspectorId='"
							+ cf.InspectorId + "'");

				}
			} catch (Exception e) {

				updatecnt = "0";
				ShowToast toast = new ShowToast(getBaseContext(),
						"There is a problem in saving your data due to invalid character.");
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" problem in saving roofcover info invalid character on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
			cf.changeimage();
			// roofcovertick.setVisibility(visibility);
			if (updatecnt == "1") {
				ShowToast toast = new ShowToast(getBaseContext(),
						"Roof covering details has been saved successfully.");
				iInspectionList = new Intent(RoofCover.this, RoofDeck.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("iden", "test");
				iInspectionList.putExtra("InspectionType", cf.InspectionType);
				iInspectionList.putExtra("status", cf.status);
				iInspectionList.putExtra("keyName", cf.value);
				iInspectionList.putExtra("Count", cf.Count);
				startActivity(iInspectionList);
			}
		} else {
		}

	}

	private boolean checkpermitdategraterthanbuild(String yearbuilt3,
			String permitDate3) {
		// TODO Auto-generated method stub
		int i1 = permitDate3.indexOf("/");
		String result = permitDate3.substring(0, i1);
		int i2 = permitDate3.lastIndexOf("/");
		String result1 = permitDate3.substring(i1 + 1, i2);
		String result2 = permitDate3.substring(i2 + 1);
		result2 = result2.trim();
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt3 = yearbuilt3.trim();
		int yr = Integer.parseInt(yearbuilt3);
		if (yr < j) {
			ShowToast toast = new ShowToast(getBaseContext(),
					"The Permit Application Date should not be greater than the Year Built.");
			return false;
		} else {
			return true;
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {

			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;

			if (chkboxvalidation[0].equals("1")) {
				txtpermitasphalt.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));

			} else if (chkboxvalidation[1].equals("1")) {
				txtpermitconcrete.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			} else if (chkboxvalidation[2].equals("1")) {
				txtpermitmetal.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			} else if (chkboxvalidation[3].equals("1")) {
				txtpermitbuiltup.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			} else if (chkboxvalidation[4].equals("1")) {
				txtpermitmembrane.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			} else if (chkboxvalidation[5].equals("1")) {
				txtpermitother.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			} else {
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);

		}
		return null;
	}

	private void getcountname() {
		try {
			Cursor cur = cf.db.rawQuery(
					"select * from Retail_inpgeneralinfo where s_SRID='"
							+ cf.Homeid + "'", null);

			int Column1 = cur.getColumnIndex("s_County");
			cur.moveToFirst();
			if (cur != null) {

				count_value = cur.getString(Column1);

			}
		} catch (Exception e) {
			
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" problem in getting county names on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(RoofCover.this, BuildCode.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("iden", "prev");
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public int getkeyarray(String[] arr, String value) {
		if (value.trim().equals("")) {
			return 0;
		}
		for (int i = 1; i < arr.length; i++) {
			if (arr[i].equals(value)) {
				return i;
			}

		}
		return 0;
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	    if(focus==1)
	    {
	    	focus=0;
	    	comments.setText(comments.getText().toString());    
	    }
	    
	 }    
}