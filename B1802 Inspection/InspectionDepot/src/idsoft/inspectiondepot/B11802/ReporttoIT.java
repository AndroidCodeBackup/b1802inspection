package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.GeneralImages.ImageAdapter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class ReporttoIT extends Activity implements Runnable  {
	EditText username,contctno,contactemail,imgupload,comments;
	Button submit;TextView inspname,welcome;String picname="",Ins_Email,vn;
	ImageView imgbtn;int t=0;
	private String[] selectedImagePath={"","",""};
	private ImageAdapter imageAdapter;
	private int count1;
	ImageView RI_Image,RI_Image2,RI_Image3;
	RelativeLayout image_li1,image_li2,image_li3;
	int count;MarshalBase64 marshal;

	CommonFunctions cf;Bitmap bitmap;ProgressDialog pd;protected int show_handler;
	private static final int SELECT_PICTURE = 0;
	UploadRestriction upr = new UploadRestriction();
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.reporttoit);
		cf = new CommonFunctions(this);
		cf.getInspectorId();
		cf.welcomemessage();
		cf.Device_Information();
		inspname = (TextView) findViewById(R.id.RI_ed_name);
		contactemail = (EditText) findViewById(R.id.etcontactemail);
		RI_Image=(ImageView) findViewById(R.id.RI_image);
        RI_Image2=(ImageView) findViewById(R.id.RI_image_2);
        RI_Image3=(ImageView) findViewById(R.id.RI_image_3);
        image_li1 =(RelativeLayout) findViewById(R.id.img_1);
        image_li2 =(RelativeLayout) findViewById(R.id.img_2);
        image_li3 =(RelativeLayout) findViewById(R.id.img_3);
        TextView txtcomments = (TextView) findViewById(R.id.RI_tv_cmt);
        txtcomments.setText(Html.fromHtml(cf.redcolor+" "+"Comments :"));
		comments = (EditText) findViewById(R.id.etcomments);
		welcome = (TextView) this.findViewById(R.id.welcomename);
		welcome.setText(cf.data);
		inspname.setText(cf.data);
		
		try {
			Cursor cur = cf.SelectTablefunction(cf.inspectorlogin," where Ins_Flag1=1");
				
			if(cur.getCount()!=0)
			{
			cur.moveToFirst();
			if (cur != null) {
				do {
					
					this.Ins_Email = cf.getsinglequotes(cur.getString(cur.getColumnIndex("Ins_Email")));
					
					System.out.println("Ins_Email "+this.Ins_Email);
					contactemail.setText(this.Ins_Email);
				} while (cur.moveToNext());
			}
			cur.close();
			
			}
			else
			{
			
			}
		}
		catch(Exception e)
		{
			System.out.println("report to IT "+e.getMessage());
		}
		
		
		
	}
	
	 public void clicker(View v)
	 {
		 switch(v.getId())
		  {		 
			case R.id.hme:
				cf.gohome();
				break;
			
		 
		  case R.id.save:
		  try
			{
			if(cf.Email_Validation(contactemail.getText().toString()).equals("Yes") || contactemail.getText().toString().equals("") )
			{
						
					ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
					NetworkInfo info = conMgr
							.getActiveNetworkInfo();
					if (info != null && info.isConnected()) {
						
						if(comments.getText().toString().trim().equals("")){
							cf.ShowToast("Please Enter Comments.",1);
							comments.requestFocus();
						}
						else{pd = ProgressDialog.show(ReporttoIT.this, "", "Reporting to IT", true);
						Thread thread = new Thread(ReporttoIT.this);
						thread.start();}

							
					}
					else
					{
						
						cf.ShowToast("Internet connection is not available.",1);
					}
			}else
			{
				
				cf.ShowToast("Please enter the valid Email.",1);
				
			}
		
			
		}
		catch (Exception ex) 
		{
			System.out.println("error occuers extest"+ex.getMessage());
		}
	
	
		  break;
		  case R.id.clear:
			  comments .setText("");
			//  contactemail .setText("");			  
			  selectedImagePath[0]="";
			  selectedImagePath[1]="";
			  selectedImagePath[2]="";
			  image_li1.setVisibility(View.GONE);
			  image_li2.setVisibility(View.GONE);
			  image_li3.setVisibility(View.GONE);

		  break;
		  case R.id.cancel:
			  cf.gohome();
		  break;
		  
		  case R.id.RI_bt_brw1:
			  if((image_li1.getVisibility()==View.GONE) || (image_li2.getVisibility()==View.GONE) || (image_li3.getVisibility()==View.GONE))
				{
					
					if((image_li1.getVisibility()==View.GONE))
					{
						count=0;
					}
					else if((image_li2.getVisibility()==View.GONE))
					{
						count=1;
					}
					else if((image_li3.getVisibility()==View.GONE))
					{
						count=2;
					}
					
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_PICTURE);
				}
			 break;
		
		 
		  case R.id.RI_image_c:
			  selectedImagePath[0]="";
			  image_li1.setVisibility(v.GONE); 
		  break;
		  case R.id.RI_image2_c:
			  selectedImagePath[1]="";
			  image_li2.setVisibility(v.GONE);
		  break;
		  case R.id.RI_image3_c:
			 selectedImagePath[2]="";
			  image_li3.setVisibility(v.GONE);
	      break;

		  }
	 }
	 public boolean senduser_error()throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException  {
			// TODO Auto-generated method stub
			try {
				
				 vn = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject request = new SoapObject(cf.NAMESPACE,cf.METHOD_NAME34);

				request.addProperty("InspectorID",(cf.InspectorId.equals(""))?"":cf.InspectorId);
				request.addProperty("ApplicationVersion",vn);
				request.addProperty("DeviceName",cf.model);
				request.addProperty("APILevel",cf.apiLevel);
				request.addProperty("InspectorName",inspname.getText().toString());
				request.addProperty("Comments",comments.getText().toString());
				request.addProperty("ContactNumber","");
				request.addProperty("ContactEmail",contactemail.getText().toString());
				request.addProperty("ReportDate",cf.datewithtime);
				
				if(!selectedImagePath[0].equals("") && image_li1.getVisibility()==View.VISIBLE)
				  {
					
				String mypath = selectedImagePath[0]; // get the full path of the image 
					String temppath[] = mypath.split("/");
					int ss = temppath[temppath.length - 1].lastIndexOf(".");
					String tests = temppath[temppath.length - 1].substring(ss);
					String elevationttype;byte[] raw;
					File f = new File(mypath);System.out.println("dFile "+f);
					if(f.exists())
					{
						System.out.println("dfdf exist");
						if(tests.equals("") || tests.equals("null")	|| tests == null)
						{
							tests=".jpg";
						}
						elevationttype ="Error image"+tests;
						

						Bitmap bitmap = ShrinkBitmap(selectedImagePath[0], 800, 800);
						System.out.println("dt"+bitmap);
						
							marshal = new MarshalBase64();
							ByteArrayOutputStream out = new ByteArrayOutputStream();
							bitmap.compress(CompressFormat.PNG, 100, out);
							System.out.println("out "+out);
							raw = out.toByteArray();
							System.out.println("raw"+raw);
							request.addProperty("imgByte1",raw);
				
						
						request.addProperty("ImageNameWithExtension1",elevationttype);
						marshal.register(envelope);
					}
				  }
				 if(!selectedImagePath[1].equals("") && image_li2.getVisibility()==View.VISIBLE)
				  {
				  	String mypath = selectedImagePath[1]; // get the full path of the image 
					String temppath[] = mypath.split("/");
					int ss = temppath[temppath.length - 1].lastIndexOf(".");
					String tests = temppath[temppath.length - 1].substring(ss);
					String elevationttype;
					File f = new File(mypath);
					if(f.exists())
					{
						if(tests.equals("") || tests.equals("null")	|| tests == null)
						{
							tests=".jpg";
						}
						elevationttype ="Error image"+tests;
						
						Bitmap bitmap = ShrinkBitmap(selectedImagePath[1], 400, 400);

						MarshalBase64 marshal = new MarshalBase64();
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100, out);
						byte[] raw = out.toByteArray();
						System.out.println("raw"+raw);
						request.addProperty("imgByte2",raw);
						request.addProperty("ImageNameWithExtension2",elevationttype);
						marshal.register(envelope);
					}
				}
				  else
				  {
						
					  	request.addProperty("imgByte2","");
						request.addProperty("ImageNameWithExtension2","");
						
						 System.out.println("iin else"+request);
				  }
				 if(!selectedImagePath[2].equals("") && image_li3.getVisibility()==View.VISIBLE)
				  {
				  	String mypath = selectedImagePath[2]; // get the full path of the image 
					String temppath[] = mypath.split("/");
					int ss = temppath[temppath.length - 1].lastIndexOf(".");
					String tests = temppath[temppath.length - 1].substring(ss);
					String elevationttype;
					File f = new File(mypath);
					if(f.exists())
					{
						if(tests.equals("") || tests.equals("null")	|| tests == null)
						{
							tests=".jpg";
						}
						elevationttype ="Error image"+tests;
						
						Bitmap bitmap = ShrinkBitmap(selectedImagePath[2], 800, 800);

						MarshalBase64 marshal = new MarshalBase64();
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100, out);
						byte[] raw = out.toByteArray();
						System.out.println("raw"+raw);
						request.addProperty("imgByte3",raw);
						request.addProperty("ImageNameWithExtension3",elevationttype);
						marshal.register(envelope);
					}
				}
				  else
				  {
						
					  	request.addProperty("imgByte3","");
						request.addProperty("ImageNameWithExtension3","");
						
						
				  }
				 envelope.setOutputSoapObject(request);
				 HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
				 androidHttpTransport.call(cf.NAMESPACE+"SendReportToIT",envelope);
				 String result =  envelope.getResponse().toString();
				 System.out.println("result"+result);
				  return cf.check_Status(result);
				
		} 
	 
	 @Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					Uri selectedImageUri = data.getData();
					if(selectedImagePath[0].equals(getPath(selectedImageUri)) || selectedImagePath[1].equals(getPath(selectedImageUri)) || selectedImagePath[2].equals(getPath(selectedImageUri)))
					{
						cf.ShowToast("Your image has been selected already.",1);
					}
					else
					{
						selectedImagePath[count] = getPath(selectedImageUri);
						boolean bu = cf.common(selectedImagePath[count]);
						if (bu) {
								try
								{
									Bitmap b=ShrinkBitmap(selectedImagePath[count], 800, 800);
									System.out.println("BB "+b);
									if(b!=null)
									{
										if(count==0)
										{
											RI_Image.setImageBitmap(b);
											image_li1.setVisibility(View.VISIBLE);
										}
										else if(count==1)
										{
											RI_Image2.setImageBitmap(b);
											image_li2.setVisibility(View.VISIBLE);
										}
										else if(count==2)
										{
											RI_Image3.setImageBitmap(b);
											image_li3.setVisibility(View.VISIBLE);
										}
									}
									else
									{
										selectedImagePath[count]="";
										cf.ShowToast("This image is not a supported format.You cannot upload.",1);
									}
								}
								catch(Exception e)
								{
									cf.ShowToast("Your selected image have some problem. Please try another one.",1);
									if(count==0)
									{
										
										image_li1.setVisibility(View.GONE);
									}
									else if(count==1)
									{
										
										image_li2.setVisibility(View.GONE);
									}
									else if(count==2)
									{
										
										image_li3.setVisibility(View.GONE);
									}
								}
								
							
						} else {
							cf.ShowToast("Please choose image less then 2MB.",1);
						}
					}
				
				}
		}

	}

	 public String getPath(Uri uri) {
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = managedQuery(uri, projection, null, null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
	
	public void run() {
		boolean b;
		try {
			b = senduser_error();
			if(b)
			{
				show_handler=0;
				handler.sendEmptyMessage(0);
			}
			else
			{
				show_handler=1;
				handler.sendEmptyMessage(0);
			}
		} catch (SocketTimeoutException e) {
			show_handler=2;
			handler.sendEmptyMessage(0);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NetworkErrorException e) {
			show_handler=2;
			handler.sendEmptyMessage(0);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			show_handler=2;
			// TODO Auto-generated catch block
			handler.sendEmptyMessage(0);
			
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			System.out.println("error occuers exmess"+e.getMessage());
			/*String	strerrorlog="Exception happens while Uploading the error information from the user  Error ="+e;
			cf.Error_LogFile_Creation(strerrorlog+" "+" at export inspection  in the stage of Exporting at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);*/
			show_handler=2;
			handler.sendEmptyMessage(0);
			
		}
	}
		
		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				pd.dismiss();
				if(show_handler==0)
				{
					cf.ShowToast("Thank you for submitting your IT issue. We will review this and get back to you as soon as possible..",1);
				   //contactemail.setText("");
					comments.setText("");
					image_li1.setVisibility(View.GONE);  
					image_li2.setVisibility(View.GONE); 
					image_li3.setVisibility(View.GONE);

				}
				else if(show_handler==1)
				{
					cf.ShowToast("Report sending failed.",1);
				}
				else if(show_handler==2)
				{
					System.out.println("error occuers exsdsd");
				}
			}
		};
	
	Bitmap ShrinkBitmap(String file, int width, int height) {

		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			
			return bitmap;
		}

	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent startMain = new Intent(this,HomeScreen.class);
			startActivity(startMain);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}