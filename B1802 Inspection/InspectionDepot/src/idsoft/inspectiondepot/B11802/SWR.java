package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.OverallComments.OC_textwatcher;
import idsoft.inspectiondepot.B11802.OverallComments.Touch_Listener;

import java.util.LinkedHashMap;
import java.util.Map;




import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class SWR extends Activity {
	ListView list;
	RelativeLayout tblcommentschange;
	ImageView Vimage;public int focus=0;
	private static final int visibility = 0;
	TextWatcher watcher;
	String commentsfill, commdescrip = "",inspectortypeid, helpcontent,InspectionType, status, comm, homeId, rdiochk, yearbuilt,
			permitdate, updatecnt, identity, roofswrvalueprev, roofswrcomment,conchkbox, comm2, descrip;
	String[] arrcomment1;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	RadioButton rdioA, rdioB, rdioC;
	int value, Count,viewimage = 1,optionid,commentsch;
	Intent iInspectionList;
	TextView nocommentsdisp, txtswrheading,prevmitidata,helptxt,swr_TV_type1;
	EditText comments, yrbuilt1, yrbuilt2, permitdate1, permitdate2;
	Button saveclose;
	private TableLayout tbllayout8, exceedslimit1;
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();
	View v1;
	LinearLayout lincomments;
	CheckBox[] cb;
	CheckBox temp_st;
	AlertDialog alertDialog;
	LinearLayout swr_parrant,swr_type1;
	
	CommonFunctions cf;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.Homeid = homeId = bunhomeId.getString("homeid");
			cf.Identity = identity = bunhomeId.getString("iden");
			cf.InspectionType = InspectionType = bunhomeId
					.getString("InspectionType");
			cf.status = status = bunhomeId.getString("status");
			cf.value = value = bunhomeId.getInt("keyName");
			cf.Count = Count = bunhomeId.getInt("Count");

		}
		setContentView(R.layout.swr);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.getDeviceDimensions();
		cf.changeimage();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 26,
				cf, 1));
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		focus=1;
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==null){
					cf.alertcontent = "To see help comments, please select SWR options.";
				}
				else if (rdiochk.equals("1") || rdioA.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection A, �SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else if (rdiochk.equals("2") || rdioB.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection B, �No SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else if (rdiochk.equals("3") || rdioC.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection C, �unable to verify/unknown� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else {
					cf.alertcontent = "To see help comments, please select SWR options.";
				}
				cf.showhelp("HELP",cf.alertcontent);
			}
		});
		txtswrheading = (TextView) findViewById(R.id.txtswrheading);

		txtswrheading
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>What is the roof shape? (Do not consider roofs of porches or carports that are attached only to the fascia or wall of the host structure over unenclosed space in the determination of roof perimeter or roof area for roof geometry classification). "));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		try {
			Cursor c2 = cf.SelectTablefunction(cf.quetsionsoriginaldata,
					"where homeid='" + cf.Homeid + "'");
			if(c2.getCount()>0){

			c2.moveToFirst();
			String bcoriddata = cf.getsinglequotes(c2.getString(c2
					.getColumnIndex("swroriginal")));
			
			prevmitidata.setText(Html
					.fromHtml("<font color=blue>Original Value : "
							+ "</font>" + "<font color=red>" + bcoriddata
							+ "</font>"));
			}
			else
			{
				prevmitidata.setText(Html
						.fromHtml("<font color=blue>Original Value : "
								+ "</font>" + "<font color=red>Not Available</font>"));
			}
			
		} catch (Exception e) {

		}
		
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
	//	rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		Vimage = (ImageView) findViewById(R.id.Vimage);
		tblcommentschange = (RelativeLayout) findViewById(R.id.tbllayoutswrcomments);
		this.nocommentsdisp = (TextView) this.findViewById(R.id.notxtcomments);

		this.tbllayout8 = (TableLayout) this.findViewById(R.id.tableLayoutComm);
		this.lincomments = (LinearLayout) this
				.findViewById(R.id.linearlayoutcomm);
		exceedslimit1 = (TableLayout) this.findViewById(R.id.exceedslimit);
		this.rdioA = (RadioButton) this.findViewById(R.id.rdio1);
		this.rdioA.setOnClickListener(OnClickListener);
		this.rdioB = (RadioButton) this.findViewById(R.id.rdio2);
		this.rdioB.setOnClickListener(OnClickListener);
		this.rdioC = (RadioButton) this.findViewById(R.id.rdio3);
		this.rdioC.setOnClickListener(OnClickListener);
		
		comments = (EditText) this.findViewById(R.id.txtswrcomments);
		comments.setOnTouchListener(new Touch_Listener());
		comments.addTextChangedListener(new QUES_textwatcher());
		
		swr_parrant = (LinearLayout)this.findViewById(R.id.swr_parrent);
		swr_type1=(LinearLayout) findViewById(R.id.swr_type);
		swr_TV_type1 = (TextView) findViewById(R.id.swr_txt);
		
		try {
			Cursor c2 = cf.db.rawQuery("SELECT SWRValue FROM "
					+ cf.tbl_questionsdata + " WHERE SRID='" + cf.Homeid + "'",
					null);
			int chkrws = c2.getCount();
			if (chkrws == 0) {
				identity = "test";
			} else {
				identity = "prev";
			}

		} catch (Exception e) {

		}
		if (identity.equals("prev")) {
			try {
				Cursor cur = cf.db.rawQuery("select * from "
						+ cf.tbl_questionsdata + " where SRID='" + cf.Homeid
						+ "'", null);
				cur.moveToFirst();
				if (cur != null) {
					roofswrvalueprev = cur.getString(cur
							.getColumnIndex("SWRValue"));

					if (roofswrvalueprev.equals("1")) {
						rdioA.setChecked(true);
						rdiochk = "1";

					} else if (roofswrvalueprev.equals("2")) {
						rdioB.setChecked(true);
						rdiochk = "2";
					} else if (roofswrvalueprev.equals("3")) {
						rdioC.setChecked(true);
						rdiochk = "3";
					} else {

					}

				}
			} catch (Exception e) {
				
			}

			try {
				Cursor cur = cf.db.rawQuery("select * from " + cf.tbl_comments
						+ " where SRID='" + cf.Homeid + "'", null);
				cur.moveToFirst();
				if (cur != null) {
					roofswrcomment = cf.getsinglequotes(cur.getString(cur
							.getColumnIndex("SecondaryWaterComment")));
					cf.showing_limit(roofswrcomment,swr_parrant,swr_type1,swr_TV_type1,"474");	
					comments.setText(roofswrcomment);
					
				}
			} catch (Exception e) {
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SWR.this +" in retrieving QUES comments table on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}
		this.Vimage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}

		});
		this.tblcommentschange.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}

		});
	}

	class Touch_Listener implements OnTouchListener
	{
		   
		   Touch_Listener()
			{
			
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	
		    		cf.setFocus(comments);
				return false;
		    }
	}
	class QUES_textwatcher implements TextWatcher
	{
	     QUES_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
				cf.showing_limit(s.toString(),swr_parrant,swr_type1,swr_TV_type1,"474");			
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		
	private void arrowcommentschange() {
		// TODO Auto-generated method stub
		Vimage.setBackgroundResource(R.drawable.arrowup);
		if (viewimage == 1) {
			Vimage.setBackgroundResource(R.drawable.arrowup);
			if (rdioA.isChecked()) {
				optionid = 1;
			} else if (rdioB.isChecked()) {
				optionid = 2;
			} else if (rdioC.isChecked()) {
				optionid = 3;
			} else {
				cf.ShowToast("Please select SWR option to view Comments.",1);

			}
			try {
				descrip = "";
				arrcomment1 = null;
				Cursor c2 = cf.db.rawQuery("SELECT * FROM "
						+ cf.tbl_admcomments
						+ " WHERE questionid='6' and optionid='" + optionid
						+ "' and status='Active' and InspectorId='"
						+ cf.InspectionType + "'", null);
				int rws = c2.getCount();
				arrcomment1 = new String[rws];
				c2.moveToFirst();
				if (rws == 0) {
					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.VISIBLE);
					lincomments.setVisibility(v1.GONE);
					Vimage.setBackgroundResource(R.drawable.arrowdown);
				} else {

					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.GONE);

					lincomments.setVisibility(v1.VISIBLE);
					if (c2 != null) {
						int i=0;
						do {
							arrcomment1[i]=cf.getsinglequotes(c2.getString(c2.getColumnIndex("description")));
							if(arrcomment1[i].contains("null"))
							{
								arrcomment1[i] = arrcomment1[i].replace("null", "");
							}							
							i++;
						} while (c2.moveToNext());						
					}
					c2.close();

					addcomments();
					viewimage = 0;
				}

			} catch (Exception e) {
				/*
				 * ShowToast toast = new ShowToast(getBaseContext(),
				 * "Please add atleast one swr comment in dashboard");
				 */
			//	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SWR.this +" in retrieving swr comments data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}

		} else if (viewimage == 0) {
			Vimage.setBackgroundResource(R.drawable.arrowdown);
			tbllayout8.setVisibility(v1.GONE);
			viewimage = 1;
		}

	}

	private void addcomments() {
		// TODO Auto-generated method stub
		lincomments.removeAllViews();
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		lincomments.addView(l1);

		cb = new CheckBox[arrcomment1.length];

		for (int i = 0; i < arrcomment1.length; i++) {

			LinearLayout l2 = new LinearLayout(this);			
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.setMargins(0, 0, 20, 0);
			l1.addView(l2);			
			
			cb[i] = new CheckBox(this);
			cb[i].setText(arrcomment1[i].trim());
			cb[i].setTextColor(Color.GRAY);
			cb[i].setSingleLine(false);
			
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
			cb[i].setMaxWidth(padding);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,12);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);

			map1.put(conchkbox, cb[i]);
			l2.addView(cb[i],paramschk);

			cb[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					if(comments.getText().length()>=474)
					{
						cf.ShowToast("Comment text exceeds the limit.", 1);
						map1.get(getidofselbtn).setChecked(false);
					}
					else
					{
					temp_st = map1.get(getidofselbtn);
					setcomments(temp_st);
					}
				}
			});
		}

	}

	private void setcomments(final CheckBox tempSt) {
		// TODO Auto-generated method stub
		comm2 = "";
		String comm1 = tempSt.getText().toString();
		if (tempSt.isChecked() == true) {
			comm2 += comm1 + " ";
			int commenttextlength = comments.getText().length();
			comments.setText(comments.getText().toString() + " " + comm2);

			if (commenttextlength >= 474) {
				alertDialog = new AlertDialog.Builder(SWR.this).create();
				alertDialog.setTitle("Exceeds Limit");
				alertDialog.setMessage("Comment text exceeds the limit");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								tempSt.setChecked(false);
								tempSt.setEnabled(true);
							}
						});
				alertDialog.show();
			} else {
				tempSt.setEnabled(false);
			}
		} else {
		}
	}

	private void getInspectortypeid() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.mbtblInspectionList,
				null);
		cur.moveToFirst();
		if (cur != null) {
			inspectortypeid = cur.getString(cur
					.getColumnIndex("InspectionTypeId"));
		}
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.rdio1:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				rdiochk = "1";
				commentsfill = "This home was verified as meeting the requirements of selection A, �SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				comments.setText(commentsfill);
				AlertDialog.Builder builder = new AlertDialog.Builder(SWR.this);
				builder.setTitle("Confirm")
						.setMessage(
								"Please confirm this home has a SWR protection that compiles with the 1802 requirements.proper verification and documentation must be provided.")
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

									}

								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

									}

								});
				builder.show();
				rdioA.setChecked(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.rdio2:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of selection B, �No SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				comments.setText(commentsfill);
				rdiochk = "2";
				rdioA.setChecked(false);
				rdioB.setChecked(true);
				rdioC.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.rdio3:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				commentsfill = "This home was verified as meeting the requirements of selection C, �unable to verify/unknown� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				comments.setText(commentsfill);
				rdiochk = "3";
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(true);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			}
		}
	};

	public void clicker(View v) {
		switch (v.getId()) {
		
	
		
		case R.id.txthelpcontentoptionA:
			  cf.alerttitle="A - SWR";
			  cf.alertcontent="SWR (also called sealed roof deck) self-adhering, polymer-modified bitumen roofing underlayment applied directly to the sheathing or foam adhesive SWR barrier (not foamed-on insulation), applied as a supplemental means to protect the dwelling from water intrusion in the event of roof covering loss";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionB:
			  cf.alerttitle="B � No SWR";
			  cf.alertcontent="No SWR";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
			
		case R.id.txthelpcontentoptionC:
			  cf.alerttitle="C - Unknown";
			  cf.alertcontent="Unknown or undetermined";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.savenext:

			comm = comments.getText().toString();

			if (!rdioA.isChecked() && (!rdioB.isChecked())
					&& (!rdioC.isChecked())) {
				cf.ShowToast("Please select the SWR.",1);

			} else if (comm.trim().equals("")) {
				cf.ShowToast("Please enter the Comments for SWR.",1);
			} else {
				try {
					Cursor c2 = cf.db.rawQuery("SELECT * FROM "
							+ cf.tbl_questionsdata + " WHERE SRID='"
							+ cf.Homeid + "'", null);
					int rws = c2.getCount();
					if (rws == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.tbl_questionsdata
								+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
								+ "VALUES ('" + cf.Homeid + "','','','" + 0
								+ "','" + 0 + "','" + 0 + "','','','','','" + 0
								+ "','" + 0 + "','','" + 0 + "','" + 0 + "','"
								+ 0 + "','" + 0 + "','" + 0 + "','','" + 0
								+ "','" + 0 + "','" + 0 + "','','" + rdiochk
								+ "','','" + 0 + "','" + 0
								+ "','','','','','','','','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','','" + 0 + "','"
								+ 0 + "','','" + 0 + "','','','" + cd + "','"
								+ 0 + "')");
					} else {
						cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
								+ " SET SWRValue='" + rdiochk + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");
					}

					Cursor c5 = cf.db.rawQuery("SELECT * FROM "
							+ cf.tbl_comments + " WHERE SRID='" + cf.Homeid
							+ "'", null);
					int rws5 = c5.getCount();
					if (rws5 == 0) {

						cf.db.execSQL("INSERT INTO "
								+ cf.tbl_comments
								+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment,RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
								+ " VALUES ('"
								+ cf.Homeid
								+ "','"
								+ cf.InspectionType
								+ "','','','','','','','','','','','','','','','"
								+ cf.convertsinglequotes(comments.getText()
										.toString()) + "','','','','','" + cd
								+ "')");

					} else {
						cf.db.execSQL("UPDATE "
								+ cf.tbl_comments
								+ " SET SecondaryWaterComment='"
								+ cf.convertsinglequotes(comments.getText()
										.toString())
								+ "',SecondaryWaterAdminComment='',CreatedOn ='"
								+ md + "'" + " WHERE SRID ='"
								+ cf.Homeid.toString() + "'");
					}
					cf.getInspectorId();
					Cursor c3 = cf.db.rawQuery("SELECT * FROM "
							+ cf.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
							+ "'", null);
					int subchkrws = c3.getCount();
					if (subchkrws == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.SubmitCheckTable
								+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
								+ "VALUES ('" + cf.InspectorId + "','"
								+ cf.Homeid
								+ "',0,0,0,0,0,0,1,0,0,0,0,0,0,0,0)");
					} else {
						cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
								+ " SET fld_swr='1' WHERE Srid ='" + cf.Homeid
								+ "' and InspectorId='" + cf.InspectorId + "'");
					}

					updatecnt = "1";
					cf.changeimage();
					// swrtick.setVisibility(visibility);
				} catch (Exception e) {

					updatecnt = "0";
					cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
					//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SWR.this +" problem in inserting or updating swr table on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					
				}

				if (updatecnt == "1") {
					cf.ShowToast("SWR details has been saved successfully.",1);
					iInspectionList = new Intent(SWR.this, OpenProtect.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList.putExtra("iden", "test");
					iInspectionList.putExtra("InspectionType",
							cf.InspectionType);
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.putExtra("keyName", cf.value);
					iInspectionList.putExtra("Count", cf.Count);
					startActivity(iInspectionList);

				}

			}
			break;
		case R.id.previous:

			iInspectionList = new Intent(SWR.this, RoofGeometry.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("iden", "prev");
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);
			startActivity(iInspectionList);
			break;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(SWR.this, RoofGeometry.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("iden", "prev");
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	   
	    if(focus==1)
	    {
	    	focus=0;
	    comments.setText(comments.getText().toString());    
	    }
	 }    
}
