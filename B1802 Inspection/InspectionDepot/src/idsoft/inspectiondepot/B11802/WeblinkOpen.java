package idsoft.inspectiondepot.B11802;


import android.app.Activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WeblinkOpen extends Activity {
	private static final String TAG = null;
	WebView webview1;
	int value, Count;
	Intent intimg;
	String InspectionType, status, homeId, identity, weburl, Page;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			weburl = bunhomeId.getString("weburl");
			homeId = bunhomeId.getString("homeid");
			identity = bunhomeId.getString("iden");
			InspectionType = bunhomeId.getString("InspectionType");
			status = bunhomeId.getString("status");
			value = bunhomeId.getInt("keyName");
			Count = bunhomeId.getInt("Count");
			Page = bunhomeId.getString("page");

		}

		setContentView(R.layout.weblink);

		webview1 = (WebView) findViewById(R.id.webview);
		webview1.getSettings().setJavaScriptEnabled(true);
		webview1.loadUrl(weburl);

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {;
System.out.println("Page= "+Page);
			if (Page.equals("roof")) {
				intimg = new Intent(WeblinkOpen.this, RoofCover.class);
			} else if (Page.equals("open")) {
				intimg = new Intent(WeblinkOpen.this, OpenProtect.class);
			}

			intimg.putExtra("homeid", homeId);
			intimg.putExtra("iden", "test");
			intimg.putExtra("InspectionType", InspectionType);
			intimg.putExtra("status", status);
			intimg.putExtra("keyName", value);
			intimg.putExtra("Count", Count);
			startActivity(intimg);System.out.println("Pageends= ");
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
