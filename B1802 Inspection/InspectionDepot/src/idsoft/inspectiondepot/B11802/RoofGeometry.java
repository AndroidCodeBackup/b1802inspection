package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.OverallComments.OC_textwatcher;
import idsoft.inspectiondepot.B11802.OverallComments.Touch_Listener;

import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class RoofGeometry extends Activity {
	ListView list;
	ImageView Vimage;
	RelativeLayout tblcommentschange;
	public int focus=0;
	String inspectortypeid, othertext = "", helpcontent;
	private static final int visibility = 0;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	String commentsfill, roofgeovalueprev, roofgeolengthprev,
			roofgeototalareaprev, roofgeoothertextprev;
	RadioButton rdioA, rdioB, rdioC;
	int viewimage = 1;
	String InspectionType, status, comm, homeId, rdiochk = "0", yearbuilt = "",
			permitdate = "", updatecnt, identity, roofgeometrycomment,
			commdescrip = "";
	Intent iInspectionList;
	AlertDialog alertDialog;
	TextView prevmitidata, helptxt;
	TextView nocommentsdisp, txroofgeometryheading,roofgeometry_TV_type1;
	EditText txtother, comments, yrbuilt1, yrbuilt2, permitdate1, permitdate2,
			txtothersquarefeet, txtotherlength;
	int commentsch, optionid;
	CheckBox[] cb;
	TextWatcher watcher;
	Button saveclose, prev;
	int value, Count;
	CheckBox temp_st;
	View v1;
	String conchkbox, descrip, comm2;
	String[] arrcomment1;
	TableLayout tbllayout8, exceedslimit1;
	LinearLayout lincomments,roofgeometry_parrant,roofgeometry_type1;
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();
	CommonFunctions cf;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.Homeid = homeId = bunhomeId.getString("homeid");
			cf.Identity = identity = bunhomeId.getString("iden");
			cf.InspectionType = InspectionType = bunhomeId
					.getString("InspectionType");
			cf.status = status = bunhomeId.getString("status");
			cf.value = value = bunhomeId.getInt("keyName");
			cf.Count = Count = bunhomeId.getInt("Count");
		}
		setContentView(R.layout.roofgeometry);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.changeimage();
		cf.getDeviceDimensions();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 25,
				cf, 1));
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		focus=1;
		txroofgeometryheading = (TextView) findViewById(R.id.txtroofgeometryheading);
		txroofgeometryheading
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>What is the roof shape? (Do not consider roofs of porches or carports that are attached only to the fascia or wall of the host structure over unenclosed space in the determination of roof perimeter or roof area for roof geometry classification). "));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==null)
				 {
					cf.alertcontent = "To see help comments, please select roof geometry options.";
				}
				else if (rdiochk == "1" || rdioA.isChecked() == true) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection A, Hip Roof of the OIR B1 -1802 Question 5 Roof Shape";
				} else if (rdiochk == "2" || rdioB.isChecked() == true) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection B, Flat Roof of the OIR B1 -1802 Question 5 Roof Shape.";
				} else if (rdiochk == "3" || rdioC.isChecked() == true) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection C, Non Hip, of the OIR B1 -1802 Question 5 Roof Shape.";
				} else {
					cf.alertcontent = "To see help comments, please select roof geometry options.";
				}

				
				cf.showhelp("HELP",cf.alertcontent);
			}
		});

		
		try {

			Cursor c2 = cf.db.rawQuery("SELECT * FROM "
					+ cf.quetsionsoriginaldata + " WHERE homeid='" + cf.Homeid
					+ "'", null);
			if(c2.getCount()>0){
				c2.moveToFirst();
				String bcoriddata = cf.getsinglequotes(c2.getString(c2.getColumnIndex("rgoriginal")));
				prevmitidata.setText(Html
					.fromHtml("<font color=blue>Original Value : " + "</font>"
							+ "<font color=red>" + bcoriddata + "</font>"));
			}
			else
			{
				prevmitidata.setText(Html
						.fromHtml("<font color=blue>Original Value : "
								+ "</font>" + "<font color=red>Not Available</font>"));
			}

		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" "+" in getting RoofGeometry questions original value on "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			
		}

		

		Vimage = (ImageView) findViewById(R.id.Vimage);
		tblcommentschange = (RelativeLayout) findViewById(R.id.tbllayoutrgcomments);
		this.nocommentsdisp = (TextView) this.findViewById(R.id.notxtcomments);

		this.rdioA = (RadioButton) this.findViewById(R.id.rdobtn1);
		this.rdioA.setOnClickListener(OnClickListener);
		this.rdioB = (RadioButton) this.findViewById(R.id.rdobtn2);
		this.rdioB.setOnClickListener(OnClickListener);
		this.rdioC = (RadioButton) this.findViewById(R.id.rdobtn3);
		this.rdioC.setOnClickListener(OnClickListener);
		this.tbllayout8 = (TableLayout) this.findViewById(R.id.tableLayoutComm);
		exceedslimit1 = (TableLayout) this.findViewById(R.id.exceedslimit);
		this.lincomments = (LinearLayout) this
				.findViewById(R.id.linearlayoutcomm);
		this.yrbuilt1 = (EditText) this.findViewById(R.id.txtyrbuilt1);
		this.yrbuilt2 = (EditText) this.findViewById(R.id.txtpermitDate1);
		this.permitdate1 = (EditText) this.findViewById(R.id.txtyrbuilt2);
		this.permitdate2 = (EditText) this.findViewById(R.id.txtpermitDate2);
		this.txtother = (EditText) this.findViewById(R.id.txtOther);
		this.txtotherlength = (EditText) this.findViewById(R.id.txtotherLen);
		this.txtothersquarefeet = (EditText) this
				.findViewById(R.id.txtotherSqfeet);

		TextView rccode = (TextView) this.findViewById(R.id.rccode);
	//	rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		
		roofgeometry_parrant = (LinearLayout)this.findViewById(R.id.roofgeometry_parrent);
		roofgeometry_type1=(LinearLayout) findViewById(R.id.roofgeometry_type);
		roofgeometry_TV_type1 = (TextView) findViewById(R.id.roofgeometry_txt);
		
		this.comments = (EditText) this
				.findViewById(R.id.txtroofgeometrycomments);
		comments.setOnTouchListener(new Touch_Listener());
		comments.addTextChangedListener(new QUES_textwatcher());
		
		
		prev = (Button) this.findViewById(R.id.previous);

		try {
			Cursor c2 = cf.db.rawQuery(
					"SELECT RoofGeoValue,RoofGeoLength,RoofGeoTotalArea FROM "
							+ cf.tbl_questionsdata + " WHERE SRID='"
							+ cf.Homeid + "'", null);
			int chkrws = c2.getCount();
			if (chkrws == 0) {
				identity = "test";
			} else {
				identity = "prev";
			}

		} catch (Exception e) {
		
		}

		if (identity.equals("prev")) {
			try {
				Cursor cur = cf.db.rawQuery("select * from "
						+ cf.tbl_questionsdata + " where SRID='" + cf.Homeid
						+ "'", null);
				if(cur.getCount()>0){
				cur.moveToFirst();
				if (cur != null) {
					roofgeovalueprev = cur.getString(cur
							.getColumnIndex("RoofGeoValue"));
					roofgeolengthprev = cur.getString(cur
							.getColumnIndex("RoofGeoLength"));
					roofgeototalareaprev = cur.getString(cur
							.getColumnIndex("RoofGeoTotalArea"));
					roofgeoothertextprev = cf.getsinglequotes(cur.getString(cur
							.getColumnIndex("RoofGeoOtherText")));

					if (roofgeovalueprev.equals("1")) {
						rdioA.setChecked(true);
						yrbuilt1.setEnabled(true);
						yrbuilt2.setEnabled(true);
						permitdate1.setEnabled(false);
						permitdate2.setEnabled(false);
						txtother.setEnabled(false);
						txtotherlength.setEnabled(false);
						txtothersquarefeet.setEnabled(false);

						yrbuilt1.setText(roofgeolengthprev);
						yrbuilt2.setText(roofgeototalareaprev);
						rdiochk = "1";
					} else if (roofgeovalueprev.equals("2")) {
						rdioB.setChecked(true);
						yrbuilt1.setEnabled(false);
						yrbuilt2.setEnabled(false);
						permitdate1.setEnabled(true);
						permitdate2.setEnabled(true);
						txtother.setEnabled(false);
						txtotherlength.setEnabled(false);
						txtothersquarefeet.setEnabled(false);
						permitdate1.setText(roofgeolengthprev);
						permitdate2.setText(roofgeototalareaprev);
						rdiochk = "2";
					} else if (roofgeovalueprev.equals("3")) {
						rdioC.setChecked(true);
						yrbuilt1.setEnabled(false);
						yrbuilt2.setEnabled(false);
						permitdate1.setEnabled(false);
						permitdate2.setEnabled(false);
						txtother.setEnabled(true);
						txtotherlength.setEnabled(true);
						txtothersquarefeet.setEnabled(true);
						txtother.setText(roofgeoothertextprev);
						txtotherlength.setText(roofgeolengthprev);
						txtothersquarefeet.setText(roofgeototalareaprev);
						rdiochk = "3";
					} else {
					}

				}
				}
			} catch (Exception e) {
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in retrieving QUE data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}

			try {
				Cursor cur = cf.db.rawQuery("select * from " + cf.tbl_comments
						+ " where SRID='" + cf.Homeid + "'", null);
				if(cur.getCount()>0){
				cur.moveToFirst();
					if (cur != null) {
						roofgeometrycomment = cf.getsinglequotes(cur.getString(cur
								.getColumnIndex("RoofGeometryComment")));
						cf.showing_limit(roofgeometrycomment,roofgeometry_parrant,roofgeometry_type1,roofgeometry_TV_type1,"474");	
						comments.setText(roofgeometrycomment);
					}
				}
			} catch (Exception e) {
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in retrieving roofgeometry comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}
		}
		this.Vimage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}
		});

		this.tblcommentschange.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				arrowcommentschange();
			}
		});

	}
	class Touch_Listener implements OnTouchListener
	{
		   Touch_Listener()
			{
				
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	
		    		cf.setFocus(comments);
				
				return false;
		    }
	}
	class QUES_textwatcher implements TextWatcher
	{
		QUES_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
				
				cf.showing_limit(s.toString(),roofgeometry_parrant,roofgeometry_type1,roofgeometry_TV_type1,"474"); 
			
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	protected void arrowcommentschange() {
		// TODO Auto-generated method stub
		//Vimage.setBackgroundResource(R.drawable.arrowup);
		if (viewimage == 1) {
			Vimage.setBackgroundResource(R.drawable.arrowup);
			if (rdioA.isChecked()) {
				optionid = 1;
			} else if (rdioB.isChecked()) {
				optionid = 2;
			} else if (rdioC.isChecked()) {
				optionid = 3;
			} else {
				cf.ShowToast("Please select Roof Geometry options to view Comments.",1);
			}
			try {
				descrip = "";
				arrcomment1 = null;
				Cursor c2 = cf.db.rawQuery("SELECT * FROM "
						+ cf.tbl_admcomments
						+ " WHERE questionid='5' and optionid='" + optionid
						+ "' and status='Active' and InspectorId='"
						+ cf.InspectionType + "'", null);
				int rws = c2.getCount();System.out.println("rws= "+rws);
				arrcomment1 = new String[rws];
				c2.moveToFirst();
				if (rws == 0) {
					
					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.VISIBLE);
					lincomments.setVisibility(v1.GONE);					
					Vimage.setBackgroundResource(R.drawable.arrowdown);
				} else {

					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.VISIBLE);
					if (c2 != null) {
						int i=0;
						do {
							arrcomment1[i]=cf.getsinglequotes(c2.getString(c2.getColumnIndex("description")));
							if(arrcomment1[i].contains("null"))
							{
								arrcomment1[i] = arrcomment1[i].replace("null", "");
							}							
							i++;
						} while (c2.moveToNext());						
					}
					c2.close();

					addcomments();
					viewimage = 0;
				}

			} catch (Exception e) {
				/*
				 * ShowToast toast = new ShowToast(getBaseContext(),
				 * "Please add atleast one roof geometry comment in dashboard");
				 */
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in retrieving roofgeometry comments in arrow click on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			}

		} else if (viewimage == 0) {
			Vimage.setBackgroundResource(R.drawable.arrowdown);
			tbllayout8.setVisibility(v1.GONE);
			viewimage = 1;
		}

	}

	private void addcomments() {
		// TODO Auto-generated method stub
		lincomments.removeAllViews();
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		lincomments.addView(l1);

		cb = new CheckBox[arrcomment1.length];

		for (int i = 0; i < arrcomment1.length; i++) {

			LinearLayout l2 = new LinearLayout(this);			
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.setMargins(0, 0, 20, 0);
			l1.addView(l2);			
			
			cb[i] = new CheckBox(this);
			cb[i].setText(arrcomment1[i].trim());
			cb[i].setTextColor(Color.GRAY);
			cb[i].setSingleLine(false);
			
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
			cb[i].setMaxWidth(padding);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,12);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);

			map1.put(conchkbox, cb[i]);
			l2.addView(cb[i],paramschk);

			cb[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					if(comments.getText().length()>=474)
					{
						cf.ShowToast("Comment text exceeds the limit.", 1);
						map1.get(getidofselbtn).setChecked(false);
					}
					else
					{
					temp_st = map1.get(getidofselbtn);
					setcomments(temp_st);
					}
				}
			});
		}

	}

	private void setcomments(final CheckBox tempSt) {
		// TODO Auto-generated method stub
		comm2 = "";
		String comm1 = tempSt.getText().toString();
		if (tempSt.isChecked() == true) {
			comm2 += comm1 + " ";
			int commenttextlength = comments.getText().length();
			comments.setText(comments.getText().toString() + " " + comm2);

			if (commenttextlength >= 474) {
				alertDialog = new AlertDialog.Builder(RoofGeometry.this)
						.create();
				alertDialog.setTitle("Exceeds Limit");
				alertDialog.setMessage("Comment text exceeds the limit");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								tempSt.setChecked(false);
								tempSt.setEnabled(true);
							}
						});
				alertDialog.show();
			} else {
				tempSt.setEnabled(false);
			}
		} else {
		}
	}

	private void getInspectortypeid() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.mbtblInspectionList,
				null);
		cur.moveToFirst();
		if (cur != null) {
			inspectortypeid = cur.getString(cur
					.getColumnIndex("InspectionTypeId"));
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.txthelpcontentoptionA:
			cf.alerttitle="A - Hip Roof";
		    cf.alertcontent="Hip roof: hip roof with no other roof shapes greater than 10 percent of the total roof system perimeter Total length of non-hip features: ____________ feet; total roof system perimeter: ____________ feet";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionB:
			cf.alerttitle="B - Flat Roof";
		    cf.alertcontent="Flat roof: roof on a building with five or more units where at least 90 percent of the main roof area has a roof slope of less than 2:12. Roof area with slope less than 2:12 ____________ sq. ft.; total roof area ____________ sq. ft.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionC:
			cf.alerttitle="C - Other Roof Type";
		    cf.alertcontent="Other roof: any roof that does not qualify as either A or B above";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		
		
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.previous:
			iInspectionList = new Intent(RoofGeometry.this, RoofWall.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("iden", "prev");
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);
			startActivity(iInspectionList);
			break;

		case R.id.savenext:
			int option = 0;
			//comm = comments.getText().toString();
			boolean stat = true;

			if (rdiochk == "1" || rdioA.isChecked() == true) {
				yearbuilt = yrbuilt1.getText().toString();
				permitdate = yrbuilt2.getText().toString();
				option = 1;

			} else if (rdiochk == "2" || rdioB.isChecked() == true) {
				yearbuilt = permitdate1.getText().toString();
				permitdate = permitdate2.getText().toString();
				option = 2;
			} else if (rdiochk == "3" || rdioC.isChecked() == true) {

				yearbuilt = txtotherlength.getText().toString();
				permitdate = txtothersquarefeet.getText().toString();
				option = 3;
				if ("".equals(txtother.getText().toString())) {
					cf.ShowToast("Please enter the text for Other Roof Geometry.",1);
					txtother.requestFocus();
					stat = false;
				} else {
					othertext = txtother.getText().toString();
				}

			} else {
				cf.ShowToast("Please select Roof Geometry type.",1);
			}

			if (yearbuilt.trim().equals("")) {
				cf.ShowToast("Please enter the measurements for Roof Geometry.",1);
				if (rdiochk == "1") {
					yrbuilt1.requestFocus();
				} else if (rdiochk == "2") {
					permitdate1.requestFocus();
				} else {
					txtotherlength.requestFocus();
				}

			}

			else if (validation(yearbuilt, permitdate, option) != "true") {
				if (rdiochk == "1") {
					cf.ShowToast("This is not a hip roof - non hip measurements are greater than allowed.",1);
					yrbuilt2.requestFocus();
					yrbuilt1.requestFocus();
					yrbuilt2.setText("");
					yrbuilt1.setText("");
				} else if (rdiochk == "2") {
					cf.ShowToast("This is not a flat roof - non flat measurements are greater than allowed.",1);
					permitdate2.requestFocus();
					permitdate1.requestFocus();
					permitdate2.setText("");
					permitdate2.setText("");
				} else {

				}

			} else if (comments.getText().toString().trim().equals("")) {
				cf.ShowToast("Please enter the Comments for Roof Geometry.",1);
				comments.requestFocus();
			}

			else if (stat) {
				updatecnt = "1";
				try {
					Cursor c2 = cf.db.rawQuery("SELECT * FROM "
							+ cf.tbl_questionsdata + " WHERE SRID='"
							+ cf.Homeid + "'", null);
					int rws = c2.getCount();
					if (rws == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.tbl_questionsdata
								+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
								+ "VALUES ('" + cf.Homeid + "','','','" + 0
								+ "','" + 0 + "','" + 0 + "','','','','','" + 0
								+ "','" + 0 + "','','" + 0 + "','" + 0 + "','"
								+ 0 + "','" + 0 + "','" + 0 + "','','"
								+ rdiochk + "','" + yearbuilt + "','"
								+ permitdate + "','"
								+ cf.convertsinglequotes(othertext) + "','" + 0
								+ "','','" + 0 + "','" + 0
								+ "','','','','','','','','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','','" + 0 + "','"
								+ 0 + "','','" + 0 + "','','','" + cd + "','"
								+ 0 + "')");

					} else {
						cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
								+ " SET RoofGeoValue='" + rdiochk
								+ "',RoofGeoLength ='" + yearbuilt
								+ "',RoofGeoTotalArea='" + permitdate
								+ "',RoofGeoOtherText='"
								+ cf.convertsinglequotes(othertext) + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");
					}
				} catch (Exception e) {

					updatecnt = "0";
					cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
					//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in saving roofgeometry values because of invalid input on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

				}

				Cursor c2 = cf.db.rawQuery("SELECT * FROM " + cf.tbl_comments
						+ " WHERE SRID='" + cf.Homeid + "'", null);
				int rws = c2.getCount();
				try {
					if (rws == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.tbl_comments
								+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment," +
								"RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
								+ " VALUES ('" + cf.Homeid + "','"
								+ cf.InspectionType + "','','','','','','','"								 
								+ "','','"+cf.convertsinglequotes(comments.getText().toString())+"','','','','','','','','','','','"
								+ cd + "')");
						

					} else {
						cf.db.execSQL("UPDATE " + cf.tbl_comments
								+ " SET RoofGeometryComment='"
								+ cf.convertsinglequotes(comments.getText().toString())
								+ "',RoofGeometryAdminComment='',CreatedOn ='"
								+ md + "'" + " WHERE SRID ='"
								+ cf.Homeid.toString() + "'");
						
					}
				} catch (Exception e) {

					updatecnt = "0";
					cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
					//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in saving roofgeometry comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

				}

				cf.getInspectorId();
				try {
					Cursor c3 = cf.db.rawQuery("SELECT * FROM "
							+ cf.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
							+ "'", null);
					int subchkrws = c3.getCount();
					if (subchkrws == 0) {
						cf.db.execSQL("INSERT INTO "
								+ cf.SubmitCheckTable
								+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
								+ "VALUES ('" + cf.InspectorId + "','"
								+ cf.Homeid
								+ "',0,0,0,0,0,1,0,0,0,0,0,0,0,0,0)");
					} else {
						cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
								+ " SET fld_roofgeo='1' WHERE Srid ='"
								+ cf.Homeid + "' and InspectorId='"
								+ cf.InspectorId + "'");
					}
				} catch (Exception e) {
					//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in inserting or roofgeometry values on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

				}

				if (updatecnt == "1") {
					cf.changeimage();
					// roofgeotick.setVisibility(visibility);
					cf.ShowToast("Roof Geometry details has been saved successfully.",1);
					iInspectionList = new Intent(RoofGeometry.this, SWR.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList.putExtra("iden", "test");
					iInspectionList.putExtra("InspectionType",
							cf.InspectionType);
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.putExtra("keyName", cf.value);
					iInspectionList.putExtra("Count", cf.Count);
					startActivity(iInspectionList);
				}

			}
			break;
		}
	}

	private String validation(String yearbuilt2, String permitdate3, int option1) {
		// TODO Auto-generated method stub
		String chk = "false";
		try {
			if (!permitdate3.equals("")) {
				if (option1 == 1) {
					int perimeter = Integer.parseInt(permitdate3);
					int yrbt = Integer.parseInt(yearbuilt2);
					perimeter = perimeter / 10;
					if (yrbt <= perimeter) {
						chk = "true";

					} else {
						chk = "false";

					}
				} else if (option1 == 2) {
					float perimeter = Float.parseFloat(permitdate3);
					float yrbt = Float.parseFloat(yearbuilt2);
					Double temp = ((89.00 / 100.00) * perimeter);

					if (temp >= yrbt) {
						chk = "true";
					} else {
						chk = "false";
					}
				} else {
					chk = "true";
				}

			} else {
				chk = "true";
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in datatype conversion types on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			chk = "false";
		}
		return chk;
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.rdobtn1:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				yrbuilt1.setEnabled(true);
				yrbuilt2.setEnabled(true);
				permitdate1.setEnabled(false);
				permitdate2.setEnabled(false);
				txtother.setEnabled(false);
				txtotherlength.setEnabled(false);
				txtothersquarefeet.setEnabled(false);
				commentsfill = "This home was verified as meeting the requirements of selection A, �Hip Roof� of the OIR B1 -1802 Question 5 Roof Shape.";
				comments.setText(commentsfill);
				rdiochk = "1";
				permitdate1.setText(null);
				yrbuilt1.requestFocus();
				permitdate2.setText(null);
				txtothersquarefeet.setText(null);
				txtotherlength.setText(null);
				txtother.setText(null);
				rdioA.setChecked(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.rdobtn2:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				permitdate1.setEnabled(true);
				permitdate2.setEnabled(true);
				yrbuilt1.setEnabled(false);
				yrbuilt2.setEnabled(false);
				txtother.setEnabled(false);
				txtotherlength.setEnabled(false);
				txtothersquarefeet.setEnabled(false);
				commentsfill = "This home was verified as meeting the requirements of selection B, �Flat Roof� of the OIR B1 -1802 Question 5 Roof Shape.";
				comments.setText(commentsfill);
				rdiochk = "2";
				yrbuilt2.setText(null);
				permitdate1.requestFocus();
				yrbuilt1.setText(null);
				txtothersquarefeet.setText(null);
				txtotherlength.setText(null);
				txtother.setText(null);
				rdioA.setChecked(false);
				rdioB.setChecked(true);
				rdioC.setChecked(false);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			case R.id.rdobtn3:
				Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				txtother.setEnabled(true);
				txtotherlength.setEnabled(true);
				txtothersquarefeet.setEnabled(true);
				permitdate1.setEnabled(false);
				permitdate2.setEnabled(false);
				yrbuilt1.setEnabled(false);
				yrbuilt2.setEnabled(false);
				commentsfill = "This home was verified as meeting the requirements of selection C, Non Hip, of the OIR B1 -1802 Question 5 Roof Shape.";
				comments.setText(commentsfill);
				permitdate1.setText(null);
				txtotherlength.requestFocus();
				permitdate2.setText(null);
				yrbuilt2.setText(null);
				yrbuilt1.setText(null);
				rdiochk = "3";
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(true);
				exceedslimit1.setVisibility(v1.GONE);
				lincomments.setVisibility(v1.GONE);
				viewimage = 1;
				break;

			}
		}
	};

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(RoofGeometry.this, RoofWall.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("iden", "prev");
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	   
	    if(focus==1)
	    {
	    	focus=0;
	    comments.setText(comments.getText().toString());    
	    }
	 }    
}
