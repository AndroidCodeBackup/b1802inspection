package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.OverallComments.OC_textwatcher;
import idsoft.inspectiondepot.B11802.OverallComments.Touch_Listener;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

public class BuildCode extends Activity {
	String appdateprev, InspectionType, status,inspectortypeid, helpcontent,commdescrip = "",commentsfill,compchk = null, 
			yearofhome, country, conchkbox, comm2,newyearbuilt, descrip,homeId, yearbuilt,
			rdiochk, permitDate, comm, updatecnt, identity, buildingcomment;	
	int spinnerPosition, yearbuiltprev, buildcodevalueprev,value, Count, chkrws,mYear, mMonth, mDay, dummy = 1, optionid,viewimage = 1;
	Button saveclose, getdate1, getdate2;
	View v1;
	public int focus=0;
	EditText permitDate1, permitDate2, comments;
	Spinner yearbuilt1, yearbuilt2;
	TextView nocommentsdisp, txtheading,prevmitidata,helptxt,buildcode_TV_type1;
	RadioButton rdobutton1, rdobutton2, rdounknown;
	ImageView Vimage;	
	
	RelativeLayout tblcommentschange;
	LinearLayout lincomments,buildcode_parrant,buildcode_type1;
	CheckBox[] cb;
	CheckBox temp_st;	
	String[] arrcomment1;
	Intent iInspectionList;
	Calendar cal = Calendar.getInstance();
	int current_year = cal.get(Calendar.YEAR), commentsch;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	AlertDialog alertDialog;
	protected static final int DATE_DIALOG_ID = 0;
	private static final int visibility = 0;
	private TableLayout tbllayout8, exceedslimit1;
	ListView list;
	String yeara[] = { "Select", "2001", "2002", "2003", "2004", "2005",
			"2006", "2007", "2008", "2009", "2010", "2011", "2012","2013" };
	
	String yearb[] = { "Select", "1994", "1995", "1996", "1997", "1998", "1999", "2000",
			"2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008",
			"2009", "2010", "2011", "2012","2013" };
	
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();	
	public CommonFunctions cf;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(2);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		try {
			Bundle bunhomeId = getIntent().getExtras();
			if (bunhomeId != null) {
				cf.Homeid = homeId = bunhomeId.getString("homeid");
				cf.Identity = identity = bunhomeId.getString("iden");
				cf.InspectionType = InspectionType = bunhomeId
						.getString("InspectionType");
				cf.status = status = bunhomeId.getString("status");
				cf.value = value = bunhomeId.getInt("keyName");
				cf.Count = Count = bunhomeId.getInt("Count");
			}
			setContentView(R.layout.buildcode);
			cf.getInspectorId();
			cf.getinspectiondate();
			cf.changeimage();
			cf.getDeviceDimensions(); focus=1;
			/** menu **/
			LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);/**  menu **/
			layout.setMinimumWidth(cf.wd);		
			layout.addView(new MyOnclickListener(getApplicationContext(), 2,cf, 0));
			
			LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);/** Questions submenu **/
			sublayout.addView(new MyOnclickListener(getApplicationContext(),
					21, cf, 1));
			
			ScrollView scr=(ScrollView)findViewById(R.id.scr);
		    scr.setSmoothScrollingEnabled(true);
		    scr.smoothScrollTo(0,0);
			  HorizontalScrollView hscr=(HorizontalScrollView)findViewById(R.id.HorizontalScrollView01);
			   hscr.smoothScrollTo(0,0);
			  

			df = new android.text.format.DateFormat();
			cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
			md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
			
			helptxt = (TextView) findViewById(R.id.help);
			helptxt.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					if (rdiochk==null)
					{	cf.alertcontent = "To see help comments, please select Building Code options.";}
					else if (rdiochk.equals("1") || rdobutton1.isChecked()) {
						cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.";
					} else if (rdiochk.equals("2") || rdobutton2.isChecked()) {
						cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
					} else if (rdiochk.equals("3") || rdounknown.isChecked()) {
						cf.alertcontent = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
					} else {
						cf.alertcontent = "To see help comments, please select Building Code options.";
					}
					cf.showhelp("HELP",cf.alertcontent);
				}
			});
			txtheading = (TextView) findViewById(R.id.txtquestion1heading);
			txtheading
					.setText(Html
							.fromHtml("<font color=red> * "
									+ "</font>Was the structure built in compliance with the Florida Building Code (FBC 2001 or later) OR for homes located in the HVHZ (Miami-Dade or Broward counties), South Florida Building Code (SFBC-94)? "));
			Vimage = (ImageView) findViewById(R.id.Vimage);
			tblcommentschange = (RelativeLayout) findViewById(R.id.tbllayoutbccomments);
			this.tbllayout8 = (TableLayout) this.findViewById(R.id.tableLayoutComm);
			prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		
			this.lincomments = (LinearLayout) this.findViewById(R.id.linearlayoutcomm);			

			this.rdobutton1 = (RadioButton) this.findViewById(R.id.rdobtn1);this.rdobutton1.setOnClickListener(OnClickListener);
			this.rdobutton2 = (RadioButton) this.findViewById(R.id.rdobtn2);this.rdobutton2.setOnClickListener(OnClickListener);
			this.rdounknown = (RadioButton) this.findViewById(R.id.rdounknown);this.rdounknown.setOnClickListener(OnClickListener);
			
			this.yearbuilt1 = (Spinner) this.findViewById(R.id.txtyrbuilt1);

			ArrayAdapter adapter = new ArrayAdapter(BuildCode.this,
					android.R.layout.simple_spinner_item, yeara);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			yearbuilt1.setAdapter(adapter);
			yearbuilt1.setEnabled(false);

			this.yearbuilt2 = (Spinner) this.findViewById(R.id.txtyrbuilt2);
			ArrayAdapter adapter2 = new ArrayAdapter(BuildCode.this,
					android.R.layout.simple_spinner_item, yearb);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			yearbuilt2.setAdapter(adapter2);
			yearbuilt2.setEnabled(false);

			this.permitDate1 = (EditText) this
					.findViewById(R.id.txtpermitDate1);
			this.permitDate2 = (EditText) this
					.findViewById(R.id.txtpermitDate2);
			exceedslimit1 = (TableLayout) this.findViewById(R.id.exceedslimit);
			exceedslimit1.setVisibility(v1.GONE);
			this.nocommentsdisp = (TextView) this
					.findViewById(R.id.notxtcomments);

			getdate1 = (Button) findViewById(R.id.Calcicon1);
			getdate2 = (Button) findViewById(R.id.Calcicon2);
			
			buildcode_parrant = (LinearLayout)this.findViewById(R.id.buildcode_parrent);
			buildcode_type1=(LinearLayout) findViewById(R.id.buildcode_type);
			buildcode_TV_type1 = (TextView) findViewById(R.id.buildcode_txt);
			this.comments = (EditText) this.findViewById(R.id.txtcomments);
			comments.setOnTouchListener(new Touch_Listener());
			comments.addTextChangedListener(new QUES_textwatcher());
			saveclose = (Button) findViewById(R.id.savenext);

			TextView rccode = (TextView) this.findViewById(R.id.rccode);
			cf.setRcvalue(rccode);
			
			final Calendar c = Calendar.getInstance();
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);
			
			try {
				Cursor c2 = cf.SelectTablefunction(cf.quetsionsoriginaldata,
						"where homeid='" + cf.Homeid + "'");
				if(c2.getCount()>0){

				c2.moveToFirst();
				String bcoriddata = cf.getsinglequotes(c2.getString(c2
						.getColumnIndex("bcoriginal")));
				prevmitidata.setText(Html
						.fromHtml("<font color=blue>Original Value : "
								+ "</font>" + "<font color=red>" + bcoriddata
								+ "</font>"));
				}
				else
				{
					prevmitidata.setText(Html
							.fromHtml("<font color=blue>Original Value : "
									+ "</font>" + "<font color=red>Not Available</font>"));

				}
			} catch (Exception e) {

			}
			try {
				Cursor c2 = cf.SelectTablefunction(cf.mbtblInspectionList,
						"where s_SRID='" + cf.Homeid + "'");
				int rws = c2.getCount();

				c2.moveToFirst();
				yearofhome = c2.getString(c2.getColumnIndex("s_YearBuilt"));
				country = cf.getsinglequotes(c2.getString(c2
						.getColumnIndex("s_County")));

				if (country.toLowerCase().equals("miami-dade")
						|| country.toLowerCase().equals("broward")) {
					Cursor c3 = cf.SelectTablefunction(cf.tbl_questionsdata,
							"where SRID='" + cf.Homeid + "'");
					int rows = c3.getCount();

					c3.moveToFirst();
					if (rows == 0) {
						rdiochk = "2";
						rdobutton2.setChecked(true);getdate2.setEnabled(true);
						yearbuilt2.setEnabled(true);
						spinnerPosition = adapter2.getPosition(yearofhome);
						yearbuilt2.setSelection(spinnerPosition);
						commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
						comments.setText(commentsfill);
					} else {
						if (c3 != null) {
							if (c3.getString(
									c3.getColumnIndex("BuildingCodeValue"))
									.equals("0")) {
								rdiochk = "2";
								rdobutton2.setChecked(true);getdate2.setEnabled(true);
								yearbuilt2.setEnabled(true);
								spinnerPosition = adapter2.getPosition(yearofhome);
								yearbuilt2.setSelection(spinnerPosition);
								commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
								comments.setText(commentsfill);
							} else {

							}

						}
					}

				}
				if (!yearofhome.equals("")) {
					int yhme = 0;
					try {
						yhme = Integer.parseInt(yearofhome);
					} catch (Exception e) {
						yhme = 0;
					}
					if (yhme < 1994 && yhme != 0) {
						rdiochk = "3";
						rdobutton2.setChecked(false);
						rdounknown.setChecked(true);
						commentsfill = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
						comments.setText(commentsfill);
					}
				}
			} catch (Exception e) {
				System.out.println("e message " + e.getMessage());
			}
			
			
			this.Vimage.setOnClickListener(new View.OnClickListener() {
				public void onClick(View arg0) {
					arrowcommentschange();
				}
			});

			this.tblcommentschange
					.setOnClickListener(new View.OnClickListener() {
						public void onClick(View arg0) {
							arrowcommentschange();
						}
					});

		} catch (Exception e) {

		}

		try {
			Cursor c2 = cf.db.rawQuery(
					"SELECT BuildingCodeYearBuilt,BuildingCodeValue,BuildingCodePerApplnDate FROM "
							+ cf.tbl_questionsdata + " WHERE SRID='"
							+ cf.Homeid + "'", null);
			chkrws = c2.getCount();
			if (chkrws == 0) {
				identity = "test";
			} else {
				fetchdata();
				identity = "prev";
			}
		} catch (Exception e) {

		}
		if (identity.equals("prev")) {

			fetchdata();

			try {
				Cursor cur = cf.SelectTablefunction(cf.tbl_comments,
						"where SRID='" + cf.Homeid + "'");
				if(cur.getCount()>0){
				cur.moveToFirst();
				if (cur != null) {
					buildingcomment = cf.getsinglequotes(cur.getString(cur
							.getColumnIndex("BuildingCodeComment")));
					cf.showing_limit(buildingcomment,buildcode_parrant,buildcode_type1,buildcode_TV_type1,"474");	
					comments.setText(buildingcomment);
				}
				}
			} catch (Exception e) {

			}
		}
	}
	class Touch_Listener implements OnTouchListener
	{
		   Touch_Listener()
			{
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	
		    		cf.setFocus(comments);
		
				return false;
		    }
	}
	class QUES_textwatcher implements TextWatcher
	{
	     
	     QUES_textwatcher()
		{
			
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
				
				cf.showing_limit(s.toString(),buildcode_parrant,buildcode_type1,buildcode_TV_type1,"474"); 
			
			
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 
	protected void arrowcommentschange() {
		// TODO Auto-generated method stub
		Vimage.setBackgroundResource(R.drawable.arrowup);
		if (viewimage == 1) {			
			Vimage.setBackgroundResource(R.drawable.arrowup);
			lincomments.setVisibility(v1.VISIBLE);
			if (rdobutton1.isChecked()) {
				optionid = 1;
			} else if (rdobutton2.isChecked()) {
				optionid = 2;
			} else if (rdounknown.isChecked()) {
				optionid = 3;
			} else {
				
				cf.ShowToast("Please select option for Building Code to view Comments.", 1);
				
			}
			try {
				descrip = "";
				arrcomment1 = null;
				Cursor c2 = cf.SelectTablefunction(cf.tbl_admcomments,
						"where questionid='1' and optionid='" + optionid
								+ "' and status='Active' and InspectorId='"
								+ cf.InspectionType + "'");
				int rws = c2.getCount();
				
				if (rws == 0) {
					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.VISIBLE);
					lincomments.setVisibility(v1.GONE);
					Vimage.setBackgroundResource(R.drawable.arrowdown);
				} else {

					tbllayout8.setVisibility(v1.VISIBLE);
					nocommentsdisp.setVisibility(v1.GONE);
					arrcomment1 = new String[rws];
					lincomments.setVisibility(v1.VISIBLE);
					c2.moveToFirst();
					if (c2 != null) {
						int i=0;
						do {
							arrcomment1[i]=cf.getsinglequotes(c2.getString(c2.getColumnIndex("description")));
							if(arrcomment1[i].contains("null"))
							{
								arrcomment1[i] = arrcomment1[i].replace("null", "");
							}							
							i++;
						} while (c2.moveToNext());						
					}
					c2.close();

					addcomments();
					viewimage = 0;
				}

			} catch (Exception e) {
				System.out.println("erro message "+e.getMessage());
				cf.ShowToast("Please add atleast one Building Code comment in Dashboard.",1);
				
			}

		} else if (viewimage == 0) {
			
			Vimage.setBackgroundResource(R.drawable.arrowdown);
			tbllayout8.setVisibility(v1.GONE);
			viewimage = 1;
		}
	}

	private void addcomments() {
		// TODO Auto-generated method stub
		lincomments.removeAllViews();
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		lincomments.addView(l1);

		cb = new CheckBox[arrcomment1.length];

		for (int i = 0; i < arrcomment1.length; i++) {

			LinearLayout l2 = new LinearLayout(this);			
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.setMargins(0, 0, 20, 0);
			l1.addView(l2);			
			
			cb[i] = new CheckBox(this);
			cb[i].setText(arrcomment1[i].trim());
			cb[i].setTextColor(Color.GRAY);
			cb[i].setSingleLine(false);
			
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
			cb[i].setMaxWidth(padding);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,12);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);

			map1.put(conchkbox, cb[i]);
			l2.addView(cb[i],paramschk);

			cb[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					if(comments.getText().length()>=474)
					{
						cf.ShowToast("Comment text exceeds the limit.", 1);
						map1.get(getidofselbtn).setChecked(false);
					}
					else
					{
					temp_st = map1.get(getidofselbtn);
					setcomments(temp_st);
					}
				}
			});
		}

	}

	private void setcomments(final CheckBox tempSt) {
		// TODO Auto-generated method stub
		comm2 = "";
		String comm1 = tempSt.getText().toString();
		if (tempSt.isChecked() == true) {
			comm2 += comm1 + " ";
			int commenttextlength = comments.getText().length();
			comments.setText(comments.getText().toString() + " " + comm2);

			if (commenttextlength >= 474) {
				alertDialog = new AlertDialog.Builder(BuildCode.this).create();
				alertDialog.setTitle("Exceeds Limit");
				alertDialog.setMessage("Comment text exceeds the limit");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								tempSt.setChecked(false);
								tempSt.setEnabled(true);
							}
						});
				alertDialog.show();
			} else {
				tempSt.setEnabled(false);
			}
		} else {
		}
	}

	private void fetchdata() {
		// TODO Auto-generated method stub

		try {
			Cursor cur = cf.SelectTablefunction(cf.tbl_questionsdata,
					"where SRID='" + cf.Homeid + "'");
			
			if(cur.getCount()>0)
			{
			cur.moveToFirst();
			if (cur != null) {
				yearbuiltprev = cur.getInt(cur
						.getColumnIndex("BuildingCodeYearBuilt"));
				buildcodevalueprev = cur.getInt(cur
						.getColumnIndex("BuildingCodeValue"));
				appdateprev = cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("BuildingCodePerApplnDate")));
				newyearbuilt = cf.getsinglequotes(cur.getString(cur
						.getColumnIndex("BuildingCodeYearBuilt")));

				if (buildcodevalueprev == 1) {
					rdiochk = "1";
					yearbuilt2.setEnabled(false);
					rdounknown.setChecked(false);
					rdobutton1.setChecked(true);
					yearbuilt1.setEnabled(true);getdate1.setEnabled(true);getdate2.setEnabled(false);
					int sel = 0;
					if (yearbuiltprev >= 2001
							&& yearbuiltprev <= cf.Current_year) {
						for (int k = 1, i = 2001; i <= yearbuiltprev; i++, k++) {

							sel = k;

						}
					}
					yearbuilt1.setSelection(sel);
					yearbuilt = newyearbuilt;
					permitDate = appdateprev;
					permitDate1.setText("" + appdateprev);
				} else if (buildcodevalueprev == 2) {
					rdiochk = "2";
					rdobutton2.setChecked(true);
					yearbuilt2.setEnabled(true);
					rdounknown.setChecked(false);
					yearbuilt1.setEnabled(false);getdate2.setEnabled(true);getdate1.setEnabled(false);
					int sel = 0;
					
					if (yearbuiltprev >= 1994 && yearbuiltprev <= cf.Current_year) {
						for (int k = 1, i = 1994; i <= yearbuiltprev; i++, k++) {

							sel = k;

						}
					}
					yearbuilt2.setSelection(sel);
					permitDate2.setText("" + appdateprev);
					yearbuilt = newyearbuilt;
					permitDate = appdateprev;

				} else if (buildcodevalueprev == 3) {
					rdobutton1.setChecked(false);
					rdobutton2.setChecked(false);
					yearbuilt2.setEnabled(false);
					yearbuilt1.setEnabled(false);getdate1.setEnabled(false);getdate2.setEnabled(false);
					rdiochk = "3";
					rdounknown.setChecked(true);
				}
			}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" in fetching buildcode data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {
		public void onClick(View v) {
			try {
				switch (v.getId()) {
				case R.id.rdobtn1:
					Vimage.setImageLevel(R.drawable.arrowdown);
					commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.";
					comments.setText(commentsfill);
					rdiochk = "1";
					rdobutton2.setSelected(false);
					rdobutton2.setChecked(false);
					rdounknown.setSelected(false);
					rdounknown.setChecked(false);
					yearbuilt1.setEnabled(true);
					permitDate1.setEnabled(true);
					yearbuilt2.setSelection(0);
					yearbuilt2.setEnabled(false);
					permitDate2.setEnabled(false);
					permitDate2.setText("");
					getdate1.setEnabled(true);
					getdate2.setEnabled(false);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;
					break;

				case R.id.rdobtn2:
					Vimage.setImageLevel(R.drawable.arrowdown);
					commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
					comments.setText(commentsfill);
					rdiochk = "2";
					rdobutton1.setSelected(false);
					rdobutton1.setChecked(false);
					rdounknown.setSelected(false);
					rdounknown.setChecked(false);
					yearbuilt2.setEnabled(true);
					permitDate1.setEnabled(false);
					yearbuilt1.setSelection(0);
					yearbuilt1.setEnabled(false);
					permitDate2.setEnabled(true);
					permitDate1.setText("");
					getdate1.setEnabled(false);
					getdate2.setEnabled(true);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;
					break;

				case R.id.rdounknown:
					Vimage.setImageLevel(R.drawable.arrowdown);
					commentsfill = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
					comments.setText(commentsfill);
					rdiochk = "3";
					rdobutton1.setSelected(false);
					rdobutton1.setChecked(false);
					rdobutton2.setSelected(false);
					rdobutton2.setChecked(false);
					yearbuilt1.setSelection(0);
					yearbuilt1.setEnabled(false);
					permitDate1.setText("");
					yearbuilt2.setEnabled(false);
					yearbuilt2.setSelection(0);
					permitDate2.setText("");
					getdate1.setEnabled(false);
					getdate2.setEnabled(false);
					exceedslimit1.setVisibility(v1.GONE);
					lincomments.setVisibility(v1.GONE);
					viewimage = 1;
					break;
				}
			} catch (Exception e) {
				System.out.println("erro in on click " + e);
			}
		}
	};

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			if (rdiochk == "1" || rdobutton1.isChecked()) {
				permitDate1.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			} else if (rdiochk == "2" || rdobutton2.isChecked()) {
				permitDate2.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			} else {

			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);

		}
		return null;
	}

	private void getInspectortypeid() {
		Cursor cur = cf.SelectTablefunction(cf.mbtblInspectionList, "");
		cur.moveToFirst();
		if (cur != null) {
			inspectortypeid = cur.getString(cur
					.getColumnIndex("InspectionTypeId"));
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		
		case R.id.Calcicon1:
			showDialog(DATE_DIALOG_ID);
			break;
			
		case R.id.Calcicon2:
			showDialog(DATE_DIALOG_ID);
			break;	
		
		case R.id.txthelpcontentoptionA:
			
			cf.alerttitle="A - Meets 2001 FBC";
		    cf.alertcontent="Built in compliance with the FBC: year built. For homes built in 2002 or 2003, provide a permit application with a date after 3/1/2002: building permit application date (mm/dd/yyyy).";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionB:
			cf.alerttitle="B - Meets SFBC-94";
		    cf.alertcontent="For the HVHZ Only: Built in compliance with the SFBC-94: year built. For homes built in 1994, 1995 or 1996, provide a permit application with a date after 9/1/1994: buildings permit application date (mm/dd/yyyy).";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.txthelpcontentoptionC:
			cf.alerttitle="C - Unknown - Does Not Meet";
		    cf.alertcontent="Unknown or does not meet the requirements of answer A or B.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.savenext:
			comm = comments.getText().toString();
			if (rdobutton1.isChecked()) {
				yearbuilt = yearbuilt1.getSelectedItem().toString();
				permitDate = permitDate1.getText().toString();
			} else if (rdobutton2.isChecked()) {
				yearbuilt = yearbuilt2.getSelectedItem().toString();
				permitDate = permitDate2.getText().toString();
			} else if (rdounknown.isChecked()) {
				yearbuilt = "";
				permitDate = "";
			} else {
				cf.ShowToast("Please select the Building Code Option.",1);
			}
			if (rdobutton1.isChecked() == true
					|| rdobutton2.isChecked() == true) {
				if (yearbuilt.equals("") || yearbuilt.equals("Select")) {
					cf.ShowToast("Please select Year Built.",1);

					if (rdobutton1.isChecked()) {
						yearbuilt1.setFocusableInTouchMode(true);
						yearbuilt1.requestFocus();
					} else if (rdobutton2.isChecked()) {
						yearbuilt2.setFocusableInTouchMode(true);
						yearbuilt2.requestFocus();
					}
				} else if (comm.trim().equals("")) {
					cf.ShowToast("Please enter the comments for Building Code.",1);

					comments.requestFocus();
				} else {

					if (yearvalidation(yearbuilt) == "true") {

						if ((yearbuilt.equals("2002") || yearbuilt
								.equals("2003"))
								|| ((yearbuilt.equals("1994")
										|| yearbuilt.equals("1995") || yearbuilt
											.equals("1996")))) {
							if (permitDate.equals("")) {
								cf.ShowToast("Please select the Permit Application Date.",1);

								if (rdobutton1.isChecked()) {
									permitDate1.setFocusableInTouchMode(true);
									permitDate1.requestFocus();
								} else if (rdobutton2.isChecked()) {
									permitDate2.setFocusableInTouchMode(true);
									permitDate2.requestFocus();
								}
							} else {
								if (checkforpermitdateistoday(permitDate) == "true") {
									if (compareyearbuiltandpermidate(yearbuilt,
											permitDate) == "true") {
										insertdata();
									}

									else if (checkforpermitdateistoday(permitDate) == "false") {
										if (rdobutton1.isChecked()) {
											if (yearbuilt.equals("2002")
													|| yearbuilt.equals("2003")) {
												cf.ShowToast("Please enter Permit Application Date after(3/1/2002).",1);

											} else {
												cf.ShowToast("The Permit Application Date should not be greater than Year Built.",1);

											}
											permitDate1.setText("");
											permitDate1.requestFocus();
										} else if (rdobutton2.isChecked()) {
											if (yearbuilt.equals("1994")
													|| yearbuilt.equals("1995")
													|| yearbuilt.equals("1996")) {
												cf.ShowToast("Please enter Permit Application Date after(9/1/1994).",1);

											} else {
												cf.ShowToast("The Permit Application Date should not be greater than Year Built.",1);

											}
											permitDate2.setText("");
											permitDate2.requestFocus();
										}
									}

								} else {
									cf.ShowToast("The Permit Application Date should not be greater than Today's Date.",1);

									if (rdobutton1.isChecked()) {
										permitDate1.setText("");
										permitDate1.requestFocus();
									} else if (rdobutton2.isChecked()) {
										permitDate2.setText("");
										permitDate2.requestFocus();
									}
								}
							}
						} else {
							if (permitDate.equals("")) {
								insertdata();
							} else {
								if (checkforpermitdateistoday(permitDate) == "true") {
									if (checkpermitdategraterthanbuild(
											yearbuilt, permitDate) == true) {
										if (compareyearbuiltandpermidate(
												yearbuilt, permitDate) == "true") {
											insertdata();
										}
									} else {
										if (rdobutton1.isChecked()) {
											permitDate1.setText("");
											permitDate1.requestFocus();
										} else if (rdobutton2.isChecked()) {
											permitDate2.setText("");
											permitDate2.requestFocus();
										}
									}
								} else {
									cf.ShowToast("The Permit Application Date should not be greater than Today's Date.",1);
									if (rdobutton1.isChecked()) {
										permitDate1.setText("");
										permitDate1.requestFocus();
									} else if (rdobutton2.isChecked()) {
										permitDate2.setText("");
										permitDate2.requestFocus();
									}
								}
							}
						}

					} else {
						if (rdobutton1.isChecked()) {
							yearbuilt1.setSelection(0);
							yearbuilt1.requestFocus();
							cf.ShowToast("Please select the Year Built greater than 2000.",1);

						} else if (rdobutton2.isChecked()) {
							yearbuilt2.setSelection(0);
							yearbuilt2.requestFocus();
							cf.ShowToast("Please select the Year Built greater than 1994.",1);

						}
					}

				}
			} else if (rdounknown.isChecked() == true) {
				if (comm.equals("")) {
					cf.ShowToast("Please enter the comments for Building Code.",1);
					comments.requestFocus();
				} else {
					insertdata();
				}
			}
			break;

		}
	}

	private boolean checkpermitdategraterthanbuild(String yearbuilt3,
			String permitDate3) {
		// TODO Auto-generated method stub
		int i1 = permitDate3.indexOf("/");
		String result = permitDate3.substring(0, i1);
		int i2 = permitDate3.lastIndexOf("/");
		String result1 = permitDate3.substring(i1 + 1, i2);
		String result2 = permitDate3.substring(i2 + 1);
		result2 = result2.trim();
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt3 = yearbuilt3.trim();
		int yr = Integer.parseInt(yearbuilt3);
		if (yr < j) {
			cf.ShowToast("The Permit Application Date should not be greater than the Year Built.",1);
			return false;
		} else {
			return true;
		}
	}

	private String compareyearbuiltandpermidate(String yearbuilt4,
			String permitDate4) {
		// TODO Auto-generated method stub
		int i1 = permitDate4.indexOf("/");
		String result = permitDate4.substring(0, i1);
		int i2 = permitDate4.lastIndexOf("/");
		String result1 = permitDate4.substring(i1 + 1, i2);
		String result2 = permitDate4.substring(i2 + 1);
		result2 = result2.trim();
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt4 = yearbuilt4.trim();
		int yr = Integer.parseInt(yearbuilt4);
		if (rdobutton1.isChecked() == true) {

			if (yearbuilt4.equals("2002") || yearbuilt4.equals("2003")) {
				if ((j >= 2002) && (j2 >= 3 || j > 2002)
						&& (j1 >= 2 || j > 2002 || j2 > 3)) {
					if (j < 2002) {
						compchk = "true";
					} else if (j == yr || j + 1 == yr) {
						compchk = "false";
						AlertDialog.Builder builder = new AlertDialog.Builder(
								BuildCode.this);
						builder.setTitle("Select")
								.setMessage(
										"The Year Built and Permit Application Date are outside of typical values,Please confirm this is correct?")
								.setCancelable(false)
								.setPositiveButton("Ok",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												if (j2 >= 3) {
													if (j1 > 1) {

														chkupdate();

													} else {

														chkupdate1();

													}
												} else {

													chkupdate1();

												}

											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												permitDate1.setText("");
											}

											private void chkupdate() {
												// TODO Auto-generated method
												// stub
												try

												{
													insertdata();
												} catch (Exception e) {
													//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												}
											}
										})
								.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate1();
											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												permitDate1.setText("");
												permitDate1.requestFocus();
											}
										});

						builder.show();

					} else if (j > yr) {
						compchk = "false";
						cf.ShowToast("The Permit Application Date Should not be greater than the Year Built.",1);
						permitDate1.setText("");
						permitDate1.requestFocus();
					}

				} else {
					compchk = "false";
					cf.ShowToast("The Permit Application Date should be greater than(03/01/2002) for the Year Built(2002 or 2003).",1);
					permitDate1.setText("");
					permitDate1.requestFocus();

				}
			} else {
				if (j + 1 == yr || j == yr) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							BuildCode.this);
					builder.setTitle("Select")
							.setMessage(
									"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate();
										}

										private void chkupdate() {
											// TODO Auto-generated method stub
											compchk = "true";
											try

											{
												insertdata();
											} catch (Exception e) {
												//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocode data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												
											}
										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate1();
										}

										private void chkupdate1() {
											// TODO Auto-generated method stub
											compchk = "false";
											permitDate1.setText("");
											permitDate1.requestFocus();
										}
									});

					builder.show();

				} else if (j < yr) {
					compchk = "true";
				} else if (j > yr) {

					compchk = "false";
					cf.ShowToast("The Permit Application Date should not be greater than the Year Built.",1);
					permitDate1.setText("");
					permitDate1.requestFocus();
				}

			}

		} else if (rdobutton2.isChecked() == true) {
			if (yearbuilt4.equals("1994") || yearbuilt4.equals("1995")
					|| yearbuilt4.equals("1996")) {
				if (j <= yr) {
					if ((j >= 1994) && (j2 >= 9 || j > 1994)
							&& (j1 > 1 || j > 1994 || j2 > 9)) {
						if (j == yr || j + 1 == yr) {
							compchk = "false";
							AlertDialog.Builder builder = new AlertDialog.Builder(
									BuildCode.this);
							builder.setTitle("Select")
									.setMessage(
											"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
									.setCancelable(false)
									.setPositiveButton(
											"Ok",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													chkupdate();
												}

												private void chkupdate() {
													// TODO Auto-generated
													// method stub
													compchk = "true";
													try

													{
														insertdata();
													} catch (Exception e) {
														//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
														
													}
												}
											})
									.setNegativeButton(
											"Cancel",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													chkupdate1();
												}

												private void chkupdate1() {
													// TODO Auto-generated
													// method stub
													compchk = "false";
													permitDate2.setText("");
													permitDate2.requestFocus();
												}
											});

							builder.show();
						} else {
							compchk = "true";
						}
					} else {
						cf.ShowToast("The Permit Application Date should be greater than(09/01/1994) for the Year Built(1994 to 1996)",1);
						permitDate2.setText("");
						permitDate2.requestFocus();
						compchk = "false";
					}
				} else if (j > yr) {
					ShowToast toast = new ShowToast(getBaseContext(),
							"The Permit Application Date should not be greater than the Year Built.");
					permitDate2.setText("");
					permitDate2.requestFocus();
					compchk = "false";
				} else if (j == yr || j + 1 == yr) {
					compchk = "false";
					AlertDialog.Builder builder = new AlertDialog.Builder(
							BuildCode.this);
					builder.setTitle("Select")
							.setMessage(
									"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate();
										}

										private void chkupdate() {
											// TODO Auto-generated method stub
											compchk = "true";
											try

											{
												insertdata();
											} catch (Exception e) {
												//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" in inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												
											}
										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate1();

										}

										private void chkupdate1() {
											// TODO Auto-generated method stub
											compchk = "false";
											permitDate2.setText("");
											permitDate2.requestFocus();
										}
									});

					builder.show();

				} else {
					cf.ShowToast("The Permit Application Date should be greater than(09/01/1994) for the Year Built(1994 to 1996).",1);
					permitDate2.setText("");
					permitDate2.requestFocus();
					compchk = "false";
				}
			} else {
				if (j <= yr) {
					if (j == yr || j + 1 == yr) {
						compchk = "false";
						AlertDialog.Builder builder = new AlertDialog.Builder(
								BuildCode.this);
						builder.setTitle("Select")
								.setMessage(
										"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
								.setCancelable(false)
								.setPositiveButton("Ok",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate();
											}

											private void chkupdate() {
												// TODO Auto-generated method
												// stub
												compchk = "true";
												try

												{
													insertdata();
												} catch (Exception e) {
													//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" in inserting or updating buildocde data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
													
												}
											}
										})
								.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate1();
											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												permitDate2.setText("");
												permitDate2.requestFocus();
											}
										});

						builder.show();

					} else {
						compchk = "true";
					}
				} else {
					cf.ShowToast("The Permit Application Date should not be greater than the Year Built.",1);
					compchk = "false";
				}
			}
		}
		return compchk;
	}

	private String checkforpermitdateistoday(String permitDate3) {
		String chkdate = null;
		int i1 = permitDate3.indexOf("/");
		String result = permitDate3.substring(0, i1);
		int i2 = permitDate3.lastIndexOf("/");
		String result1 = permitDate3.substring(i1 + 1, i2);
		String result2 = permitDate3.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;
		if (j < thsyr) {
			chkdate = "true";
		} else if (j == thsyr) {
			if (j1 <= curmnth && j2 <= curdate) {
				chkdate = "true";
			} else {
				chkdate = "false";
			}
		}

		return chkdate;

	}

	private String yearvalidation(String yearbuilt3) {
		// TODO Auto-generated method stub
		int year = Integer.parseInt(yearbuilt3);
		String chk = null;
		if (rdobutton1.isChecked() == true) {
			if (year > 2000 && year <= current_year) {
				chk = "true";
			} else {
				chk = "false";
			}
		} else if (rdobutton2.isChecked() == true) {
			if (year > 1993 && year <= current_year) {
				chk = "true";
			} else {
				chk = "false";
			}
		}
		return chk;
	}

	private void insertdata() {
		// TODO Auto-generated method stub
		try {
			Cursor c2 = cf.SelectTablefunction(cf.tbl_questionsdata,
					"where SRID='" + cf.Homeid + "'");
			int rws = c2.getCount();
			System.out.println("yearbuilt "+yearbuilt+ " permitDate "+permitDate+ " rdiochk "+rdiochk);
			if (rws == 0) {
				cf.db.execSQL("INSERT INTO "
						+ cf.tbl_questionsdata
						+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
						+ "VALUES ('" + cf.Homeid + "','" + yearbuilt + "','"
						+ permitDate + "','" + rdiochk + "','" + 0 + "','" + 0
						+ "','','','','','" + 0 + "','" + 0 + "','','" + 0
						+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','','" + 0 + "','" + 0 + "','" + 0 + "','','" + 0
						+ "','','" + 0 + "','" + 0 + "','','','','','','','','"
						+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','" + 0 + "','" + 0 + "','" + 0 + "','','" + 0
						+ "','" + 0 + "','','" + 0 + "','','','" + cd + "','"
						+ 0 + "')");
			} else {
				cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
						+ " SET BuildingCodeYearBuilt='" + yearbuilt
						+ "',BuildingCodePerApplnDate ='" + permitDate
						+ "',BuildingCodeValue='" + rdiochk + "',ModifyDate='"
						+ md + "' WHERE SRID ='" + cf.Homeid.toString() + "'");
			}
			c2.close();

			c2 = cf.SelectTablefunction(cf.tbl_comments, "where SRID='"
					+ cf.Homeid + "'");
			rws = c2.getCount();
			if (rws == 0) {
				cf.db.execSQL("INSERT INTO "
						+ cf.tbl_comments
						+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment,RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
						+ " VALUES ('"
						+ cf.Homeid
						+ "','"
						+ cf.InspectionType
						+ "','"
						+ cf.convertsinglequotes(comments.getText().toString())
						+ "','','','','','','','','','','','','','','','','','','','"
						+ cd + "')");
			} else {
				cf.db.execSQL("UPDATE " + cf.tbl_comments
						+ " SET BuildingCodeComment='"
						+ cf.convertsinglequotes(comments.getText().toString())
						+ "',BuildingCodeAdminComment='',CreatedOn ='" + md
						+ "'" + " WHERE SRID ='" + cf.Homeid.toString() + "'");
			}
			cf.getInspectorId();

			Cursor c3 = cf.SelectTablefunction(cf.SubmitCheckTable,
					"where Srid='" + cf.Homeid + "'");
			int subchkrws = c3.getCount();
			if (subchkrws == 0) {
				cf.db.execSQL("INSERT INTO "
						+ cf.SubmitCheckTable
						+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
						+ "VALUES ('" + cf.InspectorId + "','" + cf.Homeid
						+ "',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0)");
			} else {
				cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
						+ " SET fld_builcode='1' WHERE Srid ='" + cf.Homeid
						+ "' and InspectorId='" + cf.InspectorId + "'");
			}
			updatecnt = "1";
		} catch (Exception e) {
			updatecnt = "0";
			cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
			
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" problem in saving inserting or updating buildocde datas due to invalid characters on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			
		}

		if (updatecnt == "1") {
			cf.changeimage();
			// buildtick.setVisibility(visibility);
			cf.ShowToast("Building Code Details saved successfully.",1);

			iInspectionList = new Intent(BuildCode.this, RoofCover.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("iden", "test");
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);

			startActivity(iInspectionList);
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(BuildCode.this, InspectionList.class);
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	    if(focus==1)
	    {
	    	focus=0;
	    comments.setText(comments.getText().toString());    
	    }
	 }         

}