package idsoft.inspectiondepot.B11802;

import idsoft.inspectiondepot.B11802.RoofWall.QUES_textwatcher;
import idsoft.inspectiondepot.B11802.RoofWall.Touch_Listener;

import java.util.LinkedHashMap;
import java.util.Map;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class WallConstruction extends Activity {
	private TableLayout tbllayout8, exceedslimit1;
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();
	View v1;public int focus=0;
	String[] rowlength,arrcomment1,array_spiner;
	LinearLayout lincomments,wallcons_parrant,wallcons_type1;
	CheckBox[] cb;
	CheckBox temp_st;
	private CheckBox chkAnswer1, chkAnswer2, chkAnswer3, chkAnswer4,
			chkAnswer5;
	String per1 = "0", per2 = "0", per3 = "0", per4 = "0", per5 = "0",updatecnt,woodframeval = "0", reinmasonryval = "0", unreinmasonryval = "0",
			pouredconcreteval = "0", otherwallval = "0", comm,InspectionType, status, homeId, identity,
			otherwallconstxt, wallconstothertxt = "",inspectortypeid,commdescrip="",conchkbox, comm2, descrip,dupcomm;
	TextView nocommentsdisp, wallcons_TV_type1,txtheadingwallconstruction,helptxt,prevmitidata;
	Spinner spn1, spn2, spn3, spn4, spn5;
	int per11 = 0, per12 = 0, per13 = 0, per14 = 0, per15 = 0, total,
			commentsch,totp = 0,optionid,value, Count,viewimage = 1;
	Button savenext, prev;
	ArrayAdapter adapter;
	Intent iInspectionList;
	ListView list;
	ImageView Vimage;
	EditText comments, othertext;
	private static final int visibility = 0;
	AlertDialog alertDialog;
	CommonFunctions cf;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	TextWatcher watcher;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		cf.Createtablefunction(7);
		cf.Createtablefunction(8);
		cf.Createtablefunction(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.Homeid = homeId = bunhomeId.getString("homeid");
			cf.Identity = identity = bunhomeId.getString("iden");
			cf.InspectionType = InspectionType = bunhomeId
					.getString("InspectionType");
			cf.status = status = bunhomeId.getString("status");
			cf.value = value = bunhomeId.getInt("keyName");
			cf.Count = Count = bunhomeId.getInt("Count");

		}
		setContentView(R.layout.wallconstruction);
		cf.getInspectorId();
		cf.getinspectiondate();
		cf.getDeviceDimensions();
		cf.changeimage();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.relativeLayout2);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(getApplicationContext(), 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(getApplicationContext(), 28,
				cf, 1));
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		focus=1;
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			  cf.alertcontent="To see help comments, Please select Wall Construction options.";
				  cf.showhelp("HELP",cf.alertcontent);
			
			}
		});
		
		txtheadingwallconstruction = (TextView) findViewById(R.id.txtheadingwallconstruction);
		txtheadingwallconstruction
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>Check all wall construction types for exterior walls of the structure and percentages for each:"
								+ "<br/><font color=red> Note : All of these must equal to 100%"
								+ "</font>"));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);

		Vimage = (ImageView) findViewById(R.id.Vimage);
		this.nocommentsdisp = (TextView) this.findViewById(R.id.notxtcomments);
		
		try {
			Cursor c2 = cf.SelectTablefunction(cf.quetsionsoriginaldata,
					"where homeid='" + cf.Homeid + "'");
			if(c2.getCount()>0){
			c2.moveToFirst();
			String bcoriddata = cf.getsinglequotes(c2.getString(c2.getColumnIndex("wcoriginal")));
			prevmitidata.setText(Html
					.fromHtml("<font color=blue>Original Value : " + "</font>"
							+ "<font color=red>" + bcoriddata + "</font>"));
			}
			else
			{
				prevmitidata.setText(Html
						.fromHtml("<font color=blue>Original Value : "
								+ "</font>" + "<font color=red>Not Available</font>"));

			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" in selecting Wallconstruction Original value on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}
		
		this.tbllayout8 = (TableLayout) this.findViewById(R.id.tableLayoutComm);
		this.lincomments = (LinearLayout) this
				.findViewById(R.id.linearlayoutcomm);
		exceedslimit1 = (TableLayout) this.findViewById(R.id.exceedslimit);
		TextView rccode = (TextView) this.findViewById(R.id.rccode);
		//rccode.setText(cf.rcstr);
		cf.setRcvalue(rccode);
		array_spiner = new String[11];
		array_spiner[0] = "";
		array_spiner[1] = "10";
		array_spiner[2] = "20";
		array_spiner[3] = "30";
		array_spiner[4] = "40";
		array_spiner[5] = "50";
		array_spiner[6] = "60";
		array_spiner[7] = "70";
		array_spiner[8] = "80";
		array_spiner[9] = "90";
		array_spiner[10] = "100";

		try {
			this.chkAnswer1 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer1);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer4 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer4);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer2 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer2);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer3 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer3);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer5 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer5);
		} catch (Exception e) {
		}

		spn1 = (Spinner) this.findViewById(R.id.spranswer1);
		spn4 = (Spinner) this.findViewById(R.id.spranswer4);
		spn2 = (Spinner) this.findViewById(R.id.spranswer2);
		spn5 = (Spinner) this.findViewById(R.id.spranswer5);
		spn3 = (Spinner) this.findViewById(R.id.spranswer3);

		spn1.setEnabled(false);
		spn2.setEnabled(false);
		spn3.setEnabled(false);
		spn4.setEnabled(false);
		spn5.setEnabled(false);

		adapter = new ArrayAdapter(WallConstruction.this, R.layout.spinnerview,
				array_spiner);
		adapter.setDropDownViewResource(R.layout.spinnerviewdropdown);
		spn1.setAdapter(adapter);

		adapter = new ArrayAdapter(WallConstruction.this, R.layout.spinnerview,
				array_spiner);
		adapter.setDropDownViewResource(R.layout.spinnerviewdropdown);
		spn4.setAdapter(adapter);

		adapter = new ArrayAdapter(WallConstruction.this, R.layout.spinnerview,
				array_spiner);
		adapter.setDropDownViewResource(R.layout.spinnerviewdropdown);
		spn2.setAdapter(adapter);

		adapter = new ArrayAdapter(WallConstruction.this, R.layout.spinnerview,
				array_spiner);
		adapter.setDropDownViewResource(R.layout.spinnerviewdropdown);
		spn5.setAdapter(adapter);

		adapter = new ArrayAdapter(WallConstruction.this, R.layout.spinnerview,
				array_spiner);
		adapter.setDropDownViewResource(R.layout.spinnerviewdropdown);
		spn3.setAdapter(adapter);

		chkAnswer1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					woodframeval = "1";
					spn1.setEnabled(true);
					Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				} else {
					spn1.setSelection(0);
					spn1.setEnabled(false);
					woodframeval = "0";

				}
			}
		});
		chkAnswer2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					reinmasonryval = "1";
					spn2.setEnabled(true);
					Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;

				} else {
					spn2.setSelection(0);
					spn2.setEnabled(false);
					reinmasonryval = "0";
				}
			}
		});
		chkAnswer3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					otherwallval = "1";
					othertext.setEnabled(true);
					spn3.setEnabled(true);
					Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;

				} else {
					spn3.setSelection(0);
					spn3.setEnabled(false);
					othertext.setEnabled(false);
					othertext.setText("");
					otherwallval = "0";
				}
			}
		});
		chkAnswer4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					unreinmasonryval = "1";
					spn4.setEnabled(true);
					Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;

				} else {
					spn4.setSelection(0);
					spn4.setEnabled(false);
					unreinmasonryval = "0";
				}
			}
		});
		chkAnswer5.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					pouredconcreteval = "1";

					spn5.setEnabled(true);
					Vimage.setBackgroundResource(R.drawable.arrowdown);tbllayout8.setVisibility(v.GONE);viewimage=1;
				} else {
					spn5.setSelection(0);
					spn5.setEnabled(false);

					pouredconcreteval = "0";
				}
			}
		});
		spn1.setOnItemSelectedListener(new MyOnItemSelectedListener1());
		spn2.setOnItemSelectedListener(new MyOnItemSelectedListener2());
		spn3.setOnItemSelectedListener(new MyOnItemSelectedListener3());
		spn4.setOnItemSelectedListener(new MyOnItemSelectedListener4());
		spn5.setOnItemSelectedListener(new MyOnItemSelectedListener5());


		wallcons_parrant = (LinearLayout)this.findViewById(R.id.wallcons_parrent);
		wallcons_type1=(LinearLayout) findViewById(R.id.wallcons_type);
		wallcons_TV_type1 = (TextView) findViewById(R.id.wallcons_txt);
		comments = (EditText) this.findViewById(R.id.txtwallconscomments);
		comments.setOnTouchListener(new Touch_Listener());
		comments.addTextChangedListener(new QUES_textwatcher());
		
		othertext = (EditText) this.findViewById(R.id.othertxtwallconst);

		try {
			Cursor c2 = cf.db
					.rawQuery(
							"SELECT WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,"
									+ "PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer"
									+ " FROM "
									+ cf.tbl_questionsdata
									+ " WHERE SRID='" + cf.Homeid + "'", null);
			if (c2.getCount() == 0) {
				identity = "test";
			} else {
				c2.moveToFirst();
				if (c2 != null) {
					int tmp = 0;
					String WoodFrameValue, WoodFramePer, ReinMasonryValue, ReinMasonryPer, UnReinMasonryValue, UnReinMasonryPer, PouredConcrete, PouredConcretePer, OtherWal, OtherWallPer;
					WoodFrameValue = c2.getString(c2
							.getColumnIndex("WoodFrameValue"));
					ReinMasonryValue = c2.getString(c2
							.getColumnIndex("ReinMasonryValue"));
					UnReinMasonryValue = c2.getString(c2
							.getColumnIndex("UnReinMasonryValue"));
					PouredConcrete = c2.getString(c2
							.getColumnIndex("PouredConcrete"));
					OtherWal = cf.getsinglequotes(c2.getString(c2
							.getColumnIndex("OtherWal")));
					if (WoodFrameValue.equals("1")) {
						tmp = 0;
						woodframeval = "1";
						this.chkAnswer1.setChecked(true);
						spn1.setEnabled(true);
						per1 = WoodFramePer = c2.getString(c2
								.getColumnIndex("WoodFramePer"));
						if (!WoodFramePer.equals("")) {
							per11 = tmp = Integer.parseInt(WoodFramePer);
							tmp = tmp / 10;
						}
						spn1.setSelection(tmp);
					}
					if (ReinMasonryValue.equals("1")) {
						tmp = 0;
						reinmasonryval = "1";
						this.chkAnswer2.setChecked(true);
						spn2.setEnabled(true);
						per2 = ReinMasonryPer = c2.getString(c2
								.getColumnIndex("ReinMasonryPer"));
						if (!ReinMasonryPer.equals("")) {
							per12 = tmp = Integer.parseInt(ReinMasonryPer);
							tmp = tmp / 10;
						}
						spn2.setSelection(tmp);
					}
					if (UnReinMasonryValue.equals("1")) {
						tmp = 0;
						unreinmasonryval = "1";
						this.chkAnswer4.setChecked(true);
						spn4.setEnabled(true);
						per3 = UnReinMasonryPer = c2.getString(c2
								.getColumnIndex("UnReinMasonryPer"));
						if (!UnReinMasonryPer.equals("")) {
							per13 = tmp = Integer.parseInt(UnReinMasonryPer);
							tmp = tmp / 10;
						}
						spn4.setSelection(tmp);
					}
					if (PouredConcrete.equals("1")) {
						tmp = 0;
						pouredconcreteval = "1";
						this.chkAnswer5.setChecked(true);
						spn5.setEnabled(true);
						per4 = PouredConcretePer = c2.getString(c2
								.getColumnIndex("PouredConcretePer"));
						if (!PouredConcretePer.equals("")) {
							per15 = tmp = Integer.parseInt(PouredConcretePer);
							tmp = tmp / 10;
						}
						spn5.setSelection(tmp);
					}
					if (OtherWal.equals("1")) {
						tmp = 0;
						otherwallval = "1";
						this.chkAnswer3.setChecked(true);
						spn3.setEnabled(true);
						per5 = OtherWallPer = c2.getString(c2
								.getColumnIndex("OtherWallPer"));
						if (!OtherWallPer.equals("")) {
							per13 = tmp = Integer.parseInt(OtherWallPer);
							tmp = tmp / 10;
						}
						otherwallconstxt = cf.getsinglequotes(c2.getString(c2
								.getColumnIndex("OtherWallTitle")));
						othertext.setText(otherwallconstxt);
						spn3.setSelection(tmp);

					}
				}

			}
		} catch (Exception e) {
			
			//cf.Error_LogFile_Creation(e.getMessage()+ " at "+ WallConstruction.this +" problem in retrieving wallconstruction information on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}
		try {
			Cursor cur = cf.db.rawQuery("select * from " + cf.tbl_comments
					+ " where SRID='" + cf.Homeid + "'", null);
			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {
				String wallconstructcomment = cf.getsinglequotes(cur
						.getString(cur
								.getColumnIndex("WallConstructionComment")));
				
				cf.showing_limit(wallconstructcomment,wallcons_parrant,wallcons_type1,wallcons_TV_type1,"474");
				comments.setText(wallconstructcomment);
			}
			}
		} catch (Exception e) {
			
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" in retrieving wallconstruction comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}

		this.savenext = (Button) this.findViewById(R.id.savenext);
		prev = (Button) this.findViewById(R.id.previous);
		this.Vimage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Vimage.setBackgroundResource(R.drawable.arrowup);
				if (viewimage == 1) {

					if (chkAnswer1.isChecked()) {
						optionid = 1;
					} else if (chkAnswer2.isChecked()) {
						optionid = 3;
					} else if (chkAnswer3.isChecked()) {
						optionid = 5;
					} else if (chkAnswer4.isChecked()) {
						optionid = 2;
					} else if (chkAnswer5.isChecked()) {
						optionid = 4;
					} else {
						cf.ShowToast("Please select the Wall Construction option to view Comments.",1);

					}
					try {
						descrip = "";
						arrcomment1 = null;
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.tbl_admcomments
								+ " WHERE questionid='8' and optionid='"
								+ optionid
								+ "' and status='Active' and InspectorId='"
								+ cf.InspectionType + "'", null);
						int rws = c2.getCount();System.out.println("rws= "+rws);
						arrcomment1 = new String[rws];
						c2.moveToFirst();
						if (rws == 0) {
							tbllayout8.setVisibility(arg0.VISIBLE);
							nocommentsdisp.setVisibility(arg0.VISIBLE);
							lincomments.setVisibility(arg0.GONE);
							Vimage.setBackgroundResource(R.drawable.arrowdown);
						} else {

							tbllayout8.setVisibility(arg0.VISIBLE);
							nocommentsdisp.setVisibility(arg0.GONE);
							lincomments.setVisibility(arg0.VISIBLE);
							if (c2 != null) {
								int i=0;
								do {
									arrcomment1[i]=cf.getsinglequotes(c2.getString(c2.getColumnIndex("description")));
									if(arrcomment1[i].contains("null"))
									{
										arrcomment1[i] = arrcomment1[i].replace("null", "");
									}							
									i++;
								} while (c2.moveToNext());						
							}
							c2.close();

							addcomments();
							viewimage = 0;
						}

					} catch (Exception e) {
					
						//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" Wallconstruction comments on arrowclick on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}

				} else if (viewimage == 0) {
					Vimage.setBackgroundResource(R.drawable.arrowdown);
					tbllayout8.setVisibility(arg0.GONE);
					viewimage = 1;
				}

			}

		});

	}
	class Touch_Listener implements OnTouchListener
	{
		   Touch_Listener()
			{
			
				
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	cf.setFocus(comments);
				
				return false;
		    }
	}
	class QUES_textwatcher implements TextWatcher
	{
	     
	     QUES_textwatcher()
		{
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
				
				cf.showing_limit(s.toString(),wallcons_parrant,wallcons_type1,wallcons_TV_type1,"474"); 
			
		}
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}		
	}		 
	private void addcomments() {
		// TODO Auto-generated method stub
		lincomments.removeAllViews();
		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		lincomments.addView(l1);
		
		cb = new CheckBox[arrcomment1.length];


		for (int i = 0; i < arrcomment1.length; i++) {

			LinearLayout l2 = new LinearLayout(this);			
			l2.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.setMargins(0, 0, 20, 0);
			l1.addView(l2);			
			
			cb[i] = new CheckBox(this);
			cb[i].setText(arrcomment1[i].trim());
			cb[i].setTextColor(Color.GRAY);
			cb[i].setSingleLine(false);
			
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
			cb[i].setMaxWidth(padding);
			cb[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,12);
			conchkbox = "chkbox" + i;
			cb[i].setTag(conchkbox);

			map1.put(conchkbox, cb[i]);
			l2.addView(cb[i],paramschk);

			cb[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					if(comments.getText().length()>=474)
					{
						cf.ShowToast("Comment text exceeds the limit.", 1);
						map1.get(getidofselbtn).setChecked(false);
					}
					else
					{
					temp_st = map1.get(getidofselbtn);
					setcomments(temp_st);
					}
				}
			});
		}

	}

	private void setcomments(final CheckBox tempSt) {
		// TODO Auto-generated method stub
		comm2 = "";
		String comm1 = tempSt.getText().toString();
		if (tempSt.isChecked() == true) {
			comm2 += comm1 + " ";
			int commenttextlength = comments.getText().length();
			comments.setText(comments.getText().toString() + " " + comm2);

			if (commenttextlength >= 474) {
				alertDialog = new AlertDialog.Builder(WallConstruction.this)
						.create();
				alertDialog.setTitle("Exceeds Limit");
				alertDialog.setMessage("Comment text exceeds the limit");
				alertDialog.setButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								tempSt.setChecked(false);
								tempSt.setEnabled(true);
							}
						});
				alertDialog.show();
			} else {
				tempSt.setEnabled(false);
			}
		} else {
		}
	}

	private void getInspectortypeid() {
		Cursor cur = cf.SelectTablefunction(cf.mbtblInspectionList, "");
		cur.moveToFirst();
		if (cur != null) {
			inspectortypeid = cur.getString(cur
					.getColumnIndex("InspectionTypeId"));

		}
	}

	public class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			per1 = parent.getItemAtPosition(pos).toString();
			if (per1.equals("")) {
				per1 = "0";
			}
			try {
				per11 = Integer.parseInt(per1);
			} catch (Exception e) {
				
				per11 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in first spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}

		}

		public void onNothingSelected(AdapterView parent) {

			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			per2 = parent.getItemAtPosition(pos).toString();
			if (per1.equals("")) {
				per1 = "0";
			}
			try {
				per12 = Integer.parseInt(per2);
			} catch (Exception e) {
				
				per12 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in second spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {

			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			per3 = parent.getItemAtPosition(pos).toString();
			if (per3.equals("")) {
				per3 = "0";
			}
			try {
				per15 = Integer.parseInt(per3);
			} catch (Exception e) {
				per15 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in third spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			per4 = parent.getItemAtPosition(pos).toString();
			if (per4.equals("")) {
				per4 = "0";
			}
			try {
				per13 = Integer.parseInt(per4);
			} catch (Exception e) {
				per13 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in fourth spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener5 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			per5 = parent.getItemAtPosition(pos).toString();
			if (per5.equals("")) {
				per5 = "0";
			}
			try {
				per14 = Integer.parseInt(per5);
			} catch (Exception e) {
				per14 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in fifth spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		
	
		
		case R.id.txthelpcontentoptionA:
			  cf.alerttitle="Wood Frame";
			  cf.alertcontent="Wood Frame";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.txthelpcontentoptionB:
			  cf.alerttitle="Un-Reinforced Masonry";
			  cf.alertcontent="Un-Reinforced Masonry";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionC:
			  cf.alerttitle="Reinforced Masonry";
			  cf.alertcontent="Reinforced Masonry";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionD:
			  cf.alerttitle="Poured concrete";
			  cf.alertcontent="Poured concrete";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionE:
			  cf.alerttitle="Other";
			  cf.alertcontent="Other";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;

		case R.id.previous:
			iInspectionList = new Intent(WallConstruction.this,
					OpenProtect.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("iden", "prev");
			iInspectionList.putExtra("InspectionType", cf.InspectionType);
			iInspectionList.putExtra("status", cf.status);
			iInspectionList.putExtra("keyName", cf.value);
			iInspectionList.putExtra("Count", cf.Count);
			startActivity(iInspectionList);
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.savenext:
			boolean chkglobal = false;
			comm = comments.getText().toString();
			totp = per11 + per12 + per13 + per14 + per15;

			if ((chkAnswer1.isChecked() == true) && (chkglobal == false)) {
				if (per11 == 0) {
					cf.ShowToast("Please select wood frame percentage.",1);
					chkglobal = true;
				}
			}
			if ((chkAnswer2.isChecked() == true) && (chkglobal == false)) {
				if (per12 == 0) {
					cf.ShowToast("Please select reinforcedmasonry percentage.",1);
					chkglobal = true;

				}
			}
			if ((chkAnswer3.isChecked() == true) && (chkglobal == false)) {
				if (per15 == 0) {
					cf.ShowToast("Please select other percentage.",1);
					chkglobal = true;

				}
			}
			if ((chkAnswer4.isChecked() == true) && (chkglobal == false)) {
				if (per13 == 0) {
					cf.ShowToast("Please select unreinforced masonry percentage.",1);
					chkglobal = true;

				}
			}
			if ((chkAnswer5.isChecked() == true) && (chkglobal == false)) {
				if (per14 == 0) {
					cf.ShowToast("Please select poured concrete percentage.",1);
					chkglobal = true;
				}
			}
			if ((comm.trim().equals("") && (chkglobal == false))) {
				cf.ShowToast("Please enter the comments for Wall Construction Type.",1);
				chkglobal = true;

			} else if ((totp == 0) && (chkglobal == false)) {
				cf.ShowToast("All the Exterior Wall value must be equal to 100.",1);
				chkglobal = true;

			} else if (((totp > 100) || (totp < 100)) && (chkglobal == false)) {
				cf.ShowToast("All the Exterior Wall value must be equal to 100.",1);
				chkglobal = true;

			} else if ((totp == 100) && (chkglobal == false)) {
				if (otherwallval.equals("1")) {
					String s = othertext.getText().toString().trim();

					if (s.equals("")) {
						cf.ShowToast("Enter the text for Other Wall Construction Type.",1);
						chkglobal = true;

					} else {
						try {
							otherwallconstxt = othertext.getText().toString();
							Cursor c2 = cf.db.rawQuery("SELECT * FROM "
									+ cf.tbl_questionsdata + " WHERE SRID='"
									+ cf.Homeid + "'", null);
							int rws = c2.getCount();
							if (rws == 0) {
								cf.db.execSQL("INSERT INTO "
										+ cf.tbl_questionsdata
										+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
										+ "VALUES ('"
										+ cf.Homeid
										+ "','','','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','','','','','"
										+ 0
										+ "','"
										+ 0
										+ "','','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','','"
										+ 0
										+ "','','"
										+ 0
										+ "','"
										+ 0
										+ "','','','','','','','','"
										+ woodframeval
										+ "','"
										+ per11
										+ "','"
										+ reinmasonryval
										+ "','"
										+ per12
										+ "','"
										+ unreinmasonryval
										+ "','"
										+ per13
										+ "','"
										+ pouredconcreteval
										+ "','"
										+ per14
										+ "','"
										+ cf.convertsinglequotes(otherwallconstxt)
										+ "','" + otherwallval + "','" + per15
										+ "','','" + 0 + "','','','" + cd
										+ "','" + 0 + "')");

							} else {
								cf.db.execSQL("UPDATE "
										+ cf.tbl_questionsdata
										+ " SET WoodFrameValue='"
										+ woodframeval
										+ "',WoodFramePer='"
										+ per11
										+ "',ReinMasonryValue='"
										+ reinmasonryval
										+ "',ReinMasonryPer='"
										+ per12
										+ "',UnReinMasonryValue='"
										+ unreinmasonryval
										+ "',UnReinMasonryPer='"
										+ per13
										+ "',PouredConcrete='"
										+ pouredconcreteval
										+ "',PouredConcretePer='"
										+ per14
										+ "',OtherWallTitle='"
										+ cf.convertsinglequotes(otherwallconstxt)
										+ "',OtherWal='" + otherwallval
										+ "',OtherWallPer='" + per15 + "'"
										+ " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");

							}

							Cursor c5 = cf.db.rawQuery("SELECT * FROM "
									+ cf.tbl_comments + " WHERE SRID='"
									+ cf.Homeid + "'", null);
						
							if (c5.getCount() == 0) {

								cf.db.execSQL("INSERT INTO "
										+ cf.tbl_comments
										+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment,RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
										+ " VALUES ('"
										+ cf.Homeid
										+ "','"
										+ cf.InspectionType
										+ "','','','','','','','','','','','','','"
										+ cf.convertsinglequotes(comments
												.getText().toString())
										+ "','','','','','','','" + cd + "')");

							} else {

								cf.db.execSQL("UPDATE "
										+ cf.tbl_comments
										+ " SET WallConstructionComment='"
										+ cf.convertsinglequotes(comments
												.getText().toString())
										+ "',WallConstructionAdminComment='',CreatedOn ='"
										+ md + "'" + " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");
							}

							cf.getInspectorId();

							Cursor c3 = cf.db.rawQuery("SELECT * FROM "
									+ cf.SubmitCheckTable + " WHERE Srid='"
									+ cf.Homeid + "'", null);
							if (c3.getCount() == 0) {
								cf.db.execSQL("INSERT INTO "
										+ cf.SubmitCheckTable
										+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
										+ "VALUES ('" + cf.InspectorId + "','"
										+ cf.Homeid
										+ "',0,0,0,0,0,0,0,0,1,0,0,0,0,0,0)");
							} else {
								cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
										+ " SET fld_wall='1' WHERE Srid ='"
										+ cf.Homeid + "' and InspectorId='"
										+ cf.InspectorId + "'");
							}

							updatecnt = "1";
							cf.changeimage();
							// walltick.setVisibility(visibility);
						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invalid character.",1);
							
							//String errormessage ="inserting or updating wallconstrcution table";
							//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
							
						}

						if (updatecnt == "1") {
							cf.ShowToast("Wall Construction details has been saved successfully.",1);
							Intent intimg1 = new Intent(WallConstruction.this,
									ImagesData.class);
							intimg1.putExtra("homeid", cf.Homeid);
							intimg1.putExtra("InspectionType",
									cf.InspectionType);
							intimg1.putExtra("status", cf.status);
							intimg1.putExtra("keyName", cf.value);
							intimg1.putExtra("Count", cf.Count);
							startActivity(intimg1);
						}
					}
				} else {
					try {
						otherwallconstxt = othertext.getText().toString();
						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.tbl_questionsdata + " WHERE SRID='"
								+ cf.Homeid + "'", null);
						if (c2.getCount() == 0) {
							cf.db.execSQL("INSERT INTO "
									+ cf.tbl_questionsdata
									+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
									+ "VALUES ('" + cf.Homeid + "','','','" + 0
									+ "','" + 0 + "','" + 0 + "','','','','','"
									+ 0 + "','" + 0 + "','','" + 0 + "','" + 0
									+ "','" + 0 + "','" + 0 + "','" + 0
									+ "','','" + 0 + "','" + 0 + "','" + 0
									+ "','','" + 0 + "','','" + 0 + "','" + 0
									+ "','','','','','','','','" + woodframeval
									+ "','" + per11 + "','" + reinmasonryval
									+ "','" + per12 + "','" + unreinmasonryval
									+ "','" + per13 + "','" + pouredconcreteval
									+ "','" + per14 + "','" + otherwallconstxt
									+ "','"
									+ cf.convertsinglequotes(otherwallval)
									+ "','" + per15 + "','','" + 0
									+ "','','','" + cd + "','" + 0 + "')");

						} else {
							cf.db.execSQL("UPDATE " + cf.tbl_questionsdata
									+ " SET WoodFrameValue='" + woodframeval
									+ "',WoodFramePer='" + per11
									+ "',ReinMasonryValue='" + reinmasonryval
									+ "',ReinMasonryPer='" + per12
									+ "',UnReinMasonryValue='"
									+ unreinmasonryval + "',UnReinMasonryPer='"
									+ per13 + "',PouredConcrete='"
									+ pouredconcreteval
									+ "',PouredConcretePer='" + per14
									+ "',OtherWallTitle='"
									+ cf.convertsinglequotes(otherwallconstxt)
									+ "',OtherWal='" + otherwallval
									+ "',OtherWallPer='" + per15 + "'"
									+ " WHERE SRID ='" + cf.Homeid.toString()
									+ "'");

						}
					} catch (Exception e) {
						//String errormessage ="Problem in inserting or updating wallconstruction value";
						//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}

					try {

						Cursor c2 = cf.db.rawQuery("SELECT * FROM "
								+ cf.tbl_comments + " WHERE SRID='" + cf.Homeid
								+ "'", null);
						if (c2.getCount() == 0) {

							cf.db.execSQL("INSERT INTO "
									+ cf.tbl_comments
									+ " (SRID,i_InspectionTypeID,BuildingCodeComment,BuildingCodeAdminComment,RoofCoverComment,RoofCoverAdminComment,RoofDeckComment,RoofDeckAdminComment,RoofWallComment,RoofWallAdminComment,RoofGeometryComment,RoofGeometryAdminComment,GableEndBracingComment,GableEndBracingAdminComment,WallConstructionComment,WallConstructionAdminComment,SecondaryWaterComment,SecondaryWaterAdminComment,OpeningProtectionComment,OpeningProtectionAdminComment,InsOverAllComments,CreatedOn)"
									+ " VALUES ('" + cf.Homeid + "','"
									+ inspectortypeid
									+ "','','','','','','','','','','','','','"
									+ cf.convertsinglequotes(comm)
									+ "','','','','','','','" + cd + "')");

						} else {
							cf.db.execSQL("UPDATE "
									+ cf.tbl_comments
									+ " SET WallConstructionComment='"
									+ cf.convertsinglequotes(comm)
									+ "',WallConstructionAdminComment='',CreatedOn ='"
									+ md + "'" + " WHERE SRID ='"
									+ cf.Homeid.toString() + "'");
						}
					} catch (Exception e) {
						//String errormessage ="Problem in inserting or updating wallconstruction comments";
						//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}
					cf.getInspectorId();
					try {
						Cursor c3 = cf.db.rawQuery("SELECT * FROM "
								+ cf.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						if (c3.getCount() == 0) {
							cf.db.execSQL("INSERT INTO "
									+ cf.SubmitCheckTable
									+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
									+ "VALUES ('" + cf.InspectorId + "','"
									+ cf.Homeid
									+ "',0,0,0,0,0,0,0,0,1,0,0,0,0,0,0)");
						} else {
							cf.db.execSQL("UPDATE " + cf.SubmitCheckTable
									+ " SET fld_wall='1' WHERE Srid ='"
									+ cf.Homeid + "' and InspectorId='"
									+ cf.InspectorId + "'");
						}
					} catch (Exception e) {
						
						//String errormessage ="Problem in inserting or updating SubmitCheck data";
						//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}
					updatecnt = "1";
					if (updatecnt == "1") {
						cf.ShowToast("Wall Construction details has been saved successfully.",1);
						Intent intimg1 = new Intent(WallConstruction.this,
								ImagesData.class);
						intimg1.putExtra("homeid", cf.Homeid);
						intimg1.putExtra("InspectionType", cf.InspectionType);
						intimg1.putExtra("status", cf.status);
						intimg1.putExtra("keyName", cf.value);
						intimg1.putExtra("Count", cf.Count);
						startActivity(intimg1);
					}
				}

			}

			break;

		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(WallConstruction.this, OpenProtect.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("iden", "prev");
			intimg.putExtra("InspectionType", cf.InspectionType);
			intimg.putExtra("status", cf.status);
			intimg.putExtra("keyName", cf.value);
			intimg.putExtra("Count", cf.Count);
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}

	}
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);
	   
	    if(focus==1)
	    {
	    	focus=0;
	    comments.setText(comments.getText().toString());    
	    }  
	 }    
}